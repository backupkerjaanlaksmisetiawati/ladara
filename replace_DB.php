UPDATE ldr_options SET option_value = replace(option_value, 'http://localhost/ladara', 'http://ladara.devtuwaga.com/') WHERE option_name = 'home' OR option_name = 'siteurl';

UPDATE ldr_posts SET guid = replace(guid, 'http://localhost/ladara','http://ladara.devtuwaga.com/');

UPDATE ldr_posts SET post_content = replace(post_content, 'http://localhost/ladara', 'http://ladara.devtuwaga.com/');

UPDATE ldr_postmeta SET meta_value = replace(meta_value,'http://localhost/ladara','http://ladara.devtuwaga.com/');