<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */
// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'db_ladara' );
/** MySQL database username */
define( 'DB_USER', 'root' );
/** MySQL database password */
define( 'DB_PASSWORD', '123masuk' );
/** MySQL hostname */
define( 'DB_HOST', 'devladara.cunel1awijlx.ap-southeast-1.rds.amazonaws.com' );
/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8' );
/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );
/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY', '3a56ad5a649f69b6271f6fbebc05e270904432326ad300208cdac33ea7428352');
define('SECURE_AUTH_KEY', 'be69c698d735f9adb6af6daf4ce212721c4f9bba127ab986e6762dfc6a413984');
define('LOGGED_IN_KEY', '53702ce1aa47eb7c382baedbd52a0ad355a518de431a8ce382776c6cfe2a5f35');
define('NONCE_KEY', 'f6d43de8104db83147fdec0488eb72b71c38e9ca062d6850ad72fb763ed25ca2');
define('AUTH_SALT', '284ad4e93ce612488d681796897316a966607f40c63ea6b33450498bc679d8b1');
define('SECURE_AUTH_SALT', '029117e5efcd0ce4db5b32ea994ce0c4ef8d6c79342f41348f0e3a6c03181166');
define('LOGGED_IN_SALT', '0288bd2d62cc434a603734b2d64296f52ce64b1cab4b3f244a31168c2e6913d7');
define('NONCE_SALT', '48b848f5ee65a8bc8d5d38c581b9f4022c1320c23fdbbf5056873917311b5599');
define('JWT_AUTH_SECRET_KEY', '3a56ad5a649f69b6271f6fbebc05e270904432326ad300208cdac33ea7428352');
define('JWT_AUTH_CORS_ENABLE', true);
/**#@-*/
define( 'WP_CACHE_KEY_SALT', 'staging.ladara.id' );
/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'ldr_';
/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define( 'WP_DEBUG', true );
define('FS_METHOD', 'direct');
define( 'WP_DEBUG_LOG', true );
define( 'WP_DEBUG_DISPLAY', false );
define('WP_HOME','https://staging.ladara.id');
define('WP_SITEURL','https://staging.ladara.id');
$_SERVER['HTTPS'] = 'on';
define( 'FORCE_SSL_ADMIN' , true);
// define( 'SCRIPT_DEBUG', false );

define( 'MERCHANT_URL', 'https://'.$_SERVER['SERVER_NAME'].'/merchant' );

// new media image server 26/10/2020
define( 'IMAGESERVER_URL', 'https://images.'.$_SERVER['SERVER_NAME'] );
define( 'IMAGE_URL', 'https://images.ladara.id' );

/* That's all, stop editing! Happy publishing. */
/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
    define( 'ABSPATH', dirname( __FILE__ ) . '/' );
}
/** Sets up WordPress vars and included files. */
require_once( ABSPATH . 'wp-settings.php' );
