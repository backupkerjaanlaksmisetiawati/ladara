<?php get_header(); ?>
<?php if (have_posts()) : while (have_posts()) : the_post(); ?>


<?php 
$post_id = $post->ID;
$post_name = get_the_title($post_id);
$content_post = get_post($product_id);
$content_post = $content_post->post_content;

?>
    <article id="post-<?php the_ID(); ?>" <?php post_class('clearfix'); ?> role="article" itemscope itemtype="http://schema.org/BlogPosting">

    <div class="row row_qna_page">
        <div class="col-md-12 col_banner_qna">

            <div class="bx_banner_qna">
                <div class="mg_banner_qna">
                    <img src="<?php bloginfo('template_directory'); ?>/library/images/test/ex-banner.jpg">
                </div>               
            </div>


            <div class="row row_body_qna">
                <div class="col-md-4 col-lg-4 col_tab_qna_l">
                    <h3 class="ht_sub_qna">Kategori</h3>

                    <div class="bx_qna_cat">
                        <ul class="ul_qna_cat">
                            <li><a href="#">Pembelian & Pembayaran</a></li>
                            <li><a href="#">Pengiriman Produk</a></li>
                            <li class="act"><a href="#">Pendaftaran Toko</a>
                                <div class="child_qna_cat">
                                    <div class="a_child_qna_cat"><a href="#" class="act">Buka Toko</a></div>
                                    <div class="a_child_qna_cat"><a href="#">Pengaturan Toko</a></div>
                                    <div class="a_child_qna_cat"><a href="#">Proses Pesanan</a></div>
                                    <div class="a_child_qna_cat"><a href="#">Denda Toko</a></div>
                                    <div class="a_child_qna_cat"><a href="#">Pick Up</a></div>
                                    <div class="a_child_qna_cat"><a href="#">Fitur Voucher Toko</a></div>
                                </div>
                            </li>
                            <li><a href="#">Pengembalian Barang & Dana</a></li>
                        </ul>
                    </div>

                </div>
                <div class="col-md-8 col-lg-8 col_tab_qna_r">
                    
                    <div class="bx_det_qna">
                    	<a href="<?php echo home_url(); ?>/bantuan/">
                    		<div class="a_back_qna"><span class="glyphicon glyphicon-menu-left"></span> Kembali</div>
                    	</a>

                        <h1 class="ht_det_qna"><?php echo $post_name; ?></h1>


                       	<div class="cont_det_qna"><?php echo $content_post; ?></div>


                    </div>

                </div>
            </div>



        </div>
    </div>

    </article>

<?php endwhile; ?>
<?php else : ?>
<?php get_template_part('content', '404pages'); ?>
<?php endif; ?>
<?php get_footer(); ?>