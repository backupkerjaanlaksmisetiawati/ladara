<?php
/*
Template Name: Forgot Password Page
*/
?>

<?php get_header(); ?>
<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
<style type="text/css">
	.bx_head_bottom{
		display: none !important;
	}
	#wp-submit{
		background: #0080FF;
	}
</style>
<?php 
$current_user = wp_get_current_user();
$u_id = $current_user->ID;

if(isset($_SESSION['rfr']) AND $_SESSION['rfr'] != ''){
	$referer = $_SESSION['rfr'];
}else{

	$rfr = wp_get_referer();
	if(isset($rfr) AND $rfr != ''){
		$referer = wp_get_referer();
		$_SESSION['rfr'] = $referer;
	}else{
		$referer = home_url();
	}

}
?>

<div class="row"></div>
<div class="row row_login">
	<div class="col-md-12 col_login">
		<div class="box_login">

				<?php 
					if(isset($u_id) AND $u_id != ''){

						unset($_SESSION['rfr']); // unset refferer
				?>
					<div class="ok_login">
						<span class="glyphicon glyphicon-ok ok_icon"></span>
						<div class="ht_okLogin">Selamat datang di Ladara</div>
						<span>Mohon menunggu...</span>
						<img src="<?php bloginfo('template_directory'); ?>/library/images/loading.gif">
					</div>

					<div id="redirect_ok" data-hurl="<?php echo home_url(); ?>"></div>
					<script>
						var plant = document.getElementById('redirect_ok');
						var hurl = plant.getAttribute('data-hurl'); 
					    setTimeout(function(){
				          location.replace(hurl); 
				        }, 2000);
					</script>
				<?php
					}else{
				?>

						<h1 class="ht_forgot">Buat Kata Sandi Baru</h1>
						<div class="info_okLogin">Masukkan alamat email yang terdaftar di Ladara.</div>

					    <form id="formForgot" method="post" autocomplete="nope" action="" class="wp-user-form formForgot" onsubmit="submit_ForgotPassword(event);">
					        <input autocomplete="nope" name="hidden" type="text" style="display:none;">

								<div class="alert alert-success sucs_forgot">
								  	<strong>Sukses!</strong> Password baru Anda telah dikirimkan ke email terdaftar. Silahkan cek email Anda.
								</div>
								<div class="alert alert-danger err_forgot">
								  	<strong>Maaf,</strong> email salah atau tidak terdaftar. Silahkan coba kembali.
								</div>

						        <div class="f_ad_input">
						       
						        	<input type="email" ng-model="username" name="u_email" id="user_login" class="ad_input" placeholder="Masukkan Email" value="" required="required"  maxlength="50" />
						     
						        	<div id="err_email" class="f_err">*Masukkan Email</div>
						        </div>
						        
						        <div class="f_errLogin h_errLogin"></div>
						    
						        <div class="login_fields">
						            <?php // do_action('login_form'); ?>

						            <input type="submit" name="user-submit" id="wp-submit" class="sub_login" value="Kirim"/>

						            <a href="<?php echo home_url(); ?>/login/">
						            	<input type="button" name="user-submit" class="sub_forgot" value="Kembali"/>
						            </a>

						            <input type="hidden" name="redirect_to" value="<?php echo esc_attr($referer); ?>" />
						            <input type="hidden" name="user-cookie" value="1" />
						            <?php wp_nonce_field( 'FGCKNLD' ); ?>
						        </div>
					    </form>

				<?php
					}
				 ?>
		</div>
	</div>
</div>

<?php // ============= cannot back ============= ?>
<script type="text/javascript">
    (function (global) { 

        if(typeof (global) === "undefined") {
            throw new Error("window is undefined");
        }

        var _hash = "!";
        var noBackPlease = function () {
            global.location.href += "#";

            // making sure we have the fruit available for juice (^__^)
            global.setTimeout(function () {
                global.location.href += "!";
            }, 50);
        };

        global.onhashchange = function () {
            if (global.location.hash !== _hash) {
                global.location.hash = _hash;
            }
        };

        global.onload = function () {            
            noBackPlease();

            // disables backspace on page except on input fields and textarea..
            document.body.onkeydown = function (e) {
                var elm = e.target.nodeName.toLowerCase();
                if (e.which === 8 && (elm !== 'input' && elm  !== 'textarea')) {
                    e.preventDefault();
                }
                // stopping event bubbling up the DOM tree..
                e.stopPropagation();
            };          
        }

    })(window);
</script>

<?php endwhile; ?>
<?php else : ?>
		<?php get_template_part( 'content', '404pages' ); ?>	
<?php endif; ?>
<?php get_footer(); ?>