<?php
/*
Template Name: Old Payment Page
*/

require 'vendor/autoload.php';

use Xendit\Invoice;

?>
<?php get_header(); ?>
<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
<style type="text/css">
    header{
        display: none !important;
    }
</style>
<div class="row"></div>

<?php
ob_start();

date_default_timezone_set("Asia/Jakarta");
$nowdate = date('Y-m-d H:i:s');

$current_user = wp_get_current_user();
$u_id = $current_user->ID;
$u_email = $current_user->user_email;
$u_name = $current_user->display_name;

$valid_payment = 0;

if (isset($_SESSION['wc_order_id'])) {
    $order_id = $_SESSION['wc_order_id'];
} else {
    if (isset($_REQUEST['order'])) {
        $encode_id = $_REQUEST['order'];
        $result = base64url_decode($encode_id);
        $result = explode('-', $result);
        $u_id = $result[0];
        $order_id = $result[1];
    } else {
        $order_id = '';
    }
}

    if (isset($order_id) and $order_id != '') {
        $order = new WC_Order($order_id);
        $total_pay = $order->get_total();
        if (isset($total_pay)) {
            $total_pay = round($total_pay, 0);
        } else {
            $total_pay = 0;
        }
        

        $form_params = array(
            'external_id' => 'invoice-'.$order_id,
            'amount' => $total_pay,
            'payer_email' => 'Ladaraindonesia@gmail.com',
            'description' => 'Invoice #'.$order_id,
            'success_redirect_url' => 'https://staging.ladara.id/thank-you/?order='.$order_id,
            'failure_redirect_url' => 'https://staging.ladara.id/failed-payment/?order='.$order_id
        );

        $createInvoice = Invoice::create($form_params);
        
        if (isset($createInvoice) and !empty($createInvoice)) {
            $xendit_id = $createInvoice['id'];
            $status = $createInvoice['status'];
            $invoice_url = $createInvoice['invoice_url'];
            $external_id = 'invoice-'.$order_id;
            $payment_method = '';
            $amount = $createInvoice['amount'];
            $paid_amount = $amount;
            $bank_code = '';
            $paid_at = '';
            $adjusted_received_amount = '';
            $currency = $createInvoice['currency'];
            $type = 'product';

            $_SESSION['invoice_order'] = array('id' => $xendit_id,'order_id' => $order_id,'amount' => $paid_amount);
   
            // =============== SAVE XENDIT DATA ===================
            global $wpdb;
            $wpdb_query = "SELECT * FROM ldr_orders WHERE order_id = '$order_id' AND user_id = '$u_id'";
            $res_query = $wpdb->get_results($wpdb_query, OBJECT);
            $count_res = count($res_query);
            if ($count_res > 0) { // if not exist with same user_id
                global $wpdb;
                $wpdb_query_update = " UPDATE ldr_orders
                                  SET xendit_id='$xendit_id', xendit_url='$invoice_url'
                                  WHERE order_id = '$order_id' AND user_id = '$u_id' ";
                $res_query_update = $wpdb->query($wpdb_query_update);
            }

            global $wpdb;
            $wpdb_query = "SELECT * FROM ldr_payment_log WHERE order_id = '$order_id'";
            $res_query = $wpdb->get_results($wpdb_query, OBJECT);
            $count_res = count($res_query);
            if ($count_res == 0) { // if not exist
                $query = "INSERT INTO ldr_payment_log(order_id,xendit_id,external_id,payment_method,status,paid_amount,bank_code,paid_at,adjusted_received_amount,currency,type) 
                            VALUES('$order_id','$xendit_id','$external_id','$payment_method','$status','$paid_amount','$bank_code','$paid_at','$adjusted_received_amount','$currency','$type')";
                $result = $wpdb->query($query);
            }
            // =============== SAVE XENDIT DATA ===================


            // ============== reset all session ==============
                WC()->cart->get_cart_from_session(); // Load users session
                WC()->cart->empty_cart(true); // Empty the cart
                WC()->session->set('cart', array()); // Empty the session cart data
                unset($_SESSION['isucart']);
            unset($_SESSION['noshipping']);
            unset($_SESSION['usevoucher']);
            unset($_SESSION['noaddress']);
            unset($_SESSION['noshipping']);
            unset($_SESSION['usevoucher']);
            remove_UserCart($u_id);
            // ============== reset all session ==============
            
            $valid_payment = 1;
          
            echo '<META HTTP-EQUIV=REFRESH CONTENT="1; '.$invoice_url.'">';
            exit;
        } else {
            $valid_payment = 0;
        }

        ob_end_flush(); ?>

        <?php
        if ($valid_payment == 1) { // if on proses payment midtrans
        ?>
            <div class="row row_finishCheckout">
                <div class="col-md-12 col_finishCheckout">
                    <div class="bx_finishCheckout">
                        <div class="mg_registerIcon">
                            <img class="lazy" data-src="<?php bloginfo('template_directory'); ?>/library/images/ico_onhold.svg">
                        </div>
                        <div class="ht_register">Mohon Menunggu...</div>

                        <div class="ht_sucs_register">
                            Kami sedang mempersiapkan pembayaran pesanan #<?php echo $order_id ?>.
                        </div>
                        <div class="bx_def_checkout">
                            <a href="<?php echo home_url(); ?>/shop/">
                                <button class="btn_def_checkout">Belanja Lagi</button>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        <?php
        } else { // if has error midtrans
        ?>
            <div class="row row_finishCheckout">
                <div class="col-md-12 col_finishCheckout">
                    <a href="<?php echo home_url(); ?>">
                        <div class="bx_backShop">
                            <span class="glyphicon glyphicon-menu-left"></span> Kembali ke Home
                        </div>
                    </a>
                    <div class="bx_finishCheckout">
                        <div class="mg_registerIcon">
                            <img class="lazy" data-src="<?php bloginfo('template_directory'); ?>/library/images/ico_successpay.svg">
                        </div>
                        <div class="ht_register">Maaf, Sepertinya pesanan #<?php echo $order_id ?> sudah di proses.</div>

                        <div class="ht_sucs_register">
                            Yuk lihat status pesanan ini di halaman order kamu.
                        </div>
                        <div class="bx_def_checkout">
                            <a href="<?php echo home_url(); ?>/my-order/">
                                <button class="btn_def_checkout">Lihat Pesanan Saya</button>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        <?php
        } ?>

    <?php
    } else { // if no order ID
    ?>
        <div class="row row_finishCheckout">
            <div class="col-md-12 col_finishCheckout">
                <a href="<?php echo home_url(); ?>">
                    <div class="bx_backShop">
                        <span class="glyphicon glyphicon-menu-left"></span> Kembali ke Home
                    </div>
                </a>
                <div class="bx_finishCheckout">
                    <div class="mg_registerIcon">
                        <img class="lazy" data-src="<?php bloginfo('template_directory'); ?>/library/images/ico_cartempty.svg">
                    </div>
                    <div class="ht_register">Duh kok kosong? tas belanjamu bisa masuk angin nih</div>

                    <div class="ht_sucs_register">
                        Yuk isi dengan produk terbaru atau produk yang sudah kamu mimpikan dari kemarin.
                    </div>
                    <div class="bx_def_checkout">
                        <a href="<?php echo home_url(); ?>/shop/">
                            <button class="btn_def_checkout">Belanja Lagi</button>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    <?php
    }
    ?>

<?php // ============= cannot back =============?>
<script type="text/javascript">
    (function (global) { 
        if(typeof (global) === "undefined") {
            throw new Error("window is undefined");
        }
        var _hash = "!";
        var noBackPlease = function () {
            global.location.href += "#";
            // making sure we have the fruit available for juice (^__^)
            global.setTimeout(function () {
                global.location.href += "!";
            }, 50);
        };
        global.onhashchange = function () {
            if (global.location.hash !== _hash) {
                global.location.hash = _hash;
            }
        };
        global.onload = function () {            
            noBackPlease();
            // disables backspace on page except on input fields and textarea..
            document.body.onkeydown = function (e) {
                var elm = e.target.nodeName.toLowerCase();
                if (e.which === 8 && (elm !== 'input' && elm  !== 'textarea')) {
                    e.preventDefault();
                }
                // stopping event bubbling up the DOM tree..
                e.stopPropagation();
            };          
        }
    })(window);
</script>

<?php endwhile; ?>
<?php else : ?>
        <?php get_template_part('content', '404pages'); ?>    
<?php endif; ?>
<?php get_footer(); ?>