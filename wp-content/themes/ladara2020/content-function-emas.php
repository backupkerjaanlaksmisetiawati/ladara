<?php
// jangan di takeout, nanti session g jalan lagi T_T g tau kenapa session sering ngadat di server
/*
check if sessions are disabled or if sessions are enabled, but none exists
PHP_SESSION_DISABLED or 0 : if sessions are disabled.
PHP_SESSION_NONE or 1 : if sessions are enabled, but none exists.
PHP_SESSION_ACTIVE or 2 : if sessions are enabled, and one exists.
*/
if (session_status() !== PHP_SESSION_ACTIVE || session_status() !== 2) {
  // session_start();
}

require "vendor/autoload.php";

use Dotenv\Dotenv;
use Ladara\Helpers\email\EmailHelper;
use Ladara\Helpers\builder\UsersBuilder;
use Ladara\Models\Notifications;
use Xendit\EWallets;
use Xendit\Invoice;
use Xendit\VirtualAccounts;
use Xendit\Retail;
use Xendit\Xendit;
use Shipper\Shipper;

$dotenv = Dotenv::createImmutable(__DIR__.'/../../../');
$dotenv->load();

Xendit::setApiKey(getenv('XENDIT_SECRET_KEY'));
Shipper::setProductionMode((getenv('SHIPPER_PRODUCTION_MODE') ?? false))->setApiKey(getenv('SHIPPER_API_KEY'));

date_default_timezone_set("Asia/Jakarta");

gldGlobal();

/** 
 * NOTES :
 * - cek function gld_security_question untuk contoh message log dan message return
 * - cek function gld_rate untuk cara dapetin access token
 */

 /**
 * Register a custom menu page.
 */
add_action( "admin_menu", "linked_url" );
function linked_url() {
  global $menu;
  $dashboard_url = home_url()."/dashboard/";

  add_menu_page( "Dashboard Ladara Emas", "Dashboard Ladara Emas", "read", $dashboard_url."ladara-emas", "", "dashicons-text", 3 );
  add_submenu_page($dashboard_url."ladara-emas", "Daftar Transaksi", "Daftar Transaksi", "read", $dashboard_url."ladara-emas-transaction", "", 1 );
  add_submenu_page($dashboard_url."ladara-emas", "Daftar User", "Daftar User", "read", $dashboard_url."ladara-emas-user", "", 1 );
}

add_action( "admin_menu" , "linkedurl_function" );
function linkedurl_function() {
  global $menu;
  global $submenu;
  
  $dashboard_url = home_url()."/dashboard/";
  
  // ddbug($submenu);  

  // if(isset($submenu["ladara-emas"])) {
  //   foreach($submenu["ladara-emas"] as $key_emas => $submenu_emas) {
  //     $submenu["ladara-emas"][$key_emas][2] = $dashboard_url.$submenu["ladara-emas"][$key_emas][2];
  //   }
  // }

  // $menu[3][2] = $dashboard_url.$menu[3][2];
  
  // $menu[3][2] = $menu[3][2];
}

/******************************** CRON LADARA EMAS ********************************/
  /**
   * Cron untuk proses update transaksi jual emas
   *
   * @author Amy <laksmise@gmail.com>
   */
  function cronJualEmas() {
    global $wpdb;

    ddlog("Start cron update sell history status.", "ladara_emas_cron");
    
    $get_transaksi_emas = $wpdb->get_results(
      "SELECT * FROM ldr_transaksi_emas
        WHERE type='sell' AND status='Pending'"
    );

    foreach($get_transaksi_emas as $transaksi) {

      ddlog("Start if user " . $transaksi->user_id . " for invoice " . $transaksi->invoice_order . " exist or not for cron update sell history status.", "ladara_emas_cron");

      $user_emas = $wpdb->get_row(
        "SELECT id, user_id, email, name, ts_password 
          FROM ldr_user_emas 
          WHERE id=" . $transaksi->user_id . "",
        OBJECT
      );

      if(!empty($user_emas)) {

        ddlog("End if user " . $transaksi->user_id . " for invoice " . $transaksi->invoice_order . " exist or not for cron update sell history status. User exist.", "ladara_emas_cron");

        ddlog("Start login to treasury user " . $user_emas->email . " for cron update sell history status.", "ladara_emas_cron");

        //login ke treasury satu per satu ^^;
        $login_trs = gld_login(array("user_id"=>$user_emas->user_id));
        $login_trs = json_decode($login_trs);

        if($login_trs->code === 200) {

          ddlog("End login to treasury user " . $user_emas->email . " for cron update sell history status. Success.", "ladara_emas_cron");

          $data_email = [
            "invoice_order"     => $transaksi->invoice_order,
            "customer_name"     => $user_emas->name,
            "customer_email"    => $user_emas->email,
            "created_date"      => $transaksi->created_date,
            "total_price"       => $transaksi->total_price,
            "total_emas"        => $transaksi->total_gold,
            "partner_fee"       => $transaksi->partner_fee,
            "total_payment"     => $transaksi->total_payment,
            "history_note"      => ""
          ];

          $customer = new UsersBuilder($user_emas->name, $user_emas->email);
          $admin = new UsersBuilder($GLOBALS["gld_global"]["email_admin"]["name"], $GLOBALS["gld_global"]["email_admin"]["email"]);

          ddlog("Start check sell emas status for invoice " . $transaksi->invoice_order . ".", "ladara_emas_cron");
          
          $authorization = "Authorization: Bearer " . $login_trs->data->access_token;
          $curl_header = array();
          $curl_header[] = $authorization;

          $body_detail_history = [
            "type"        => "sell",
            "invoice_no"  => $transaksi->trs_invoice,
          ];
          
          $ch = curl_init();
          curl_setopt($ch, CURLOPT_URL, getenv('TREASURY_BASE_URL') . "/detail-history");
          curl_setopt($ch, CURLOPT_TIMEOUT, $GLOBALS["gld_global"]["curl_timeout"]);
          curl_setopt($ch, CURLOPT_HTTPHEADER, $curl_header);
          curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
          curl_setopt($ch, CURLOPT_HTTP_VERSION, CURL_HTTP_VERSION_1_1);
          curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
          curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
          curl_setopt($ch, CURLOPT_ENCODING, "");
          curl_setopt($ch, CURLOPT_MAXREDIRS, $GLOBALS["gld_global"]["curl_maxredirs"]);
          curl_setopt($ch, CURLOPT_POSTFIELDS, $body_detail_history);

          $output = curl_exec($ch);
          curl_getinfo($ch, CURLINFO_EFFECTIVE_URL);
          curl_getinfo($ch, CURLINFO_HTTP_CODE);
          curl_close($ch);
          $json_response = json_decode($output);

          if(isset($json_response->meta->code)) {

            if($json_response->meta->code === 200) {

              ddlog("Start update sell emas invoice " . $transaksi->invoice_order . " status from pending to " . strtolower($json_response->data->status) . ".", "ladara_emas_cron");

              if(strtolower($json_response->data->status) === 'approved') {

                $wpdb->query("UPDATE ldr_transaksi_emas SET 
                  status='Approved'
                  WHERE id=" . $transaksi->id);

                $wpdb->query("INSERT INTO ldr_transaksi_emas_log (
                    transaksi_emas_id, 
                    log, 
                    status
                  ) VALUES (
                    " . $transaksi->id . ", 
                    'Penjualan emas dengan invoice " . $transaksi->invoice_order . " disetujui', 
                    'Approved'
                  )");

                ddlog("Start send email approved sell emas invoice " . $transaksi->invoice_order . " to user " . $user_emas->email . ".", "ladara_emas_cron");

                $email_customer = (new EmailHelper($customer))->disetujuiJualEmas($data_email);

                ddlog("End send email approved sell emas invoice " . $transaksi->invoice_order . " to user " . $user_emas->email . ".", "ladara_emas_cron");

                ddlog("Start send email approved sell emas invoice " . $transaksi->invoice_order . " to admin.", "ladara_emas_cron");

                $email_admin = (new EmailHelper($admin))->adminDisetujuiJualEmas($data_email);

                ddlog("End send email approved sell emas invoice " . $transaksi->invoice_order . " to admin.", "ladara_emas_cron");

                ddlog("Start add web notification approved sell emas invoice " . $transaksi->invoice_order . ".", "ladara_emas_cron");

                $notif_data = [
                  "userId"        => $user_emas->user_id, //wajib
                  "title"         => "Selamat! Penjualan emas kamu telah disetujui!", //wajib
                  "descriptions"  => "Penjualan emas kamu sebesar " . str_replace(".", ",", $transaksi->total_gold) . " gram telah disetujui! Yuk cek rate emas terkini di Ladara Emas.", //wajib
                  "type"          => "emas", //pesanan, info, pembayaran, emas // wajib
                  "orderId"       => 0,
                  "data"          => [] //array optional bisa diisi dengan data lainnya
                ];

                Notifications::addNotification($notif_data);

                ddlog("End add web notification approved sell emas invoice " . $transaksi->invoice_order . ".", "ladara_emas_cron");

                ddlog("End update sell emas invoice " . $transaksi->invoice_order . " status from pending to " . strtolower($json_response->data->status) . ".", "ladara_emas_cron");

              }

              if(strtolower($json_response->data->status) === 'rejected') {

                $log_message = "Penjualan emas dengan invoice " . $transaksi->invoice_order . " ditolak";

                $notif_description = "Penjualan emas kamu sebesar " . str_replace(".", ",", $transaksi->total_gold) . " gram telah ditolak!";

                $notif_description = $notif_description . " Kamu dapat menjual emas kembali di Ladara Emas";

                if(isset($json_response->data->AdditionalNote)) {
                  $data_email["history_note"] = strtolower(str_replace("support@treasury.id", "cs@ladara.id", $json_response->data->AdditionalNote));

                  $log_message = $log_message . " karena " . $data_email["history_note"];

                  $notif_description = $notif_description . " Karena " . $data_email["history_note"] . ".";
                }

                $wpdb->query("UPDATE ldr_transaksi_emas SET 
                  status='Rejected', 
                  status_reason='" . $data_email["history_note"] . "'
                  WHERE id=" . $transaksi->id);

                $wpdb->query("INSERT INTO ldr_transaksi_emas_log (
                  transaksi_emas_id, 
                  log, 
                  status
                ) VALUES (
                  " . $transaksi->id . ", 
                  '" . $log_message . "', 
                  'Rejected'
                )");

                ddlog("Start send email rejected sell emas invoice " . $transaksi->invoice_order . " to user " . $user_emas->email . ".", "ladara_emas_cron");

                $email_customer = (new EmailHelper($customer))->jualEmasDitolak($data_email);

                ddlog("End send email rejected sell emas invoice " . $transaksi->invoice_order . " to user " . $user_emas->email . ".", "ladara_emas_cron");

                ddlog("Start send email rejected sell emas invoice " . $transaksi->invoice_order . " to admin.", "ladara_emas_cron");

                $email_admin = (new EmailHelper($admin))->adminJualEmasDitolak($data_email);

                ddlog("End send email rejected sell emas invoice " . $transaksi->invoice_order . " to admin.", "ladara_emas_cron");

                ddlog("Start add web notification rejected sell emas invoice " . $transaksi->invoice_order . ".", "ladara_emas_cron");

                $notif_data = [
                  "userId"        => $user_emas->user_id,
                  "title"         => "Duh! penjualan emas kamu ditolak",
                  "descriptions"  => $notif_description,
                  "type"          => "emas",
                  "orderId"       => 0,
                  "data"          => []
                ];

                Notifications::addNotification($notif_data);

                ddlog("End add web notification rejected sell emas invoice " . $transaksi->invoice_order . ".", "ladara_emas_cron");

                ddlog("End update sell emas invoice " . $transaksi->invoice_order . " status from pending to " . strtolower($json_response->data->status) . ".", "ladara_emas_cron");

              }

              ddlog("End check sell emas status for invoice " . $transaksi->invoice_order . ". Success with status " . strtolower($json_response->data->status) . ".", "ladara_emas_cron");

            } else {

              ddlog("End check sell emas status for invoice " . $transaksi->invoice_order . ". Error " . $json_response->data->code . " (" . $json_response->data->message . ").", "ladara_emas_cron");

            }

          } else {

            ddlog("End check sell emas status for invoice " . $transaksi->invoice_order . ". Failed to connect treasury api. \n" . htmlentities($output), "ladara_emas_cron");

          }

        } else {

          ddlog("End login to treasury user " . $user_emas->email . " for cron update sell history status. Error " . $login_trs->code . " (" . $login_trs->message . ").", "ladara_emas_cron");

        }

      } else {

        ddlog("End if user " . $transaksi->user_id . " for invoice " . $transaksi->invoice_order . " exist or not for cron update sell history status. User isn't exist.", "ladara_emas_cron");

      }

    }

    ddlog("End cron update sell history status.", "ladara_emas_cron");
  }
  add_action( "cronJualEmas", "cronJualEmas", 0 );

  /**
   * Cron untuk proses update transaksi beli emas
   *
   * @author Amy <laksmise@gmail.com>
   */
  function cronBeliEmas() {
    global $wpdb;

    ddlog("Start cron update buy history status.", "ladara_emas_cron");
    
    $current_date = date("Y-m-d H:i:s");
    
    $get_transaksi_emas = $wpdb->get_results(
      "SELECT * FROM ldr_transaksi_emas
        WHERE type='buy' AND status='Pending'"
    );

    foreach($get_transaksi_emas as $transaksi) {
      
      ddlog("Start if user " . $transaksi->user_id . " for invoice " . $transaksi->invoice_order . " exist or not not for cron update buy history status.", "ladara_emas_cron");

      $user_emas = $wpdb->get_row(
        "SELECT id, user_id, email, name, ts_password 
          FROM ldr_user_emas 
          WHERE id=" . $transaksi->user_id . "",
        OBJECT
      );

      if(!empty($user_emas)) {

        ddlog("End if user " . $transaksi->user_id . " for invoice " . $transaksi->invoice_order . " exist or not for cron update buy history status. User exist.", "ladara_emas_cron");

        $customer = new UsersBuilder($user_emas->name, $user_emas->email);
        $admin = new UsersBuilder($GLOBALS["gld_global"]["email_admin"]["name"], $GLOBALS["gld_global"]["email_admin"]["email"]);

        $data_email = [
          "invoice_order"     => $transaksi->invoice_order,
          "customer_name"     => $user_emas->name,
          "customer_email"    => $user_emas->email,
          "due_date"          => $transaksi->due_date,
          "created_date"      => $transaksi->created_date,
          "metode_pembayaran" => $transaksi->payment_method,
          "total_price"       => $transaksi->total_price,
          "total_emas"        => $transaksi->total_gold,
          "booking_fee"       => $transaksi->booking_fee,
          "xendit_fee"        => $transaksi->xendit_fee,
          "tax"               => $transaksi->tax,
          "partner_fee"       => $transaksi->partner_fee,
          "total_payment"     => $transaksi->total_payment
        ];

        if(!empty($transaksi->xendit_va)) {
          $data_email["no_va"] = $transaksi->xendit_va;
        }

        if($transaksi->due_date < $current_date) {
          ddlog("Start update buy emas invoice " . $transaksi->invoice_order . " status from pending to expired. Expired on " . $transaksi->due_date . ".", "ladara_emas_cron");

          $wpdb->query("UPDATE ldr_transaksi_emas SET 
            status='Canceled', 
            status_reason='Pembelian melewati batas pembayaran' 
            WHERE id=" . $transaksi->id);

          $wpdb->query("INSERT INTO ldr_transaksi_emas_log (
              transaksi_emas_id, 
              log, 
              status
            ) VALUES (
              " . $transaksi->id . ", 
              'Pembelian emas dengan invoice " . $transaksi->invoice_order . " dibatalkan karena melewati batas pembayaran.', 
              'Canceled'
            )");

          ddlog("Start send email expired buy emas invoice " . $transaksi->invoice_order . " to " . $user_emas->email . ".", "ladara_emas_cron");
          
          $email_customer = (new EmailHelper($customer))->pembatalanBeliEmas($data_email);

          ddlog("End send email expired buy emas invoice " . $transaksi->invoice_order . " to " . $user_emas->email . ".", "ladara_emas_cron");

          ddlog("Start send email expired buy emas invoice " . $transaksi->invoice_order . " to admin.", "ladara_emas_cron");

          $email_admin = (new EmailHelper($admin))->adminPembatalanBeliEmas($data_email);

          ddlog("End send email expired buy emas invoice " . $transaksi->invoice_order . " to admin.", "ladara_emas_cron");

          ddlog("Start add web notification expired buy emas invoice " . $transaksi->invoice_order . ".", "ladara_emas_cron");

          $notif_data = [
            "userId"        => $user_emas->user_id,
            "title"         => "Invoice pembelian emas kamu telah expired!",
            "descriptions"  => "Pembelian emas kamu dengan invoice " . $transaksi->invoice_order . " telah expired! Tapi kamu dapat membeli emas kembali di Ladara Emas.",
            "type"          => "emas",
            "orderId"       => 0,
            "data"          => []
          ];

          Notifications::addNotification($notif_data);

          ddlog("End add web notification expired buy emas invoice " . $transaksi->invoice_order . ".", "ladara_emas_cron");
          
          ddlog("End update buy emas invoice " . $transaksi->invoice_order . " status from pending to expired. Expired on " . $transaksi->due_date . ".", "ladara_emas_cron");

        } else {

          ddlog("Start login to treasury user " . $user_emas->email . " for cron update buy history status.", "ladara_emas_cron");

          //login ke treasury satu per satu ^^;
          $login_trs = gld_login(array("user_id"=>$user_emas->user_id));
          $login_trs = json_decode($login_trs);

          if($login_trs->code === 200) {

            ddlog("End login to treasury user " . $user_emas->email . " for cron update buy history status. Success.", "ladara_emas_cron");

            $authorization = "Authorization: Bearer " . $login_trs->data->access_token;
            $curl_header = array();
            $curl_header[] = $authorization;

            ddlog("Start check detail history on treasury invoice " . $transaksi->invoice_order . " for cron update buy history status.", "ladara_emas_cron");

            $body_detail_history = [
              "type"        => $transaksi->type,
              "invoice_no"  => $transaksi->trs_invoice,
            ];

            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, getenv('TREASURY_BASE_URL') . "/detail-history");
            curl_setopt($ch, CURLOPT_TIMEOUT, $GLOBALS["gld_global"]["curl_timeout"]);
            curl_setopt($ch, CURLOPT_HTTPHEADER, $curl_header);
            curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
            curl_setopt($ch, CURLOPT_HTTP_VERSION, CURL_HTTP_VERSION_1_1);
            curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_ENCODING, "");
            curl_setopt($ch, CURLOPT_MAXREDIRS, $GLOBALS["gld_global"]["curl_maxredirs"]);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $body_detail_history);

            $output = curl_exec($ch);
            curl_getinfo($ch, CURLINFO_EFFECTIVE_URL);
            curl_getinfo($ch, CURLINFO_HTTP_CODE);
            curl_close($ch);
            $json_response = json_decode($output);

            if(isset($json_response->meta->code)) {

              if($json_response->meta->code === 200) {

                $trsStatus = str_replace(" ", "", strtolower($json_response->data->status));

                if($trsStatus == "pembayaranberhasil") {

                  ddlog("Start update buy emas invoice " . $transaksi->invoice_order . " status from pending to paid for cron update buy history status.", "ladara_emas_cron");

                  $wpdb->query("UPDATE ldr_transaksi_emas SET 
                    status = 'Success'
                    WHERE id = " . $transaksi->id);
                  
                  $wpdb->query("INSERT INTO ldr_transaksi_emas_log (
                      transaksi_emas_id, 
                      log, 
                      status
                    ) VALUES (
                      " . $transaksi->id . ", 
                      'Pembelian emas dengan invoice " . $transaksi->invoice_order . " sukses.', 
                      'Success'
                    )");

                  ddlog("Start send email paid buy emas invoice " . $transaksi->invoice_order . " to " . $user_emas->email . " for cron update buy history status.", "ladara_emas_cron");

                  $email_customer = (new EmailHelper($customer))->suksesBeliEmas($data_email);

                  ddlog("End send email paid buy emas invoice " . $transaksi->invoice_order . " to " . $user_emas->email . " for cron update buy history status.", "ladara_emas_cron");

                  ddlog("Start send email paid buy emas invoice " . $transaksi->invoice_order . " to admin for cron update buy history status.", "ladara_emas_cron");

                  $email_admin = (new EmailHelper($admin))->adminSuksesBeliEmas($data_email);

                  ddlog("End send email paid buy emas invoice " . $transaksi->invoice_order . " to admin for cron update buy history status.", "ladara_emas_cron");

                  ddlog("Start add web notification paid buy emas invoice " . $transaksi->invoice_order . " for cron update buy history status.", "ladara_emas_cron");

                  $notif_data = [
                    "userId"        => $user_emas->user_id,
                    "title"         => "Selamat! Pembelian emas kamu telah berhasil!",
                    "descriptions"  => "Pembelian emas kamu sebesar " . str_replace(".", ",", $transaksi->total_gold) . " gram telah berhasil! Yuk cek rate emas terkini di Ladara Emas.",
                    "type"          => "emas",
                    "orderId"       => 0,
                    "data"          => []
                  ];

                  Notifications::addNotification($notif_data);

                  ddlog("End add web notification paid buy emas invoice " . $transaksi->invoice_order . " for cron update buy history status.", "ladara_emas_cron");

                  ddlog("End update buy emas invoice " . $transaksi->invoice_order . " status from pending to paid for cron update buy history status. Success.", "ladara_emas_cron");

                } else if($trsStatus == "bataswaktupembayaranhabis") {

                  ddlog("Start update buy emas invoice " . $transaksi->invoice_order . " status from pending to expired from treasury for cron update buy history status.", "ladara_emas_cron");

                  $wpdb->query("UPDATE ldr_transaksi_emas SET 
                    status = 'Canceled', 
                    status_reason='Pembelian melewati batas pembayaran' 
                    WHERE id = " . $transaksi->id);

                  $wpdb->query("INSERT INTO ldr_transaksi_emas_log (
                      transaksi_emas_id, 
                      log, 
                      status
                    ) VALUES (
                      " . $transaksi->id . ", 
                      'Pembelian emas dengan invoice " . $transaksi->invoice_order . " dibatalkan karena melewati batas pembayaran.', 
                      'Canceled'
                    )");

                  ddlog("Start send email expired from treasury buy emas invoice " . $transaksi->invoice_order . " to " . $user_emas->email . " for cron update buy history status.", "ladara_emas_cron");

                  $email_customer = (new EmailHelper($customer))->pembatalanBeliEmas($data_email);

                  ddlog("End send email expired from treasury buy emas invoice " . $transaksi->invoice_order . " to " . $user_emas->email . " for cron update buy history status.", "ladara_emas_cron");

                  ddlog("Start send email expired from treasury buy emas invoice " . $transaksi->invoice_order . " to admin for cron update buy history status.", "ladara_emas_cron");

                  $email_admin = (new EmailHelper($admin))->adminPembatalanBeliEmas($data_email);

                  ddlog("End send email expired from treasury buy emas invoice " . $transaksi->invoice_order . " to admin for cron update buy history status.", "ladara_emas_cron");

                  ddlog("Start add web notification expired from treasury buy emas invoice " . $transaksi->invoice_order . " for cron update buy history status.", "ladara_emas_cron");

                  $notif_data = [
                    "userId"        => $user_emas->user_id,
                    "title"         => "Invoice pembelian emas kamu telah expired!",
                    "descriptions"  => "Pembelian emas kamu dengan invoice " . $transaksi->invoice_order . " telah expired! Tapi kamu dapat membeli emas kembali di Ladara Emas.",
                    "type"          => "emas",
                    "orderId"       => 0,
                    "data"          => []
                  ];

                  Notifications::addNotification($notif_data);

                  ddlog("End add web notification expired from treasury buy emas invoice " . $transaksi->invoice_order . " for cron update buy history status.", "ladara_emas_cron");

                  ddlog("End update buy emas invoice " . $transaksi->invoice_order . " status from pending to expired from treasury for cron update buy history status. Success.", "ladara_emas_cron");
                  
                }

                ddlog("End check detail history on treasury invoice " . $transaksi->invoice_order . " for cron update buy history status. Success.", "ladara_emas_cron");

              } else {

                ddlog("End check detail history on treasury invoice " . $transaksi->invoice_order . " for cron update buy history status. Error " . $json_response->meta->code . " (" . $json_response->data->message . ").", "ladara_emas_cron");

              }

            } else {

              ddlog("End check detail history on treasury invoice " . $transaksi->invoice_order . " for cron update buy history status. Failed to connect treasury api. \n" . htmlentities($output), "ladara_emas_cron");

            }

          } else {

            ddlog("End login to treasury user " . $user_emas->email . " for cron update buy history status. Error " . $login_trs->code . " (" . $login_trs->message . ").", "ladara_emas_cron");

          }

        }

      } else {

        ddlog("End if user " . $transaksi->user_id . " for invoice " . $transaksi->invoice_order . " exist or not cron update buy history status. User isn't exist.", "ladara_emas_cron");

      }

    }

    ddlog("End cron update buy history status.", "ladara_emas_cron");
  }
  add_action( "cronBeliEmas", "cronBeliEmas", 0 );

  /**
   * Cron verify user
   *
   * @author Amy <laksmise@gmail.com>
   */
  function cronVerifikasiLadaraEmasAkun() {
    global $wpdb;

    ddlog("Start cron user verification.", "ladara_emas_cron");
    
    $get_users = $wpdb->get_results(
      "SELECT id, user_id, name, email, ts_password FROM ldr_user_emas WHERE verified_user=0",
      OBJECT
    );

    foreach($get_users as $user) {

      ddlog("Start login treasury with email " . $user->email . " for cron user verification.", "ladara_emas_cron");

      //login ke treasury satu per satu ^^;
      $login_trs = gld_login(array("user_id"=>$user->user_id));
      $login_trs = json_decode($login_trs);

      if($login_trs->code === 200) {

        ddlog("End login treasury with email " . $user->email . " for cron user verification. Success.", "ladara_emas_cron");

        ddlog("Start get treasury profile with email " . $user->email . " for cron user verification.", "ladara_emas_cron");

        $authorization = "Authorization: Bearer " . $login_trs->data->access_token;
        $curl_header = array();
        $curl_header[] = $authorization;

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, getenv('TREASURY_BASE_URL') . "/profile");
        curl_setopt($ch, CURLOPT_TIMEOUT, $GLOBALS["gld_global"]["curl_timeout"]);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
        curl_setopt($ch, CURLOPT_HTTP_VERSION, CURL_HTTP_VERSION_1_1);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "GET");
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_ENCODING, "");
        curl_setopt($ch, CURLOPT_MAXREDIRS, $GLOBALS["gld_global"]["curl_maxredirs"]);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $curl_header);

        $output = curl_exec($ch);
        curl_getinfo($ch, CURLINFO_EFFECTIVE_URL);
        curl_getinfo($ch, CURLINFO_HTTP_CODE);
        curl_close($ch);
        $json_response = json_decode($output);

        if(isset($json_response->meta->code)) {

          if($json_response->meta->code !== 200) {

            ddlog("End get treasury profile with email " . $user->email . " for cron user verification. Error " . $json_response->data->code. " (" . $json_response->data->message. ").", "ladara_emas_cron");

          } else {

            ddlog("End get treasury profile with email " . $user->email . " for cron user verification. Success.", "ladara_emas_cron");

            // ddbug($login_trs, false);
            // ddbug($json_response, false);
            // ddbug($user);

            if (
              strtolower($json_response->data->RegNoKYCStatus) == "verified" &&
              strtolower($json_response->data->SelfieKYCStatus) == "verified"
            ) {

              ddlog("User email " . $user->email . " verified.", "ladara_emas_cron");

              $gold_balance = $json_response->data->gold_balance;
              $gold_balance_in_currency = $json_response->data->gold_balance_in_currency;

              $query_update_ldr_user_emas = "UPDATE ldr_user_emas SET 
                  verified_user=1, 
                  gold_balance=" . $gold_balance . ",
                  gold_balance_in_currency = " . $gold_balance_in_currency . "
                  WHERE id=" . $user->id;
              $wpdb->query($query_update_ldr_user_emas);

              $data_email = [
                "customer_name"   => $user->name,
                "customer_email"  => $user->email
              ];

              ddlog("Start send email user verification to " . $user->email . " for cron user verification.", "ladara_emas_cron");

              $customer = new UsersBuilder($user->name, $user->email);
              $email_customer = (new EmailHelper($customer))->verifikasiAkunEmas($data_email);

              ddlog("End send email user verification to " . $user->email . " for cron user verification.", "ladara_emas_cron");

              ddlog("Start send email user verification to admin for cron user verification.", "ladara_emas_cron");
              
              $admin = new UsersBuilder($GLOBALS["gld_global"]["email_admin"]["name"], $GLOBALS["gld_global"]["email_admin"]["email"]);
              $email_admin = (new EmailHelper($admin))->adminVerifikasiAkunEmas($data_email);

              ddlog("End send email user verification to admin for cron user verification.", "ladara_emas_cron");

              ddlog("Start add web notification user verification for cron user verification.", "ladara_emas_cron");

              $notif_data = [
                "userId"        => $user->user_id,
                "title"         => "Selamat! Akun Ladara Emas kamu sudah terverifikasi",
                "descriptions"  => "Kamu dapat menggunakan fitur jual emas di Ladara Emas. Yuk cek rate emas terkini di Ladara Emas.",
                "type"          => "emas",
                "orderId"       => 0,
                "data"          => []
              ];

              Notifications::addNotification($notif_data);

              ddlog("End add web notification user verification for cron user verification.", "ladara_emas_cron");

            } else {
              
              ddlog("User email " . $user->email . " haven't verified.", "ladara_emas_cron");

            }

          }

        } else {

          ddlog("End get treasury profile with email " . $user->email . " for cron user verification. Failed to connect treasury api. \n" . htmlentities($output), "ladara_emas_cron");

        }

      } else {        

        ddlog("End login treasury with email " . $user->email . " for cron user verification. Error " . $login_trs->code. " (" . $login_trs->message. ").", "ladara_emas_cron");

      }

    }
    
    ddlog("End cron user verification.", "ladara_emas_cron");

  }
  add_action( "cronVerifikasiLadaraEmasAkun", "cronVerifikasiLadaraEmasAkun", 0 );
  
  /**
   * Cron for refresh balance
   *
   * @author Amy <laksmise@gmail.com>
   */
  function cronRefreshBalanceEmas() {
    global $wpdb;

    ddlog("Start cron refresh balance.", "ladara_emas_cron");
    
    $get_users = $wpdb->get_results(
      "SELECT id, user_id, email, ts_password FROM ldr_user_emas",
      OBJECT
    );

    foreach($get_users as $user) {

      ddlog("Start login treasury with email " . $user->email . " for cron refresh balance.", "ladara_emas_cron");

      //login ke treasury satu per satu ^^;
      $login_trs = gld_login(array("user_id"=>$user->user_id));
      $login_trs = json_decode($login_trs);

      if($login_trs->code === 200) {

        ddlog("End login treasury with email " . $user->email . " for cron refresh balance. Success.", "ladara_emas_cron");

        ddlog("Start get treasury profile with email " . $user->email . " for cron refresh balance.", "ladara_emas_cron");

        $authorization = "Authorization: Bearer " . $login_trs->data->access_token;
        $curl_header = array();
        $curl_header[] = $authorization;

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, getenv('TREASURY_BASE_URL') . "/profile");
        curl_setopt($ch, CURLOPT_TIMEOUT, $GLOBALS["gld_global"]["curl_timeout"]);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
        curl_setopt($ch, CURLOPT_HTTP_VERSION, CURL_HTTP_VERSION_1_1);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "GET");
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_ENCODING, "");
        curl_setopt($ch, CURLOPT_MAXREDIRS, $GLOBALS["gld_global"]["curl_maxredirs"]);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $curl_header);

        $output = curl_exec($ch);
        curl_getinfo($ch, CURLINFO_EFFECTIVE_URL);
        curl_getinfo($ch, CURLINFO_HTTP_CODE);
        curl_close($ch);
        $json_response = json_decode($output);

        if(isset($json_response->meta->code)) {

          if($json_response->meta->code !== 200) {

            ddlog("End get treasury profile with email " . $user->email . " for cron refresh balance. Error " . $json_response->data->code . " (" . $json_response->data->message . ").", "ladara_emas_cron");

          } else {

            ddlog("End get treasury profile with email " . $user->email . " for cron refresh balance. Success.", "ladara_emas_cron");

            ddlog("Start update balance for user " . $user->email . " for cron refresh balance.", "ladara_emas_cron");

            $wpdb->query("UPDATE ldr_user_emas SET 
              gold_balance=".$json_response->data->gold_balance.",
              gold_balance_in_currency=".$json_response->data->gold_balance_in_currency."
              WHERE id=" . $user->id);

            ddlog("End update balance for user " . $user->email . " for cron refresh balance.", "ladara_emas_cron");

          }

        } else {

          ddlog("End get treasury profile with email " . $user->email . " for cron refresh balance. Failed to connect treasury api. \n" . htmlentities($output), "ladara_emas_cron");

        }

      } else {

        ddlog("End login treasury with email " . $user->email . " for cron refresh balance. Error " . $login_trs->code . " (" . $login_trs->message . ").", "ladara_emas_cron");

      }

    }
    
    ddlog("End cron refresh balance.", "ladara_emas_cron");   
  }
  add_action( "cronRefreshBalanceEmas", "cronRefreshBalanceEmas", 0 );
  
  /**
   * Cron for retrieve payment
   *
   * @author Amy <laksmise@gmail.com>
   */
  function cronRetrievePaymentEmas() {
    global $wpdb;

    ddlog("Start cron retrieve payment.", "ladara_emas_cron");

    $start_date = date("Y-m-d")." 00-00-00";
    $end_date = date("Y-m-d")." 23-59-59";
    
    gld_check_payment_status(
      array(
        "start_date"  => $start_date,
        "end_date"    => $end_date,
      )
    );
    
    ddlog("End cron retrieve payment.", "ladara_emas_cron");   
  }
  add_action( "cronRetrievePaymentEmas", "cronRetrievePaymentEmas", 0 );
/******************************** CRON LADARA EMAS ********************************/


/******************************** FUNCTION WEBSITE ********************************/
	/**
	 * for debug, what else???
	 *
	 * @param array   $array
	 * @param boolean $is_exit
	 * @param integer $var_handling
	 * @return void
	 * @author Amy <laksmise@gmail.com>
	 */
  function ddbug($array, $is_exit=true, $var_handling=1) {
    echo "<pre>";
    if($var_handling === 1) {
      print_r($array);
    } else if($var_handling === 2) {
      var_dump($array);
    } else {
      echo "error! cek parameter var_handling ya!";
      exit;
    }
    echo "</pre>";
    if($is_exit === true) {
      exit;
    }
  }
  add_action( "ddbug", "ddbug", 0 );

	/**
	 * for log, what else???
   * dont forget to set WP_DEBUG_LOG on wp-config to true FIRST!!
   * 
	 * @param string  $arMsg
	 * @param string  $fileName
	 * @author Amy <laksmise@gmail.com>
	 */
  function ddlog($arMsg, $fileName="ladara_emas") {
    $stEntry = "";
    
    $arLogData['event_datetime'] = '['.date('Y-m-d h:i:s A').'] [client '.$_SERVER['REMOTE_ADDR'].']';

    $stEntry .= $arLogData['event_datetime']." ".$arMsg."\n";  

    if($fileName === "ladara_emas") {
      $stCurLogFileName = 'ladara_emas_'.date('Ymd').'.log';
    } else {
      $stCurLogFileName = $fileName.'_'.date('Ymd').'.log';
    }

    // if(WP_DEBUG_LOG === true) {
      $dirname = "log";

      if (!file_exists($dirname)) {
        mkdir($dirname, 0777);
      }

      $stCurLogFileName = $dirname . "/" . $stCurLogFileName;

      $fHandler = fopen($stCurLogFileName,'a+');
      fwrite($fHandler,$stEntry);
      fclose($fHandler);
    // }
  }
  add_action( "ddlog", "ddlog", 0 );

	/**
	 * Bulan dalam bahasa Indonesia
	 *
   * @param int $month
	 * @return string
	 * @author Amy <laksmise@gmail.com>
	 */
  function month_indonesia($month) {
    $bulan = array (
      1 => "Januari",
      2 => "Februari",
      3 => "Maret",
      4 => "April",
      5 => "Mei",
      6 => "Juni",
      7 => "Juli",
      8 => "Agustus",
      9 => "September",
      10 => "Oktober",
      11 => "November",
      12 => "Desember"
    );
    return $bulan[$month];
  }
  add_action( "month_indonesia", "month_indonesia", 0 );

	/**
	 * Hari dalam bahasa Indonesia
	 *
   * @param int $day
	 * @return string
	 * @author Amy <laksmise@gmail.com>
	 */
  function day_indonesia($day) {
    $hari = array (
      0 => "Minggu",
      1 => "Senin",
      2 => "Selasa",
      3 => "Rabu",
      4 => "Kamis",
      5 => "Jumat",
      6 => "Sabtu"
    );
    return $hari[$day];
  }
  add_action( "day_indonesia", "day_indonesia", 0 );

	/**
	 * unset session untuk testing
	 *
	 * @return void
	 * @author Amy <laksmise@gmail.com>
	 */
  function unset_session_emas() {
    unset($_SESSION["customer_token"]);
    unset($_SESSION["customer_token_activity"]);
    unset($_SESSION["calculate_value_from"]);
    unset($_SESSION["harga_beli_emas"]);
    exit;
  }
  add_action( "unset_session_emas", "unset_session_emas", 0 );

	/**
	 * global string, go ahead to add more
	 *
	 * @return void
	 * @author Amy <laksmise@gmail.com>
	 */
  function gldGlobal() {
    global $gld_global;
    $gld_global = [
      "curl_timeout"        => 0,
      "curl_connecttimeout" => 0,
      "curl_maxredirs"      => 10,
      "email_admin"         => [
        "name"  => "Ladara Emas",
        "email" => "ladaraemas@ladaraindonesia.com"
      ],
      "xendit"          => [
        "company_code"  => "88908",
        "merchant_name" => "88908 XENDIT"
      ]
    ];
  }
  add_action( "gldGlobal", "gldGlobal" );

	/**
	 * check format email dan check apakah user pakai email disposable atau tidak
	 *
   * @param string $email
	 * @return array
	 * @author Amy <laksmise@gmail.com>
	 */
  function gld_check_email_format($email) {
    /* disable provider email disposable/palsu */
    $disabled_domain = ["mailinator.com", "yopmail.com", "etlgr.com"]; //add more
    $domain = ltrim(stristr($email, "@"), "@");
    $user = stristr($email, "@", TRUE);

    if (in_array($domain, $disabled_domain)) {
      $return = [
        "code"    => 400,
        "status"  => "failed",
        "message" => "Email provider kamu tidak diijinkan. Silahkan gunakan email yang lain.",
        "data"    => []
      ];

      return $return;
    }

    if(filter_var($email, FILTER_VALIDATE_EMAIL) === false) {
      $return = [
        "code"    => 400,
        "status"  => "failed",
        "message" => "Format email kamu salah.",
        "data"    => []
      ];
      return $return;
    };

    if ( empty($user) && empty($domain) && checkdnsrr($domain) == false ) {
      $return = [
        "code"    => 400,
        "status"  => "failed",
        "message" => "Format email kamu salah.",
        "data"    => []
      ];

      return $return;
    }

    $return = [
      "code"    => 200,
      "status"  => "success",
      "message" => "Valid format email.",
      "data"    => []
    ];

    return $return;
  }
  
	/**
	 * Search treasury bank name from bank code
	 *
	 * @param string  $bank_code
	 * @param array   $banks
	 * @author Amy <laksmise@gmail.com>
	 */
  function gld_search_bank_name($bank_code, $banks) {
    foreach ($banks as $key => $val) {
      if ($val->code === $bank_code) {
        return $val->name;
      }
    }
    return null;
  }

  /**
   * Pagination
   * 
   * param reference example
   * $param = array(
   * 		'base'				=> 'http://localhost/ladara/',
   * 		'page'				=> $page,
   * 		'pages' 			=> $pages,
   * 		'key'					=> 'hal',
   * 		'next_text'		=> 'Next',
   * 		'prev_text'		=> 'Prev',
   * 		'first_text'	=> 'First',
   * 		'last_text'		=> 'Last'
   * );
   * 
   * param detail:
   * base				: (string - mandatory)	current page
   * page				: (integer - mandatory)	current page
   * pages			: (integer - mandatory)	total amount of pages
   * key				: (string - optional)		parameters name, default value 'hal'.
   * 																		please don't use 'page' or 'paged' it'll replaced by wordpress (yea a.n.n.o.y.i.n.g)
   * next_text	: (string - optional)		default value is 'Next'
   * prev_text	: (string - optional)		default value is 'Prev'
   * first_text	: (string - optional)		default value is 'First'
   * last_text	: (string - optional)		default value is 'Last'
   * show_dots  : (boolean - optional)  do you want to show dots (...) or not
   *
   * @param array $datas
   * @return void
   * @author Amy <laksmise@gmail.com>
   * 
   */
  function gld_pagination($datas) {
    $key = (isset($datas['key'])) ? $datas['key'] : 'hal';
    $next_text = (isset($datas['next_text'])) ? $datas['next_text'] : 'Next';
    $prev_text = (isset($datas['prev_text'])) ? $datas['prev_text'] : 'Prev';
    $first_text = (isset($datas['first_text'])) ? $datas['first_text'] : 'First';
    $last_text = (isset($datas['last_text'])) ? $datas['last_text'] : 'Last';

    $page = $datas['page'];
    $pages = $datas['pages'];
    $base = $datas['base'];

    $prevs = array();
    for ($p=1; $p<=4; $p++){
      $prevs[] = $page-$p;
    }sort($prevs);

    $nexts = array();
    for ($n=1; $n<=2; $n++){
      $nexts[] = $page+$n;
    }sort($nexts);

    $html = '';

    if($pages>5 && $page!=1) {
      $html .= '<a class="first" href="' . $base . $key . '=1">' . $first_text . '</a> ';
    }
    if($page!=1) {
      $html .= '<a class="prev" href="' . $base . $key . '=' . $prevs[3] . '">' . $prev_text . '</a> ';
    }

    if($pages>5) {
      if($page<5) {
        for ($i=1; $i<=5 ; $i++){
          $active = ($page==$i) ? 'class="active"':'';
          $html .= '<a href="' . $base . $key . '=' . $i . '" '. $active . '>'. $i . '</a> ';
        }
        if(isset($datas['show_dots']) && $datas['show_dots'] === true) {
          $html .= '<span>...</span> ';
        }
      } else {
        if(isset($datas['show_dots']) && $datas['show_dots'] === true) {
          if($page!=1 || $page==$pages) {
            $html .= '<span>...</span> ';
          }
        }
        if($page!=$pages) {
          unset($prevs[0]);
          unset($prevs[1]);
        }
        foreach($prevs as $prev) {
          if($page!=1 || $page==$pages) {
            $html .= '<a href="' . $base . $key . '=' . $prev . '">' . $prev . '</a> ';
          }
        }
        $html .= '<a href="' . $base . $key . '=' . $page . '" class="active">' . $page . '</a> ';
        if($page!=$pages) {
          foreach($nexts as $next) {
            if($next<=$pages) {
              $html .= '<a href="' . $base . $key . '=' . $next . '">' . $next . '</a> ';
            }
          }
        }
        if(isset($datas['show_dots']) && $datas['show_dots'] === true) {
          if($page!=$pages) {
          	$html .= '<span>...</span> ';
          }
        }
      }
    } else {
      for ($i=1; $i<=$pages ; $i++){
        $active = ($page==$i) ? 'class="active"':'';
        $html .= '<a href="' . $base . $key . '='. $i . '" '. $active . '>'. $i . '</a> ';
      }
    }

    if($page!=$pages && $nexts[0]<=$pages) {
      $html .= '<a class="next" href="' . $base . $key . '='. $nexts[0] . '">' . $next_text . '</a> ';
    }
    if($pages>5 && $page!=$pages && $nexts[0]<=$pages) {
      $html .= '<a class="last" href="' . $base . $key . '='. $pages . '">' . $last_text . '</a> ';
    }

    echo $html;
  }
  add_action( "gld_pagination", "gld_pagination", 0 );

  /**
   * Pagination sort link
   *
   * @param string  $menu
   * @param array   $params
   * @return string
   */
  function gld_pagination_sort_link($menu, $params) {
    $link = home_url()."/dashboard/".$menu."?";

    if(
      (isset($params["action"]) && isset($params["page"])) || 
      (isset($params["action"]) && !isset($params["page"]))
    ) {
      foreach($params as $key_par => $par) {
        $link .= $key_par."=".$par."&";
      }
    } elseif(!isset($params["action"]) && isset($params["page"])) {
      $link .= "page=".$params["page"]."&";
    }

    return $link;
  }
  add_action( "gld_pagination_sort_link", "gld_pagination_sort_link", 0 );

  /**
   * Pagination link
   *
   * @param string  $menu
   * @param array   $params
   * @return string
   */
  function gld_pagination_link($menu, $params) {
    $link = home_url()."/dashboard/".$menu."?";

    if(
      (isset($params["action"]) && isset($params["order"])) || 
      (isset($params["action"]) && !isset($params["order"]))
    ) {
      foreach($params as $key_par => $par) {
        $link .= $key_par."=".$par."&";
      }
    } elseif(!isset($params["action"]) && isset($params["order"])) {
      $link .= "order=".$params["order"]."&";
      $link .= "sort=".$params["sort"]."&";
    }

    return $link;
  }
  add_action( "gld_pagination_link", "gld_pagination_link", 0 );

  /**
   * Undocumented function
   *
   * @param [type] $total
   * @param [type] $current_page
   * @return void
   */
  function gld_pagination_prep($total, $current_page) {
    $limit = adm_gld_limit();
    $pages = ceil($total/$limit);
    $page = $current_page;

    $showing_from = 1;
    $showing_to = $limit;
    if($total < $limit) {
      $showing_to = $total;
    }
    if($current_page > 1) {
      $showing_from = $showing_to*($current_page-1)+1;
      $showing_to = $showing_to*$current_page;
    }
    //kalau udah halaman terakhir, showing_to = total
    if($pages == $current_page) {
      $showing_to = $total;
    }

    $rtn = [
      "pages"         => $pages,
      "page"          => $page,
      "showing_to"    => $showing_to,
      "showing_from"  => $showing_from
    ];

    return $rtn;
  }
  add_action( "gld_pagination_prep", "gld_pagination_prep", 0 );

	/**
	 * Refresh ladara emas session token
	 * 
	 * param example:
	 * $param = array(
	 * 		"user_id" => 1,
	 * );
	 * 
	 * param detail:
	 * user_id  : (integer - optional)  ladara web user id, get from get_current_user_id if param not sent
	 *
   * @param array $datas
	 * @author Amy <laksmise@gmail.com>
	 */
  function gld_refresh_session($datas) {
    if(!isset($datas["user_id"])) {
      $user_id = get_current_user_id();
    } else {
      $user_id = $datas["user_id"];
    }

    if (!isset($_SESSION["customer_token"]) && !isset($_SESSION["customer_token_activity"])) {
      gld_login(array("user_id"=>$user_id));
    } else {
      if (date("Y-m-d") > $_SESSION["customer_token_activity"]) {
        unset($_SESSION["customer_token"]);
        unset($_SESSION["customer_token_activity"]);
        gld_refresh_session(array("user_id"=>$user_id));
      }
    }
  }
  add_action( "gld_refresh_session", "gld_refresh_session", 0 );

	/**
	 * Check is current user activated ladara emas account or not
	 * 
	 * param example:
	 * $param = array(
	 * 		"user_id" => 1,
	 * );
	 * 
	 * param detail:
	 * user_id  : (integer - optional)  ladara web user id, get from get_current_user_id if param not sent
	 *
   * @param array $datas
	 * @return $return[]
	 * @author Amy <laksmise@gmail.com>
	 */
  function gld_check_user_emas($datas) {
    global $wpdb;

    if(!isset($datas["user_id"])) {
      $user_id = get_current_user_id();
    } else {
      $user_id = $datas["user_id"];
    }

    ddlog("Start check if " . $datas["user_id"] . " activated Ladara Emas account or not.");

    $query = "SELECT id, user_id, name, email, ts_password, trs_token, trs_token_expired 
    FROM ldr_user_emas WHERE user_id=" . $user_id . " ORDER BY id";
    $user_emas = $wpdb->get_row($query, OBJECT);

    if(empty($user_emas)) {
      ddlog("End check if " . $datas["user_id"] . " activated ladara emas account or not. User haven't activated Ladara Emas account.");

      $return = array(
        "code"    => 404,
        "status"  => "failed",
        "message" => "Kamu belum melakukan aktivasi Ladara Emas.",
        "data"    => []
      );

      return json_encode($return);
    }

    ddlog("End check if " . $datas["user_id"] . " activated ladara emas account or not. User activated Ladara Emas account.");

    $return = array(
      "code"    => 200,
      "status"  => "success",
      "message" => "Sukses.",
      "data"    => $user_emas
    );

    return json_encode($return);
  }
  add_action( "gld_check_user_emas", "gld_check_user_emas", 0 );

	/**
	 * Login treasury partner
	 * 
	 * @return $return[]
	 * @author Amy <laksmise@gmail.com>
	 */
  function gld_login_trs_partner() {
    global $wpdb;

    $current_date = date("Y-m-d");

    ddlog("Start login treasury partner.");

    //get from options
    $treasury_token = $wpdb->get_row("SELECT * FROM ldr_options WHERE option_name='treasury_token'", OBJECT);
    $treasury_token_expired = $wpdb->get_row("SELECT * FROM ldr_options WHERE option_name='treasury_token_expired'", OBJECT);
    if(!empty($treasury_token_expired)) {
      $treasury_token_expired = date("Y-m-d", strtotime($treasury_token_expired->option_value));
    } else {
      $treasury_token_expired = "0000-00-00";
    }

    if(empty($treasury_token) || ($treasury_token_expired != "0000-00-00" && $treasury_token_expired < $current_date)) {
      $body = [
        "client_id"			=> getenv('TREASURY_CLIENT_ID'),
        "client_secret"	=> getenv('TREASURY_CLIENT_SECRET'),
        "grant_type"		=> getenv('TREASURY_GRANT_CLIENT')
      ];

      $ch = curl_init();

      curl_setopt_array($ch, array(
        CURLOPT_URL => getenv('TREASURY_BASE_URL') . "/login",
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_ENCODING => "",
        CURLOPT_MAXREDIRS => $GLOBALS["gld_global"]["curl_maxredirs"],
        CURLOPT_TIMEOUT => $GLOBALS["gld_global"]["curl_timeout"],
        CURLOPT_FOLLOWLOCATION => true,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => "POST",
        CURLOPT_POSTFIELDS => $body,
        CURLOPT_HTTPHEADER => array(
          "Accept: application/json",
        ),
      ));
      
      $output = curl_exec($ch);
      curl_getinfo($ch, CURLINFO_EFFECTIVE_URL);
      curl_getinfo($ch, CURLINFO_HTTP_CODE);
      curl_close($ch);
      $json_response = json_decode($output);

      if(!isset($json_response->meta->code)) {
        ddlog("End login treasury partner. Failed to connect treasury api. \n" . htmlentities($output));

        $return = [
          "code"    => 500,
          "status"  => "failed",
          "message" => "Maaf terjadi gangguan untuk server login partner. Silahkan coba lagi beberapa saat lagi.",
          "data"    => []
        ];
        
        return json_encode($return);
      }

      if($json_response->meta->code !== 200) {
        ddlog("End login treasury partner. Error " . $json_response->meta->code . " (" . $json_response->meta->message . ").");

        $return = [
          "code"    => $json_response->meta->code,
          "status"  => "failed",
          "message" => "Maaf gagal login partner, " . $json_response->meta->message . " . Silahkan coba lagi beberapa saat lagi.",
          "data"    => []
        ];
        
        return json_encode($return);
      }

      ddlog("End login treasury partner. Success.");

      $trs_token = $json_response->data->access_token;
      $trs_token_expired = date('Y-m-d', strtotime($current_date. ' + 15 days'));
      // $trs_token_expired = date('Y-m-d', strtotime($json_response->data->expires_in);

      if(empty($treasury_token)) {
        $wpdb->query("INSERT INTO ldr_options (
          option_name, 
          option_value 
        ) VALUES (
          'treasury_token', 
          '" . $trs_token . "',
        )");
      } else {
        $wpdb->query("UPDATE ldr_options SET 
          option_value='" . $trs_token . "'
          WHERE option_name='treasury_token'");
      }

      if($treasury_token_expired == "0000-00-00" || empty($treasury_token_expired)) {
        $wpdb->query("INSERT INTO ldr_options (
          option_name, 
          option_value 
        ) VALUES (  
          'treasury_token_expired',
          '" . $trs_token_expired . "'
        )");
      }

      if(!empty($treasury_token_expired)) {
        $wpdb->query("UPDATE ldr_options SET 
          option_value='" . $trs_token_expired . "'
          WHERE option_name='treasury_token_expired'");
      }
    }

    $return = [
      "code"    => 200,
      "status"  => "success",
      "message" => "Sukses.",
      "data"    => [
        "expires_in"    => $trs_token_expired,
        "access_token"  => $trs_token
      ]
    ];

    return json_encode($return);
  }
  add_action( "gld_login_trs_partner", "gld_login_trs_partner", 0 );

	/**
	 * Login ladara emas user to treasury
	 * 
	 * param example:
	 * $param = array(
	 * 		"user_id" => 1,
	 * );
	 * 
	 * param detail:
	 * user_id  : (integer - optional)  ladara web user id, get from get_current_user_id if param not sent
	 *
   * @param array $datas
	 * @return $return[]
	 * @author Amy <laksmise@gmail.com>
	 */
  function gld_login($datas) {
    global $wpdb;

    $current_date = date("Y-m-d");

    if(!isset($datas["user_id"])) {
      $user_id = get_current_user_id();
    } else {
      $user_id = $datas["user_id"];
    }

    ddlog("Start check if " . $user_id . " activated their Ladara Emas account.");

    $user = gld_check_user_emas(array("user_id"=>$user_id));
    $user = json_decode($user);

    if($user->code !== 200) {
      ddlog("End check if " . $user_id . " activated their Ladara Emas account. Error " . $user->code . " (" . $user->message . ").");

      $wpdb->query("INSERT INTO ldr_user_emas_log (
          user_emas_id, 
          log 
        ) VALUES (
          0,  
          'User id ' . $user_id . ' failed to tried access dashboard Ladara Emas. Error " . $user->code . " (" . $user->message . ").' 
        )"); //0 berarti belum ada id user emas

      $return = array(
        "code"    => $user->code,
        "status"  => $user->status,
        "message" => $user->message,
        "data"    => $user->data
      );

      return json_encode($return);
    }

    ddlog("End check if " . $user_id . " activated their Ladara Emas account. Activated.");

    ddlog("Start if user is " . $user->data->email . " old user.");  

    $login_email = $user->data->email;
    $login_pass = $user->data->ts_password;  

    if(empty($user->data->ts_password)) {
      ddlog("End if user is " . $user->data->email . " old user. User is old user.");

      // get password
      $get_user_web = $wpdb->get_row(
        "SELECT ID, user_email, user_pass FROM ldr_users
          WHERE ID=" . $user_id . " AND user_email='".$user->data->email."'",
        OBJECT
      );

      ddlog("Start login treasury partner for update password process.");

      $login_partner = gld_login_trs_partner();
      $login_partner = json_decode($login_partner);
      
      if($login_partner->code !== 200) {
        ddlog("End login treasury partner for update password process. Failed.");

        $return = [
          "code"    => $login_partner->code,
          "status"  => $login_partner->status,
          "message" => $login_partner->message,
          "data"    => $login_partner->data
        ];

        return json_encode($return);
      }

      ddlog("End login treasury partner for update password process. Success.");

      ddlog("Start update password for user " . $user->data->email . ".");

      if(!empty($get_user_web) && !empty($get_user_web->user_pass)) {
        $body = [
          "email"                 => $user->data->email,
          "password"              => $get_user_web->user_pass,
          "password_confirmation" => $get_user_web->user_pass,
        ];

        $curl = curl_init();

        curl_setopt_array($curl, array(
          CURLOPT_URL => getenv('TREASURY_BASE_URL') . "/update-password",
          CURLOPT_RETURNTRANSFER => true,
          CURLOPT_ENCODING => "",
          CURLOPT_MAXREDIRS => $GLOBALS["gld_global"]["curl_maxredirs"],
          CURLOPT_TIMEOUT => $GLOBALS["gld_global"]["curl_timeout"],
          CURLOPT_FOLLOWLOCATION => true,
          CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
          CURLOPT_CUSTOMREQUEST => "POST",
          CURLOPT_POSTFIELDS => $body,
          CURLOPT_HTTPHEADER => array(
            "Authorization: Bearer " . $login_partner->data->access_token
          ),
        ));
        
        $output = curl_exec($curl);
        curl_getinfo($curl, CURLINFO_EFFECTIVE_URL);
        curl_getinfo($curl, CURLINFO_HTTP_CODE);
        curl_close($curl);
        $json_response = json_decode($output);

        if(!isset($json_response->meta->code)) {
          ddlog("End update password for user " . $user->data->email . ". Failed to connect treasury api. \n" . htmlentities($output));

          $wpdb->query("INSERT INTO ldr_user_emas_log (
              user_emas_id, 
              log 
            ) VALUES (
              " . $user->data->id . ",  
              'Failed to connect treasury api to update profile.' 
            )");

          $return = [
            "code"    => 500,
            "status"  => "failed",
            "message" => "Maaf terjadi gangguan untuk mengubah profile Ladara Emas kamu. Silahkan coba lagi beberapa saat lagi.",
            "data"    => []
          ];
          
          return json_encode($return);
        }

        if($json_response->meta->code !== 200) {
          ddlog("End update password for user " . $user->data->email . ". Error " . $json_response->meta->code . " (" . $json_response->meta->message . ").");

          $wpdb->query("INSERT INTO ldr_user_emas_log (
              user_emas_id, 
              log 
            ) VALUES (
              " . $user->data->id . ",  
              'Failed to update password Ladara Emas. Error " . $json_response->meta->code . " (" . $json_response->meta->message . ").' 
            )");

          $return = [
            "code"    => $json_response->meta->code,
            "status"  => "failed",
            "message" => "Maaf gagal mengubah password Ladara Emas kamu. Silahkan coba lagi beberapa saat lagi.",
            "data"    => []
          ];
          
          return json_encode($return);
        }
      }

      ddlog("End update password for user " . $user->data->email . ". Success.");

      // update password
      $wpdb->query("UPDATE ldr_user_emas SET 
        web_password='',
        ts_password='" . $get_user_web->user_pass . "'
        WHERE user_id=" . $user_id . " AND email='" . $user->data->email . "'");

      $login_pass = $get_user_web->user_pass;

      $wpdb->query("INSERT INTO ldr_user_emas_log (
          user_emas_id, 
          log 
        ) VALUES (
          " . $user->data->id . ",  
          'Success to update password Ladara Emas.' 
        )");
    }
    
    if(empty($user->data->trs_token) || (!empty($user->data->trs_token_expired) && $user->data->trs_token_expired < $current_date) ) {
      ddlog("End if user is " . $user->data->email . " old user. User is new user.");

      ddlog("Start login to treasury for user " . $user->data->email . ".");

      $body = [
        "client_id"			=> getenv('TREASURY_CLIENT_ID'),
        "client_secret" => getenv('TREASURY_CLIENT_SECRET'),
        "grant_type"    => getenv('TREASURY_GRANT_CUSTOMER'),
        "email"         => $login_email,
        "password"      => $login_pass
      ];

      $curl_header = array();
      $curl_header[] = "Accept: application/json";

      $ch = curl_init();

      curl_setopt_array($ch, array(
        CURLOPT_URL => getenv('TREASURY_BASE_URL') . "/login",
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_ENCODING => "",
        CURLOPT_MAXREDIRS => $GLOBALS["gld_global"]["curl_maxredirs"],
        CURLOPT_TIMEOUT => $GLOBALS["gld_global"]["curl_timeout"],
        CURLOPT_FOLLOWLOCATION => true,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => "POST",
        CURLOPT_POSTFIELDS => $body,
        CURLOPT_HTTPHEADER => array(
          "Accept: application/json",
        ),
      ));
      
      $output_login = curl_exec($ch);
      curl_getinfo($ch, CURLINFO_EFFECTIVE_URL);
      curl_getinfo($ch, CURLINFO_HTTP_CODE);
      curl_close($ch);
      $json_login = json_decode($output_login);

      if(!isset($json_login->meta->code)) {
        ddlog("End login to treasury for user " . $user->data->email . ". Failed to connect treasury api. \n" . htmlentities($output_login));

        $wpdb->query("INSERT INTO ldr_user_emas_log (
            user_emas_id, 
            log 
          ) VALUES (
            " . $user->data->id . ",  
            'Failed to connect treasury api to login Treasury.' 
          )");

        $return = [
          "code"    => 500,
          "status"  => "failed",
          "message" => "Maaf terjadi gangguan server. Silahkan coba lagi beberapa saat lagi.",
          "data"    => []
        ];
        
        return json_encode($return);
      }

      if($json_login->meta->code !== 200) {
        ddlog("End login to treasury for user " . $user->data->email . ". Error " . $json_login->meta->code . " (" . $json_login->meta->message . ").");

        $wpdb->query("INSERT INTO ldr_user_emas_log (
            user_emas_id, 
            log 
          ) VALUES (
            " . $user->data->id . ",  
            'Failed to login Ladara Emas. Error " . $json_login->meta->code . " (" . $json_login->meta->message . ").' 
          )");

        // $message = "Maaf gagal login ladara emas, " . $json_login->meta->message . ". Silahkan coba lagi beberapa saat lagi.";
        $message_failed_login = "Mohon maaf kami sedang mengalami kendala server. Silahkan coba beberapa saat lagi.";

        $return = [
          "code"    => $json_login->meta->code,
          "status"  => "failed",
          "message" => $message_failed_login,
          "data"    => []
        ];
        
        return json_encode($return);
      }

      $wpdb->query("INSERT INTO ldr_user_emas_log (
          user_emas_id, 
          log 
        ) VALUES (
          " . $user->data->id . ",  
          'Success to login Ladara Emas.' 
        )");

      ddlog("End login to treasury for user " . $user->data->email . ". Success.");

      $trs_token = $json_login->data->access_token;
      $trs_token_expired = date('Y-m-d', strtotime($current_date. ' + 15 days'));
      // $trs_token_expired = date('Y-m-d', strtotime($json_login->data->expires_in);

      $wpdb->query("UPDATE ldr_user_emas SET 
        trs_token='" . $trs_token . "',
        trs_token_expired='" . $trs_token_expired . "'
        WHERE user_id=" . $user_id . " AND email='" . $user->data->email . "'");
    } else {
      $trs_token = $user->data->trs_token;
      $trs_token_expired = $user->data->trs_token_expired;
    }

    $_SESSION["customer_token"] = $trs_token;
    $_SESSION["customer_token_activity"] = $trs_token_expired;

    $return = [
      "code"    => 200,
      "status"  => "success",
      "message" => "Sukses.",
      "data"    => [
        "expires_in"    => $trs_token_expired,
        "access_token"  => $trs_token
      ]
    ];
      
    return json_encode($return);
  }
  add_action( "gld_login", "gld_login", 0 );

	/**
	 * Get profile user_emas
	 * 
	 * param example:
	 * $param = array(
	 * 		"user_id" => 1,
	 * );
	 * 
	 * param detail:
	 * user_id  : (integer - optional)   user id ladara web, kalau kosong ambil dari get_current_user_id
	 *
   * @param array $datas
	 * @return $return[]
	 * @author Amy <laksmise@gmail.com>
	 */
  function gld_profile_emas($datas) {
    global $wpdb;

    if(!isset($datas["user_id"])) {
      $user_id = get_current_user_id();
    } else {
      $user_id = $datas["user_id"];
    }

    $user_emas = $wpdb->get_row(
      "SELECT ldr_user_emas.id, 
        ldr_user_emas.name,
        ldr_user_emas.email,
        ldr_user_emas.gender,
        ldr_user_emas.birthday,
        ldr_user_emas.phone,
        ldr_user_emas.account_name,
        ldr_user_emas.account_number,
        ldr_user_emas.bank_code,
        ldr_user_emas.bank_name,
        ldr_user_emas.branch,
        ldr_user_emas.gold_balance,
        ldr_user_emas.gold_balance_in_currency,
        ldr_user_emas.activated_user,
        ldr_user_emas.verified_user,
        ldr_user_emas.trs_user,
        ldr_users.ID 
      FROM ldr_user_emas 
      JOIN ldr_users ON ldr_user_emas.user_id = ldr_users.ID
      WHERE user_id=" . $user_id,
      OBJECT
    );

    if(empty($user_emas)) {
      $return = array(
        "code"    => 404,
        "status"  => "failed",
        "message" => "Maaf user tidak dapat ditemukan.",
        "data"    => []
      );

      return json_encode($return);
    }
    
    $return = array(
      "code"    => 200,
      "status"  => "success",
      "message" => "Sukses.",
      "data"    => [
        "id"                        => $user_emas->id,
        "name"                      => $user_emas->name,
        "email"                     => $user_emas->email,
        "gender"                    => $user_emas->gender,
        "birthday"                  => $user_emas->birthday,
        "phone"                     => $user_emas->phone,
        "account_name"              => $user_emas->account_name,
        "account_number"            => $user_emas->account_number,
        "bank_code"                 => $user_emas->bank_code,
        "bank_name"                 => $user_emas->bank_name,
        "branch"                    => $user_emas->branch,
        "gold_balance"              => $user_emas->gold_balance,
        "gold_balance_in_currency"  => $user_emas->gold_balance_in_currency,
        "activated_user"            => $user_emas->activated_user,
        "verified_user"             => $user_emas->verified_user,
        "trs_user"                  => $user_emas->trs_user,
        "id_web"                    => $user_emas->ID
      ]
    );

    return json_encode($return);
  }
  add_action( "gld_profile_emas", "gld_profile_emas", 0 );

	/**
	 * Get profile user
	 * 
	 * param example:
	 * $param = array(
	 * 		"user_id" => 1,
	 * );
	 * 
	 * param detail:
	 * user_id  : (integer - optional)   user id ladara web, kalau kosong ambil dari get_current_user_id
	 *
   * @param array $datas
	 * @return $return[]
	 * @author Amy <laksmise@gmail.com>
	 */
  function gld_user_profile($datas) {
    global $wpdb;

    if(!isset($datas["user_id"])) {
      $user_id = get_current_user_id();
    } else {
      $user_id = $datas["user_id"];
    }
    
    $get_user = $wpdb->get_row(
      "SELECT ldr_users.user_email,
          ldr_users.user_pass,
          ldr_user_profile.id,
          ldr_user_profile.user_id,
          ldr_user_profile.name,
          ldr_user_profile.email,
          ldr_user_profile.phone,
          ldr_user_profile.gender,
          ldr_user_profile.birthdate
        FROM ldr_users 
        LEFT JOIN ldr_user_profile ON ldr_users.ID = ldr_user_profile.user_id 
        WHERE ldr_users.ID=" . $user_id,
      OBJECT
    );

    if(empty($get_user)) {

      $return = array(
        "code"    => 404,
        "status"  => "failed",
        "message" => "Maaf user tidak dapat ditemukan.",
        "data"    => []
      );

    } else {

      $data_user = array(
        "id"        => $user_id,
        "name"      => "",
        "phone"     => "",
        "gender"    => "",
        "birthdate" => "",
        "email"     => $get_user->user_email,
        "pass"      => $get_user->user_pass
      );
      
      if(!empty($get_user->name)) {
        $data_user["name"] = $get_user->name;
      }
      
      if(!empty($get_user->phone)) {
        $data_user["phone"] = $get_user->phone;
      }
      
      if(!empty($get_user->email)) {
        $data_user["email"] = $get_user->email;
      }
      
      if(!empty($get_user->birthdate)) {
        $data_user["birthdate"] = date("Y-m-d", strtotime($get_user->birthdate));
      }
      
      if(!empty($get_user->gender)) {
        $data_user["gender"] = strtolower($get_user->gender);
      }

      if(!empty($data_user["phone"]) && !empty($data_user["birthdate"]) && !empty($data_user["gender"])) {

        $data_user["source"] = "user_profile";
        $return = array(
          "code"    => 200,
          "status"  => "success",
          "message" => "Sukses.",
          "data"    => $data_user
        );

      } else {

        $first_name = get_user_meta($user_id, "first_name", true);
        $last_name = get_user_meta($user_id, "last_name", true);
        $nama_lengkap = get_user_meta($user_id, "nama_lengkap", true);
        $tanggal_lahir = get_user_meta($user_id, "tanggal_lahir", true);
        $telepon = get_user_meta($user_id, "telepon", true);
        $jenis_kelamin = get_user_meta($user_id, "jenis_kelamin", true);

        if(!empty($nama_lengkap)) {
          $name = $nama_lengkap;
        } else {
          $name = $first_name . " " . $last_name;
        }

        if(!empty($name) && !empty($tanggal_lahir) && !empty($telepon) && !empty($jenis_kelamin)) {
          $data_user["name"] = $name;
          $data_user["phone"] = $telepon;
          $data_user["gender"] = strtolower($jenis_kelamin);

          $data_user["birthdate"] = date("Y-m-d", strtotime($tanggal_lahir));
          $data_user["source"] = "usermeta";

          $return = array(
            "code"    => 200,
            "status"  => "success",
            "message" => "Sukses.",
            "data"    => $data_user
          );

        } else {

          $return = array(
            "code"    => 404,
            "status"  => "failed",
            "message" => "Kamu belum melengkapi profile.",
            "data"    => []
          );

        }

      }

    }

    return json_encode($return);
  }
  add_action( "gld_user_profile", "gld_user_profile", 0 );

	/**
	 * Get profile treasury
	 * 
	 * param example:
	 * $param = array(
	 * 		"user_id" => 1,
	 * );
	 * 
	 * param detail:
	 * user_id  : (integer - optional)   user id ladara web, kalau kosong ambil dari get_current_user_id
	 *
   * @param array $datas
	 * @return $return[]
	 * @author Amy <laksmise@gmail.com>
	 */
  function gld_trs_profile($datas) {

    if(!isset($datas["user_id"])) {
      $user_id = get_current_user_id();
    } else {
      $user_id = $datas["user_id"];
    }

    ddlog("Start get treasury user profile id " . $user_id . ".");

    if(!isset($_SESSION["customer_token"])) {
      $login_trs = gld_login(array("user_id"=>$user_id));
      $login_trs = json_decode($login_trs);

      $customer_token = $login_trs->data->access_token;
    } else {
      $customer_token = $_SESSION["customer_token"];
    }

    $authorization = "Authorization: Bearer " . $customer_token;
    $curl_header = array();
    $curl_header[] = $authorization;

    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, getenv('TREASURY_BASE_URL') . "/profile");
    curl_setopt($ch, CURLOPT_TIMEOUT, $GLOBALS["gld_global"]["curl_timeout"]);
    curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
    curl_setopt($ch, CURLOPT_HTTP_VERSION, CURL_HTTP_VERSION_1_1);
    curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "GET");
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_ENCODING, "");
    curl_setopt($ch, CURLOPT_MAXREDIRS, $GLOBALS["gld_global"]["curl_maxredirs"]);
    curl_setopt($ch, CURLOPT_HTTPHEADER, $curl_header);

    $output = curl_exec($ch);
    curl_getinfo($ch, CURLINFO_EFFECTIVE_URL);
    curl_getinfo($ch, CURLINFO_HTTP_CODE);
    curl_close($ch);
    $json_response = json_decode($output);

    if(!isset($json_response->meta->code)) {
      ddlog("End get treasury user profile id " . $user_id . ". Failed to connect treasury api. \n" . htmlentities($output));

      $return = [
        "code"    => 500,
        "status"  => "failed",
        "message" => "Maaf terjadi gangguan untuk mendapatkan profile. Silahkan coba lagi beberapa saat lagi.",
        "data"    => []
      ];
      
      return json_encode($return);
    }

    if($json_response->meta->code !== 200) {
      ddlog("End get treasury user profile id " . $user_id . ". Error " . $json_response->meta->code . " (" . $json_response->meta->message . ").");

      $return = [
        "code"    => $json_response->meta->code,
        "status"  => "failed",
        "message" => "Maaf gagal mendapatkan profile. Silahkan coba lagi beberapa saat lagi.",
        "data"    => []
      ];
      
      return json_encode($return);
    }

    ddlog("End get treasury user profile id " . $user_id . ". Success.");

    $return = [
      "code"    => 200,
      "status"  => "success",
      "message" => "Sukses.",
      "data"    => $json_response->data
    ];

    return json_encode($return);
  }
  add_action( "gld_trs_profile", "gld_trs_profile", 0 );

	/**
	 * Handling user exist or old user in Ladara Emas,
   * but doesn't exist in treasury
	 * 
	 * param example:
	 * $param = array(
	 * 		"user_id" => 1,
	 * );
	 * 
	 * param detail:
	 * user_id  : (integer - optional)   user id ladara emas
	 *
   * @param array $datas
	 * @return void
	 * @author Amy <laksmise@gmail.com>
	 */
  function gld_user_exist_handling($datas) {
    global $wpdb;

    if(!isset($datas["user_id"])) {
      $user_id = get_current_user_id();
    } else {
      $user_id = $datas["user_id"];
    }

    // $wpdb->query("DELETE FROM ldr_user_emas WHERE user_id=" . $user_id . "");
  }
  add_action( "gld_user_exist_handling", "gld_user_exist_handling", 0 );
  
	/**
	 * Register treasury
	 * 
	 * param example:
	 * $param = array(
	 * 		"user_id"                   => 3,
   *    "name"                      => "Laksmi Setiawati",
	 * 		"email"                     => "laksmise@gmail.com",
   *    "ts_password"               => "Drife100%!",
   *    "gender"                    => "wanita",
   *    "birthday"                  => "1991-01-01",
   *    "referral_code"             => "LDRTRS01",
   *    "phone"                     => "08123456789",
   *    "security_question"         => "KQxz9YXazA14VEO",
   *    "security_question_answer"  => "Mickey Mouse",
   *    "selfie_scan"               => "",
   *    "id_card_scan"              => "",
   *    "account_name"              => "Laksmi Setiawati",
   *    "account_number"            => "1234567",
   *    "bank_code"                 => "BCA",
   *    "branch"                    => "Jakarta"
	 * );
	 * 
	 * param detail:
	 * user_id                  : (integer - mandatory)   ladara web user id, get from get_current_user_id if param not sent
	 * name                     : (string - mandatory)    ladara web current login user name
	 * email                    : (string - mandatory)    ladara web current login user email
	 * ts_password              : (string - mandatory)    ladara web current login user password, yes it have same value
	 * gender                   : (string - mandatory)    ladara web current login user gender, value are wanita or male
	 * birthday                 : (date - mandatory)      ladara web current login user birthdate in YYYY-MM-DD format
	 * referral_code            : (string - mandatory)    treasury referral code
	 * phone                    : (string - mandatory)    ladara web current login user phone
	 * security_question        : (integer - mandatory)   treasury security question id
	 * security_question_answer : (string - mandatory)    by default Mickey Mouse
	 * selfie_scan              : (string - optional)     base64
	 * id_card_scan             : (string - optional)     base64
	 * account_name             : (string - optional)     user bank owner name, it must have same vale with param name
	 * account_number           : (string - optional)     user bank number
	 * bank_code                : (string - optional)     bank code from treasury bank list
	 * branch                   : (string - optional)     bank branch
	 *
   * @param array $datas
	 * @return $return[]
	 * @author Amy <laksmise@gmail.com>
	 */
  function gld_register($datas) {
    global $wpdb;

    if(!isset($datas["user_id"])) {
      $user_id = get_current_user_id();
    } else {
      $user_id = $datas["user_id"];
    }

    ddlog("Start register user id " . $user_id . " to treasury.");

    $get_user = gld_user_profile(array("user_id"=>$user_id));
    $get_user = json_decode($get_user);

    if($get_user->code !== 200) {
      $return = array(
        "code"    => $get_user->code,
        "status"  => $get_user->status,
        "message" => $get_user->message,
        "data"    => $get_user->data
      );

      return json_encode($return);
    }

    // $check_email = gld_check_email_format($datas["email"]);
    // if($check_email["code"] !== 200) {
    //   $return = array(
    //     "code"    => $check_email["code"],
    //     "status"  => "failed",
    //     "message" => $check_email["message"],
    //     "data"    => []
    //   );

    //   return json_encode($return);
    // }

    //mandatory
    $data_user = array(
      "user_id"                   => $user_id,
      "name"                      => $datas["name"],
      "email"                     => $datas["email"],
      "ts_password"               => $datas["ts_password"],
      // "created_date"              => date("Y-m-d H:i:s"),
      "gender"                    => $datas["gender"],
      "birthday"                  => $datas["birthday"],
      "referral_code"             => $datas["referral_code"],
      "phone"                     => $datas["phone"],
      "security_question"         => $datas["security_question"],
      "security_question_answer"  => $datas["security_question_answer"],
      "verified_user"             => 0,
      "gold_balance"              => 0,
      "gold_balance_in_currency"  => 0,
      "selfie_scan"               => "",
      "id_card_scan"              => "",
      "account_name"              => "",
      "account_number"            => "",
      "bank_code"                 => "",
      "branch"                    => ""
    );

    //optional
    if(isset($datas["selfie_scan"])) {
      $data_user["selfie_scan"] = $datas["selfie_scan"];
    }

    if(isset($datas["id_card_scan"])) {
      $data_user["id_card_scan"] = $datas["id_card_scan"];
    }

    if(isset($datas["account_name"])) {
      $data_user["account_name"] = $datas["account_name"];
    }

    if(isset($datas["account_number"])) {
      $data_user["account_number"] = $datas["account_number"];
    }

    if(isset($datas["bank_code"])) {
      $data_user["bank_code"] = $datas["bank_code"];
    }

    if(isset($datas["branch"])) {
      $data_user["branch"] = $datas["branch"];
    }

    $bank_name = "";
    if(!empty($datas["bank_code"])) {
      $gld_bank = gld_bank(array("type"=>"trs"));
      $gld_bank = json_decode($gld_bank);
      $bank_name = gld_search_bank_name($datas["bank_code"], $gld_bank->data);
    }

    ddlog("Start login treasury partner to register new user for user id " . $user_id . ".");

    $login_partner = gld_login_trs_partner();
    $login_partner = json_decode($login_partner);

    if($login_partner->code !== 200) {
      ddlog("End login treasury partner to register new user for user id " . $user_id . ". Error " . $login_partner->code . " (" . $login_partner->message . ").");

      $return = [
        "code"    => $login_partner->code,
        "status"  => $login_partner->status,
        "message" => $login_partner->message,
        "data"    => $login_partner->data
      ];

      return json_encode($return);
    }

    ddlog("End login treasury partner to register new user for user id " . $user_id . ". Success.");

    $wpdb->query("INSERT INTO ldr_user_emas (
        user_id, 
        name, 
        email, 
        web_password, 
        ts_password, 
        gender, 
        birthday, 
        referral_code, 
        phone, 
        security_question, 
        security_question_answer, 
        selfie_scan, 
        id_card_scan, 
        account_name, 
        account_number, 
        bank_code,  
        bank_name, 
        branch,
        verified_user, 
        gold_balance, 
        gold_balance_in_currency,
        activated_user 
      ) VALUES (
        " . $data_user["user_id"] . ",  
        '" . $data_user["name"] . "',  
        '" . $data_user["email"] . "',  
        '',  
        '" . $data_user["ts_password"] . "',  
        '" . $data_user["gender"] . "', 
        '" . $data_user["birthday"] . "',  
        '" . $data_user["referral_code"] . "',  
        '" . $data_user["phone"] . "',  
        '" . $data_user["security_question"] . "',  
        '" . $data_user["security_question_answer"] . "',  
        '" . $data_user["selfie_scan"] . "',  
        '" . $data_user["id_card_scan"] . "',  
        '" . $data_user["account_name"] . "',  
        '" . $data_user["account_number"] . "',  
        '" . $data_user["bank_code"] . "',   
        '" . $bank_name . "',  
        '" . $data_user["branch"] . "',
        " . $data_user["verified_user"] . ",  
        " . $data_user["gold_balance"] . ",  
        " . $data_user["gold_balance_in_currency"] . ",
        0
      )");

    if($data_user["gender"] === "wanita") {
      $trs_gender = "Female";
    } else {
      $trs_gender = "Male";
    }

    $body = array(
      "name"                      => $data_user["name"],
      "email"                     => $data_user["email"],
      "password"		              => $data_user["ts_password"],
      "password_confirmation"     => $data_user["ts_password"],
      "gender"                    => $trs_gender,
      "birthday"                  => $data_user["birthday"],
      "referral_code"             => $data_user['referral_code'],
      "phone"                     => $data_user['phone'],
      "security_question"         => $data_user['security_question'],
      "security_question_answer"  => $data_user['security_question_answer'],
      "owner_name"                => $data_user['account_name'],
      "account_number"            => $data_user['account_number'],
      "bank_code"                 => $data_user['bank_code'],
      "branch"                    => $data_user['branch'],
      "selfie_scan"               => $data_user['selfie_scan'],
      "id_card_scan"              => $data_user['id_card_scan'],
    );

    $curl = curl_init();
    
    curl_setopt_array($curl, array(
      CURLOPT_URL => getenv('TREASURY_BASE_URL') . "/register",
      CURLOPT_RETURNTRANSFER => true,
      CURLOPT_ENCODING => "",
      CURLOPT_MAXREDIRS => $GLOBALS["gld_global"]["curl_maxredirs"],
      CURLOPT_TIMEOUT => $GLOBALS["gld_global"]["curl_timeout"],
      CURLOPT_FOLLOWLOCATION => true,
      CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
      CURLOPT_CUSTOMREQUEST => "POST",
      CURLOPT_POSTFIELDS => $body,
      CURLOPT_HTTPHEADER => array(
        "Authorization: Bearer " . $login_partner->data->access_token
      ),
    ));
    
    $output = curl_exec($curl);
    curl_getinfo($curl, CURLINFO_EFFECTIVE_URL);
    curl_getinfo($curl, CURLINFO_HTTP_CODE);
    curl_close($curl);
    $json_response = json_decode($output);

    if(!isset($json_response->meta->code)) {
      ddlog("End register user id " . $user_id . " to treasury. Failed to connect treasury api. \n" . htmlentities($output));

      $wpdb->query("INSERT INTO ldr_user_emas_log (
          user_emas_id, 
          log 
        ) VALUES (
          0,  
          'User " . $data_user["name"] . " (" . $data_user["email"] . ") tried to activate Ladara Emas account. Failed to connect treasury register api.' 
        )"); //0 berarti belum ada id user emas

      $return = [
        "code"    => 500,
        "status"  => "failed",
        "message" => "Maaf terjadi gangguan untuk aktivasi Ladara Emas. Silahkan hubungi tim kami.",
        "data"    => []
      ];
      
      return json_encode($return);
    }

    if($json_response->meta->code !== 200) {
      ddlog("End register user id " . $user_id . " to treasury. Error " . $json_response->meta->code . " (" . $json_response->meta->message . ").");

      $wpdb->query("INSERT INTO ldr_user_emas_log (
          user_emas_id, 
          log 
        ) VALUES (
          0,  
          'User " . $data_user["name"] . " (" . $data_user["email"] . ") tried to activate Ladara Emas account. Error " . $json_response->meta->code . " (" . $json_response->meta->message . ").' 
        )"); //0 berarti belum ada id user emas

      $return = [
        "code"    => $json_response->meta->code,
        "status"  => "failed",
        "message" => "Maaf kamu gagal melakukan aktivasi Ladara Emas, " . $json_response->meta->message . " Silahkan hubungi tim kami.",
        "data"    => []
      ];
      
      return json_encode($return);
    }

    $new_user_emas = $wpdb->get_row(
      "SELECT id, user_id, name, email
        FROM ldr_user_emas 
        WHERE user_id=" . $data_user["user_id"] . " 
          AND email='" . $data_user["name"] . "' 
        ORDER BY id DESC LIMIT 1",
      OBJECT
    );

    ddlog("New user emas : " . json_encode($new_user_emas) . ".");

    $wpdb->query("UPDATE ldr_user_emas SET 
        activated_user=1 
      WHERE id=" . $new_user_emas->id);

    $wpdb->query("INSERT INTO ldr_user_emas_log (
        user_emas_id, 
        log 
      ) VALUES (
        " . $new_user_emas->id . ",  
        'User " . $new_user_emas->name . " (" . $new_user_emas->email . ") activate Ladara Emas account.' 
      )");
    
    gld_login(array("user_id"=>$user_id));

    $data_email = [
      "customer_name"   => $data_user["name"],
      "customer_email"  => $data_user["email"]
    ];

    $customer = new UsersBuilder($data_user["name"], $data_user["email"]);
    $email_customer = (new EmailHelper($customer))->aktivasiAkunEmas($data_email);
    
    $admin = new UsersBuilder($GLOBALS["gld_global"]["email_admin"]["name"], $GLOBALS["gld_global"]["email_admin"]["email"]);
    $email_admin = (new EmailHelper($admin))->adminAktivasiAkunEmas($data_email);

    ddlog("Start add web notification user " . $data_user["email"] . " register to treasury.");

    $notif_data = [
      "userId"        => $user_id,
      "title"         => "Selamat datang di Ladara Emas!",
      "descriptions"  => "Kamu telah melakukan aktivasi akun Ladara Emas. Kamu sudah bisa menabung emas di Ladara Emas.",
      "type"          => "emas",
      "orderId"       => 0,
      "data"          => []
    ];

    Notifications::addNotification($notif_data);

    ddlog("Start add web notification user " . $data_user["email"] . " register to treasury.");

    ddlog("End register user id " . $user_id . " to treasury. Success.");

    $return = [
      "code"    => 200,
      "status"  => "success",
      "message" => "Sukses.",
      "data"    => []
    ];
      
    return json_encode($return);
  }
  add_action( "gld_register", "gld_register", 0 );

	/**
	 * Get list security question dari treasury
   * 
	 * @return $return[]
	 * @author Amy <laksmise@gmail.com>
	 */
  function gld_security_question() {
    ddlog("Start login treasury partner to get treasury security question.");

    $login_partner = gld_login_trs_partner();
    $login_partner = json_decode($login_partner);

    if($login_partner->code !== 200) {
      ddlog("End login treasury partner to get treasury security question. Failed.");

      $return = [
        "code"    => $login_partner->code,
        "status"  => $login_partner->status,
        "message" => $login_partner->message,
        "data"    => $login_partner->data
      ];

      return json_encode($return);
    }

    ddlog("End login treasury partner to get treasury security question. Success.");

    ddlog("Start get treasury security question.");

    $authorization = "Authorization: Bearer " . $login_partner->data->access_token;
    $curl_header = array();
    $curl_header[] = $authorization;

    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, getenv('TREASURY_BASE_URL') . "/security-question");
    curl_setopt($ch, CURLOPT_TIMEOUT, $GLOBALS["gld_global"]["curl_timeout"]);
    curl_setopt($ch, CURLOPT_HTTPHEADER, $curl_header);
    curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
    curl_setopt($ch, CURLOPT_HTTP_VERSION, CURL_HTTP_VERSION_1_1);
    curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "GET");
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_ENCODING, "");
    curl_setopt($ch, CURLOPT_MAXREDIRS, $GLOBALS["gld_global"]["curl_maxredirs"]);
    
    $output = curl_exec($ch);
    curl_getinfo($ch, CURLINFO_EFFECTIVE_URL);
    curl_getinfo($ch, CURLINFO_HTTP_CODE);
    curl_close($ch);
    $json_response = json_decode($output);

    if(!isset($json_response->meta->code)) {
      ddlog("End get treasury security question. Failed to connect treasury api. \n" . htmlentities($output));

      $return = [
        "code"    => 500,
        "status"  => "failed",
        "message" => "Maaf terjadi gangguan untuk mendapatkan security question. Silahkan coba lagi beberapa saat lagi.",
        "data"    => []
      ];
      
      return json_encode($return);
    }

    if($json_response->meta->code !== 200) {
      ddlog("End get treasury security question. Error " . $json_response->meta->code . " (" . $json_response->meta->message . ").");

      $return = [
        "code"    => $json_response->meta->code,
        "status"  => "failed",
        "message" => "Maaf gagal mendapatkan security question. Silahkan coba lagi beberapa saat lagi.",
        "data"    => []
      ];
      
      return json_encode($return);
    }

    ddlog("End get treasury security question. Success.");

    $return = [
      "code"    => 200,
      "status"  => "success",
      "message" => "Sukses.",
      "data"    => $json_response->data
    ];

    return json_encode($return);
  }
  add_action( "gld_security_question", "gld_security_question", 0 );

	/**
	 * Harga emas
	 * 
	 * param example:
	 * $param = array(
	 * 		"user_id" => 1,
	 * );
	 * 
	 * param detail:
	 * user_id  : (integer - optional)  ladara web user id, get from get_current_user_id if param not sent
   * 
	 * @return $return[]
	 * @author Amy <laksmise@gmail.com>
	 */
  function gld_rate($datas) {
    if(!isset($datas["user_id"])) {
      $user_id = get_current_user_id();
    } else {
      $user_id = $datas["user_id"];
    }

    if(!isset($_SESSION["customer_token"])) {
      $login_trs = gld_login(array("user_id"=>$user_id));
      $login_trs = json_decode($login_trs);

      $customer_token = $login_trs->data->access_token;
    } else {
      $customer_token = $_SESSION["customer_token"];
    }

    // kadang g muncul mulu rate nya, g tiap hari ganti
    $current_date = date("Y-m-d H:i:s");
    $start_date =date('Y-m-d H:i:s', strtotime('-1 day'));
    $end_date = $current_date;

    $authorization = "Authorization: Bearer " . $customer_token;
    $curl_header = array();
    $curl_header[] = $authorization;
    
    $post = [
      "start_date"  => $start_date,
      "end_date"    => $end_date,
    ];

    ddlog("Start get rate emas start from " . $start_date . " to " . $end_date . ".");

    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, getenv('TREASURY_BASE_URL') . "/gold-price");
    curl_setopt($ch, CURLOPT_TIMEOUT, $GLOBALS["gld_global"]["curl_timeout"]);
    curl_setopt($ch, CURLOPT_HTTPHEADER, $curl_header);
    curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
    curl_setopt($ch, CURLOPT_HTTP_VERSION, CURL_HTTP_VERSION_1_1);
    curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_ENCODING, "");
    curl_setopt($ch, CURLOPT_MAXREDIRS, $GLOBALS["gld_global"]["curl_maxredirs"]);
    curl_setopt($ch, CURLOPT_POSTFIELDS, $post);
    
    $output = curl_exec($ch);
    curl_getinfo($ch, CURLINFO_EFFECTIVE_URL);
    curl_getinfo($ch, CURLINFO_HTTP_CODE);
    curl_close($ch);
    $json_response = json_decode($output);

    if(!isset($json_response->meta->code)) {
      ddlog("End get rate emas start from " . $start_date . " to " . $end_date . ". Failed to connect treasury api. \n" . htmlentities($output));

      $return = [
        "code"    => 500,
        "status"  => "failed",
        "message" => "Maaf terjadi gangguan untuk mendapatkan rate emas. Silahkan coba lagi beberapa saat lagi.",
        "data"    => []
      ];
      
      return json_encode($return);
    }

    if($json_response->meta->code !== 200) {
      ddlog("End get rate emas start from " . $start_date . " to " . $end_date . ". Error " . $json_response->meta->code . " (" . $json_response->meta->message . ").");

      $return = [
        "code"    => $json_response->meta->code,
        "status"  => "failed",
        "message" => "Maaf gagal mendapatkan rate emas. Silahkan coba lagi beberapa saat lagi.",
        "data"    => []
      ];
      
      return json_encode($return);
    }

    ddlog("End get rate emas start from " . $start_date . " to " . $end_date . ". Success.");

    $return = array(
      "code"    => 200,
      "status"  => "success",
      "message" => "Sukses.",
      "data"    => $json_response->data[0]
    );

    return json_encode($return);
  }
  add_action( "gld_rate", "gld_rate", 0 );

	/**
	 * Calculate emas dari gram value ke money value atau money value ke gram value
   * dengan validasi berdasarkan SnK treasury, user harus pernah berbelanja min 0.5 gram dari total untuk calculate jual emas
	 * 
	 * param example:
	 * $param = array(
	 * 		"amount_type"       => "gold",
   *    "amount"            => 1,
	 * 		"transaction_type"  => "buy",
	 * 		"payment_type"      => "gross",
   *    "user_id"           => 1
	 * );
	 * 
	 * param detail:
   * amount_type      : (string - mandatory)    source of your value
   *                                            the option are "gold" for gold value and "currency" for currency (in rupiah) value
   * amount           : (integer - mandatory)   for example 1 when use "gold" and 200000 when use "currency"
	 * transaction_type : (string - mandatory)		type rate, "buy" for buying rate and "sell" for selling rate
	 * payment_type     : (string - optional)     by default we use "gross" because it used for treasury's partner
   * user_id          : (integer - optional)    user id ladara web, kalau kosong ambil dari get_current_user_id
	 *
	 * @return 
	 * @author Amy <laksmise@gmail.com>
	 */
  function gld_calculate($datas) {
    global $wpdb;

    ddlog("Start calculate emas.");

    if(!isset($datas["user_id"])) {
      $user_id = get_current_user_id();
    } else {
      $user_id = $datas["user_id"];
    }
    
    $profile = gld_profile_emas(array("user_id"=>$user_id));
    $profile = json_decode($profile);

    $user_emas_id = $profile->data->id;

    $body_data = array(
      "amount_type"       => $datas["amount_type"],
      "amount"            => $datas["amount"],
      "transaction_type"  => "buy",
      "payment_type"      => "gross"
    );
    
    if(isset($datas["transaction_type"])) {
      $body_data["transaction_type"] = $datas["transaction_type"];
    }

    if($body_data["transaction_type"] === "sell") {

      $count_emas_query = "SELECT SUM(total_gold) as sum_total_gold FROM ldr_transaksi_emas WHERE user_id = " . $user_emas_id;
      $count_emas = $wpdb->get_row($count_emas_query, OBJECT);

      $total_gold_balance = str_replace(".", ",", round($count_emas->sum_total_gold, 1, PHP_ROUND_HALF_UP));
      $gold_balance = str_replace(".", ",", round($profile->data->gold_balance, 1, PHP_ROUND_HALF_UP));

      ddlog("gold_balance = $gold_balance");
      ddlog("total_gold_balance = $total_gold_balance");

      if($profile->data->gold_balance == 0) {
        ddlog("End calculate emas. User " . $profile->data->email . " don't have emas.");

        $return = array(
          "code"    => 400,
          "status"  => "failed",
          "message" => "Kamu belum memiliki emas.",
          "data"    => [
            "buy_price"   => 0,
            "sell_price"  => 0,
            "currency"    => 0,
            "unit"        => 0,
            "total"       => 0
          ]
        );
        return json_encode($return);
      }

      if($body_data["amount_type"] === "gold") {
        $amount = str_replace(",", ".", $datas["amount"]);
        if($profile->data->gold_balance < $amount) {
          ddlog("End calculate emas. User " . $profile->data->email . " emas balance not enough to do sell emas. User have " . $profile->data->gold_balance . " gram emas, and want to sell " . $datas["amount"] . " gram.");

          $return = array(
            "code"    => 400,
            "status"  => "failed",
            "message" => "Maaf saldo kamu tidak cukup.",
            "data"    => [
              "buy_price"   => 0,
              "sell_price"  => 0,
              "currency"    => 0,
              "unit"        => 0,
              "total"       => 0
            ]
          );

          return json_encode($return);
        }
      }

      if($body_data["amount_type"] === "currency") {
        $amount = str_replace(".", "", $datas["amount"]);
        if($profile->data->gold_balance_in_currency < $amount) {
          ddlog("End calculate emas. User " . $profile->data->email . " emas currency balance not enough to do sell emas. User have " . $profile->data->gold_balance_in_currency . ", and want to sell " . $datas["amount"] . ".");

          $return = array(
            "code"    => 400,
            "status"  => "failed",
            "message" => "Maaf saldo kamu tidak cukup.",
            "data"    => [
              "buy_price"   => 0,
              "sell_price"  => 0,
              "currency"    => 0,
              "unit"        => 0,
              "total"       => 0
            ]
          );

          return json_encode($return);
        }
      }

    }

    if(!isset($_SESSION["customer_token"])) {
      $login_trs = gld_login(array("user_id"=>$user_id));
      $login_trs = json_decode($login_trs);

      $customer_token = $login_trs->data->access_token;
    } else {
      $customer_token = $_SESSION["customer_token"];
    }

    if($datas["amount_type"] == 'currency') {
      $amount = str_replace(".", "", $datas["amount"]);
    } else if($datas["amount_type"] == 'gold') {
      $amount = str_replace(",",".",$datas["amount"]);
    }

    $body = array(
      "amount_type"       => $datas["amount_type"],
      "amount"            => $amount,
      "transaction_type"  => $datas["transaction_type"],
      "payment_type"      => "gross",
    );

    $authorization = "Authorization: Bearer " . $customer_token;
    $curl_header = array();
    $curl_header[] = $authorization;

    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, getenv('TREASURY_BASE_URL') . "/calculate");
    curl_setopt($ch, CURLOPT_TIMEOUT, $GLOBALS["gld_global"]["curl_timeout"]);
    curl_setopt($ch, CURLOPT_HTTPHEADER, $curl_header);
    curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
    curl_setopt($ch, CURLOPT_HTTP_VERSION, CURL_HTTP_VERSION_1_1);
    curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_ENCODING, "");
    curl_setopt($ch, CURLOPT_MAXREDIRS, $GLOBALS["gld_global"]["curl_maxredirs"]);
    curl_setopt($ch, CURLOPT_POSTFIELDS, $body);
    
    $output = curl_exec($ch);
    curl_getinfo($ch, CURLINFO_EFFECTIVE_URL);
    curl_getinfo($ch, CURLINFO_HTTP_CODE);
    curl_close($ch);
    $json_response = json_decode($output);

    if(!isset($json_response->meta->code)) {
      ddlog("End calculate emas. Failed to connect treasury api. \n" . htmlentities($output));

      $return = [
        "code"    => 500,
        "status"  => "failed",
        "message" => "Maaf terjadi gangguan dalam melakukan penghitungan emas. Silahkan coba lagi beberapa saat lagi.",
        "data"    => []
      ];
      
      return $return;
    }

    if($json_response->meta->code !== 200) {
      ddlog("End calculate emas. Error " . $json_response->meta->code . " (" . $json_response->meta->message . ").");

      $return = [
        "code"    => $json_response->meta->code,
        "status"  => "failed",
        "message" => "Maaf gagal melakukan penghitungan emas. Silahkan coba lagi beberapa saat lagi.",
        "data"    => []
      ];
      
      return $return;
    }
    
    ddlog("End calculate emas. Success.");

    if($body["transaction_type"] === "sell") {
      $json_response->data->currency_value = 0;
    }

    $return = array(
      "code"    => 200,
      "status"  => "success",
      "message" => "Sukses.",
      "data"    => $json_response->data
    );

    return json_encode($return);
  }
  add_action( "gld_calculate", "gld_calculate", 0 );

	/**
	 * Beli emas,
   * calculate terlebih dahulu untuk memastikan kalau data yang dikirim sudah sesuai dengan hasil calculate treasury
	 * 
	 * param example:
	 * $param = array( 
   *    "amount"            => 0.0918,
   *    "amount_type"       => "gold",
   *    "bank"              => "MANDIRI",
   *    "bank_trs"          => "BMRI",
   *    "type_payment"      => "va",
   *    "latitude"          => "-6.3504384",
   *    "longitude"         => "106.8924928"
	 * );
	 * 
	 * param detail:
	 * amount           : (float - mandatory)   for example 0.0918 when use "gold" and 200000 when use "currency"
	 * amount_type      : (string - mandatory)  source of your value,
   *                                          the option are "gold" for gold value and "currency" for currency (in rupiah) value
	 * type_payment     : (string - mandatory)  payment type va for virtual account, cc for credit card, 
   *                                          ewallet for instan payment (ovo, dana and linkaja) and retail for offline payment (indomaret and alfamart)
	 * ewallet_phone    : (string - optional)   phone number that registered in instant payment
   * retail_type      : (string - optional)   offline payment type for payment type retail. option are INDOMARET and ALFAMART
	 * bank             : (string - optional)   Xendit bank code for va payment. value are MANDIRI, BNI, BCA, BRI and PERMATA
	 * bank_trs         : (string - optional)   Treasury bank code. default BNI
   *                                          value are BMRI for MANDIRI, BNI for BNI, BCA for BCA,
   *                                            BRIN for BRI, and BBBA for PERMATA
	 * latitude         : (string - optional)   latitude map
	 * longitude        : (string - optional)   longitude map
	 *
	 * @return 
	 * @author Amy <laksmise@gmail.com>
	 */
  function gld_buy($datas) {
    global $wpdb;

    if(!isset($datas["user_id"])) {
      $user_id = get_current_user_id();
    } else {
      $user_id = $datas["user_id"];
    }
    
    ddlog("Start calculate buy emas for user id " . $user_id . ".");

    $calculate_datas = array(
      "amount_type"       => $datas["amount_type"],
      "amount"            => $datas["amount"],
      "transaction_type"  => "buy",
      "payment_type"      => "gross",
      "user_id"           => $user_id
    );

    $calculate_beli_emas = gld_calculate($calculate_datas);
    $calculate_beli_emas = json_decode($calculate_beli_emas);

    if($calculate_beli_emas->code !== 200) {
      ddlog("End calculate buy emas for user id " . $user_id . ". Error " . $calculate_beli_emas->code . "(" . $calculate_beli_emas->message . ".");

      $return = array(
        "code"    => $calculate_beli_emas->code,
        "status"  => $calculate_beli_emas->status,
        "message" => $calculate_beli_emas->message,
        "data"    => $calculate_beli_emas->data
      );

      return json_encode($return);
    }
    
    ddlog("End calculate buy emas for user id " . $user_id . ". Success.");

    ddlog("Start select user emas for user id " . $user_id . ".");
    
    $data_user = $wpdb->get_row(
      "SELECT ldr_user_emas.id, 
        ldr_user_emas.name,
        ldr_user_emas.email
      FROM ldr_user_emas 
      WHERE user_id=" . $user_id . "",
      OBJECT
    );

    ddlog("End select user emas for user id " . $user_id . ".");

    ddlog("Start insert add new buy transaction emas for user id " . $user_id . " with status 'Create Invoice'.");

    $wpdb->query("INSERT INTO ldr_transaksi_emas (
        user_id, 
        type, 
        status,
        created_date
      ) VALUES (
        " . $data_user->id . ", 
        'buy', 
        'Create Invoice', 
        '" . date('Y-m-d H:i:s') . "'
      )");
    
    ddlog("End insert add new buy transaction emas for user id " . $user_id . " with status 'Create Invoice'.");

    ddlog("Start select latest buy transaction emas for user id " . $user_id . " with status 'Create Invoice'.");

    $new_transaksi_emas = $wpdb->get_row(
      "SELECT id, user_id, invoice_order 
        FROM ldr_transaksi_emas 
        WHERE user_id=" . $data_user->id . " 
          AND type='buy' 
          AND status='Create Invoice' 
        ORDER BY id DESC LIMIT 1",
      OBJECT
    );
    
    ddlog("End select latest buy transaction emas for user id " . $user_id . " with status 'Create Invoice'.");

    if(!empty($new_transaksi_emas)) {
      $new_transaksi_emas_id = $new_transaksi_emas->id;
    } else {
      $new_transaksi_emas_id = 0;
    }

    $wpdb->query("INSERT INTO ldr_transaksi_emas_log (
        transaksi_emas_id, 
        log, 
        status
      ) VALUES (
        " . $new_transaksi_emas_id . ", 
        'Membuat invoice beli emas.', 
        'Create Invoice'
      )");

    $external_id_invoice = "invoice-emas-" . date("Ymd") . "-" . date("His");
    $invoice_id = "LDREMAS".date("YmdHis");

    ddlog("Start process buy emas to treasury for user id " . $user_id . " with treasury invoice " . $invoice_id . ".");

    $beli_emas_datas = array(
      "invoice_number"  => $invoice_id,
      "unit"            => $calculate_beli_emas->data->unit,
      "total"           => $calculate_beli_emas->data->total,
      "payment_method"  => "partner",
      "payment_channel" => $datas["bank_trs"]
    );
    if((isset($datas["latitude"]) && isset($datas["longitude"])) && (!empty($datas["latitude"]) && !empty($datas["longitude"]))) {
      $beli_emas_datas["latitude"]  = $datas["latitude"];
      $beli_emas_datas["longitude"] = $datas["longitude"];
    };

    if(!isset($_SESSION["customer_token"])) {
      $login_trs = gld_login(array("user_id"=>$user_id));
      $login_trs = json_decode($login_trs);

      $customer_token = $login_trs->data->access_token;
    } else {
      $customer_token = $_SESSION["customer_token"];
    }

    $curl = curl_init();

    curl_setopt_array($curl, array(
      CURLOPT_URL => getenv('TREASURY_BASE_URL') . "/buy",
      CURLOPT_RETURNTRANSFER => true,
      CURLOPT_ENCODING => "",
      CURLOPT_MAXREDIRS => $GLOBALS["gld_global"]["curl_maxredirs"],
      CURLOPT_TIMEOUT => $GLOBALS["gld_global"]["curl_timeout"],
      CURLOPT_FOLLOWLOCATION => true,
      CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
      CURLOPT_CUSTOMREQUEST => "POST",
      CURLOPT_POSTFIELDS => $beli_emas_datas,
      CURLOPT_HTTPHEADER => array(
        "Authorization: Bearer " . $customer_token
      ),
    ));
    
    $output_beli_emas = curl_exec($curl);
    curl_getinfo($curl, CURLINFO_EFFECTIVE_URL);
    curl_getinfo($curl, CURLINFO_HTTP_CODE);
    curl_close($curl);
    $beli_emas = json_decode($output_beli_emas);

    if(!isset($beli_emas->meta->code)) {
      ddlog("End process buy emas to treasury for user id " . $user_id . " with treasury invoice " . $invoice_id . ". Failed to connect treasury api. \n" . htmlentities($output_beli_emas));

      $return = [
        "code"    => 500,
        "status"  => "failed",
        "message" => "Maaf terjadi gangguan dalam membeli emas. Silahkan coba lagi beberapa saat lagi.",
        "data"    => []
      ];
      
      return json_encode($return);
    }

    // ddbug($beli_emas);

    if($beli_emas->meta->code !== 200) {
      ddlog("End process buy emas to treasury for user id " . $user_id . " with treasury invoice " . $invoice_id . ". Error " . $beli_emas->meta->code . " (" . $beli_emas->meta->message . ").");

      $return = [
        "code"    => $beli_emas->meta->code,
        "status"  => "failed",
        "message" => "Maaf gagal membeli emas. Silahkan coba lagi beberapa saat lagi.",
        "data"    => []
      ];
      
      return json_encode($return);
    }

    ddlog("End process buy emas to treasury for user id " . $user_id . " with treasury invoice " . $invoice_id . ". Success.");

    $invoice_order = "INV/EMAS/" . date("Ymd") . "/" . date("His");

    $wpdb->query("UPDATE ldr_transaksi_emas SET 
        trs_invoice='" . $invoice_id . "', 
        invoice_order='" . $invoice_order . "', 
        total_gold='" . $beli_emas->data->unit . "', 
        total_price='" . $beli_emas->data->currency . "', 
        buying_rate='" . $beli_emas->data->buy_price . "', 
        selling_rate='" . $beli_emas->data->sell_price . "', 
        booking_fee='" . $beli_emas->data->booking_fee . "', 
        tax='" . $beli_emas->data->tax . "', 
        partner_fee='" . $beli_emas->data->parter_fee . "', 
        trs_payment_channel='" . $datas["bank_trs"] . "',
        trs_due_date='" . $beli_emas->data->due_date . "'
      WHERE id=" . $new_transaksi_emas_id);

    $total_emas_currency = round($beli_emas->data->total);
    $emas_currency = round($beli_emas->data->currency);

    $LadaraFee = $wpdb->get_row("SELECT * FROM ldr_xendit_fee WHERE category = '" . strtolower($datas["type_payment"]) . "' LIMIT 1", OBJECT);  

    if(strtolower($datas["type_payment"]) === "va") {
      $xendit_fee = $LadaraFee->fee_price ?? 4500;
      $xendit_ppn = round(floatval($LadaraFee->fee_ppn ?? 0.1) * $xendit_fee);
      $xendit_fee = $xendit_fee + $xendit_ppn;
    } else if(strtolower($datas["type_payment"]) === "cc") {
      $xendit_fee = $LadaraFee->fee_price ?? 2000;
      $xendit_variable_fee = round(floatval($LadaraFee->fee_cc ?? 0.029) * $total_emas_currency);
      $xendit_ppn = round(floatval($LadaraFee->fee_ppn ?? 0.1) * $xendit_fee);
      $xendit_ppn_variable = round(floatval($LadaraFee->fee_ppn ?? 0.1) * $xendit_variable_fee);
      $xendit_total_ppn = $xendit_ppn + $xendit_ppn_variable;
      $xendit_fee = $xendit_fee + $xendit_variable_fee + $xendit_total_ppn;
    } else if(strtolower($datas["type_payment"]) === "ewallet") {
      $xendit_fee_percent = $LadaraFee->fee_cc ?? 1.5;
      $xendit_fee = round(($xendit_fee_percent / 100) * $total_emas_currency);
      $xendit_ppn = round(floatval($LadaraFee->fee_ppn ?? 0.1) * $xendit_fee);
      $xendit_fee = $xendit_fee + $xendit_ppn;
    } else if(strtolower($datas["type_payment"]) === "retail") {
      $xendit_fee = $LadaraFee->fee_price ?? 5000;
    }

    $total_amount = $total_emas_currency + $xendit_fee;
    $xendit_id = NULL;
    $xendit_external_id = NULL;
    $xendit_payment_code = NULL;
    $xendit_url = NULL;
    $xendit_bank = NULL;
    $xendit_va = NULL;
    $nohp = NULL;

    ddlog("Start process create invoice buy emas for invoice " . $invoice_order . ".");

    if (strtolower($datas["type_payment"]) === "ewallet" && isset($datas["ewallet_type"])) {
      $metode_pembayaran = strtoupper($datas["ewallet_type"]);
      $nohp = "0" . $datas["ewallet_phone"];

      $data_invoice = [
        "external_id"   => $external_id_invoice,
        "amount"        => $total_amount,
        "ewallet_type"  => strtoupper($datas["ewallet_type"])
      ];

      if(strtolower($datas["ewallet_type"]) === "ovo") {
        $data_invoice["phone"] = $nohp;
      }

      if(strtolower($datas["ewallet_type"]) === "dana") {
        $data_invoice['expiration_date'] = date(DATE_ATOM, strtotime("+1 hours"));
        $data_invoice['callback_url'] = home_url().'/payment-confirmation';
        // $data_invoice['callback_url'] = home_url().'/emas/beli/konfirmasi-pembayaran';
        $data_invoice['redirect_url'] = home_url().'/emas/beli/sukses-bayar/?invoice=' . $invoice_order;
      }

      if(strtolower($datas["ewallet_type"]) === "linkaja") {
        $orderDetails = array(
          [
            "id"        => $new_transaksi_emas_id,
            "name"      => "Emas " . $beli_emas->data->unit . "gram",
            "price"     => $total_emas_currency,
            "quantity"  => 1
          ]
        );
        $data_invoice['items'] = $orderDetails;
        $data_invoice["phone"] = $nohp;
        $data_invoice['callback_url'] = home_url().'/payment-confirmation';
        // $data_invoice['callback_url'] = home_url().'/emas/beli/konfirmasi-pembayaran';
        $data_invoice['redirect_url'] = home_url().'/emas/beli/sukses-bayar/?invoice=' . $invoice_order;
      }

      $create_invoice = EWallets::create($data_invoice);
      // ddbug($datas, false);
      // ddbug($data_invoice, false);
      // ddbug($create_invoice);
      
      if(strtolower($datas["ewallet_type"]) === "linkaja" && !isset($create_invoice["external_id"])) {
        ddlog("End process create invoice buy emas for invoice " . $invoice_order . ". Failed.\n" . json_encode($create_invoice));
        
        $response = array(
          "code"    => 400,
          "status"  => "failed",
          "message" => "No hp kamu belum terdaftar di LinkAja. Silahkan daftar di aplikasi LinkAja terlebih dahulu.",
          "data"    => []
        );
        
        return json_encode($return);
      }
      
      if(strtolower($datas["ewallet_type"]) !== "linkaja" && !isset($create_invoice["external_id"])) {
        ddlog("End process create invoice buy emas for invoice " . $invoice_order . ". Failed.\n" . json_encode($create_invoice));
        
        $response = array(
          "code"    => 400,
          "status"  => "failed",
          "message" => "Maaf invoice pembelian emasmu gagal dibuat. Silahkan coba lagi setelah beberapa menit.",
          "data"    => []
        );
        
        return json_encode($return);
      }

      $xendit_external_id = $create_invoice["external_id"];

      if(strtolower($datas["ewallet_type"]) === "ovo") {
        $xendit_id = $create_invoice["business_id"];
      }

      if(strtolower($datas["ewallet_type"]) === "dana") {
        $xendit_url = $create_invoice["checkout_url"];
        $xendit_id = $create_invoice["external_id"];
      }
    }
    
    if (strtolower($datas["type_payment"]) === "retail" && isset($datas["retail_type"])) {
      $metode_pembayaran = strtoupper($datas["retail_type"]);

      $data_invoice = [
          "external_id"         => $external_id_invoice,
          "retail_outlet_name"  => strtoupper($datas["retail_type"]),
          "name"                => $data_user->name,
          "expected_amount"     => $total_amount,
          "expiration_date"     => date(DATE_ATOM, strtotime("+1 hours")),
          "is_single_use"       => true
      ];

      $create_invoice = Retail::create($data_invoice);
      
      if(!isset($create_invoice["external_id"])) {
        ddlog("End process create invoice buy emas for invoice " . $invoice_order . ". Failed.\n" . json_encode($create_invoice));
        
        $response = array(
          "code"    => 400,
          "status"  => "failed",
          "message" => "Maaf invoice pembelian emasmu gagal dibuat. Silahkan coba lagi setelah beberapa menit.",
          "data"    => []
        );
        
        return json_encode($return);
      }

      $xendit_external_id = $create_invoice["external_id"];
      $xendit_id = $create_invoice["id"];
      $xendit_payment_code = $create_invoice["payment_code"];
    }
    
    if (strtolower($datas["type_payment"]) === "va") {
      $namabank = $datas["bank"];
      if(strtolower($datas["bank"]) === "mandiri" || strtolower($datas["bank"]) === "permata") {
        $namabank = ucfirst($datas["bank"]);
      }
      $metode_pembayaran = $namabank . " Virtual Account";
      $payment_methods_xendit = [$datas["bank"]];
      $xendit_bank = $datas["bank"];

      $data_invoice = [
        'external_id'           => $external_id_invoice,
        'amount'                => $total_amount,
        'description'           => "Pembelian Emas di Ladara Emas dengan invoice " . $invoice_order,
        'payer_email'           => $data_user->email,
        'invoice_duration'      => 3600,
        'payment_methods'       => $payment_methods_xendit,
        'should_send_email'     => false,
        'success_redirect_url'  => home_url().'/emas/beli/sukses-bayar/?invoice=' . $invoice_order,
        'failure_redirect_url'  => home_url().'/emas/beli/gagal-bayar/?invoice=' . $invoice_order
      ];

      // Get User Virtual Account
      $virtualAccountUser = $wpdb->get_row("SELECT * FROM ldr_virtual_accounts WHERE user_id = $user_id AND bank_code = '$namabank'");
      $virtualAccountName = preg_replace('/\s+/', ' ', preg_replace("/[^a-zA-Z ]/", "", $data_user->name)); // Get only string
      $virtualAccountNumber = '9999'.sprintf('%06u', $user_id);

      if ($virtualAccountUser === null) { // If User doesn't have Virtual Account, create for him
        $VA = VirtualAccounts::create([
          'external_id' => 'VA_'.$user_id.'_'.$namabank.'_'.$virtualAccountNumber,
          'bank_code' => $namabank,
          'name' => $virtualAccountName
        ]);

        $insertData = [
          'user_id' => $user_id,
          'virtual_account_id' => $VA['id'],
          'virtual_account_number' => $VA["account_number"],
          'bank_code' => $namabank
        ];

        $wpdb->insert('ldr_virtual_accounts', $insertData);

        $virtualAccountUser = (object) $insertData;
      }

      $data_invoice['callback_virtual_account_id'] = $virtualAccountUser->virtual_account_id;
      
      $create_invoice = Invoice::create($data_invoice);

      if(!isset($create_invoice["id"])) {
        ddlog("End process create invoice buy emas for invoice " . $invoice_order . ". Failed.\n" . json_encode($create_invoice));
        
        $response = array(
          "code"    => 400,
          "status"  => "failed",
          "message" => "Maaf invoice pembelian emasmu gagal dibuat. Silahkan coba lagi setelah beberapa menit.",
          "data"    => []
        );
        
        return json_encode($return);
      }

      $xendit_id = $create_invoice["id"];
      $xendit_external_id = $create_invoice["external_id"];
      $xendit_url = $create_invoice["invoice_url"];
      $xendit_va = $virtualAccountUser->virtual_account_number;
    }
    
    if (strtolower($datas["type_payment"]) === "cc") {
      $metode_pembayaran = "Kartu Kredit";
      $xendit_bank = "";

      $data_invoice = [
        'external_id'           => $external_id_invoice,
        'amount'                => $total_amount,
        'description'           => "Pembelian Emas di Ladara Emas dengan invoice " . $invoice_order,
        'payer_email'           => $data_user->email,
        'invoice_duration'      => 3600,
        'payment_methods'       => ["CREDIT_CARD"],
        'should_send_email'     => false,
        'success_redirect_url'  => home_url().'/emas/beli/sukses-bayar/?invoice=' . $invoice_order,
        'failure_redirect_url'  => home_url().'/emas/beli/gagal-bayar/?invoice=' . $invoice_order
      ];
      
      $create_invoice = Invoice::create($data_invoice);

      if(!isset($create_invoice["id"])) {
        ddlog("End process create invoice buy emas for invoice " . $invoice_order . ". Failed.\n" . json_encode($create_invoice));
        
        $response = array(
          "code"    => 400,
          "status"  => "failed",
          "message" => "Maaf invoice pembelian emasmu gagal dibuat. Silahkan coba lagi setelah beberapa menit.",
          "data"    => []
        );
        
        return json_encode($return);
      }

      $xendit_id = $create_invoice["id"];
      $xendit_external_id = $create_invoice["external_id"];
      $xendit_url = $create_invoice["invoice_url"];
    }
    
    ddlog("End process create invoice buy emas for invoice " . $invoice_order . ". Success.");

    ddlog("Start update status invoice " . $invoice_order . " to 'Pending'.");


    // if($nohp !== NULL) {
    //   $wpdb->query("UPDATE ldr_transaksi_emas SET 
    //       ewallet_phone='" . $nohp . "' 
    //     WHERE id=" . $new_transaksi_emas_id);
    // }

    // if (strtolower($datas["type_payment"]) === "retail") {
    //   $wpdb->query("UPDATE ldr_transaksi_emas SET 
    //       xendit_payment_code='" . $xendit_payment_code . "' 
    //     WHERE id=" . $new_transaksi_emas_id);
    // }

    $data_update_transaksi_emas = [
      "trs_invoice"         => $invoice_id,
      "invoice_order"       => $invoice_order,
      "total_gold"          => $beli_emas->data->unit,
      "total_price"         => $beli_emas->data->currency,
      "payment_method"      => $metode_pembayaran,
      // "trs_va"              => $beli_emas->data->virtual_account, //treasury virtual account
      "trs_due_date"        => $beli_emas->data->due_date,
      "trs_payment_channel" => $datas["bank_trs"],
      "buying_rate"         => $beli_emas->data->buy_price,
      "selling_rate"        => $beli_emas->data->sell_price,
      "booking_fee"         => $beli_emas->data->booking_fee,
      "tax"                 => $beli_emas->data->tax,
      "partner_fee"         => $beli_emas->data->parter_fee,
      // "bank_fee"            => $beli_emas->data->bank_fee, //treasury bank fee
      "xendit_fee"          => $xendit_fee,
      "xendit_external_id"  => $xendit_external_id,
      "xendit_id"           => $xendit_id,
      "xendit_url"          => $xendit_url,
      "xendit_bank"         => $xendit_bank,
      "xendit_va"           => $xendit_va,
      "xendit_payment_code" => $xendit_payment_code,
      "ewallet_phone"       => $nohp,
      "total_payment"       => $total_amount,
      "status"              => "Pending",
      "due_date"            => date("Y-m-d H:i:s", strtotime('+1 hours')),
      "created_date"        => date('Y-m-d H:i:s')
    ];

    $wpdb->query("UPDATE ldr_transaksi_emas SET 
        invoice_order = '" . $data_update_transaksi_emas["invoice_order"] . "', 
        payment_method = '" . $data_update_transaksi_emas["payment_method"] . "', 
        xendit_fee='" . $data_update_transaksi_emas["xendit_fee"] . "', 
        xendit_external_id='" . $data_update_transaksi_emas["xendit_external_id"] . "', 
        xendit_id='" . $data_update_transaksi_emas["xendit_id"] . "', 
        xendit_url='" . $data_update_transaksi_emas["xendit_url"] . "', 
        xendit_bank='" . $data_update_transaksi_emas["xendit_bank"] . "', 
        xendit_va='" . $data_update_transaksi_emas["xendit_va"] . "', 
        xendit_payment_code='" . $data_update_transaksi_emas["xendit_payment_code"] . "', 
        ewallet_phone='" . $data_update_transaksi_emas["ewallet_phone"] . "', 
        trs_payment_channel = '" . $data_update_transaksi_emas["trs_payment_channel"] . "', 
        total_payment = '" . $data_update_transaksi_emas["total_payment"] . "', 
        status = '" . $data_update_transaksi_emas["status"] . "', 
        due_date = '" . $data_update_transaksi_emas["due_date"] . "', 
        created_date = '" . $data_update_transaksi_emas["created_date"] . "'
      WHERE id = " . $new_transaksi_emas_id);

    ddlog("End update status invoice " . $invoice_order . " to 'Pending'. Success");

    $wpdb->query("INSERT INTO ldr_transaksi_emas_log (
        transaksi_emas_id, 
        log, 
        status
      ) VALUES (
        " . $new_transaksi_emas_id . ", 
        'Pembelian emas dengan invoice " . $data_update_transaksi_emas["invoice_order"] . " menunggu pembayaran.', 
        'Pending'
      )");

    ddlog("Start add payment log for invoice " . $invoice_order . ".");

    $data_payment_log = [
      "order_id"                  => $new_transaksi_emas_id,
      "xendit_id"                 => $data_update_transaksi_emas["xendit_id"],
      "external_id"               => $data_update_transaksi_emas["xendit_external_id"],
      "payment_method"            => "",
      "status"                    => $create_invoice["status"],
      "paid_amount"               => $total_amount,
      "bank_code"                 => "",
      "paid_at"                   => "",
      "adjusted_received_amount"  => "",
      // "currency"                  => $create_invoice["currency"],
      "currency"                  => "IDR",
      "type"                      => "EMAS"
    ];

    $query_ldr_payment_log = "INSERT INTO ldr_payment_log (
        order_id, 
        xendit_id, 
        external_id, 
        payment_method, 
        status, 
        paid_amount, 
        bank_code, 
        paid_at, 
        adjusted_received_amount, 
        currency, 
        type
      ) VALUES (
        " . $data_payment_log["order_id"] . ", 
        '" . $data_payment_log["xendit_id"] . "', 
        '" . $data_payment_log["external_id"] . "', 
        '" . $data_payment_log["payment_method"] . "', 
        '" . $data_payment_log["status"] . "', 
        '" . $data_payment_log["paid_amount"] . "', 
        '" . $data_payment_log["bank_code"] . "', 
        '" . $data_payment_log["paid_at"] . "', 
        '" . $data_payment_log["adjusted_received_amount"] . "', 
        '" . $data_payment_log["currency"] . "', 
        '" . $data_payment_log["type"] . "'
      )";
    $wpdb->query($query_ldr_payment_log);

    ddlog("End add payment log for invoice " . $invoice_order . ".");

    if(strtolower($datas["type_payment"]) === "va") {
      $va_instructions = gld_va_instructions(array("bank"=>$xendit_bank));
      $va_instructions = json_decode($va_instructions);
    }

    $data_email = [
      "invoice_order"     => $data_update_transaksi_emas["invoice_order"],
      "customer_name"     => $data_user->name,
      "customer_email"    => $data_user->email,
      "created_date"      => $data_update_transaksi_emas["created_date"],
      "due_date"          => $data_update_transaksi_emas["due_date"],
      "metode_pembayaran" => $data_update_transaksi_emas["payment_method"],
      "total_price"       => $data_update_transaksi_emas["total_price"],
      "total_emas"        => $data_update_transaksi_emas["total_gold"],
      "booking_fee"       => $data_update_transaksi_emas["booking_fee"],
      "tax"               => $data_update_transaksi_emas["tax"],
      "partner_fee"       => $data_update_transaksi_emas["partner_fee"],
      "total_payment"     => $data_update_transaksi_emas["total_payment"],
      "xendit_fee"        => $data_update_transaksi_emas["xendit_fee"],
      "xen_merchant_name" => $GLOBALS["gld_global"]["xendit"]["company_code"],
      "xen_company_code"  => $GLOBALS["gld_global"]["xendit"]["merchant_name"],
      "xendit_url"        => $data_update_transaksi_emas["xendit_url"],
      "no_va"             => $data_update_transaksi_emas["xendit_va"]
    ];

    if($data_update_transaksi_emas["xendit_va"] !== NULL && $va_instructions->code === 200) {
      $data_email["va_instructions"] = $va_instructions->data;
    }

    if(isset($xendit_payment_code)) {
      $data_email["xendit_payment_code"] = $xendit_payment_code;
    }

    ddlog("Start send email to user for invoice " . $invoice_order . ".");

    $customer = new UsersBuilder($data_user->name, $data_user->email);
    $email_customer = (new EmailHelper($customer))->invoiceBeliEmas($data_email);
    
    ddlog("End send email to user for invoice " . $invoice_order . ".");

    ddlog("Start send email to admin for invoice " . $invoice_order . ".");
    
    $admin = new UsersBuilder($GLOBALS["gld_global"]["email_admin"]["name"], $GLOBALS["gld_global"]["email_admin"]["email"]);
    $email_admin = (new EmailHelper($admin))->adminInvoiceBeliEmas($data_email);

    ddlog("End send email to admin for invoice " . $invoice_order . ".");

    ddlog("Start add web notification buy emas with invoice " . $invoice_order . ".");

    $notif_data = [
      "userId"        => $user_id,
      "title"         => "Kamu baru saja membeli emas",
      "descriptions"  => "Kamu baru saja membeli emas sebesar " . $data_update_transaksi_emas["total_gold"] . " gram. Yuk segera selesaikan pembelian emas mu!",
      "type"          => "emas",
      "orderId"       => 0,
      "data"          => []
    ];

    Notifications::addNotification($notif_data);

    ddlog("Start add web notification buy emas with invoice " . $invoice_order . ". Success.");

    $return = array(
      "code"    => 200,
      "status"  => "success",
      "message" => "Sukses.",
      "data"    => [
        "no_invoice"          => $data_update_transaksi_emas["invoice_order"],
        "created_at"          => $data_update_transaksi_emas["created_date"],
        "due_date"            => $data_update_transaksi_emas["due_date"],
        "metode_pembayaran"   => $data_update_transaksi_emas["payment_method"],
        "type_payment"        => strtolower($datas["type_payment"]),
        "total_emas"          => $data_update_transaksi_emas["total_gold"],
        "total_price"         => $data_update_transaksi_emas["total_price"],
        "total_payment"       => $data_update_transaksi_emas["total_payment"],
        "booking_fee"         => $data_update_transaksi_emas["booking_fee"],
        "tax"                 => $data_update_transaksi_emas["tax"],
        "partner_fee"         => $data_update_transaksi_emas["partner_fee"],
        "xen_merchant_name"   => $GLOBALS["gld_global"]["xendit"]["company_code"],
        "xen_company_code"    => $GLOBALS["gld_global"]["xendit"]["merchant_name"],
        "xendit_url"          => $data_update_transaksi_emas["xendit_url"],
        "xendit_fee"          => $data_update_transaksi_emas["xendit_fee"],
        "xendit_payment_code" => $data_update_transaksi_emas["xendit_payment_code"],
        "no_va"               => $data_update_transaksi_emas["xendit_va"],
        "va_instructions"     => [],
      ]
    );
    
    if($data_update_transaksi_emas["xendit_va"] !== NULL && $va_instructions->code === 200) {
      $return["data"]["va_instructions"] = $va_instructions->data;
    }

    return json_encode($return);
  }
  add_action( "gld_buy", "gld_buy", 0 );

	/**
	 * Update transaction status after xendit callback
	 * 
	 * param example:
	 * $param = array(
   *    "xendit_id" => "5eba6a00fef1c01bd4801aa4,
   *    "status"    => "Pending",
	 * );
	 * 
	 * param detail:
	 * xendit_id  : (string - mandatory)    invoice trs
	 * status     : (string - mandatory)    status
	 *
   * @param array $datas
	 * @return $return[]
	 * @author Amy <laksmise@gmail.com>
	 */
  function gld_payment($datas) {
    global $wpdb;

    ddlog("Start update transaction status after xendit callback for xendit id " . $datas["xendit_id"] . ".");

    $transaksi = $wpdb->get_row(
      "SELECT * FROM ldr_transaksi_emas WHERE xendit_id='" . $datas["xendit_id"] . "'",
      OBJECT
    );

    if(!empty($transaksi)) {

      ddlog("Start get user emas for invoice " . $transaksi->invoice_order);

      $data_user = $wpdb->get_row(
        "SELECT ldr_user_emas.id, 
          ldr_user_emas.user_id, 
          ldr_user_emas.name,
          ldr_user_emas.email,
          ldr_user_emas.gender,
          ldr_user_emas.ts_password,
          ldr_user_emas.gold_balance,
          ldr_user_emas.gold_balance_in_currency,
          ldr_user_emas.verified_user
        FROM ldr_user_emas WHERE id='" . $transaksi->user_id . "'",
        OBJECT
      );

      if(!empty($data_user)) {

        ddlog("End get user emas for invoice " . $transaksi->invoice_order . ". User not found.");
      
        $customer = new UsersBuilder($data_user->name, $data_user->email);
        $admin = new UsersBuilder($GLOBALS["gld_global"]["email_admin"]["name"], $GLOBALS["gld_global"]["email_admin"]["email"]);

        // $gold_balance = $trs_profile->data->gold_balance;
        // $gold_balance_in_currency = $trs_profile->data->gold_balance_in_currency;

        $gold_balance = $data_user->gold_balance + $transaksi->total_gold;
        $gold_balance_in_currency = $data_user->gold_balance_in_currency + $transaksi->total_price;

        $data_email = [
          "invoice_order"     => $transaksi->invoice_order,
          "customer_name"     => $data_user->name,
          "customer_email"    => $data_user->email,
          "created_date"      => $transaksi->created_date,
          "due_date"          => $transaksi->due_date,
          "metode_pembayaran" => $transaksi->payment_method,
          "total_price"       => $transaksi->total_price,
          "total_emas"        => $transaksi->total_gold,
          "booking_fee"       => $transaksi->booking_fee,
          "tax"               => $transaksi->tax,
          "partner_fee"       => $transaksi->partner_fee,
          "total_payment"     => $transaksi->total_payment,
          "xendit_fee"        => $transaksi->xendit_fee
        ];

        if(!empty($transaksi->xendit_va)) {
          $data_email["no_va"] = $transaksi->xendit_va;
        }

        if(str_replace(" ", "", strtolower($transaksi->payment_method)) == "kartukredit") {
          $payment_note = "Payment Ladara Emas with Credit Card";
        } else {
          $payment_note = "Payment Ladara Emas with " . $transaksi->payment_method;
        }

        ddlog("Start update status pembelian emas untuk invoice " . $transaksi->invoice_order);

        if (strtolower($datas["status"]) == 'paid' || strtolower($datas["status"]) == 'settled' || strtolower($status) === 'completed' || strtolower($status) === 'success_completed') { // jika status = paid / lunas
          ddlog("Status paid atau settled");

          ddlog("Start login treasury untuk invoice " . $transaksi->invoice_order);

          $ts_login_data = [
            "client_id"			=> getenv('TREASURY_CLIENT_ID'),
            "client_secret"	=> getenv('TREASURY_CLIENT_SECRET'),
            "grant_type"		=> getenv('TREASURY_GRANT_CUSTOMER'),
            "email"         => $data_user->email,
            "password"      => $data_user->ts_password
          ];

          $ch = curl_init();

          curl_setopt_array($ch, array(
            CURLOPT_URL => getenv('TREASURY_BASE_URL') . "/login",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => $GLOBALS["gld_global"]["curl_maxredirs"],
            CURLOPT_TIMEOUT => $GLOBALS["gld_global"]["curl_timeout"],
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "POST",
            CURLOPT_POSTFIELDS => $ts_login_data,
            CURLOPT_HTTPHEADER => array(
              "Accept: application/json",
            ),
          ));

          if(curl_exec($ch) !== false) {

            $ts_login = curl_exec($ch);
            curl_getinfo($ch, CURLINFO_EFFECTIVE_URL);
            curl_getinfo($ch, CURLINFO_HTTP_CODE);
            curl_close($ch);
            $ts_login_decode = json_decode($ts_login);

            if($ts_login_decode->meta->code === 200) {
              ddlog("Start notify payment ke treasury untuk invoice " . $transaksi->invoice_order);

              $authorization = "Authorization: Bearer " . $ts_login_decode->data->access_token;
              $curl_header = array();
              $curl_header[] = $authorization;

              $payment_notify_body = [
                "invoice_number"  => $transaksi->trs_invoice,
                "payment_note"    => $payment_note,
              ];

              $curl = curl_init();
              curl_setopt($curl, CURLOPT_URL, getenv('TREASURY_BASE_URL') . "/payment-notify");
              curl_setopt($curl, CURLOPT_TIMEOUT, 0);
              curl_setopt($curl, CURLOPT_HTTPHEADER, $curl_header);
              curl_setopt($curl, CURLOPT_FOLLOWLOCATION, true);
              curl_setopt($curl, CURLOPT_HTTP_VERSION, CURL_HTTP_VERSION_1_1);
              curl_setopt($curl, CURLOPT_CUSTOMREQUEST, "POST");
              curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
              curl_setopt($curl, CURLOPT_ENCODING, "");
              curl_setopt($curl, CURLOPT_MAXREDIRS, 0);
              curl_setopt($curl, CURLOPT_POSTFIELDS, $payment_notify_body);

              $payment_notify = curl_exec($curl);
              curl_getinfo($curl, CURLINFO_EFFECTIVE_URL);
              curl_getinfo($curl, CURLINFO_HTTP_CODE);
              curl_close($curl);
              $payment_notify_decode = json_decode($payment_notify);

              if(isset($payment_notify_decode->meta->code)) {
                
                if($payment_notify_decode->meta->code !== 200) {

                  ddlog("End notify payment ke treasury, gagal notity payment untuk invoice " . $transaksi->invoice_order . ". message error: " . $payment_notify_decode->meta->message);

                  ddlog("Start check status transaksi ke treasury ketika gagal notify untuk invoice " . $transaksi->invoice_order);

                  $detail_history_body = [
                    "type"          => "buy",
                    "invoice_no"    => $transaksi->trs_invoice,
                  ];

                  $cur = curl_init();
                  curl_setopt($cur, CURLOPT_URL, getenv('TREASURY_BASE_URL') . "/detail-history");
                  curl_setopt($cur, CURLOPT_TIMEOUT, 0);
                  curl_setopt($cur, CURLOPT_HTTPHEADER, $curl_header);
                  curl_setopt($cur, CURLOPT_FOLLOWLOCATION, true);
                  curl_setopt($cur, CURLOPT_HTTP_VERSION, CURL_HTTP_VERSION_1_1);
                  curl_setopt($cur, CURLOPT_CUSTOMREQUEST, "POST");
                  curl_setopt($cur, CURLOPT_RETURNTRANSFER, true);
                  curl_setopt($cur, CURLOPT_ENCODING, "");
                  curl_setopt($cur, CURLOPT_MAXREDIRS, 0);
                  curl_setopt($cur, CURLOPT_POSTFIELDS, $detail_history_body);

                  $detail_history = curl_exec($cur);
                  curl_getinfo($cur, CURLINFO_EFFECTIVE_URL);
                  curl_getinfo($cur, CURLINFO_HTTP_CODE);
                  curl_close($cur);
                  $detail_history_decode = json_decode($detail_history);

                  if(isset($detail_history_decode->meta->code)) {

                    if($detail_history_decode->meta->code === 200 && str_replace(" ", "", strtolower($detail_history_decode->data->status)) == "pembayaranberhasil") {
                      ddlog("End check status transaksi ke treasury, transaksi sukses untuk invoice " . $transaksi->invoice_order);

                      ddlog("Start update status untuk invoice " . $transaksi->invoice_order . ", sukses pembayaran berhasil");

                      $wpdb->query("UPDATE ldr_transaksi_emas SET 
                        status='Success'
                        WHERE id=" . $transaksi->id . " AND trs_invoice='" . $transaksi->trs_invoice . "'");

                      $wpdb->query("INSERT INTO ldr_transaksi_emas_log (
                          transaksi_emas_id, 
                          log, 
                          status
                        ) VALUES (
                          " . $transaksi->id . ", 
                          'Pembelian emas dengan invoice " . $transaksi->invoice_order . " sukses.', 
                          'Success'
                        )");

                      ddlog("End update status untuk invoice " . $transaksi->invoice_order . ", sukses");

                      ddlog("Start update balance untuk invoice " . $transaksi->invoice_order);

                      $wpdb->query("UPDATE ldr_user_emas SET 
                          gold_balance=" . $gold_balance . ",
                          gold_balance_in_currency=" . $gold_balance_in_currency . "
                          WHERE id=" . $data_user->id);
                      
                      ddlog("End update balance untuk invoice " . $transaksi->invoice_order);

                      ddlog("Start kirim email ke user dengan invoice " . $transaksi->invoice_order);
                      $email_customer = (new EmailHelper($customer))->suksesBeliEmas($data_email);
                      ddlog("End kirim email ke user dengan invoice " . $transaksi->invoice_order);

                      ddlog("Start kirim email ke admin dengan invoice " . $transaksi->invoice_order);
                      $email_admin = (new EmailHelper($admin))->adminSuksesBeliEmas($data_email);
                      ddlog("End kirim email ke admin dengan invoice " . $transaksi->invoice_order);

                    } else {

                      ddlog("End check status transaksi ke treasury, transaksi " . $detail_history_trs->data->status . " untuk invoice " . $transaksi->invoice_order);
                      
                      // seharusnya cek ulang

                      ddlog("Start update status untuk invoice " . $transaksi->invoice_order . ", sukses");

                      $wpdb->query("UPDATE ldr_transaksi_emas SET 
                        status='Success'
                        WHERE id=" . $transaksi->id . " AND trs_invoice='" . $transaksi->trs_invoice . "'");

                      $wpdb->query("INSERT INTO ldr_transaksi_emas_log (
                          transaksi_emas_id, 
                          log, 
                          status
                        ) VALUES (
                          " . $transaksi->id . ", 
                          'Pembelian emas dengan invoice " . $transaksi->invoice_order . " sukses.', 
                          'Success'
                        )");

                      ddlog("End update status untuk invoice " . $transaksi->invoice_order . ", sukses");

                      ddlog("Start update balance untuk invoice " . $transaksi->invoice_order);

                      $wpdb->query("UPDATE ldr_user_emas SET 
                          gold_balance=" . $gold_balance . ",
                          gold_balance_in_currency=" . $gold_balance_in_currency . "
                          WHERE id=" . $data_user->id);
                      
                      ddlog("End update balance untuk invoice " . $transaksi->invoice_order);

                      ddlog("Start kirim email ke user dengan invoice " . $transaksi->invoice_order);
                      $email_customer = (new EmailHelper($customer))->suksesBeliEmas($data_email);
                      ddlog("End kirim email ke user dengan invoice " . $transaksi->invoice_order);

                      ddlog("Start kirim email ke admin dengan invoice " . $transaksi->invoice_order);
                      $email_admin = (new EmailHelper($admin))->adminSuksesBeliEmas($data_email);
                      ddlog("End kirim email ke admin dengan invoice " . $transaksi->invoice_order);

                    }
                  } else {
                    ddlog("End check status transaksi ke treasury, transaksi A/N untuk invoice " . $transaksi->invoice_order);
                  }

                } else {

                  $wpdb->query("UPDATE ldr_transaksi_emas SET 
                    status='Success'
                    WHERE id=" . $transaksi->id . " AND trs_invoice='" . $transaksi->trs_invoice . "'");

                  $wpdb->query("INSERT INTO ldr_transaksi_emas_log (
                      transaksi_emas_id, 
                      log, 
                      status
                    ) VALUES (
                      " . $transaksi->id . ", 
                      'Pembelian emas dengan invoice " . $transaksi->invoice_order . " sukses.', 
                      'Success'
                    )");

                  $wpdb->query("UPDATE ldr_user_emas SET 
                    gold_balance=" . $gold_balance . ",
                    gold_balance_in_currency=" . $gold_balance_in_currency . "
                    WHERE id=" . $data_user->id);

                  $email_customer = (new EmailHelper($customer))->suksesBeliEmas($data_email);
                  $email_admin = (new EmailHelper($admin))->adminSuksesBeliEmas($data_email);

                  $notif_data = [
                    "userId"        => $data_user->user_id,
                    "title"         => "Selamat! Pembelian emas kamu telah berhasil!",
                    "descriptions"  => "Pembelian emas kamu sebesar " . str_replace(".", ",", $transaksi->total_gold) . " gram telah berhasil! Yuk cek rate emas terkini di Ladara Emas.",
                    "type"          => "emas",
                    "orderId"       => 0,
                    "data"          => []
                  ];

                  Notifications::addNotification($notif_data);

                }

              } else {
                ddlog("End notify payment ke treasury, gagal notity payment untuk invoice " . $transaksi->invoice_order . ". message error: A/N");
              }

            } else {
              ddlog("End login treasury untuk invoice " . $transaksi->invoice_order . " gagal login. error message : " . $ts_login_decode->meta->message);
            }

          } else {
            ddlog("End login treasury untuk invoice " . $transaksi->invoice_order . " gagal login 2 error message : " . curl_error($ch));
          }

        } else if (strtolower($datas["status"]) == 'pending') { // jika status = pending / on-hold
          
          ddlog("Status pending");
          
          $wpdb->query("UPDATE ldr_transaksi_emas SET 
            status='Pending'
          WHERE id=" . $transaksi->id . " AND trs_invoice='" . $transaksi->trs_invoice . "'");

          $wpdb->query("INSERT INTO ldr_transaksi_emas_log (
              transaksi_emas_id, 
              log, 
              status
            ) VALUES (
              " . $transaksi->id . ", 
              'Pembelian emas dengan invoice " . $transaksi->invoice_order . " sedang diproses.', 
              'Pending'
            )");

          // $email_customer = (new EmailHelper($customer))->pendingBeliEmas($data_email);
          // $email_admin = (new EmailHelper($admin))->adminPendingBeliEmas($data_email);

        } else if (strtolower($datas["status"]) == 'expired') {
          
          ddlog("Status expired");
          
          $wpdb->query("UPDATE ldr_transaksi_emas SET 
            status='Canceled', 
            status_reason='Pembelian melewati batas pembayaran' 
          WHERE id=" . $transaksi->id . " AND trs_invoice='" . $transaksi->trs_invoice . "'");

          $wpdb->query("INSERT INTO ldr_transaksi_emas_log (
              transaksi_emas_id, 
              log, 
              status
            ) VALUES (
              " . $transaksi->id . ", 
              'Pembelian emas dengan invoice " . $transaksi->invoice_order . " dibatalkan.', 
              'Canceled'
            )");

          $email_customer = (new EmailHelper($customer))->pembatalanBeliEmas($data_email);
          $email_admin = (new EmailHelper($admin))->adminPembatalanBeliEmas($data_email);

          $notif_data = [
            "userId"        => $data_user->user_id,
            "title"         => "Invoice pembelian emas kamu telah expired!",
            "descriptions"  => "Pembelian emas kamu dengan invoice " . $transaksi->invoice_order . " telah expired! Tapi kamu dapat membeli emas kembali di Ladara Emas.",
            "type"          => "emas",
            "orderId"       => 0,
            "data"          => []
          ];

          Notifications::addNotification($notif_data);

        } else if (strtolower($datas["status"]) == 'canceled' || strtolower($datas["status"]) == 'failed') {

          ddlog("Status canceled atau failed");
          
          $wpdb->query("UPDATE ldr_transaksi_emas SET 
            status='Canceled', 
            status_reason='Pembayaran gagal' 
          WHERE id=" . $transaksi->id . " AND trs_invoice='" . $transaksi->trs_invoice . "'");

          $wpdb->query("INSERT INTO ldr_transaksi_emas_log (
              transaksi_emas_id, 
              log, 
              status
            ) VALUES (
              " . $transaksi->id . ", 
              'Pembelian emas dengan invoice " . $transaksi->invoice_order . " dibatalkan.', 
              'Canceled'
            )");

          $email_customer = (new EmailHelper($customer))->gagalBeliEmas($data_email);
          $email_admin = (new EmailHelper($admin))->adminGagalBeliEmas($data_email);

          $notif_data = [
            "userId"        => $data_user->user_id,
            "title"         => "Duh! Pembelian emas kamu gagal",
            "descriptions"  => "Pembelian emas kamu sebesar " . str_replace(".", ",", $transaksi->total_gold) . " gram gagal! Tapi kamu dapat membeli emas kembali di Ladara Emas.",
            "type"          => "emas",
            "orderId"       => 0,
            "data"          => []
          ];

          Notifications::addNotification($notif_data);
        }

        ddlog("End update status pembelian emas untuk invoice " . $transaksi->invoice_order);

      } else {
        
        ddlog("End get user emas for invoice " . $transaksi->invoice_order . ". User not found.");

      }

    } else {

      ddlog("End update transaction status after xendit callback for xendit id " . $datas["xendit_id"] . ". Transaction not found.");

    }

  }
  add_action( "gld_payment", "gld_payment", 0 );

	/**
	 * Update transaction status after xendit callback
	 * 
	 * param example:
	 * $param = array(
   *    "xendit_external_id"  => "5eba6a00fef1c01bd4801aa4,
   *    "status"              => "Pending",
	 * );
	 * 
	 * param detail:
	 * xendit_external_id : (string - mandatory)    invoice trs
	 * status             : (string - mandatory)    status
	 *
   * @param array $datas
	 * @return $return[]
	 * @author Amy <laksmise@gmail.com>
	 */
  function gld_payment_response($datas) {
    global $wpdb;

    ddlog("Start update transaction status after xendit callback for xendit external id " . $datas["xendit_external_id"] . ".");

    $transaksi = $wpdb->get_row(
      "SELECT * FROM ldr_transaksi_emas WHERE xendit_external_id='" . $datas["xendit_external_id"] . "'",
      OBJECT
    );

    if(!empty($transaksi)) {

      ddlog("Start get user emas for invoice " . $transaksi->invoice_order);

      $data_user = $wpdb->get_row(
        "SELECT ldr_user_emas.id, 
          ldr_user_emas.name,
          ldr_user_emas.email,
          ldr_user_emas.gender,
          ldr_user_emas.ts_password,
          ldr_user_emas.gold_balance,
          ldr_user_emas.gold_balance_in_currency,
          ldr_user_emas.verified_user
        FROM ldr_user_emas WHERE id='" . $transaksi->user_id . "'",
        OBJECT
      );

      if(!empty($data_user)) {

        ddlog("End get user emas for invoice " . $transaksi->invoice_order . ". User found.");
      
        $customer = new UsersBuilder($data_user->name, $data_user->email);
        $admin = new UsersBuilder($GLOBALS["gld_global"]["email_admin"]["name"], $GLOBALS["gld_global"]["email_admin"]["email"]);

        // $gold_balance = $trs_profile->data->gold_balance;
        // $gold_balance_in_currency = $trs_profile->data->gold_balance_in_currency;

        $gold_balance = $data_user->gold_balance + $transaksi->total_gold;
        $gold_balance_in_currency = $data_user->gold_balance_in_currency + $transaksi->total_price;

        $data_email = [
          "invoice_order"     => $transaksi->invoice_order,
          "customer_name"     => $data_user->name,
          "customer_email"    => $data_user->email,
          "created_date"      => $transaksi->created_date,
          "due_date"          => $transaksi->due_date,
          "metode_pembayaran" => $transaksi->payment_method,
          "total_price"       => $transaksi->total_price,
          "total_emas"        => $transaksi->total_gold,
          "booking_fee"       => $transaksi->booking_fee,
          "tax"               => $transaksi->tax,
          "partner_fee"       => $transaksi->partner_fee,
          "total_payment"     => $transaksi->total_payment,
          "xendit_fee"        => $transaksi->xendit_fee
        ];

        if(!empty($transaksi->xendit_va)) {
          $data_email["no_va"] = $transaksi->xendit_va;
        }

        if(str_replace(" ", "", strtolower($transaksi->payment_method)) == "kartukredit") {
          $payment_note = "Payment Ladara Emas with Credit Card";
        } else {
          $payment_note = "Payment Ladara Emas with " . $transaksi->payment_method;
        }

        ddlog("Start update status pembelian emas untuk invoice " . $transaksi->invoice_order);

        if (strtolower($datas["status"]) == 'paid' || strtolower($datas["status"]) == 'settled') { // jika status = paid / lunas
          ddlog("Status paid atau settled");

          ddlog("Start login treasury untuk invoice " . $transaksi->invoice_order);

          $ts_login_data = [
            "client_id"			=> getenv('TREASURY_CLIENT_ID'),
            "client_secret"	=> getenv('TREASURY_CLIENT_SECRET'),
            "grant_type"		=> getenv('TREASURY_GRANT_CUSTOMER'),
            "email"         => $data_user->email,
            "password"      => $data_user->ts_password
          ];

          $ch = curl_init();

          curl_setopt_array($ch, array(
            CURLOPT_URL => getenv('TREASURY_BASE_URL') . "/login",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => $GLOBALS["gld_global"]["curl_maxredirs"],
            CURLOPT_TIMEOUT => $GLOBALS["gld_global"]["curl_timeout"],
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "POST",
            CURLOPT_POSTFIELDS => $ts_login_data,
            CURLOPT_HTTPHEADER => array(
              "Accept: application/json",
            ),
          ));

          if(curl_exec($ch) !== false) {

            $ts_login = curl_exec($ch);
            curl_getinfo($ch, CURLINFO_EFFECTIVE_URL);
            curl_getinfo($ch, CURLINFO_HTTP_CODE);
            curl_close($ch);
            $ts_login_decode = json_decode($ts_login);

            if($ts_login_decode->meta->code === 200) {
              ddlog("Start notify payment ke treasury untuk invoice " . $transaksi->invoice_order);

              $authorization = "Authorization: Bearer " . $ts_login_decode->data->access_token;
              $curl_header = array();
              $curl_header[] = $authorization;

              $payment_notify_body = [
                "invoice_number"  => $transaksi->trs_invoice,
                "payment_note"    => $payment_note,
              ];

              $curl = curl_init();
              curl_setopt($curl, CURLOPT_URL, getenv('TREASURY_BASE_URL') . "/payment-notify");
              curl_setopt($curl, CURLOPT_TIMEOUT, 0);
              curl_setopt($curl, CURLOPT_HTTPHEADER, $curl_header);
              curl_setopt($curl, CURLOPT_FOLLOWLOCATION, true);
              curl_setopt($curl, CURLOPT_HTTP_VERSION, CURL_HTTP_VERSION_1_1);
              curl_setopt($curl, CURLOPT_CUSTOMREQUEST, "POST");
              curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
              curl_setopt($curl, CURLOPT_ENCODING, "");
              curl_setopt($curl, CURLOPT_MAXREDIRS, 0);
              curl_setopt($curl, CURLOPT_POSTFIELDS, $payment_notify_body);

              $payment_notify = curl_exec($curl);
              curl_getinfo($curl, CURLINFO_EFFECTIVE_URL);
              curl_getinfo($curl, CURLINFO_HTTP_CODE);
              curl_close($curl);
              $payment_notify_decode = json_decode($payment_notify);

              if(isset($payment_notify_decode->meta->code)) {
                
                if($payment_notify_decode->meta->code !== 200) {

                  ddlog("End notify payment ke treasury, gagal notity payment untuk invoice " . $transaksi->invoice_order . ". message error: " . $payment_notify_decode->meta->message);

                  ddlog("Start check status transaksi ke treasury ketika gagal notify untuk invoice " . $transaksi->invoice_order);

                  $detail_history_body = [
                    "type"          => "buy",
                    "invoice_no"    => $transaksi->trs_invoice,
                  ];

                  $cur = curl_init();
                  curl_setopt($cur, CURLOPT_URL, getenv('TREASURY_BASE_URL') . "/detail-history");
                  curl_setopt($cur, CURLOPT_TIMEOUT, 0);
                  curl_setopt($cur, CURLOPT_HTTPHEADER, $curl_header);
                  curl_setopt($cur, CURLOPT_FOLLOWLOCATION, true);
                  curl_setopt($cur, CURLOPT_HTTP_VERSION, CURL_HTTP_VERSION_1_1);
                  curl_setopt($cur, CURLOPT_CUSTOMREQUEST, "POST");
                  curl_setopt($cur, CURLOPT_RETURNTRANSFER, true);
                  curl_setopt($cur, CURLOPT_ENCODING, "");
                  curl_setopt($cur, CURLOPT_MAXREDIRS, 0);
                  curl_setopt($cur, CURLOPT_POSTFIELDS, $detail_history_body);

                  $detail_history = curl_exec($cur);
                  curl_getinfo($cur, CURLINFO_EFFECTIVE_URL);
                  curl_getinfo($cur, CURLINFO_HTTP_CODE);
                  curl_close($cur);
                  $detail_history_decode = json_decode($detail_history);

                  if(isset($detail_history_decode->meta->code)) {

                    if($detail_history_decode->meta->code === 200 && str_replace(" ", "", strtolower($detail_history_decode->data->status)) == "pembayaranberhasil") {
                      ddlog("End check status transaksi ke treasury, transaksi sukses untuk invoice " . $transaksi->invoice_order);

                      ddlog("Start update status untuk invoice " . $transaksi->invoice_order . ", sukses pembayaran berhasil");

                      $wpdb->query("UPDATE ldr_transaksi_emas SET 
                        status='Success'
                        WHERE id=" . $transaksi->id . " AND trs_invoice='" . $transaksi->trs_invoice . "'");

                      $wpdb->query("INSERT INTO ldr_transaksi_emas_log (
                          transaksi_emas_id, 
                          log, 
                          status
                        ) VALUES (
                          " . $transaksi->id . ", 
                          'Pembelian emas dengan invoice " . $transaksi->invoice_order . " sukses.', 
                          'Success'
                        )");

                      ddlog("End update status untuk invoice " . $transaksi->invoice_order . ", sukses");

                      ddlog("Start update balance untuk invoice " . $transaksi->invoice_order);

                      $wpdb->query("UPDATE ldr_user_emas SET 
                          gold_balance=" . $gold_balance . ",
                          gold_balance_in_currency=" . $gold_balance_in_currency . "
                          WHERE id=" . $data_user->id);
                      
                      ddlog("End update balance untuk invoice " . $transaksi->invoice_order);

                      ddlog("Start kirim email ke user dengan invoice " . $transaksi->invoice_order);
                      $email_customer = (new EmailHelper($customer))->suksesBeliEmas($data_email);
                      ddlog("End kirim email ke user dengan invoice " . $transaksi->invoice_order);

                      ddlog("Start kirim email ke admin dengan invoice " . $transaksi->invoice_order);
                      $email_admin = (new EmailHelper($admin))->adminSuksesBeliEmas($data_email);
                      ddlog("End kirim email ke admin dengan invoice " . $transaksi->invoice_order);

                    } else {

                      ddlog("End check status transaksi ke treasury, transaksi " . $detail_history_trs->data->status . " untuk invoice " . $transaksi->invoice_order);
                      
                      // seharusnya cek ulang

                      ddlog("Start update status untuk invoice " . $transaksi->invoice_order . ", sukses");

                      $wpdb->query("UPDATE ldr_transaksi_emas SET 
                        status='Success'
                        WHERE id=" . $transaksi->id . " AND trs_invoice='" . $transaksi->trs_invoice . "'");

                      $wpdb->query("INSERT INTO ldr_transaksi_emas_log (
                          transaksi_emas_id, 
                          log, 
                          status
                        ) VALUES (
                          " . $transaksi->id . ", 
                          'Pembelian emas dengan invoice " . $transaksi->invoice_order . " sukses.', 
                          'Success'
                        )");

                      ddlog("End update status untuk invoice " . $transaksi->invoice_order . ", sukses");

                      ddlog("Start update balance untuk invoice " . $transaksi->invoice_order);

                      $wpdb->query("UPDATE ldr_user_emas SET 
                          gold_balance=" . $gold_balance . ",
                          gold_balance_in_currency=" . $gold_balance_in_currency . "
                          WHERE id=" . $data_user->id);
                      
                      ddlog("End update balance untuk invoice " . $transaksi->invoice_order);

                      ddlog("Start kirim email ke user dengan invoice " . $transaksi->invoice_order);
                      $email_customer = (new EmailHelper($customer))->suksesBeliEmas($data_email);
                      ddlog("End kirim email ke user dengan invoice " . $transaksi->invoice_order);

                      ddlog("Start kirim email ke admin dengan invoice " . $transaksi->invoice_order);
                      $email_admin = (new EmailHelper($admin))->adminSuksesBeliEmas($data_email);
                      ddlog("End kirim email ke admin dengan invoice " . $transaksi->invoice_order);

                    }
                  } else {
                    ddlog("End check status transaksi ke treasury, transaksi A/N untuk invoice " . $transaksi->invoice_order);
                  }

                } else {

                  $wpdb->query("UPDATE ldr_transaksi_emas SET 
                    status='Success'
                    WHERE id=" . $transaksi->id . " AND trs_invoice='" . $transaksi->trs_invoice . "'");

                  $wpdb->query("INSERT INTO ldr_transaksi_emas_log (
                      transaksi_emas_id, 
                      log, 
                      status
                    ) VALUES (
                      " . $transaksi->id . ", 
                      'Pembelian emas dengan invoice " . $transaksi->invoice_order . " sukses.', 
                      'Success'
                    )");

                  $wpdb->query("UPDATE ldr_user_emas SET 
                    gold_balance=" . $gold_balance . ",
                    gold_balance_in_currency=" . $gold_balance_in_currency . "
                    WHERE id=" . $data_user->id);

                  $email_customer = (new EmailHelper($customer))->suksesBeliEmas($data_email);
                  $email_admin = (new EmailHelper($admin))->adminSuksesBeliEmas($data_email);

                }

              } else {
                ddlog("End notify payment ke treasury, gagal notity payment untuk invoice " . $transaksi->invoice_order . ". message error: A/N");
              }

            } else {
              ddlog("End login treasury untuk invoice " . $transaksi->invoice_order . " gagal login. error message : " . $ts_login_decode->meta->message);
            }

          } else {
            ddlog("End login treasury untuk invoice " . $transaksi->invoice_order . " gagal login 2 error message : " . curl_error($ch));
          }

        } else if (strtolower($datas["status"]) == 'pending') { // jika status = pending / on-hold
          
          ddlog("Status pending");
          
          $wpdb->query("UPDATE ldr_transaksi_emas SET 
            status='Pending'
          WHERE id=" . $transaksi->id . " AND trs_invoice='" . $transaksi->trs_invoice . "'");

          $wpdb->query("INSERT INTO ldr_transaksi_emas_log (
              transaksi_emas_id, 
              log, 
              status
            ) VALUES (
              " . $transaksi->id . ", 
              'Pembelian emas dengan invoice " . $transaksi->invoice_order . " sedang diproses.', 
              'Pending'
            )");

          // $email_customer = (new EmailHelper($customer))->pendingBeliEmas($data_email);
          // $email_admin = (new EmailHelper($admin))->adminPendingBeliEmas($data_email);

        } else if (strtolower($datas["status"]) == 'expired') {
          
          ddlog("Status expired");
          
          $wpdb->query("UPDATE ldr_transaksi_emas SET 
            status='Canceled', 
            status_reason='Pembelian melewati batas pembayaran' 
          WHERE id=" . $transaksi->id . " AND trs_invoice='" . $transaksi->trs_invoice . "'");

          $wpdb->query("INSERT INTO ldr_transaksi_emas_log (
              transaksi_emas_id, 
              log, 
              status
            ) VALUES (
              " . $transaksi->id . ", 
              'Pembelian emas dengan invoice " . $transaksi->invoice_order . " dibatalkan.', 
              'Canceled'
            )");

          $email_customer = (new EmailHelper($customer))->pembatalanBeliEmas($data_email);
          $email_admin = (new EmailHelper($admin))->adminPembatalanBeliEmas($data_email);

        } else if (strtolower($datas["status"]) == 'canceled') {

          ddlog("Status canceled");
          
          $wpdb->query("UPDATE ldr_transaksi_emas SET 
            status='Canceled', 
            status_reason='Pembayaran gagal' 
          WHERE id=" . $transaksi->id . " AND trs_invoice='" . $transaksi->trs_invoice . "'");

          $wpdb->query("INSERT INTO ldr_transaksi_emas_log (
              transaksi_emas_id, 
              log, 
              status
            ) VALUES (
              " . $transaksi->id . ", 
              'Pembelian emas dengan invoice " . $transaksi->invoice_order . " dibatalkan.', 
              'Canceled'
            )");

          $email_customer = (new EmailHelper($customer))->gagalBeliEmas($data_email);
          $email_admin = (new EmailHelper($admin))->adminGagalBeliEmas($data_email);

        } else if (strtolower($datas["status"]) == 'failed') {

          ddlog("Status failed");
          
          $wpdb->query("UPDATE ldr_transaksi_emas SET 
            status='Failed', 
            status_reason='Pembayaran gagal' 
          WHERE id=" . $transaksi->id . " AND trs_invoice='" . $transaksi->trs_invoice . "'");

          $wpdb->query("INSERT INTO ldr_transaksi_emas_log (
              transaksi_emas_id, 
              log, 
              status
            ) VALUES (
              " . $transaksi->id . ", 
              'Pembelian emas dengan invoice " . $transaksi->invoice_order . " dibatalkan.', 
              'Canceled'
            )");

          $email_customer = (new EmailHelper($customer))->gagalBeliEmas($data_email);
          $email_admin = (new EmailHelper($admin))->adminGagalBeliEmas($data_email);
        }

        ddlog("End update status pembelian emas untuk invoice " . $transaksi->invoice_order);

      } else {
        
        ddlog("End get user emas for invoice " . $transaksi->invoice_order . ". User not found.");

      }

    } else {

      ddlog("End update transaction status after xendit callback for xendit external id " . $datas["xendit_external_id"] . ". Transaction not found.");

    }

  }
  add_action( "gld_payment_response", "gld_payment_response", 0 );

	/**
	 * Update transaction if xendit failed call callback emas
	 * 
	 * param example:
	 * $param = array(
   *    "start_date"  => "2020-10-01,
   *    "end_date"    => "2020-10-31",
	 * );
	 * 
	 * param detail:
	 * start_date : (date - mandatory)    
	 * end_date   : (date - mandatory)    
	 *
   * @param array $datas
	 * @author Amy <laksmise@gmail.com>
	 */
  function gld_check_payment_status($datas) {
    global $wpdb;

    $start_date = $datas["start_date"]." 00-00-00";
    $end_date = $datas["end_date"]." 23-59-59";

    $histories = $wpdb->get_results(
      "SELECT ldr_user_emas.user_id, ldr_transaksi_emas.user_id AS user_emas_id, ldr_user_emas.name, ldr_user_emas.email, ldr_user_emas.ts_password, 
          ldr_user_emas.gold_balance, ldr_user_emas.gold_balance_in_currency,
          ldr_user_emas.trs_token, ldr_user_emas.trs_token_expired, 
          ldr_transaksi_emas.id AS transaksi_id, ldr_transaksi_emas.type, ldr_transaksi_emas.invoice_order, ldr_transaksi_emas.total_gold, 
          ldr_transaksi_emas.total_price, ldr_transaksi_emas.buying_rate, ldr_transaksi_emas.booking_fee, ldr_transaksi_emas.tax, 
          ldr_transaksi_emas.partner_fee, ldr_transaksi_emas.bank_fee, ldr_transaksi_emas.trs_invoice, ldr_transaksi_emas.trs_due_date, 
          ldr_transaksi_emas.trs_payment_channel, ldr_transaksi_emas.payment_method, ldr_transaksi_emas.ewallet_phone,
          ldr_transaksi_emas.xendit_fee, ldr_transaksi_emas.xendit_va, ldr_transaksi_emas.xendit_payment_code, ldr_transaksi_emas.xendit_bank, 
          ldr_transaksi_emas.xendit_id, ldr_transaksi_emas.xendit_external_id, ldr_transaksi_emas.xendit_url, ldr_transaksi_emas.total_payment, 
          ldr_transaksi_emas.status, ldr_transaksi_emas.status_reason, ldr_transaksi_emas.due_date, ldr_transaksi_emas.created_date, 
          ldr_transaksi_emas.updated_date
        FROM ldr_transaksi_emas 
        JOIN ldr_user_emas ON ldr_transaksi_emas.user_id = ldr_user_emas.id
        WHERE status!='Success' 
          AND status!='Create Invoice' 
          AND type='buy' 
          AND ldr_transaksi_emas.created_date > '".$start_date."' AND ldr_transaksi_emas.created_date < '".$end_date."'
      ");

    foreach($histories as $h) {

      if(isset($datas["show_debug"]) && $datas["show_debug"] === TRUE) {
        $new_h = $h;
        unset($new_h->ts_password);
        unset($new_h->trs_token);
        unset($new_h->invoice_order);
        ddbug($new_h, false);
      }

      if(!empty($h->xendit_id)) {
        ddlog("Transaction ".$h->invoice_order." have xendit id.", "ladara_emas_cron");

        $transaksi_id = $h->transaksi_id;
        $user_emas_id = $h->user_emas_id;
        
        ddlog("Start retrieve payment for invoice ".$h->invoice_order.".", "ladara_emas_cron");

        $retrieve_payment = gld_retrieve_payment($transaksi_id, $user_emas_id);
        //gld_retrieve_payment dipake di mobile jg, supaya nggak ngerusak mobile jadi bikin 2 function ;;
        $retrieve_payment = json_decode($retrieve_payment);

        if(isset($datas["show_debug"]) && $datas["show_debug"] === TRUE) {
          ddbug($retrieve_payment, FALSE);
        }

        if($retrieve_payment->data->status === 1) {

          ddlog("End retrieve payment for invoice ".$h->invoice_order.". Invoice has been paid.", "ladara_emas_cron");

          ddlog("Start login treasury for retrieve payment process for invoice ".$h->invoice_order.".", "ladara_emas_cron");

          //login ke treasury satu per satu ^^;
          $login_trs = gld_login(array("user_id"=>$h->user_id));
          $login_trs = json_decode($login_trs);

          if($login_trs->code === 200) {

            ddlog("End login treasury for retrieve payment process for invoice ".$h->invoice_order.". Success.", "ladara_emas_cron");
            
            ddlog("Start payment notify for retrieve payment process for invoice ".$h->invoice_order.".", "ladara_emas_cron");

            $authorization = "Authorization: Bearer " . $login_trs->data->access_token;
            $curl_header = array();
            $curl_header[] = $authorization;

            if(str_replace(" ", "", strtolower($h->payment_method)) == "kartukredit") {
              $payment_note = "Payment Ladara Emas with Credit Card";
            } else {
              $payment_note = "Payment Ladara Emas with " . $h->payment_method;
            }

            $payment_notify_body = [
              "invoice_number"  => $h->trs_invoice,
              "payment_note"    => $payment_note,
            ];

            $curl = curl_init();
            curl_setopt($curl, CURLOPT_URL, getenv('TREASURY_BASE_URL') . "/payment-notify");
            curl_setopt($curl, CURLOPT_TIMEOUT, 0);
            curl_setopt($curl, CURLOPT_HTTPHEADER, $curl_header);
            curl_setopt($curl, CURLOPT_FOLLOWLOCATION, true);
            curl_setopt($curl, CURLOPT_HTTP_VERSION, CURL_HTTP_VERSION_1_1);
            curl_setopt($curl, CURLOPT_CUSTOMREQUEST, "POST");
            curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($curl, CURLOPT_ENCODING, "");
            curl_setopt($curl, CURLOPT_MAXREDIRS, 0);
            curl_setopt($curl, CURLOPT_POSTFIELDS, $payment_notify_body);

            $payment_notify = curl_exec($curl);
            curl_getinfo($curl, CURLINFO_EFFECTIVE_URL);
            curl_getinfo($curl, CURLINFO_HTTP_CODE);
            curl_close($curl);

            $payment_notify_decode = json_decode($payment_notify);

            if(isset($datas["show_debug"]) && $datas["show_debug"] === TRUE) {
              ddbug($payment_notify_decode, FALSE);
            }

            if(isset($payment_notify_decode->meta->code)) {
              
              if($payment_notify_decode->meta->code === 200) {
            
                ddlog("End payment notify for retrieve payment process for invoice ".$h->invoice_order.". Success.", "ladara_emas_cron");
                
                ddlog("Start update status invoice ".$h->invoice_order." and ".$h->email." balance.", "ladara_emas_cron");

                $wpdb->query("UPDATE ldr_transaksi_emas SET 
                  status='Success',
                  status_reason='' 
                  WHERE id=" . $h->transaksi_id . " AND trs_invoice='" . $h->trs_invoice . "'");

                $wpdb->query("INSERT INTO ldr_transaksi_emas_log (
                    transaksi_emas_id, 
                    log, 
                    status
                  ) VALUES (
                    " . $transaksi->id . ", 
                    'Pembelian emas dengan invoice " . $h->invoice_order . " sukses.', 
                    'Success'
                  )");

                $gold_balance = $h->gold_balance + $h->total_gold;
                $gold_balance_in_currency = $h->gold_balance_in_currency + $h->total_price;

                $wpdb->query("UPDATE ldr_user_emas SET 
                    gold_balance=" . $gold_balance . ",
                    gold_balance_in_currency=" . $gold_balance_in_currency . "
                    WHERE id=" . $h->user_emas_id);
                
                ddlog("End update status invoice ".$h->invoice_order." and ".$h->email." balance.", "ladara_emas_cron");

              } else {
                
                ddlog("End payment notify for retrieve payment process for invoice ".$h->invoice_order.". Error " . $payment_notify_decode->meta->code . " (" . $payment_notify_decode->meta->message . ").", "ladara_emas_cron");

              }

            } else {
              
              ddlog("End payment notify for retrieve payment process for invoice ".$h->invoice_order.". \n" . htmlentities($payment_notify), "ladara_emas_cron");

            }

          } else {

            ddlog("End login treasury for retrieve payment process for invoice ".$h->invoice_order.". Error " . $login_trs->code . " (" . $login_trs->message . ").", "ladara_emas_cron");

          }

        } else {

          ddlog("End retrieve payment for invoice ".$h->invoice_order.". Invoice haven't been paid.", "ladara_emas_cron");

        }

      } else {

        ddlog("Transaction ".$h->invoice_order." doesn't have xendit id.", "ladara_emas_cron");

      }

      if(isset($datas["show_debug"]) && $datas["show_debug"] === TRUE) {
        echo "<hr />";
      }

    }

    if(isset($datas["show_debug"]) && $datas["show_debug"] === TRUE) {
      exit;
    }

  }
  add_action( "gld_check_payment_status", "gld_check_payment_status", 0 );

	/**
	 * Call api xendit va instructions
	 * 
	 * param example:
	 * $param = array(
	 * 		"bank" => "MANDIRI"
	 * );
	 * 
	 * param detail:
	 * bank : (string - mandatory)  xendit bank type. MANDIRI, BCA, PERMATA, BRI and BNI
	 *
   * @param array $datas
	 * @return $return[]
	 * @author Amy <laksmise@gmail.com>
	 */
  function gld_va_instructions($datas) {
    ddlog("Start get xendit va instructions.");

    $url_json_id = "https://invoice.xendit.co/static/locales/id/";
    $urlInstructions = $url_json_id . "bni_instructions.json";
    if(isset($datas["bank"])) {
      $urlInstructions = $url_json_id . strtolower($datas["bank"]) . "_instructions.json";
    }
    
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $urlInstructions);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

    $output = curl_exec($ch);
    curl_close($ch);
    $json_response = json_decode($output);

    if(empty($json_response)) {
      ddlog("End get xendit va instructions. Failed. \n" . json_encode($json_response));

      $return = [
        "code"    => 500,
        "status"  => "failed",
        "message" => "Maaf gagal mengambil instruksi virtual account. Silahkan coba lagi beberapa saat lagi.",
        "data"    => []
      ];

      return json_encode($return);
    }

    ddlog("End get xendit va instructions. Success.");

    $return = [
      "code"    => 200,
      "status"  => "success",
      "message" => "Sukses.",
      "data"    => $json_response
    ];

    return json_encode($return);
  }
  add_action( "gld_va_instructions", "gld_va_instructions", 0 );

	/**
	 * jual emas,
   * calculate terlebih dahulu untuk memastikan kalau data yang dikirim sudah sesuai dengan hasil calculate treasury
	 * 
	 * param example:
	 * $param = array(
	 * 		"amount"      => 0.0918,
	 * 		"amount_type" => "gold",
   *    "latitude"    => "-6.3504384",
   *    "longitude"   => "106.8924928"
	 * );
	 * 
	 * param detail:
	 * amount       : (float - mandatory)   for example 0.0918 when use "gold" and 200000 when use "currency"
	 * amount_type  : (string - mandatory)  type of amount,
   *                                      options are "gold" for gold value and "currency" for currency (in rupiah) value
	 * latitude     : (string - optional)   latitude map
	 * longitude    : (string - optional)   longitude map
	 *
	 * @return 
	 * @author Amy <laksmise@gmail.com>
	 */
  function gld_sell($datas) {
    global $wpdb;

    if(!isset($datas["user_id"])) {
      $user_id = get_current_user_id();
    } else {
      $user_id = $datas["user_id"];
    }
    
    ddlog("Start calculate sell emas for user id " . $user_id . ".");

    $calculate_datas = array(
      "amount_type"       => $datas["amount_type"],
      "amount"            => $datas["amount"],
      "transaction_type"  => "sell",
      "payment_type"      => "gross",
      "user_id"           => $user_id
    );

    $calculate = gld_calculate($calculate_datas);
    $calculate = json_decode($calculate);

    if($calculate->code !== 200) {
      ddlog("End calculate sell emas for user id " . $user_id . ". Error " . $calculate->code . "(" . $calculate->message . ".");

      $return = array(
        "code"    => $calculate->code,
        "status"  => $calculate->status,
        "message" => $calculate->message,
        "data"    => $calculate->data
      );

      return json_encode($return);
    }

    ddlog("End calculate sell emas for user id " . $user_id . ". Success.");

    ddlog("Start select user emas for user id " . $user_id . ".");
    
    // $data_user = $wpdb->get_row(
    //   "SELECT ldr_user_emas.id, 
    //     ldr_user_emas.name,
    //     ldr_user_emas.email
    //   FROM ldr_user_emas 
    //   WHERE user_id=" . $user_id . "",
    //   OBJECT
    // );

    $data_user = gld_profile_emas(array("user_id"=>$user_id));
    $data_user = json_decode($data_user);

    ddlog("End select user emas for user id " . $user_id . ".");

    ddlog("Start insert add new buy transaction emas for user " . $data_user->data->email . " with status 'Create Invoice'.");

    $wpdb->query("INSERT INTO ldr_transaksi_emas (
        user_id, 
        type, 
        status,
        created_date
      ) VALUES (
        " . $data_user->data->id . ", 
        'sell', 
        'Create Invoice', 
        '" . date('Y-m-d H:i:s') . "'
      )");
    
    ddlog("End insert add new buy transaction emas for user " . $data_user->data->email . " with status 'Create Invoice'.");

    ddlog("Start select latest buy transaction emas for user " . $data_user->data->email . " with status 'Create Invoice'.");

    $new_transaksi_emas = $wpdb->get_row(
      "SELECT id, user_id, invoice_order 
        FROM ldr_transaksi_emas 
        WHERE user_id=" . $data_user->data->id . " 
          AND type='sell' 
          AND status='Create Invoice' 
        ORDER BY id DESC LIMIT 1",
      OBJECT
    );

    $wpdb->query("INSERT INTO ldr_transaksi_emas_log (
        transaksi_emas_id, 
        log, 
        status
      ) VALUES (
        " . $new_transaksi_emas_id . ", 
        'Membuat invoice jual emas.', 
        'Create Invoice'
      )");
    
    ddlog("End select latest buy transaction emas for user " . $data_user->data->email . " with status 'Create Invoice'.");

    if(!empty($new_transaksi_emas)) {
      $new_transaksi_emas_id = $new_transaksi_emas->id;
    } else {
      $new_transaksi_emas_id = 0;
    }

    ddlog("Start process sell emas to treasury for user " . $data_user->data->email . " with transaction id " . $new_transaksi_emas_id . ".");
    
    $jual_emas_datas = [
      "total" => $calculate->data->total,
      "unit"  => $calculate->data->unit
    ];

    if(!isset($_SESSION["customer_token"])) {
      $login_trs = gld_login(array("user_id"=>$user_id));
      $login_trs = json_decode($login_trs);

      $customer_token = $login_trs->data->access_token;
    } else {
      $customer_token = $_SESSION["customer_token"];
    }

    $curl = curl_init();

    curl_setopt_array($curl, array(
      CURLOPT_URL => getenv('TREASURY_BASE_URL') . "/sell",
      CURLOPT_RETURNTRANSFER => true,
      CURLOPT_ENCODING => "",
      CURLOPT_MAXREDIRS => $GLOBALS["gld_global"]["curl_maxredirs"],
      CURLOPT_TIMEOUT => $GLOBALS["gld_global"]["curl_timeout"],
      CURLOPT_FOLLOWLOCATION => true,
      CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
      CURLOPT_CUSTOMREQUEST => "POST",
      CURLOPT_POSTFIELDS => $jual_emas_datas,
      CURLOPT_HTTPHEADER => array(
        "Authorization: Bearer " . $customer_token
      ),
    ));
    
    $output_jual_emas = curl_exec($curl);
    curl_getinfo($curl, CURLINFO_EFFECTIVE_URL);
    curl_getinfo($curl, CURLINFO_HTTP_CODE);
    curl_close($curl);
    $jual_emas = json_decode($output_jual_emas);

    if(!isset($jual_emas->meta->code)) {
      ddlog("End process sell emas to treasury for user id " . $user_id . " with transaction id " . $new_transaksi_emas_id . ". Failed to connect treasury api. \n" . htmlentities($output_jual_emas));

      $return = [
        "code"    => 500,
        "status"  => "failed",
        "message" => "Maaf terjadi gangguan dalam menjual emas. Silahkan coba lagi beberapa saat lagi.",
        "data"    => []
      ];
      
      return json_encode($return);
    }

    if($jual_emas->meta->code !== 200) {
      ddlog("End process sell emas to treasury for user " . $data_user->data->email . " with transaction id " . $new_transaksi_emas_id . ". Error " . $jual_emas->meta->code . " (" . $jual_emas->meta->message . ").");

      $return = [
        "code"    => $jual_emas->meta->code,
        "status"  => "failed",
        "message" => "Maaf gagal menjual emas. Silahkan coba lagi beberapa saat lagi.",
        "data"    => []
      ];
      
      return json_encode($return);
    }

    ddlog("End process sell emas to treasury for user " . $data_user->data->email . " with transaction id " . $new_transaksi_emas_id . ". Success.");

    $invoice_order = "INV/EMAS/" . date("Ymd") . "/" . date("His");

    ddlog("Start update status invoice " . $invoice_order . " to 'Pending'.");

    $data_update_transaksi_emas = [
      "trs_invoice"         => $jual_emas->data->invoice_no,
      "invoice_order"       => $invoice_order,
      "total_gold"          => $calculate->data->unit,
      "total_price"         => $calculate->data->currency,
      "buying_rate"         => $calculate->data->buy_price,
      "selling_rate"        => $calculate->data->sell_price,
      "booking_fee"         => $calculate->data->booking_fee,
      "tax"                 => $calculate->data->tax,
      "partner_fee"         => $calculate->data->partner_fee,
      "bank_account"        => $data_user->data->account_number,
      "bank_account_name"   => $data_user->data->account_name,
      "bank_name"           => $data_user->data->bank_name,
      "total_payment"       => $calculate->data->total,
      "status"              => "Pending",
      "created_date"        => date('Y-m-d H:i:s')
    ];

    $query_update_ldr_transaksi_emas = "UPDATE ldr_transaksi_emas SET 
        trs_invoice = '" . $data_update_transaksi_emas["trs_invoice"] . "', 
        invoice_order = '" . $data_update_transaksi_emas["invoice_order"] . "', 
        total_gold = '" . $data_update_transaksi_emas["total_gold"] . "', 
        total_price = '" . $data_update_transaksi_emas["total_price"] . "', 
        buying_rate = '" . $data_update_transaksi_emas["buying_rate"] . "', 
        selling_rate = '" . $data_update_transaksi_emas["selling_rate"] . "', 
        booking_fee = '" . $data_update_transaksi_emas["booking_fee"] . "', 
        tax = '" . $data_update_transaksi_emas["tax"] . "', 
        partner_fee = '" . $data_update_transaksi_emas["partner_fee"] . "',
        bank_account = '" . $data_update_transaksi_emas["bank_account"] . "',
        bank_account_name = '" . $data_update_transaksi_emas["bank_account_name"] . "',
        bank_name = '" . $data_update_transaksi_emas["bank_name"] . "', 
        total_payment = '" . $data_update_transaksi_emas["total_payment"] . "', 
        status = '" . $data_update_transaksi_emas["status"] . "', 
        created_date = '" . $data_update_transaksi_emas["created_date"] . "'
      WHERE id = " . $new_transaksi_emas_id;
    $wpdb->query($query_update_ldr_transaksi_emas);

    $wpdb->query("INSERT INTO ldr_transaksi_emas_log (
        transaksi_emas_id, 
        log, 
        status
      ) VALUES (
        " . $new_transaksi_emas_id . ", 
        'Penjualan emas dengan invoice " . $data_update_transaksi_emas["invoice_order"] . " sedang diproses.', 
        'Pending'
      )");

    ddlog("End update status invoice " . $invoice_order . " to 'Pending'. Success."); 

    ddlog("Start update user emas balance for user id " . $user_id . ".");

    $trs_profile = gld_trs_profile(array("user_id"=>$user_id));
    $trs_profile = json_decode($trs_profile);

    if ($trs_profile->code === 200) {
      ddlog("End update user emas balance for user id " . $user_id . ". Success.");

      $gold_balance = $trs_profile->data->gold_balance;
      $gold_balance_in_currency = $trs_profile->data->gold_balance_in_currency;

      $wpdb->query("UPDATE ldr_user_emas SET 
        gold_balance=" . $gold_balance . ",
        gold_balance_in_currency=" . $gold_balance_in_currency . "
      WHERE id=" . $data_user->data->id);
    } else {
      ddlog("End update user emas balance for user id " . $user_id . ". Error " . $trs_profile->code . " (" . $trs_profile->message . ").");
    }

    $data_email = [
      "invoice_order"     => $data_update_transaksi_emas["invoice_order"],
      "customer_name"     => $data_user->data->name,
      "customer_email"    => $data_user->data->email,
      "created_date"      => $data_update_transaksi_emas["created_date"],
      "total_price"       => $data_update_transaksi_emas["total_price"],
      "total_emas"        => $data_update_transaksi_emas["total_gold"],
      "partner_fee"       => $data_update_transaksi_emas["partner_fee"],
      "total_payment"     => $data_update_transaksi_emas["total_payment"]
    ];

    ddlog("Start send email to user for invoice " . $invoice_order . ".");

    $customer = new UsersBuilder($data_user->data->name, $data_user->data->email);
    $email_customer = (new EmailHelper($customer))->invoiceJualEmas($data_email);

    ddlog("End send email to user for invoice " . $invoice_order . ".");

    ddlog("Start send email to admin for invoice " . $invoice_order . ".");
    
    $admin = new UsersBuilder($GLOBALS["gld_global"]["email_admin"]["name"], $GLOBALS["gld_global"]["email_admin"]["email"]);
    $email_admin = (new EmailHelper($admin))->adminInvoiceJualEmas($data_email);

    ddlog("End send email to admin for invoice " . $invoice_order . ".");

    ddlog("Start add web notification sell emas with invoice " . $invoice_order . ".");

    $notif_data = [
      "userId"        => $user_id,
      "title"         => "Kamu baru saja menjual emas",
      "descriptions"  => "Kamu baru saja menjual emas sebesar " . $data_update_transaksi_emas["total_gold"] . " gram. Kami akan memproses penjualan emas mu.",
      "type"          => "emas",
      "orderId"       => 0,
      "data"          => []
    ];

    Notifications::addNotification($notif_data);

    ddlog("Start add web notification sell emas with invoice " . $invoice_order . ". Success.");

    $return = array(
      "code"    => 200,
      "status"  => "success",
      "message" => "Sukses.",
      "data"    => [
        "no_invoice"        => $data_update_transaksi_emas["invoice_order"],
        "created_at"        => $data_update_transaksi_emas["created_date"],
        "selling_rate"      => $data_update_transaksi_emas["selling_rate"],
        "total_emas"        => $data_update_transaksi_emas["total_gold"],
        "total_currency"    => $data_update_transaksi_emas["total_price"],
        "total_payment"     => $data_update_transaksi_emas["total_payment"],
        "tax"               => $data_update_transaksi_emas["tax"],
        "partner_fee"       => $data_update_transaksi_emas["partner_fee"],
        "bank_name"         => $data_user->data->bank_name,
        "account_number"    => $data_user->data->account_number,
        "account_name"      => $data_user->data->account_name
      ]
    );

    return json_encode($return);
  }
  add_action( "gld_sell", "gld_sell", 0 );

	/**
	 * Get ladara emas banner
	 * 
	 * @return $return[]
	 * @author Amy <laksmise@gmail.com>
	 */
  function gld_banner() {
    global $wpdb;
    
    $post = $wpdb->get_row("SELECT ID, post_name  FROM ldr_posts
      WHERE post_name='emas'", OBJECT);

    $postID = $post->ID;

    $banner_desktop = get_post_meta($postID, "banner_emas_desktop", TRUE);
    $banner_desktop = wp_get_attachment_url($banner_desktop);

    $banner_mobile = get_post_meta($postID, "banner_emas_mobile", TRUE);
    $banner_mobile = wp_get_attachment_url($banner_mobile);

    if(!empty($banner_desktop) && !empty($banner_mobile)) {
      $return = array(
        "code"    => 200,
        "status"  => "success",
        "message" => "Success.",
        "data"    => [
          "desktop" => $banner_desktop,
          "mobile"  => $banner_mobile
        ]
      );
    } else {
      $template_directory = get_template_directory_uri();

      $return = array(
        "code"    => 404,
        "status"  => "failed",
        "message" => "Banner can not be found. Please use default banner.",
        "data"    => [
          "desktop" => $template_directory . "/library/images/emas/banner-register-ladara-emas.png",
          "mobile"  => $template_directory . "/library/images/emas/banner-register-ladara-emas-mobile.png"
        ]
      );
    }

    return json_encode($return);
  }
  add_action( "gld_banner", "gld_banner", 0 );

	/**
	 * Get "kelebihan ladara emas" content
	 * 
	 * @return $return[]
	 * @author Amy <laksmise@gmail.com>
	 */
  function gld_landing_page() {
    global $wpdb;
    
    $post = $wpdb->get_row("SELECT ID, post_name  FROM ldr_posts
      WHERE post_name='emas'", OBJECT);

    $postID = $post->ID;

    $advs = get_post_meta($postID, "ladara_emas_advantages", TRUE);

    if(!empty($advs)) {
      $advantages = array();
      foreach($advs as $key => $adv) {
        $advantages[$key]["title"] = $adv["ladara_emas_advantages_title"];
        $advantages[$key]["description"] = strip_tags($adv["ladara_emas_advantages_description"]);
        $icon = wp_get_attachment_url($adv["ladara_emas_advantages_icon"]);
        $advantages[$key]["icon"] = $icon;
      }

      $return = array(
        "code"    => 200,
        "status"  => "success",
        "message" => "Success.",
        "data"    => $advantages
      );
    } else {
      $template_directory = get_template_directory_uri();

      $return = array(
        "code"    => 404,
        "status"  => "failed",
        "message" => "Ladara Emas Advantages can not be found. Please use default content.",
        "data"    => [
          [
            "title"       => "Aman dan terpercaya",
            "description" => "PT. Treasury bertanggungjawab sebagai manajer keuangan yang juga diawasi oleh OJK",
            "icon"        => $template_directory . "/library/images/emas/icon-shield.png"
          ],
          [
            "title"       => "Gampang dan untung!",
            "description" => "Beli emas dengan harga murah dan jual dengan keuntungan yang tinggi!",
            "icon"        => $template_directory . "/library/images/emas/icon-money.png"
          ],
          [
            "title"       => "Jual beli emas instan!",
            "description" => "Kamu bisa jual beli emas dimanapun dan kapanpun",
            "icon"        => $template_directory . "/library/images/emas/icon-files-and-folders.png"
          ]
        ]
      );
    }

    return json_encode($return);
  }
  add_action( "gld_landing_page", "gld_landing_page", 0 );

	/**
	 * Create fixed xendit VA per user
   * not used, gonna be deleted soon
	 * 
	 * param example:
	 * $param = array(
	 * 		"external_id"   => "ladara_emas_4814",
	 * 		"bank_code"     => "MANDIRI",
	 * 		"name"          => "Amy"
   *    "no_va_account" => "90081310842277"
	 * );
	 * 
	 * param detail:
	 * external_id    : (string - mandatory)
	 * bank_code      : (string - mandatory)
	 * name           : (string - mandatory)
   * no_va_account  : (string - optional)   va number for fixed va numbber
   *                                        will use random va number from xendit if not send this parameter
	 *
   * @param array $datas
	 * @return $return[]
	 * @author Amy <laksmise@gmail.com>
	 */
  function gld_create_va($datas) {
    // $post = array(
    //   "external_id" => $datas["external_id"],
    //   "bank_code"   => $datas["bank_code"],
    //   "name"        => $datas["name"]
    // );

    // if(isset($datas["no_va_account"])) {
      
    // }

    // $response = VirtualAccounts::create($post);

    // if(isset($response["id"])) {
    //   $return = array(
    //     "code"    => 200,
    //     "status"  => "success",
    //     "message" => "Sukses.",
    //     "data"    => $response
    //   );
    // } else {
    //   $return = array(
    //     "code"    => 500,
    //     "status"  => "failed",
    //     "message" => "Maaf gagal membuat virtual account bank " . $datas["bank_code"] . " untuk user " . $datas["name"] . ".",
    //     "data"    => $post
    //   );
    // }

    // return json_encode($return);
  }
  add_action( "gld_create_va", "gld_create_va", 0 );

	/**
	 * call api bank list xendit atau treasury
	 * 
	 * param example:
	 * $param = array(
	 * 		"type" => "xen"
	 * );
	 * 
	 * param detail:
	 * type : (string - mandatory)  source bank, xen for xendit and trs for treasury
	 *
	 * @return 
	 * @author Amy <laksmise@gmail.com>
	 */
  function gld_bank($datas) {

    if($datas["type"] === 'xen') {

      ddlog("Start get xendit bank list for va");
      
      $response = VirtualAccounts::getVABanks();

      $response = array_filter($response, function($fn) {
        return $fn['code'] !== 'BCA';
      });

      if(!isset($response[0]["name"])) {
        ddlog("End get xendit bank list for va. Failed. \n" . json_encode($response));

        $return = [
          "code"    => 500,
          "status"  => "failed",
          "message" => "Maaf gagal mendapatkan data bank. Silahkan coba lagi beberapa saat lagi.",
          "data"    => []
        ];

        return json_encode($return);
      }
      
      ddlog("End get xendit bank list for va. Success.");

      $return = array(
        "code"    => 200,
        "status"  => "success",
        "message" => "Sukses.",
        "data"    => $response
      );

      return json_encode($return);

    } else if($datas["type"] === 'trs') {

      ddlog("Start get treasury bank list for user bank.");

      $ch = curl_init();
      curl_setopt($ch, CURLOPT_URL, getenv('TREASURY_BASE_URL') . "/bank-list");
      curl_setopt($ch, CURLOPT_TIMEOUT, $GLOBALS["gld_global"]["curl_timeout"]);
      curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

      // if(curl_exec($ch) === false) {
      //   ddlog("End get treasury bank list for user bank. Error curl. \n" . curl_error($ch));

      //   $return = [
      //     "code"    => 500,
      //     "status"  => "failed",
      //     "message" => "Maaf gagal terkoneksi ke server data bank. Silahkan coba lagi beberapa saat lagi.",
      //     "data"    => []
      //   ];

      //   return json_encode($return);
      // }

      $datas = [];
      
      $output = curl_exec($ch);
      curl_getinfo($ch, CURLINFO_EFFECTIVE_URL);
      curl_getinfo($ch, CURLINFO_HTTP_CODE);
      curl_close($ch);
      $json_response = json_decode($output);

      if(!isset($json_response->meta->code)) {
        ddlog("End get treasury bank list for user bank. Failed to connect treasury api. \n" . htmlentities($output));

        $return = [
          "code"    => 500,
          "status"  => "failed",
          "message" => "Maaf terjadi gangguan untuk mendapatkan data bank. Silahkan coba lagi beberapa saat lagi.",
          "data"    => []
        ];
        
        return json_encode($return);
      }

      if($json_response->meta->code !== 200) {
        ddlog("End get treasury bank list for user bank. Error " . $json_response->meta->code . " (" . $json_response->meta->message . ").");

        $return = [
          "code"    => $json_response->meta->code,
          "status"  => "failed",
          "message" => "Maaf gagal mendapatkan data bank. Silahkan coba lagi beberapa saat lagi.",
          "data"    => []
        ];
        
        return json_encode($return);
      }

      ddlog("End get treasury bank list for user bank. Success.");

      $return = array(
        "code"    => $json_response->meta->code,
        "status"  => $json_response->meta->status,
        "message" => $json_response->meta->message
      );

      $bank_list = $json_response->data;

      // sort list bank berdasarkan nama bank secara alphabet A-Z
      if(!empty($bank_list)) {
        $col  = "name";
        $sort = array();
        foreach ($bank_list as $i => $obj) {
          $sort[$i] = $obj->{$col};
        }
        array_multisort($sort, SORT_ASC, $bank_list);
      } else {
        $bank_list = [];
      }

      $return["data"] = $bank_list;

      return json_encode($return);

    }

  }
  add_action( "gld_bank", "gld_bank", 0 );

	/**
	 * Get history transaksi
   * 
	 * param example:
	 * $param = array(
   *    "user_id" => 1,
	 * 		"type"    => "all"
	 * 		"limit"   => 5,
   *    "page"    => 1
	 * );
	 * 
	 * param detail:
	 * user_id  : (string - mandatory)    ladara emas user id
	 * type     : (string - mandatory)    transaction. option are all/sell/buy
	 * limit    : (integer - mandatory)   limit transaction for pagination
	 * page     : (integer - mandatory)   current page for pagination
	 *
   * @param array $datas
	 * @return 
	 * @author Amy <laksmise@gmail.com>
	 */
  function gld_history($datas) {
    global $wpdb;

    if(isset($datas["user_id"])) {
      ddlog("Start get transaction " . $datas["type"] . " emas for user emas id " . $datas["user_id"] . ".");
    } else {
      ddlog("Start get transaction " . $datas["type"] . " emas for dashboard.");
    }

    $query_history = "SELECT 
      id, user_id, type, invoice_order, total_gold, total_price, 
      bank_account, bank_account_name, bank_name, 
      buying_rate, selling_rate, booking_fee, tax, partner_fee, payment_method, 
      xendit_fee, xendit_va, xendit_bank, xendit_url, total_payment, status, due_date, created_date 
    FROM ldr_transaksi_emas WHERE status!='Create Invoice'";

    if(isset($datas["user_id"])) {
      $query_history .= " AND user_id='" . $datas["user_id"] . "'";
    }

    if($datas["type"] !== 'all') {
      $query_history .= " AND type='" . $datas["type"] . "'";
    }

    $query_history .= " ORDER BY id DESC";

    if(isset($datas["limit"])) {
      if(isset($datas["page"])) {
        $query_history .= " LIMIT " . $datas["page"] . ", " . $datas["limit"];
      } else {
        $query_history .= " LIMIT 0, " . $datas["limit"];
      }
    }

    $get_history = $wpdb->get_results($query_history, OBJECT);

    if(isset($datas["user_id"])) {
      ddlog("End get transaction " . $datas["type"] . " emas for user emas id " . $datas["user_id"] . ". Success.");
    } else {
      ddlog("End get transaction " . $datas["type"] . " emas for dashboard. Success.");
    }

    if(isset($datas["user_id"])) {
      ddlog("Start count total transaction " . $datas["type"] . " emas for user emas id " . $datas["user_id"] . ".");
    } else {
      ddlog("Start count total transaction " . $datas["type"] . " emas for dashboard.");
    }

    $total_history_query = "SELECT COUNT(*) as total FROM ldr_transaksi_emas WHERE status!='Create Invoice'";

    if(isset($datas["user_id"])) {
      $total_history_query .= " AND user_id='" . $datas["user_id"] . "'";
    }

    if($datas["type"] !== 'all') {
      $total_history_query .= " AND type='" . $datas["type"] . "'";
    }

    $total_history_query .= " ORDER BY id DESC";

    ddlog($total_history_query);
    
    $total_history = $wpdb->get_row($total_history_query, OBJECT);
    $total_history = (!empty($total_history)) ? $total_history->total : 0;

    if(isset($datas["user_id"])) {
      ddlog("End count total transaction " . $datas["type"] . " emas for user emas id " . $datas["user_id"] . ". Success. Total " . $total_history);
    } else {
      ddlog("End count total transaction " . $datas["type"] . " emas for dashboard. Success. Total " . $total_history);
    }    

    $return = array(
      "code"    => 200,
      "status"  => "success",
      "message" => "Sukses.",
      "data"    => [
        "total"   => $total_history,
        "history" => $get_history
      ]
    );

    return json_encode($return);
  }
  add_action( "gld_history", "gld_history", 0 );

  /**
   * Get detail history transaksi
   * 
   * param example:
   * $param = array(
   *    "user_id"     => 80,
   * 		"invoice_no"  => "INV/EMAS/20200803/155"
   * );
   * 
   * param detail:
   * user_id    : (string - mandatory)    ladara emas user id
   * invoice_no : (string - mandatory)    Invoice no
   *
   * @param array $datas
   * @return 
   * @author Amy <laksmise@gmail.com>
   */
  function gld_detail_history($datas) {
    global $wpdb;

    if(!isset($datas["user_id"])) {
      $user_id = get_current_user_id();
    } else {
      $user_id = $datas["user_id"];
    }

    // $profile = gld_profile_emas(array("user_id"=>$user_id));
    // $profile = json_decode($profile);
    // $user_id = $profile->data->id;

    $user = $wpdb->get_row(
      "SELECT id FROM ldr_user_emas WHERE user_id=".$user_id."",
      OBJECT
    );
    
    $history = $wpdb->get_row(
      "SELECT * FROM ldr_transaksi_emas WHERE invoice_order='".$datas["invoice_no"]."' AND user_id=".$user->id." AND status!='Create Invoice'",
      OBJECT
    );

    $return = array(
      "code"    => 200,
      "status"  => "success",
      "message" => "Sukses.",
      "data"    => $history
    );

    return json_encode($return);
  }
  add_action( "gld_detail_history", "gld_detail_history", 0 );

	/**
	 * Treasury detail history
   * 
	 * param example:
	 * $param = array(
   *    "type_history"  => "buy,
   *    "invoice_no"    => "LDREMAS200422084049109"
	 * );
	 * 
	 * param detail:
	 * type_history : (string - mandatory)  type transaction history, buy or sell
	 * invoice_no   : (string - mandatory)  no invoice trs
   * 
	 * @return 
	 * @author Amy <laksmise@gmail.com>
	 */
  function gld_trs_detail_history($datas) {
    global $wpdb;

    if(!isset($datas["user_id"])) {
      $user_id = get_current_user_id();
    } else {
      $user_id = $datas["user_id"];
    }

    if(!isset($_SESSION["customer_token"])) {
      $login_trs = gld_login(array("user_id"=>$user_id));
      $login_trs = json_decode($login_trs);

      $customer_token = $login_trs->data->access_token;
    } else {
      $customer_token = $_SESSION["customer_token"];
    }

    ddlog("Start get treasury detail transaction for invoice " . $datas["invoice_no"] . ".");

    $authorization = "Authorization: Bearer " . $customer_token;
    $curl_header = array();
    $curl_header[] = $authorization;

    $post = [
      "type_history"  => $datas["type_history"],
      "invoice_no"    => $datas["invoice_no"],
    ];

    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, getenv('TREASURY_BASE_URL') . "/detail-history");
    curl_setopt($ch, CURLOPT_TIMEOUT, $GLOBALS["gld_global"]["curl_timeout"]);
    curl_setopt($ch, CURLOPT_HTTPHEADER, $curl_header);
    curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
    curl_setopt($ch, CURLOPT_HTTP_VERSION, CURL_HTTP_VERSION_1_1);
    curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_ENCODING, "");
    curl_setopt($ch, CURLOPT_MAXREDIRS, $GLOBALS["gld_global"]["curl_maxredirs"]);
    curl_setopt($ch, CURLOPT_POSTFIELDS, $post);
    
    $output = curl_exec($ch);
    curl_getinfo($ch, CURLINFO_EFFECTIVE_URL);
    curl_getinfo($ch, CURLINFO_HTTP_CODE);
    curl_close($ch);
    $json_response = json_decode($output);

    if(!isset($json_response->meta->code)) {
      ddlog("End get treasury detail transaction for invoice " . $datas["invoice_no"] . ". Failed to connect treasury api. \n" . htmlentities($output));

      $return = [
        "code"    => 500,
        "status"  => "failed",
        "message" => "Maaf terjadi gangguan untuk mendapatkan detail transaksi. Silahkan coba lagi beberapa saat lagi.",
        "data"    => []
      ];
      
      return json_encode($return);
    }

    if($json_response->meta->code !== 200) {
      ddlog("End get treasury detail transaction for invoice " . $datas["invoice_no"] . ". Error " . $json_response->meta->code . " (" . $json_response->meta->message . ").");

      $return = [
        "code"    => $json_response->meta->code,
        "status"  => "failed",
        "message" => "Maaf gagal mendapatkan detail transaksi. Silahkan coba lagi beberapa saat lagi.",
        "data"    => []
      ];
      
      return json_encode($return);
    }

    ddlog("End get treasury detail transaction for invoice " . $datas["invoice_no"] . ". Success.");
    
    $return = array(
      "code"    => 200,
      "status"  => "success",
      "message" => "Sukses.",
      "data"    => $json_response->data
    );

    return json_encode($return);
  }
  add_action( "gld_trs_detail_history", "gld_trs_detail_history", 0 );

  /**
   * retrieve virtual account no
   * not used, gonna be deleted soon
   *
   * @param [type] $user_va_id
   * @return void
   */
  function gld_retrieve_va($user_va_id) {
    // $output = VirtualAccounts::retrieve($user_va_id);

    // if(isset($output["status"])) {
    //   $response = array(
    //     "code"    => 200,
    //     "status"  => "success",
    //     "message" => "Sukses.",
    //     "data"    => $response
    //   );
    // } else {
    //   $response = array(
    //     "code"    => 404,
    //     "status"  => "failed",
    //     "message" => "Virtual Account Xendit dengan ID " . $user_va_id . " tidak ditemukan.",
    //     "data"    => []
    //   );
    // }

    // return $response;
  }
  add_action( "gld_retrieve_va", "gld_retrieve_va", 0 );

  /**
   * retrieve payment
   *
   * @param [type] $transaksi_id
   * @param [type] $user_id
   * @return void
   */
  function gld_retrieve_payment($transaksi_id, $user_id) {
    global $wpdb;

    $history = $wpdb->get_row(
        "SELECT id, user_id, type, invoice_order, total_gold, total_price, xendit_id, xendit_bank, 
          buying_rate, selling_rate, booking_fee, tax, partner_fee, payment_method, total_payment, status 
        FROM ldr_transaksi_emas WHERE id=".$transaksi_id." AND status!='Create Invoice'",
        OBJECT
      );
    
    if(empty($history)) {
      $return = array(
        "code"    => 404,
        "status"  => "failed",
        "message" => "History not found.",
        "data"    => []
      );

      return json_encode($return);
    }
    
    $payment_method = strtolower($history->payment_method);
    $invoiceStatus = 0;
    $invoice = [];

    if ($payment_method === 'kartu kredit') {
      $invoice = Invoice::retrieve($history->xendit_id);
    } elseif ($payment_method === 'ovo' || $payment_method === 'dana' || $payment_method === 'linkaja') {
      $invoice = EWallets::getPaymentStatus($history->xendit_id, strtoupper($history->xendit_bank));
    } else { //virtual account
      $invoice = Invoice::retrieve($history->xendit_id);
    }    

    if ($invoice['status'] === 'PAID' || $invoice['status'] === 'SETTLED' || $invoice['status'] === 'SUCCESS_COMPLETED' || $invoice['status'] === 'COMPLETED') {
      $invoiceStatus = 1;
    } elseif ($invoice['status'] === 'PENDING') {
      $invoiceStatus = 0;
    } else {
      $invoiceStatus = -1;
    }

    $return = array(
      "code"    => 200,
      "status"  => "success",
      "message" => "Sukses.",
      "data"    => [
        "status"  => $invoiceStatus,
        "invoice" => $invoice,
        "history" => $history
      ]
    );

    return json_encode($return);
  }
  add_action( "gld_retrieve_payment", "gld_retrieve_payment", 0 );

  /**
   * Change 0 number into 'Juta' or 'Miliar' text
   *
   * @return void
   */
  function gld_terbilang($nominal, $is_short = false, $add_space = false) {
    $len = strlen($nominal);
    if ($len >= 10) {
        $bilangan = "Milyar";

        if ($is_short === true) {
          $bilangan = "m";
        }
    } elseif ($len >= 7 && $len <= 9) {
        $bilangan = "Juta";

        if ($is_short === true) {
          $bilangan = "jt";
        }
    } else {
      // if ($len >= 5 && $len <= 6) {
      //   $bilangan = "Ratus ribu";

      //   if ($is_short === true) {
      //     $bilangan = "Ratus Rb";
      //   }
      // } else {
        $bilangan = "Ribu";

        if ($is_short === true) {
          $bilangan = "rb";
        }
      // }
    }
    
    $angka = number_format($nominal, 0, ".", ".");
    $angka = explode('.', $angka);
    
    if ($len >= 10) {
      return $angka[0] . "," . substr($angka[1], 0, -1) . "" . $bilangan;
    } else {
      return $angka[0] . "" . $bilangan;
    }
  }
  add_action('gld_terbilang', 'gld_terbilang', 0);
/******************************** FUNCTION WEBSITE ********************************/


/*************************** FUNCTION HIT API TREASURY ***************************/
  function trs_history($datas) {
    if(!isset($datas["user_id"])) {
      $user_id = get_current_user_id();
    } else {
      $user_id = $datas["user_id"];
    }

    gld_refresh_session(array("user_id"=>$user_id));

    if(isset($_SESSION["customer_token"])) {
      $authorization = "Authorization: Bearer " . $_SESSION["customer_token"];
      $curl_header = array();
      $curl_header[] = $authorization;
      
      $body = [
        "type" => $datas["history_type"]
      ];

      $ch = curl_init();
      curl_setopt($ch, CURLOPT_URL, getenv('TREASURY_BASE_URL') . "/history");
      curl_setopt($ch, CURLOPT_TIMEOUT, $GLOBALS["gld_global"]["curl_timeout"]);
      curl_setopt($ch, CURLOPT_HTTPHEADER, $curl_header);
      curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
      curl_setopt($ch, CURLOPT_HTTP_VERSION, CURL_HTTP_VERSION_1_1);
      curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
      curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
      curl_setopt($ch, CURLOPT_ENCODING, "");
      curl_setopt($ch, CURLOPT_MAXREDIRS, $GLOBALS["gld_global"]["curl_maxredirs"]);
      curl_setopt($ch, CURLOPT_POSTFIELDS, $body);

      if(curl_exec($ch) === false) {
        $body["curl_error"] = curl_error($ch);
        $return = [
          "code"    => 500,
          "status"  => "failed",
          "message" => "Maaf terjadi gangguan untuk mendapatkan riwayat transaksi. Silahkan coba lagi beberapa saat lagi.",
          "data"    => $body
        ];
      } else {
        $output = curl_exec($ch);
        curl_getinfo($ch, CURLINFO_EFFECTIVE_URL);
        curl_getinfo($ch, CURLINFO_HTTP_CODE);
        curl_close($ch);
        $json_response = json_decode($output);

        if(isset($json_response->meta->code)) {
          if($json_response->meta->code !== 200) {
            $body["trs_message"] = $json_response->meta->message;
            $return = [
              "code"    => $json_response->meta->code,
              "status"  => "failed",
              "message" => "Gagal mendapatkan riwayat transaksi. Silahkan coba lagi beberapa saat lagi.",
              "data"    => $body
            ];
          } else {
            $return = [
              "code"    => 200,
              "status"  => "success",
              "message" => "Sukses.",
              "data"    => $json_response->data
            ];
          }
        } else {
          $body["html"] = htmlentities($output);
          $return = [
            "code"    => 500,
            "status"  => "failed",
            "message" => "Maaf terjadi gangguan untuk mendapatkan riwayat transaksi. Silahkan coba lagi beberapa saat lagi.",
            "data"    => $body
          ];
        }
      }
    } else {
      $return = [
        "code"    => 400,
        "status"  => "failed",
        "message" => "Invalid customer token.",
        "data"    => []
      ];
    }

    return $return;
  }
/*************************** FUNCTION HIT API TREASURY ***************************/


/******************************** AJAX FUNCTION ********************************/  
	/**
	 * Ajax calculate value jual/beli rupiah ke emas atau emas ke rupiah
	 *
	 * @return  void
	 * @author Amy <laksmise@gmail.com>
	 */
	function ajax_calculate_emas() {
    $datas = array(
      "user_id"           => get_current_user_id(),
      "transaction_type"  => $_POST["typeCalculate"],
      "amount_type"       => $_POST["valueType"],
    );
    
    $value_gram = str_replace(",", ".", $_POST["emasGrValue"]);
    $value_currency = str_replace(".", "", $_POST["emasRpValue"]);

    if($_POST["valueType"] == "currency") {
      $datas["amount"] = $value_currency;
    }
    if($_POST["valueType"] == "gold") {
      $datas["amount"] = $value_gram ;
    }

    $calculate = gld_calculate($datas);
    $calculate = json_decode($calculate);

    if($calculate->code === 200) {
      $return = array(
        "code"    => 200,
        "status"  => "success",
        "message" => "Sukses.",
        "data"    => $calculate->data
      );
    } else {
      $return = array(
        "code"    => $calculate->code,
        "status"  => $calculate->status,
        "message" => $calculate->message,
        "data"    => $calculate->data
      );
    }

		echo json_encode($return);
		die();
	}
	add_action("wp_ajax_ajax_calculate_emas","ajax_calculate_emas");
  add_action("wp_ajax_nopriv_ajax_calculate_emas", "ajax_calculate_emas");
  
	/**
	 * Ajax register akun ladara emas
	 *
	 * @return  void
	 * @author Amy <laksmise@gmail.com>
	 */
	function ajax_gld_register() {
    global $wpdb;

    $param_user = [
      "user_id" => get_current_user_id()
    ];

    $get_user = gld_user_profile($param_user);
    $get_user = json_decode($get_user);

    if($get_user->code === 200) {
      if(empty($_POST["bank"])) {
        $validation["bank"] = "Bank harus dipilih";
      } else {
        if(ctype_alnum($_POST["bank"]) == FALSE) {
          $validation["bank"] = "Bank hanya boleh alfabet";
        }
      }
      if(empty($_POST["norek"])) {
        $validation["norek"] = "Nomor Rekening tidak boleh kosong";
      } else {
        if(is_numeric($_POST["norek"]) == FALSE) {
          $validation["norek"] = "Nomor Rekening hanya boleh numeric";
        }
      }
      if(empty($_POST["owner"])) {
        $validation["owner"] = "Nama Pemilik Rekening tidak boleh kosong";
      } else {
        if(preg_match('/^[a-z0-9 ]+$/i', $_POST["owner"]) !== 1) {
          $validation["owner"] = "Nama Pemilik Rekening hanya boleh alfabet";
        } else {
          if(strtolower($get_user->data->name) !== strtolower($_POST["owner"])) {
            $validation["owner"] = "Nama Pemilik Rekening tidak sesuai dengan nama kamu";
          }
        }
      }
      /*if(empty($_POST["branch"])) {
        $validation["branch"] = "Branch Bank tidak boleh kosong";
      } else {
        if(preg_match('/^[a-z0-9 ]+$/i', $_POST["branch"]) !== 1) {
          $validation["branch"] = "Branch Bank hanya boleh alfabet";
        }
      }*/
      if(!empty($_POST["branch"])) {
        if(preg_match('/^[a-z0-9 ]+$/i', $_POST["branch"]) !== 1) {
          $validation["branch"] = "Branch Bank hanya boleh alfabet";
        }
      }

      if(!empty($validation)) {
        $return = array(
          "code"    => 400,
          "status"  => "failed",
          "message" => "Validation",
          "data"    => $validation
        );
      } else {
        $return = array(
          "code"    => 200,
          "status"  => "success",
          "message" => "Sukses validasi",
          "data"    => []
        );
      }
    } else {
      $return = array(
        "code"    => 404,
        "status"  => "failed",
        "message" => "User tidak terdaftar",
        "data"    => []
      );
    }
    
		echo json_encode($return);
		die();
	}
	add_action("wp_ajax_ajax_gld_register","ajax_gld_register");
  add_action("wp_ajax_nopriv_ajax_gld_register", "ajax_gld_register");
  
	/**
	 * Ajax register akun ladara emas
	 *
	 * @return  void
	 * @author  Amy <laksmise@gmail.com>
	 */
	function ajax_gld_load_history() {
    global $wpdb;

    $profile = gld_profile_emas(array("user_id"=>get_current_user_id()));
    $profile = json_decode($profile);
    $user_id = $profile->data->id;

    $page = ($_POST["page"] - 1) * $_POST["per_page"];

    $type = $_POST["type"];
    if($_POST["type"] === "mall") {
      $type = "all";
    } else if($_POST["type"] === "mbuy") {
      $type = "buy";
    } else if($_POST["type"] === "msell") {
      $type = "sell";
    }

    $history_emas = gld_history(array("type" => $type, "user_id" => $user_id, "limit" => $_POST["per_page"], "page" => $page));
    $history_emas = json_decode($history_emas);

    $template_directory = get_template_directory_uri();
    
    if($history_emas->code === 200 && !empty($history_emas->data->history)) {
      $html = "";
      //malas check dia ada huruf "m" atau g :P
      if($_POST["type"] === "mall" || $_POST["type"] === "mbuy" || $_POST["type"] === "msell") {
        foreach($history_emas->data->history as $x => $h) {
          $html .= "<div class=\"col_historyEmas\">";
            $html .= "<div class=\"col_heStatus\">";
              $html .= "<div>";
                $html .= "<img src=\"" . $template_directory . "/library/images/emas/icon-gold-history.svg\" alt=\"icon\" />";
                $html .= "<p>Emas</p>";
              $html .= "</div>";

              $html .= "<p class=\"tgl\">";
                $html .= "Tanggal Transaksi";
                $html .= "<br />";
                $html .= "<strong>";
                  $html .= date("j", strtotime($h->created_date)) . " " . month_indonesia(date("n", strtotime($h->created_date))) . " " . date("Y", strtotime($h->created_date)) . " " . date("H:i", strtotime($h->created_date)) . " WIB";
                $html .= "</strong>";
              $html .= "</p>";
            $html .= "</div>";

            $html .= "<div class=\"col_heDetail\">";
              $html .= "<div class=\"box_hePembayaran\">";
                $html .= "<div>";
                  if($h->type === 'buy') {
                    $html .= "<p>Nominal pembelian</p>";
                  }
                  if($h->type === 'sell') {
                    $html .= "<p>Nominal penjualan</p>";
                  }
                  $html .= "<p class=\"amount\">";
                    $html .= "Rp " . number_format($h->total_payment, 0, ".", ".");
                  $html .= "</p>";
                $html .= "</div>";

                $html .= "<div>";
                  if($h->type === 'buy') {
                    $html .= "<p>Metode pembayaran</p>";
                    $html .= "<p>" . $h->payment_method . "</p>";
                  }
                  if($h->type === 'sell') {
                    $html .= "<p>Dana Ditransfer ke</p>";
                    $html .= "<p>";
                      $html .= $h->bank_name;
                      $html .= "<br />";
                      $html .= $h->bank_account;
                      $html .= "<br />";
                      $html .= "<span>a.n " . $h->bank_account_name . "</span>";
                    $html .= "</p>";
                  }
                $html .= "</div>";

                $html .= "<div>";
                  $html .= "<p>Invoice</p>";
                  $html .= "<p>";
                    $html .= "(" . $h->invoice_order . ")";
                  $html .= "</p>";
                $html .= "</div>";

                $html .= "<div>";
                  $html .= "<p>Status Transaksi</p>";
                  if($h->type === 'buy' && strtolower($h->status) === "success") {
                    $html .= "<p class=\"success\">Pembelian Berhasil</p>";
                  }
                  if($h->type === 'buy' && strtolower($h->status) === "pending") {
                    $html .= "<p class=\"pending\">Pembelian Menunggu Pembayaran</p>";
                  }
                  if($h->type === 'buy' && strtolower($h->status) === "canceled") {
                    $html .= "<p class=\"failed\">Pembelian Telah Dibatalkan</p>";
                  }
                  if($h->type === 'buy' && strtolower($h->status) === "failed") {
                    $html .= "<p class=\"failed\">Pembelian Telah Gagal</p>";
                  }
                  if($h->type === 'sell' && strtolower($h->status) === "pending") {
                    $html .= "<p class=\"pending\">Penjualan Sedang Diproses</p>";
                  }
                  if($h->type === 'sell' && strtolower($h->status) === "approved") {
                    $html .= "<p class=\"success\">Penjualan Telah Disetujui</p>";
                  }
                  if($h->type === 'sell' && strtolower($h->status) === "rejected") {
                    $html .= "<p class=\"failed\">Penjualan Telah Ditolak</p>";
                  }
                $html .= "</div>";
              $html .= "</div>";

              if(strtolower($h->status) === "success") {
                $html .= "<div class=\"jmlh_emas\">";
                  if($h->type === 'buy') {
                    $html .= "Emas kamu telah masuk sebanyak";
                    $html .= "<strong>" . str_replace(".", ",", $h->total_gold) . "</strong>gr";
                    $html .= "ke Akun kamu";
                  }
                  if($h->type === 'sell') {
                    $html .= "Emas kamu telah terpotong sebanyak";
                    $html .= "<strong>" . str_replace(".", ",", $h->total_gold) . "</strong>gr";
                    $html .= "dari Akun kamu";
                  }
                $html .= "</div>";
              }

              $html .= "<div class=\"col_heDetailTransaksi\">";
                $html .= "<div class=\"title mtransaksi_semua_" . $x . "\">";
                  $html .= "<div>";
                    $html .= "<p>";
                      if($h->type === 'buy') {
                        $html .= "<a href=\"" . home_url() . "/emas/transaksi-detail?invoice=" . $h->invoice_order . "\">";
                          $html .= "Lihat Invoice";
                        $html .= "</a>";
                      }
                      if($h->type === 'sell') {
                        $html .= "<a href=\"" . home_url() . "/emas/transaksi-detail?faktur=" . $h->invoice_order . "\">";
                          $html .= "Lihat Invoice";
                        $html .= "</a>";
                      }
                    $html .= "</p>";
                  $html .= "</div>";
                  $html .= "<span onclick=\"show_detailHistoryEmas('mtransaksi_semua_" . $x . "')\">";
                    $html .= "Detail Transaksi";
                  $html .= "</span>";
                $html .= "</div>";

                $html .= "<div class=\"jmlh mtransaksi_semua_" . $x . "\">";
                  $html .= "<div>";
                    $html .= "<p>Jumlah Emas</p>";
                    $html .= "<p>" . str_replace(".", ",", $h->total_gold) . " gram</p>";
                  $html .= "</div>";
                  $html .= "<div>";
                    if($h->type === 'buy') {
                      $html .= "<p>Harga Emas Saat Transaksi</p>";
                      $html .= "<p>Rp " . number_format($h->buying_rate, 0, ".", ".") . "/gr</p>";
                    }
                    if($h->type === 'sell') {
                      $html .= "<p>Harga Emas Saat Transaksi</p>";
                      $html .= "<p>Rp " . number_format($h->selling_rate, 0, ".", ".") . "/gr</p>";
                    }
                  $html .= "</div>";
                $html .= "</div>";
              $html .= "</div>";
            $html .= "</div>";
          $html .= "</div>";
        }
      } else {
        foreach($history_emas->data->history as $x => $h) {
          $html .= "<div class=\"col_historyEmas\">";
            $html .= "<div class=\"col_heStatus\">";
              $html .= "<img src=\"" . $template_directory . "/library/images/emas/icon-gold-history.svg\" alt=\"icon\" />";
            
              $html .= "<div>";
                $html .= "<p>Emas</p>";
                /*<p>Transaksi Pembelian #10010514</p>*/
                $html .= "<p>Status Transaksi</p>";
                if($h->type === 'buy' && strtolower($h->status) === "success") {
                  $html .= "<p class=\"success\">Pembelian Berhasil</p>";
                }
                if($h->type === 'buy' && strtolower($h->status) === "pending") {
                  $html .= "<p class=\"pending\">Pembelian Menunggu Pembayaran</p>";
                }
                if($h->type === 'buy' && strtolower($h->status) === "canceled") {
                  $html .= "<p class=\"failed\">Pembelian Telah Dibatalkan</p>";
                }
                if($h->type === 'buy' && strtolower($h->status) === "failed") {
                  $html .= "<p class=\"failed\">Pembelian Telah Gagal</p>";
                }
                if($h->type === 'sell' && strtolower($h->status) === "pending") {
                  $html .= "<p class=\"pending\">Penjualan Sedang Diproses</p>";
                }
                if($h->type === 'sell' && strtolower($h->status) === "approved") {
                  $html .= "<p class=\"success\">Penjualan Telah Disetujui</p>";
                }
                if($h->type === 'sell' && strtolower($h->status) === "rejected") {
                  $html .= "<p class=\"failed\">Penjualan Telah Ditolak</p>";
                }
              $html .= "</div>";
            $html .= "</div>";

            $html .= "<div class=\"col_heDetail\">";
              $html .= "<p class=\"tgl\">";
                $html .= "Tanggal Transaksi: ";
                $html .= "<strong>";
                  $html .= date("j", strtotime($h->created_date)) . " ";
                  $html .= month_indonesia(date("n", strtotime($h->created_date))) . " ";
                  $html .= date("Y", strtotime($h->created_date)) . " ";
                  $html .= date("H:i", strtotime($h->created_date)) . " WIB";
                $html .= "</strong>";
              $html .= "</p>";

              $html .= "<div class=\"box_hePembayaran\">";
                $html .= "<div>";
                  if($h->type === 'buy') {
                    $html .= "<p>Nominal pembelian</p>";
                  }
                  if($h->type === 'sell') {
                    $html .= "<p>Nominal penjualan</p>";
                  }
                  $html .= "<p class=\"amount\">";
                    $html .= "Rp " . number_format($h->total_payment, 0, ".", ".");
                  $html .= "</p>";
                $html .= "</div>";

                $html .= "<div>";
                  if($h->type === 'buy') {
                    $html .= "<p>Metode pembayaran</p>";
                    $html .= "<p>" . $h->payment_method . "</p>";
                  }
                  if($h->type === 'sell') {
                    $html .= "<p>Dana Ditransfer ke</p>";
                    $html .= "<p>";
                      $html .= $h->bank_name . " - " . $h->bank_account;
                      $html .= "<br />";
                      $html .= "<span>a.n" . $h->bank_account_name . "</span>";
                    $html .= "</p>";
                  }
                $html .= "</div>";

                $html .= "<div>";
                  $html .= "<p>Invoice</p>";
                  $html .= "<p>";
                    $html .= "(" . $h->invoice_order . ")";
                  $html .= "</p>";
                $html .= "</div>";
              $html .= "</div>";

              if(strtolower($h->status) === "success") {
                $html .= "<div class=\"jmlh_emas\">";
                  if($h->type === 'buy') {
                    $html .= "Emas kamu telah masuk sebanyak ";
                    $html .= "<strong>" . str_replace(".", ",", $h->total_gold) . "</strong>gr ";
                    $html .= "ke Akun kamu";
                  }
                  if($h->type === 'sell') {
                    $html .= "Emas kamu telah terpotong sebanyak ";
                    $html .= "<strong>" . str_replace(".", ",", $h->total_gold) . "</strong>gr ";
                    $html .= "dari Akun kamu";
                  }
                $html .= "</div>";
              }

              $html .= "<div class=\"col_heDetailTransaksi\">";
                $html .= "<div class=\"title transaksi_semua_" . $x . "\">";
                  if($h->type === 'buy') {
                    $html .= "<a href=\"" . home_url() . "/emas/transaksi-detail?invoice=" . $h->invoice_order . "\">";
                      $html .= "Lihat Invoice";
                    $html .= "</a>";
                  }
                  if($h->type === 'sell') {
                    $html .= "<a href=\"" . home_url() . "/emas/transaksi-detail?faktur=" . $h->invoice_order . "\">";
                      $html .= "Lihat Invoice";
                    $html .= "</a>";
                  }
                  $html .= "<span onclick=\"show_detailHistoryEmas('transaksi_semua_" . $x . "')\">";
                    $html .= "Detail Transaksi";
                  $html .= "</span>";
                $html .= "</div>";

                $html .= "<div class=\"jmlh transaksi_semua_" . $x . "\">";
                  $html .= "<div>";
                    $html .= "<p>Jumlah Emas</p>";
                    $html .= "<p>" . str_replace(".", ",", $h->total_gold) . " gram</p>";
                  $html .= "</div>";
                  $html .= "<div>";
                    if($h->type === "buy") {
                      $html .= "<p>Harga Emas Saat Transaksi</p>";
                      $html .= "<p>Rp " . number_format($h->buying_rate, 0, ".", ".") . "/gr</p>";
                    }
                    if($h->type === "sell") {
                      $html .= "<p>Harga Emas Saat Transaksi</p>";
                      $html .= "<p>Rp " . number_format($h->selling_rate, 0, ".", ".") . "/gr</p>";
                    }
                  $html .= "</div>";
                $html .= "</div>";
              $html .= "</div>";
            $html .= "</div>";
          $html .= "</div>";
        }
      }

      if(!empty($html)) {
        $return = array(
          "code"  => 200,
          "html"  => $html
        );
      } else {
        $return = array(
          "code"  => 404,
          "html"  => ""
        );
      }
    } else {
      $return = array(
        "code"  => $history_emas->code,
        "html"  => ""
      );
    }

    echo json_encode($return);
    die();
	}
	add_action("wp_ajax_ajax_gld_load_history","ajax_gld_load_history");
  add_action("wp_ajax_nopriv_ajax_gld_load_history", "ajax_gld_load_history");

	/**
	 * Ajax refresh rate emas
	 *
	 * @return  void
	 * @author  Amy <laksmise@gmail.com>
	 */
  function ajax_gld_rate() {
    $harga_emas = gld_rate(array("user_id"=>get_current_user_id()));    
		echo $harga_emas;
		die();
  }
	add_action("wp_ajax_ajax_gld_rate","ajax_gld_rate");
  add_action("wp_ajax_nopriv_ajax_gld_rate", "ajax_gld_rate");

	/**
	 * Ajax refresh dan update balance
	 *
	 * @return  void
	 * @author  Amy <laksmise@gmail.com>
	 */
  function ajax_gld_balance() {
    global $wpdb;

    $trs_profile = gld_trs_profile(array("user_id"=>get_current_user_id()));
    $trs_profile = json_decode($trs_profile);

    $data_user = gld_profile_emas(array("user_id"=>get_current_user_id()));
    $data_user = json_decode($data_user);

    if ($trs_profile->code === 200) {
      $gold_balance = $trs_profile->data->gold_balance;
      $gold_balance_in_currency = $trs_profile->data->gold_balance_in_currency;

      if(!empty($gold_balance_in_currency) || $gold_balance_in_currency != 0) {
        $query_update_user_emas = "UPDATE ldr_user_emas SET 
            gold_balance_in_currency = '" . $gold_balance_in_currency . "' 
          WHERE id = " . $data_user->data->id;
        $wpdb->query($query_update_user_emas);
      }
    }
      
    $return = [
      "code"    => 200,
      "status"  => "success",
      "message" => "Sukses.",
      "data"    => [
        "balance_gram"      => $data_user->data->gold_balance,
        "balance_currency"  => $data_user->data->gold_balance_in_currency
      ]
    ];

		echo json_encode($return);
		die();
  }
	add_action("wp_ajax_ajax_gld_balance","ajax_gld_balance");
  add_action("wp_ajax_nopriv_ajax_gld_balance", "ajax_gld_balance");
  
	/**
	 * Ajax register akun ladara emas
	 *
	 * @return  void
	 * @author Amy <laksmise@gmail.com>
	 */
	function ajax_linking_treasury() {
    global $wpdb;

    $user_id = get_current_user_id();

    $validation = [];

    if(empty($_POST["email"])) {
      $validation["email"] = "Email Treasury tidak boleh kosong.";
    } else {
      $check_email_format = gld_check_email_format($_POST["email"]);
      if($check_email_format["code"] !== 200) {
        $validation["email"] = $check_email_format["message"];
      }
    }

    if(empty($_POST["password"])) {
      $validation["password"] = "Password Treasury tidak boleh kosong.";
    }

    if(!empty($validation)) {
      $return = array(
        "code"    => 400,
        "status"  => "failed",
        "message" => "Validation",
        "data"    => $validation
      );

      echo json_encode($return);
      die();
    }

    $email = $_POST["email"];
    $password = $_POST["password"];
    $bank = "";//$_POST["bank"];
    $norek = "";//$_POST["norek"];
    $owner = "";//$_POST["owner"];
    $branch = "";//$_POST["branch"];

    $user_profile = gld_user_profile(array("user_id"=>$user_id));
    $user_profile = json_decode($user_profile);

    if($user_profile->code !== 200) {
      $return = array(
        "code"    => $user_profile->code,
        "status"  => $user_profile->status,
        "message" => $user_profile->message,
        "data"    => []
      );

      echo json_encode($return);
      die();
    }

    ddlog("Start login to treasury for user " . $email . " for checking user existance on treasury.");

    $body = [
      "client_id"			=> getenv('TREASURY_CLIENT_ID'),
      "client_secret" => getenv('TREASURY_CLIENT_SECRET'),
      "grant_type"    => getenv('TREASURY_GRANT_CUSTOMER'),
      "email"         => $email,
      "password"      => $password
    ];

    $curl_header = array();
    $curl_header[] = "Accept: application/json";

    $ch = curl_init();

    curl_setopt_array($ch, array(
      CURLOPT_URL => getenv('TREASURY_BASE_URL') . "/login",
      CURLOPT_RETURNTRANSFER => true,
      CURLOPT_ENCODING => "",
      CURLOPT_MAXREDIRS => $GLOBALS["gld_global"]["curl_maxredirs"],
      CURLOPT_TIMEOUT => $GLOBALS["gld_global"]["curl_timeout"],
      CURLOPT_FOLLOWLOCATION => true,
      CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
      CURLOPT_CUSTOMREQUEST => "POST",
      CURLOPT_POSTFIELDS => $body,
      CURLOPT_HTTPHEADER => array(
        "Accept: application/json",
      ),
    ));
    
    $output_login = curl_exec($ch);
    curl_getinfo($ch, CURLINFO_EFFECTIVE_URL);
    curl_getinfo($ch, CURLINFO_HTTP_CODE);
    curl_close($ch);
    $json_login = json_decode($output_login);

    if(!isset($json_login->meta->code)) {
      ddlog("End login to treasury for user " . $email . " for checking user existance on treasury. Failed to connect treasury api. \n" . htmlentities($output_login));

      $return = array(
        "code"    => 500,
        "status"  => "failed",
        "message" => "Maaf terjadi gangguan untuk cek user Treasury. Silahkan coba lagi beberapa saat lagi.",
        "data"    => $validation
      );

      echo json_encode($return);
      die();
    }

    if($json_login->meta->code !== 200) {
      ddlog("End login to treasury for user " . $email . " for checking user existance on treasury. Error " . $json_login->meta->code . " (" . $json_login->meta->message . ").");

      $return = [
        "code"    => $json_login->meta->code,
        "status"  => "failed",
        "message" => "Maaf gagal untuk cek user Treasury, " . $json_login->meta->message . ". Silahkan coba lagi beberapa saat lagi.",
        "data"    => []
      ];

      echo json_encode($return);
      die();
    }

    ddlog("End login to treasury for user " . $email . " for checking user existance on treasury. Success.");
    
    ddlog("Start get treasury profile with email " . $email . " for user existance on treasury.");

    $authorization = "Authorization: Bearer " . $json_login->data->access_token;
    $curl_header = array();
    $curl_header[] = $authorization;

    $curl = curl_init();
    curl_setopt($curl, CURLOPT_URL, getenv('TREASURY_BASE_URL') . "/profile");
    curl_setopt($curl, CURLOPT_TIMEOUT, $GLOBALS["gld_global"]["curl_timeout"]);
    curl_setopt($curl, CURLOPT_FOLLOWLOCATION, true);
    curl_setopt($curl, CURLOPT_HTTP_VERSION, CURL_HTTP_VERSION_1_1);
    curl_setopt($curl, CURLOPT_CUSTOMREQUEST, "GET");
    curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($curl, CURLOPT_ENCODING, "");
    curl_setopt($curl, CURLOPT_MAXREDIRS, $GLOBALS["gld_global"]["curl_maxredirs"]);
    curl_setopt($curl, CURLOPT_HTTPHEADER, $curl_header);

    $output_trs_profile = curl_exec($curl);
    curl_getinfo($curl, CURLINFO_EFFECTIVE_URL);
    curl_getinfo($curl, CURLINFO_HTTP_CODE);
    curl_close($curl);
    $json_trs_profile = json_decode($output_trs_profile);

    if(!isset($json_trs_profile->meta->code)) {
      ddlog("End get treasury profile with email " . $email . " for user existance on treasury. Failed to connect treasury api. \n" . htmlentities($output_trs_profile));

      $return = [
        "code"    => 500,
        "status"  => "failed",
        "message" => "Maaf terjadi gangguan untuk mendapatkan profile Treasury. Silahkan coba lagi beberapa saat lagi.",
        "data"    => []
      ];

      echo json_encode($return);
      die();
    }

    if($json_trs_profile->meta->code !== 200) {
      // ddlog(json_encode($json_trs_profile));

      ddlog("End login to treasury for user " . $email . " for checking user existance on treasury. Error " . $json_trs_profile->meta->code . " (" . $json_trs_profile->meta->message . ").");

      $return = [
        "code"    => $json_trs_profile->meta->code,
        "status"  => "failed",
        // "message" => "Maaf gagal untuk cek user Treasury, " . $json_trs_profile->meta->message . ". Silahkan coba lagi beberapa saat lagi.",
        "message" => "Maaf gagal menghubungkan akun Treasury kamu dengan Ladara Emas. Silahkan coba lagi beberapa saat lagi.",
        "data"    => []
      ];

      echo json_encode($return);
      die();
    }

    ddlog("End login to treasury for user " . $email . " for checking user existance on treasury. Success.");

    $verified_user = 0;
    if (
      strtolower($json_trs_profile->data->RegNoKYCStatus) == "verified" &&
      strtolower($json_trs_profile->data->SelfieKYCStatus) == "verified"
    ) {
      $verified_user = 1;
    }

    $bank_name = "";
    // if(!empty($bank)) {
    //   $gld_bank = gld_bank(array("type"=>"trs"));
    //   $gld_bank = json_decode($gld_bank);
    //   $bank_name = gld_search_bank_name($bank, $gld_bank->data);
    // }

    if(!isset($_POST["is_update"]) || (isset($_POST["is_update"]) && $_POST["is_update"] === "false")) {
      $wpdb->query("INSERT INTO ldr_user_emas (
          user_id, 
          name, 
          email, 
          ts_password, 
          gender, 
          birthday, 
          phone, 
          security_question, 
          security_question_answer, 
          account_name, 
          account_number, 
          bank_code,  
          bank_name, 
          branch,
          verified_user, 
          gold_balance, 
          gold_balance_in_currency,
          trs_user 
        ) VALUES (
          " . $user_id . ",  
          '" . $user_profile->data->name . "',  
          '" . $email . "',  
          '" . $password . "',  
          '" . $user_profile->data->gender . "', 
          '" . $user_profile->data->birthdate . "',  
          '" . $user_profile->data->phone . "',  
          '',  
          '(connect to treasury)',   
          '" . $owner . "',  
          '" . $norek . "',  
          '" . $bank . "',   
          '" . $bank_name . "',  
          '" . $branch . "',
          " . $verified_user . ",  
          " . $json_trs_profile->data->gold_balance . ",  
          " . $json_trs_profile->data->gold_balance . ",
          1
        )");

      $new_user_emas = $wpdb->get_row(
        "SELECT id, user_id, name, email
          FROM ldr_user_emas 
          WHERE user_id=" . $user_id . " 
            AND email='" . $user_profile->data->email . "' 
          ORDER BY id DESC LIMIT 1",
        OBJECT
      );

      $wpdb->query("INSERT INTO ldr_user_emas_log (
          user_emas_id, 
          log 
        ) VALUES (
          " . $new_user_emas->id . ",  
          'User " . $new_user_emas->name . " (" . $new_user_emas->email . ") linking their Treasury account to Ladara Emas.' 
        )");

      $data_email = [
        "customer_name"   => $user_profile->data->name,
        "customer_email"  => $user_profile->data->email
      ];

      $customer = new UsersBuilder($user_profile->data->name, $user_profile->data->email);
      $email_customer = (new EmailHelper($customer))->aktivasiAkunEmas($data_email);
      
      $admin = new UsersBuilder($GLOBALS["gld_global"]["email_admin"]["name"], $GLOBALS["gld_global"]["email_admin"]["email"]);
      $email_admin = (new EmailHelper($admin))->adminAktivasiAkunEmas($data_email);

      ddlog("Start add web notification user " . $user_profile->data->email . " register to treasury.");

      $notif_data = [
        "userId"        => $user_id,
        "title"         => "Selamat datang di Ladara Emas!",
        "descriptions"  => "Kamu telah melakukan aktivasi akun Ladara Emas. Kamu sudah bisa menabung emas di Ladara Emas.",
        "type"          => "emas",
        "orderId"       => 0,
        "data"          => []
      ];

      Notifications::addNotification($notif_data);
      
      ddlog("End add web notification user " . $user_profile->data->email . " register to treasury.");
    }
    
    if(isset($_POST["is_update"]) && $_POST["is_update"] === "true") {
      $wpdb->query("UPDATE ldr_user_emas 
        SET name='" . $email . "', 
          ts_password='" . $password . "' 
        WHERE id=" . $user_id . ";");

      $wpdb->query("INSERT INTO ldr_user_emas_log (
          user_emas_id, 
          log 
        ) VALUES (
          " . $user_profile->data->id . ",  
          'User " . $user_profile->data->name . " (" . $user_profile->data->email . ") update their Treasury credentials for linking their Treasury account to Ladara Emas.' 
        )");

      $data_email = [
        "customer_name"   => $user_profile->data->name,
        "customer_email"  => $user_profile->data->email,
        "log_date"        => date("Y-m-d H:i:s")
      ];

      $customer = new UsersBuilder($user_profile->data->name, $user_profile->data->email);
      $email_customer = (new EmailHelper($customer))->updateAkunTreasury($data_email);
      
      $admin = new UsersBuilder($GLOBALS["gld_global"]["email_admin"]["name"], $GLOBALS["gld_global"]["email_admin"]["email"]);
      $email_admin = (new EmailHelper($admin))->adminUpdateAkunTreasury($data_email);

      ddlog("Start add web notification user " . $user_profile->data->email . " register to treasury.");

      $notif_data = [
        "userId"        => $user_id,
        "title"         => "Akun Ladara Emas kamu telah berubah!",
        "descriptions"  => "Kamu telah melakukan perubahan akun Ladara Emas kamu yang terhubung dengan Treasury.",
        "type"          => "emas",
        "orderId"       => 0,
        "data"          => []
      ];

      Notifications::addNotification($notif_data);
      
      ddlog("End add web notification user " . $user_profile->data->email . " register to treasury.");
    }

    $return = [
      "code"    => 200,
      "status"  => "success",
      "message" => "Sukses.",
      "data"    => []
    ];

    echo json_encode($return);
    die();
    
	}
	add_action("wp_ajax_ajax_linking_treasury","ajax_linking_treasury");
  add_action("wp_ajax_nopriv_ajax_linking_treasury", "ajax_linking_treasury");

	/**
	 * Ajax cancel buy
	 *
	 * @return  void
	 * @author  Amy <laksmise@gmail.com>
	 */
  function ajax_gld_cancel_buy() {
    global $wpdb;
    
    $noinvoice = $_POST["invoice"];

    $wpdb->query("UPDATE ldr_transaksi_emas SET 
        status = 'Canceled', 
        status_reason='Membatalkan pembelian' 
      WHERE invoice_order = '" . $noinvoice . "'");
      
    $return = [
      "code"    => 200,
      "status"  => "success",
      "message" => "Sukses.",
      "data"    => []
    ];

		echo json_encode($return);
		die();
  }
	add_action("wp_ajax_ajax_gld_cancel_buy","ajax_gld_cancel_buy");
  add_action("wp_ajax_nopriv_ajax_gld_cancel_buy", "ajax_gld_cancel_buy");
/******************************** AJAX FUNCTION ********************************/


/************************** AJAX DASHBOARD LADARA EMAS **************************/
	/**
	 * Ajax check status jual lewat dashboard ladara emas
	 *
	 * @return  void
	 * @author  Amy <laksmise@gmail.com>
	 */
  function ajax_gld_status_jual() {
    global $wpdb;
    
    $invoice = $_POST["invoice"];

    $get_transaction = $wpdb->get_row(
      "SELECT * FROM ldr_transaksi_emas WHERE id='$invoice'",
      OBJECT
    );

    if(empty($get_transaction)) {
      $return = [
        "code"    => 404,
        "status"  => "failed",
        "message" => "Transaksi " . $get_transaction->invoice_order . " tidak ditemukan.",
        "data"    => []
      ];

      echo json_encode($return);
      die();
    }

    $user_emas = $wpdb->get_row(
      "SELECT * FROM ldr_user_emas WHERE id=".$get_transaction->user_id."",
      OBJECT
    );

    if(empty($user_emas)) {
      $return = [
        "code"    => 404,
        "status"  => "failed",
        "message" => "Customer pemilik invoice " . $get_transaction->invoice_order . " tidak ditemukan.",
        "data"    => []
      ];

      echo json_encode($return);
      die();
    }

    $login_trs = gld_login(array("user_id"=>$user_emas->user_id));
    $login_trs = json_decode($login_trs);

    if($login_trs->code !== 200) {
      $return = [
        "code"    => $login_trs->code,
        "status"  => $login_trs->status,
        "message" => $login_trs->message,
        "data"    => []
      ];

      echo json_encode($return);
      die();
    }

    $data_email = [
      "invoice_order"     => $get_transaction->invoice_order,
      "customer_name"     => $user_emas->name,
      "customer_email"    => $user_emas->email,
      "created_date"      => $get_transaction->created_date,
      "total_price"       => $get_transaction->total_price,
      "total_emas"        => $get_transaction->total_gold,
      "partner_fee"       => $get_transaction->partner_fee,
      "total_payment"     => $get_transaction->total_payment,
      "history_note"      => ""
    ];

    $customer = new UsersBuilder($user_emas->name, $user_emas->email);
    $admin = new UsersBuilder($GLOBALS["gld_global"]["email_admin"]["name"], $GLOBALS["gld_global"]["email_admin"]["email"]);
      
    $authorization = "Authorization: Bearer " . $login_trs->data->access_token;
    $curl_header = array();
    $curl_header[] = $authorization;

    $body_detail_history = [
      "type"        => "sell",
      "invoice_no"  => $get_transaction->trs_invoice,
    ];
    
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, getenv('TREASURY_BASE_URL') . "/detail-history");
    curl_setopt($ch, CURLOPT_TIMEOUT, $GLOBALS["gld_global"]["curl_timeout"]);
    curl_setopt($ch, CURLOPT_HTTPHEADER, $curl_header);
    curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
    curl_setopt($ch, CURLOPT_HTTP_VERSION, CURL_HTTP_VERSION_1_1);
    curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_ENCODING, "");
    curl_setopt($ch, CURLOPT_MAXREDIRS, $GLOBALS["gld_global"]["curl_maxredirs"]);
    curl_setopt($ch, CURLOPT_POSTFIELDS, $body_detail_history);

    $output = curl_exec($ch);
    curl_getinfo($ch, CURLINFO_EFFECTIVE_URL);
    curl_getinfo($ch, CURLINFO_HTTP_CODE);
    curl_close($ch);
    $json_response = json_decode($output);

    if(!isset($json_response->meta->code)) {
      $return = [
        "code"    => 500,
        "status"  => "failed",
        "message" => "Maaf terjadi kendala koneksi. Silahkan coba lagi beberapa saat lagi.",
        "data"    => []
      ];

      echo json_encode($return);
      die();
    }

    if($json_response->meta->code !== 200) {
      $return = [
        "code"    => $json_response->meta->code,
        "status"  => "failed",
        "message" => "Maaf gagal mengecek transaksi penjualan ".$get_transaction->invoice_order.". Silahkan coba lagi beberapa saat lagi.",
        "data"    => []
      ];

      echo json_encode($return);
      die();
    }

    if(strtolower($json_response->data->status) === 'approved') {
      $wpdb->query("UPDATE ldr_transaksi_emas SET 
        status='Approved'
        WHERE id=" . $get_transaction->id);

      $wpdb->query("INSERT INTO ldr_transaksi_emas_log (
          transaksi_emas_id, 
          log, 
          status
        ) VALUES (
          " . $get_transaction->id . ", 
          'Penjualan emas dengan invoice " . $get_transaction->invoice_order . " disetujui', 
          'Approved'
        )");

      $email_customer = (new EmailHelper($customer))->disetujuiJualEmas($data_email);

      $email_admin = (new EmailHelper($admin))->adminDisetujuiJualEmas($data_email);

      $notif_data = [
        "userId"        => $user_emas->user_id,
        "title"         => "Selamat! Penjualan emas kamu telah disetujui!",
        "descriptions"  => "Penjualan emas kamu sebesar " . str_replace(".", ",", $get_transaction->total_gold) . " gram telah disetujui! Yuk cek rate emas terkini di Ladara Emas.",
        "type"          => "emas",
        "orderId"       => 0,
        "data"          => []
      ];

      Notifications::addNotification($notif_data);
      
      $return = [
        "code"    => 200,
        "status"  => "success",
        "message" => "Penjualan ".$get_transaction->invoice_order." telah di approve. Silahkan refresh halaman untuk mengupdate halaman detail transaksi jual.",
        "data"    => []
      ];

      echo json_encode($return);
      die();
    } else if(strtolower($json_response->data->status) === 'rejected') {
      $log_message = "Penjualan emas dengan invoice " . $get_transaction->invoice_order . " ditolak";
      $notif_description = "Penjualan emas kamu sebesar " . str_replace(".", ",", $get_transaction->total_gold) . " gram telah ditolak!";
      $notif_description = $notif_description . " Kamu dapat menjual emas kembali di Ladara Emas";

      if(isset($json_response->data->AdditionalNote)) {
        $data_email["history_note"] = strtolower(str_replace("support@treasury.id", "cs@ladara.id", $json_response->data->AdditionalNote));
        $log_message = $log_message . " karena " . $data_email["history_note"];
        $notif_description = $notif_description . " Karena " . $data_email["history_note"] . ".";
      }

      $wpdb->query("UPDATE ldr_transaksi_emas SET 
        status='Rejected', 
        status_reason='" . $data_email["history_note"] . "'
        WHERE id=" . $get_transaction->id);

      $wpdb->query("INSERT INTO ldr_transaksi_emas_log (
        transaksi_emas_id, 
        log, 
        status
      ) VALUES (
        " . $get_transaction->id . ", 
        '" . $log_message . "', 
        'Rejected'
      )");

      $email_customer = (new EmailHelper($customer))->jualEmasDitolak($data_email);

      $email_admin = (new EmailHelper($admin))->adminJualEmasDitolak($data_email);

      $notif_data = [
        "userId"        => $user_emas->user_id,
        "title"         => "Duh! penjualan emas kamu ditolak",
        "descriptions"  => $notif_description,
        "type"          => "emas",
        "orderId"       => 0,
        "data"          => []
      ];

      Notifications::addNotification($notif_data);
      
      $return = [
        "code"    => 202,
        "status"  => "success",
        "message" => "Penjualan ".$get_transaction->invoice_order." di reject karena " . $data_email["history_note"] . ". Silahkan refresh halaman untuk mengupdate halaman detail transaksi jual.",
        "data"    => []
      ];

      echo json_encode($return);
      die();
    } else {
      $return = [
        "code"    => 202,
        "status"  => "success",
        "message" => "Penjualan ".$get_transaction->invoice_order." masih dalam proses pengecekan.",
        "data"    => []
      ];

      echo json_encode($return);
      die();
    }
    
  }
	add_action("wp_ajax_ajax_gld_status_jual","ajax_gld_status_jual");
  add_action("wp_ajax_nopriv_ajax_gld_status_jual", "ajax_gld_status_jual");

	/**
	 * Ajax check user verifikasi for ladara emas admin dashboard
	 *
	 * @return  void
	 * @author  Amy <laksmise@gmail.com>
	 */
  function ajax_gld_user_verifikasi() {
    global $wpdb;
    
    $user_emas_id = $_POST["user_id"];

    $user_emas = $wpdb->get_row(
      "SELECT id, user_id, email, name, ts_password 
        FROM ldr_user_emas 
        WHERE id=" . $user_emas_id . "",
      OBJECT
    );

    $login_trs = gld_login(array("user_id"=>$user_emas->user_id));
    $login_trs = json_decode($login_trs);

    if($login_trs->code === 200) {
      
      $curl = curl_init();

      curl_setopt_array($curl, array(
        CURLOPT_URL => getenv('TREASURY_BASE_URL') . "/profile",
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_ENCODING => "",
        CURLOPT_MAXREDIRS => $GLOBALS["gld_global"]["curl_maxredirs"],
        CURLOPT_TIMEOUT => $GLOBALS["gld_global"]["curl_timeout"],
        CURLOPT_FOLLOWLOCATION => true,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => "GET",
        CURLOPT_HTTPHEADER => array(
          "Authorization: Bearer " . $login_trs->data->access_token
        ),
      ));
        
      $output = curl_exec($curl);
      curl_getinfo($curl, CURLINFO_EFFECTIVE_URL);
      curl_getinfo($curl, CURLINFO_HTTP_CODE);
      curl_close($curl);
      $json_response = json_decode($output);

      if(!isset($json_response->meta->code)) {
        $return = [
          "code"    => 500,
          "status"  => "failed",
          "message" => "Maaf terjadi gangguan untuk memverifikasi user. Silahkan coba lagi beberapa saat lagi.",
          "data"    => []
        ];
      }

      if($json_response->meta->code !== 200) {
        $return = [
          "code"    => $json_response->meta->code,
          "status"  => "failed",
          "message" => "Maaf gagal memverifikasi user. Silahkan coba lagi beberapa saat lagi.",
          "data"    => []
        ];
      }

      if (
        strtolower($json_response->data->RegNoKYCStatus) == "verified" &&
        strtolower($json_response->data->SelfieKYCStatus) == "verified"
      ) {
        $gold_balance = $json_response->data->gold_balance;
        $gold_balance_in_currency = $json_response->data->gold_balance_in_currency;

        $query_update_ldr_user_emas = "UPDATE ldr_user_emas SET 
            verified_user=1, 
            gold_balance=" . $gold_balance . ",
            gold_balance_in_currency = " . $gold_balance_in_currency . "
            WHERE id=" . $user_emas->user_id;
        $wpdb->query($query_update_ldr_user_emas);

        $data_email = [
          "customer_name"   => $user_emas->name,
          "customer_email"  => $user_emas->email
        ];

        $customer = new UsersBuilder($user_emas->name, $user_emas->email);
        $email_customer = (new EmailHelper($customer))->verifikasiAkunEmas($data_email);
        
        $admin = new UsersBuilder($GLOBALS["gld_global"]["email_admin"]["name"], $GLOBALS["gld_global"]["email_admin"]["email"]);
        $email_admin = (new EmailHelper($admin))->adminVerifikasiAkunEmas($data_email);

        $notif_data = [
          "userId"        => $user_emas->user_id,
          "title"         => "Selamat! Akun Ladara Emas kamu sudah terverifikasi",
          "descriptions"  => "Kamu dapat menggunakan fitur jual emas di Ladara Emas. Yuk cek rate emas terkini di Ladara Emas.",
          "type"          => "emas",
          "orderId"       => 0,
          "data"          => []
        ];

        Notifications::addNotification($notif_data);

        $return = [
          "code"    => 200,
          "status"  => "success",
          "message" => "Sukses memverifikasi user.",
          "data"    => []
        ];
      } else {
        $return = [
          "code"    => 201,
          "status"  => "success",
          "message" => "User belum terverifikasi.",
          "data"    => []
        ];
      }
    } else {
      $return = [
        "code"    => $login_trs->code,
        "status"  => $login_trs->status,
        "message" => $login_trs->message,
        "data"    => []
      ];
    }

    echo json_encode($return);
    die();
  }
	add_action("wp_ajax_ajax_gld_user_verifikasi","ajax_gld_user_verifikasi");
  add_action("wp_ajax_nopriv_ajax_gld_user_verifikasi", "ajax_gld_user_verifikasi");

  function ajax_gld_retrieve_payment() {
    global $wpdb;
    
    $transaksi_id = $_POST["invoice"];
    $user_id = $_POST["user"];

    $retrieve_payment = gld_retrieve_payment($transaksi_id, $user_id);
    $retrieve_payment = json_decode($retrieve_payment);

    if($retrieve_payment->data->status === 1) {

      //login ke treasury satu per satu ^^;
      $login_trs = gld_login(array("user_id"=>$h->user_id));
      $login_trs = json_decode($login_trs);

      if($login_trs->code === 200) {

        $authorization = "Authorization: Bearer " . $login_trs->data->access_token;
        $curl_header = array();
        $curl_header[] = $authorization;

        if(str_replace(" ", "", strtolower($h->payment_method)) == "kartukredit") {
          $payment_note = "Payment Ladara Emas with Credit Card";
        } else {
          $payment_note = "Payment Ladara Emas with " . $h->payment_method;
        }

        $payment_notify_body = [
          "invoice_number"  => $h->trs_invoice,
          "payment_note"    => $payment_note,
        ];

        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, getenv('TREASURY_BASE_URL') . "/payment-notify");
        curl_setopt($curl, CURLOPT_TIMEOUT, 0);
        curl_setopt($curl, CURLOPT_HTTPHEADER, $curl_header);
        curl_setopt($curl, CURLOPT_FOLLOWLOCATION, true);
        curl_setopt($curl, CURLOPT_HTTP_VERSION, CURL_HTTP_VERSION_1_1);
        curl_setopt($curl, CURLOPT_CUSTOMREQUEST, "POST");
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_ENCODING, "");
        curl_setopt($curl, CURLOPT_MAXREDIRS, 0);
        curl_setopt($curl, CURLOPT_POSTFIELDS, $payment_notify_body);

        $payment_notify = curl_exec($curl);
        curl_getinfo($curl, CURLINFO_EFFECTIVE_URL);
        curl_getinfo($curl, CURLINFO_HTTP_CODE);
        curl_close($curl);

        $payment_notify_decode = json_decode($payment_notify);

        if(isset($payment_notify_decode->meta->code)) {
          
          if($payment_notify_decode->meta->code === 200) {
            $wpdb->query("UPDATE ldr_transaksi_emas SET 
              status='Success',
              status_reason='' 
              WHERE id=" . $h->transaksi_id . " AND trs_invoice='" . $h->trs_invoice . "'");

            $wpdb->query("INSERT INTO ldr_transaksi_emas_log (
                transaksi_emas_id, 
                log, 
                status
              ) VALUES (
                " . $transaksi->id . ", 
                'Pembelian emas dengan invoice " . $h->invoice_order . " sukses.', 
                'Success'
              )");

            $gold_balance = $h->gold_balance + $h->total_gold;
            $gold_balance_in_currency = $h->gold_balance_in_currency + $h->total_price;

            $wpdb->query("UPDATE ldr_user_emas SET 
                gold_balance=" . $gold_balance . ",
                gold_balance_in_currency=" . $gold_balance_in_currency . "
                WHERE id=" . $h->user_emas_id);

          } else {
            $return = [
              "code"    => $payment_notify_decode->meta->code,
              "status"  => "failed",
              "message" => "Maaf gagal melakukan payment notify Treasury. " . $payment_notify_decode->meta->message . ". Silahkan hubungi Treasury.",
              "data"    => []
            ];

            echo json_encode($return);
            die();
          } 

        } else {
          $return = [
            "code"    => 500,
            "status"  => "failed",
            "message" => "Maaf terjadi gangguan melakukan payment notify Treasury. Silahkan hubungi Treasury.",
            "data"    => []
          ];

          echo json_encode($return);
          die();
        } 

      } else {
        $return = [
          "code"    => $login_trs->code,
          "status"  => $login_trs->status,
          "message" => $login_trs->message,
          "data"    => []
        ];

        echo json_encode($return);
        die();
      }      

    } elseif($retrieve_payment->data->status === -1) {

      $return = array(
        "code"    => 202,
        "status"  => "success",
        "message" => "Invoice " . $retrieve_payment->data->history->invoice_order . " telah melewati batas pembayaran",
        "data"    => []
      );

      echo json_encode($return);
      die();

    } elseif($retrieve_payment->data->status === 0) {

      $return = array(
        "code"    => 202,
        "status"  => "success",
        "message" => "Invoice ".$retrieve_payment->data->history->invoice_order." masih menunggu pembayaran",
        "data"    => []
      );

      echo json_encode($return);
      die();

    }

  }
	add_action("wp_ajax_ajax_gld_retrieve_payment","ajax_gld_retrieve_payment");
  add_action("wp_ajax_nopriv_ajax_gld_retrieve_payment", "ajax_gld_retrieve_payment");
/************************** AJAX DASHBOARD LADARA EMAS **************************/

/**************************** DASHBOARD LADARA EMAS ****************************/
  function adm_gld_limit() {
    $limit = isset($_SESSION["adm_gld_limit"]) ? $_SESSION["adm_gld_limit"] : 20;
    return $limit;
  }
  add_action( "adm_gld_limit", "adm_gld_limit", 0 );

  function adm_gld_user_list($params) {
    global $wpdb;

    $limit = adm_gld_limit();
    $page = $params["pg"];

    /** where */
      $where = "";

      if(isset($params["action"])) {
        $where .= " WHERE";

        if(!empty($params["name"])) {
          $where .= " name LIKE '%" . $params["name"] . "%'";

          if(!empty($params["email"])) {
            $where .= " AND";
          }
        }

        if(!empty($params["email"])) {
          $where .= " email LIKE '%" . $params["email"] . "%'";

          if(!empty($params["date_from"])) {
            $where .= " AND";
          }
        }

        if(!empty($params["date_from"])) {
          $where .= " created_date > '" . $params["date_from"] . "'";

          if(!empty($params["date_to"])) {
            $where .= " AND created_date < '" . $params["date_to"] . "'";
          } else {
            $where .= " AND created_date < '" . date("Y-m-d H:i:s") . "'";
          }

          if($params["gender"] !== "all") {
            $where .= " AND";
          }
        }

        if($params["gender"] !== "all") {
          $where .= " gender='" . $params["gender"] . "'";
        }

        if($params["gender"] !== "all" && $params["activated"] !== "all") {
          $where .= " AND";
        }

        if($params["activated"] === "yes") {
          $where .= " activated_user=1";
        }

        if($params["activated"] === "no") {
          $where .= " activated_user=0";
        }

        if($params["verified"] !== "all" && $params["activated"] !== "all") {
          $where .= " AND";
        }

        if($params["verified"] === "yes") {
          $where .= " verified_user=1";
        }

        if($params["verified"] === "no") {
          $where .= " verified_user=0";
        }
      }
    /** where */

    /** sorting */
      $order_by = "";

      if(!isset($params["order"]) && !isset($params["sort"])) {
        $order_by .= " ORDER BY id DESC";
      }

      if(isset($params["order"]) && isset($params["sort"])) {
        if($params["order"] === "name") {
          $order_by .= " ORDER BY name";
        }
        if($params["order"] === "gender") {
          $order_by .= " ORDER BY gender";
        }
        if($params["order"] === "email") {
          $order_by .= " ORDER BY email";
        }
        if($params["order"] === "created_date") {
          $order_by .= " ORDER BY created_date";
        }
        if($params["order"] === "activated") {
          $order_by .= " ORDER BY activated_user";
        }
        if($params["order"] === "verified") {
          $order_by .= " ORDER BY verified_user";
        }

        $sort = " " . $params["sort"];
        if($params["order"] === "gender" && $params["sort"] === "DESC") {
          $sort = " ASC";
        } elseif($params["order"] === "gender" && $params["sort"] === "ASC") {
          $sort = " DESC";
        }
        $order_by .= $sort;
      }
    /** sorting */

    /** data */
      $start = ($page > 1) ? ($page * $limit) - $limit : 0;
      $query = "SELECT * FROM ldr_user_emas";
      $query .= $where;
      $query .= $order_by;
      $query .= " LIMIT " . $start . ", " . $limit;
      // echo $query;exit;
      $list = $wpdb->get_results($query, OBJECT);
    /** data */

    /** total */
      $total_query = "SELECT COUNT(*) as total FROM ldr_user_emas";
      $total_query .= $where;
      $total = $wpdb->get_row($total_query, OBJECT);
      $total = (!empty($total)) ? $total->total : 0;
    /** total */

    $rtn = [
      "total" => $total,
      "datas" => $list
    ];

    return $rtn;
  }
  add_action( "adm_gld_user_list", "adm_gld_user_list", 0 );

  function adm_gld_transaction_list($params) {
    global $wpdb;

    $limit = adm_gld_limit();
    $page = $params["pg"];

    /** where */
      $where = "";

      if(isset($params["type"]) && $params["type"] !== "all") {
        $where .= " AND ldr_transaksi_emas.type='" . $params["type"] . "'";
      }

      if(isset($params["customer"]) && $params["customer"] !== "all") {
        $where .= " AND ldr_transaksi_emas.user_id=" . $params["customer"] . "";
      }

      if(isset($params["status"]) && $params["status"] !== "all") {
        $where .= " AND ldr_transaksi_emas.status='" . $params["status"] . "'";
      }

      if(isset($params["invoice"]) && !empty($params["invoice"])) {
        $invoice = str_replace("%2F", "/", $params["invoice"]);
        $where .= " AND ldr_transaksi_emas.invoice_order='" . $invoice . "'";
      }

      if(!empty($params["date_from"])) {
        $where .= " AND ldr_transaksi_emas.created_date > '" . $params["date_from"] . "'";

        if(!empty($params["date_to"])) {
          $where .= " AND ldr_transaksi_emas.created_date < '" . $params["date_to"] . "'";
        } else {
          $where .= " AND ldr_transaksi_emas.created_date < '" . date("Y-m-d H:i:s") . "'";
        }
      }
    /** where */

    /** sorting */
      $order_by = "";

      if(!isset($params["order"]) && !isset($params["sort"])) {
        $order_by .= " ORDER BY id DESC";
      }

      if(isset($params["order"]) && isset($params["sort"])) {
        if($params["order"] === "customer") {
          $order_by .= " ORDER BY user_name";
        }
        if($params["order"] === "order_date") {
          $order_by .= " ORDER BY created_date";
        }
        if($params["order"] === "type") {
          $order_by .= " ORDER BY type";
        }
        if($params["order"] === "invoice") {
          $order_by .= " ORDER BY invoice_order";
        }
        if($params["order"] === "total_gold") {
          $order_by .= " ORDER BY total_gold";
        }
        if($params["order"] === "total_payment") {
          $order_by .= " ORDER BY total_payment";
        }
        if($params["order"] === "status") {
          $order_by .= " ORDER BY status";
        }

        $order_by .= " " . $params["sort"];
      }
    /** sorting */

    /** data */
      $start = ($page > 1) ? ($page * $limit) - $limit : 0;
      $query = "SELECT ldr_transaksi_emas.*, ldr_user_emas.name AS user_name FROM ldr_transaksi_emas 
        JOIN ldr_user_emas ON ldr_transaksi_emas.user_id=ldr_user_emas.id
        WHERE status!='Create Invoice'";
      $query .= $where;
      $query .= $order_by;
      $query .= " LIMIT " . $start . ", " . $limit;
      // echo $query; exit;
      $list = $wpdb->get_results($query, OBJECT);
    /** data */

    /** total */
      $total_query = "SELECT COUNT(*) as total FROM ldr_transaksi_emas WHERE status!='Create Invoice'";
      $total_query .= $where;
      $total = $wpdb->get_row($total_query, OBJECT);
      $total = (!empty($total)) ? $total->total : 0;
    /** total */

    $rtn = [
      "total" => $total,
      "datas" => $list
    ];

    return $rtn;
  }
  add_action( "adm_gld_transaction_list", "adm_gld_transaction_list", 0 );

  function adm_gld_user_chart($currentYear) {
    global $wpdb;

    $data = [];

    for($month=1; $month<=12; $month++) {
      $total = $wpdb->get_row("SELECT COUNT(*) AS total FROM ldr_user_emas WHERE ldr_user_emas.activated_user=1 AND created_date>='".$currentYear."-".$month."-01' AND created_date<='".$currentYear."-".$month."-31'", OBJECT);
      $data[$currentYear][$month] = $total->total;
    }
    
    $rtn = $data;

    return $rtn;
  }
  add_action( "adm_gld_user_chart", "adm_gld_user_chart", 0 );

  function adm_gld_income_chart($currentYear) {
    global $wpdb;

    $data = [];

    for($month=1; $month<=12; $month++) {
      $total = $wpdb->get_row("SELECT SUM(ldr_transaksi_emas.partner_fee) AS income FROM ldr_transaksi_emas JOIN ldr_user_emas ON ldr_transaksi_emas.user_id=ldr_user_emas.id WHERE status='Success' AND type='buy' AND ldr_transaksi_emas.created_date>='".$currentYear."-".$month."-01' AND ldr_transaksi_emas.created_date<='".$currentYear."-".$month."-31'", OBJECT);
      $data[$currentYear][$month] = (!empty($total->income)) ? $total->income : 0;
    }
    
    $rtn = $data;

    return $rtn;
  }
  add_action( "adm_gld_income_chart", "adm_gld_income_chart", 0 );
/**************************** DASHBOARD LADARA EMAS ****************************/
?>