<?php
/*
Template Name: Manajemen Voucher - Edit
*/
?>

<?php get_header('admin'); ?>

<?php if (have_posts()) :
    while (have_posts()) :
        the_post();

        global $wpdb;
        date_default_timezone_set("Asia/Jakarta");
        $nowdate = date('Y-m-d H:i:s');

        $current_user = wp_get_current_user();
        $u_id = $current_user->ID;
        $u_roles = $current_user->roles;
        $admin = 0;

        if (isset($u_roles) and !empty($u_roles)) {
            foreach ($u_roles as $key => $value) {
                if (strtolower($value) == 'administrator') {
                    $admin = 1;
                }
                if (strtolower($value) == 'shop_manager') {
                    $admin = 1;
                }
            }
        }

        if ((isset($u_id) and $u_id != 0) and $admin == 1) {
            $categories = sendRequest('GET', MERCHANT_URL . '/api/categories');
            $voucherId = isset($_GET['id']) ? htmlspecialchars($_GET['id']) : 0;

            $voucher = $wpdb->get_row("SELECT * FROM ldr_vouchers WHERE deleted_at IS NULL AND merchant_id = 0 AND id = $voucherId");
            $voucherCategories = $wpdb->get_results("SELECT category_id FROM ldr_voucher_categories WHERE voucher_id = $voucherId");
            $categoryIds = array_map(function ($fn) {
                return $fn->category_id;
            }, $voucherCategories);

            $voucherProducts = $wpdb->get_results("SELECT * FROM ldr_products WHERE id IN (SELECT product_id FROM ldr_voucher_products WHERE voucher_id = $voucherId)");
            $products = array_map(function ($fn) {
                return [
                    'label' => $fn->title,
                    'value' => $fn->id,
                    'selected' => true
                ];
            }, $voucherProducts);
?>
            <style>
                .header {
                    background: #EDEFF6;
                    color: #607FC2;
                    font-weight: bold;
                    padding: 26px;
                    border-radius: 10px 10px 0 0;
                    box-shadow: 0px 3px 6px #A1A1A129;
                }

                .body-form {
                    padding: 31px 5px;
                    width: 100%;
                    background: #ffffff;
                    box-shadow: 0px 3px 6px #A1A1A129;
                }

                .d-block {
                    display: block;
                }

                .ldr.text-info {
                    color: #313131 !important;
                    font-size: 11px;
                    font-weight: 100;
                }

                .mb-50 {
                    margin-bottom: 50px;
                }

                .btn-submit {
                    background: #0080FF 0% 0% no-repeat padding-box;
                    border-radius: 147px;
                    color: #ffffff;
                    padding: 10px 30px;
                }

                .btn-submit:hover {
                    color: #ffffff;
                }

                .btn-cancle {
                    background: #FFFFFF 0% 0% no-repeat padding-box;
                    border: 2px solid #0080FF;
                    border-radius: 147px;
                    color: #0080FF;
                    padding: 10px 30px;
                    width: 120px;
                }

                .btn-cancle:hover {
                    color: #0080FF;
                }
            </style>
            <!-- Begin Page Content -->
            <div class="container-fluid">

                <!-- Page Heading -->
                <div class="row">
                    <div class="col-md-12">
                        <div class="header">
                            Edit Voucher
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="alert alert-danger chk_err">
                            <!-- Checkout Errors -->
                        </div>
                        <div class="body-form">
                            <form id="formKategori">
                                <div class="form-group row mb-50">
                                    <label class="col-md-4 col-form-label">
                                        Target Voucher
                                        <span class="d-block ldr text-info">
                                            <ul>
                                                <li>Publik: Voucher akan ditampilkan di halaman Checkout dan dapat digunakan oleh semua calon pembeli.</li>
                                                <li>Terbatas: Voucher tidak ditampilkan di halaman Checkout tetapi dapat dibagikan secara pribadi ke pelanggan Anda.</li>
                                            </ul>
                                        </span>
                                    </label>
                                    <div class="col-md-8">
                                        <select class="form-control" id="target">
                                            <option value="1" <?= ($voucher->target === '1' ? 'selected' : '') ?>>Publik</option>
                                            <option value="2" <?= ($voucher->target === '2' ? 'selected' : '') ?>>Terbatas</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group row mb-50">
                                    <label class="col-md-4 col-form-label">
                                        Nama Voucher <small>(Mandatory)</small>
                                    </label>
                                    <div class="col-md-8">
                                        <input id="name" required type="text" class="form-control" maxlength="30" value="<?= $voucher->name ?>">
                                    </div>
                                </div>
                                <div class="form-group row mb-50">
                                    <label class="col-md-4 col-form-label">
                                        Kode Voucher <small>(Mandatory)</small>
                                    </label>
                                    <div class="col-md-8">
                                        <input id="code" required type="text" class="form-control" maxlength="10" value="<?= $voucher->code ?>">
                                    </div>
                                </div>
                                <div class="form-group row mb-50">
                                    <label class="col-md-4 col-form-label">
                                        Syarat &amp; Ketentuan <small>(Optional)</small>
                                    </label>
                                    <div class="col-md-8">
                                        <textarea id="description" cols="30" rows="8" class="form-control"><?= $voucher->description ?></textarea>
                                    </div>
                                </div>
                                <div class="form-group row mb-50">
                                    <label class="col-md-4 col-form-label">
                                        Voucher Gratis Ongkir <small>(Optional)</small>
                                        <span class="d-block ldr text-info">
                                            Check pilihan ini apabila voucher merupakan voucher free ongkir
                                        </span>
                                    </label>
                                    <div class="col-md-8">
                                        <input type="checkbox" id="free_shipping" value="1" <?= ($voucher->free_shipping === '1' ? 'checked' : '') ?>>
                                    </div>
                                </div>
                                <div class="form-group row mb-50">
                                    <label class="col-md-4 col-form-label">
                                        Nominal Potongan Harga <small>(Mandatory)</small>
                                    </label>
                                    <div class="form-check form-check-inline" style="margin-right: 30px;">
                                        <input class="form-check-input" type="radio" id="discount_type" value="1" name="discount_type" <?= $voucher->discount_type === '1' ? 'checked' : '' ?> onchange="changeDiscountType(this)">
                                        <label class="form-check-label" for="discount_type">Dalam Persentase</label>
                                    </div>
                                    <div class="form-group col-md-2">
                                        <span class="discount_type_1" <?= $voucher->discount_type === '2' ? 'style="display: none;"' : '' ?>>
                                            <label for="period_start" class="control-label">Persentase</label>
                                            <input id="amount_percent" type="number" class="form-control" min="0" value="<?= $voucher->discount_type === '1' ? intval($voucher->discount_amount) : 0 ?>">
                                        </span>
                                    </div>
                                    <div class="form-group col-md-3">
                                        <span class="discount_type_1" <?= $voucher->discount_type === '2' ? 'style="display: none;"' : '' ?>>
                                            <label for="time_start" class="control-label">Maksimum Potongan Harga</label>
                                            <input id="amount_percent_max" type="text" class="form-control currency" value="<?= $voucher->discount_type === '1' ? number_format($voucher->discount_max) : 0 ?>">
                                        </span>
                                    </div>
                                    <div class="col-md-4"></div>
                                    <div class="form-check form-check-inline" style="margin-right: 30px;">
                                        <input class="form-check-input" type="radio" id="discount_type" value="2" name="discount_type" <?= $voucher->discount_type === '2' ? 'checked' : '' ?> onchange="changeDiscountType(this)">
                                        <label class="form-check-label" for="discount_type">Dalam Rupiah (Rp)</label>
                                    </div>
                                    <div class="form-group col-md-3">
                                        <span class="discount_type_2" <?= $voucher->discount_type === '1' ? 'style="display: none; margin-top: 10px;"' : '' ?>>
                                            <input id="amount_fix" type="text" class="form-control currency" value="<?= $voucher->discount_type === '2' ? number_format($voucher->discount_amount) : 0 ?>">
                                        </span>
                                    </div>
                                </div>
                                <div class="form-group row mb-50">
                                    <label class="col-md-4 col-form-label">
                                        Minimum Pembelian <small>(Mandatory)</small>
                                        <span class="d-block ldr text-info">
                                            Minimum pembelian tidak boleh kurang dari Rp 10.000
                                        </span>
                                    </label>
                                    <div class="col-md-8">
                                        <input id="min_spend" required type="text" class="form-control currency" value="<?= number_format($voucher->min_spend) ?>">
                                    </div>
                                </div>
                                <div class="form-group row mb-50">
                                    <label class="col-md-4 col-form-label">
                                        Maksimum Pembelian <small>(Mandatory)</small>
                                        <span class="d-block ldr text-info">
                                            Maksimum pembelian tidak boleh kurang dari Minimum Pembelian
                                        </span>
                                    </label>
                                    <div class="col-md-8">
                                        <input id="max_spend" required type="text" class="form-control currency" value="<?= number_format($voucher->max_spend) ?>">
                                    </div>
                                </div>
                                <div class="form-group row mb-50">
                                    <label class="col-md-4 col-form-label">
                                        Kuota Voucher <small>(Mandatory)</small>
                                        <span class="d-block ldr text-info">
                                            Masukkan jumlah kuota voucher
                                        </span>
                                    </label>
                                    <div class="col-md-8">
                                        <input id="quota" required type="number" class="form-control" min="0" value="<?= intval($voucher->limit_usage) ?>">
                                    </div>
                                </div>
                                <div class="form-group row mb-50">
                                    <label class="col-md-4 col-form-label">
                                        Limit per Customer <small>(Mandatory)</small>
                                        <span class="d-block ldr text-info">
                                            Masukkan batasan penggunaan oleh customer
                                        </span>
                                    </label>
                                    <div class="col-md-8">
                                        <input id="quota_per_user" required type="number" class="form-control" min="0" value="<?= $voucher->limit_usage_per_user ?>">
                                    </div>
                                </div>
                                <div class="form-group row mb-50">
                                    <label class="col-md-4 col-form-label">
                                        Periode Diskon <small>(Mandatory)</small>
                                    </label>
                                    <div class="form-group col-md-4">
                                        <label for="period_start_date" class="control-label">Tanggal Mulai</label>
                                        <input id="period_start_date" type="text" class="form-control" value="<?= date('Y-m-d', strtotime($voucher->period_start)) ?>">
                                    </div>
                                    <div class="form-group col-md-4">
                                        <label for="period_start_time" class="control-label">Waktu Mulai</label>
                                        <input id="period_start_time" type="text" class="form-control" value="<?= date('H:i', strtotime($voucher->period_start)) ?>">
                                    </div>
                                    <div class="col-md-4"></div>
                                    <div class="form-group col-md-4">
                                        <label for="period_end_date" class="control-label">Tanggal Selesai</label>
                                        <input id="period_end_date" type="text" class="form-control" value="<?= date('Y-m-d', strtotime($voucher->period_end)) ?>">
                                    </div>
                                    <div class="form-group col-md-4">
                                        <label for="period_end_time" class="control-label">Waktu Selesai</label>
                                        <input id="period_end_time" type="text" class="form-control" value="<?= date('H:i', strtotime($voucher->period_end)) ?>">
                                    </div>
                                </div>
                                <div class="form-group row mb-50">
                                    <label class="col-md-4 col-form-label">
                                        Banner <small>(Mandatory)</small>
                                        <span class="d-block ldr text-info">
                                            Banner yang akan muncul di Detail Voucher (Rasio ukuran 1:3)
                                        </span>
                                    </label>
                                    <div class="col-md-8">
                                        <input id="banner" required type="file" class="form-control">
                                    </div>
                                </div>
                                <div class="form-group row mb-50">
                                    <label class="col-md-4 col-form-label">
                                        Pilih Produk <small>(Optional)</small>
                                        <span class="d-block ldr text-info">
                                            Produk-produk tertentu yang dapat menggunakan voucher
                                            <br>
                                            (Kosongkan jika dipakai di semua produk)
                                        </span>
                                    </label>
                                    <div class="col-md-8">
                                        <select class="form-control" id="list-products" multiple>
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group row mb-50">
                                    <label class="col-md-4 col-form-label">
                                        Pilih Kategori Produk <small>(Optional)</small>
                                        <span class="d-block ldr text-info">
                                            Kategori-kategori tertentu yang dapat menggunakan voucher
                                            <br>
                                            (Kosongkan jika dipakai di semua kategori)
                                        </span>
                                    </label>
                                    <div class="col-md-8">
                                        <?php
                                        foreach ($categories->data as $category) {
                                        ?>
                                            <div class="checkbox">
                                                <label><input type="checkbox" name="categories" value="<?= $category->id ?>" <?= (in_array($category->id, $categoryIds) ? 'checked' : '') ?>> <?= $category->name ?></label>
                                            </div>
                                            <?php
                                            foreach ($category->children as $children) {
                                            ?>
                                                <div class="checkbox" style="margin-left: 25px; display: none;">
                                                    <label><input type="checkbox" name="categories" data-parent="<?= $category->id ?>" value="<?= $children->id ?>" <?= (in_array($children->id, $categoryIds) ? 'checked' : '') ?>> <?= $children->name ?></label>
                                                </div>
                                        <?php
                                            }
                                        }
                                        ?>
                                    </div>
                                </div>
                                <div class="footer-form" style="width: 100%;border-top: 1px solid #DDDDDD;padding: 10px 0;text-align: right;">
                                    <div>
                                        <a href="<?= home_url('/dashboard/manajemen-voucher') ?>">
                                            <input value="Batal" class="btn btn-cancle">
                                        </a>
                                        <button type="button" class="btn btn-submit" onclick="submitVoucher()">Simpan</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>

            <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/flatpickr/dist/flatpickr.min.css">
            <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/choices.js/public/assets/styles/choices.min.css" />
            <script src="https://cdn.jsdelivr.net/npm/flatpickr"></script>
            <script src="https://cdn.jsdelivr.net/npm/choices.js/public/assets/scripts/choices.min.js"></script>
            <script src="//cdnjs.cloudflare.com/ajax/libs/numeral.js/2.0.6/numeral.min.js"></script>

            <script>
                let products = JSON.parse('<?= json_encode($products) ?>');

                flatpickr("#period_start_date, #period_end_date");
                flatpickr("#period_start_time, #period_end_time", {
                    enableTime: true,
                    noCalendar: true,
                    dateFormat: "H:i",
                    time_24hr: true
                });

                function changeDiscountType(event) {
                    if (event.value === '1') {
                        document.querySelectorAll('.discount_type_1').forEach(el => {
                            el.style.display = 'block'
                        })

                        document.querySelectorAll('.discount_type_2').forEach(el => {
                            el.style.display = 'none'
                        })
                    } else {
                        document.querySelectorAll('.discount_type_1').forEach(el => {
                            el.style.display = 'none'
                        })

                        document.querySelectorAll('.discount_type_2').forEach(el => {
                            el.style.display = 'block'
                        })
                    }
                }

                document.querySelectorAll('.currency').forEach(el => {
                    el.addEventListener('keyup', function(event) {
                        event.target.value = numeral(event.target.value).format('0,0')
                    }, false)
                })

                Object.values(document.getElementsByName('categories')).forEach(element => {
                    element.addEventListener(
                        'click',
                        function(event) {
                            let {
                                checked,
                                value
                            } = event.target

                            if (checked) {
                                Object.values(document.querySelectorAll(`[name="categories"][data-parent="${value}"]`)).map(val => val.checked = true)
                            } else {
                                Object.values(document.querySelectorAll(`[name="categories"][data-parent="${value}"]`)).map(val => val.checked = false)
                            }

                            let parentId = event.target.getAttribute('data-parent')
                            let childrenlength = Object.values(document.querySelectorAll(`[data-parent="${parentId}"]`)).length
                            let checkedLength = Object.values(document.querySelectorAll(`[data-parent="${parentId}"]`)).filter(val => val.checked).length

                            if (childrenlength === checkedLength) {
                                try {
                                    document.querySelector(`[name="categories"][value="${parentId}"]`).checked = true
                                } catch (err) {
                                    //
                                }
                            } else {
                                document.querySelector(`[name="categories"][value="${parentId}"]`).checked = false
                            }

                            if (parentId === null) {
                                parentId = value
                            }

                            let checkedChildren = Object.values(document.querySelectorAll(`[data-parent="${parentId}"]`)).filter(val => val.checked).length

                            if (checkedChildren > 0) {
                                Object.values(document.querySelectorAll(`[name="categories"][data-parent="${parentId}"]`)).map(val => val.parentNode.parentNode.style.display = 'block')
                            } else {
                                Object.values(document.querySelectorAll(`[name="categories"][data-parent="${parentId}"]`)).map(val => val.parentNode.parentNode.style.display = 'none')
                            }
                        },
                        false
                    )
                })

                const element = document.querySelector('#list-products');
                const choices = new Choices(element, {
                    choices: products,
                    searchResultLimit: 10,
                    removeItemButton: true
                });

                async function postData(url = '', data = {}) {
                    const formData = new FormData();

                    for (let key in data) {
                        formData.append(key, data[key]);
                    }

                    const response = await fetch(url, {
                        method: 'POST',
                        body: formData
                    });

                    return response.json();
                }

                postData('<?= home_url() ?>/wp-admin/admin-ajax.php', {
                    action: 'ajaxGetProductVouchers',
                    keyword: ''
                }).then(data => {
                    choices.setChoices(data)
                });

                element.addEventListener(
                    'search',
                    function(event) {
                        postData('<?= home_url() ?>/wp-admin/admin-ajax.php', {
                            action: 'ajaxGetProductVouchers',
                            keyword: event.detail.value
                        }).then(data => {
                            choices.setChoices(data)
                        });
                    },
                    false
                );

                function submitVoucher() {
                    let tempData = {
                        id: <?= $voucherId ?>,
                        action: 'ajaxSaveVoucher',
                        target: document.getElementById('target').value,
                        name: document.getElementById('name').value,
                        code: document.getElementById('code').value,
                        description: document.getElementById('description').value,
                        free_shipping: document.getElementById('free_shipping').checked,
                        period_start_date: document.getElementById('period_start_date').value,
                        period_start_time: document.getElementById('period_start_time').value,
                        period_end_date: document.getElementById('period_end_date').value,
                        period_end_time: document.getElementById('period_end_time').value,
                        discount_type: Object.values(document.getElementsByName('discount_type')).find(val => val.checked).value,
                        amount_percent: document.getElementById('amount_percent').value,
                        amount_percent_max: parseInt(document.getElementById('amount_percent_max').value.replace(/,/gi, "")),
                        amount_fix: parseInt(document.getElementById('amount_fix').value.replace(/,/gi, "")),
                        min_spend: parseInt(document.getElementById('min_spend').value.replace(/,/gi, "")),
                        max_spend: parseInt(document.getElementById('max_spend').value.replace(/,/gi, "")),
                        quota: document.getElementById('quota').value,
                        quota_per_user: document.getElementById('quota_per_user').value,
                        products: choices.getValue(true),
                        categories: Object.values(document.querySelectorAll('[name="categories"]')).filter(val => val.checked).map(val => val.value)
                    }

                    let formData = new FormData();
                    let files = $('#banner')[0].files[0];
                    formData.append('banner', files);

                    for (let key in tempData) {
                        formData.append(key, tempData[key]);
                    }
                    $.ajax({
                        url: ajaxscript.ajaxurl,
                        type: 'POST',
                        data: formData,
                        processData: false,
                        contentType: false,
                        statusCode: {
                            200: function(xhr) {
                                location.replace(hurl + '/dashboard/manajemen-voucher/')
                            },
                            422: function(xhr) {
                                let data = JSON.parse(xhr.responseText)
                                let html = ''

                                for (prop in data.errors) {
                                    for (key of data.errors[prop]) {
                                        html += `<li>${key}</li>`
                                    }
                                }

                                $('.chk_err').empty().css('text-align', 'left').append(html).fadeIn()
                                $('html, body').animate({
                                    scrollTop: $('body').offset().top
                                }, 1000)

                                $('.fix_loading').fadeOut()
                            },
                            500: function(xhr) {
                                alert(xhr.responseText)
                            }
                        }
                    })
                }
            </script>

        <?php } else { ?>
            <script>
                // 'Getting' data-attributes using getAttribute
                var plant = document.getElementById('body');
                console.log(plant);
                var hurl = plant.getAttribute('data-hurl');
                location.replace(hurl + '/login/');
            </script>
        <?php } ?>

    <?php endwhile; ?>
<?php else : ?>
    <?php get_template_part('content', '404pages'); ?>
<?php endif; ?>
<?php get_footer('admin'); ?>