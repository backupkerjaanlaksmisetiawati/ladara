<?php
$current_admin = wp_get_current_user();

$page_id = $post->ID;
$page_post = get_post($page_id);
$page_slug = $page_post->post_name;

$active1 = '';
$active2 = '';
$active3 = '';
$active4 = '';
$active5 = '';

if (strtolower($page_slug) == 'dashboard') {
    $active1 = 'active';
} else if (strtolower($page_slug) == 'central') {
    $active2 = 'active';
} else if (strtolower($page_slug) == 'warehouse') {
    $active3 = 'active';
} else if (strtolower($page_slug) == 'completed') {
    $active4 = 'active';
} else {
    $active5 = 'active';
}

?>

<!-- Sidebar -->
<ul class="navbar-nav bg-gradient-primary sidebar sidebar-dark accordion" style="background: #060629 0% 0% no-repeat padding-box;" id="accordionSidebar">

    <!-- Sidebar - Brand -->
    <a class="sidebar-brand d-flex align-items-center justify-content-center" style="background: transparent linear-gradient(117deg, #00CF3E 0%, #0080FF 100%) 0% 0% no-repeat padding-box;" href="<?php echo home_url(); ?>/dashboard/">

        <div class="mg_LogoAdmin dashboard">
            <img src="<?php bloginfo('template_directory'); ?>/library/images/main_logo.png">
        </div>

        <!-- <div class="sidebar-brand-text mx-3">SB Admin <sup>2</sup></div> -->
    </a>

    <!-- Divider -->
    <hr class="sidebar-divider my-0">

    <!-- Nav Item - Dashboard -->
    <?php /*
      <li class="nav-item <?php echo $active1; ?>">
        <a class="nav-link" href="<?php echo home_url(); ?>/dashboard/">
          <i class="fas fa-fw fa-tachometer-alt"></i>
          <span>Dashboard</span></a>
      </li>
      */ ?>

    <!-- Divider -->
    <hr class="sidebar-divider">

    <!-- Nav Item - Tables -->
    <li class="nav-item">
        <a class="nav-link" href="<?php echo home_url(); ?>/wp-admin/">
            <i class="fas fa-arrow-alt-circle-left"></i>
            <span>Back to WP Admin</span>
        </a>
    </li>

    <!-- Heading -->
    <div class="sidebar-heading">
        Finance Admin
    </div>

    <!-- Nav Item - Tables -->
    <li class="nav-item <?php echo $active2; ?>">
        <a class="nav-link" href="<?php echo home_url(); ?>/dashboard/finance/">
            <i class="fas fa-money-bill-wave-alt"></i>
            <span>Pesanan</span>
        </a>
    </li>

    <!-- Heading -->
    <div class="sidebar-heading">
        Admin Toko
    </div>

    <li class="nav-item <?php echo $active3; ?>">
        <a class="nav-link" href="<?php echo home_url(); ?>/dashboard/daftar-merchant/">
            <i class="fas fa-users"></i>
            <span>Daftar Toko</span>
        </a>
        <a class="nav-link" href="<?php echo home_url(); ?>/dashboard/manajemen-voucher/">
            <i class="fas fa-clone"></i>
            <span>Manajemen Voucher</span>
        </a>
    </li>

    <li class="nav-item <?php echo $active3; ?>">
        <a class="nav-link" href="<?php echo home_url(); ?>/dashboard/daftar-produk/">
            <i class="fas fa-th-list"></i>
            <span>Daftar Produk</span>
        </a>
    </li>

    <li class="nav-item <?php echo $active3; ?>">
        <a class="nav-link" href="<?php echo home_url(); ?>/dashboard/admin-kategori/">
            <i class="fas fa-th-list"></i>
            <span>Kategori Produk</span>
        </a>
    </li>

    <!-- Heading -->
    <div class="sidebar-heading">
        Ladara Emas
    </div>

    <li class="nav-item <?php echo (strtolower($page_slug) == 'ladara-emas') ? "active" : "" ; ?>">
        <a class="nav-link" href="<?php echo home_url(); ?>/dashboard/ladara-emas/">
            <i class="fas fa-tachometer-alt"></i>
            <span>Dashboard</span>
        </a>
    </li>

    <li class="nav-item <?php echo (strtolower($page_slug) == 'ladara-emas-statistic') ? "active" : "" ; ?>">
        <a class="nav-link" href="<?php echo home_url(); ?>/dashboard/ladara-emas-statistic/">
            <i class="fas fa-chart-line"></i>
            <span>Stastistik</span>
        </a>
    </li>

    <li class="nav-item <?php echo (strtolower($page_slug) == 'ladara-emas-transaction') ? "active" : "" ; ?>">
        <a class="nav-link" href="<?php echo home_url(); ?>/dashboard/ladara-emas-transaction/">
            <i class="fas fa-coins"></i>
            <span>Daftar Transaksi</span>
        </a>
    </li>

    <li class="nav-item <?php echo (strtolower($page_slug) == 'ladara-emas-user') ? "active" : "" ; ?>">
        <a class="nav-link" href="<?php echo home_url(); ?>/dashboard/ladara-emas-user/">
            <i class="fas fa-user-circle"></i>
            <span>Daftar User</span>
        </a>
    </li>

    <?php if($current_admin->ID === 20) { ?>
        <li class="nav-item">
            <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="true" aria-controls="collapseTwo">
                <i class="fas fa-cog"></i>
                <span>Hit Cron Manually</span>
            </a>
            <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordionSidebar">
                <div class="bg-white py-2 collapse-inner rounded">
                    <h6 class="collapse-header">Fitur :</h6>
                    <a class="collapse-item" target="_blank" href="<?php echo home_url(); ?>/emas/manual-function?x=refresh_balance">
                        Refresh Balance
                    </a>
                    <a class="collapse-item" target="_blank" href="<?php echo home_url(); ?>/emas/manual-function?x=cek_jual">
                        Cek Jual
                    </a>
                    <a class="collapse-item" target="_blank" href="<?php echo home_url(); ?>/emas/manual-function?x=cek_beli">
                        Cek Beli
                    </a>
                    <a class="collapse-item" target="_blank" href="<?php echo home_url(); ?>/emas/manual-function?x=verifikasi">
                        Verifikasi User
                    </a>
                    <a class="collapse-item" target="_blank" href="<?php echo home_url(); ?>/emas/manual-function?x=retrieve_payment">
                        Retrieve Payment
                    </a>
                </div>
            </div>
        </li>
    <?php } ?>

    <!-- Heading -->
    <div class="sidebar-heading">
        Report Order
    </div>

    <!-- Nav Item - Tables -->
    <li class="nav-item <?php echo $active2; ?>">
        <a class="nav-link" href="<?php echo home_url(); ?>/dashboard/so-report/">
            <i class="fas fa-money-bill-wave-alt"></i>
            <span>SO Report</span>
        </a>
    </li>  

    <!-- Heading -->
    <div class="sidebar-heading">
        Top up &amp; Tagihan
    </div>

    <li class="nav-item <?php echo (strtolower($page_slug) == 'kategori-topup') ? "active" : "" ; ?>">
        <a class="nav-link" href="<?php echo home_url(); ?>/dashboard/kategori-topup/">
            <i class="fas fa-th-list"></i>
            <span>Kategori Top up</span>
        </a>
    </li>
    <?php /*
      <!-- Nav Item - Pages Collapse Menu -->
      <li class="nav-item <?php echo $active5; ?>">
        <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="true" aria-controls="collapseTwo">
          <i class="fas fa-users-cog"></i>
          <span>SUPER ADMIN</span>
        </a>
        <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordionSidebar">
          <div class="bg-white py-2 collapse-inner rounded">
            <h6 class="collapse-header">Fitur :</h6>
            <a class="collapse-item" href="<?php echo home_url(); ?>/dashboard/report-order/">Laporan Order</a>
            <a class="collapse-item" href="<?php echo home_url(); ?>/dashboard/report-mile/">Laporan Mile Poin</a>
            <a class="collapse-item" href="<?php echo home_url(); ?>/dashboard/report-member/">Member & AFiliasi</a>
            <a class="collapse-item" href="<?php echo home_url(); ?>/dashboard/report-stock/">Produk & Stock</a>
            <a class="collapse-item" href="<?php echo home_url(); ?>/dashboard/report-stock/upload/">Tambah Produk</a>
          </div>
        </div>
      </li>
      */ ?>

    <!-- Divider -->
    <hr class="sidebar-divider d-none d-md-block">

    <!-- Sidebar Toggler (Sidebar) -->
    <div class="text-center d-none d-md-inline">
        <button class="rounded-circle border-0" id="sidebarToggle"></button>
    </div>

</ul>
<!-- End of Sidebar -->