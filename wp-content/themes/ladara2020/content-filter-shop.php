<?php
if (isset($_GET['minrange'])) {
    $minrange = htmlspecialchars($_GET['minrange']);
    $_SESSION['minrange'] = $minrange;
} else {
    $minrange = '';
}

if (isset($_SESSION['minrange'])) {
    $minrange = htmlspecialchars($_SESSION['minrange']);
}

if (isset($_GET['maxrange'])) {
    $maxrange = htmlspecialchars($_GET['maxrange']);
    $_SESSION['maxrange'] = $maxrange;
} else {
    $maxrange = '';
}

if (isset($_SESSION['maxrange'])) {
    $maxrange = htmlspecialchars($_SESSION['maxrange']);
}

?>

<div class="filter_shops animated">
	
	<div class="ht_subfitler2 mob_display">Filter <span class="hide_mobfilter">tutup</span></div>

	<div class="bx_subfilter_sp">
		<div class="ht_subfitler">Rentangan Harga</div>

		<div class="f_subfilter">
			<span>Rp</span>
			<input id="minrange" type="text" class="txt_subfilter onlynumber" name="minrange" placeholder="Minimum" minlength="1" maxlength="10" required="required" value="<?php echo $minrange; ?>">
			<div id="err_minimum" class="f_err"></div>
		</div>

		<div class="f_subfilter">
			<span>Rp</span>
			<input id="maxrange" type="text" class="txt_subfilter onlynumber" name="maxrange" placeholder="Maximum" minlength="1" maxlength="10" required="required" value="<?php echo $maxrange; ?>">
			<div id="err_minimum" class="f_err"></div>
		</div>

		<input type="button" class="sub_subfilter get_filterPrice" value="Terapkan Harga">
		
	</div>

	<div class="bx_subfilter_sp second_subfilter">
		<div class="ht_subfitler">Pilihan Brand</div>

		<div id="v_subfilter_brand" class="bx_subfilter_in act animated slideInUp">
			<?php /*
	        $args = array(
	            'post_type' => 'brand',
	            'posts_per_page' => -1,
	            'post_status' => 'publish',
	            'orderby' => 'title',
	            'order' => 'ASC',
	        );
	            $the_query = new WP_Query($args);
	            while ($the_query->have_posts()) : $the_query->the_post();
	                $id_brand = get_the_ID();
	                $title_brand = get_the_title($id_brand);
	        ?>
				<div class="f_aform f_aform_brand">
					<label class="cont_check"><?php echo $title_brand; ?>
					<input type="checkbox" name="filter_brand" class="check_catVoucher click_brand" value="<?php echo $id_brand; ?>" >
					<span class="checkmark"></span>
					</label>
				</div>		
			<?php
	            endwhile;
	            wp_reset_query(); */
	        ?>
		</div>

		<input type="button" class="sub_subfilter get_filterPrice2 mob_display" value="Terapkan Brand">

	</div>

</div>

<div class="fix_btnfilter_mob mob_display">
	
	<a id="open_mob_shopsort" class="a_mobfil_urutkan">Urutkan</a> <span>|</span> <a id="open_mob_shopfilter" class="a_mobfil_filter">Filter</a>

</div>