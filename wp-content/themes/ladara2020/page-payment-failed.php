<?php
/*
Template Name: Payment Failed Page
*/
?>

<?php get_header(); ?>
<?php if (have_posts()) : while (have_posts()) : the_post(); ?>

<?php 
$current_user = wp_get_current_user();
$u_id = $current_user->ID;
?>

<div class="row row_masterCart">
    <div class="col-md-1"></div>
    <div class="col-md-10 bx_emptyCart">
        <div class="mg_emptyCart">
            <img src="<?php bloginfo('template_directory'); ?>/library/images/empty_cart.png">
        </div>
        <h1 class="ht_emptyCart">Duh Maaf,</h1>
        <h2 class="tx_emptyCart">Pembayaranmu Gagal. <br/>Yuk lihat status pesanan ini di halaman order kamu.</h2>
        <a href="<?php echo home_url(); ?>/my-order/">
            <button class="btn_default btn_emptyCart">Lihat Pesanan Saya</button>
        </a>
    </div>
    <div class="col-md-1"></div>
</div>

<?php // ============= cannot back ============= ?>
<script type="text/javascript">
    (function (global) { 

        if(typeof (global) === "undefined") {
            throw new Error("window is undefined");
        }

        var _hash = "!";
        var noBackPlease = function () {
            global.location.href += "#";

            // making sure we have the fruit available for juice (^__^)
            global.setTimeout(function () {
                global.location.href += "!";
            }, 50);
        };

        global.onhashchange = function () {
            if (global.location.hash !== _hash) {
                global.location.hash = _hash;
            }
        };

        global.onload = function () {            
            noBackPlease();

            // disables backspace on page except on input fields and textarea..
            document.body.onkeydown = function (e) {
                var elm = e.target.nodeName.toLowerCase();
                if (e.which === 8 && (elm !== 'input' && elm  !== 'textarea')) {
                    e.preventDefault();
                }
                // stopping event bubbling up the DOM tree..
                e.stopPropagation();
            };          
        }

    })(window);
</script>

<?php endwhile; ?>
<?php else : ?>
		<?php get_template_part( 'content', '404pages' ); ?>	
<?php endif; ?>
<?php get_footer(); ?>