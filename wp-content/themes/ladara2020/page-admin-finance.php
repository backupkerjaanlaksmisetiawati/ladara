<?php
/*
Template Name: Finance Dashboard Page
*/
?>

<?php get_header('admin'); ?>

<?php
if (have_posts()) :
    while (have_posts()) :
        the_post();

        global $wpdb;
        $currentUser = wp_get_current_user();
        $userId = $current_user->ID;
        $userRoles = $current_user->roles;
        $admin = 0;

        if (isset($userRoles) and !empty($userRoles)) {
            foreach ($userRoles as $key => $value) {
                if (strtolower($value) == 'administrator') {
                    $admin = 1;
                } elseif (strtolower($value) == 'shop_manager') {
                    $admin = 1;
                }
            }
        }

        $listStatus = [
            -2 => 'Pesanan Ditolak',
            -1 => 'Pesanan Dibatalkan',
            0 => 'Menunggu Pembayaran',
            1 => 'Menunggu Konfirmasi Merchant',
            2 => 'Pesanan Diproses',
            3 => 'Pesanan Dalam Pengiriman',
            4 => 'Pesanan Selesai'
        ];

        if ((isset($userId) and $userId != 0) and $admin == 1) {
            $status = isset($_GET['status']) ? htmlspecialchars($_GET['status']) : 'All';
            $startDate = isset($_GET['startDate']) && $_GET['startDate'] !== '' ? htmlspecialchars($_GET['startDate']) : date('Y-01-01');
            $endDate = isset($_GET['endDate']) && $_GET['endDate'] !== '' ? htmlspecialchars($_GET['endDate']) : date('Y-12-31');

            $startDate .= ' 00:00:00';
            $endDate .= ' 23:59:59';

            $sqlQueryOrders = "SELECT * FROM ldr_orders LEFT JOIN ldr_users ON ldr_orders.user_id = ldr_users.id WHERE ldr_orders.deleted_at IS NULL AND created_at BETWEEN '$startDate' AND '$endDate'";

            if ($status !== 'All') {
                $sqlQueryOrders .= " AND ldr_orders.id IN (SELECT order_id FROM ldr_order_merchants WHERE status = $status)";
            }

            $queryOrders = $wpdb->get_results($sqlQueryOrders);
?>

            <div class="container-fluid" id="ladara-emas-dashboard">
                <h1 class="h3 mb-4 text-gray-800">
                    Daftar Transaksi
                    <br />
                    <span style="font-size:1rem;">
                        <?= date('d F Y', strtotime($startDate)) ?> &mdash; <?= date('d F Y', strtotime($endDate)) ?>
                    </span>
                </h1>

                <!-- DataTales Example -->
                <div class="card shadow mb-4">
                    <div class="card-header py-3">
                        <div style="display: flex;">
                            <div style="width: 50%;">
                                <h6 class="m-0 font-weight-bold text-primary ht_admdash">
                                    Search
                                </h6>
                            </div>
                            <div style="width: 50%;" align="right">
                                <button id="btn-download-report" type="button" class="btn btn-success">Download</button>
                            </div>
                        </div>
                        <form>
                            <div class="row">
                                <div class="col-sm-12 col-md-3">
                                    <label>Status</label>
                                    <select name="status" class="jsselectHideSearch">
                                        <option value="All" <?= $status === 'All' ? 'selected' : '' ?>>Semua</option>
                                        <option value="-2" <?= $status === '-2' ? 'selected' : '' ?>>Pesanan Ditolak</option>
                                        <option value="-1" <?= $status === '-1' ? 'selected' : '' ?>>Pesanan Dibatalkan</option>
                                        <option value="0" <?= $status === '0' ? 'selected' : '' ?>>Menunggu Pembayaran</option>
                                        <option value="1" <?= $status === '1' ? 'selected' : '' ?>>Menunggu Konfirmasi Merchant</option>
                                        <option value="2" <?= $status === '2' ? 'selected' : '' ?>>Pesanan Diproses</option>
                                        <option value="3" <?= $status === '2' ? 'selected' : '' ?>>Pesanan Dalam Pengiriman</option>
                                        <option value="4" <?= $status === '2' ? 'selected' : '' ?>>Pesanan Selesai</option>
                                    </select>
                                </div>
                                <div class="col-sm-12 col-md-6">
                                    <label>Tanggal Transaksi</label>
                                    <div class="row range-date">
                                        <div class="no-margin date">
                                            <input type="text" name="startDate" id="period_start_date" value="<?= isset($_GET['startDate']) ? $startDate : '' ?>" placeholder="Tanggal Awal" />
                                        </div>
                                        <div class="no-margin">
                                            <p class="no-margin">-</p>
                                        </div>
                                        <div class="no-margin date">
                                            <input type="text" name="endDate" id="period_end_date" value="<?= isset($_GET['endDate']) ? $endDate : '' ?>" placeholder="Tanggal Akhir" />
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-12 col-md-3" style="margin-top: 40px;">
                                    <button type="submit" name="action" value="search" class="btn_ca_confirm gldcsell">Search</button>
                                    <a href="<?php echo home_url() . "/dashboard/finance"; ?>" class="btn_ca_confirm gldcsell see-all">See All</a>
                                </div>
                            </div>
                        </form>
                    </div>
                    <div class="card-body">
                        <div class="table-responsive">
                            <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0" data-order="[[ 0, &quot;desc&quot; ]]">
                                <thead>
                                    <tr>
                                        <th>Order ID</th>
                                        <th>Xendit ID</th>
                                        <th>Shipper ID</th>
                                        <th>Customer (User ID)</th>
                                        <th>Order Date</th>
                                        <th>Total</th>
                                        <th>Status</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                    foreach ($queryOrders as $order) {
                                        $queryOrderMerchants = $wpdb->get_results("SELECT * FROM ldr_order_merchants WHERE deleted_at IS NULL AND order_id = {$order->id}");
                                        $paymentLog = $wpdb->get_row("SELECT * FROM ldr_payment_log WHERE order_id = {$order->id}");

                                        foreach ($queryOrderMerchants as $orderMerchant) {
                                            $totalPayment = 0;

                                            $orderItems = $wpdb->get_results("SELECT * FROM ldr_order_items WHERE deleted_at IS NULL AND order_merchant_id = {$orderMerchant->id}");

                                            foreach ($orderItems as $orderItem) {
                                                $totalPayment += (intval($orderItem->price) * intval($orderItem->qty));
                                            }

                                            $totalPayment += intval($orderMerchant->shipping_price) + intval($orderMerchant->insurance_price) + ($order->xendit_fee / count($queryOrderMerchants));
                                    ?>
                                            <tr>
                                                <td><?= $orderMerchant->id ?></td>
                                                <td><?= $paymentLog->external_id ?></td>
                                                <td><?= $orderMerchant->shipper_tracking_id ?></td>
                                                <td><?= $order->display_name . ' ( ' . $order->user_id . ' )'; ?></td>
                                                <td><?= date('d-m-Y', strtotime($orderMerchant->created_at)) ?></td>
                                                <td align="right">
                                                    <b>Rp <?= number_format($totalPayment); ?></b>
                                                </td>
                                                <td>
                                                    <?= $listStatus[intval($orderMerchant->status)] ?>
                                                </td>
                                                <td>
                                                    <a href="<?= home_url('/dashboard/finance/detail?id=') . $orderMerchant->id ?>">
                                                        <button class="btn_ca_confirm" title="Lihat Detail Order.">
                                                            Lihat
                                                        </button>
                                                    </a>
                                                </td>
                                            </tr>
                                    <?php
                                        }
                                    }
                                    ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
            <script>
                document.querySelector('#btn-download-report').addEventListener(
                    'click',
                    function(event) {
                        let startDate = document.querySelector('#period_start_date').value
                        let endDate = document.querySelector('#period_end_date').value

                        const formData = new FormData()
                        formData.append('action', 'ajaxDownloadOrderReport')
                        formData.append('start_date', startDate)
                        formData.append('end_date', endDate)

                        fetch(ajaxscript.ajaxurl, {
                                method: 'POST',
                                body: formData
                            })
                            .then(resp => resp.blob())
                            .then(blob => {
                                const url = window.URL.createObjectURL(blob)
                                const a = document.createElement('a')
                                a.style.display = 'none'
                                a.href = url
                                a.download = 'Order Report.xlsx'
                                document.body.appendChild(a)
                                a.click()
                                window.URL.revokeObjectURL(url)
                            })
                            .catch(() => alert('Terjadi kesalahan sistem.'));
                    },
                    false
                )
            </script>
        <?php
        } else { ?>
            <script>
                // 'Getting' data-attributes using getAttribute
                var plant = document.getElementById('body');
                console.log(plant);
                var hurl = plant.getAttribute('data-hurl');
                location.replace(hurl + '/login/');
            </script>
        <?php } ?>

    <?php endwhile; ?>
<?php else : ?>
    <?php get_template_part('content', '404pages'); ?>
<?php endif; ?>

<?php get_footer('admin'); ?>

<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/flatpickr/dist/flatpickr.min.css">
<script src="https://cdn.jsdelivr.net/npm/flatpickr"></script>
<script>
    flatpickr("#period_start_date, #period_end_date");
</script>