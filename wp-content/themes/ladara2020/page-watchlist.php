<?php
/*
Template Name: Watchlist Page
*/
?>

<?php get_header(); ?>
<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
<?php 
date_default_timezone_set("Asia/Jakarta");
$nowdate = date('Y-m-d');
$now_time = strtotime($nowdate);

$current_user = wp_get_current_user();
$u_id = $current_user->ID;
$user_login = $current_user->user_login;
$nama_lengkap = get_field('nama_lengkap', 'user_'.$u_id);

if(isset($u_id) AND $u_id != 0){

$offset = 0;
$limit_item = 30;
if(isset($_GET['pg'])){
	$page = $_GET['pg'];
	$page_now = (int)$page-1;
	$offset = (int)$page_now*(int)$limit_item;
}else{
	$page = 1;
	$offset = (int)$page*0;
}
$keyword = '';

global $wp;
$cur_page = home_url( $wp->request );
?>

<div class="row"></div>

<input type="hidden" name="keyword_s" value="<?php echo $keyword; ?>">
<input type="hidden" name="offset" value="<?php echo $offset; ?>">
<input type="hidden" name="limit" value="<?php echo $limit_item; ?>">
<input type="hidden" name="pagination" value="<?php echo $page; ?>">
<input type="hidden" name="page_url" value="<?php echo $cur_page; ?>">

<div id="watchlistpage" class="row row_profile">
	<div class="col-md-3 col_profile des_display">
		
		<?php get_template_part( 'content', 'menu-profile' ); ?>

	</div>
	<div class="col-md-9 col_profile">
		
		<div class="row row_cont_tab_profile">
			<h1 class="ht_profile ht_myvoucher">Wishlist</h1>

			<div id="view_resProduct" class="row row_wishlist">

			</div>

			<div class="row row_wishlist">
				<div class="col-md-12 col_pagination_shop view_pagi_shops">
					<?php // view pagi shops my ajax ?>

					<?php /*
					global $wpdb;
					$wpdb_query = "SELECT * 
									FROM ldr_wishlist
									WHERE user_id = '$u_id'
									ORDER BY product_id DESC
									LIMIT $limit_item OFFSET $offset";
					$res_query = $wpdb->get_results($wpdb_query, OBJECT);
					$count_res = count($res_query);

					if($count_res > 0){
						foreach ($res_query as $key => $value){
							$valid = 1;
							$product_id = $value->product_id;
							$product = wc_get_product( $product_id );

							$product_name = get_the_title($product_id);
							$short_name = get_the_title($product_id);
							if(strlen($short_name) > 35) $short_name = substr($short_name, 0, 35).'...';
							$product_link = get_the_permalink($product_id);
							$product_price = $product->get_regular_price();
							$product_sale = $product->get_sale_price();

							$thumb = wp_get_attachment_image_src( get_post_thumbnail_id($product_id), 'medium' );
							if($thumb){
								$urlphoto = $thumb['0'];
							}else{
								$urlphoto = get_template_directory_uri().'/library/images/default_sorry.jpg';
							}
							$def_image = get_template_directory_uri().'/library/images/default_sorry.jpg';

							$alt = 'Ladara Indonesia Gallery';
					?>

							<div class="col-md-4 col_bx_productList">
								<div class="bx_productList">
									<?php 
									if ($u_id != 0) {
										global $wpdb;
										$wpdb_query = "SELECT * FROM ldr_wishlist WHERE user_id = '$u_id' AND product_id = '$product_id' ";
										$res_query = $wpdb->get_results($wpdb_query, OBJECT);
										$count_res = count($res_query);
										if($count_res == 0){
											$love_1 = 'act';
											$love_2 = '';
									?>
											<div class="mg_heart addWishlist love_<?php echo $product_id; ?>" title="Tambah ke wishlist" data-id="<?php echo $product_id; ?>">
												<span class="glyphicon glyphicon-heart"></span>
											</div>
									<?php 
										}else{
											$love_2 = 'act';	
									?>
											<div id="btn_" class="mg_heart removeWishlist love_<?php echo $product_id; ?> act" title="Hapus dari wishlist" data-id="<?php echo $product_id; ?>" >
												<span class="glyphicon glyphicon-heart"></span>
											</div>
									<?php
										}
									}
									?>
									<a href="<?php echo $product_link; ?>" title="Lihat <?php echo $product_name; ?>">
										<div class="mg_productList">
											<img class="lazy" src="<?php echo $def_image; ?>" data-src="<?php echo $urlphoto; ?>" alt="<?php echo $alt; ?>">
										</div>
										<div class="bx_in_productList">
											<h4 class="ht_productList"><?php echo $short_name; ?></h4>
											<?php if(isset($product_sale) AND $product_sale != ''){ ?>
												<div class="tx_price">Rp <?php echo number_format($product_sale); ?></div>
												<div class="tx_price_sale">Rp <?php echo number_format($product_price); ?></div>
											<?php }else{ ?>
												<div class="tx_price">Rp <?php echo number_format($product_price); ?></div>
											<?php } ?>
											<div class="bx_rate_product">
												<?php 
													$pro_rate = get_field('product_rate',$product_id);
													$total_product_rate = get_field('total_product_rate',$product_id);
													$count_rate = $total_product_rate;
													$full_star = $pro_rate;
													$empty_star = 5;
													if($count_rate != 0){
														for ($a=1; $a <= $empty_star; $a++) { 

															if($a <= $full_star){
															?>
																<img src="<?php bloginfo('template_directory'); ?>/library/images/star1.svg">
															<?php
															}else{
															?>
																<img src="<?php bloginfo('template_directory'); ?>/library/images/star0.svg">
															<?php
															}
														} 
													?>
														<span class="sp_rate_info">(<?php echo $count_rate; ?>)</span>
													<?php
													}
												?>
											</div>
										</div>
									</a>	
								</div>
							</div>
					<?php
						}
					} */
				 	?>


				</div>
			</div>

		</div>

	</div>
</div>


<?php }else{ ?>
    <script>
        // 'Getting' data-attributes using getAttribute
        var plant = document.getElementById('body');
        console.log(plant);
        var hurl = plant.getAttribute('data-hurl'); 
        location.replace(hurl+'/login/'); 
    </script>
<?php } ?>

<?php endwhile; ?>
<?php else : ?>
		<?php get_template_part( 'content', '404pages' ); ?>	
<?php endif; ?>
<?php get_footer(); ?>