<!doctype html>
<!--[if lt IE 7]><html <?php language_attributes(); ?> class="no-js lt-ie9 lt-ie8 lt-ie7"><![endif]-->
<!--[if (IE 7)&!(IEMobile)]><html <?php language_attributes(); ?> class="no-js lt-ie9 lt-ie8"><![endif]-->
<!--[if (IE 8)&!(IEMobile)]><html <?php language_attributes(); ?> class="no-js lt-ie9"><![endif]-->
<!--[if gt IE 8]><!-->
<html <?php language_attributes(); ?> class="no-js">
<!--<![endif]-->

<head>
    <meta charset="utf-8">
    <?php // Google Chrome Frame for IE 
    ?>
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">

    <?php 
        $actual_link = (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? "https" : "http") . "://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
        
        $REQUEST_URI = explode("/", $_SERVER['REQUEST_URI']);
        $status_meta_product = 0;
        $h_pro_name = '';
        // ===== untuk product detail =====
        foreach ($REQUEST_URI as $key_uri => $value_uri) {
            if(strtolower($value_uri) AND $value_uri == 'product'){
                
                $exSlug = explode('-', $actual_link);
                $pro_id = end($exSlug);
                if(isset($pro_id) AND $pro_id != ''){
                    global $wpdb;
                    $wpdb_query_uri = "SELECT * FROM ldr_products WHERE id = '$pro_id' ";
                    $res_query_uri = $wpdb->get_results($wpdb_query_uri, OBJECT);
                    $count_res_uri = count($res_query_uri);
                    if($count_res_uri > 0){
                        foreach ($res_query_uri as $key_pro => $value_pro) {
                            $h_pro_name = $value_pro->title;
                            $h_pro_name = 'Jual '.$h_pro_name;
                            $status_meta_product = 1;
                        }
                    }
                }
            }
        }
        // ===== untuk product category =====
        if ( strstr( $actual_link, '?category=' ) ) {
            $exSlug = explode('?category=', $actual_link);
            $h_pro_cat = $exSlug[1];
            if(isset($h_pro_cat) AND $h_pro_cat != ''){
                $h_pro_cat = str_replace('-', ' ', $h_pro_cat);
                $h_pro_name =  'Produk Kategori '.$h_pro_cat;
                $status_meta_product = 1;
            }
        }
    ?>
    <?php
        if($status_meta_product == 1){
        ?>
            <title><?php echo $h_pro_name; ?></title>
        <?php
        }else{
        ?>
            <title><?php wp_title(''); ?></title>
        <?php
        }
    ?>
    <?php // mobile meta (hooray!) 
    ?>
    <meta name="HandheldFriendly" content="True">
    <meta name="MobileOptimized" content="320">
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <?php // icons & favicons (for more: http://www.jonathantneal.com/blog/understand-the-favicon/) 
    ?>
    <link rel="apple-touch-icon" href="<?php echo get_template_directory_uri(); ?>/library/images/apple-icon-touch.png">
    <link rel="icon" href="<?php echo get_template_directory_uri(); ?>/favicon-min.png">
    <!--[if IE]>
		<link rel="shortcut icon" href="<?php echo get_template_directory_uri(); ?>/favicon.ico">
	<![endif]-->
    <?php // or, set /favicon.ico for IE10 win 
    ?>
    <meta name="msapplication-TileColor" content="#f01d4f">
    <meta name="msapplication-TileImage" content="<?php echo get_template_directory_uri(); ?>/library/images/win8-tile-icon.png">
    <link rel="pingback" href="<?php bloginfo('pingback_url'); ?>">
    <?php // Put you url link font here....  
    ?>
    <link href="https://fonts.googleapis.com/css?family=Montserrat:300,400,400i,500,600,700&display=swap" rel="stylesheet">
    <!-- <script src="https://kit.fontawesome.com/45b63cdda3.js" crossorigin="anonymous"></script> -->
    
    <meta name="ahrefs-site-verification" content="5c4c03b1d12a3b12a41d668025f5f9e489b0765d7cfd48b293502b92e2813dc8">

    <?php // Insert your css & jss only at function.php 
    ?>
    <?php // wordpress head functions 
    ?>
    <?php wp_head(); ?>
    <?php // end of wordpress head 
    ?>
    <?php // drop Google Analytics Here 
    ?>
    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-167113692-1"></script>
    <script>
        window.dataLayer = window.dataLayer || [];

        function gtag() {
            dataLayer.push(arguments);
        }
        gtag('js', new Date());

        gtag('config', 'UA-167113692-1');
    </script>
    <?php // end analytics 
    ?>

</head>

<body id="body" <?php body_class(); ?> itemscope itemtype="http://schema.org/WebPage" data-hurl="<?php echo home_url(); ?>" data-merchant_hurl="<?php echo constant('MERCHANT_URL'); ?>">
    <?php

    use Ladara\Models\Notifications;
    global $wpdb;

    $current_user = wp_get_current_user();
    $u_id = $current_user->ID;
    if ($u_id == '') {
        $u_id = 0;
    }
    ?>

    <?php
    $u_roles = $current_user->roles;
    $admin = 0;

    if (isset($u_roles) and !empty($u_roles)) {
        foreach ($u_roles as $key => $value) {
            // if(strtolower($value) == 'administrator'){
            //     $admin = 1;
            // }
            if (strtolower($value) == 'shop_manager') {
                $admin = 1;
            }
        }
    }

    if ($admin == 1) {
    ?>
        <script>
            // 'Getting' data-attributes using getAttribute
            var plant = document.getElementById('body');
            console.log(plant);
            var hurl = plant.getAttribute('data-hurl');
            location.replace(hurl + '/dashboard/finance/');
        </script>
    <?php
    }

    if (isset($_GET['mobile']) and $_GET['mobile'] != '') {
        $yes_mobile = 1;
    } else {
        $yes_mobile = 0;
    }
    ?>

    <div id="container">
        <?php if ($yes_mobile == 0) : ?>
            <header class="header" role="banner">
                <div class="row bx_head_top">
                    <div class="col-md-6 col-lg-6 col_head_top">
                        <ul class="ul_head_top ul_left">
    						<li id="open_download_apps"><a class="a_download_apps"><i class="fas fa-mobile-alt"></i> <span>Download LaDaRa App</span> <span class="glyphicon glyphicon-menu-down sp_download_apps"></span> </a> </li>
    						<!-- <li><a href="<?php // echo home_url(); ?>/shop/">Promo Terbaru</a></li> -->
                        </ul>

                        <div id="box_hd_download_apps" class="bx_hd_download_apps animated">
                            <?php 
                                $link_android = get_field('link_android',2);
                                if(isset($link_android) AND $link_android != ''){
                            ?>
                                    <div class="lft_dwl_apps">
                                        <a target="_blank" class="a_dwl_apps" href="<?php echo $link_android; ?>" title="Unduh Aplikasi Ladara di Google Play">
                                            <div class="mg_dwl_apps">
                                                <img src="<?php bloginfo('template_directory'); ?>/library/images/ico_android.svg">
                                            </div>
                                            <div class="btn_dwl_apps">Android</div>
                                        </a>
                                    </div>
                            <?php } ?>
                            <?php 
                                $link_apple = get_field('link_apple',2);
                                if(isset($link_apple) AND $link_apple != ''){
                            ?>
                                    <div class="rgt_dwl_apps">
                                        <a target="_blank" class="a_dwl_apps" href="<?php echo $link_apple; ?>" title="Unduh Aplikasi Ladara di App Store">
                                            <div class="mg_dwl_apps">
                                                <img src="<?php bloginfo('template_directory'); ?>/library/images/ico_apple.svg">
                                            </div>
                                            <div class="btn_dwl_apps">iOS</div>
                                        </a>
                                    </div>
                            <?php } ?>
                        </div>

                    </div>
                    <div class="col-md-6 col-lg-6 col_head_top">
                        <ul class="ul_head_top ul_right">
                            <!-- <li><a href="javascript:;">Pusat Bantuan <span class="glyphicon glyphicon-menu-down"></span></a></li> -->
                        </ul>
                    </div>
                </div>
                <div id="myHeaderDesktop" class="row bx_head_middle bx_web_head_middle">
                    <div class="col-xs-2 col-md-2 col_mn_logo">
                        <a href="<?php echo home_url(); ?>" title="Home - Ladara Indonesia">
                            <div class="mg_masterLogo animated jello">
                                <img src="<?php bloginfo('template_directory'); ?>/library/images/main_logo.png">
                            </div>
                        </a>
                    </div>

                    <div class="col-xs-2 col-md-1 col_mn_cat">
                        <div id="open_topmenu" class="tx_openMenu" title="Lihat semua kategori">
                            <img src="<?php bloginfo('template_directory'); ?>/library/images/menu_kategori.svg">
                            Kategori
                        </div>
                        <?php  // =================== Get All Category ================== ?>
                        <div class="row fix_catMenu animated fadeIn">
                            <div id="v_catMenu" class="bx_catMenu">
                                <?php // ajax get category here... ?>
                                <div class="row">
                                    <div class="col-md-4 col_catMenu">
                                        <div class="bx_catMenu_cont">
                                            <div class="ht_ul_catMenu mob_display">
                                                Belanja 
                                                <span class="hide_sup_catMenu">tutup</span>
                                            </div>

                                            <ul class="ul_catMenu">
                                                <?php
                                                $all_parent = array();
                                                $all_catID = array();
                                                $query = "SELECT * FROM ldr_categories where parent = 0 AND slug != 'emas'";
                                                $all_catVoucher = $wpdb->get_results($query);
                                                $op = 0;
                                                ?>
                                                <?php foreach ($all_catVoucher as $key => $value) { ?>
                                                    <?php
                                                    $op++;
                                                    $ido = $value->id;
                                                    $name_cat = $value->name;
                                                    $slug_cat = $value->slug;
                                                    $all_catID[] = array('par_id' => $ido, 'par_name' => $name_cat);

//                                                    $cat_image = home_url().'/'.$value->image;
                                                    $cat_image = IMAGE_URL.'/'.$value->image;
                                                    ?>
                                                    <?php if ($op == 1) { ?>
                                                        <li class="act" data-id="tcap_<?php echo $ido; ?>">
                                                            <a>
                                                                <div class="mg_catMenu">
                                                                    <img src="<?php echo $cat_image; ?>">
                                                                </div>
                                                                <?php echo $name_cat; ?>
                                                            </a>
                                                        </li>
                                                    <?php } else { ?>
                                                        <li class="" data-id="tcap_<?php echo $ido; ?>">
                                                            <a>
                                                                <div class="mg_catMenu">
                                                                    <img src="<?php echo $cat_image; ?>">
                                                                </div> 
                                                                <?php echo $name_cat; ?>
                                                            </a>
                                                        </li>
                                                    <?php } ?>
                                                <?php } ?>
                                                <li class="">
                                                    <a href="<?php echo home_url(); ?>/emas/">
                                                        <div class="mg_catMenu">
                                                            <img src="<?php echo IMAGE_URL; ?>/wp-content/uploads/2020/04/Ladara-Emas.png">
                                                        </div> 
                                                        Ladara Emas
                                                    </a>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                    
                                    <div class="col-md-8 col_catMenu2 col_cont_catMenu">
                                        <?php $op = 0; ?>
                                        <?php foreach ($all_catID as $key => $value) { ?>
                                            <?php 
                                            $op++;
                                            $parent_ID = $value['par_id'];
                                            $parent_name = $value['par_name'];
                                            ?>
                                            <?php if ($op == 1) { ?>
                                            <div id="tcap_<?php echo $parent_ID; ?>" class="box_tsMenu act animated fadeIn">
                                            <?php } else { ?>
                                            <div id="tcap_<?php echo $parent_ID; ?>" class="box_tsMenu animated fadeIn">
                                            <?php } ?>
                                                <div class="ht_catMenu"><?php echo $parent_name; ?>
                                                    <span class="hide_catMenu mob_display">tutup</span>
                                                </div>

                                                <div class="bx_cont_catMenu">

                                                    <?php
                                                    $query = "SELECT * FROM ldr_categories where parent = $parent_ID";
                                                    $all_catVoucher = $wpdb->get_results($query);
                                                    ?>

                                                    <?php foreach ($all_catVoucher as $key => $value) { ?>
                                                        <?php
                                                        $cat_id = $value->id;
                                                        $cat_name = $value->name;
                                                        $cat_slug = $value->slug;
                                                        ?>

                                                        <div class="col_in_catMenu">
                                                            <a href="<?php echo home_url(); ?>/product?category=<?php echo $cat_slug.'-'.$cat_id; ?>">
                                                                <div class="head_catMenu">
                                                                    <?php echo $cat_name; ?>
                                                                </div>
                                                            </a>

                                                            <?php
                                                            $query = "SELECT * FROM ldr_categories where parent = $cat_id";
                                                            $all_catVoucher = $wpdb->get_results($query);

                                                            $count_child = count($all_catVoucher);
                                                            ?>

                                                            <?php if ($count_child > 0) { ?>
                                                                <ul class="ul_cont_catMenu child_catMenu">
                                                                    <?php foreach ($all_catVoucher as $key => $value) { ?>
                                                                        <?php
                                                                        $child_id = $value->id;
                                                                        $child_name = $value->name;
                                                                        $child_slug = $value->slug;
                                                                        ?>
                                                                        <li>
                                                                            <a href="<?php echo home_url(); ?>/product?category=<?php echo $child_slug.'-'.$child_id; ?>">
                                                                                <?php echo $child_name; ?>
                                                                            </a>
                                                                        </li>
                                                                    <?php } ?>
                                                                </ul>
                                                            <?php } ?>
                                                        </div>
                                                    <?php } ?>
                                                </div>
                                            </div>
                                        <?php } ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <?php  // =================== Get All Category ================== ?>
                    </div>

                    <div class="col-xs-8 <?php if (isset($u_id) and $u_id != '' and $u_id != 0) : ?>col-md-5<?php else : ?>col-md-6<?php endif; ?> col_mn_search">
                        <?php
                        if (isset($_GET['keyword'])) {
                            $keyword = htmlspecialchars($_GET['keyword']);
                        } else {
                            $keyword = '';
                        }
                        ?>
                        <form method="get" id="form_searching" action="<?php echo home_url(); ?>/product/">
                            <input id="searchNow" type="text" name="keyword" class="txt_searchNow" placeholder="Cari produk atau brand yuk!" value="<?php echo $keyword; ?>">
                            <span class="glyphicon glyphicon-search sp_searchNow submit-search" title="Cari sekarang."></span>
                            <div class="res_tagsSearch">
                                <ul id="show_tagsSearch" class="ul_tagsSearch">
                                </ul>
                            </div>
                            <button type="submit" class="sub_search hide" form="form_searching" value="Submit"><img src="<?php bloginfo('template_directory'); ?>/library/images/logo-cari.png"> Cari</button>
                        </form>
                    </div>
                    
                    <div class="col-xs-2 <?php if (isset($u_id) and $u_id != '' and $u_id != 0) : ?>col-md-4<?php else : ?>col-md-3<?php endif; ?> col_mn_profile">
                        <?php $count_cart = get_itemCart_count(); ?>

                        <div class="bx_mg_1 bx_web_notifcart">
                            <a class="mg_cart a_open_mnCart" title="Lihat keranjang belanja" id="a_open_mnCart" data-cart="<?php echo $count_cart; ?>">
                                <img src="<?php bloginfo('template_directory'); ?>/library/images/cart.svg">
                                <span id="v_cart"><?php echo $count_cart; ?></span>
                            </a>

                            <div class="row fix_cartMenu">
                                <div id="v_listcart" class="bx_cartMenu" style="right: 12%;">
                                    <?php // ajax get cart item here... 
                                    ?>
                                </div>
                            </div>
                        </div>

                        <?php $count_actNotif = 0; ?>

                        <?php if (isset($u_id) and $u_id != '' and $u_id != 0) { ?>
                            <?php
                                $dataMerchant = getMerchant($u_id);
                                if (isset($current_user->display_name)) {
                                    $display_name = $current_user->display_name;
                                    $data_name = explode(" ", $display_name);
                                    $display_name = $data_name[0];
                                } else {
                                    $display_name = 'customer';
                                }

                                $user_avatar = get_field('user_avatar', 'user_' . $u_id);
                                if (isset($user_avatar) and $user_avatar != '') {
                                    $linkpoto = wp_get_attachment_image_src($user_avatar, 'thumbnail');
                                    $url_avatar = $linkpoto[0];
                                } else {
                                    $url_avatar = get_template_directory_uri() . '/library/images/icon_profile.png';
                                }
                                
                                date_default_timezone_set("Asia/Jakarta");
                                $nowdate = date('Y-m-d H:i:s');
                                $d_now = date('d');

                                $res_notif = Notifications::getNotifications($u_id, 1);

                                $notifPenjualan = [];
                                if($dataMerchant !== null){
                                    $notifPenjualan = Notifications::getNotifications($u_id, 1, $dataMerchant->id ?? 0);
                                }

                                if (isset($res_notif['data'])) {
                                    $count_notif = count($res_notif['data']);
                                    foreach ($res_notif['data'] as $key => $value) {
                                        $notif_status = $value['status'];
                                        if (isset($notif_status) and $notif_status == 1) {
                                        } else {
                                            $count_actNotif++;
                                        }
                                    }
                                }

                                if (isset($notifPenjualan['data'])) {
                                    $count_notif = count($notifPenjualan['data']);
                                    foreach ($notifPenjualan['data'] as $key => $value) {
                                        $notif_status = $value['status'];
                                        if (isset($notif_status) and $notif_status == 1) {
                                        } else {
                                            $count_actNotif++;
                                        }
                                    }
                                }
                            ?>

                            <div class="bx_mg_4 bx_web_notifcart" id="a_open_mnNotif">
                                <a class="a_notification" title="Buka notifikasi">
                                    <span class="glyphicon glyphicon-bell"></span>
                                    <?php if ($count_actNotif > 0) { ?>
                                        <span><?php echo $count_actNotif; ?></span>
                                    <?php } ?>
                                </a>
                            </div>

                            <?php if (isset($u_id) and $u_id != '' and $u_id != 0) { ?>
                                <a <?php if ($dataMerchant !== null) { ?>href="<?php echo constant('MERCHANT_URL'); ?>"<?php } ?> class="bx_web_merchant" <?php if ($dataMerchant === null) { ?>id="a_open_mnToko"<?php } ?>>
                                    <div class="icon_ava" style="border: none;">
                                        <?php if ($dataMerchant !== null) { ?>
                                            <img src="<?php bloginfo('template_directory'); ?>/library/images/market.png">
                                        <?php } else { ?>
                                            <img src="<?php bloginfo('template_directory'); ?>/library/images/market.png">
                                        <?php } ?>
                                    </div>
                                    <?php if ($dataMerchant !== null) { ?>
                                        <span class="nm">
                                            <?php echo $dataMerchant->name; ?>
                                        </span>
                                    <?php } else { ?>
                                        <span class="nm">Toko</span>
                                        <span class="ico_openProfile">
                                            <i class="fas fa-angle-down"></i>
                                        </span>
                                    <?php } ?>
                                </a>
                            <?php } ?>

                            <div class="bx_web_profile" id="a_open_mnProfile">
                                <div class="icon_ava">
                                    <img src="<?php echo $url_avatar; ?>">
                                </div>
                                <span class="nm">
                                    <?php echo $display_name; ?>
                                </span>
                                <span class="ico_openProfile">
                                    <i class="fas fa-angle-down"></i>
                                </span>
                            </div>

                            <div class="mob_topmenu mob_display" id="a_open_mnProfile_mob">
                                <img src="<?php bloginfo('template_directory'); ?>/library/images/mobile_menu.svg">
                            </div>
                        <?php } else { ?>
                            <a href="<?php echo home_url(); ?>/login/" title="Masuk sekarang!">
                                <div class="mob_topmenu_login mob_display">Masuk</div>
                            </a>

                            <div class="bx_mg_2 des_display">
                                <a href="<?php echo home_url(); ?>/login/" class="a_loginNow" title="Masuk sekarang!">
                                    Masuk
                                </a>
                            </div>
                            <div class="bx_mg_3 des_display">
                                <a href="<?php echo home_url(); ?>/register/" class="a_register" title="Daftar sekarang!">
                                    Daftar
                                </a>
                            </div>
                        <?php } ?>
                    </div>

                    <?php if (is_page(array(2))) { ?>
                        <div class="row bx_head_bottom header_list_tops">
                            <div id="list_topHits" class="col-md-12 col_head_bottom">
                                <ul class="ul_head_bottom">

                                    <?php
                                    $get_headcategory = get_post_meta($post->ID, 'kategoriheader', true);
                                    if (isset($get_headcategory)) {

                                        $count_headcat = count($get_headcategory);
                                        $op = 0;
                                        foreach ($get_headcategory as $key => $value) {
                                            $op++;
                                            $cathead_name = $value['head-category-name'];
                                            $cathead_url = $value['head-category-url'];

                                            if ($op == $count_headcat) {
                                    ?>
                                                <li><a href="<?php echo $cathead_url; ?>"><?php echo $cathead_name; ?></a></li>
                                            <?php
                                            } else {
                                            ?>
                                                <li><a href="<?php echo $cathead_url; ?>"><?php echo $cathead_name; ?> <span>|</span></a></li>
                                    <?php
                                            }
                                        }
                                    }
                                    ?>
                                </ul>
                            </div>
                        </div>
                    <?php } else { ?>
                        <style type="text/css">
                            .header {
                                /* height: 135px; */
                            }

                            .header_list_tops {
                                display: none !important;
                            }
                        </style>
                    <?php } ?>
                </div>
                
                <div id="myHeaderMobile" class="row bx_head_middle bx_web_head_middle">
                    <div class="col-xs-2 col-md-2 col_mn_logo">
                        <a href="<?php echo home_url(); ?>" title="Home - Ladara Indonesia" class="mg_masterLogo animated jello">
                            <img src="<?php bloginfo('template_directory'); ?>/library/images/main_logo_mobile.png">
                        </a>
                        <div class="row fix_catMenu animated fadeIn">
                            <div id="v_catMenu" class="bx_catMenu">
                                <?php // ajax get category here... ?>
                                <div class="row">
                                    <div class="col-md-4 col_catMenu">
                                        <div class="bx_catMenu_cont">
                                            <div class="ht_ul_catMenu mob_display">
                                                Belanja 
                                                <span class="hide_sup_catMenu">tutup</span>
                                            </div>

                                            <ul class="ul_catMenu">
                                                <?php
                                                $all_parent = array();
                                                $all_catID = array();
                                                $query = "SELECT * FROM ldr_categories where parent = 0 AND slug != 'emas'";
                                                $all_catVoucher = $wpdb->get_results($query);
                                                $op = 0;
                                                ?>
                                                <?php foreach ($all_catVoucher as $key => $value) { ?>
                                                    <?php
                                                    $op++;
                                                    $ido = $value->id;
                                                    $name_cat = $value->name;
                                                    $slug_cat = $value->slug;
                                                    $all_catID[] = array('par_id' => $ido, 'par_name' => $name_cat);

                                                    $cat_image = IMAGE_URL.'/'.$value->image;
                                                    ?>
                                                    <?php if ($op == 1) { ?>
                                                        <li class="act" data-id="tcap_<?php echo $ido; ?>">
                                                            <a>
                                                                <div class="mg_catMenu">
                                                                    <img src="<?php echo $cat_image; ?>">
                                                                </div>
                                                                <?php echo $name_cat; ?>
                                                            </a>
                                                        </li>
                                                    <?php } else { ?>
                                                        <li class="" data-id="tcap_<?php echo $ido; ?>">
                                                            <a>
                                                                <div class="mg_catMenu">
                                                                    <img src="<?php echo $cat_image; ?>">
                                                                </div> 
                                                                <?php echo $name_cat; ?>
                                                            </a>
                                                        </li>
                                                    <?php } ?>
                                                <?php } ?>
                                                <li class="">
                                                    <a href="<?php echo home_url(); ?>/emas/">
                                                        <div class="mg_catMenu">
                                                            <img src="<?php echo IMAGE_URL; ?>/wp-content/uploads/2020/04/Ladara-Emas.png">
                                                        </div> 
                                                        Ladara Emas
                                                    </a>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                    
                                    <div class="col-md-8 col_catMenu2 col_cont_catMenu">
                                        <?php $op = 0; ?>
                                        <?php foreach ($all_catID as $key => $value) { ?>
                                            <?php 
                                            $op++;
                                            $parent_ID = $value['par_id'];
                                            $parent_name = $value['par_name'];
                                            ?>
                                            <?php if ($op == 1) { ?>
                                            <div id="tcap_<?php echo $parent_ID; ?>" class="box_tsMenu act animated fadeIn">
                                            <?php } else { ?>
                                            <div id="tcap_<?php echo $parent_ID; ?>" class="box_tsMenu animated fadeIn">
                                            <?php } ?>
                                                <div class="ht_catMenu"><?php echo $parent_name; ?>
                                                    <span class="hide_catMenu mob_display">tutup</span>
                                                </div>

                                                <div class="bx_cont_catMenu">

                                                    <?php
                                                    $query = "SELECT * FROM ldr_categories where parent = $parent_ID";
                                                    $all_catVoucher = $wpdb->get_results($query);
                                                    ?>

                                                    <?php foreach ($all_catVoucher as $key => $value) { ?>
                                                        <?php
                                                        $cat_id = $value->id;
                                                        $cat_name = $value->name;
                                                        $cat_slug = $value->slug;
                                                        ?>

                                                        <div class="col_in_catMenu">
                                                            <a href="<?php echo home_url(); ?>/product?category=<?php echo $cat_slug.'-'.$cat_id; ?>">
                                                                <div class="head_catMenu">
                                                                    <?php echo $cat_name; ?>
                                                                </div>
                                                            </a>

                                                            <?php
                                                            $query = "SELECT * FROM ldr_categories where parent = $cat_id";
                                                            $all_catVoucher = $wpdb->get_results($query);

                                                            $count_child = count($all_catVoucher);
                                                            ?>

                                                            <?php if ($count_child > 0) { ?>
                                                                <ul class="ul_cont_catMenu child_catMenu">
                                                                    <?php foreach ($all_catVoucher as $key => $value) { ?>
                                                                        <?php
                                                                        $child_id = $value->id;
                                                                        $child_name = $value->name;
                                                                        $child_slug = $value->slug;
                                                                        ?>
                                                                        <li>
                                                                            <a href="<?php echo home_url(); ?>/product?category=<?php echo $child_slug.'-'.$child_id; ?>">
                                                                                <?php echo $child_name; ?>
                                                                            </a>
                                                                        </li>
                                                                    <?php } ?>
                                                                </ul>
                                                            <?php } ?>
                                                        </div>
                                                    <?php } ?>
                                                </div>
                                            </div>
                                        <?php } ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <?php  // =================== Get All Category ================== ?>
                    </div>

                    <div class="col-xs-7 col_mn_search">
                        <?php
                        if (isset($_GET['keyword'])) {
                            $keyword = htmlspecialchars($_GET['keyword']);
                        } else {
                            $keyword = '';
                        }
                        ?>
                        <form method="get" id="form_searching" action="<?php echo home_url(); ?>/product/">
                            <input id="searchNow" type="text" name="keyword" class="txt_searchNow" placeholder="Cari produk atau brand yuk!" value="<?php echo $keyword; ?>">
                            <span class="glyphicon glyphicon-search sp_searchNow submit-search" title="Cari sekarang."></span>
                            <div class="res_tagsSearch">
                                <ul id="show_tagsSearch" class="ul_tagsSearch">
                                </ul>
                            </div>
                            <button type="submit" class="sub_search hide" form="form_searching" value="Submit"><img src="<?php bloginfo('template_directory'); ?>/library/images/logo-cari.png"> Cari</button>
                        </form>
                    </div>
                    
                    <div class="col-xs-2 <?php if (isset($u_id) and $u_id != '' and $u_id != 0) : ?>col-md-4<?php else : ?>col-md-3<?php endif; ?> col_mn_profile"  style="user-select: none;">
                        <?php $count_cart = get_itemCart_count(); ?>

                        <div class="bx_mg_1 bx_web_notifcart">
                            <?php if (isset($u_id) and $u_id != '' and $u_id != 0) { // for mobile cart ?>
                                <a href="javascript:;" class="mob_display mg_cart a_open_mnCart" title="Lihat keranjang belanja" id="" data-cart="<?php echo $count_cart; ?>">
                                    <img src="<?php bloginfo('template_directory'); ?>/library/images/cart.svg">
                                    <span id="v_cart" class="sp_c_cart"><?php echo $count_cart; ?></span>
                                </a>

                                <?php /*
                                <a href="javascript:;" class="des_display mg_cart a_open_mnCart" title="Lihat keranjang belanja" id="a_open_mnCart" data-cart="<?php echo $count_cart; ?>">
                                    <img src="<?php bloginfo('template_directory'); ?>/library/images/cart.svg">
                                    <span id="v_cart" class="sp_c_cart"><?php echo $count_cart; ?></span>
                                </a>
                                */ ?>
                            <?php } ?>

                            <div class="row fix_cartMenu">
                                <div id="v_listcartMobile" class="bx_cartMenu">
                                    <?php // ajax get cart item here... 
                                    ?>
                                </div>
                            </div>
                        </div>

                        <?php $count_actNotif = 0; ?>

                        <?php if (isset($u_id) and $u_id != '' and $u_id != 0) { ?>
                            <?php
                                $dataMerchant = getMerchant($u_id);
                                if (isset($current_user->display_name)) {
                                    $display_name = $current_user->display_name;
                                    $data_name = explode(" ", $display_name);
                                    $display_name = $data_name[0];
                                } else {
                                    $display_name = 'customer';
                                }

                                $user_avatar = get_field('user_avatar', 'user_' . $u_id);
                                if (isset($user_avatar) and $user_avatar != '') {
                                    $linkpoto = wp_get_attachment_image_src($user_avatar, 'thumbnail');
                                    $url_avatar = $linkpoto[0];
                                } else {
                                    $url_avatar = get_template_directory_uri() . '/library/images/icon_profile.png';
                                }
                                
                                date_default_timezone_set("Asia/Jakarta");
                                $nowdate = date('Y-m-d H:i:s');
                                $d_now = date('d');

                                $res_notif = Notifications::getNotifications($u_id, 1);
                                $notifPenjualan = [];
                                if($dataMerchant->id){
                                    $notifPenjualan = Notifications::getNotifications($u_id, 1, $dataMerchant->id ?? 0);
                                }

                                if (isset($res_notif['data'])) {
                                    $count_notif = count($res_notif['data']);
                                    foreach ($res_notif['data'] as $key => $value) {
                                        $notif_status = $value['status'];
                                        if (isset($notif_status) and $notif_status == 1) {
                                        } else {
                                            $count_actNotif++;
                                        }
                                    }
                                }

                                if (isset($notifPenjualan['data'])) {
                                    $count_notif = count($notifPenjualan['data']);
                                    foreach ($notifPenjualan['data'] as $key => $value) {
                                        $notif_status = $value['status'];
                                        if (isset($notif_status) and $notif_status == 1) {
                                        } else {
                                            $count_actNotif++;
                                        }
                                    }
                                }
                            ?>

                            <div class="mob_topmenu mob_display" id="a_open_mnProfile_mob_mobile">
                                <img src="<?php bloginfo('template_directory'); ?>/library/images/mobile_menu.svg">
                            </div>
                        <?php } else { ?>
                            <a href="<?php echo home_url(); ?>/login/" title="Masuk sekarang!">
                                <div class="mob_topmenu_login mob_display">Masuk</div>
                            </a>

                            <div class="bx_mg_2 des_display">
                                <a href="<?php echo home_url(); ?>/login/" class="a_loginNow" title="Masuk sekarang!">
                                    Masuk
                                </a>
                            </div>
                            <div class="bx_mg_3 des_display">
                                <a href="<?php echo home_url(); ?>/register/" class="a_register" title="Daftar sekarang!">
                                    Daftar
                                </a>
                            </div>
                        <?php } ?>
                    </div>

                    <?php if (is_page(array(2))) { ?>
                        <div class="row bx_head_bottom header_list_tops">
                            <div id="list_topHits" class="col-md-12 col_head_bottom">
                                <ul class="ul_head_bottom">

                                    <?php
                                    $get_headcategory = get_post_meta($post->ID, 'kategoriheader', true);
                                    if (isset($get_headcategory)) {

                                        $count_headcat = count($get_headcategory);
                                        $op = 0;
                                        foreach ($get_headcategory as $key => $value) {
                                            $op++;
                                            $cathead_name = $value['head-category-name'];
                                            $cathead_url = $value['head-category-url'];

                                            if ($op == $count_headcat) {
                                    ?>
                                                <li><a href="<?php echo $cathead_url; ?>"><?php echo $cathead_name; ?></a></li>
                                            <?php
                                            } else {
                                            ?>
                                                <li><a href="<?php echo $cathead_url; ?>"><?php echo $cathead_name; ?> <span>|</span></a></li>
                                    <?php
                                            }
                                        }
                                    }
                                    ?>
                                </ul>
                            </div>
                        </div>
                    <?php } else { ?>
                        <style type="text/css">

                            .header_list_tops {
                                display: none !important;
                            }
                        </style>
                    <?php } ?>
                </div>
            </header>
        <?php endif; ?>

        <?php if (isset($u_id) and $u_id != '' and $u_id != 0) {  ?>
            <?php // get profile ===================================================== 
            ?>
            <div class="row fix_tokoMenu">
                <div class="bx_tokoMenu" style="right: 105px; width: 20%; text-align: center;">
                    Anda Belum Memiliki Toko
                    <a href="<?php echo home_url(); ?>/register-merchant/">
                        <input type="button" class="sub_login" value="Daftarkan Toko" style="margin-bottom: 15px;" />
                    </a>
                    <a href="<?= home_url('syarat-ketentuan-daftar-toko') ?>" style="font-size: 11px;">
                        <b>Pelajari lebih lengkap Tentang Toko</b>
                    </a>
                </div>
            </div>

            <div class="row fix_profileMenu">
                <div class="bx_profileMenu" style="right: 15px;">
                    <ul class="mn_profileMenu">
                        <li><a href="<?php echo home_url(); ?>/profile/"><span><img class="mg_mn_voucher" src="<?php bloginfo('template_directory'); ?>/library/images/profile_setting.svg"></span>Pengaturan</a></li>
                        <li id="" class="voucherpage"><a href="<?php echo home_url(); ?>/my-order/"><span><img class="mg_mn_voucher" src="<?php bloginfo('template_directory'); ?>/library/images/profile_order.svg"></span>Pembelian</a></li>
                        <li><a href="<?php echo home_url(); ?>/wishlist/"><span><img class="mg_mn_voucher" src="<?php bloginfo('template_directory'); ?>/library/images/profile_wishlist.svg"></span>Wishlist</a></li>
                        <li class="logoutNow"><a href="<?php echo home_url(); ?>/logout/"><span><img class="mg_mn_voucher" src="<?php bloginfo('template_directory'); ?>/library/images/profile_logout.png"></span>Keluar</a></li>
                    </ul>
                </div>
            </div>
            <?php // get profile ===================================================== 
            ?>

            <?php // get notification user =========================================== 
            ?>
            <div class="row fix_topnotif">
                 <div class="bx_topnotif">
                    <div class="ht_topnotif">Notifikasi</div>
                     
                     <ul class="nav nav-tabs notif">
                         <li style="width: 50%;" class="active"><a data-toggle="tab" href="#pembelian">Pembelian</a></li>
                         <li style="width: 50%;"><a data-toggle="tab" href="#penjualan">Penjualan</a></li>
                     </ul>
                     <div class="tab-content">
                         <div id="pembelian" class="tab-pane fade in active">
                             <div class="bx_in_topnotif">
                                 <?php
                                 if (isset($res_notif['data']) and !empty($res_notif['data'])) {
                                     $count_notif = count($res_notif['data']);
                                     foreach ($res_notif['data'] as $key => $value) {
                                         $notif_id = $value['id'];
                                         $order_id_notif = $value['order_id'];
                                         $type_notif = $value['type'];
                                         if (strtolower($type_notif) == 'info') {
                                             $link_notif = home_url();
                                         } else if (strtolower($type_notif) == 'emas') {
                                             $link_notif = home_url() . '/emas/';
                                         } else {
                                             $link_notif = home_url() . '/my-order/';
                                         }
                                         $notif_status = $value['status'];
                                         if (isset($notif_status) and $notif_status == 1) {
                                             $class_act = '';
                                             $icon_act = '';
                                         } else {
                                             $class_act = 'new';
                                             $icon_act = '<span class="ntf_new">baru</span>';
                                         }
                                         $url_image_notif = $value['url_image'];

                                            // $url_newimage = home_url().'/wp-content/themes/ladara2020';
                                            // $url_image = str_replace('https://ladara.id/wp-content/themes/ladara2020', '', $url_image_notif);
                                            // $url_image_notif = $url_newimage.$url_image;

                                         $title_notif = $value['title'];
                                         $desc_notif = $value['descriptions'];
                                         $date_created_notif = $value['date_created'];
                                         $day_notif = date("d", strtotime($date_created_notif));
                                         $time_notif = date("h:i", strtotime($date_created_notif));
                                         $date_notif = date("d M", strtotime($date_created_notif));
                                         if ($day_notif == $d_now) {
                                             $date_notif = 'hari ini';
                                         }

                                         ?>
                                         <div class="row row_topnotif <?php echo $class_act; ?> open_thisNotif" id="notif_<?php echo $notif_id; ?>" data-id="<?php echo $notif_id; ?>">
                                             <a href="<?php echo $link_notif; ?>" title="Lihat detail">
                                                 <div class="col-md-12 col_topnotif">
                                                     <div class="bx_date_topnotif"><img src="<?php echo $url_image_notif; ?>">info • <?php echo $date_notif; ?> <?php echo $icon_act; ?> <span><?php echo $time_notif; ?></span></div>
                                                     <div class="hts_topnotif"><?php echo $title_notif; ?></div>
                                                     <div class="cont_topnotif">
                                                         <?php echo $desc_notif; ?>
                                                     </div>
                                                 </div>
                                             </a>
                                         </div>
                                         <?php
                                     }
                                 } else {
                                     ?>
                                     <div class="row row_topnotif" id="notif_<?php echo $notif_id; ?>">
                                         <div class="col-md-12 col_topnotif">
                                             <div class="mg_nonotif">
                                                 <img src="<?php bloginfo('template_directory'); ?>/library/images/notif_empty.svg">
                                             </div>
                                             <div class="ht_topnotif noborder">Belum ada notifikasi.</div>
                                         </div>
                                     </div>
                                     <?php
                                 }
                                 ?>
                                 <?php if (($count_notif) > 10) { ?>
                                     <a href="<?php echo home_url(); ?>/notifikasi/" title="Lihat notifikasi selengkapnya.">
                                         <div class="ht_topnotif">Selengkapnya</div>
                                     </a>
                                 <?php } ?>
                             </div>
                         </div>
                         <div id="penjualan" class="tab-pane fade">
                             <div class="bx_in_topnotif">
                                 <?php
                                 if (isset($notifPenjualan['data']) and !empty($notifPenjualan['data'])) {
                                     $count_notif = count($notifPenjualan['data']);
                                     foreach ($notifPenjualan['data'] as $key => $value) {
                                         $notif_id = $value['id'];
                                         $order_id_notif = $value['order_id'];
                                         $type_notif = $value['type'];
                                         if (strtolower($type_notif) == 'info') {
                                             $link_notif = home_url();
                                         } else if (strtolower($type_notif) == 'emas') {
                                             $link_notif = home_url() . '/emas/';
                                         } else {
                                             $link_notif = home_url() . '/my-order/';
                                         }
                                         $notif_status = $value['status'];
                                         if (isset($notif_status) and $notif_status == 1) {
                                             $class_act = '';
                                             $icon_act = '';
                                         } else {
                                             $class_act = 'new';
                                             $icon_act = '<span class="ntf_new">baru</span>';
                                         }
                                         $url_image_notif = $value['url_image'];

                                            // $url_newimage = home_url().'/wp-content/themes/ladara2020';
                                            // $url_image = str_replace('https://ladara.id/wp-content/themes/ladara2020', '', $url_image_notif);
                                            // $url_image_notif = $url_newimage.$url_image;

                                         $title_notif = $value['title'];
                                         $desc_notif = $value['descriptions'];
                                         $date_created_notif = $value['date_created'];
                                         $day_notif = date("d", strtotime($date_created_notif));
                                         $time_notif = date("h:i", strtotime($date_created_notif));
                                         $date_notif = date("d M", strtotime($date_created_notif));
                                         if ($day_notif == $d_now) {
                                             $date_notif = 'hari ini';
                                         }

                                         ?>
                                         <div class="row row_topnotif <?php echo $class_act; ?> open_thisNotif" id="notif_<?php echo $notif_id; ?>" data-id="<?php echo $notif_id; ?>">
                                             <a href="<?php echo $link_notif; ?>" title="Lihat detail">
                                                 <div class="col-md-12 col_topnotif">
                                                     <div class="bx_date_topnotif"><img src="<?php echo $url_image_notif; ?>">info • <?php echo $date_notif; ?> <?php echo $icon_act; ?> <span><?php echo $time_notif; ?></span></div>
                                                     <div class="hts_topnotif"><?php echo $title_notif; ?></div>
                                                     <div class="cont_topnotif">
                                                         <?php echo $desc_notif; ?>
                                                     </div>
                                                 </div>
                                             </a>
                                         </div>
                                         <?php
                                     }
                                 } else {
                                     ?>
                                     <div class="row row_topnotif" id="notif_<?php echo $notif_id; ?>">
                                         <div class="col-md-12 col_topnotif">
                                             <div class="mg_nonotif">
                                                 <img src="<?php bloginfo('template_directory'); ?>/library/images/notif_empty.svg">
                                             </div>
                                             <div class="ht_topnotif noborder">Belum ada notifikasi.</div>
                                         </div>
                                     </div>
                                     <?php
                                 }
                                 ?>
                                 <?php if (($count_notif) > 10) { ?>
                                     <a href="<?php echo home_url(); ?>/notifikasi/" title="Lihat notifikasi selengkapnya.">
                                         <div class="ht_topnotif">Selengkapnya</div>
                                     </a>
                                 <?php } ?>
                             </div>
                         </div>
                     </div>
            </div>
    </div>
    <?php // get notification user =========================================== 
    ?>
<?php } ?>

<style>
    <?php if ($yes_mobile == 1) { ?>
        .wa__btn_popup,
        .wa__popup_chat_box {
            display: none !important;
        }
    <?php } ?>
    /* temporary, mau push ke testing, style-1.css masih dipakai bersama */
    @media only screen and (max-width:769px) {
        .wa__btn_popup {
            bottom: 6em !important;
        }
    }
</style>


<div class="fix_loading">
    <img src="<?php bloginfo('template_directory'); ?>/library/images/mg_loading.svg"> <span>Loading...</span>
</div>

<?php
if (is_page(array(
    'cart',
    'checkout',
    'payment',
    'thank-you',
))) {
?>

<?php
} else {
?>
    <?php if ($yes_mobile == 0) : ?>
        <div class="row row_fix_mobmenu mob_display">
            <div class="col-md-12">

                <ul class="ul_fix_mobmenu">
                    <li class="act"><a href="<?php echo home_url(); ?>">
                            <img src="<?php bloginfo('template_directory'); ?>/library/images/ico_mob_1.svg">
                            <span>Beranda</span>
                        </a></li>
                    <li id="open_topmenu_mob"><a>
                            <img src="<?php bloginfo('template_directory'); ?>/library/images/ico_mob_2.svg">
                            <span>Kategori</span>
                        </a></li>
                    <li><a href="<?php echo home_url(); ?>/cart/">
                            <?php if ((int)$count_cart > 0) { ?>
                                <span class="sp_notif_mob"><?php echo $count_cart; ?></span>
                            <?php } ?>
                            <img src="<?php bloginfo('template_directory'); ?>/library/images/ico_mob_3.svg">
                            <span>Keranjang</span>
                        </a></li>
                    <li><a href="<?php echo home_url(); ?>/notifikasi/">
                            <?php if ((int)$count_actNotif > 0) { ?>
                                <span class="sp_notif_mob"><?php echo $count_actNotif; ?></span>
                            <?php } ?>
                            <img src="<?php bloginfo('template_directory'); ?>/library/images/ico_mob_4.svg">
                            <span>Notifikasi</span>
                        </a></li>
                    <li><a href="<?php echo home_url();     ?>/profile/menu/">
                            <img src="<?php bloginfo('template_directory'); ?>/library/images/ico_mob_5.svg">
                            <span>Akun</span>
                        </a></li>
                </ul>

            </div>
        </div>
    <?php endif; ?>
<?php
}
?>

<?php // next to body and footer.php 
?>