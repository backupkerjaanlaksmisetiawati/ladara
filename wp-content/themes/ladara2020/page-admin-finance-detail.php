<?php
/*
Template Name: Finance Detail Dashboard Page
*/

use Shipper\Location;

?>

<?php get_header('admin'); ?>

<?php if (have_posts()) :
    while (have_posts()) :
        the_post();
        date_default_timezone_set("Asia/Jakarta");
        $nowdate = date('Y-m-d H:i:s');

        $current_user = wp_get_current_user();
        $u_id = $current_user->ID;
        $u_roles = $current_user->roles;
        $admin = 0;
        $orderMerchantId = isset($_GET['id']) ? htmlspecialchars($_GET['id']) : 0;

        if (isset($u_roles) and !empty($u_roles)) {
            foreach ($u_roles as $key => $value) {
                if (strtolower($value) == 'administrator') {
                    $admin = 1;
                }
                if (strtolower($value) == 'shop_manager') {
                    $admin = 1;
                }
            }
        }

        if ((isset($u_id) and $u_id != 0) and $admin == 1) {
            $orderMerchant = sendRequest('GET', MERCHANT_URL . '/api/orders/get-order/' . $orderMerchantId);
            $trackOrder = sendRequest('GET', MERCHANT_URL . '/api/orders/track-order/' . $orderMerchantId);
?>

            <div class="container-fluid">
                <h1 class="h3 mb-2 text-gray-800">Data Order</h1>
                <div class="alert alert-danger chk_err"></div>
                <div class="card shadow mb-4">
                    <div class="card-header py-3">
                        <h6 class="m-0 font-weight-bold text-primary ht_admdash">Detail Order</h6>
                    </div>
                    <div class="card-body">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Nama Toko</label>
                                    <input type="text" class="form-control" value="<?= $orderMerchant->data->merchant_name; ?>" readonly>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>No. Invoice</label>
                                    <input type="text" class="form-control" value="<?= $orderMerchant->data->no_invoice; ?>" readonly>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Tanggal Order</label>
                                    <input type="text" class="form-control" value="<?= $orderMerchant->data->created_at; ?>" readonly>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Metode Pembayaran</label>
                                    <input type="text" class="form-control" value="<?= $orderMerchant->data->payment_method; ?>" readonly>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Nama Kurir</label>
                                    <input type="text" class="form-control" value="<?= $orderMerchant->data->courier_name; ?> (<?= $orderMerchant->data->courier_rate_name; ?>)" readonly>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Biaya Pengiriman (Rp)</label>
                                    <input type="text" class="form-control" value="<?= number_format($orderMerchant->data->shipping_price); ?>" readonly>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Biaya Asuransi Pengiriman (Rp)</label>
                                    <input type="text" class="form-control" value="<?= number_format($orderMerchant->data->insurance_price); ?>" readonly>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Biaya Admin (Rp)</label>
                                    <input type="text" class="form-control" value="<?= number_format($orderMerchant->data->admin_fee); ?>" readonly>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Nama Penerima</label>
                                    <input type="text" class="form-control" value="<?= $orderMerchant->data->order_address->consignee; ?>" readonly>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>No. Telepon</label>
                                    <input type="text" class="form-control" value="<?= $orderMerchant->data->order_address->phone; ?>" readonly>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Provinsi</label>
                                    <input type="text" class="form-control" value="<?= $orderMerchant->data->order_address->province_name; ?>" readonly>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Kota</label>
                                    <input type="text" class="form-control" value="<?= $orderMerchant->data->order_address->city_name; ?>" readonly>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Kecamatan</label>
                                    <input type="text" class="form-control" value="<?= $orderMerchant->data->order_address->suburb_name; ?>" readonly>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Kelurahan</label>
                                    <input type="text" class="form-control" value="<?= $orderMerchant->data->order_address->area_name; ?> (<?= $orderMerchant->data->order_address->postal_code; ?>)" readonly>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Xendit ID</label>
                                    <input type="text" class="form-control" value="<?= $orderMerchant->data->xendit_id; ?>" readonly>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Xendit URL</label>
                                    <a href="<?= $orderMerchant->data->xendit_url ?>" target="_blank">
                                        <input type="text" class="form-control" value="<?= $orderMerchant->data->xendit_url; ?>" readonly>
                                    </a>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label><?= $orderMerchant->data->shipment_provider ?> Order ID</label>
                                    <input type="text" class="form-control" value="<?= $orderMerchant->data->shipment_order_id; ?>" readonly>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>AWB Number</label>
                                    <input type="text" class="form-control" value="<?= $orderMerchant->data->shipment_awb_number; ?>" readonly>
                                </div>
                            </div>
                        </div>
                        <br>
                        <div class="row">
                            <div class="col-md-12">
                                <h1 class="h5 mb-2 text-gray-800">
                                    <b>Rincian Pembayaran LaDaRa</b>
                                </h1>
                                <table class="table table-bordered">
                                    <thead>
                                        <th>Nama Produk</th>
                                        <th>Jumlah</th>
                                        <th>Harga</th>
                                        <th>Total</th>
                                    </thead>
                                    <tbody>
                                        <?php
                                        $totalComissionPrice = 0;

                                        foreach ($orderMerchant->data->order_items as $orderItem) {
                                            $totalComissionPrice += $orderItem->commission_price;
                                        ?>
                                            <tr>
                                                <td><?= $orderItem->product_name ?></td>
                                                <td style="text-align: center;"><?= $orderItem->qty ?></td>
                                                <td style="text-align: center;"><?= number_format($orderItem->price) ?></td>
                                                <td style="text-align: center;"><?= number_format($orderItem->qty * $orderItem->price) ?></td>
                                            </tr>
                                        <?php
                                        }
                                        ?>
                                        <tr>
                                            <td>Komisi</td>
                                            <td style="text-align: center;">-</td>
                                            <td style="text-align: center;">-</td>
                                            <td style="text-align: center; font-weight: bold; color: red;">-<?= number_format($totalComissionPrice) ?></td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <b>Total Harga</b>
                                            </td>
                                            <td style="text-align: right; padding-right: 80px;" colspan="3"><?= number_format($orderMerchant->data->total_price - $totalComissionPrice) ?></td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                            <div class="col-md-12" align="right">

                                <?php

                                if (intval($orderMerchant->data->status) === 0) {
                                ?>

                                    <button class="btn btn-warning btn_confirm_payment" title="Konfirmasi Pembayaran Order Ini." data-id="<?php echo $orderMerchant->data->order_id; ?>">
                                        <b>Konfirmasi Pembayaran</b>
                                    </button>

                                <?php
                                } else if (intval($orderMerchant->data->status) !== 4) {
                                ?>

                                    <button class="btn btn-danger btn_complete_order" title="Selesaikan Order Ini." data-id="<?php echo $orderMerchant->data->id; ?>">
                                        <b>Konfirmasi Pesanan Selesai</b>
                                    </button>

                                <?php
                                }
                                ?>

                            </div>
                        </div>
                        <?php
                        if (count($trackOrder->data->courier->tracking_history)) {
                        ?>
                            <br>
                            <div class="row">
                                <div class="col-md-12">
                                    <h1 class="h5 mb-2 text-gray-800">Log Pengiriman</h1>
                                    <table class="table table-bordered">
                                        <thead>
                                            <th>Tanggal</th>
                                            <th>Status</th>
                                            <th>Deskripsi</th>
                                        </thead>
                                        <tbody>
                                            <?php
                                            foreach ($trackOrder->data->courier->tracking_history as $tracking) {
                                            ?>
                                                <tr>
                                                    <td><?= $tracking->time ?></td>
                                                    <td><?= $tracking->event ?></td>
                                                    <td><?= $tracking->description ?></td>
                                                </tr>
                                            <?php
                                            } ?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        <?php
                        }
                        ?>
                        <br>
                        <div class="row">
                            <div class="col-md-12">
                                <h1 class="h5 mb-2 text-gray-800">Log Pesanan</h1>
                                <table class="table table-bordered">
                                    <thead>
                                        <th>Tanggal</th>
                                        <th>Deskripsi</th>
                                    </thead>
                                    <tbody>
                                        <?php
                                        foreach ($orderMerchant->data->order_logs as $orderLog) {
                                        ?>
                                            <tr>
                                                <td><?= $orderLog->time ?></td>
                                                <td><?= $orderLog->log ?></td>
                                            </tr>
                                        <?php
                                        } ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <div class="row" style="margin-top: 20px;">
                            <div class="col-md-6">
                                <a href="<?php echo home_url(); ?>/dashboard/finance">
                                    <button class="btn btn-info">
                                        <b>Kembali</b>
                                    </button>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- /.container-fluid -->
        <?php
        } else { ?>
            <script>
                // 'Getting' data-attributes using getAttribute
                var plant = document.getElementById('body');
                console.log(plant);
                var hurl = plant.getAttribute('data-hurl');
                location.replace(hurl + '/login/');
            </script>
        <?php } ?>

    <?php endwhile; ?>
<?php else : ?>
    <?php get_template_part('content', '404pages'); ?>
<?php endif; ?>
<?php get_footer('admin'); ?>