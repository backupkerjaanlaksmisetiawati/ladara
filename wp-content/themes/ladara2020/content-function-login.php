<?php

//config.php

//Include Google Client Library for PHP autoload file
require_once 'vendor-google/autoload.php';

//Make object of Google API Client for call Google API
$google_client = new Google_Client();

//Set the OAuth 2.0 Client ID
$google_client->setClientId(getenv('GOOGLE_API_KEY'));

//Set the OAuth 2.0 Client Secret key
$google_client->setClientSecret(getenv('GOOGLE_CLIENT_SECRET'));

//Set the OAuth 2.0 Redirect URI
$google_client->setRedirectUri(getenv('GOOGLE_REDIRECT_URI'));

//
$google_client->addScope('email');

$google_client->addScope('profile');

//start session on web page
// session_start();

?>