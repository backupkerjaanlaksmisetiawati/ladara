<div id="modal-login" class="modal fade" role="dialog">
    <div class="modal-dialog" style="width: 53rem;">
        <div class="modal-content" style="padding: 3rem;">
            <div class="modal-header" style="border-bottom: 0px; padding: 15px 0px;">
                <button type="button" class="close" data-dismiss="modal" style="margin-top: -25px;">&times;</button>
                <h4 class="modal-title">
                    <div class="row">
                        <div class="col-xs-6">
                            <b>Masuk</b>
                        </div>
                        <div class="col-xs-6" style="text-align: right;">
                            <a href="<?php echo home_url(); ?>/register/" title="Daftar sekarang!">
                                Daftar
                            </a>
                        </div>
                    </div>
                </h4>
            </div>
            <div class="modal-body">
                <form id="formLogin" method="post" autocomplete="nope" action="<?php bloginfo('url'); ?>/login/" class="wp-user-form formLogin" onsubmit="submit_LoginNow(event);">
                    <input autocomplete="nope" name="hidden" type="text" style="display:none;">
                    <div class="f_ad_input">
                        <input type="email" ng-model="username" name="log" id="user_login" class="ad_input" placeholder="Masukkan Email" value="" required="required" maxlength="50" />
                        <div id="err_email" class="f_err">*Masukkan Email</div>
                    </div>
                    <div class="f_ad_input">
                        <input type="password" name="pwd" id="user_pass" class="ad_input" placeholder="Masukkan kata sandi" value="" required="required" autocomplete="nope" minlength="5" />
                        <span id="open_password" class="glyphicon glyphicon-eye-close open_password" title="Lihat/sembunyikan password."></span>
                        <div id="err_pwd" class="f_err">*Masukkan Kata Sandi</div>
                    </div>
                    <div class="f_errLogin h_errLogin"></div>
                    <div class="login_fields">
                        <input type="submit" name="user-submit" id="wp-submit" class="sub_login" value="Masuk" />
                        <input type="hidden" name="redirect_to" value="<?php echo esc_attr($referer); ?>" />
                        <input type="hidden" name="user-cookie" value="1" />
                        <?php wp_nonce_field('login_submit'); ?>
                    </div>
                </form>
                <div class="info_login">atau masuk dengan</div>
                <a href="<?php echo $loginUrl_fb; ?>" title="Login dengan akun facebook.">
                    <div id="sub_facebook" class="btn_logSocial login_facebook">
                        <img src="<?php bloginfo('template_directory'); ?>/library/images/icon_fb.svg"> Facebook
                    </div>
                </a>
                <a href="<?php echo $url_googleLogin; ?>" title="Login dengan akun google.">
                    <div id="sub_google" class="btn_logSocial login_google">
                        <img src="<?php bloginfo('template_directory'); ?>/library/images/icon_google.svg"> Google
                    </div>
                </a>
                <div class="a_bergabung">
                    Belum punya akun Ladara? <a href="<?php echo home_url(); ?>/daftar/" title="Daftar Sekarang!">Daftar</a>
                </div>
                <div class="a_bergabung">
                    <a class="" href="<?php echo home_url(); ?>/lupa-password/">Lupa kata sandi?</a>
                </div>
            </div>
        </div>
    </div>
</div>