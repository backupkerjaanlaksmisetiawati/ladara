<?php
/*
Template Name: Email Page
*/
?>

<?php // get_header(); ?>
<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
<?php 
date_default_timezone_set("Asia/Jakarta");
$nowdate = date('Y-m-d');
$now_time = strtotime($nowdate);
?>


<html>
<head><link href="https://fonts.googleapis.com/css?family=Roboto" rel="stylesheet">
	<link href="https://fonts.googleapis.com/css?family=Noto+Sans:400,400i,700,700i" rel="stylesheet">
</head>
<body style="margin: 0px; padding: 0px; background-color: grey;font-family: \'Roboto\', sans-serif;letter-spacing: .3px;font-weight: 500;" bgcolor="#FFFFFF">
<table style="background-image: linear-gradient(to left, #f4bf03, #fec601);margin: auto;margin-top: 30px; font-family: \'Roboto\', sans-serif;font-size:14px;text-align: center;padding: 30px;" width="530" border="0" cellspacing="0" cellpadding="0">
	<tr>
		<td>
			<img style="height: 40px;width: auto;" src="<?php bloginfo('template_directory'); ?>/library/images/logo-white-2.png">
		</td>
	</tr>
</table>
<table style="background: #fff; margin: auto; font-family: \'Roboto\', sans-serif;font-size:14px;text-align: left;padding: 30px;line-height: 20px;" width="530" border="0" cellspacing="0" cellpadding="0">
	<tr>
		<td>
			<p style="font-size: 20px;"><b>Hai, Edi Susanto!</b></p>
			<p style="">Yeay! Akun anda sudah aktif sekarang. Sekarang belanja yuk!</p>
			<p style="text-align: center;margin: auto;margin-top: 30px;">
				<a style="background: #fec601;padding: 10px;padding-left: 20px;padding-right: 20px;color: #fff;text-decoration: none;font-weight: 600;font-size: 12px;" href="<?php echo home_url(); ?>">Belanja Sekarang</a>
			</p>
		</td>
	</tr>
	<tr>
		<td>
			<p style="width: 100px;height: 2px;background: #f0f0f0;margin: auto;margin-top: 30px;"></p>
			<p style="font-size: 11px; color: #555;line-height: normal;"><i>Email ini adalah email otomatis. Jangan membalas kepada email ini. Jika anda punya pertanyaan, silahkan hubungi kami di email <a href="#">cs@milenialmall.com</a>. </i></p>
		</td>
	</tr>
</table>
<table style="background: #f3f3f3; margin: auto; font-family: \'Roboto\', sans-serif;font-size:14px;text-align: left;padding: 30px;line-height: 22px;" width="530" border="0" cellspacing="0" cellpadding="0">
	<tr>
		<td>
			<p style="text-align: center;font-size: 11px;font-weight: 600;color: #888;background: #f3f3f3;">
				© 2019-2020 Milenialmall.com. <span>All Rights Reserved</span>
			</p>
		</td>
	</tr>
</table>
</body>
</html>

<?php endwhile; ?>
<?php else : ?>
		<?php get_template_part( 'content', '404pages' ); ?>	
<?php endif; ?>
<?php // get_footer(); ?>