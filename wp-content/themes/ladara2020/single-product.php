<?php get_header(); ?>
<?php if (have_posts()) :
    while (have_posts()) :
        the_post();
?>
<style type="text/css">
    @media only screen and (max-width:769px) {
        .row_fix_mobmenu {
            display: none !important;
        }
    }
</style>
<article id="post-<?php the_ID(); ?>" <?php post_class('clearfix'); ?> role="article" itemscope itemtype="http://schema.org/BlogPosting">
    <?php
    $current_user = wp_get_current_user();
    $u_id = $current_user->ID;
    $product_id = $post->ID;
    $pro_name = get_the_title($post->ID);
    $attch_id = $product->get_gallery_image_ids();

    $allcategory = array();
    $category = get_the_terms($product_id, 'product_cat');

    $new_listcategory = array();
    $count_category = count($category);

    $op = 0;
    for ($i = 0; $i < $count_category; $i++) {
        $count_listcat = count($new_listcategory);
        foreach ($category as $key => $value) {
            $cat_parent = $value->parent;

            if ((int)$count_listcat < (int)$count_category) {
                if ($cat_parent == 0) {
                    $new_listcategory[0] = $value;
                }
                if ($cat_parent == $new_listcategory[$op]->term_id) {
                    $new_listcategory[] = $value;
                    $op++;
                }
            }
        }
    }

    $product_price = $product->get_regular_price();
    $product_sale = $product->get_sale_price();
    $afiliasi_price = 0;

    if (isset($status_afilasi) and strtolower($status_afilasi) == 'yes') {
        $afiliasi_price = get_field('afilasi_price', $product_id);
    }


    $diskon_percentage = 0;

    if (isset($product_sale) and $product_sale != 0) {
        $pr_diskon = (((int)$product_price - (int)$product_sale) / (int)$product_price) * 100;
        $pr_diskon = round($pr_diskon, 2);
    } else {
        $pr_diskon = 0;
    }

    $product_sku = $product->get_sku();
    $product_weight = $product->get_weight();

    $product_brand_data = get_field('product_brand', $product_id);
    if (isset($product_brand_data)) {
        $product_brand = get_the_title($product_brand_data);
    } else {
        $product_brand = 'Tidak ada';
    }

    $product_description = get_field('product_description', $product_id);

    $product_ukuran = get_field('product_ukuran', $product_id);
    $product_bahan = get_field('product_bahan', $product_id);

    $seller_data = get_field('seller_name', $product_id);
    if (isset($seller_data) and !empty($seller_data)) {
        $seller_id = $seller_data->ID;
        $seller_name = get_the_title($seller_id);
    } else {
        $seller_id = 0;
        $seller_name = '';
    }

    // empty cart for first time =========
    if (!isset($_SESSION['empty_cart'])) {
        WC()->cart->empty_cart();
        unset($_SESSION['isucart']);
        $_SESSION['empty_cart'] = 'empty';
    }
    $count_cart = 0;
    $data_cart = '';
    WC()->cart->get_cart_from_session(); // Load users session
    WC()->cart->empty_cart(true); // Empty the cart
    WC()->session->set('cart', array()); // Empty the session cart data
    unset($_SESSION['isucart']);
    // empty cart for first time =========

    $mycart = array();
    $oneseller = 1;
    global $wpdb;
    $wpdb_query = "SELECT * FROM ldr_cart WHERE user_id = '$u_id' ";
    $res_query = $wpdb->get_results($wpdb_query, OBJECT);
    $count_res = count($res_query);
    foreach ($res_query as $key => $value) {
        $tb_id = $value->id;
        $tb_pro_id = $value->product_id;
        $tb_seller_id = $value->seller_id;
        $mycart[$tb_id] = array('pro_id' => $tb_pro_id, 'seller_id' => $tb_seller_id);

        if ($tb_seller_id != $seller_id) {
            $oneseller = 0;
        }
    }


    // for seo meta tags ====
    $pro_title = '';
    $pro_desc = '';
    // for seo meta tags ====

    ?>
    <div class="row row_singlePro">
        <div class="col-md-12 ht_proCategory">
            <div class="bx_shop_category">
                <a href="<?php echo home_url(); ?>/product/"><span class="cate">Shop</span></a>
                <?php
                foreach ($new_listcategory as $term => $value) {
                    $product_cat_id = $value->term_id;
                    $product_cat_slug = $value->slug;
                    $product_cat_name = $value->name;
                    array_push($allcategory, $product_cat_id); ?>
                    <span class="arrow"><img src="<?php bloginfo('template_directory'); ?>/library/images/next_shop.svg"></span>
                    <a href="<?php echo home_url(); ?>/product?category=<?php echo $product_cat_slug; ?>"><span class="cate"><?php echo $product_cat_name; ?></span></a>
                <?php
                }
                ?>
                <span class="arrow"><img src="<?php bloginfo('template_directory'); ?>/library/images/next_shop.svg"></span>
                <span class="cate"><?php echo get_the_title($post->ID); ?></span>
            </div>
        </div>
        <div class="col-md-12 col_singlePro">
            <div class="row row_top_singlePro">
                <div class="col-md-5 col_left_singlePro">
                    <?php
                    if ($u_id != 0) {
                        global $wpdb;
                        $wpdb_query = "SELECT * FROM ldr_wishlist WHERE user_id = '$u_id' AND product_id = '$product_id' ";
                        $res_query = $wpdb->get_results($wpdb_query, OBJECT);
                        $count_res = count($res_query);

                        if ($count_res == 0) {
                            $love_1 = 'act';
                            $love_2 = ''; ?>
                            <div class="visible-xs mg_heart addWishlist love_<?php echo $product_id; ?>" title="Tambah ke wishlist" data-id="<?php echo $product_id; ?>" style="top: 5px;right: 25px;">
                                <span class="glyphicon glyphicon-heart"></span>
                            </div>
                        <?php
                        } else {
                            $love_2 = 'act'; ?>
                            <div id="btn_" class="visible-xs mg_heart removewishlist love_<?php echo $product_id; ?> act" title="Hapus dari wishlist" data-id="<?php echo $product_id; ?>" style="top: 5px;right: 25px;">
                                <span class="glyphicon glyphicon-heart"></span>
                            </div>
                    <?php
                        }
                    }
                    ?>
                    <div id="slide_bigProduct" class="owl-carousel owl-theme popup-gallery">
                        <?php
                        foreach ($attch_id as $key => $value) {
                            $id_poto = $value;
                            $linkpoto = wp_get_attachment_image_src($id_poto, 'large');
                            $linkpoto_full = wp_get_attachment_image_src($id_poto, 'full');
                            if ($linkpoto) {
                                $large_photo = $linkpoto['0'];
                                $full_photo = $linkpoto_full[0];
                            } else {
                                $large_photo = get_template_directory_uri() . '/library/images/sorry.jpg';
                                $full_photo = get_template_directory_uri() . '/library/images/sorry.jpg';
                            }
                            $alt = get_post_meta($id_poto, '_wp_attachment_image_alt', true);
                            if (!isset($alt)) {
                                $alt = 'Milenial Mall Gallery';
                            } ?>
                            <div class="item" data-hash="photo-<?php echo $id_poto; ?>">
                                <a href="<?php echo $full_photo; ?>" rel="<div class='tx_magnific'></div>" title="View Photo Gallery">
                                    <div class="mg_slider_product">
                                        <img src="<?php echo $large_photo; ?>" alt="<?php echo $alt; ?>" title="klik untuk memperbesar foto.">
                                    </div>
                                </a>
                            </div>
                        <?php
                        }
                        ?>
                    </div>
                    <div class="box_bannerThumb">
                        <?php
                        foreach ($attch_id as $key => $value) {
                            $id_poto = $value;
                            $linkpoto = wp_get_attachment_image_src($id_poto, 'thumbnail');
                            $linkpoto_full = wp_get_attachment_image_src($id_poto, 'full');
                            if ($linkpoto) {
                                $thumb_photo = $linkpoto[0];
                                $full_photo = $linkpoto_full[0];
                            } else {
                                $thumb_photo = get_template_directory_uri() . '/library/images/sorry.jpg';
                                $full_photo = get_template_directory_uri() . '/library/images/sorry.jpg';
                            }
                            $alt = get_post_meta($id_poto, '_wp_attachment_image_alt', true);
                            if (!isset($alt)) {
                                $alt = 'Milenial Mall Gallery';
                            } ?>
                            <a href="#photo-<?php echo $id_poto; ?>" title="klik untuk melihat foto.">
                                <div class="mg_bannerThumb">
                                    <img src="<?php echo $thumb_photo; ?>" alt="<?php echo $alt; ?>">
                                </div>
                            </a>
                        <?php
                        }
                        ?>
                    </div>
                </div>
                <div class="col-md-7 col_right_singlePro">
                    <h1 class="title_singlePro"><?php echo $pro_name; ?></h1>
                    <div class="bx_rate_productDetail">
                        <?php /*
                $count_rate = rand(0,200);
                $full_star = rand(1,5);
                $empty_star = 5;

                if($count_rate != 0){
                ?>
                    <span class="sp_rate_count top"><?php echo $full_star; ?></span>
                <?php
                    for ($a=1; $a <= $empty_star; $a++) {

                        if($a <= $full_star){
                        ?>
                            <img src="<?php bloginfo('template_directory'); ?>/library/images/star1.svg">
                        <?php
                        }else{
                        ?>
                            <img src="<?php bloginfo('template_directory'); ?>/library/images/star0.svg">
                        <?php
                        }
                    }
                ?>
                    <span class="sp_rate_count right">(<?php echo $count_rate; ?>)</span>
                <?php
                } */
                        ?>
                        <!-- 	<span class="sp_dot"></span> -->
                        <span class="sp_brand"><?php echo $seller_name; ?></span>
                    </div>
                    <div id="product_stop" class="row row_procont">
                        <div class="col-md-3 col_procont_r0">
                            <div class="xt_sub_product">Harga</div>
                        </div>
                        <div class="col-md-9 col_procont_r1">
                            <div class="bx_pro_price">
                                <?php if (isset($product_sale) and $product_sale != 0) { ?>
                                    <div class="xt_price_product">
                                        <span class="pro_finalPrice"><span>Rp</span> <?php echo number_format($product_sale); ?></span>
                                    </div>
                                    <?php if (intval($product_price) !== intval($product_sale)) : ?>
                                        <div class="xt_price_product">
                                            <span class="pro_basePrice"><i>Rp <?php echo number_format($product_price); ?></i></span>
                                            <span class="sale_off">Hemat <?php echo $pr_diskon; ?>%</span>
                                        </div>
                                    <?php endif; ?>
                                <?php } else { ?>
                                    <div class="xt_price_product">
                                        <span class="pro_finalPrice"><span>Rp</span> <?php echo number_format($product_price); ?></span>
                                        <span class="sale_off"><span><i class="fas fa-star star"></i></span> Termurah</span>
                                    </div>
                                <?php } ?>
                            </div>
                        </div>
                    </div>
                    <form id="form_addtoCart" class="form_addtoCart" action="" method="get" onsubmit="submit_addtoCart(event);">
                        <input type="hidden" name="user_id" value="<?php echo $u_id; ?>">
                        <input type="hidden" name="pro_id" value="<?php echo $product_id; ?>">
                        <input type="hidden" name="slr_id" value="<?php echo $seller_id; ?>">

                        <div class="row row_procont product_varit animated">
                            <div class="col-md-3 col_procont_r0">
                                <div class="xt_sub_product">Variasi</div>
                            </div>
                            <div class="col-md-9 col_procont_r1">
                                <?php // ================ pilih warna =============
                                global $wpdb;
                                $ab = 0;
                                $ready = 0;
                                $wpdb_query = "SELECT DISTINCT warna_value FROM ldr_stock WHERE product_id = $product_id ";
                                $res_query = $wpdb->get_results($wpdb_query, OBJECT);
                                $count_res = count($res_query);
                                if ($count_res > 0) {
                                    $ready = 1; ?>
                                    <div class="bx_in_variation">
                                        <div class="ht_variation">Pilih warna:</div>
                                        <div class="bx_rad_variation">
                                            <?php
                                            foreach ($res_query as $key => $value) {
                                                $ab++;
                                                $warna_value = $value->warna_value; ?>
                                                <input type="radio" id="rad_<?php echo $ab; ?>" name="warna" value="<?php echo strtolower($warna_value); ?>" class="rad_var">
                                                <label class="label_var" for="rad_<?php echo $ab; ?>"><?php echo ucfirst($warna_value); ?></label>
                                            <?php
                                            } ?>
                                        </div>
                                    </div>
                                <?php
                                }
                                ?>
                                <?php // ================ pilih ukuran =============
                                global $wpdb;
                                $wpdb_query = "SELECT DISTINCT ukuran_value FROM ldr_stock WHERE product_id = $product_id ";
                                $res_query = $wpdb->get_results($wpdb_query, OBJECT);
                                $count_res = count($res_query);
                                if ($count_res > 0) {
                                    $detect_value = $res_query[0]->ukuran_value;
                                    if (isset($detect_value) and $detect_value != '') {
                                        $ready = 1; ?>
                                        <div class="bx_in_variation">
                                            <div class="ht_variation">Pilih Ukuran:</div>
                                            <div class="bx_rad_variation">
                                                <?php
                                                foreach ($res_query as $key => $value) {
                                                    $ab++;
                                                    $ukuran_value = $value->ukuran_value; ?>
                                                    <input type="radio" id="rad_<?php echo $ab; ?>" name="ukuran" value="<?php echo strtolower($ukuran_value); ?>" class="rad_var">
                                                    <label class="label_var1" for="rad_<?php echo $ab; ?>"><?php echo ucfirst($ukuran_value); ?></label>
                                                <?php
                                                } ?>
                                            </div>
                                        </div>
                                <?php
                                    }
                                }
                                ?>
                                <?php // ================ pilih nomor =============
                                global $wpdb;
                                $wpdb_query = "SELECT DISTINCT nomor_value FROM ldr_stock WHERE product_id = $product_id ";
                                $res_query = $wpdb->get_results($wpdb_query, OBJECT);
                                $count_res = count($res_query);
                                if ($count_res > 0) {
                                    $detect_value = $res_query[0]->nomor_value;
                                    if (isset($detect_value) and $detect_value != '') {
                                        $ready = 1; ?>
                                        <div class="bx_in_variation">
                                            <div class="ht_variation">Pilih Nomor:</div>
                                            <div class="bx_rad_variation">
                                                <?php
                                                foreach ($res_query as $key => $value) {
                                                    $ab++;
                                                    $nomor_value = $value->nomor_value; ?>
                                                    <input type="radio" id="rad_<?php echo $ab; ?>" name="nomor" value="<?php echo strtolower($nomor_value); ?>" class="rad_var">
                                                    <label class="label_var2" for="rad_<?php echo $ab; ?>"><?php echo ucfirst($nomor_value); ?></label>
                                                <?php
                                                } ?>
                                            </div>
                                        </div>
                                <?php
                                    }
                                }
                                ?>
                                <div class="err_msg_variation">Maaf, silahkan pilih variasi produk</div>
                            </div>
                        </div>

                        <div id="row_putQty" class="row row_procont">
                            <div class="col-md-3 col_procont_r0">
                                <div class="xt_sub_product">Jumlah</div>
                            </div>
                            <div class="col-md-9 col_procont_r1">

                                <div class="bx_ch_qty">
                                    <span class="sp_minus_qty">-</span>
                                    <input type="number" name="qty" value="1" class="txt_putqty" min="1" max="100" required="required" readonly="readonly">
                                    <span class="sp_plus_qty act">+</span>
                                    <span class="v_checkqty "></span>
                                </div>
                            </div>
                        </div>
                        <div class="row row_buyProduct">
                            <div class="col-md-12 col_procont_r0">
                                <?php if ($ready == 0) { ?>
                                    <style type="text/css">
                                        .product_varit,
                                        #row_putQty {
                                            display: none !important;
                                        }
                                    </style>
                                    <div class="bx_err_msg_addtocart">
                                        <div class="alert alert-danger" role="alert" style="font-weight: 500;">
                                            <B>Maaf,</B> seluruh stok produk ini habis.
                                        </div>
                                        <div class="err_msg_addtocart" style="display: block;"></div>
                                    </div>
                                <?php } else { // if stock variation ready
                                ?>
                                    <div class="bx_err_msg_addtocart">
                                        <div class="err_msg_addtocart">Maaf, stok untuk variasi di atas tidak tersedia.</div>
                                        <div class="sucs_msg_addtocart">Sukses menambahkan produk ini ke keranjang belanja Anda.</div>
                                    </div>
                                    <div class="bx_submit_addtocart row_fix_addtocart">
                                        <span>
                                            <input type="submit" data-sub="1" class="sub_buyProduct sub_addtoCart" value="Beli Sekarang">
                                        </span>
                                        <span id="addtocart_detail" class="bx_iconcart"><img src="<?php bloginfo('template_directory'); ?>/library/images/cart_black.svg"></span>
                                        <?php if (isset($u_id) and $u_id != 0) {
                                            global $wpdb;
                                            $wpdb_query = "SELECT * FROM ldr_wishlist WHERE user_id = '$u_id' AND product_id = '$product_id' ";
                                            $res_query = $wpdb->get_results($wpdb_query, OBJECT);
                                            $count_res = count($res_query);
                                            if ($count_res == 0) {
                                                $love_1 = 'act';
                                                $love_2 = ''; ?>
                                                <span id="addWishlist" class="bx_iconlove <?php echo $love_1; ?>" title="Tambah ke wishlist"><img src="<?php bloginfo('template_directory'); ?>/library/images/heart_2.svg"> tambah ke wishlist</span>
                                                <span id="removewishlist" class="bx_iconlove <?php echo $love_2; ?>" title="Hapus dari wishlist"><img src="<?php bloginfo('template_directory'); ?>/library/images/heart_1.svg"></span>
                                            <?php
                                            } else {
                                                $love_2 = 'acts'; ?>
                                                <span id="removewishlist" class="bx_iconlove <?php echo $love_2; ?> hidden-xs" title="Hapus dari wishlist"><img src="<?php bloginfo('template_directory'); ?>/library/images/heart_1.svg"> sudah di wishlist</span>
                                        <?php
                                            }
                                        }
                                        ?>
                                    </div>
                                <?php } ?>
                            </div>
                        </div>
                    </form>
                </div>
                <div class="col-md-12 col_singleProTabs">
                    <div class="row row_singleProTabs">
                        <ul class="proTabs">
                            <li class="act" data-id="tabs1"><a href="javascript:;">Deskripsi</a></li>
                            <li data-id="tabs2"><a href="javascript:;">Spesifikasi Lengkap</a></li>
                            <?php if (isset($u_id) and $u_id != 0) { ?>
                                <li data-id="tabs3"><a href="javascript:;">Diskusi</a></li>
                            <?php } ?>
                        </ul>
                    </div>
                    <div class="row row_singleProContent">
                        <?php // ============= TABS 1 ============
                        ?>
                        <div id="tabs1" class="col-md-12 box_detailProduct act animated fadeIn">
                            <h3 class="ht_ProTabs">Deskripsi Produk</h3>
                            <div class="ht_nameProTabs"><?php echo $pro_name; ?></div>
                            <?php
                            $content_post = get_post($product_id);
                            $content = $content_post->post_content;

                            if (isset($content) and $content != '') {
                                $pro_desc = $content;
                            ?>
                                <div class="box_tabsinfo2">
                                    <?php echo nl2br($content); ?>
                                </div>
                            <?php
                            } ?>
                        </div>
                        <?php // ============= TABS 2 ============
                        ?>
                        <div id="tabs2" class="col-md-12 box_detailProduct  animated fadeIn">
                            <h3 class="ht_ProTabs">Spesifikasi Produk</h3>
                            <div class="ht_nameProTabs"><?php echo $pro_name; ?></div>
                            <div class="row row_specsTabs">
                                <?php if (isset($product_brand) and $product_brand != '') { ?>
                                    <div class="bx_specsTabs">
                                        <span class="ht_specsTabs">Brand</span>
                                        <span class="tx_specsTabs"><?php echo $product_brand; ?></span>
                                    </div>
                                <?php } ?>
                                <?php if (isset($product_bahan) and $product_bahan != '') { ?>
                                    <div class="bx_specsTabs">
                                        <span class="ht_specsTabs">Bahan</span>
                                        <span class="tx_specsTabs"><?php echo $product_bahan; ?></span>
                                    </div>
                                <?php } ?>
                                <?php if (isset($product_ukuran) and $product_ukuran != '') { ?>
                                    <div class="bx_specsTabs">
                                        <span class="ht_specsTabs">Ukuran</span>
                                        <span class="tx_specsTabs"><?php echo $product_ukuran; ?></span>
                                    </div>
                                <?php } ?>
                                <?php if (isset($product_weight) and $product_weight != '') { ?>
                                    <div class="bx_specsTabs">
                                        <span class="ht_specsTabs">Berat</span>
                                        <span class="tx_specsTabs"><?php echo $product_weight; ?> gram</span>
                                    </div>
                                <?php } ?>
                                <?php if (isset($product_sku) and $product_sku != '') { ?>
                                    <div class="bx_specsTabs">
                                        <span class="ht_specsTabs">SKU</span>
                                        <span class="tx_specsTabs"><?php echo $product_sku; ?></span>
                                    </div>
                                <?php } ?>
                            </div>
                        </div>
                        <?php // ============= TABS 3 ============
                        ?>
                        <?php if (isset($u_id) and $u_id != 0) { ?>
                            <div id="tabs3" class="col-md-12 box_detailProduct  animated fadeIn">
                                <?php
                                global $wpdb;
                                $wpdb_query = "SELECT * FROM eod_comments WHERE product_id = '$product_id' AND review_type = '1' AND reply_id = '0' AND user_id = '$u_id' ";
                                $res_query_main = $wpdb->get_results($wpdb_query, OBJECT);
                                $count_res_main = count($res_query_main);

                                ?>
                                <div class="box_allulasan">
                                    <h3 class="ht_ProTabs">Spesifikasi Produk</h3>
                                    <div class="ht_nameProTabs"><?php echo $pro_name; ?></div>
                                    <?php if ($count_res_main == 0) { ?>
                                        <div class="row row_diskusi">
                                            <div class="col-md-6 col_diskusi">
                                                <form class="form_diskusi" data-id="0">
                                                    <input type="hidden" name="pro_id" value="<?php echo $product_id; ?>">
                                                    <div class="f_diskusi">
                                                        <textarea name="diskusi_com" class="area_diskusi" placeholder="Tulis pesan..." minlength="10" maxlength="300" required="required"></textarea>
                                                        <label class="lab_error">Maksimal 300 karakter.</label>
                                                        <label class="lab_success"></label>
                                                    </div>
                                                    <input type="button" class="sub_diskusi" value="Kirim" onclick="submit_diskusi(0);">
                                                </form>
                                            </div>
                                        </div>
                                    <?php } ?>
                                </div>
                                <div class="row box_content_ulasan">
                                    <?php
                                    global $wpdb;

                                    $currentUserAvatar = get_field('user_avatar', 'user_' . $u_id);
                                    if ($currentUserAvatar !== '') {
                                        $linkpoto = wp_get_attachment_image_src($currentUserAvatar, 'thumbnail');
                                        $currentUrlProfilePhoto = $linkpoto[0];
                                    } else {
                                        $currentUrlProfilePhoto = get_template_directory_uri() . '/library/images/icon_profile.png';
                                    }

                                    $queryDiscussion = $wpdb->get_results("SELECT * FROM ldr_discussions WHERE deleted_at IS NULL AND product_id = 2");

                                    foreach ($queryDiscussion as $discussion) {
                                        $queryDicussionDetail = $wpdb->get_results("SELECT * FROM ldr_discussion_details WHERE deleted_at IS NULL AND discussion_id = " . $discussion->id); ?>

                                        <div class="col-md-12 col_userDiskusi">

                                            <?php
                                            foreach ($queryDicussionDetail as $key => $discussionDetail) {
                                                $postDate = date('d/m/Y H:i', strtotime($discussionDetail->created_at));

                                                if ($discussionDetail->discussionner_type === 'App\User') {
                                                    $userDiscussion = get_userdata($discussionDetail->discussionner_id);
                                                    $shortName = substr($userDiscussion->display_name ?? '', 0, -3) . '*****';
                                                    $userAvatar = get_field('user_avatar', 'user_' . $discussionDetail->discussionner_id);

                                                    if ($userAvatar !== '') {
                                                        $linkpoto = wp_get_attachment_image_src($userAvatar, 'thumbnail');
                                                        $urlProfilePhoto = $linkpoto[0];
                                                    } else {
                                                        $urlProfilePhoto = get_template_directory_uri() . '/library/images/icon_profile.png';
                                                    }
                                                } else {
                                                    $userDiscussion = $wpdb->get_row("SELECT * FROM ldr_merchants WHERE id = " . $discussionDetail->discussionner_id);
                                                    $shortName = $userDiscussion->name;

                                                    $urlProfilePhoto = home_url('/') . $userDiscussion->picture;
                                                }

                                                if ($key === 0) :
                                                ?>
                                                    <div class="box_left_diskusi">
                                                        <div class="mg_kprofile">
                                                            <img src="<?php echo $urlProfilePhoto; ?>">
                                                        </div>
                                                    </div>
                                                    <div class="box_right_diskusi">
                                                        <div class="comment_disuser">
                                                            <?php echo $discussionDetail->content; ?>
                                                        </div>
                                                        <div class="ht_kprofile"><?php echo $shortName . ' . <span>' . $postDate . '</span>'; ?></div>
                                                    <?php else : ?>
                                                        <div class="row row_admDisReply">
                                                            <div class="bx_left_repdis">
                                                                <div class="mg_kprofile">
                                                                    <img src="<?php echo $urlProfilePhoto; ?>">
                                                                </div>
                                                            </div>
                                                            <div class="bx_right_repdis">
                                                                <span class="repdis_com"><?php echo $discussionDetail->content; ?></span><br />
                                                                <span class="repdis_prof"><?php echo $shortName . ' . <span>' . $postDate . '</span>'; ?></span>
                                                            </div>
                                                        </div>
                                                <?php
                                                endif;
                                            }
                                            ?>
                                                <div class="row row_diskusi">
                                                    <div class="col-md-2">
                                                        <div class="mg_kprofile" style="float: none; margin-left: 45px;">
                                                            <img src="<?php echo $currentUrlProfilePhoto; ?>">
                                                        </div>
                                                    </div>
                                                    <div class="col-md-10 col_diskusi">
                                                        <form class="form_diskusi" data-id="<?php echo $discussion->id ?>">
                                                            <input type="hidden" name="pro_id" value="<?php echo $product_id; ?>">
                                                            <div class="f_diskusi">
                                                                <textarea name="diskusi_com" class="area_diskusi" placeholder="Tulis pesan..." minlength="10" maxlength="300" required="required" rows="2"></textarea>
                                                                <label class="lab_error">Maksimal 300 karakter.</label>
                                                                <label class="lab_success"></label>
                                                            </div>
                                                            <input type="button" class="sub_diskusi" value="Kirim" onclick="submit_diskusi(<?php echo $discussion->id ?>);">
                                                        </form>
                                                    </div>
                                                </div>
                                                    </div>
                                        </div>
                                <?php
                                    }
                                }
                                ?>
                                </div>
                            </div>
                    </div>
                </div>
            </div>
            <?php // ================================== RELATED PRODUCT =============================
            ?>
            <div class="row row_relatedProduct">
                <h3 class="ht_sub_productdetail">Rekomendasi Produk</h3>
                <?php
                $client = new GuzzleHttp\Client();
                try {
                    $params = [
                            'limit' => 12,
                            'price' => 'rand'
                    ];
                    $req = $client->request('GET', MERCHANT_URL . '/api/product/search-products', [
                        'query' => $params
                    ]);
                    $response = json_decode($req->getBody());
                } catch (\GuzzleHttp\Exception\GuzzleException $e) {
                    $response = [];
                }
                ?>

                <div id="slide_relatedProduct" class="owl-carousel owl-theme">
                    <?php foreach ($response as $product): ?>
                        <div class="item col_bx_productList">
                            <div class="bx_productList">
                                <a href="<?php echo $product->permalink.'-'.$product->id; ?>" title="Lihat <?php echo $product->title; ?>">
                                    <?php if ($product->wishlist):?>
                                        <div id="btn_" class="mg_heart removewishlist love_<?php echo $product->id; ?> act" title="Hapus dari wishlist" data-id="<?php echo $product->id; ?>">
                                            <span class="glyphicon glyphicon-heart"></span>
                                        </div>
                                    <?php else: ?>
                                        <div class="mg_heart addWishlist love_<?php echo $product->id; ?>" title="Tambah ke wishlist" data-id="<?php echo $product->id; ?>">
                                            <span class="glyphicon glyphicon-heart"></span>
                                        </div>
                                    <?php endif;?>
                                    <div class="mg_productList">
                                        <img class="" src="<?php echo $product->images; ?>" alt="<?php echo $product->title; ?>">
                                    </div>
                                    <div class="bx_in_productList">
                                        <h4 class="ht_productList"><?php echo $product->title; ?></h4>
                                        <?php if ($product->sale_price): ?>
                                            <div class="tx_price">Rp <?php echo number_format($product->sale_price); ?></div>
                                            <div class="tx_price_sale">Rp <?php echo number_format($product->price); ?></div>
                                        <?php  else:  ?>
                                            <div class="tx_price">Rp <?php echo number_format($product_price); ?></div>
                                        <?php endif; ?>
                                        <div class="bx_rate_product">
                                        </div>
                                    </div>
                            </div>
                            </a>
                        </div>
                    <?php endforeach; ?>
                </div>

            </div>
        </div>
        <div class="col-md-12 col_kontenPro">
        </div>
    </div>

    <?php // ============= Pop up Gallery ==================
    ?>
    <?php insertPostViewed($post->ID); ?>
    <?php // =============================== POP UP SUCESS ======================
    ?>
    <?php get_template_part('content', 'pop-addcart'); ?>
    <?php // =============================== POP UP SUCESS ======================
    ?>

</article>


<?php 
$pro_desc = strip_tags($pro_desc);
?>

<script type="application/ld+json">
{
  "@context": "https://schema.org",
  "@type": "Product",
  "aggregateRating": {
    "@type": "AggregateRating",
      "ratingValue": "4.12",
      "reviewCount": "213"
  },
  "description": "<?php echo $pro_desc; ?>",
  "name": "<?php echo $pro_title; ?>",
  "image": "kenmore-microwave-17in.jpg",
  "offers": {
    "@type": "Offer",
    "availability": "https://schema.org/InStock",
    "price": "55.00",
    "priceCurrency": "USD"
  },
  "review": [
    {
      "@type": "Review",
      "author": "Ellie",
      "datePublished": "2011-04-01",
      "reviewBody": "The lamp burned out and now I have to replace it.",
      "name": "Not a happy camper",
      "reviewRating": {
        "@type": "Rating",
        "bestRating": "5",
        "ratingValue": "1",
        "worstRating": "1"
      }
    },
    {
      "@type": "Review",
      "author": "Lucas",
      "datePublished": "2011-03-25",
      "reviewBody": "Great microwave for the price. It is small and fits in my apartment.",
      "name": "Value purchase",
      "reviewRating": {
        "@type": "Rating",
        "bestRating": "5",
        "ratingValue": "4",
        "worstRating": "1"
      }
    }
  ]
}
</script>

    <?php endwhile; ?>
<?php else : ?>
    <article id="post-not-found" class="hentry clearfix">
        <header class="article-header">
            <h1><?php _e('Oops, Post Not Found!', 'bonestheme'); ?></h1>
        </header>
        <section class="entry-content">
            <p><?php _e('Uh Oh. Something is missing. Try double checking things.', 'bonestheme'); ?></p>
        </section>
        <footer class="article-footer">
            <p><?php _e('This is the error message in the single.php template.', 'bonestheme'); ?></p>
        </footer>
    </article>
<?php endif; ?>
<?php get_footer(); ?>