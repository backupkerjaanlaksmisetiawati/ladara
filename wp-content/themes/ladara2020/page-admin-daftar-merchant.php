<?php
/*
Template Name: Daftar Merchant Dashboard Page
*/
?>

<?php get_header('admin'); ?>

<?php if (have_posts()) :
    while (have_posts()) :
        the_post();
        date_default_timezone_set("Asia/Jakarta");
        $nowdate = date('Y-m-d H:i:s');

        $current_user = wp_get_current_user();
        $u_id = $current_user->ID;
        $u_roles = $current_user->roles;
        $admin = 0;

        if (isset($u_roles) and !empty($u_roles)) {
            foreach ($u_roles as $key => $value) {
                if (strtolower($value) == 'administrator') {
                    $admin = 1;
                }
                if (strtolower($value) == 'shop_manager') {
                    $admin = 1;
                }
            }
        }

        if ((isset($u_id) and $u_id != 0) and $admin == 1) {
?>

            <!-- Begin Page Content -->
            <div class="container-fluid">

                <!-- Page Heading -->
                <h1 class="h3 mb-2 text-gray-800">Data Toko</h1>

                <!-- DataTales Example -->
                <div class="card shadow mb-4">
                    <div class="card-header py-3">
                        <h6 class="m-0 font-weight-bold text-primary ht_admdash">&nbsp;</h6>
                    </div>
                    <div class="card-body">
                        <div class="table-responsive">
                            <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0" data-order="[[ 0, &quot;desc&quot; ]]">
                                <thead>
                                    <tr>
                                        <th>ID Toko</th>
                                        <th>Nama Toko</th>
                                        <th>URL Toko</th>
                                        <th>Tanggal Pendaftaran</th>
                                        <th>Status</th>
                                        <th>Aksi</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                    global $wpdb;
                                    $res_query = $wpdb->get_results("SELECT * FROM ldr_merchants", OBJECT);
                                    $res_count = count($res_query);

                                    if ($res_count > 0) {
                                        foreach ($res_query as $key => $value) {
                                    ?>
                                            <tr>
                                                <td><?php echo $value->id; ?></td>
                                                <td><?php echo $value->name; ?></td>
                                                <td><?php echo $value->url; ?></td>
                                                <td><?php echo $value->created_at ?></td>
                                                <td>
                                                    <?php
                                                    if ($value->status == '1') {
                                                        echo '<span style="color: green;">Aktif</span>';
                                                    } else if ($value->status == '-1') {
                                                        echo '<span style="color: red;">Suspend</span>';
                                                    } else if ($value->status == '-2') {
                                                        echo '<span style="color: blue;">Tutup</span>';
                                                    } else {
                                                        echo '<span>Tidak Aktif</span>';
                                                    }
                                                    ?>
                                                </td>
                                                <td>
                                                    <a href="<?php echo home_url(); ?>/dashboard/daftar-merchant/detail?id=<?php echo $value->id; ?>">
                                                        <button class="btn btn-info">
                                                            <b>Detail</b>
                                                        </button>
                                                    </a>
                                                </td>
                                            </tr>
                                    <?php
                                        }
                                    } ?>

                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
            <!-- /.container-fluid -->
        <?php
        } else { ?>
            <script>
                // 'Getting' data-attributes using getAttribute
                var plant = document.getElementById('body');
                console.log(plant);
                var hurl = plant.getAttribute('data-hurl');
                location.replace(hurl + '/login/');
            </script>
        <?php } ?>

    <?php endwhile; ?>
<?php else : ?>
    <?php get_template_part('content', '404pages'); ?>
<?php endif; ?>
<?php get_footer('admin'); ?>