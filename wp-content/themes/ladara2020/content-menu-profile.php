<?php
date_default_timezone_set("Asia/Jakarta");
$current_user = wp_get_current_user();
$u_id = $current_user->ID;
$user_login = $current_user->user_login;
$user_avatar = get_field('user_avatar', 'user_' . $u_id);
if (isset($user_avatar) and $user_avatar != '') {
    $linkpoto = wp_get_attachment_image_src($user_avatar, 'thumbnail');
    $url_avatar = $linkpoto[0];
} else {
    $url_avatar = get_template_directory_uri() . '/library/images/icon_profile.png';
}

$nama_lengkap = get_field('nama_lengkap', 'user_' . $u_id);
$tanggal_lahir = get_field('tanggal_lahir', 'user_' . $u_id);
$telepon = get_field('telepon', 'user_' . $u_id);
$jenis_kelamin = get_field('jenis_kelamin', 'user_' . $u_id);

?>

<div class="bx_signProfile">
    <div class="ico_profile">
        <div class="mg_ico_profile">
            <img src="<?php echo $url_avatar; ?>" alt="My Account - Milenial Mall">
        </div>
        <h3 class="xt_profile"><?php echo $nama_lengkap ?></h3>
    </div>

    <div class="bx_menuProfile">
        <div class="bx_fmenu">
            <div class="ht_fmenu act">Profile Saya <span class="glyphicon glyphicon-menu-up"></span></div>
            <div class="chl_fmenu act">
                <a class="pm_profile act" href="<?php echo home_url(); ?>/profil/">Profil</a>
                <a class="pm_addres" href="<?php echo home_url(); ?>/profil/address/">Alamat</a>
                <a class="pm_security" href="<?php echo home_url(); ?>/profil/security/">Ubah Kata Sandi</a>
                <?php /*<a class="pm_bank" href="<?php echo home_url(); ?>/profile/bank/">Bank</a>*/ ?>
                <a class="pm_link_to_treasury" href="<?php echo home_url(); ?>/profile/connect-to-treasury/">Connect to Treasury Account</a>
            </div>
        </div>

        <div class="bx_fmenu">
            <div class="ht_fmenu act">Pesanan Saya <span class="glyphicon glyphicon-menu-up"></span></div>
            <div class="chl_fmenu act">
                <a class="p_myorder" href="<?php echo home_url(); ?>/my-order/">Status Pesanan</a>
                <a class="p_history" href="<?php echo home_url(); ?>/my-history/">Riwayat Pesanan</a>
            </div>
        </div>

        <div class="bx_fmenu">
            <a href="<?php echo home_url(); ?>/wishlist/">
                <div class="ht_fmenu p_wishlist">Wishlist</div>
            </a>
        </div>
    </div>
</div>