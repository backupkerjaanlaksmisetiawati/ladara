<?php
/*
Template Name: My Order Page
*/

use Shipper\Order;

?>

<?php get_header(); ?>
<?php if (have_posts()) :
    while (have_posts()) :
        the_post();

        global $wpdb;
        date_default_timezone_set("Asia/Jakarta");
        $nowdate = date('Y-m-d');

        $currentUser = wp_get_current_user();
        $userId = $currentUser->ID;
        $user_login = $currentUser->user_login;
        $user_avatar = get_field('user_avatar', 'user_' . $userId);
        $mile_reward = get_field('mile_reward', 'user_' . $userId);
        $nama_lengkap = get_field('nama_lengkap', 'user_' . $userId);
        $tanggal_lahir = get_field('tanggal_lahir', 'user_' . $userId);

        if (isset($tanggal_lahir)) {
            $dob_data = explode('-', $tanggal_lahir);
            $dob_year = $dob_data[0];
            $dob_month = $dob_data[1];
            $dob_day = $dob_data[2];
        }

        $telepon = get_field('telepon', 'user_' . $userId);
        $jenis_kelamin = get_field('jenis_kelamin', 'user_' . $userId);

        if (isset($userId) and $userId != 0) {
            if (isset($_GET['from_date'])) {
                $from_date = $_GET['from_date'];
            } else {
                $from_date = '';
                $from_date = date("Y-m-d", strtotime($nowdate . "-30 days"));
            }

            if (isset($_GET['to_date'])) {
                $to_date = $_GET['to_date'];
            } else {
                $to_date = $nowdate;
            }

            $after_odate = $from_date . ' 00:00:00';
            $before_odate = $to_date . ' 23:59:59';

?>
            <div class="row"></div>
            <style>
                .btn-ulasan{
                    background: #ffffff;
                    border: 1px solid #0080ff;
                    color: #0080ff;
                }
            </style>

            <div id="myorderpage" class="row row_profile">
                <div class="col-md-3 col_profile des_display">
                    <?php get_template_part('content', 'menu-profile'); ?>
                </div>
                <div class="col-md-9 col_profile">
                    <div class="row row_cont_tab_profile">
                        <h1 class="ht_profile ht_myvoucher">Status Pesanan</h1>
                        <div class="col-md-12 col_cont_myorder">
                            <form class="form_filterMyOrder" onsubmit="submit_filterMyOrder_2(event);" method="post" id="form_filterMyOrder">
                                <div class="row row_filter_myorder">
                                    <div class="col-md-3 col_fl_myorder">
                                        <div class="f_search_myorder">
                                            <label>Filter dari tanggal</label>
                                            <input type="text" name="from_date" class="txt_date_myorder all_datepicker" value="<?php echo $from_date; ?>" placeholder="<?php echo $nowdate; ?>">
                                            <span class="sp_datepick"><i class="far fa-calendar-alt"></i></span>
                                        </div>
                                    </div>
                                    <div class="col-md-3 col_fl_myorder">
                                        <div class="f_search_myorder">
                                            <label>sampai tanggal</label>
                                            <input type="text" name="to_date" class="txt_date_myorder all_datepicker" value="<?php echo $to_date; ?>" placeholder="<?php echo $nowdate; ?>">
                                            <span class="sp_datepick"><i class="far fa-calendar-alt"></i></span>
                                        </div>
                                    </div>
                                    <div class="col-md-3 col_fl_myorder">
                                        <input type="submit" id="wp-submit" class="sub_filter_myorder" value="cari">
                                    </div>
                                </div>
                            </form>
                        </div>

                        <div class="row row_myorder_tabs ">
                            <ul class="order_tabs des_display">
                                <li class="act" data-id="tabs1"><a>Semua</a></li>
                                <li data-id="tabs2"><a>Menunggu Konfirmasi</a></li>
                                <li data-id="tabs3"><a>Diproses</a></li>
                                <li data-id="tabs4"><a>Dikirim</a></li>
                                <li data-id="tabs5"><a>Sampai Tujuan</a></li>
                            </ul>
                            <select class="sel_myorder_tabs mob_display" id="mob_myorder_tabs">
                                <option value="tabs1">Semua</option>
                                <option value="tabs2">Menunggu Konfirmasi</option>
                                <option value="tabs3">Diproses</option>
                                <option value="tabs4">Dikirim</option>
                                <option value="tabs5">Sampai Tujuan</option>
                            </select>
                        </div>

                        <?php
                        // $queryOrderMerchants = $wpdb->get_results("SELECT * FROM ldr_order_merchants om LEFT JOIN ldr_orders o ON om.order_id = o.id WHERE o.user_id = $userId AND o.created_at BETWEEN '$after_odate' AND '$before_odate' AND om.deleted_at IS NULL ORDER BY om.id DESC");
                        $queryOrders = $wpdb->get_results("SELECT * FROM ldr_orders WHERE user_id = $userId AND created_at BETWEEN '$after_odate' AND '$before_odate' AND deleted_at IS NULL ORDER BY id DESC");
                        $orderMerchantsWithStatus1 = [];
                        $orderMerchantsWithStatus2 = [];
                        $orderMerchantsWithStatus3 = [];
                        $orderMerchantsWithStatus4 = [];
                        $tempOrderMerchants = []; // For storing order merchants so we don't need to call database again
                        $tempOrderItems = []; // For storing order items so we don't need to call database again
                        ?>

                        <div id="tabs1" class="row row_box_myorder animated fadeIn act">
                            <?php
                            if (count($queryOrders)) {
                                foreach ($queryOrders as $order) {
                                    $queryOrderMerchants = $wpdb->get_results("SELECT * FROM ldr_order_merchants WHERE deleted_at IS NULL AND status IN (0, 1, 2, 3, 4) AND order_id = " . $order->id);
                                    $tempOrderMerchants[$order->id] = $queryOrderMerchants;
                                    $paymentLog = $wpdb->get_row("SELECT * FROM ldr_payment_log WHERE order_id = " . $order->id);

                                    foreach ($queryOrderMerchants as $orderMerchant) {
                                        $orderDate = date("d M Y", strtotime($orderMerchant->created_at));
                                        $adminFee = $order->xendit_fee / count($queryOrderMerchants);
                                        $totalFee = $orderMerchant->shipping_price + $orderMerchant->insurance_price + $adminFee;
                                        $awbNumber = '';
                                        $courierName = '';
                                        $courierRateName = '';
                                        $voucherName = '';
                                        $discountVoucher = 0;

                                        if ($order->voucher_id !== '' && $order->voucher_id !== null) {
                                            $voucherName = $wpdb->get_var("SELECT code FROM ldr_vouchers WHERE id = {$order->voucher_id}");
                                            $discountVoucher = $order->discount_voucher_price / count($queryOrderMerchants);
                                            $totalFee -= $discountVoucher;
                                        }

                                        if ($orderMerchant->shipment_provider === 'SHIPPER') {
                                            $shipperOrder = $wpdb->get_row("SELECT * FROM ldr_shipper_orders WHERE order_merchant_id = $orderMerchant->id");
                                            $courier = $wpdb->get_row("SELECT * FROM ldr_shipper_couriers WHERE id = " . $shipperOrder->courier_id);
                                            $courierName = $courier->name;
                                            $courierRateName = $courier->rate_name;
                                            $awbNumber = $shipperOrder->awb_number;
                                        } else {
                                            $courier = $wpdb->get_row("SELECT * FROM ldr_jne_orders WHERE order_merchant_id = $orderMerchant->id");
                                            $courierName = 'JNE';
                                            $courierRateName = $courier->service_code ?? '';
                                            $awbNumber = $courier->awb_number ?? '';
                                        }

                                        if ($awbNumber === '') {
                                            $awbNumber = '-';
                                        }

                                        $orderMerchant->voucherName = $voucherName;
                                        $orderMerchant->discountVoucher = $discountVoucher;
                                        $orderMerchant->awbNumber = $awbNumber;
                                        $orderMerchant->courier_name = $courierName;
                                        $orderMerchant->courier_rate_name = $courierRateName;
                                        $orderMerchant->payment_method = $paymentLog->payment_method;
                                        $orderMerchant->checkout_url = $order->xendit_url;
                            ?>

                                        <div class="col-md-12 box_list_myorder">
                                            <div class="ht_cont_myorder"><?php echo $orderDate; ?> (<span><?php echo $orderMerchant->no_invoice; ?></span>)</div>
                                            <div class="row row_fill2_myorder">
                                                <div class="col-md-12 col_fill_myorder">
                                                    <?php
                                                    $queryOrderItems = $wpdb->get_results("SELECT * FROM ldr_order_items WHERE deleted_at IS NULL AND order_merchant_id = " . $orderMerchant->id);
                                                    $tempOrderItems[$orderMerchant->id] = $queryOrderItems;
                                                    $totalPrice = 0;

                                                    foreach ($queryOrderItems as $key => $orderItem) {
                                                        $totalPrice += $orderItem->price * $orderItem->qty;
                                                    }

                                                    foreach ($queryOrderItems as $key => $orderItem) {
                                                        $product = $wpdb->get_row("SELECT * FROM ldr_products WHERE id = " . $orderItem->product_id);
                                                        $productWeight = intval($product->weight ?? 0) * $orderItem->qty;
                                                        $mainImage = $wpdb->get_row("SELECT * FROM ldr_product_images WHERE name = 'imageUtama' AND deleted_at IS NULL AND products_id = " . $orderItem->product_id);

                                                        if ($mainImage !== null) {
                                                            $urlphoto = home_url('/') . $mainImage->path;
                                                        } else {
                                                            $urlphoto = get_template_directory_uri() . '/library/images/sorry.png';
                                                        }

                                                        $urlphoto = str_replace(home_url(), IMAGE_URL, $urlphoto);
                                                    ?>

                                                        <div class="row row_orderlist_myorder" <?php echo $key === 0 ? '' : 'style="border-top: 1px solid rgba(0, 0, 0, .1);"'; ?>>
                                                            <div class="col-md-6 col_in_myorder ">
                                                                <div class="box_mg_myorder">
                                                                    <img src="<?php echo $urlphoto; ?>" alt="<?php echo $product->title ?? 'Deleted Product'; ?>">
                                                                </div>
                                                                <div class="box_cont_pro_myorder">
                                                                    <a href="<?= ($product === null ? '#' : '/product/' . $product->permalink . '-' . $product->id) ?>" title="Lihat produk lengkap...">
                                                                        <div class="ht_pro_myorder">
                                                                            <?php echo $product->title ?? 'Deleted Product'; ?>
                                                                        </div>
                                                                    </a>
                                                                    <div class="sm_myorder">
                                                                        <span class="price">Rp <?php echo number_format($orderItem->price); ?></span> <br />
                                                                        <br />
                                                                        <span class="qty"><?php echo $orderItem->qty; ?> barang (<?php echo $productWeight; ?> gram)</span>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-3 col_in_myorder in_border">
                                                                <?php if ($key === 0) { ?>
                                                                    <div class="ft_myorder order_space">
                                                                        <label>Status</label>

                                                                        <?php
                                                                        if ($orderMerchant->status === '4') {
                                                                            $orderMerchantsWithStatus4[] = $orderMerchant; ?>
                                                                            <div class="tt_myorder color_delivered"><b>Diterima</b></div>
                                                                        <?php
                                                                        } elseif ($orderMerchant->status === '3') {
                                                                            $orderMerchantsWithStatus3[] = $orderMerchant; ?>
                                                                            <div class="tt_myorder color_ondelivery"><b>Dikirim</b></div>
                                                                        <?php
                                                                        } elseif ($orderMerchant->status === '2') {
                                                                            $orderMerchantsWithStatus2[] = $orderMerchant; ?>
                                                                            <div class="tt_myorder color_process"><b>Diproses</b></div>
                                                                        <?php
                                                                        } elseif ($orderMerchant->status === '1') {
                                                                            $orderMerchantsWithStatus1[] = $orderMerchant; ?>
                                                                            <div class="tt_myorder color_process"><b>Menunggu Konfirmasi</b></div>
                                                                        <?php
                                                                        } else {
                                                                        ?>
                                                                            <div class="tt_myorder"><b>Menunggu Pembayaran</b></div>
                                                                        <?php
                                                                        }
                                                                        ?>

                                                                    </div>
                                                                    <div class="ft_myorder des_display">
                                                                        <label>Total Harga Produk</label>
                                                                        <div class="tt_myorder"><b class="price">Rp <?php echo number_format($orderItem->qty * $orderItem->price); ?></b></div>
                                                                    </div>
                                                                <?php } else { ?>
                                                                    <div class="ft_myorder ano_product des_display">
                                                                        <label>Total Harga Produk</label>
                                                                        <div class="tt_myorder"><b class="price">Rp <?php echo number_format($orderItem->qty * $orderItem->price); ?></b></div>
                                                                    </div>
                                                                <?php } ?>
                                                            </div>
                                                            <?php if ($key === 0) { ?>
                                                                <div class="col-md-3 col_in_myorder in_last">
                                                                    <div class="ft_myorder_total">
                                                                        <label>Total Belanja</label>
                                                                        <br />
                                                                        <label class="price">Rp <?php echo number_format($totalPrice + $totalFee); ?></label>
                                                                    </div>
                                                                    <?php if (intval($orderMerchant->status) === 0 && in_array($orderMerchant->payment_method, ['cc', 'ewallet'])) { ?>
                                                                        <a href="<?= $orderMerchant->checkout_url ?>">
                                                                            <input type="button" class="btn_buyagain des_display" value="Lanjutkan Pembayaran" style="background: transparent linear-gradient(90deg, #00CF3E 0%, #0080FF 100%) 0% 0% no-repeat padding-box;">
                                                                        </a>
                                                                    <?php } ?>
                                                                    <a href="<?= ($product === null ? '#' : '/product/' . $product->permalink . '-' . $product->id) ?>">
                                                                        <input type="button" class="btn_buyagain des_display" value="Beli Lagi">
                                                                    </a>
                                                                    <?php if (intval($orderMerchant->status) === 4):?>
                                                                    <?php
                                                                        global $wpdb;
                                                                        $cekUlasan = $wpdb->get_row("select * from ldr_product_reviews
                                                                                                    where product_id = $product->id and order_merchant_id = $orderMerchant->id
                                                                                                    and user_id = $userId")
                                                                        ?>
                                                                        <?php if (!$cekUlasan):?>
<!--                                                                            <a href="--><?//= ($product === null ? '#' : '/my-order/tambah-ulasan?order_merchant_id=' . $orderMerchant->id . '&product_id=' . $product->id) ?><!--">-->
<!--                                                                                <input type="button" class="btn_buyagain des_display btn-ulasan" value="Beri Ulasan">-->
<!--                                                                            </a>-->
                                                                        <?php endif;?>
                                                                    <?php endif;?>
                                                                </div>
                                                            <?php } else { ?>
                                                                <div class="col-md-3 col_in_myorder in_last">
                                                                    <a href="<?= ($product === null ? '#' : '/product/' . $product->permalink . '-' . $product->id) ?>">
                                                                        <input type="button" class="btn_buyagain des_display add_mar" value="Beli Lagi">
                                                                    </a>
                                                                    <?php if (intval($orderMerchant->status) === 4):?>
                                                                        <?php
                                                                        global $wpdb;
                                                                        $cekUlasan = $wpdb->get_row("select * from ldr_product_reviews
                                                                                                    where product_id = $product->id and order_merchant_id = $orderMerchant->id
                                                                                                    and user_id = $userId")
                                                                        ?>
                                                                        <?php if (!$cekUlasan):?>
<!--                                                                            <a href="--><?//= ($product === null ? '#' : '/my-order/tambah-ulasan?order_merchant_id=' . $orderMerchant->id . '&product_id=' . $product->id) ?><!--">-->
<!--                                                                                <input type="button" class="btn_buyagain des_display btn-ulasan" value="Beri Ulasan">-->
<!--                                                                            </a>-->
                                                                        <?php endif;?>
                                                                    <?php endif;?>
                                                                </div>
                                                            <?php } ?>
                                                        </div>
                                                    <?php
                                                    } ?>
                                                </div>
                                                <div class="col-md-12 col_fill_myorder2">
                                                    <div class="row bx_infoResi">
                                                        <div class="col-md-6 col_infoResi">
                                                            <div class="icon_freeshipping">
                                                                <img class="mg_car" src="<?php bloginfo('template_directory'); ?>/library/images/normal_delivery.svg">
                                                            </div>
                                                            <div class="bx_info_noResi">
                                                                <div class="hx_noResi">Jasa : <b><?php echo $orderMerchant->courier_name; ?> (<?php echo $orderMerchant->courier_rate_name; ?>)</b></div>
                                                                <?php if ($orderMerchant->status === '3' || $orderMerchant->status === '4') : ?>
                                                                    <div class="hx_noResi">Resi : <b><?= $orderMerchant->awbNumber ?></b> </div>
                                                                    <?php if ($orderMerchant->shipper_id !== '') : ?>
                                                                        <div class="hx_noResi btn_trackResi" data-id='<?= $orderMerchant->id ?>' title="Lacak pengiriman ini.">Lacak Pengiriman</div>
                                                                <?php
                                                                    endif;
                                                                endif;
                                                                ?>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-6 col_infoResi in_border">
                                                            <div class="row">
                                                                <div class="col-md-6 col_ftmyorder">
                                                                    <div class="ft_myorder">
                                                                        <label>Biaya Pengiriman</label>
                                                                        <div class="tt_myorder"><b>Rp <?php echo number_format($orderMerchant->shipping_price); ?></b></div>
                                                                    </div>
                                                                    <div class="ft_myorder">
                                                                        <label>Biaya Asuransi Pengiriman</label>
                                                                        <div class="tt_myorder"><b>Rp <?php echo number_format($orderMerchant->insurance_price); ?></b></div>
                                                                    </div>
                                                                    <div class="ft_myorder">
                                                                        <label>Biaya Admin</label>
                                                                        <?php if ($adminFee !== 0) { ?>
                                                                            <div class="tt_myorder"><b>Rp <?php echo number_format($adminFee); ?></b></div>
                                                                        <?php } else { ?>
                                                                            <div class="tt_myorder"><b>Gratis</b></div>
                                                                        <?php } ?>
                                                                    </div>
                                                                    <?php
                                                                    if ($orderMerchant->discountVoucher !== 0) {
                                                                    ?>
                                                                        <div class="ft_myorder ">
                                                                            <label>Voucher <?= $orderMerchant->voucherName ?></label>
                                                                            <div class="tt_myorder color_delivered"><b>Rp <?php echo number_format($orderMerchant->discountVoucher); ?></b></div>
                                                                        </div>
                                                                    <?php
                                                                    }
                                                                    ?>
                                                                </div>
                                                                <div class="col-xs-12 col-md-12 col_mob_right_total mob_display">
                                                                    <div class="mob_bleft_total">
                                                                        <div class="ft_myorder order_space">
                                                                            <label>Status</label>
                                                                            <?php if (intval($orderMerchant->status) === 4) { ?>
                                                                                <div class="tt_myorder color_delivered"><b>Diterima</b></div>
                                                                            <?php } elseif (intval($orderMerchant->status) === 3) { ?>
                                                                                <div class="tt_myorder color_ondelivery"><b>Dikirim</b></div>
                                                                            <?php } elseif (intval($orderMerchant->status) === 2) { ?>
                                                                                <div class="tt_myorder color_process"><b>Diproses</b></div>
                                                                            <?php } elseif (intval($orderMerchant->status) === 1) { ?>
                                                                                <div class="tt_myorder color_process"><b>Menunggu Konfirmasi</b></div>
                                                                            <?php } else { ?>
                                                                                <div class="tt_myorder"><b>Menunggu Pembayaran</b></div>
                                                                            <?php } ?>
                                                                        </div>
                                                                    </div>
                                                                    <div class="mob_bright_total">
                                                                        <div class="ft_myorder_total">
                                                                            <label>Total Belanja</label><br />
                                                                            <label class="price">Rp <?php echo number_format($totalFee + $totalPrice); ?></label>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="col-md-6 col_ftmyorder">
                                                                    <a href="<?php echo home_url(); ?>/invoice/?id=<?php echo $orderMerchant->id; ?>">
                                                                        <div class="a_detail_myorder" data-id="<?php echo $orderMerchant->id; ?>" title="lihat detail order ini.">Lihat Detail</div>
                                                                    </a>
                                                                    <?php if (intval($orderMerchant->status) === 3) { ?>
                                                                        <div class="btn_confirm_delivered" data-poid=<?php echo $orderMerchant->id; ?>>Konfirmasi Sampai</div>
                                                                    <?php } ?>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                <?php
                                    }
                                }
                            } else {
                                ?>
                                <div class="row row_masterInformation">
                                    <div class="col-md-12 col_emptyInformation">
                                        <div class="mg_emptyInformation">
                                            <img src="<?php bloginfo('template_directory'); ?>/library/images/ico_noorder.svg">
                                        </div>
                                        <h2 class="tx_emptyInformation">Tidak ada transaksi saat ini.</h2>
                                    </div>
                                </div>
                            <?php
                            } ?>
                        </div>
                        <div id="tabs2" class="row row_box_myorder animated fadeIn">
                            <?php
                            if (count($orderMerchantsWithStatus1)) {
                                foreach ($orderMerchantsWithStatus1 as $orderMerchant) {
                                    $orderDate = date("d M Y", strtotime($orderMerchant->created_at));
                                    $adminFee = $order->xendit_fee / count($tempOrderMerchants[$orderMerchant->order_id]);
                                    $totalFee = $orderMerchant->shipping_price + $orderMerchant->insurance_price + $adminFee - $orderMerchant->discountVoucher;
                            ?>

                                    <div class="col-md-12 box_list_myorder">
                                        <div class="ht_cont_myorder"><?php echo $orderDate; ?> (<span><?php echo $orderMerchant->no_invoice; ?></span>)</div>
                                        <div class="row row_fill2_myorder">
                                            <div class="col-md-12 col_fill_myorder">
                                                <?php
                                                $totalPrice = 0;

                                                foreach ($tempOrderItems[$orderMerchant->id] as $key => $orderItem) {
                                                    $totalPrice += $orderItem->price * $orderItem->qty;
                                                }

                                                foreach ($tempOrderItems[$orderMerchant->id] as $key => $orderItem) {
                                                    $product = $wpdb->get_row("SELECT * FROM ldr_products WHERE id = " . $orderItem->product_id);
                                                    $productWeight = intval($product->weight ?? 0) * $orderItem->qty;
                                                    $mainImage = $wpdb->get_row("SELECT * FROM ldr_product_images WHERE name = 'imageUtama' AND deleted_at IS NULL AND products_id = " . $orderItem->product_id);

                                                    if ($mainImage !== null) {
                                                        $urlphoto = home_url('/') . $mainImage->path;
                                                    } else {
                                                        $urlphoto = get_template_directory_uri() . '/library/images/sorry.png';
                                                    }
                                                    $urlphoto = str_replace(home_url(), IMAGE_URL, $urlphoto);
                                                ?>

                                                    <div class="row row_orderlist_myorder" <?php echo $key === 0 ? '' : 'style="border-top: 1px solid rgba(0, 0, 0, .1);"'; ?>>
                                                        <div class="col-md-6 col_in_myorder ">
                                                            <div class="box_mg_myorder">
                                                                <img src="<?php echo $urlphoto; ?>" alt="<?php echo $product->title ?? 'Deleted Product'; ?>">
                                                            </div>
                                                            <div class="box_cont_pro_myorder">
                                                                <a href="<?= ($product === null ? '#' : '/product/' . $product->permalink . '-' . $product->id) ?>" title="Lihat produk lengkap...">
                                                                    <div class="ht_pro_myorder">
                                                                        <?php echo $product->title ?? 'Deleted Product'; ?>
                                                                    </div>
                                                                </a>
                                                                <div class="sm_myorder">
                                                                    <span class="price">Rp <?php echo number_format($orderItem->price); ?></span> <br />
                                                                    <br />
                                                                    <span class="qty"><?php echo $orderItem->qty; ?> barang (<?php echo $productWeight; ?> gram)</span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-3 col_in_myorder in_border">
                                                            <?php if ($key === 0) { ?>
                                                                <div class="ft_myorder order_space">
                                                                    <label>Status</label>
                                                                    <div class="tt_myorder color_process"><b>Menunggu Konfirmasi</b></div>
                                                                </div>
                                                                <div class="ft_myorder des_display">
                                                                    <label>Total Harga Produk</label>
                                                                    <div class="tt_myorder"><b class="price">Rp <?php echo number_format($orderItem->price * $orderItem->qty); ?></b></div>
                                                                </div>
                                                            <?php } else { ?>
                                                                <div class="ft_myorder ano_product des_display">
                                                                    <label>Total Harga Produk</label>
                                                                    <div class="tt_myorder"><b class="price">Rp <?php echo number_format($orderItem->price * $orderItem->qty); ?></b></div>
                                                                </div>
                                                            <?php } ?>
                                                        </div>
                                                        <?php if ($key === 0) { ?>
                                                            <div class="col-md-3 col_in_myorder in_last">
                                                                <div class="ft_myorder_total">
                                                                    <label>Total Belanja</label><br />
                                                                    <label class="price">Rp <?php echo number_format($totalFee + $totalPrice); ?></label>
                                                                </div>
                                                                <a href="<?= ($product === null ? '#' : '/product/' . $product->permalink . '-' . $product->id) ?>">
                                                                    <input type="button" class="btn_buyagain des_display" value="Beli Lagi">
                                                                </a>
                                                                <a href="<?= ($product === null ? '#' : '/product/' . $product->permalink . '-' . $product->id) ?>">
                                                                    <input type="button" class="btn_buyagain des_display" value="Beli Lagi">
                                                                </a>
                                                            </div>
                                                        <?php } else { ?>
                                                            <div class="col-md-3 col_in_myorder in_last">
                                                                <a href="<?= ($product === null ? '#' : '/product/' . $product->permalink . '-' . $product->id) ?>">
                                                                    <input type="button" class="btn_buyagain des_display add_mar" value="Beli Lagi">
                                                                </a>
                                                                <a href="<?= ($product === null ? '#' : '/product/' . $product->permalink . '-' . $product->id) ?>">
                                                                    <input type="button" class="btn_buyagain des_display" value="Beli Lagi">
                                                                </a>
                                                            </div>
                                                        <?php } ?>
                                                    </div>
                                                <?php
                                                } ?>
                                            </div>
                                            <div class="col-md-12 col_fill_myorder2">
                                                <div class="row bx_infoResi">
                                                    <div class="col-md-6 col_infoResi">
                                                        <div class="icon_freeshipping">
                                                            <img class="mg_car" src="<?php bloginfo('template_directory'); ?>/library/images/normal_delivery.svg">
                                                        </div>
                                                        <div class="bx_info_noResi">
                                                            <div class="hx_noResi">Jasa : <b><?php echo $orderMerchant->courier_name; ?> (<?php echo $orderMerchant->courier_rate_name; ?>)</b></div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6 col_infoResi in_border">
                                                        <div class="row">
                                                            <div class="col-md-6 col_ftmyorder">
                                                                <div class="ft_myorder ">
                                                                    <label>Biaya Pengiriman</label>
                                                                    <div class="tt_myorder"><b>Rp <?php echo number_format($orderMerchant->shipping_price); ?></b></div>
                                                                </div>
                                                                <div class="ft_myorder">
                                                                    <label>Biaya Asuransi Pengiriman</label>
                                                                    <div class="tt_myorder"><b>Rp <?php echo number_format($orderMerchant->insurance_price); ?></b></div>
                                                                </div>
                                                                <div class="ft_myorder">
                                                                    <label>Biaya Admin</label>
                                                                    <?php if ($adminFee !== 0) { ?>
                                                                        <div class="tt_myorder"><b>Rp <?php echo number_format($adminFee); ?></b></div>
                                                                    <?php } else { ?>
                                                                        <div class="tt_myorder"><b>Gratis</b></div>
                                                                    <?php } ?>
                                                                </div>
                                                                <?php
                                                                if ($orderMerchant->discountVoucher !== 0) {
                                                                ?>
                                                                    <div class="ft_myorder ">
                                                                        <label>Voucher <?= $orderMerchant->voucherName ?></label>
                                                                        <div class="tt_myorder color_delivered"><b>Rp <?php echo number_format($orderMerchant->discountVoucher); ?></b></div>
                                                                    </div>
                                                                <?php
                                                                }
                                                                ?>
                                                            </div>
                                                            <div class="col-xs-12 col-md-12 col_mob_right_total mob_display">
                                                                <div class="mob_bleft_total">
                                                                    <div class="ft_myorder order_space">
                                                                        <label>Status</label>
                                                                        <div class="tt_myorder color_process"><b>Menunggu Konfirmasi</b></div>
                                                                    </div>
                                                                </div>
                                                                <div class="mob_bright_total">
                                                                    <div class="ft_myorder_total">
                                                                        <label>Total Belanja</label><br />
                                                                        <label class="price">Rp <?php echo number_format($totalFee + $totalPrice); ?></label>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-6 col_ftmyorder">
                                                                <a href="<?php echo home_url(); ?>/invoice/?id=<?php echo $orderMerchant->id; ?>">
                                                                    <div class="a_detail_myorder" data-id="<?php echo $orderMerchant->id; ?>" title="lihat detail order ini.">Lihat Detail</div>
                                                                </a>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                <?php
                                }
                            } else {
                                ?>
                                <div class="row row_masterInformation">
                                    <div class="col-md-12 col_emptyInformation">
                                        <div class="mg_emptyInformation">
                                            <img src="<?php bloginfo('template_directory'); ?>/library/images/ico_noorder.svg">
                                        </div>
                                        <h2 class="tx_emptyInformation">Tidak ada transaksi saat ini.</h2>
                                    </div>
                                </div>
                            <?php
                            }
                            ?>
                        </div>

                        <div id="tabs3" class="row row_box_myorder animated fadeIn">
                            <?php
                            if (count($orderMerchantsWithStatus2)) {
                                foreach ($orderMerchantsWithStatus2 as $orderMerchant) {
                                    $orderDate = date("d M Y", strtotime($orderMerchant->created_at));
                                    $adminFee = $order->xendit_fee / count($tempOrderMerchants[$orderMerchant->order_id]);
                                    $totalFee = $orderMerchant->shipping_price + $orderMerchant->insurance_price + $adminFee - $orderMerchant->discountVoucher;
                            ?>

                                    <div class="col-md-12 box_list_myorder">
                                        <div class="ht_cont_myorder"><?php echo $orderDate; ?> (<span><?php echo $orderMerchant->no_invoice; ?></span>)</div>
                                        <div class="row row_fill2_myorder">
                                            <div class="col-md-12 col_fill_myorder">
                                                <?php
                                                $totalPrice = 0;

                                                foreach ($tempOrderItems[$orderMerchant->id] as $key => $orderItem) {
                                                    $totalPrice += $orderItem->price * $orderItem->qty;
                                                }

                                                foreach ($tempOrderItems[$orderMerchant->id] as $key => $orderItem) {
                                                    $product = $wpdb->get_row("SELECT * FROM ldr_products WHERE id = " . $orderItem->product_id);
                                                    $productWeight = intval($product->weight ?? 0) * $orderItem->qty;
                                                    $mainImage = $wpdb->get_row("SELECT * FROM ldr_product_images WHERE name = 'imageUtama' AND deleted_at IS NULL AND products_id = " . $orderItem->product_id);

                                                    if ($mainImage !== null) {
                                                        $urlphoto = home_url('/') . $mainImage->path;
                                                    } else {
                                                        $urlphoto = get_template_directory_uri() . '/library/images/sorry.png';
                                                    }
                                                    $urlphoto = str_replace(home_url(), IMAGE_URL, $urlphoto);
                                                ?>

                                                    <div class="row row_orderlist_myorder" <?php echo $key === 0 ? '' : 'style="border-top: 1px solid rgba(0, 0, 0, .1);"'; ?>>
                                                        <div class="col-md-6 col_in_myorder ">
                                                            <div class="box_mg_myorder">
                                                                <img src="<?php echo $urlphoto; ?>" alt="<?php echo $product->title ?? 'Deleted Product'; ?>">
                                                            </div>
                                                            <div class="box_cont_pro_myorder">
                                                                <a href="<?= ($product === null ? '#' : '/product/' . $product->permalink . '-' . $product->id) ?>" title="Lihat produk lengkap...">
                                                                    <div class="ht_pro_myorder">
                                                                        <?php echo $product->title ?? 'Deleted Product'; ?>
                                                                    </div>
                                                                </a>
                                                                <div class="sm_myorder">
                                                                    <span class="price">Rp <?php echo number_format($orderItem->price); ?></span> <br />
                                                                    <br />
                                                                    <span class="qty"><?php echo $orderItem->qty; ?> barang (<?php echo $productWeight; ?> gram)</span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-3 col_in_myorder in_border">
                                                            <?php if ($key === 0) { ?>
                                                                <div class="ft_myorder order_space">
                                                                    <label>Status</label>
                                                                    <div class="tt_myorder color_process"><b>Diproses</b></div>
                                                                </div>
                                                                <div class="ft_myorder des_display">
                                                                    <label>Total Harga Produk</label>
                                                                    <div class="tt_myorder"><b class="price">Rp <?php echo number_format($orderItem->price * $orderItem->qty); ?></b></div>
                                                                </div>
                                                            <?php } else { ?>
                                                                <div class="ft_myorder ano_product des_display">
                                                                    <label>Total Harga Produk</label>
                                                                    <div class="tt_myorder"><b class="price">Rp <?php echo number_format($orderItem->price * $orderItem->qty); ?></b></div>
                                                                </div>
                                                            <?php } ?>
                                                        </div>
                                                        <?php if ($key === 0) { ?>
                                                            <div class="col-md-3 col_in_myorder in_last">
                                                                <div class="ft_myorder_total">
                                                                    <label>Total Belanja</label><br />
                                                                    <label class="price">Rp <?php echo number_format($totalFee + $totalPrice); ?></label>
                                                                </div>
                                                                <a href="<?= ($product === null ? '#' : '/product/' . $product->permalink . '-' . $product->id) ?>">
                                                                    <input type="button" class="btn_buyagain des_display" value="Beli Lagi">
                                                                </a>
                                                            </div>
                                                        <?php } else { ?>
                                                            <div class="col-md-3 col_in_myorder in_last">
                                                                <a href="<?= ($product === null ? '#' : '/product/' . $product->permalink . '-' . $product->id) ?>">
                                                                    <input type="button" class="btn_buyagain des_display add_mar" value="Beli Lagi">
                                                                </a>
                                                            </div>
                                                        <?php } ?>
                                                    </div>
                                                <?php
                                                } ?>
                                            </div>
                                            <div class="col-md-12 col_fill_myorder2">
                                                <div class="row bx_infoResi">
                                                    <div class="col-md-6 col_infoResi">
                                                        <div class="icon_freeshipping">
                                                            <img class="mg_car" src="<?php bloginfo('template_directory'); ?>/library/images/normal_delivery.svg">
                                                        </div>
                                                        <div class="bx_info_noResi">
                                                            <div class="hx_noResi">Jasa : <b><?php echo $orderMerchant->courier_name; ?> (<?php echo $orderMerchant->courier_rate_name; ?>)</b></div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6 col_infoResi in_border">
                                                        <div class="row">
                                                            <div class="col-md-6 col_ftmyorder">
                                                                <div class="ft_myorder ">
                                                                    <label>Biaya Pengiriman</label>
                                                                    <div class="tt_myorder"><b>Rp <?php echo number_format($orderMerchant->shipping_price); ?></b></div>
                                                                </div>
                                                                <div class="ft_myorder">
                                                                    <label>Biaya Asuransi Pengiriman</label>
                                                                    <div class="tt_myorder"><b>Rp <?php echo number_format($orderMerchant->insurance_price); ?></b></div>
                                                                </div>
                                                                <div class="ft_myorder">
                                                                    <label>Biaya Admin</label>
                                                                    <?php if ($adminFee !== 0) { ?>
                                                                        <div class="tt_myorder"><b>Rp <?php echo number_format($adminFee); ?></b></div>
                                                                    <?php } else { ?>
                                                                        <div class="tt_myorder"><b>Gratis</b></div>
                                                                    <?php } ?>
                                                                </div>
                                                                <?php
                                                                if ($orderMerchant->discountVoucher !== 0) {
                                                                ?>
                                                                    <div class="ft_myorder ">
                                                                        <label>Voucher <?= $orderMerchant->voucherName ?></label>
                                                                        <div class="tt_myorder color_delivered"><b>Rp <?php echo number_format($orderMerchant->discountVoucher); ?></b></div>
                                                                    </div>
                                                                <?php
                                                                }
                                                                ?>
                                                            </div>
                                                            <div class="col-xs-12 col-md-12 col_mob_right_total mob_display">
                                                                <div class="mob_bleft_total">
                                                                    <div class="ft_myorder order_space">
                                                                        <label>Status</label>
                                                                        <div class="tt_myorder color_process"><b>Diproses</b></div>
                                                                    </div>
                                                                </div>
                                                                <div class="mob_bright_total">
                                                                    <div class="ft_myorder_total">
                                                                        <label>Total Belanja</label><br />
                                                                        <label class="price">Rp <?php echo number_format($totalFee + $totalPrice); ?></label>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-6 col_ftmyorder">
                                                                <a href="<?php echo home_url(); ?>/invoice/?id=<?php echo $orderMerchant->id; ?>">
                                                                    <div class="a_detail_myorder" data-id="<?php echo $orderMerchant->id; ?>" title="lihat detail order ini.">Lihat Detail</div>
                                                                </a>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                <?php
                                }
                            } else {
                                ?>
                                <div class="row row_masterInformation">
                                    <div class="col-md-12 col_emptyInformation">
                                        <div class="mg_emptyInformation">
                                            <img src="<?php bloginfo('template_directory'); ?>/library/images/ico_noorder.svg">
                                        </div>
                                        <h2 class="tx_emptyInformation">Tidak ada transaksi saat ini.</h2>
                                    </div>
                                </div>
                            <?php
                            }
                            ?>
                        </div>

                        <div id="tabs4" class="row row_box_myorder animated fadeIn">
                            <?php
                            if (count($orderMerchantsWithStatus3)) {
                                foreach ($orderMerchantsWithStatus3 as $orderMerchant) {
                                    $orderDate = date("d M Y", strtotime($orderMerchant->created_at));
                                    $adminFee = $order->xendit_fee / count($tempOrderMerchants[$orderMerchant->order_id]);
                                    $totalFee = $orderMerchant->shipping_price + $orderMerchant->insurance_price + $adminFee - $orderMerchant->discountVoucher;
                            ?>

                                    <div class="col-md-12 box_list_myorder">
                                        <div class="ht_cont_myorder"><?php echo $orderDate; ?> (<span><?php echo $orderMerchant->no_invoice; ?></span>)</div>
                                        <div class="row row_fill2_myorder">
                                            <div class="col-md-12 col_fill_myorder">
                                                <?php
                                                $totalPrice = 0;

                                                foreach ($tempOrderItems[$orderMerchant->id] as $key => $orderItem) {
                                                    $totalPrice += $orderItem->price * $orderItem->qty;
                                                }

                                                foreach ($tempOrderItems[$orderMerchant->id] as $key => $orderItem) {
                                                    $product = $wpdb->get_row("SELECT * FROM ldr_products WHERE id = " . $orderItem->product_id);
                                                    $productWeight = intval($product->weight ?? 0) * $orderItem->qty;
                                                    $mainImage = $wpdb->get_row("SELECT * FROM ldr_product_images WHERE name = 'imageUtama' AND deleted_at IS NULL AND products_id = " . $orderItem->product_id);

                                                    if ($mainImage !== null) {
                                                        $urlphoto = home_url('/') . $mainImage->path;
                                                    } else {
                                                        $urlphoto = get_template_directory_uri() . '/library/images/sorry.png';
                                                    }
                                                    $urlphoto = str_replace(home_url(), IMAGE_URL, $urlphoto);
                                                ?>

                                                    <div class="row row_orderlist_myorder" <?php echo $key === 0 ? '' : 'style="border-top: 1px solid rgba(0, 0, 0, .1);"'; ?>>
                                                        <div class="col-md-6 col_in_myorder ">
                                                            <div class="box_mg_myorder">
                                                                <img src="<?php echo $urlphoto; ?>" alt="<?php echo $product->title ?? 'Deleted Product'; ?>">
                                                            </div>
                                                            <div class="box_cont_pro_myorder">
                                                                <a href="<?= ($product === null ? '#' : '/product/' . $product->permalink . '-' . $product->id) ?>" title="Lihat produk lengkap...">
                                                                    <div class="ht_pro_myorder">
                                                                        <?php echo $product->title ?? 'Deleted Product'; ?>
                                                                    </div>
                                                                </a>
                                                                <div class="sm_myorder">
                                                                    <span class="price">Rp <?php echo number_format($orderItem->price); ?></span> <br />
                                                                    <br />
                                                                    <span class="qty"><?php echo $orderItem->qty; ?> barang (<?php echo $productWeight; ?> gram)</span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-3 col_in_myorder in_border">
                                                            <?php if ($key === 0) { ?>
                                                                <div class="ft_myorder order_space">
                                                                    <label>Status</label>
                                                                    <div class="tt_myorder color_ondelivery"><b>Dikirim</b></div>
                                                                </div>
                                                                <div class="ft_myorder des_display">
                                                                    <label>Total Harga Produk</label>
                                                                    <div class="tt_myorder"><b class="price">Rp <?php echo number_format($orderItem->price * $orderItem->qty); ?></b></div>
                                                                </div>
                                                            <?php } else { ?>
                                                                <div class="ft_myorder ano_product des_display">
                                                                    <label>Total Harga Produk</label>
                                                                    <div class="tt_myorder"><b class="price">Rp <?php echo number_format($orderItem->price * $orderItem->qty); ?></b></div>
                                                                </div>
                                                            <?php } ?>
                                                        </div>
                                                        <?php if ($key === 0) { ?>
                                                            <div class="col-md-3 col_in_myorder in_last">
                                                                <div class="ft_myorder_total">
                                                                    <label>Total Belanja</label><br />
                                                                    <label class="price">Rp <?php echo number_format($totalFee + $totalPrice); ?></label>
                                                                </div>
                                                                <a href="<?= ($product === null ? '#' : '/product/' . $product->permalink . '-' . $product->id) ?>">
                                                                    <input type="button" class="btn_buyagain des_display" value="Beli Lagi">
                                                                </a>
                                                            </div>
                                                        <?php } else { ?>
                                                            <div class="col-md-3 col_in_myorder in_last">
                                                                <a href="<?= ($product === null ? '#' : '/product/' . $product->permalink . '-' . $product->id) ?>">
                                                                    <input type="button" class="btn_buyagain des_display add_mar" value="Beli Lagi">
                                                                </a>
                                                            </div>
                                                        <?php } ?>
                                                    </div>
                                                <?php
                                                } ?>
                                            </div>
                                            <div class="col-md-12 col_fill_myorder2">
                                                <div class="row bx_infoResi">
                                                    <div class="col-md-6 col_infoResi">
                                                        <div class="icon_freeshipping">
                                                            <img class="mg_car" src="<?php bloginfo('template_directory'); ?>/library/images/normal_delivery.svg">
                                                        </div>
                                                        <div class="bx_info_noResi">
                                                            <div class="hx_noResi">Jasa : <b><?php echo $orderMerchant->courier_name; ?> (<?php echo $orderMerchant->courier_rate_name; ?>)</b></div>
                                                            <div class="hx_noResi">Resi : <b><?= $orderMerchant->awbNumber ?></b> </div>
                                                            <?php if ($orderMerchant->shipper_id !== '') : ?>
                                                                <div class="hx_noResi btn_trackResi" data-id='<?= $orderMerchant->id ?>' title="Lacak pengiriman ini.">Lacak Pengiriman</div>
                                                            <?php endif; ?>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6 col_infoResi in_border">
                                                        <div class="row">
                                                            <div class="col-md-6 col_ftmyorder">
                                                                <div class="ft_myorder ">
                                                                    <label>Biaya Pengiriman</label>
                                                                    <div class="tt_myorder"><b>Rp <?php echo number_format($orderMerchant->shipping_price); ?></b></div>
                                                                </div>
                                                                <div class="ft_myorder">
                                                                    <label>Biaya Asuransi Pengiriman</label>
                                                                    <div class="tt_myorder"><b>Rp <?php echo number_format($orderMerchant->insurance_price); ?></b></div>
                                                                </div>
                                                                <div class="ft_myorder">
                                                                    <label>Biaya Admin</label>
                                                                    <?php if ($adminFee !== 0) { ?>
                                                                        <div class="tt_myorder"><b>Rp <?php echo number_format($adminFee); ?></b></div>
                                                                    <?php } else { ?>
                                                                        <div class="tt_myorder"><b>Gratis</b></div>
                                                                    <?php } ?>
                                                                </div>
                                                                <?php
                                                                if ($orderMerchant->discountVoucher !== 0) {
                                                                ?>
                                                                    <div class="ft_myorder ">
                                                                        <label>Voucher <?= $orderMerchant->voucherName ?></label>
                                                                        <div class="tt_myorder color_delivered"><b>Rp <?php echo number_format($orderMerchant->discountVoucher); ?></b></div>
                                                                    </div>
                                                                <?php
                                                                }
                                                                ?>
                                                            </div>
                                                            <div class="col-xs-12 col-md-12 col_mob_right_total mob_display">
                                                                <div class="mob_bleft_total">
                                                                    <div class="ft_myorder order_space">
                                                                        <label>Status</label>
                                                                        <div class="tt_myorder color_ondelivery"><b>Dikirim</b></div>
                                                                    </div>
                                                                </div>
                                                                <div class="mob_bright_total">
                                                                    <div class="ft_myorder_total">
                                                                        <label>Total Belanja</label><br />
                                                                        <label class="price">Rp <?php echo number_format($totalFee + $totalPrice); ?></label>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-6 col_ftmyorder">
                                                                <a href="<?php echo home_url(); ?>/invoice/?id=<?php echo $orderMerchant->id; ?>">
                                                                    <div class="a_detail_myorder" data-id="<?php echo $orderMerchant->id; ?>" title="lihat detail order ini.">Lihat Detail</div>
                                                                </a>
                                                                <div class="btn_confirm_delivered" data-poid=<?php echo $orderMerchant->id; ?>>Konfirmasi Sampai</div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                <?php
                                }
                            } else {
                                ?>
                                <div class="row row_masterInformation">
                                    <div class="col-md-12 col_emptyInformation">
                                        <div class="mg_emptyInformation">
                                            <img src="<?php bloginfo('template_directory'); ?>/library/images/ico_noorder.svg">
                                        </div>
                                        <h2 class="tx_emptyInformation">Tidak ada transaksi saat ini.</h2>
                                    </div>
                                </div>
                            <?php
                            }
                            ?>
                        </div>

                        <div id="tabs5" class="row row_box_myorder animated fadeIn">
                            <?php
                            if (count($orderMerchantsWithStatus4)) {
                                foreach ($orderMerchantsWithStatus4 as $orderMerchant) {
                                    $orderDate = date("d M Y", strtotime($orderMerchant->created_at));
                                    $adminFee = $order->xendit_fee / count($tempOrderMerchants[$orderMerchant->order_id]);
                                    $totalFee = $orderMerchant->shipping_price + $orderMerchant->insurance_price + $adminFee - $orderMerchant->discountVoucher;
                            ?>

                                    <div class="col-md-12 box_list_myorder">
                                        <div class="ht_cont_myorder"><?php echo $orderDate; ?> (<span><?php echo $orderMerchant->no_invoice; ?></span>)</div>
                                        <div class="row row_fill2_myorder">
                                            <div class="col-md-12 col_fill_myorder">
                                                <?php
                                                $totalPrice = 0;

                                                foreach ($tempOrderItems[$orderMerchant->id] as $key => $orderItem) {
                                                    $totalPrice += $orderItem->price * $orderItem->qty;
                                                }

                                                foreach ($tempOrderItems[$orderMerchant->id] as $key => $orderItem) {
                                                    $product = $wpdb->get_row("SELECT * FROM ldr_products WHERE id = " . $orderItem->product_id);
                                                    $productWeight = intval($product->weight ?? 0) * $orderItem->qty;
                                                    $mainImage = $wpdb->get_row("SELECT * FROM ldr_product_images WHERE name = 'imageUtama' AND deleted_at IS NULL AND products_id = " . $orderItem->product_id);

                                                    if ($mainImage !== null) {
                                                        $urlphoto = home_url('/') . $mainImage->path;
                                                    } else {
                                                        $urlphoto = get_template_directory_uri() . '/library/images/sorry.png';
                                                    }
                                                    $urlphoto = str_replace(home_url(), IMAGE_URL, $urlphoto);
                                                ?>

                                                    <div class="row row_orderlist_myorder" <?php echo $key === 0 ? '' : 'style="border-top: 1px solid rgba(0, 0, 0, .1);"'; ?>>
                                                        <div class="col-md-6 col_in_myorder ">
                                                            <div class="box_mg_myorder">
                                                                <img src="<?php echo $urlphoto; ?>" alt="<?php echo $product->title ?? 'Deleted Product'; ?>">
                                                            </div>
                                                            <div class="box_cont_pro_myorder">
                                                                <a href="<?= ($product === null ? '#' : '/product/' . $product->permalink . '-' . $product->id) ?>" title="Lihat produk lengkap...">
                                                                    <div class="ht_pro_myorder">
                                                                        <?php echo $product->title ?? 'Deleted Product'; ?>
                                                                    </div>
                                                                </a>
                                                                <div class="sm_myorder">
                                                                    <span class="price">Rp <?php echo number_format($orderItem->price); ?></span> <br />
                                                                    <br />
                                                                    <span class="qty"><?php echo $orderItem->qty; ?> barang (<?php echo $productWeight; ?> gram)</span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-3 col_in_myorder in_border">
                                                            <?php if ($key === 0) { ?>
                                                                <div class="ft_myorder order_space">
                                                                    <label>Status</label>
                                                                    <div class="tt_myorder color_delivered"><b>Diterima</b></div>
                                                                </div>
                                                                <div class="ft_myorder des_display">
                                                                    <label>Total Harga Produk</label>
                                                                    <div class="tt_myorder"><b class="price">Rp <?php echo number_format($orderItem->price * $orderItem->qty); ?></b></div>
                                                                </div>
                                                            <?php } else { ?>
                                                                <div class="ft_myorder ano_product des_display">
                                                                    <label>Total Harga Produk</label>
                                                                    <div class="tt_myorder"><b class="price">Rp <?php echo number_format($orderItem->price * $orderItem->qty); ?></b></div>
                                                                </div>
                                                            <?php } ?>
                                                        </div>
                                                        <?php if ($key === 0) { ?>
                                                            <div class="col-md-3 col_in_myorder in_last">
                                                                <div class="ft_myorder_total">
                                                                    <label>Total Belanja</label><br />
                                                                    <label class="price">Rp <?php echo number_format($totalFee + $totalPrice); ?></label>
                                                                </div>
                                                                <a href="<?= ($product === null ? '#' : '/product/' . $product->permalink . '-' . $product->id) ?>">
                                                                    <input type="button" class="btn_buyagain des_display" value="Beli Lagi">
                                                                </a>
                                                                <?php if (intval($orderMerchant->status) === 4):?>
                                                                    <?php
                                                                    global $wpdb;
                                                                    $cekUlasan = $wpdb->get_row("select * from ldr_product_reviews
                                                                                                    where product_id = $product->id and order_merchant_id = $orderMerchant->id
                                                                                                    and user_id = $userId")
                                                                    ?>
                                                                    <?php if (!$cekUlasan):?>
<!--                                                                        <a href="--><?//= ($product === null ? '#' : '/my-order/tambah-ulasan?order_merchant_id=' . $orderMerchant->id . '&product_id=' . $product->id) ?><!--">-->
<!--                                                                            <input type="button" class="btn_buyagain des_display btn-ulasan" value="Beri Ulasan">-->
<!--                                                                        </a>-->
                                                                    <?php endif;?>
                                                                <?php endif;?>
                                                            </div>
                                                        <?php } else { ?>
                                                            <div class="col-md-3 col_in_myorder in_last">
                                                                <a href="<?= ($product === null ? '#' : '/product/' . $product->permalink . '-' . $product->id) ?>">
                                                                    <input type="button" class="btn_buyagain des_display add_mar" value="Beli Lagi">
                                                                </a>
                                                                <?php if (intval($orderMerchant->status) === 4):?>
                                                                    <?php
                                                                    global $wpdb;
                                                                    $cekUlasan = $wpdb->get_row("select * from ldr_product_reviews
                                                                                                    where product_id = $product->id and order_merchant_id = $orderMerchant->id
                                                                                                    and user_id = $userId")
                                                                    ?>
                                                                    <?php if (!$cekUlasan):?>
<!--                                                                        <a href="--><?//= ($product === null ? '#' : '/my-order/tambah-ulasan?order_merchant_id=' . $orderMerchant->id . '&product_id=' . $product->id) ?><!--">-->
<!--                                                                            <input type="button" class="btn_buyagain des_display btn-ulasan" value="Beri Ulasan">-->
<!--                                                                        </a>-->
                                                                    <?php endif;?>
                                                                <?php endif;?>
                                                            </div>
                                                        <?php } ?>
                                                    </div>
                                                <?php
                                                } ?>
                                            </div>
                                            <div class="col-md-12 col_fill_myorder2">
                                                <div class="row bx_infoResi">
                                                    <div class="col-md-6 col_infoResi">
                                                        <div class="icon_freeshipping">
                                                            <img class="mg_car" src="<?php bloginfo('template_directory'); ?>/library/images/normal_delivery.svg">
                                                        </div>
                                                        <div class="bx_info_noResi">
                                                            <div class="hx_noResi">Jasa : <b><?php echo $orderMerchant->courier_name; ?> (<?php echo $orderMerchant->courier_rate_name; ?>)</b></div>
                                                            <div class="hx_noResi">Resi : <b><?= $orderMerchant->awbNumber; ?></b> </div>
                                                            <?php if ($orderMerchant->shipper_id !== '') : ?>
                                                                <div class="hx_noResi btn_trackResi" data-id='<?= $orderMerchant->id ?>' title="Lacak pengiriman ini.">Lacak Pengiriman</div>
                                                            <?php endif; ?>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6 col_infoResi in_border">
                                                        <div class="row">
                                                            <div class="col-md-6 col_ftmyorder">
                                                                <div class="ft_myorder ">
                                                                    <label>Biaya Pengiriman</label>
                                                                    <div class="tt_myorder"><b>Rp <?php echo number_format($orderMerchant->shipping_price); ?></b></div>
                                                                </div>
                                                                <div class="ft_myorder">
                                                                    <label>Biaya Asuransi Pengiriman</label>
                                                                    <div class="tt_myorder"><b>Rp <?php echo number_format($orderMerchant->insurance_price); ?></b></div>
                                                                </div>
                                                                <div class="ft_myorder">
                                                                    <label>Biaya Admin</label>
                                                                    <?php if ($adminFee !== 0) { ?>
                                                                        <div class="tt_myorder"><b>Rp <?php echo number_format($adminFee); ?></b></div>
                                                                    <?php } else { ?>
                                                                        <div class="tt_myorder"><b>Gratis</b></div>
                                                                    <?php } ?>
                                                                </div>
                                                                <?php
                                                                if ($orderMerchant->discountVoucher !== 0) {
                                                                ?>
                                                                    <div class="ft_myorder ">
                                                                        <label>Voucher <?= $orderMerchant->voucherName ?></label>
                                                                        <div class="tt_myorder color_delivered"><b>Rp <?php echo number_format($orderMerchant->discountVoucher); ?></b></div>
                                                                    </div>
                                                                <?php
                                                                }
                                                                ?>
                                                            </div>
                                                            <div class="col-xs-12 col-md-12 col_mob_right_total mob_display">
                                                                <div class="mob_bleft_total">
                                                                    <div class="ft_myorder order_space">
                                                                        <label>Status</label>
                                                                        <div class="tt_myorder color_delivered"><b>Diterima</b></div>
                                                                    </div>
                                                                </div>
                                                                <div class="mob_bright_total">
                                                                    <div class="ft_myorder_total">
                                                                        <label>Total Belanja</label><br />
                                                                        <label class="price">Rp <?php echo number_format($totalFee + $totalPrice); ?></label>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-6 col_ftmyorder">
                                                                <a href="<?php echo home_url(); ?>/invoice/?id=<?php echo $orderMerchant->id; ?>">
                                                                    <div class="a_detail_myorder" data-id="<?php echo $orderMerchant->id; ?>" title="lihat detail order ini.">Lihat Detail</div>
                                                                </a>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                <?php
                                }
                            } else {
                                ?>
                                <div class="row row_masterInformation">
                                    <div class="col-md-12 col_emptyInformation">
                                        <div class="mg_emptyInformation">
                                            <img src="<?php bloginfo('template_directory'); ?>/library/images/ico_noorder.svg">
                                        </div>
                                        <h2 class="tx_emptyInformation">Tidak ada transaksi saat ini.</h2>
                                    </div>
                                </div>
                            <?php
                            }
                            ?>
                        </div>
                    </div>
                </div>
            </div>


            <div class="row row_confirmBox">
                <div class="col-md-4 close_allBox"></div>
                <div class="col-md-4 col_confirmBox animated fadeIn">

                    <div class="close_confirmBox" title="Tutup"><span class="glyphicon glyphicon-remove"></span></div>

                    <div class="inbox_confirmBox">
                        <div class="info_confirmBox">
                            <b>Apakah pesanan ini sudah diterima dengan baik?</b> <br />
                            <span>Pastikan produk yang diterima sudah sesuai dengan yang kamu inginkan ya.</span>
                        </div>
                    </div>
                    <div class="bx_btn_confirmBox">
                        <input type="button" class="btn_confirmBox no" data-act="no" value="Belum Sampai" title="Tutup">
                    </div>
                    <div class="bx_btn_confirmBox">
                        <input type="button" class="btn_confirmBox yes" data-act="yes" value="Sudah Sampai" title="Konfirmasi Sudah Terima">
                    </div>
                </div>

                <div class="col-md-4 close_allBox"></div>
            </div>

            <input type="hidden" name="po_id" value="">
            <div class="row row_openDetailOrder">
                <div class="col-md-4 close_addcart_af"></div>
                <div class="col-md-4 col_addalamat_af animated zoomIn">
                    <div class="box_addalamat_af">
                        <div id="" class="hide_popup" title="Tutup popup ini"><span><i class="far fa-minus-square"></i></span></div>
                        <div id="view_myorderDetail">
                            <div id="tt_title" class="ht_addcart_af red no_invoice">&nbsp;</div>
                            <div class="ht_detailWA">Alamat Tujuan :</div>
                            <div class="cont_detailWA destination">&nbsp;</div>
                            <div class="bx_dis_dropshiper">
                                <div class="ht_detailWA">Penjual / Pengirim:</div>
                                <div class="cont_detailWA origin">&nbsp;</div>
                            </div>
                            <div class="row bx_infoResi_in">
                                <div class="col-md-2">
                                    <div class="icon_freeshipping">
                                        <img class="mg_car" src="<?= get_template_directory_uri() . '/library/images/normal_delivery.svg' ?>">
                                    </div>
                                </div>
                                <div class="col-md-10">
                                    <div class="hx_noResi courier_name">&nbsp;</div>
                                    <div class="hx_noResi awb_number">&nbsp;</div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12 tracking">&nbsp;</div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-4 close_addcart_af"></div>
            </div>
        <?php
        } else { ?>
            <script>
                // 'Getting' data-attributes using getAttribute
                var plant = document.getElementById('body');
                console.log(plant);
                var hurl = plant.getAttribute('data-hurl');
                location.replace(hurl + '/login/');
            </script>
        <?php } ?>

    <?php endwhile; ?>
<?php else : ?>
    <?php get_template_part('content', '404pages'); ?>
<?php endif; ?>
<?php get_footer(); ?>