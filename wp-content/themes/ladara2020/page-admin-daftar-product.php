<?php
/*
Template Name: Admin Daftar Produk
*/
?>
<?php get_header('admin'); 
 if (have_posts()) : while (have_posts()) : the_post(); ?>

<style type="text/css">
.box_filterReport{
    margin-bottom: 30px;
}
.sel_filterReport{
    width: 100%;
    height: 30px;
    line-height: 35px;
    font-size: 12px;
    color: #000;
    font-weight: 500;
    border-radius: 3px;
    border: 1px solid rgba(0,0,0,.2);
    padding-left: 10px;
    padding-right: 10px;
}
.txt_filterReport{
    width: 100%;
    height: 30px;
    line-height: 35px;
    font-size: 12px;
    color: #000;
    font-weight: 500;
    border-radius: 3px;
    border: 1px solid rgba(0,0,0,.2);
    padding-left: 10px;
    padding-right: 10px;
}
.sub_filterReport{
    background: #2a96a5;
    font-size: 12px;
    font-weight: 600;
    text-transform: uppercase;
    letter-spacing: .3px;
    color: #fff;
    width: auto;
    line-height: 30px;
    border: 0;
    border-radius: 3px;
    padding-left: 15px;
    padding-right: 15px;
    margin-top: 20px;
}
</style>

<?php 
date_default_timezone_set("Asia/Jakarta");
$nowdate = date('Y-m-d');

$current_user = wp_get_current_user();
$u_id = $current_user->ID;
$u_roles = $current_user->roles;
$admin = 0;

if(isset($u_roles) AND !empty($u_roles)){
  foreach ($u_roles as $key => $value) {
      if(strtolower($value) == 'administrator'){
          $admin = 1;
      }
  }
}

if((isset($u_id) OR $u_id != 0) AND $admin == 1){

  if(isset($_GET['from_date'])){
    $from_date = $_GET['from_date'];
  }else{
    $from_date = $nowdate;
  }

  if(isset($_GET['to_date'])){
    $to_date = $_GET['to_date'];
  }else{
    $to_date = $nowdate;
  }

  if(isset($_GET['cat'])){
    $this_cat = $_GET['cat'];
  }else{
    $this_cat = '';
  }

  if(isset($_GET['merchant'])){
    $this_merchant = $_GET['merchant'];
  }else{
    $this_merchant = '';
  }

?>

  <!-- Begin Page Content -->
  <div class="container-fluid">

    <!-- Page Heading -->
    <h1 class="h3 mb-2 text-gray-800">Daftar Produk</h1>

    <!-- DataTales Example -->
    <div class="card shadow mb-4">
      <div class="card-header py-3">
        <h6 class="m-0 font-weight-bold text-primary"></h6>
      </div>
      <div class="card-body">

        <form class="form_filterReport" onsubmit="form_productReport(event);" method="post" id="form_filterReport">
            <div class="row box_filterReport">

              <div class="col-md-2 col_filterReport">
                  <div class="label_filterReport">Pilih Kategori :</div>
                  <select name="sel_category" class="jsselect sel_filterReport">
                      <option value="">Semua Kategori</option>
                      <?php 
                          global $wpdb;
                          $this_catname = '';
                          $query = "SELECT *
                                    FROM ldr_categories
                                    ORDER BY name ASC";

                          $res_query_cat = $wpdb->get_results($query, OBJECT);
                          $res_count_cat = count($res_query_cat);
                          if ($res_count_cat > 0){
                            foreach ($res_query_cat as $key_cat => $value_cat) {
                              $cat_id = $value_cat->id;
                              $cat_name = $value_cat->name;
                              $cat_slug = $value_cat->slug;

                              if($cat_id == $this_cat){
                              ?>
                                  <option value="<?php echo $cat_id; ?>" selected="selected"><?php echo ucfirst($cat_name); ?></option>
                              <?php
                              }else{
                              ?>
                                  <option value="<?php echo $cat_id; ?>"><?php echo ucfirst($cat_name); ?></option>
                              <?php
                              }
                            } 
                          }
                      ?>
                  </select>
              </div>

              <div class="col-md-2 col_filterReport">
                  <div class="label_filterReport">Pilih Merchant (ID) :</div>
                  <select name="sel_merchant" class="jsselect sel_filterReport">
                      <option value="">Semua Merchant</option>
                      <?php 
                          global $wpdb;
            
                          $query = "SELECT *
                                    FROM ldr_merchants
                                    ORDER BY name ASC";

                          $res_query_mer = $wpdb->get_results($query, OBJECT);
                          $res_count_mer = count($res_query_mer);
                          if ($res_count_mer > 0){
                            foreach ($res_query_mer as $key_mer => $value_mer) {
                              $mer_id = $value_mer->id;
                              $mer_name = $value_mer->name;

                              if($mer_id == $this_merchant){
                              ?>
                                  <option value="<?php echo $mer_id; ?>" selected="selected"><?php echo ucfirst($mer_name).' ('.$mer_id.')'; ?></option>
                              <?php
                              }else{
                              ?>
                                  <option value="<?php echo $mer_id; ?>"><?php echo ucfirst($mer_name).' ('.$mer_id.')'; ?></option>
                              <?php
                              }
                            } 
                          }
                      ?>
                  </select>
              </div>

              <div class="col-md-2 col_filterReport">
                  <div class="label_filterReport">Dari Tanggal :</div>
                  <input type="text" name="from_date" class="txt_filterReport all_datepicker" value="<?php echo $from_date; ?>" readonly>
              </div>
              <div class="col-md-2 col_filterReport">
                  <div class="label_filterReport">Sampai Tanggal :</div>
                  <input type="text" name="to_date" class="txt_filterReport all_datepicker" value="<?php echo $to_date; ?>" readonly>
              </div>
              <div class="col-md-2 col_filterReport">
                  <input type="submit" class="sub_filterReport" value="Submit">
              </div>
            </div>
        </form>

        <div class="table-responsive">
          <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0" data-order="[[ 0, &quot;desc&quot; ]]">
            <thead>
              <tr>
                <th>Produk ID</th>
                <th>Nama</th>
                <th>Merchant (ID)</th>
                <th>Kategori</th>
                <th>Stock</th>
                <th>Harga</th>
                <th>Status</th>
              </tr>
            </thead>
            
            <tbody>
            <?php 

              $from_date = $from_date.' 00:00:00';
              $to_date = $to_date.' 23:59:59';

              if(isset($this_cat) AND $this_cat != ''){
                $query_cat = "AND categories_id = $this_cat";
              }else{
                $query_cat = "";
              }

              if(isset($this_merchant) AND $this_merchant != ''){
                $query_mer = "AND merchant_id = $this_merchant";
              }else{
                $query_mer = "";
              }

              global $wpdb;

              $query = "SELECT *
                        FROM ldr_products
                        WHERE created_at BETWEEN '$from_date' AND '$to_date'
                        $query_cat
                        $query_mer
                        ORDER BY id DESC ";

              $res_query = $wpdb->get_results($query, OBJECT);
              $res_count = count($res_query);

              // echo "<pre>";
              // // print_r($query);
              // echo "<Br/>";
              // print_r($res_query);
              // echo "</pre>";

              if ($res_count > 0){
                  foreach ($res_query as $key => $value) {

                    $product_id = $value->id;
                    $merchant_id = $value->merchant_id;
                    $product_name = ucfirst(strtolower($value->title));
                    $permalink = $value->permalink;
                    $product_link = home_url().'/product/'.$permalink.'-'.$product_id;
                    $brand_id = $value->brand_id;
                    $categories_id = $value->categories_id;
                    $price = $value->price;
                    $stock = $value->stock;
                    $status = $value->status;

                    $query2 = "SELECT *
                              FROM ldr_merchants
                              WHERE id = $merchant_id
                              ORDER BY name ASC";
                    $res_query_mer = $wpdb->get_results($query2, OBJECT);
                    $res_count_mer = count($res_query_mer);
                    if ($res_count_mer > 0){
                      foreach ($res_query_mer as $key_mer => $value_mer){
                          $merchant_name = ucfirst(strtolower($value_mer->name));
                      }
                    }else{
                        $merchant_name = '';
                    }

                    $query3 = "SELECT *
                              FROM ldr_categories
                              WHERE id = $categories_id
                              ORDER BY name ASC";

                    $res_query_cat = $wpdb->get_results($query3, OBJECT);
                    $res_count_cat = count($res_query_cat);
                    if ($res_count_cat > 0){
                      foreach ($res_query_cat as $key_cat => $value_cat) {
                        $cat_name = ucfirst(strtolower($value_cat->name));
                      }
                    }else{
                        $cat_name = '';
                    }


                    ?>
                        <tr>
                          <td><?php echo $product_id; ?></td>
                          <td>
                            <a target="_blank" href="<?php echo $product_link; ?>" title="Lihat detail produk ini">
                              <?php echo $product_name; ?>
                            </a>
                          </td>
                          <td><?php echo $merchant_name.' ('.$merchant_id.')'; ?></td>
                          <td><?php echo $cat_name; ?></td>
                          <td><?php echo $stock; ?></td>
                          <td><?php echo $price; ?></td>
                          <td><?php echo $status; ?></td>
                        </tr>
                    <?php
                  }
              }
            ?>
            </tbody>
          </table>
        </div>

      </div>
    </div>

  </div>
  <!-- /.container-fluid -->

<?php }else{ ?>
    <script>
        // 'Getting' data-attributes using getAttribute
        var plant = document.getElementById('body');
        console.log(plant);
        var hurl = plant.getAttribute('data-hurl'); 
        location.replace(hurl+'/login/'); 
    </script>
<?php } ?>


<?php endwhile; ?>
<?php else : ?>
    <?php get_template_part( 'content', '404pages' ); ?>  
<?php endif; ?>
<?php get_footer('admin'); ?>