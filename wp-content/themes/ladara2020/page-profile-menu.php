<?php
/*
Template Name: Profile Menu Page
*/
?>

<?php get_header(); ?>
<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
<style type="text/css">
@media only screen and (max-width:769px) {
	#inner-footer{
		display: none !important;
	}
}
</style>
<?php 
date_default_timezone_set("Asia/Jakarta");
$current_user = wp_get_current_user();
$u_id = $current_user->ID;
$user_login = $current_user->user_login;

$user_avatar = get_field('user_avatar', 'user_'.$u_id);
if(isset($user_avatar) AND $user_avatar != ''){
	$linkpoto = wp_get_attachment_image_src($user_avatar,'thumbnail');
	$url_avatar = $linkpoto[0];
}else{
	$url_avatar = get_template_directory_uri().'/library/images/icon_profile.png';
}

$nama_lengkap = get_field('nama_lengkap', 'user_'.$u_id);
$tanggal_lahir = get_field('tanggal_lahir', 'user_'.$u_id);
if(isset($tanggal_lahir)){
	$dob_data = explode('-', $tanggal_lahir);
	$dob_year = $dob_data[0];
	$dob_month = $dob_data[1];
	$dob_day = $dob_data[2];
}
$telepon = get_field('telepon', 'user_'.$u_id);
$jenis_kelamin = get_field('jenis_kelamin', 'user_'.$u_id);


if(isset($u_id) AND $u_id != 0){
?>

<div id="profilpage" class="row row_profile">
	<div class="col-md-3"></div>
	<div class="col-md-6 col_profile">
		
		<?php get_template_part( 'content', 'menu-profile' ); ?>

	</div>
	<div class="col-md-3"></div>
</div>


<?php }else{ ?>
    <script>
        // 'Getting' data-attributes using getAttribute
        var plant = document.getElementById('body');
        console.log(plant);
        var hurl = plant.getAttribute('data-hurl'); 
        location.replace(hurl+'/login/'); 
    </script>
<?php } ?>
<?php endwhile; ?>
<?php else : ?>
		<?php get_template_part( 'content', '404pages' ); ?>	
<?php endif; ?>
<?php get_footer(); ?>