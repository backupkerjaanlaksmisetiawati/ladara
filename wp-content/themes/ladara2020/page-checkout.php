<?php
/*
Template Name: New Checkout Page
*/

use GuzzleHttp\Client;
use GuzzleHttp\Cookie\CookieJar;
use Shipper\Location;
use Shipper\Rates;
?>

<?php get_header(); ?>

<?php if (have_posts()) :
    while (have_posts()) :
        the_post();

        global $wpdb;
        date_default_timezone_set("Asia/Jakarta");
        $nowdate = date('Y-m-d H:i:s');

        $currentUser = wp_get_current_user();
        $userId = $currentUser->ID;
        $identifier = $_SESSION['identifier'];

        if (isset($userId) && $userId != 0) {
            $totalAllItem = 0;
            $totalAllItemWithVoucher = 0;
            $cartIds = $_SESSION['cartIds'];
            $queryCart = $wpdb->get_results("SELECT * FROM ldr_cart WHERE user_id = '$userId' AND id IN ($cartIds)");
            $countCart = count($queryCart);

            if ($countCart === 0) {
?>
                <div class="row row_finishCheckout">
                    <div class="col-md-12 col_finishCheckout">
                        <a href="<?php echo home_url(); ?>">
                            <div class="bx_backShop">
                                <span class="glyphicon glyphicon-menu-left"></span> Kembali ke Home
                            </div>
                        </a>
                        <div class="bx_finishCheckout">
                            <div class="mg_registerIcon">
                                <img class="lazy" data-src="<?php bloginfo('template_directory'); ?>/library/images/ico_cartempty.svg">
                            </div>
                            <div class="ht_register">Duh kok kosong? tas belanjamu bisa masuk angin nih</div>
                            <div class="ht_sucs_register">
                                Yuk isi dengan produk terbaru atau produk yang sudah kamu mimpikan dari kemarin.
                            </div>
                            <div class="bx_def_checkout">
                                <a href="<?php echo home_url(); ?>/shop/">
                                    <button class="btn_def_checkout">Belanja Lagi</button>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            <?php
            } else { // ================= just do it =========================
                $invalidOrder = false;
                $allDataCart = [];

                foreach ($queryCart as $key => $value) {
                    $allDataCart[$value->seller_id][] = $value;
                }
            ?>

                <style type="text/css">
                    @media only screen and (max-width:769px) {

                        .row_fix_mobmenu,
                        #inner-footer {
                            display: none !important;
                        }
                    }
                </style>

                <form action="" name="form_checkoutCart" class="form_checkoutCart" id="form_checkoutCart" method="post" style="width: 100%;">
                    <input type="hidden" name="user_id" value="<?php echo $userId; ?>">
                    <div class="row row_masterCart">
                        <div class="col-md-12 col_masterCart">
                            <a href="<?php echo home_url(); ?>/shop/">
                                <div class="bx_backShop">
                                    <span class="glyphicon glyphicon-menu-left"></span> Lanjutkan Belanja
                                </div>
                            </a>
                            <div class="bx_tabsCheckout des_display">
                                <div class="tb_1chk act">
                                    <div class="sp_tabsch act">1</div>Pengiriman
                                </div>
                                <span class="line_tabsch"></span>
                                <div class="tb_1chk">
                                    <div class="sp_tabsch">2</div>Pembayaran
                                </div>
                            </div>
                            <div class="bx_checkout_page">
                                <h1 class="ht_masterCheckout">Checkout</h1>
                                <div class="alert alert-danger chk_err">
                                    <!-- Checkout Errors -->
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-7 col_checkout_page1">
                                    <div class="bx_checkout_page">
                                        <div class="btc_alamat">Alamat Pengiriman
                                            <a id="chs_open_address" title="Pilih alamat disini.">Pilih Alamat Lain</a>
                                        </div>
                                        <?php
                                        $destinationAreaId = null;

                                        if (isset($_SESSION['checkoutAddressId'][$identifier])) {
                                            $queryAddress = $wpdb->get_row("SELECT * FROM ldr_user_addresses WHERE user_id = '$userId' AND deleted_at IS NULL AND id = " . $_SESSION['checkoutAddressId'][$identifier]);
                                        } else {
                                            $queryAddress = $wpdb->get_row("SELECT * FROM ldr_user_addresses WHERE user_id = '$userId' AND deleted_at IS NULL AND main_address = 1");
                                        }

                                        if ($queryAddress !== null) {
                                            $getLocation = Location::searchLocation($queryAddress->postal_code);

                                            $location = array_values(array_filter($getLocation->data->rows, function ($fn) use ($queryAddress) {
                                                return $fn->area_id == $queryAddress->area_id;
                                            }));

                                            $addressId = $queryAddress->id;
                                            $name = $queryAddress->name;
                                            $consignee = $queryAddress->consignee;
                                            $phone = $queryAddress->phone;
                                            $address = $queryAddress->address;
                                            $provinceName = $location[0]->province_name;
                                            $cityName = $location[0]->city_name;
                                            $suburbName = $location[0]->suburb_name;
                                            $postalCode = $queryAddress->postal_code;
                                            $longitude = $queryAddress->longitude;
                                            $latitude = $queryAddress->latitude;
                                            $mainAddress = $queryAddress->main_address;

                                            $destinationAreaId = $queryAddress->area_id; // For Shipper
                                            $destinationCoord = $latitude . ',' . $longitude;

                                            if (!isset($_SESSION['checkoutAddressId'][$identifier])) {
                                                $_SESSION['checkoutAddressId'][$identifier] = $addressId;
                                            }
                                        ?>
                                            <div class="btc_info_addr">
                                                <b><?php echo $name; ?></b>
                                                <br />
                                                <?php echo $address . ', ' . $suburbName; ?>
                                                <br />
                                                <?php echo $cityName . ', ' . $provinceName . ', ' . $postalCode; ?>
                                                <br />
                                                Telp - <?php echo $phone; ?>
                                            </div>
                                        <?php
                                        } else {
                                            $invalidOrder = true;
                                        ?>
                                            <div class="cont_ckalamat">
                                                Anda belum memiliki alamat tujuan, Silahkan tambahkan dulu yuk!
                                            </div>

                                            <div class="bx_btn_af_ckalamat " style="text-align: center;">
                                                <input id="open_addAddress" type="button" class="btn_ckalamat_to" value="Tambah Alamat">
                                            </div>
                                        <?php
                                        }

                                        $shippingNote = '';
                                        if (isset($_SESSION['shippingNote'][$identifier]) && $_SESSION['shippingNote'][$identifier] != '') {
                                            $shippingNote = $_SESSION['shippingNote'][$identifier];
                                        }
                                        ?>

                                        <div class="bx_tx_ckalamat add_notealamat">
                                            <?= ($shippingNote === '' ? 'Tambah' : 'Ubah') ?> Catatan Pengiriman
                                        </div>

                                        <div class="ac_view_addnote"><?php echo $shippingNote; ?></div>
                                        <div class="ac_addnote">
                                            <textarea id="area_catatan" name="shippingNote" class="txarea_checkout" placeholder="Tambahan informasi..."><?php echo $shippingNote; ?></textarea>
                                            <input type="button" class="btn_save_area_catatan" value="Tambah">
                                        </div>
                                    </div>
                                    <div class="bx_checkout_page">
                                        <div class="btc_alamat">Pesanan Anda</div>

                                        <?php
                                        $totalShipmentFee = 0;
                                        $totalShipmentFeeWithVoucher = 0;
                                        $totalInsuranceFee = 0;
                                        $currentDate = date('Y-m-d H:i:s');
                                        $getCurrentDiscount = $wpdb->get_col("SELECT id FROM ldr_discount WHERE deleted_at IS NULL AND start_date <= '$currentDate' AND end_date >= '$currentDate'");

                                        foreach ($allDataCart as $sellerId => $cart) {
                                            $querySeller = $wpdb->get_row("SELECT * FROM ldr_merchants WHERE id = $sellerId AND deleted_at IS NULL");
                                            $originAreaId = $querySeller->area_id;
                                            $originCoord = $querySeller->latitude . ',' . $querySeller->longitude;
                                        ?>

                                            <div class="ht_cartSeller" style="padding-top: 20px;">
                                                <b><?php echo $querySeller->name; ?></b>
                                            </div>

                                            <?php
                                            $totalWeight = 0;
                                            $useInsurance = false;
                                            $forceInsurance = false;
                                            $shipmentFee = 0;
                                            $insuranceFee = 0;
                                            $getDomesticRates = (object) ['status' => 500];

                                            $requestBody = [
                                                'address_id' => $queryAddress->id,
                                                'carts' => array_map(function ($fn) {
                                                    return [
                                                        'id' => $fn->id,
                                                        'qty' => $fn->qty
                                                    ];
                                                }, $cart)
                                            ];

                                            if ($destinationAreaId !== null) {
                                                try {
                                                    $getDomesticRates = sendRequest('POST', MERCHANT_URL . '/api/shipper/get-price', $requestBody);

                                                    $checkCompulsoryInsurance = array_filter((array) $getDomesticRates->data, function ($fn) {
                                                        return $fn->data[0]->compulsory_insurance === 1;
                                                    });

                                                    $forceInsurance = count($checkCompulsoryInsurance) > 0;
                                                } catch (Exception $e) {
                                                    $invalidOrder = true;
                                                }

                                                $useInsurance = $forceInsurance;
                                            }

                                            foreach ($cart as $key => $value) {
                                                $productId = $value->product_id;
                                                $emptyStatus = '';
                                                $minusStatus = false;

                                                $product = $wpdb->get_row("SELECT * FROM ldr_products WHERE id = $productId AND deleted_at IS NULL");

                                                if ($product !== null) {
                                                    $cartQty = $value->qty;
                                                    $priceTotal = intval($product->price) * intval($cartQty);
                                                    $totalWeight = intval($product->weight) * intval($cartQty);
                                                    $salePrice = 0;
                                                    $salePriceTotal = 0;
                                                    $discountPercentage = 0;

                                                    $queryStock = $wpdb->get_row("SELECT * FROM ldr_stock WHERE id = " . $value->stock_id);

                                                    $size = $queryStock->ukuran_value;
                                                    $number = $queryStock->nomor_value;
                                                    $color = $queryStock->warna_value;
                                                    $productQuantity = intval($queryStock->product_quantity);

                                                    if ($productQuantity < $cartQty) {
                                                        $minusStatus = true;
                                                    }

                                                    $image = $wpdb->get_row("SELECT * FROM ldr_product_images WHERE products_id = $productId AND name = 'imageUtama' AND deleted_at IS NULL");
                                                    $urlPhoto = IMAGE_URL.'/' . $image->path;

                                                    if (count($getCurrentDiscount)) {
                                                        $productDiscount = $wpdb->get_row("SELECT * FROM ldr_discount_product WHERE deleted_at IS NULL AND product_id = $productId AND discount_id IN (" . implode(',', $getCurrentDiscount) . ")");
                                                        $salePrice = $productDiscount->discount_price ?? 0;
                                                        $discountPercentage = round(($product->price - $salePrice) / $product->price * 100);

                                                        if (intval($salePrice) !== 0) {
                                                            $salePriceTotal = intval($salePrice) * $cartQty;
                                                        }
                                                    }

                                                    if ($salePriceTotal !== 0) {
                                                        $totalAllItem += $salePriceTotal;
                                                    } else {
                                                        $totalAllItem += $priceTotal;
                                                    }
                                            ?>

                                                    <div class="row row_btc_product">
                                                        <div class="col-xs-2 col-md-2 col_btc_product">
                                                            <div class="mg_cartItem">
                                                                <img src="<?php echo $urlPhoto; ?>" alt="<?php echo $product->title; ?>">
                                                            </div>
                                                        </div>
                                                        <div class="col-xs-10 col-md-7 col_btc_mid_product">
                                                            <h4 class="ht_ck_items"><?php echo $product->title; ?></h4>

                                                            <div class="cont_ck_items ck_variasi">
                                                                <?php if ($color != '') { ?>
                                                                    <span><?php echo $color; ?></span>
                                                                <?php } ?>
                                                                <?php if ($size != '') { ?>
                                                                    <span><?php echo $size; ?></span>
                                                                <?php } ?>
                                                                <?php if ($number != '') { ?>
                                                                    <span><?php echo $number; ?></span>
                                                                <?php } ?>
                                                            </div>
                                                            <div class="bx_btc_price">
                                                                <span class="sale_chk"></span>
                                                                Rp <?php echo number_format($product->price); ?> / produk
                                                            </div>

                                                            <?php
                                                            if ($minusStatus) {
                                                            ?>
                                                                <div class="cont_ck_items" style="color: red;">Jumlah : <?php echo $productQuantity; ?> pcs</div>
                                                            <?php
                                                            } else {
                                                            ?>
                                                                <div class="cont_ck_items">Jumlah : <?php echo $cartQty; ?> pcs</div>
                                                            <?php
                                                            } ?>

                                                            <div class="cont_ck_items">Berat : <?php echo $totalWeight; ?> gram</div>
                                                        </div>
                                                        <div class="col-xs-12 col-md-3 col_btc_product">
                                                            <?php
                                                            if ($productQuantity === 0) {
                                                                $emptyStatus = 'HABIS';
                                                                $priceTotal = 0;
                                                                $salePriceTotal = 0;
                                                            } ?>
                                                            <h4 class="ht_ck_items" style="color: red;"><?php echo $emptyStatus; ?></h4>
                                                            <div class="bx_btc_subtotal">
                                                                <?php if ($product->asuransi === '1' && !$forceInsurance) {
                                                                    if (!$useInsurance) {
                                                                        $useInsurance = true;
                                                                    }
                                                                ?>
                                                                    <label class="cont_check" style="font-size: 10px; margin-top: -30px;">Asuransi Pengiriman</a>
                                                                        <input type="checkbox" class="check-insurance" value="true" checked data-seller-id="<?php echo $sellerId; ?>">
                                                                        <span class="checkmark"></span>
                                                                    </label>
                                                                <?php } ?>
                                                                <span>Subtotal</span>
                                                                <br />
                                                                <?php if (intval($salePrice) !== 0) { ?>
                                                                    Rp <?= number_format($salePriceTotal); ?>
                                                                    <?php if (intval($salePriceTotal) > 0) : ?>
                                                                        <div class="tx_price_sale" style="text-decoration: none;">
                                                                            <span style="text-decoration: line-through;">Rp <span class="total_cart_price_old" data-id="<?= $key ?>"><?= number_format($priceTotal) ?></span></span>
                                                                            <span class="discount" style="color: white;"><?php echo $discountPercentage; ?> %</span>
                                                                        </div>
                                                                    <?php endif; ?>
                                                                <?php } else { ?>
                                                                    Rp <?= number_format($priceTotal); ?>
                                                                <?php } ?>
                                                            </div>
                                                        </div>
                                                    </div>
                                            <?php
                                                }
                                            }
                                            ?>

                                            <div class="row row_btc_shipping">

                                                <?php
                                                if ($getDomesticRates->status === 200) {
                                                ?>
                                                    <div class="f_sel_shipping">
                                                        <label>Metode Pengiriman</label>
                                                        <input type="hidden" name="use_insurance[<?= $sellerId ?>]" class="use-insurance" value="<?= var_export($useInsurance) ?>" data-seller-id="<?php echo $sellerId; ?>">
                                                        <select class="sel_btc_shipping" name="shipping_method" data-seller-id="<?php echo $sellerId; ?>">
                                                            <?php
                                                            foreach ($getDomesticRates->data as $type => $rates) {
                                                                if (count($rates->data)) {
                                                                    $shipmentFee = $shipmentFee === 0 ? $rates->data[0]->shipping_price : $shipmentFee;
                                                                    $insuranceFee = $insuranceFee === 0 && $useInsurance ? $rates->data[0]->insurance_price : $insuranceFee;

                                                                    if (!isset($_SESSION['couriers'][$identifier][$sellerId])) {
                                                                        $_SESSION['couriers'][$identifier][$sellerId] = [
                                                                            'rate_id' => $rates->data[0]->rate_id,
                                                                            'name' => $rates->data[0]->name,
                                                                            'rate_name' => $rates->data[0]->rate_name,
                                                                            'logo_url' => $rates->data[0]->logo_url,
                                                                            'type' => $rates->data[0]->type,
                                                                            'use_insurance' => $useInsurance
                                                                        ];
                                                                    }

                                                                    $duration = "({$rates->min_day} - {$rates->max_day} Hari)";
                                                                    $rates->min_day === $rates->max_day && $duration = "($rates->max_day Hari)";
                                                            ?>
                                                                    <option style="background-color: white; color: #666666;" disabled><?= ucwords($type); ?> <?= $duration ?></option>
                                                                    <?php
                                                                    foreach ($rates->data as $rate) {
                                                                    ?>
                                                                        <option value="<?php echo $rate->rate_id; ?>" data-courier='<?= json_encode($rate) ?>' style="background: white; color: black;">
                                                                            &emsp; <?= "{$rate->name} - {$rate->rate_name}" ?> (Rp <?= number_format($rate->shipping_price); ?>)
                                                                        </option>
                                                            <?php
                                                                    }
                                                                }
                                                            }

                                                            $totalShipmentFee += $shipmentFee;

                                                            if ($useInsurance) {
                                                                $totalInsuranceFee += $insuranceFee;
                                                            }
                                                            ?>
                                                        </select>
                                                        <input type="hidden" name="courier[]">
                                                    </div>
                                                    <div class="bx_btc_shipping">
                                                        <label>Biaya Pengiriman</label>
                                                        <br />
                                                        <span class="label-shipment-fee" data-seller-id="<?php echo $sellerId; ?>">Rp <?php echo number_format($shipmentFee); ?></span>
                                                        <?php if ($useInsurance) : ?>
                                                            <br />
                                                            <label>Asuransi Pengiriman</label>
                                                            <br />
                                                            <span class="label-insurance-fee" data-seller-id="<?php echo $sellerId; ?>">Rp <?php echo number_format($insuranceFee); ?></span>
                                                        <?php endif ?>
                                                    </div>
                                                <?php
                                                } else {
                                                ?>
                                                    <div class="alert alert-danger" role="alert" style="font-size: 13px;">
                                                        <?php
                                                        if ($destinationAreaId === null) {
                                                        ?>
                                                            <b>Maaf, silahkan periksa kembali alamat tujuan Anda.</b>
                                                        <?php
                                                        } else {
                                                        ?>
                                                            <b>Maaf, opsi pengiriman tidak tersedia untuk alamat tujuan Anda.</b>
                                                        <?php
                                                        }
                                                        ?>
                                                    </div>
                                                <?php
                                                }
                                                ?>
                                            </div>
                                        <?php
                                        }

                                        $totalPayment = intval($totalAllItem) + intval($totalShipmentFee) + intval($totalInsuranceFee);
                                        $voucherId = isset($_SESSION['voucherId'][$identifier]) ? $_SESSION['voucherId'][$identifier] : 0;

                                        $voucher = $wpdb->get_row("SELECT * FROM ldr_vouchers WHERE id = " . $voucherId);
                                        $voucherAmount = 0;
                                        $voucherFreeShipping = 0;

                                        if ($voucher !== null) {
                                            $voucherAmount = ($voucher->discount_type === "1") ? round(($voucher->discount_amount / 100) * $totalAllItem) : $voucher->discount_amount;
                                            $voucherFreeShipping = $voucher->free_shipping;
                                            $totalAllItemWithVoucher = $totalAllItem;
                                            $totalShipmentFeeWithVoucher = $totalShipmentFee;

                                            if (!$voucherFreeShipping) {
                                                $totalAllItemWithVoucher = max($totalAllItem - $voucherAmount, 0);
                                            } else {
                                                $totalShipmentFeeWithVoucher = max($totalShipmentFee - $voucherAmount, 0);
                                            }

                                            $totalPayment = intval($totalAllItemWithVoucher) + intval($totalShipmentFeeWithVoucher) + intval($totalInsuranceFee);
                                        }
                                        ?>
                                    </div>
                                </div>
                                <div class="col-md-5 col_checkout_page2">
                                    <div id="box-checkout-voucher" class="bx_checkout_page">
                                        <div class="btc_alamat pad_alamat">Voucher</div>
                                        <div class="f_chk_total d-flex jc-space-between" onclick="popupVoucher('showpopup')">
                                            <div id="txtVoucher">
                                                <?php if ($voucher !== null) { ?>
                                                    Kamu hemat <strong>Rp <?= number_format(round($voucherAmount), 0, ",", ".") ?></strong><br />
                                                    <span>Kode voucher: <?= $voucher->code ?></span>
                                                <?php } else { ?>
                                                    Dapatkan potongan harga
                                                <?php } ?>
                                            </div>
                                            <div class="icon-arrow d-flex jc-space-between ai-center">
                                                <img src="<?php bloginfo("template_directory"); ?>/library/images/arrow-down-blue.svg" />
                                            </div>
                                        </div>
                                    </div>

                                    <div class="bx_checkout_page">
                                        <div class="btc_alamat pad_alamat">Rincian Pembayaran</div>
                                        <div class="f_chk_total total_pay" style="border-top: 0; margin-top: 0; padding-top: 0;">
                                            <div>
                                                Total Harga
                                                <span class="rv_pricetotal <?= ($voucher !== null && !$voucherFreeShipping ? 'line-through' : '') ?>">
                                                    Rp <?= number_format($totalAllItem) ?>
                                                </span>
                                            </div>
                                            <div <?= ($voucher !== null && !$voucherFreeShipping ? '' : 'style="display: none;"') ?>>
                                                &nbsp;
                                                <span class="rv_priceTotalWithDiscount total_pay_txt" style="<?= ($voucher !== null ? 'display: block;' : '') ?>">
                                                    Rp <?= number_format($totalAllItemWithVoucher) ?>
                                                </span>
                                            </div>
                                        </div>
                                        <div class="f_chk_total total_pay" style="border-top: 0; margin-top: 0; padding-top: 0;">
                                            <div>
                                                Biaya Pengiriman
                                                <span class="rv_shippingtotal <?= ($voucher !== null && $voucherFreeShipping ? 'line-through' : '') ?>">
                                                    Rp <?= number_format($totalShipmentFee); ?>
                                                </span>
                                            </div>
                                            <div <?= ($voucher !== null && $voucherFreeShipping ? '' : 'style="display: none;"') ?>>
                                                &nbsp;
                                                <span class="rv_shippingTotalWithDiscount total_pay_txt" style="<?= ($voucher !== null ? 'display: block;' : '') ?>">
                                                    Rp <?= number_format($totalShipmentFeeWithVoucher) ?>
                                                </span>
                                            </div>
                                        </div>
                                        <div class="f_chk_total">
                                            Biaya Asuransi <span class="rv_insurancetotal">Rp <?php echo number_format($totalInsuranceFee); ?></span>
                                        </div>
                                        <input type="hidden" class="rv_discounttype" value="<?= $voucher->discount_type ?? 0 ?>">
                                        <input type="hidden" class="rv_discountamount" value="<?= $voucher->discount_amount ?? 0 ?>">
                                        <input type="hidden" class="rv_discountmax" value="<?= $voucher->discount_max ?? 0 ?>">
                                        <input type="hidden" class="rv_discountfreeshipping" value="<?= $voucher->free_shipping ?? 0 ?>">
                                        <div class="f_chk_total total_pay">
                                            Total Pembayaran
                                            <span class="rv_totalpayment total_pay_txt">
                                                Rp <?php echo number_format($totalPayment); ?>
                                            </span>
                                        </div>
                                        <div class="row_fix_submitCheckout">
                                            <div class="mob_left_cartCheckout mob_display">
                                                <div class="info_cartBottom">Total Harga (<?php echo $countCart; ?> barang)</div>
                                                <div class="price_cartBottom">Rp <?php echo number_format($totalAllItem); ?></div>
                                            </div>
                                            <input type="button" class="sub_cartCheckout" value="Pilih Pembayaran" <?php echo ($invalidOrder ? 'disabled' : ''); ?> onclick="submitCheckoutCart(event);">
                                        </div>

                                        <?php
                                        wp_nonce_field('CKNBP');
                                        ?>

                                        <input type="hidden" name="u_total_payment" placeholder="total payment" value="<?php echo $totalPayment; ?>">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
                <script>
                    if (window.history.replaceState) {
                        window.history.replaceState(null, null, window.location.href);
                    }
                </script>

            <?php
                get_template_part('content', 'checkout-address');
                get_template_part('content', 'popconfirm');
                get_template_part('popups/popup', 'voucher');
                get_template_part('popups/popup', 'voucher-detail');
            }
        } else { ?>
            <script>
                // 'Getting' data-attributes using getAttribute
                var plant = document.getElementById('body');
                console.log(plant);
                var hurl = plant.getAttribute('data-hurl');
                location.replace(hurl + '/login/');
            </script>
<?php }
    endwhile;
else :
    get_template_part('content', '404pages');
endif;
get_footer();
?>