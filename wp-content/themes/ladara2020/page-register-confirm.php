<?php
/*
Template Name: Register Confirm
*/
use Ladara\Models\Notifications;
?>

<?php get_header(); ?>
<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
<?php 
$current_user = wp_get_current_user();
$u_id = $current_user->ID;
$valid = 0;

if(isset($_GET['ups'])){
	$upscode = $_GET['ups'];

	global $wpdb;
	$wpdb_query2 = "SELECT * FROM ldr_users_temp WHERE activation = '$upscode' AND password != '' ";
	$res_query2 = $wpdb->get_results($wpdb_query2, OBJECT);
	$count_res2 = count($res_query2);
	if($count_res2 > 0){

		foreach ($res_query2 as $key => $value) {
			$u_email = $value->email;
			$u_password = $value->password;
			$u_fullname = $value->fullname;
			$u_phone = $value->phone;

			$wpdb_query = "SELECT * FROM ldr_users WHERE user_login = '$u_email' OR user_email = '$u_email' ";
			$res_query = $wpdb->get_results($wpdb_query, OBJECT);
			$count_res = count($res_query);
			if($count_res == 0){

				$user_id = wp_create_user( $u_email, $u_password, $u_email);

				if(isset($user_id)){
					wp_update_user( array( 'ID' => $user_id, 'display_name' => $u_fullname, 'user_nicename' => $u_fullname, 'first_name' => $u_fullname ) );

					update_field('nama_lengkap',$u_fullname,'user_'.$user_id);

	                $result = $wpdb->update($wpdb->users, array('user_login' => $u_email), array('ID' => $user_id));
	                wp_update_user( array ( 'ID' => $user_id, 'nickname' => $u_email ) ) ;
	                wp_update_user( array ( 'ID' => $user_id, 'user_email' => $u_email ) ) ;

					update_field("telepon",$u_phone,"user_".$user_id);

					$wp_user_object = new WP_User($user_id);
					$wp_user_object->set_role('customer');

					global $wpdb;
					$wpdb_query_update = " UPDATE ldr_users_temp
					              SET password=''
					              WHERE activation = '$upscode'
					            ";
					$res_query_update = $wpdb->query($wpdb_query_update);

					$acc_type = 0;
			        //Insert into custom database
			        $insert_result = $wpdb->insert('ldr_user_profile', array(
			            'user_id' => $user_id,
			            'name' => $u_fullname,
			            'account_type' => $acc_type,
			            'email' => $u_email,
			            'phone' => $u_phone,
			            'created_date' => current_time( 'mysql', true)
			        ));

					$valid = 1;

					// sent email
					// email_RegisterConfirm($u_email,$u_fullname);

			        // =========== insert notification ==============
					$orderId_notif = '0';
					$type_notif = 'info';
					$title_notif = 'Selamat Datang';
					$desc_notif = 'Terima kasih sudah bergabung bersama Ladara Indonesia. Dapatkan ribuan keperluan Anda hanya di LaDaRa Indonesia.';
					$data = [
					      'userId' => $user_id, //wajib
					      'title' => $title_notif, //wajib
					      'descriptions' => $desc_notif, //wajib
					      'type' => $type_notif, //pesanan, info, pembayaran, emas // wajib
					      'orderId' => $orderId_notif, // optional
					      'data' => [] //array optional bisa diisi dengan data lainnya
					];
					$addNotif = Notifications::addNotification($data);
					// =========== insert notification ==============
					

				}
			}
		}
	}

}else{
	$upscode = '';
}

?>

<div class="row"></div>

<div class="row row_register">
	<div class="col-md-1"></div>
	<div class="col-md-10 col_register">
		
		<div class="col_cont_register">
			
			

			<?php if($valid == 1 AND $upscode != ''){ ?>

				<div class="mg_registerIcon">
					<img class="lazy" data-src="<?php bloginfo('template_directory'); ?>/library/images/regis_success.svg">
				</div>
				<div class="ht_register">Selamat! Akun Anda telah terdaftar</div>

				<div class="ht_sucs_register">
					<b>Terima kasih telah melakukan pendaftaran.</b><br/> Nikmati pengalaman belanja yang menyenangkan di Ladara Indonesia
				</div>

			<?php }else if($valid != 1 AND $upscode != ''){ ?>

				<div class="mg_registerIcon">
					<img class="lazy" data-src="<?php bloginfo('template_directory'); ?>/library/images/emas\img-finished-activation.svg">
				</div>
				<div class="ht_register">Akun Anda telah aktif</div>

				<div class="ht_sucs_register">
					Segera Login dan Belanja yuk!
				</div>

			<?php }else{ ?>

				<div class="mg_registerIcon">
					<img class="lazy" data-src="<?php bloginfo('template_directory'); ?>/library/images/payment_gagal.svg">
				</div>
				<div class="ht_register">Maaf,</div>

				<div class="ht_sucs_register">
					sepertinya Anda nyasar ya? Mendingan belanja yuk!
				</div>
			<?php } ?>

			<a href="<?php echo home_url(); ?>/login/" title="Yuk Login Sekarang!">
				<input type="button" class="sub_register" value="Login Sekarang">
			</a>

		</div>
	
	</div>
	<div class="col-md-1"></div>
</div>

<?php // ============= cannot back ============= ?>
<script type="text/javascript">
    (function (global) { 

        if(typeof (global) === "undefined") {
            throw new Error("window is undefined");
        }

        var _hash = "!";
        var noBackPlease = function () {
            global.location.href += "#";

            // making sure we have the fruit available for juice (^__^)
            global.setTimeout(function () {
                global.location.href += "!";
            }, 50);
        };

        global.onhashchange = function () {
            if (global.location.hash !== _hash) {
                global.location.hash = _hash;
            }
        };

        global.onload = function () {            
            noBackPlease();

            // disables backspace on page except on input fields and textarea..
            document.body.onkeydown = function (e) {
                var elm = e.target.nodeName.toLowerCase();
                if (e.which === 8 && (elm !== 'input' && elm  !== 'textarea')) {
                    e.preventDefault();
                }
                // stopping event bubbling up the DOM tree..
                e.stopPropagation();
            };          
        }

    })(window);
</script>

<?php endwhile; ?>
<?php else : ?>
		<?php get_template_part( 'content', '404pages' ); ?>	
<?php endif; ?>
<?php get_footer(); ?>