      </div>
      <!-- End of Main Content -->

    </div>
    <!-- End of Content Wrapper -->

  </div>
  <!-- End of Page Wrapper -->

  <!-- Scroll to Top Button-->
  <a class="scroll-to-top rounded" href="#page-top">
    <i class="fas fa-angle-up"></i>
  </a>

  <!-- Logout Modal-->
  <div class="modal fade" id="logoutModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Ready to Leave?</h5>
          <button class="close" type="button" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">×</span>
          </button>
        </div>
        <div class="modal-body">Select "Logout" below if you are ready to end your current session.</div>
        <div class="modal-footer">
          <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>
          <a class="btn btn-primary" href="<?php echo wp_logout_url(home_url('/')); ?>">Logout</a>
        </div>
      </div>
    </div>
  </div>


	<footer class="footer" role="contentinfo">

		<div id="inner-footer" class="wrap clearfix">

			<div class="row row_copyright">
				<div class="col-md-12">
					<a href="<?php echo home_url(); ?>">
						<div class="pt_copyright">
              ©2020 Ladara Indonesia. <span>All Rights Reserved</span>
						</div>
					</a>
				</div>
			</div>

		</div>

	</footer>

</div> <?php // end of div container ?>

<?php wp_footer(); ?>
    
  <!-- Bootstrap core JavaScript-->
  <script src="<?php echo get_template_directory_uri(); ?>/sbadmin2/vendor/jquery/jquery.min.js"></script>
  <script src="<?php echo get_template_directory_uri(); ?>/sbadmin2/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

  <!-- Core plugin JavaScript-->
  <script src="<?php echo get_template_directory_uri(); ?>/sbadmin2/vendor/jquery-easing/jquery.easing.min.js"></script>

  <!-- Custom scripts for all pages-->
  <script src="<?php echo get_template_directory_uri(); ?>/sbadmin2/js/sb-admin-2.min.js"></script>


  <?php 
  // tambahkan setiap buat halaman baru di halaman dashboard admin dengan slug, dan tambahkan juga di function.php *jika di perlukan datatable, datepicker.
  if ( is_page( array('finance',
                            'daftar-merchant',
                            'detail',
                            'completed',
                            'ladara-emas',
                            'report-order',
                            'report-mile',
                            'report-member',
                            'report-stock',
                            'upload',
                            'admin-kategori',
                            'ladara-emas-transaction',
                            'ladara-emas-user',
                            'user-emas-detail',
                            'emas-transaction-detail',
                            'so-report',
                            'daftar-produk'
                          ) ) ) { ?>

      <script src="<?php echo get_template_directory_uri(); ?>/library/js/jquery-datepicker.min.js"></script>
      <script src="<?php echo get_template_directory_uri(); ?>/library/js/jquery.datetimepicker.full.min.js"></script>

      <script src="<?php echo get_template_directory_uri(); ?>/sbadmin2/vendor/datatables/jquery.dataTables.min.js"></script>
      <script src="<?php echo get_template_directory_uri(); ?>/sbadmin2/vendor/datatables/dataTables.bootstrap4.min.js"></script>

      <script src="<?php echo get_template_directory_uri(); ?>/library/js/select2.min.js"></script>

  <?php }else{ ?>

    <!-- Page level plugins -->
    <script src="<?php echo get_template_directory_uri(); ?>/sbadmin2/vendor/chart.js/Chart.min.js"></script>
    <!-- Page level custom scripts -->
    <script src="<?php echo get_template_directory_uri(); ?>/sbadmin2/js/demo/chart-area-demo.js"></script>
    <script src="<?php echo get_template_directory_uri(); ?>/sbadmin2/js/demo/chart-pie-demo.js"></script>

  <?php } ?>

</body>

</html>