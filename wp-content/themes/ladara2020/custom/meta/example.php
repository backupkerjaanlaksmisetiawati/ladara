<?php

$args2 = array(
    'type'             => 'string', // Validate and sanitize the meta value as a string.
    'description'    => 'A meta key associated with a string meta value.', // Shown in the schema for the meta key.
    'single'        => false, // Return an array with the type used as the items type. Default: false.
    'show_in_rest'    => true, // Show in the WP REST API response. Default: false.
);
 
register_meta( 'custom_type', 'my_usermeta_key', $args2 );

?>