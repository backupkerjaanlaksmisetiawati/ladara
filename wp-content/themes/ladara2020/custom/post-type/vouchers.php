<?php
/**
 * NOTE:
 * custom taxonomy tanpa WCK
 * supaya setiap ada penambahan taxonomy
 * semua enviroment (local, staging, live) langsung terupdate
 * tanpa harus setting WCK
 * pastikan file ter-include di functions.php
 * check dibagian '6. custom/post-type/(any).php' untuk melihat cara men-include
 * file ini dibuat di folder terpisah supaya lebih rapih
 * 
 * - Ryu Amy -
 */

register_post_type( 'voucher',
	array(
    'labels' => array(
      'name' => __( 'Voucher', 'bonestheme' ), 
      'singular_name' => __( 'Voucher', 'bonestheme' ),
      'all_items' => __( 'All Voucher', 'bonestheme' ),
      'add_new' => __( 'Add New', 'bonestheme' ),
      'add_new_item' => __( 'Add New Voucher', 'bonestheme' ),
      'edit' => __( 'Edit', 'bonestheme' ),
      'edit_item' => __( 'Edit Voucher', 'bonestheme' ),
      'new_item' => __( 'New Voucher', 'bonestheme' ),
      'view_item' => __( 'View Voucher', 'bonestheme' ),
      'search_items' => __( 'Search Voucher', 'bonestheme' ),
      'not_found' =>  __( 'Nothing found in the Database.', 'bonestheme' ),
      'not_found_in_trash' => __( 'Nothing found in Trash', 'bonestheme' ),
      'parent_item_colon' => ''
		),
		'description' => __( 'Ladara emas Voucher', 'bonestheme' ),
		'public' => true,
		'hierarchical' => false,
		'exclude_from_search' => true,
		'publicly_queryable' => true,
		'show_ui' => true,
		'show_in_menu' => true,
		'show_in_nav_menus' => true,
		'show_in_rest' => true,
		'menu_position' => 25,
		'menu_icon' => 'dashicons-tickets-alt',
		'capability_type' => 'post',
		'supports' => array( 'title', 'editor', 'custom-fields', 'page-attributes', 'revisions' ),
		'taxonomies' => array( 'category' ),
		'has_archive' => false,
		'rewrite'	=> array(
			'with_front' => true,
			'slug' => 'vouchers'
		)
	)
);
?>
