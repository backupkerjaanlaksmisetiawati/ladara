<?php
/* Bones Custom Post Type Example
This page walks you through creating 
a custom post type and taxonomies. You
can edit this one or copy the following code 
to create another one. 

I put this in a separate file so as to 
keep it organized. I find it easier to edit
and change things if they are concentrated
in their own file.

Developed by: Eddie Machado
URL: http://themble.com/bones/
*/

/**
 * NOTE:
 * custom taxonomy tanpa WCK
 * supaya setiap ada penambahan taxonomy
 * semua enviroment (local, staging, live) langsung terupdate
 * tanpa harus setting WCK
 * pastikan file ter-include di functions.php
 * check dibagian '5. custom-taxonomy/(any).php' untuk melihat cara men-include
 * 
 * thanks Eddie Machado for develop this
 * 
 * - Ryu Amy -
 */

// register_taxonomy(
//   'posisi-kepengurusan',
//   array('kepengurusan'),
//   array(
//     'hierarchical' => true,
//     'labels' => array(
//       'name' => __( 'Posisi Kepengurusan', 'bonestheme' ),
//       'singular_name' => __( 'Posisi Kepengurusan', 'bonestheme' ),
//       'search_items' =>  __( 'Search Posisi Kepengurusan', 'bonestheme' ),
//       'all_items' => __( 'All Posisi Kepengurusan', 'bonestheme' ),
//       'parent_item' => __( 'Parent Posisi Kepengurusan', 'bonestheme' ),
//       'parent_item_colon' => __( 'Parent Posisi Kepengurusan:', 'bonestheme' ),
//       'edit_item' => __( 'Edit Posisi Kepengurusan', 'bonestheme' ),
//       'update_item' => __( 'Update Posisi Kepengurusan', 'bonestheme' ),
//       'add_new_item' => __( 'Add New Posisi Kepengurusan', 'bonestheme' ),
//       'new_item_name' => __( 'New Posisi Kepengurusan Name', 'bonestheme' )
//     ),
//     'show_admin_column' => true, 
//     'show_ui' => true,
//     'query_var' => true,
//     'rewrite' => array(
//       'slug' => 'posisi-kepengurusan',
//       'with_front' => true
//     ),
//   )
// );
	
/*
  looking for custom meta boxes?
  check out this fantastic tool:
  https://github.com/jaredatch/Custom-Metaboxes-and-Fields-for-WordPress
*/	

?>