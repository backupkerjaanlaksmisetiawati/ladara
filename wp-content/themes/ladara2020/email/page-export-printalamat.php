<?php
/*
Template Name: Export Print Alamat
*/
?>
<link href="https://fonts.googleapis.com/css?family=Ubuntu:400,500&display=swap" rel="stylesheet">

<?php get_header('admin'); ?>
<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
<style type="text/css">
	header, #accordionSidebar, #adm_topbar{
		display: none !important;
	}


.row_printDefault{
	font-family: 'Ubuntu', sans-serif;
	font-weight: 500;
	display: flex;
	padding-top: 20px;
	padding-bottom: 20px;
	background: #fff;
	font-size: 12px;
	margin: 0;
	color: #000;
}
.col_printDefault{
	margin: 0;
	margin-left: 15px;
}
.col-print-1 {width:8%;  float:left;}
.col-print-2 {width:16%; float:left;}
.col-print-3 {width:25%; float:left;}
.col-print-4 {width:33%; float:left;}
.col-print-5 {width:42%; float:left;}
.col-print-6 {width:50%; float:left;}
.col-print-7 {width:58%; float:left;}
.col-print-8 {width:66%; float:left;}
.col-print-9 {width:75%; float:left;}
.col-print-10{width:83%; float:left;}
.col-print-11{width:92%; float:left;}
.col-print-12{width:100%; float:left;}
.bx_right_table{
	text-align: right;
}
.table_print{
	font-size: 12px;
	color: #000;
}
.ttd_print{
	width: 25%;
	padding: 10px;
	padding-top: 40px;
	text-align: center;
	border: 1px solid #000;
}
.info_print{
	font-size: 10px;
	text-align: center;
}
.info_btsprint{
	font-size: 9px;
	color: #000;
	font-weight: 400;
	text-align: center;
}
.ht_adsprintDefault{
    font-size: 17px;
    color: #000;
    text-align: left;
    font-weight: 600;
    letter-spacing: .3px;
    margin: 0;    
}
.row_topPrint{
	border-bottom: 1px solid #000;
    padding-bottom: 10px;
    padding-top: 10px;
    margin-bottom: 15px;
}
.mg_barcode{
	margin: auto;
	text-align: center;
}
.mg_barcode img{
	width: 150px;
	height: auto;
}
.tt_printDef{
    font-size: 11px;
    color: #000;
}
.box_printDef{
    font-size: 12px;
    color: #333;
}
.mg_headprint{
	height: 60px;width: auto;
}
</style>

<?php 
$current_user = wp_get_current_user();
$u_id = $current_user->ID;
$u_roles = $current_user->roles;
$admin = 0;

if(isset($u_roles) AND !empty($u_roles)){
  foreach ($u_roles as $key => $value) {

		if(strtolower($value) == 'administrator'){
			$admin = 1;
		}
		if(strtolower($value) == 'shop_manager'){
			$admin = 1;
		}
  }
}

if((isset($u_id) OR $u_id != 0) AND $admin == 1){


	if(isset($_GET['so'])){
	  $order_id = $_GET['so'];
	}else{
	  $order_id = '';
	}

	$shipping_total = 0;
	$order = wc_get_order($order_id);
	if(isset($order) AND !empty($order)){ // jika ada

	    $courier_resi = '';
	    global $wpdb;
	    $query = "SELECT *
	              FROM ldr_orders
	              WHERE order_id = '$order_id' 
	              ";
	    $res_query = $wpdb->get_results($query, OBJECT);

	    // echo "<pre>";
	    // print_r($res_query);
	    // echo "</pre>";
	    $nomor_booking_kurir = '';
	    $res_count = count($res_query);
	    if ($res_count > 0){
	        foreach ($res_query as $key => $value){
	            $courier_name = $value->courier_name;
	            // $courier_resi = $value->courier_resi;
	            $shipping_total = $value->shipping_price;
	            $voucher_id = $value->voucher_id;
	            $address_id = $value->address_id;
	            $notes = $value->notes;
	            $mile_poin = $value->mile_poin;

	            $nomor_booking_kurir = $value->nomor_booking_kurir;
	        }
	    }

	    $user_id = $order->user_id;
	    $user_data = get_userdata( $user_id );
	    $user_name = $user_data->data->display_name;
	    $order_status  = $order->get_status();
	    $total_payment = $order->get_total();

	    $get_Data = $order->get_data();  
	    $billing = $get_Data['billing'];
	    $email = $billing['email'];
	    $country_code = $billing['country'];

	    $countries_obj   = new WC_Countries();
	    $allcountries = $countries_obj->countries;
	    $country = $allcountries[$country_code];

	    $order_date = get_the_date('Y-m-d | h:i:s', $order_id);
	    $so_date = get_the_date('d F Y', $order_id);
	    $soon_date = date("d F Y", strtotime(get_the_date('Y-m-d', $order_id) . " +1 day"));

		global $wpdb;
	    $wpdb_query = "SELECT * FROM ldr_orders_address WHERE  order_id = '$order_id' ";
	    $res_query = $wpdb->get_results($wpdb_query, OBJECT);

	    $count_res = count($res_query);
	    if($count_res > 0){ // if not exist with same user_id
	    	foreach ($res_query as $key => $value) {
	    		$nama_toko = $value->nama_toko;
	    		$telepon_toko = $value->telepon_toko;
				$first_name = $value->nama_penerima;
				$address_1 = $value->alamat;
				$provinsi = $value->provinsi;
				$kota = $value->kota;
				$kecamatan = $value->kecamatan;
				$kode_pos = $value->kode_pos;
				$longitude = $value->longitude;
				$latitude = $value->latitude;
				$phone = $value->telepon_penerima;
	    	}
	    	
		}

		$tx_date = get_the_date('dmY', $order_id);
		$inv_id = 'INV/'.$tx_date.'/'.$order_id;

		if(isset($nomor_booking_kurir) AND $nomor_booking_kurir != ''){
			$code_barcode = $nomor_booking_kurir;
		}else{
			$code_barcode = $order_id;
		}
?>

	<div class="row row_printDefault">
		<!-- <div class="col-print-3"></div> -->
		<div class="col-print-5 col_printDefault">
			
			<div class="row row_topPrint">
				<div class="col-print-6">
						<img class="mg_headprint" style="" src="<?php bloginfo('template_directory'); ?>/library/images/main_logo.png">

						<div class="tt_printDef">Penjual / Pengirim:</div>
					<?php if(isset($nama_toko) AND $nama_toko != ''){ ?>
						<div class="ht_adsprintDefault"><?php echo $nama_toko; ?></div>
						<?php /*
						<div class="tel_prinDefault">Telepon: <?php echo $telepon_toko; ?></div>
						*/ ?>
					<?php }else{ ?>
						<div class="ht_adsprintDefault">Ladara Indonesia</div>
						<div class="tel_prinDefault">www.ladara.id</div>
					<?php } ?>

				</div>
				<div class="col-print-6">
					<div class="mg_barcode">
						<!-- <img src="<?php // bloginfo('template_directory'); ?>/library/images/barcode.jpg"> -->
						<img alt='invoice barcode' src='<?php bloginfo('template_directory'); ?>/barcode/barcode.php?codetype=code128&size=40&text=<?php echo $code_barcode; ?>'/>
					</div>
					<?php if(isset($nomor_booking_kurir) AND $nomor_booking_kurir != ''){ ?>
						<div class="info_print big"><b><?php echo $nomor_booking_kurir; ?></b></div> 
						<div class="info_btsprint"><i>Kode Booking Ini Bukan No Resi Pengiriman</i></div>
					<?php }else{ ?>
						<div class="info_print"><?php echo $inv_id; ?></div> 
						<div class="info_btsprint"><i>Kode Booking Ini Bukan No Resi Pengiriman</i></div>
					<?php } ?>
					
				</div>
			</div>
			

			<?php /* if(isset($nama_toko) AND $nama_toko != ''){ ?>
				<div class="row row_in_printDef">
					<div class="col-print-12">
						<div class="bx_print_addr">
							<div class="tt_printDef">Pembeli / Tujuan :</div>
							<div class="box_printDef"><b><?php echo $first_name; ?></b></div>
							<div class="box_printDef">
		                        <?php echo $address_1; ?> <br/>
		                        <?php echo  $kota.', '.$kecamatan; ?> <br/>
		                        <?php echo $provinsi.'. '.$postcode; ?>
							</div>
							<div class="box_printDef">Telepon: <?php echo $phone; ?></div>
						</div>
					</div>
					<div class="col-print-12">
						<div class="bx_print_addr">
							<div class="tt_printDef">Detail</div>
							<div class="box_printDef">Tanggal Pembelian: <?php echo $so_date; ?></div>
							<div class="box_printDef">Kurir: <b>JNE Reguler</b></div>
							<div class="box_printDef">Ongkir: <b>Rp <?php echo number_format($shipping_total); ?></b></div>
						</div>
					</div>
				</div>
			<?php }else{ */ ?>
				<div class="row row_in_printDef">
					
					<div class="col-print-12">
						<div class="bx_print_addr">
							<div class="bx_print_addr">
								<div class="tt_printDef">Detail</div>
								<div class="box_printDef">Tanggal Pembelian: <?php echo $so_date; ?></div>
								<div class="box_printDef">Kurir: <b>JNE Reguler</b></div>
								<div class="box_printDef">Ongkir: <b>Rp <?php echo number_format($shipping_total); ?></b></div>
							</div>
						</div>
					</div>
					<div class="col-print-12">
						<div class="bx_print_addr">
							<div class="tt_printDef">Pembeli / Tujuan :</div>
							<div class="box_printDef"><b><?php echo $first_name; ?></b></div>
							<div class="box_printDef">
		                        <?php echo $address_1; ?> <br/>
		                        <?php echo  $kota.', '.$kecamatan; ?> <br/>
		                        <?php echo $provinsi.'. '.$postcode; ?>
							</div>
							<div class="box_printDef">Telepon: <?php echo $phone; ?></div>
						</div>
					</div>

				</div>

			<?php // } ?>
			
		</div>
		<!-- <div class="col-print-3"></div> -->
	</div>

	<?php } ?>

<?php }else{ ?>
    <script>
        // 'Getting' data-attributes using getAttribute
        var plant = document.getElementById('body');
        console.log(plant);
        var hurl = plant.getAttribute('data-hurl'); 
        location.replace(hurl+'/login/'); 
    </script>
<?php } ?>

<?php endwhile; ?>
<?php else : ?>
		<?php get_template_part( 'content', '404pages' ); ?>	
<?php endif; ?>
<?php // get_footer(); ?>