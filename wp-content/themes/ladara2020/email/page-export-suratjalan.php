<?php
/*
Template Name: Export Surat Jalan
*/
?>
<link href="https://fonts.googleapis.com/css?family=Ubuntu:400,500&display=swap" rel="stylesheet">

<?php get_header('admin'); ?>
<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
<style type="text/css">
	header, #accordionSidebar, #adm_topbar{
		display: none !important;
	}


.row_printDefault{
	font-family: 'Ubuntu', sans-serif;
	font-weight: 500;
	display: flex;
	padding-top: 20px;
	padding-bottom: 20px;
	background: #fff;
	font-size: 14px;
}
.col-print-1 {width:8%;  float:left;}
.col-print-2 {width:16%; float:left;}
.col-print-3 {width:25%; float:left;}
.col-print-4 {width:33%; float:left;}
.col-print-5 {width:42%; float:left;}
.col-print-6 {width:50%; float:left;}
.col-print-7 {width:58%; float:left;}
.col-print-8 {width:66%; float:left;}
.col-print-9 {width:75%; float:left;}
.col-print-10{width:83%; float:left;}
.col-print-11{width:92%; float:left;}
.col-print-12{width:100%; float:left;}
.bx_right_table{
	text-align: right;
}
.table_print{
	font-size: 14px;
	color: #000;
}
.ttd_print{
	width: 25%;
	padding: 10px;
	padding-top: 40px;
	text-align: center;
	border: 2px solid #000;
}
.info_print{
	margin-top: 20px;
}
.ht_printDefault{
    font-size: 20px;
    color: #333;
    text-align: left;
    font-weight: 600;
    letter-spacing: .3px;
    margin: auto;
    padding-bottom: 10px;
    padding-top: 10px;
    margin-bottom: 15px;
}
.mg_headprint{
	height: 60px;width: auto;
	margin-bottom: 15px;
}
</style>

<?php 
$current_user = wp_get_current_user();
$u_id = $current_user->ID;
$u_roles = $current_user->roles;
$admin = 0;

if(isset($u_roles) AND !empty($u_roles)){
  foreach ($u_roles as $key => $value) {
		if(strtolower($value) == 'administrator'){
			$admin = 1;
		}
		if(strtolower($value) == 'shop_manager'){
			$admin = 1;
		}
  }
}

if((isset($u_id) AND $u_id != 0) AND $admin == 1){


	if(isset($_GET['so'])){
	  $order_id = $_GET['so'];
	}else{
	  $order_id = '';
	}

	$order = wc_get_order($order_id);
	if(isset($order) AND !empty($order)){ // jika ada

		$shipping_total = 0;
		$total_harga_final = 0;
	    $courier_resi = '';
	    global $wpdb;
	    $query = "SELECT *
	              FROM ldr_orders
	              WHERE order_id = '$order_id' 
	              ";
	    $res_query = $wpdb->get_results($query, OBJECT);
	    $res_count = count($res_query);
	    if ($res_count > 0){
	        foreach ($res_query as $key => $value){
	            $courier_name = $value->courier_name;
	            $courier_resi = $value->courier_resi;
	            $shipping_total = $value->shipping_price;
	            $voucher_id = $value->voucher_id;
	            $address_id = $value->address_id;
	            $notes = $value->notes;
	            $mile_poin = $value->mile_poin;
	        }
	    }

	    $user_id = $order->user_id;
	    $user_data = get_userdata( $user_id );
	    $user_name = $user_data->data->display_name;
	    $order_status  = $order->get_status();
	    $total_payment = $order->get_total();

	    $get_Data = $order->get_data();  
	    $billing = $get_Data['billing'];
	    $email = $billing['email'];
	    $country_code = $billing['country'];

	    $countries_obj   = new WC_Countries();
	    $allcountries = $countries_obj->countries;
	    $country = $allcountries[$country_code];

	    $order_date = get_the_date('Y-m-d | h:i:s', $order_id);
	    $so_date = get_the_date('d F Y', $order_id);
	    $soon_date = date("d F Y", strtotime(get_the_date('Y-m-d', $order_id) . " +1 day"));


	    $nama_toko = 'Ladara Indonesia';
	    $telepon_toko = '62-8xxxxxxx';

        global $wpdb;
        $queryOrder = "SELECT *
                  FROM ldr_orders_item
                  WHERE order_id = '$order_id' 
                  ";
        $respOrder = $wpdb->get_results($queryOrder, OBJECT);
        $productId = $respOrder[0]->product_id ?? 0;
        $nama_toko = '';
        if ($productId){
            $sellerData = get_field('seller_name',$productId);
            $nama_toko = $sellerData->post_title;
        }

	    $wpdb_query = "SELECT * FROM ldr_orders_address WHERE  order_id = '$order_id' ";
	    $res_query = $wpdb->get_results($wpdb_query, OBJECT);
	    $count_res = count($res_query);
	    if($count_res > 0){ // if not exist with same user_id
	    	foreach ($res_query as $key => $value) {
	    		$telepon_toko = $value->telepon_toko;
				$first_name = $value->nama_penerima;
				$address_1 = $value->alamat;
				$provinsi = $value->provinsi;
				$kota = $value->kota;
				$kecamatan = $value->kecamatan;
				$kode_pos = $value->kode_pos;
				$longitude = $value->longitude;
				$latitude = $value->latitude;
				$phone = $value->telepon_penerima;
	    	}
	    	
		}
?>

	<div class="row row_printDefault">
		<div class="col-print-12 col_printDefault" style="font-weight: 500;padding-left: 30px;padding-right: 30px;">
			
			<div class="row row_in_printDef">
				<div class="col-print-6">
					<div class="bx_print_addr">
						<img class="mg_headprint" style="" src="<?php bloginfo('template_directory'); ?>/library/images/main_logo.png">

						<div class="tt_printDef">Penjual / Pengirim:</div>
						<div class="box_printDef">Nama Toko: <b><?php echo $nama_toko; ?></b></div>
						<?php /*
						<div class="box_printDef">Telepon: <?php echo $telepon_toko; ?></div>
						*/ ?>
					</div>

					<div class="bx_print_addr">
						<div class="tt_printDef">Pembeli / Tujuan :</div>
						<div class="box_printDef"><b><?php echo $first_name; ?></b></div>
						<div class="box_printDef">
	                        <?php echo $address_1; ?> <br/>
	                        <?php echo  $kota.', '.$kecamatan; ?> <br/>
	                        <?php echo $provinsi.'. '.$postcode; ?>
						</div>
						<div class="box_printDef">Telepon: <?php echo $phone; ?></div>
					</div>
				</div>
				<div class="col-print-6">
					<div class="ht_printDefault">Purchase Order</div>

					<div class="tt_printDef">Nomor Order: #<?php echo $order_id; ?></div>
					<br/>

					<div class="tt_printDef">Detail Pembayaran:</div>
					<div class="box_printDef"><b>Tanggal Pembelian:</b> <?php echo $so_date; ?></div>
					<div class="box_printDef"><b>Tanggal Jatuh Tempo:</b> <?php echo $soon_date; ?></div>
				</div>
			</div>

			<table class="table table_print" border="2" style="border: 2px solid #000;">
				<thead>
					<tr>
						<th scope="col">No.</th>
						<th scope="col">SKU</th>
						<th scope="col">Produk</th>
						<th scope="col">Variasi</th>
						<th scope="col">Jumlah</th>
						<th scope="col">Harga</th>
					</tr>
				</thead>
				<tbody>
					<?php
						$total_berat = 0;
	                    $save_stock = array();
	                    $op = 0;
	                    $total_pcs = 0;

	                    $res_count = count($respOrder);
	                    if ($res_count > 0){
	                        foreach ($respOrder as $key => $value){
	                        	$op++;
	                            $product_id = $value->product_id;
	                            $product = wc_get_product( $product_id );

	                            $product_name = get_the_title($product_id);
	                            $db_stock_id = $value->stock_id;
	                            $product_qty = $value->qty;
	                            $total_pcs = (int)$total_pcs+(int)$product_qty;
	                            $product_price = $value->price;
	                            
	                            $product_variasi = $value->variasi;
	                            $product_weight = $product->get_weight();
	                            $product_sku = $product->get_sku();
	                            $total_berat = (int)$total_berat+(int)$product_weight;
	                            $total_price = (int)$product_price*(int)$product_qty;

	                            $total_harga_final = (int)$total_harga_final+(int)$total_price;
					 ?>
								<tr>
									<td scope="row"><?php echo $op; ?></td>
									<td><?php echo $product_sku; ?></td>
									<td><?php echo $product_name; ?></td>
									<td><?php echo $product_variasi; ?></td>
									<td><?php echo $product_qty; ?></td>
									<td>Rp <?php echo number_format($total_price); ?></td>
								</tr>
					<?php 
		                    }
		                }
	                ?>
				</tbody>

				<?php
					$total_pay_price = (int)$total_harga_final+(int)$shipping_total;
				?>
				<tfoot>
					<tr>
						<td scope="row"></td>
						<td colspan="4" style="text-align: right;">Ongkos Kirim</td>
						<td>Rp <?php echo number_format($shipping_total); ?></td>
					</tr>
					<tr>
						<td scope="row"></td>
						<td colspan="4" style="text-align: right;"><b>Total Harga</b></td>
						<td><b>Rp <?php echo number_format($total_pay_price); ?></b></td>
					</tr>
				</tfoot>

			</table>

			<div class="bx_right_table">
				Total Pcs : <span><?php echo $total_pcs; ?> pcs.</span>
			</div>

			<table class="table_print" border="2" style="border: 2px solid #000;">
				<tr>
					<td class="ttd_print">administrasi</td>
					<td class="ttd_print">pengeluaran barang</td>
					<td class="ttd_print">kurir</td>
					<td class="ttd_print">penerima</td>
				</tr>
			</table>

			<div class="info_print">
				*klaim atas kekurangan barang hanya bisa kami terima pada saat serah terima barang, dan disaksikan oleh pengantar dan di catat dalam surat jalan.
			</div>

		</div>
	</div>

	<?php } ?>

<?php }else{ ?>
    <script>
        // 'Getting' data-attributes using getAttribute
        var plant = document.getElementById('body');
        console.log(plant);
        var hurl = plant.getAttribute('data-hurl'); 
        location.replace(hurl+'/login/'); 
    </script>
<?php } ?>

<?php endwhile; ?>
<?php else : ?>
		<?php get_template_part( 'content', '404pages' ); ?>	
<?php endif; ?>
<?php // get_footer(); ?>