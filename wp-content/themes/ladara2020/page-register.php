<?php
/*
Template Name: Register Page
*/
?>

<?php get_header(); ?>
<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
<?php 
$current_user = wp_get_current_user();
$u_id = $current_user->ID;

if(isset($_SESSION['rfr']) AND $_SESSION['rfr'] != ''){
	$referer = $_SESSION['rfr'];
}else{

	$rfr = wp_get_referer();
	if(isset($rfr) AND $rfr != ''){
		$referer = wp_get_referer();
		$_SESSION['rfr'] = $referer;
	}else{
		$referer = home_url();
	}

}

?>
<style type="text/css">
#wp-submit{
	background: #0080FF;
}
</style>
<div class="row"></div>
<div class="row row_login row_register">
	<div class="col-md-12 col_login">
		<div class="box_login">

			<?php 
				if(isset($u_id) AND $u_id != ''){

					unset($_SESSION['rfr']); // unset refferer
			?>
				<div class="ok_login">
					<span class="glyphicon glyphicon-ok ok_icon"></span>
					<div class="ht_okLogin">Selamat datang di Ladara</div>
					<span>Mohon menunggu...</span>
					<img src="<?php bloginfo('template_directory'); ?>/library/images/loading.gif">
				</div>

				<div id="redirect_ok" data-hurl="<?php echo home_url(); ?>"></div>
				<script>
					var plant = document.getElementById('redirect_ok');
					var hurl = plant.getAttribute('data-hurl'); 
				    setTimeout(function(){
			          location.replace(hurl); 
			        }, 2000);
				</script>
			<?php
				}else{

					// ================ custom Facebook auth ================
					require_once 'vendor-fb/autoload.php';
					// session_start();

					$fb = new Facebook\Facebook([
                        'app_id' => getenv('FACEBOOK_APP_ID'),
                        'app_secret' => getenv('FACEBOOK_APP_SECRET'),
                        'default_graph_version' => getenv('FACEBOOK_DEFAULT_GRAPH_VERSION')
                    ]);

					$helper = $fb->getRedirectLoginHelper();

					if (isset($helper)) {

                        $permissions = ['email']; // Optional permissions
                        $callbackUrl = getenv('FACEBOOK_CALLBACK_URL');
						$loginUrl_fb = $helper->getLoginUrl($callbackUrl, $permissions);

						if(isset($_SESSION['fblog']) AND $_SESSION['fblog'] == $loginUrl_fb){
							session_destroy();
						}else{
							$_SESSION['fblog'] = $loginUrl_fb;
						}
					}
					// ================ custom Facebook auth ================
					// ================ custom Google auth ================
					include('content-function-login.php'); 
					$login_button = '';

					if(!isset($_SESSION['access_token'])){
						$url_googleLogin = $google_client->createAuthUrl();
					}else{
						$url_googleLogin = '#';

						// ================ custom logout social media ================
						//Reset OAuth access token
						$google_client->revokeToken();
						//Destroy entire session data.
						session_destroy();
						// ================ custom logout social media ================
					}
					// ================ custom Google auth ================

			?>
					<h1 class="ht_register">Daftar Akun Baru</h1>
					<div class="a_bergabung tx_info_register">
					    Sudah punya akun Ladara? <a href="<?php echo home_url(); ?>/login/" title="Daftar Sekarang!">Masuk di sini</a>
				    </div>

				    <div id="suc_register" class="alert alert-success h_err" role="alert">
					  <b>Registrasi sukses!</b> Silahkan periksa email Anda, dan segera aktifasi.
					</div>

					<div id="fail_register" class="alert alert-danger h_err" role="alert">
					  <b>Gagal!</b> Sepertinya email Anda sudah terdaftar, coba gunakan yang lain yuk!
					</div>

				    <form id="formRegister" method="post" autocomplete="nope" action="" class="wp-user-form formRegister" onsubmit="submit_newRegister(event);">
				        <input autocomplete="nope" name="hidden" type="text" style="display:none;">

						<?php 
							if(isset($_GET['login']) AND $_GET['login'] == 'failed'){
						?>
							<div class="alert alert-danger">
							  <strong>Maaf,</strong> email atau password salah. Silahkan coba kembali.
							</div>
						<?php
							}
						?>
					        <div class="f_ad_input">
					       
					        	<input type="email" ng-model="username" name="u_email" id="user_login" class="ad_input" placeholder="Masukkan Email" value="" required="required"  maxlength="50" />
					     
					        	<div id="err_email" class="f_err">*Masukkan Email</div>
					        </div>

					        <div class="f_ad_input">
					        
					        	<input type="password" name="u_password" id="user_pass" class="ad_input" placeholder="Masukkan kata sandi" value="" required="required" autocomplete="nope" minlength="5"/>
					        	<span id="open_password" class="glyphicon glyphicon-eye-close open_password" title="Lihat/sembunyikan password."></span>
			
					        	<div id="err_pwd" class="f_err">*Masukkan Kata Sandi</div>
					        </div>

					        <div class="f_ad_input">
					        
					        	<input type="password" name="re_password" id="user_pass2" class="ad_input" placeholder="Masukkan kata sandi" value="" required="required" autocomplete="nope" minlength="5"/>
					        	<span id="open_password2" class="glyphicon glyphicon-eye-close open_password" title="Lihat/sembunyikan password."></span>
			
					        	<div id="err_repwd" class="f_err">*Konfirmasi Kata Sandi tidak sama</div>
					        </div>

					        <div class="f_ad_input">
								<input type="text" id="iput_name" name="u_nama" class="ad_input" minlength="3" placeholder="Masukkan Nama Lengkap" required="required">
								<div id="err_nama" class="f_err">*Silahkan isi nama Anda</div>
							</div>
					        
					        <div class="f_errLogin h_errLogin"></div>
					    
					        <div class="login_fields">
					            <input type="submit" name="user-submit" id="wp-submit" class="sub_login" value="Daftar"/>
					            <?php wp_nonce_field( 'NWRGST' ); ?>
					        </div>
				    </form>

				    <div class="info_login">atau masuk dengan</div>


				    <div class="row row_lg_reg">
				    	<div class="col-md-6 col_lg_reg">
				    		<a href="<?php echo $loginUrl_fb; ?>" title="Login dengan akun facebook.">
					    		<div id="sub_facebook" class="btn_logSocial login_facebook">
							    	<img src="<?php bloginfo('template_directory'); ?>/library/images/icon_fb.svg"> Facebook
							    </div>
							</a>
				    	</div>
				    	<div class="col-md-6 col_lg_reg">
				    		<a href="<?php echo $url_googleLogin; ?>" title="Login dengan akun google.">
							    <div id="sub_google" class="btn_logSocial login_google">
							    	<img src="<?php bloginfo('template_directory'); ?>/library/images/icon_google.svg"> Google
							    </div>	
							</a>
				    	</div>
				    </div>
				   
				    <div class="a_bergabung info_register">
				    	Dengan mendaftar, Anda setuju dengan <a target="_blank" href="<?php echo home_url(); ?>/syarat-ketentuan/">Syarat dan Ketentuan</a> serta <a target="_blank" href="<?php echo home_url(); ?>/privacy-policy/">Kebijakan Privasi.</a>
					</div>
			<?php
				}
			 ?>
		</div>
	</div>
</div>

<?php // ============= cannot back ============= ?>
<script type="text/javascript">
    (function (global) { 

        if(typeof (global) === "undefined") {
            throw new Error("window is undefined");
        }

        var _hash = "!";
        var noBackPlease = function () {
            global.location.href += "#";

            // making sure we have the fruit available for juice (^__^)
            global.setTimeout(function () {
                global.location.href += "!";
            }, 50);
        };

        global.onhashchange = function () {
            if (global.location.hash !== _hash) {
                global.location.hash = _hash;
            }
        };

        global.onload = function () {            
            noBackPlease();

            // disables backspace on page except on input fields and textarea..
            document.body.onkeydown = function (e) {
                var elm = e.target.nodeName.toLowerCase();
                if (e.which === 8 && (elm !== 'input' && elm  !== 'textarea')) {
                    e.preventDefault();
                }
                // stopping event bubbling up the DOM tree..
                e.stopPropagation();
            };          
        }

    })(window);
</script>

<?php endwhile; ?>
<?php else : ?>
		<?php get_template_part( 'content', '404pages' ); ?>	
<?php endif; ?>
<?php get_footer(); ?>