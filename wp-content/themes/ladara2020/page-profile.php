<?php
/*
Template Name: Profile Page
*/
?>

<?php get_header(); ?>
<?php if (have_posts()) : while (have_posts()) : the_post(); ?>

<?php 
date_default_timezone_set("Asia/Jakarta");
$current_user = wp_get_current_user();
$u_id = $current_user->ID;
$user_login = $current_user->user_login;

$user_avatar = get_field('user_avatar', 'user_'.$u_id);
if(isset($user_avatar) AND $user_avatar != ''){
	$linkpoto = wp_get_attachment_image_src($user_avatar,'thumbnail');
	$url_avatar = $linkpoto[0];
}else{
	$url_avatar = get_template_directory_uri().'/library/images/icon_profile.png';
}

$nama_lengkap = get_field('nama_lengkap', 'user_'.$u_id);
$tanggal_lahir = get_field('tanggal_lahir', 'user_'.$u_id);
if(isset($tanggal_lahir)){
	$dob_data = explode('-', $tanggal_lahir);
	$dob_year = $dob_data[0];
	$dob_month = $dob_data[1];
	$dob_day = $dob_data[2];
}
$telepon = get_field('telepon', 'user_'.$u_id);
$jenis_kelamin = get_field('jenis_kelamin', 'user_'.$u_id);

if(isset($u_id) AND $u_id != 0){
?>

<style type="text/css">
#wp-submit{
	background: #0080FF;
}
</style>

<div class="row"></div>
<div id="profilpage" class="row row_profile">
	<div class="col-md-3 col_profile des_display">
		<?php get_template_part( 'content', 'menu-profile' ); ?>
	</div>
	<div class="col-md-9 col_profile">
		<div class="row row_cont_tab_profile">
			<h1 class="ht_profile">Data Profil</h1>
			<div class="bx_f_editProfile">
				<form id="form_saveprofile" class="form_saveprofile" action="" onsubmit="submit_saveProfile(event);" action="" method="post" enctype="multipart/form-data">
					<input type="hidden" name="u_id" value="<?php echo $u_id; ?>">
					<div class="f_avatar">
						<div class="mg_f_avatar" id="thumb-output">
							<img src="<?php echo $url_avatar; ?>">
						</div>
						<a class="a_uploadphoto browse" id="">Pilih Gambar</a>
						<input type="file" name="file_photo" class="inputfile" id="file-input"/>
						<div class="h_errinfo alert alert-danger"></div>
						<div class="h_sucsinfo alert alert-success"></div>
						<div class="info_f_avatar">
							Ukuran gambar: maksimal 1000kb / 1mb, <br/>Format gambar: .JPG .JPEG .PNG
						</div>
					</div>
					<div class="f_aform">
						<label><b>Email Login </b><span>*</span></label>
						<input type="text" name="email_login" class="txt_aform" required="required" readonly="readonly" value="<?php echo ($user_login ? $user_login : ''); ?>">
						<div id="p1" class="err_aform"></div>
					</div>
					<div class="f_aform">
						<label><b>Nama Lengkap </b><span>*</span></label>
						<input type="text" name="nama_lengkap" class="txt_aform" required="required" value="<?php echo ($nama_lengkap ? $nama_lengkap : ''); ?>">
						<div id="p2" class="err_aform"></div>
					</div>
					<div class="f_aform">
						<label><b>Nomor Telepon </b><span>*</span></label>
						<input type="text" name="nomor_hp" class="txt_aform onlyphone" required="required" value="<?php echo ($telepon ? $telepon : ''); ?>">
						<div id="p3" class="err_aform"></div>
					</div>
					<div class="f_aform">
						<label><b>Tanggal Lahir </b><span>*</span></label>
						<div class="bx_dob_aform">
							<select name="dob_day" class="sel_aform sel_dob_1" required="required">
								<?php 
									for ($i=1; $i <= 31; $i++) { 
										if($i == $dob_day){
										?>
											<option value="<?php echo $i; ?>" selected><?php echo $i; ?></option>
										<?php
										}else{
										?>
											<option value="<?php echo $i; ?>"><?php echo $i; ?></option>
										<?php
										}
									} 
								?>
							</select>
							<?php 	
								$all_month = array();
								$all_month[1] = 'Januari';
								$all_month[2] = 'Februari';
								$all_month[3] = 'Maret';
								$all_month[4] = 'April';
								$all_month[5] = 'Mei';
								$all_month[6] = 'Juni';
								$all_month[7] = 'Juli';
								$all_month[8] = 'Agustus';
								$all_month[9] = 'September';
								$all_month[10] = 'Oktober';
								$all_month[11] = 'November';
								$all_month[12] = 'Desember';
							?>
							<select name="dob_month" class="sel_aform sel_dob_2" required="required">
								<?php 
									foreach ($all_month as $key => $value) {
										if($key == $dob_month){
										?>
											<option value="<?php echo $key; ?>" selected><?php echo $value; ?></option>
										<?php
										}else{
										?>
											<option value="<?php echo $key; ?>"><?php echo $value; ?></option>
										<?php
										}
									}
								?>
							</select>
							<select name="dob_year" class="sel_aform sel_dob_3" required="required">
								<?php 
									for ($i=2010; $i >= 1900; $i--) { 
										if($i == $dob_year){
										?>
											<option value="<?php echo $i; ?>" selected><?php echo $i; ?></option>
										<?php
										}else{
										?>
											<option value="<?php echo $i; ?>"><?php echo $i; ?></option>
										<?php
										}
							 		} 
							 	?>
							</select>
						</div>
						<div id="p4" class="err_aform"></div>
					</div>
					<div class="f_aform inline_gender">
						<label><b>Jenis Kelamin </b><span>*</span></label>
						<div class="bx_rad_gender">
							<?php if(strtolower($jenis_kelamin) == 'pria'){
							?>
									<input type="radio" name="gender" class="rad_aform" id="gender_pria" value="pria" checked="checked">
							<?php
								}else{
							?>
									<input type="radio" name="gender" class="rad_aform" id="gender_pria" value="pria">
							<?php } ?>
							<label for="gender_pria">Pria</label>
						</div>
						<div class="bx_rad_gender">
							<?php if(strtolower($jenis_kelamin) == 'wanita'){
							?>
									<input type="radio" name="gender" class="rad_aform" id="gender_wanita" value="wanita" checked="checked">
							<?php
								}else{
							?>
									<input type="radio" name="gender" class="rad_aform" id="gender_wanita" value="wanita">
							<?php } ?>
							<label for="gender_wanita">Wanita</label>
						</div>
						<div id="p5" class="err_aform"></div>
					</div>
					<div class="f_aform">
						<div class="err_info alert-success"><b>Selamat!</b> profil Anda telah di perbaruhi.</div>
						<input type="submit" id="wp-submit" class="sub_aform" value="Simpan Profil">
					</div>
				</form>
			</div>
		</div>
	</div>
</div>
<script>
if ( window.history.replaceState ) {
  window.history.replaceState( null, null, window.location.href );
}
</script>
<?php }else{ ?>
    <script>
        // 'Getting' data-attributes using getAttribute
        var plant = document.getElementById('body');
        console.log(plant);
        var hurl = plant.getAttribute('data-hurl'); 
        location.replace(hurl+'/login/'); 
    </script>
<?php } ?>
<?php endwhile; ?>
<?php else : ?>
		<?php get_template_part( 'content', '404pages' ); ?>	
<?php endif; ?>
<?php get_footer(); ?>