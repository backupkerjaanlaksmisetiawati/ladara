<?php
/*
Template Name: Daftar Brand Page
*/
// use Ladara\Models\Notifications;

get_header();
if (have_posts()) : while (have_posts()) : the_post();

date_default_timezone_set("Asia/Jakarta");
$nowdate = date('Y-m-d H:i:s');
$d_now = date('d');

?>

<article id="post-<?php the_ID(); ?>" <?php post_class('clearfix'); ?> role="article" itemscope itemtype="http://schema.org/BlogPosting">
	
    <div class="row row_globalPage">
        <div class="col-md-12">
            
            <a href="<?php echo home_url(); ?>">
                <div class="bx_backShop">
                    <span class="glyphicon glyphicon-menu-left"></span> Kembali ke Home
                </div>
            </a>

            <h1 class="ht_globalPage_notif"><?php echo get_the_title($post->ID); ?></h1>

            <div class="bx_chcat_2 bx_brandPage">
                    <?php
                        $op = 1;
                        $args = array(
                            'post_type' => 'brand',
                            'posts_per_page' => -1,
                            'post_status' => 'publish',
                            'orderby' => 'title',
                            'order' => 'ASC',
                            'with_product' => true
                        );
                        $the_query = new WP_Query($args);
                        while ($the_query->have_posts()) : $the_query->the_post();
                        global $product;
                        $post_id = get_the_ID();
                        $post_title = get_the_title($post_id);
                        $post_link = get_permalink($post_id);

                        $linkpoto = wp_get_attachment_image_src(get_post_thumbnail_id($post_id), 'thumbnail');
                        if ($linkpoto) {
                            $urlphoto = $linkpoto['0'];
                        } else {
                            global $wpdb;
                            $getImage = $wpdb->get_row("SELECT * FROM ldr_posts where post_parent = $post_id");
                            if($getImage){
                                $urlphoto = str_replace(get_site_url(), IMAGE_URL,  $getImage->guid);
                            }else{
                                $urlphoto = get_template_directory_uri().'/library/images/default_sorry.jpg';
                            }
                        }
                        $alt = $post_title;

                        if (($op - 1) % 6 === 0) {
                            ?> <div class="row"> <?php
                        }

                        ?>
                            <div class="col-xs-4 col-md-2 col_bx_choosenCat col_branditems">
                                <a href="<?php echo $post_link.'-'.$post_id; ?>" title="Lihat detail brand ini.">
                                    <div class="mg_choosenCat_brand">
                                        <img class="lazy" data-src="<?php echo $urlphoto; ?>" title="<?php echo $post_title; ?>">
                                    </div>
                                    <h4 class="tx_choosenCat"><?php echo $post_title; ?></h4>
                                </a>
                            </div>
                        <?php

                        if ($op % 6 === 0) {
                            ?> </div> <?php
                        }

                        $op++;
                        endwhile;
                        wp_reset_query();
                    ?>
            </div>

     
        </div>
    </div>

</article>

<?php endwhile; ?>
<?php else : ?>
		<?php get_template_part('content', '404pages'); ?>	
<?php endif; ?>
<?php get_footer(); ?>