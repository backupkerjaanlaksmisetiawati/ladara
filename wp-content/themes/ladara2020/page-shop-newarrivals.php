<?php
/*
Template Name: Shop New Arrivals
*/
?>

<?php get_header(); ?>
<?php if (have_posts()) : while (have_posts()) : the_post(); ?>

<?php 
date_default_timezone_set("Asia/Jakarta");
$nowdate = date('Y-m-d');
$now_time = strtotime($nowdate);

$current_user = wp_get_current_user();
$u_id = $current_user->ID;

if(isset($_GET['keyword'])){
	$keyword = $_GET['keyword'];
}else{
	$keyword = '';
}

$offset = 0;
$limit_item = 30;
if(isset($_GET['pg'])){
	$page = $_GET['pg'];
	$page_now = (int)$page-1;
	$offset = (int)$page_now*(int)$limit_item;
}else{
	$page = 1;
	$offset = (int)$page*0;
}

global $wp;
$cur_page = home_url( $wp->request );

?>

<input type="hidden" name="keyword_s" value="<?php echo $keyword; ?>">
<input type="hidden" name="offset" value="<?php echo $offset; ?>">
<input type="hidden" name="limit" value="<?php echo $limit_item; ?>">
<input type="hidden" name="pagination" value="<?php echo $page; ?>">
<input type="hidden" name="page_url" value="<?php echo $cur_page; ?>">

<div id="shop_newarrivals" class="row row_profile" data-url="<?php echo home_url(); ?>">
	<div class="col-md-3 col_profile">
		
		<?php get_template_part( 'content', 'filter-shop' ); ?>

	</div>
	<div class="col-md-9 col_product_shoplist">
			
		<div class="mg_banner_shoplist">
			<img src="<?php bloginfo('template_directory'); ?>/library/images/banner_product.jpg">
			<div class="bx_fly_banner">
				<div class="bor_shoplist"></div>
				<div class="ht_banner_shoplist">Produk Terbaru</div>
				<div class="info_banner_shoplist">Selalu periksa stok produk terbaru sebelum mulai membeli / menjual.</div>
			</div>
		</div>

		<div class="box_head_shoplist">	
			<div class="ht_head_sortby pull_right">
				<div class="label">Urutkan: </div>
				<select name="filter_sortby" class="sel_sortby">
					<option value="terkait">Terkait</option>
					<option value="terkait">Terlaris</option>
					<option value="terkait">Terbaru</option>
				</select>
			</div>
		</div>

	
		<div id="view_resProduct" class="row">

		</div>

		<div class="row">
			<div class="col-md-12 col_pagination_shop view_pagi_shops">
				<?php // view pagi shops my ajax ?>
			</div>
		</div>
		
	</div>
</div>



<?php endwhile; ?>
<?php else : ?>
		<?php get_template_part( 'content', '404pages' ); ?>	
<?php endif; ?>
<?php get_footer(); ?>