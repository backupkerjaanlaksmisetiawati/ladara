<?php
/*
Template Name: Profile Security
*/
?>

<?php get_header(); ?>
<?php if (have_posts()) : while (have_posts()) : the_post(); ?>

<?php 
$current_user = wp_get_current_user();
$u_id = $current_user->ID;
$user_login = $current_user->user_login;

if(isset($u_id) AND $u_id != 0){

?>
<style type="text/css">
#wp-submit{
	background: #0080FF;
}
</style>

<div class="row"></div>
<div id="securitypage"  class="row row_profile">
	<div class="col-md-3 col_profile des_display">

		<?php get_template_part( 'content', 'menu-profile' ); ?>
		
	</div>
	<div class="col-md-9 col_profile">
		
		<div class="row row_cont_tab_profile">
			<h1 class="ht_profile">Ubah Password</h1>

			<div class="bx_f_editProfile">
				<form id="form_changepass" class="form_changepass" action="" onsubmit="submit_changepass(event);" method="post">
				<input type="hidden" name="u_id" value="<?php echo $u_id; ?>">
				<input type="hidden" name="login" value="<?php echo $user_login; ?>">
					
					<div class="f_aform">
						<label>Password Lama <span>*</span></label>
						<input type="password" name="pass_lama" class="txt_aform" required="required" value="" pattern=".{5,}" >
						<div id="p1" class="err_aform"></div>
					</div>

					<div class="f_aform">
						<label>Password Baru <span>*</span></label>
						<input type="password" name="pass_baru" class="txt_aform" required="required" value="" pattern=".{5,}" title="5 characters minimum">
						<div id="p2" class="err_aform"></div>
					</div>

					<div class="f_aform">
						<label>Konfirmasi Password Baru <span>*</span></label>
						<input type="password" name="conf_pass_baru" class="txt_aform" required="required" value="" pattern=".{5,}" title="5 characters minimum">
						<div id="p3" class="err_aform"></div>
					</div>

					<div class="f_aform">
						<div class="err_info alert-success"><b>Selamat!</b> password Anda telah di perbaruhi.</div>
						<input type="submit" id="wp-submit" class="sub_aform" value="Simpan Password">
					</div>

				</form>
			</div>

		</div>


	</div>
</div>

<?php }else{ ?>
    <script>
        // 'Getting' data-attributes using getAttribute
        var plant = document.getElementById('body');
        console.log(plant);
        var hurl = plant.getAttribute('data-hurl'); 
        location.replace(hurl+'/login/'); 
    </script>
<?php } ?>

<?php endwhile; ?>
<?php else : ?>
		<?php get_template_part( 'content', '404pages' ); ?>	
<?php endif; ?>
<?php get_footer(); ?>