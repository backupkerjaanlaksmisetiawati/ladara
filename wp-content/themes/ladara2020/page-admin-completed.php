<?php
/*
Template Name: Completed Order Page
*/
?>

<?php get_header('admin'); ?>

<?php if (have_posts()) : while (have_posts()) : the_post(); ?>

<?php 
date_default_timezone_set("Asia/Jakarta");
$nowdate = date('Y-m-d H:i:s');

$current_user = wp_get_current_user();
$u_id = $current_user->ID;
$u_roles = $current_user->roles;
$admin = 0;

if(isset($u_roles) AND !empty($u_roles)){
  foreach ($u_roles as $key => $value) {
      if(strtolower($value) == 'administrator'){
          $admin = 1;
      }
      if(strtolower($value) == 'shop_manager'){
          $admin = 1;
      }
  }
}

if((isset($u_id) OR $u_id != 0) AND $admin == 1){

?>

  <!-- Begin Page Content -->
  <div class="container-fluid">

    <!-- Page Heading -->
    <h1 class="h3 mb-2 text-gray-800">Completed Data Order</h1>

    <!-- DataTales Example -->
    <div class="card shadow mb-4">
      <div class="card-header py-3">
        <h6 class="m-0 font-weight-bold text-primary">Ondelivery & Completed</h6>
      </div>
      <div class="card-body">
        <div class="table-responsive">
          <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
            <thead>
              <tr>
                <th>Order ID</th>
                <th>Customer (user ID)</th>
                <th>Order Date</th>
                <th>Total</th>
                <th>Status</th>
                <th>Kurir & Resi</th>
                <th>Confirm</th>
              </tr>
            </thead>
            <tfoot>
              <tr>
                <th>Order ID</th>
                <th>Customer (user ID)</th>
                <th>Order Date</th>
                <th>Total</th>
                <th>Status</th>
                <th>Kurir & Resi</th>
                <th>Confirm</th>
              </tr>
            </tfoot>
            <tbody>

            <?php 
            $order_statuses = array(  
                                      'wc-ondelivery',
                                      'wc-completed'
                                  ); 
            $a = 0;
            // $all_order_status = $order_statuses;
            $all_order_status = join("','",$order_statuses); 
            global $wpdb;
            $query = "SELECT id, post_date, post_status 
                      FROM ldr_posts
                      WHERE post_type = 'shop_order' 
                      AND post_status IN('$all_order_status')
                      ORDER BY id DESC ";
            $res_query = $wpdb->get_results($query, OBJECT);
            $res_count = count($res_query);

            if ($res_count > 0){
                foreach ($res_query as $key => $value) {
                    
                    $order_id = $value->id;
                    $a++;
                    $order = wc_get_order($order_id);
                    $user_id = $order->user_id;
                    $user_data = get_userdata( $user_id );
                    $user_name = $user_data->data->display_name;
                    $order_status  = $order->get_status();
                    $total_payment = $order->get_total();

                    $get_Data = $order->get_data();  
                    $billing = $get_Data['billing'];
                    $first_name = $billing['first_name'];
                    $address_1 = $billing['address_1'];
                    $address_2 = $billing['address_2'];
                    $city = $billing['city'];
                    $state = $billing['state'];
                    $postcode = $billing['postcode'];
                    $country = $billing['country'];
                    $email = $billing['email'];
                    $phone = $billing['phone'];

                    $order_date = get_the_date('Y-m-d | h:i:s', $order_id);
                    $yearnow = date('Y',strtotime($order_date));
                    $monthnow = date('m',strtotime($order_date));
                    $daynow = date('d',strtotime($order_date));


                    $courier_name = '';
                    $courier_resi = '';
                    global $wpdb;
                    $query = "SELECT *
                              FROM ldr_orders
                              WHERE order_id = '$order_id' 
                              ";
                    $res_query = $wpdb->get_results($query, OBJECT);
                    $res_count = count($res_query);
                    if ($res_count > 0){
                        foreach ($res_query as $key => $value){
                            $courier_name = $value->courier_name;
                            $courier_resi = $value->courier_resi;
                        }
                    }


            ?>
                        <tr>
                          <td>
                            <a href="<?php echo home_url(); ?>/dashboard/warehouse/detail/?so=<?php echo $order_id; ?>">
                            <?php echo $order_id; ?>   
                            </a> 
                          </td>
                          <td><?php echo $user_name.' ( '.$user_id.' )'; ?></td>
                          <td><?php echo $order_date; ?></td>
                          <td><?php echo number_format($total_payment); ?></td>

                          <?php if(strtolower($order_status) == 'completed'){ ?>
                            <td style="color: green;"><?php echo $order_status; ?></td>
                          <?php }else{ ?>
                            <td ><b><?php echo $order_status; ?></b></td>
                          <?php } ?>
                          <td>
                              <?php echo $courier_name; ?><br/>
                              <?php echo $courier_resi; ?>
                          </td>
                          <td>
                            <?php if(strtolower($order_status) == 'ondelivery'){ ?>
                                <input type="button" class="btn_ca_confirm btn_ca_ondelivery btnc_<?php echo $order_id; ?>" data-id="<?php echo $order_id; ?>" value="Selesai">
                            <?php }else{ ?>
                                <a href="<?php echo home_url(); ?>/dashboard/warehouse/detail/?so=<?php echo $order_id; ?>">
                                  Lihat detail
                                </a>
                            <?php } ?>
                          </td>
                        </tr>            
            <?php 
                }
            }
            ?>

            </tbody>
          </table>
        </div>
      </div>
    </div>

  </div>
  <!-- /.container-fluid -->

<?php }else{ ?>
    <script>
        // 'Getting' data-attributes using getAttribute
        var plant = document.getElementById('body');
        console.log(plant);
        var hurl = plant.getAttribute('data-hurl'); 
        location.replace(hurl+'/login/'); 
    </script>
<?php } ?>

<?php endwhile; ?>
<?php else : ?>
    <?php get_template_part( 'content', '404pages' ); ?>  
<?php endif; ?>
<?php get_footer('admin'); ?>