<?php
/*
Template Name: Home Page
*/
?>
<?php
$current_user = wp_get_current_user();
$u_id = $current_user->ID;
$u_roles = $current_user->roles;

if (isset($u_roles) and !empty($u_roles)) {
    if ($u_roles[0] == 'shop_manager') {
        header('Location: ' . get_home_url() . '/dashboard/finance/');
        exit();
    }
}

// ==== remove session checkout, shop ====
unset($_SESSION['minrange']);
unset($_SESSION['maxrange']);
unset($_SESSION['by']);
unset($_SESSION['category']);
// ==== remove session checkout, shop ====

?>
<?php get_header(); ?>
<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
        <div class="row"></div>
        <div class="row row_homeBanner">
            <div class="col-md-12 col_homeBanner">

                <div id="slide_homeBanner_desktop" class="owl-carousel owl-theme des_display">
                    <?php
                    $args = array(
                        'post_type' => 'banner',
                        'posts_per_page' => 8,
                        'post_status' => 'publish',
                        'orderby' => 'date',
                        'order' => 'DESC',
                    );
                    $the_query = new WP_Query($args);
                    while ($the_query->have_posts()) : $the_query->the_post();
                        global $wpdb;

                        $post_id = get_the_ID();
                        $post_title = get_the_title($post_id);
                        $post_link = get_permalink($post_id);

                        $linkpoto = wp_get_attachment_image_src(get_post_thumbnail_id($post_id), 'full');
                        if ($linkpoto) {
                            $urlphoto = $linkpoto['0'];
                        } else {
                            $urlphoto = get_template_directory_uri() . '/library/images/default_banner.jpg';
                        }

                        $alt = $post_title;
                    ?>
                        <div class="item">
                            <a href="<?php echo $post_link; ?>" title="<?php echo $post_title; ?>">
                                <div class="mg_slider_homeBanner">
                                    <img class="" src="<?php echo $urlphoto; ?>" alt="<?php echo $alt; ?>" title="Dapatkan penawaran menarik ini sekarang juga!">
                                </div>
                            </a>
                        </div>
                    <?php
                    endwhile;
                    wp_reset_query();
                    ?>

                </div>
                <div id="slide_homeBanner_mobile" class="owl-carousel owl-theme mob_display">

                    <?php
                    $args = array(
                        'post_type' => 'banner',
                        'posts_per_page' => 8,
                        'post_status' => 'publish',
                        'orderby' => 'date',
                        'order' => 'DESC',
                    );
                    $the_query = new WP_Query($args);
                    while ($the_query->have_posts()) : $the_query->the_post();
                        global $wpdb;

                        $post_id = get_the_ID();
                        $post_title = get_the_title($post_id);
                        $post_link = get_permalink($post_id);

                        $bannerMobileId = get_post_meta($post_id, 'banner-mobile', true);

                        $linkpoto = wp_get_attachment_url($bannerMobileId);
                        if ($linkpoto) {
                            $urlphoto = $linkpoto;
                        } else {
                            $urlphoto = get_template_directory_uri() . '/library/images/default_banner.jpg';
                        }

                        $alt = $post_title;

                    ?>
                        <div class="item">
                            <a href="<?php echo $post_link; ?>" title="<?php echo $post_title; ?>">
                                <div class="mg_slider_homeBanner">
                                    <img class="" src="<?php echo $urlphoto; ?>" alt="<?php echo $alt; ?>" title="Dapatkan penawaran menarik ini sekarang juga!">
                                </div>
                            </a>
                        </div>
                    <?php
                    endwhile;
                    wp_reset_query();
                    ?>

                </div>
                <div class="slide_banner_arrow">
                    <span class="def_slide_nav slide_PrevBtn"><img src="<?php bloginfo('template_directory'); ?>/library/images/next-1.svg"></span>
                    <span class="def_slide_nav slide_NextBtn"><img src="<?php bloginfo('template_directory'); ?>/library/images/next.svg"></span>
                </div>
            </div>
        </div>
        <div class="row row_homePage">
<!--            <div class="col-md-12 col_choosenCat">-->
<!--                <div class="bx_chcat_1">-->
<!--                    <h2 class="ht_choosenCat">Top up &amp; Tagihan</h2>-->
<!--                    <div class="row_topup">-->
<!--                        <div id="pulsa" class="active">-->
<!--                            <img src="--><?php //bloginfo('template_directory'); ?><!--/library/images/icon-topup-pulsa.svg" />-->
<!--                            <label>Pulsa</label>-->
<!--                        </div>-->
<!--                        <div id="paket-data">-->
<!--                            <img src="--><?php //bloginfo('template_directory'); ?><!--/library/images/icon-topup-paketdata.svg" />-->
<!--                            <label>Paket Data</label>-->
<!--                        </div>-->
<!--                        <div id="gopay">-->
<!--                            <img src="--><?php //bloginfo('template_directory'); ?><!--/library/images/icon-topup-gopay.svg" />-->
<!--                            <label>Top up Gopay</label>-->
<!--                        </div>-->
<!--                        <div id="listrik-pln">-->
<!--                            <img src="--><?php //bloginfo('template_directory'); ?><!--/library/images/icon-topup-pln.svg" />-->
<!--                            <label>Listrik PLN</label>-->
<!--                        </div>-->
<!--                        <div id="pdam">-->
<!--                            <img src="--><?php //bloginfo('template_directory'); ?><!--/library/images/icon-topup-pdam.svg" />-->
<!--                            <label>Air PDAM</label>-->
<!--                        </div>-->
<!--                        --><?php ///*
//                        <div id="bpjs">
/*                            <img src="<?php bloginfo('template_directory'); ?>/library/images/icon-topup-bpjs.svg" />*/
//                            <label>BPJS</label>
//                        </div>*/ ?>
<!--                    </div>-->
<!---->
<!--                    <div class="row_formTopup">-->
<!--                        <form class="formTopup pulsa active">-->
<!--                            <div>-->
<!--                                <label>Nomor Telepon</label>-->
<!--                                <input type="text" class="field" placeholder="08121234567" />-->
<!--                            </div>-->
<!--                            <div>-->
<!--                                <label>Nomor Telepon</label>-->
<!--                                <select class="field">-->
<!--                                    <option>Rp 20.000</option>-->
<!--                                    <option>Rp 50.000</option>-->
<!--                                </select>-->
<!--                            </div>-->
<!--                            <div>-->
<!--                                <label>&nbsp;</label>-->
<!--                                <button type="button">Isi Pulsa</button>-->
<!--                            </div>-->
<!--                        </form>-->
<!---->
<!--                        <form class="formTopup paket-data">-->
<!--                            <div>-->
<!--                                <label>Nomor Telepon</label>-->
<!--                                <input type="text" class="field" placeholder="08121234567" />-->
<!--                            </div>-->
<!--                            <div>-->
<!--                                <label>Nomor Telepon</label>-->
<!--                                <select class="field">-->
<!--                                    <option>Rp 20.000</option>-->
<!--                                    <option>Rp 50.000</option>-->
<!--                                </select>-->
<!--                            </div>-->
<!--                            <div>-->
<!--                                <label>&nbsp;</label>-->
<!--                                <button type="button">Beli Paket Data</button>-->
<!--                            </div>-->
<!--                        </form>-->
<!---->
<!--                        <form class="formTopup gopay">-->
<!--                            <div>-->
<!--                                <label>Nomor Telepon</label>-->
<!--                                <input type="text" class="field" placeholder="08121234567" />-->
<!--                            </div>-->
<!--                            <div>-->
<!--                                <label>Nomor Telepon</label>-->
<!--                                <select class="field">-->
<!--                                    <option>Rp 20.000</option>-->
<!--                                    <option>Rp 50.000</option>-->
<!--                                </select>-->
<!--                            </div>-->
<!--                            <div>-->
<!--                                <label>&nbsp;</label>-->
<!--                                <button type="button">Topup</button>-->
<!--                            </div>-->
<!--                        </form>-->
<!---->
<!--                        <form class="formTopup listrik-pln">-->
<!--                            <h4>Coming Soon</h4>-->
<!--                        </form>-->
<!---->
<!--                        <form class="formTopup pdam">-->
<!--                            <h4>Coming Soon</h4>-->
<!--                        </form>-->
<!--                    </div>-->
<!--                </div>-->
<!--            </div>-->

            <div class="col-md-12 col_choosenCat">
                <?php 
                    $op = 0;
                    $query = 'SELECT * FROM ldr_categories where featured = 1 limit 7';
                    $res_data = $wpdb->get_results($query);
                    $count_res = count($res_data);
                    if($count_res > 0){
                 ?>
                    <div class="bx_chcat_1">
                        <h2 class="ht_choosenCat">Kategori Pilihan</h2>
                        <div class="row row_chsCat">
                            <?php
                            foreach ($res_data as $key => $value) {
                                $op++;
                                $cat_name = $value->name;
                                $cat_url = home_url() . '/product?category=' . $value->slug . '-' . $value->id;
                                if ($value->slug == "emas") {
                                    $cat_url = home_url() . '/emas';
                                }
//                            $urlphoto = home_url() . '/' . $value->image;
                            $urlphoto = IMAGE_URL . '/' . $value->image;
                            $alt = $cat_name;

                                if ($op <= 6) {
                            ?>
                                    <div class="col-xs-3 col-md-2 col_bx_choosenCat">
                                        <a href="<?php echo $cat_url; ?>">
                                            <div class="mg_choosenCat">
                                                <img class="lazy" data-src="<?php echo $urlphoto; ?>" title="<?php echo $alt; ?>">
                                            </div>
                                            <h4 class="tx_choosenCat"><?php echo $cat_name; ?></h4>
                                        </a>
                                    </div>
                                <?php
                                } else {
                                ?>
                                    <div class="col-xs-3 col-md-2 col_bx_choosenCat mob_display">
                                        <a href="<?php echo $cat_url; ?>">
                                            <div class="mg_choosenCat">
                                                <img src="<?php echo $urlphoto; ?>" title="<?php echo $alt; ?>">
                                            </div>
                                            <h4 class="tx_choosenCat"><?php echo $cat_name; ?></h4>
                                        </a>
                                    </div>
                                    <div class="col-xs-3 col-md-2 col_bx_choosenCat mob_display">
                                        <a onclick="document.querySelector('.tx_openMenu').click()">
                                            <div class="mg_choosenCat">
                                                <img class="ico_seeall" src="<?php bloginfo('template_directory'); ?>/library/images/menu.png">
                                            </div>
                                            <h4 class="tx_choosenCat">Lihat semua</h4>
                                        </a>
                                    </div>
                            <?php
                                }
                            }
                            ?>
                        </div>
                    </div>
                <?php 
                    }
                ?>

                <div class="bx_chcat_2">
                    <h2 class="ht_choosenCat">Brands</h2>
                    <div class="row">

                        <?php
                        $op = 0;
                        $args = array(
                            'post_type' => 'brand',
                            'posts_per_page' => 7,
                            'post_status' => 'publish',
                            'orderby' => 'date',
                            'order' => 'DESC',
                            'meta_query' => array(
                                array(
                                    'key' => 'show_brand',
                                    'value' => true,
                                    'compare' => 'EXISTS',
                                ),
                            ),
                        );
                        $the_query = new WP_Query($args);
                        while ($the_query->have_posts()) : $the_query->the_post();
                            $op++;
                            $post_id = get_the_ID();
                            $post_title = get_the_title($post_id);
                            $post_link = get_permalink($post_id) . '-' . $post_id;

                            $linkpoto = wp_get_attachment_image_src(get_post_thumbnail_id($post_id), 'medium');
                            if ($linkpoto) {
                                $urlphoto = $linkpoto['0'];
                            } else {
                                $urlphoto = get_template_directory_uri() . '/library/images/default_sorry.jpg';
                            }
                            $alt = $post_title;

                            if ($op <= 5) {
                        ?>
                                <div class="col-xs-3 col-md-2 col_bx_choosenCat">
                                    <a href="<?php echo $post_link; ?>" title="Lihat detail brand ini.">
                                        <div class="mg_choosenCat_brand">
                                            <img class="lazy" data-src="<?php echo $urlphoto; ?>" title="<?php echo $post_title; ?>">
                                        </div>
                                        <h4 class="tx_choosenCat"><?php echo $post_title; ?></h4>
                                    </a>
                                </div>
                            <?php
                            } else {
                            ?>
                                <div class="col-xs-3 col-md-2 col_bx_choosenCat mob_display">
                                    <a href="<?php echo $post_link; ?>" title="Lihat detail brand ini.">
                                        <div class="mg_choosenCat_brand">
                                            <img class="lazy" data-src="<?php echo $urlphoto; ?>" title="<?php echo $post_title; ?>">
                                        </div>
                                        <h4 class="tx_choosenCat"><?php echo $post_title; ?></h4>
                                    </a>
                                </div>
                        <?php
                            }
                        endwhile;
                        wp_reset_query();
                        ?>

                        <div class="col-xs-3 col-md-2 col_bx_choosenCat">
                            <a href="<?php echo home_url(); ?>/daftar-brand/">
                                <div class="mg_choosenCat_brand">
                                    <img class="lazy" data-class="ico_seeall" src="<?php bloginfo('template_directory'); ?>/library/images/menu.png">
                                </div>
                                <h4 class="tx_choosenCat">Lihat semua</h4>
                            </a>
                        </div>
                    </div>
                </div>
            </div>

            <?php
            $args = array(
                'post_type' => 'promo',
                'posts_per_page' => 4,
                'post_status' => 'publish',
                'orderby' => 'date',
                'order' => 'DESC',
                'meta_query' => array(
                    array(
                        'key' => 'show_promo',
                        'value' => true,
                        'compare' => 'EXISTS',
                    ),
                ),
            );
            $the_query = new WP_Query($args);
            $count_promo = $the_query->found_posts;
            if ($count_promo > 0) {
            ?>
                <div class="col-md-12 col_tabsPromo">
                    <h2 class="ht_homepage">Promo untuk kamu <?php /* <a href="#segera">Lihat Semua <span class="glyphicon glyphicon-menu-right"></span></a> */ ?></h2>
                    <div class="row row_bnpromo">
                        <div id="slide_homePromo" class="owl-carousel owl-theme">
                            <?php
                            while ($the_query->have_posts()) : $the_query->the_post();
                                $promo_id = get_the_ID();
                                $promo_title = get_the_title($promo_id);
                                $promo_link = get_permalink($promo_id);

                                $id_photo = get_field('promo_banner', $promo_id);
                                $thumb = wp_get_attachment_image_src($id_photo, 'full');
                                if ($thumb) {
                                    $urlphoto = $thumb['0'];
                                } else {
                                    $urlphoto = get_template_directory_uri() . '/library/images/default_sorry.jpg';
                                } ?>
                                <div class="item col_bnpromo">
                                    <a href="<?php echo $promo_link; ?>" title="Lihat promo <?php echo $promo_title; ?>">
                                        <div class="mg_bnpromo">
                                            <img class="lazy" data-src="<?php echo $urlphoto; ?>" alt="Promo Terbaru saat ini.">
                                        </div>
                                    </a>
                                </div>
                            <?php
                            endwhile;
                            wp_reset_query(); ?>
                        </div>
                    </div>
                </div>
            <?php
            }
            ?>

            <?php
            $args = array(
                'post_type' => 'promo',
                'posts_per_page' => 4,
                'post_status' => 'publish',
                'orderby' => 'date',
                'order' => 'DESC',
                'meta_query' => array(
                    array(
                        'key' => 'brand_deals',
                        'value' => true,
                        'compare' => 'EXISTS',
                    ),
                ),
            );
            $the_query = new WP_Query($args);
            $count_promo = $the_query->found_posts;
            if ($count_promo > 0) {
            ?>
                <div class="col-md-12 col_tabsPromo">
                    <h2 class="ht_homepage">Brand Deals <?php /*<a href="#">Lihat Semua <span class="glyphicon glyphicon-menu-right"></span></a> */ ?></h2>
                    <div class="row row_bnpromo">
                        <div id="slide_brandDeals" class="owl-carousel owl-theme">
                            <?php
                            while ($the_query->have_posts()) : $the_query->the_post();
                                $promo_id = get_the_ID();
                                $promo_title = get_the_title($promo_id);
                                $promo_link = get_permalink($promo_id);
                                $id_photo = get_field('brand_deals_banner', $promo_id);
                                $thumb = wp_get_attachment_image_src($id_photo, 'full');
                                if ($thumb) {
                                    $urlphoto = $thumb['0'];
                                } else {
                                    $urlphoto = get_template_directory_uri() . '/library/images/default_sorry.jpg';
                                } ?>
                                <a href="<?php echo $promo_link; ?>" title="Lihat promo <?php echo $promo_title; ?>">
                                    <div class="item">
                                        <div class="mg_bndeals">
                                            <img class="" src="<?php echo $urlphoto; ?>">
                                        </div>
                                    </div>
                                </a>
                            <?php
                            endwhile;
                            wp_reset_query(); ?>
                        </div>
                    </div>
                </div>
            <?php
            }
            ?>
            <?php
                $response = sendRequest('GET', MERCHANT_URL . '/api/product/search-products?limit=8');
                if(isset($response->data->products) AND !empty($response->data->products)){
                    if (count($response->data->products) > 0) {
                        ?>
                        <div class="col-md-12 col_tabsPromo">
                            <h2 class="ht_homepage">Produk Terbaru <a href="<?php echo home_url(); ?>/shop/">Lihat Semua <span class="glyphicon glyphicon-menu-right"></span></a></h2>
                            <div class="row row_bnpromo">
                                <?php
                                foreach ($response->data->products as $row) {
                                ?>
                                    <div class="col-xs-6 col-md-3 col_bx_productList">
                                        <div class="bx_productList">
                                            <?php
                                            if ($u_id != 0) {
                                                global $wpdb;
                                                if ($row->wishlist == 0) {
                                                    $love_1 = 'act';
                                                    $love_2 = ''; ?>
                                                    <div class="mg_heart addWishlist love_<?php echo $row->id; ?>" title="Tambah ke wishlist" data-id="<?php echo $row->id; ?>">
                                                        <span class="glyphicon glyphicon-heart"></span>
                                                    </div>
                                                <?php
                                                } else {
                                                    $love_2 = 'act'; ?>
                                                    <div id="btn_" class="mg_heart removewishlist love_<?php echo $row->id; ?> act" title="Hapus dari wishlist" data-id="<?php echo $row->id; ?>">
                                                        <span class="glyphicon glyphicon-heart"></span>
                                                    </div>
                                            <?php
                                                }
                                            }
                                            ?>
                                            <a href="/product/<?php echo $row->permalink . '-' . $row->id; ?>" title="Lihat <?php echo $row->name; ?>">
                                                <div class="mg_productList">
                                                    <img class="lazy" data-src="<?php echo $row->images; ?>" alt="<?php echo $row->name; ?>">
                                                </div>
                                                <div class="bx_in_productList">
                                                    <h4 class="ht_productList"><?php echo $row->name; ?></h4>
                                                    <?php if (intval($row->sale_price) !== 0) { ?>
                                                        <div class="tx_price">Rp <?php echo number_format($row->sale_price); ?></div>
                                                        <?php if (intval($row->sale_price) !== intval($row->price)) : ?>
                                                            <div class="tx_price_sale" style="text-decoration: none;">
                                                                <span style="text-decoration: line-through;">Rp <?php echo number_format($row->price); ?></span>
                                                                <span class="discount"><?php echo $row->discount->text; ?></span>
                                                            </div>
                                                        <?php endif; ?>
                                                    <?php } else { ?>
                                                        <div class="tx_price">Rp <?php echo number_format($row->price); ?></div>
                                                    <?php } ?>
                                                </div>
                                            </a>
                                        </div>
                                    </div>
                                <?php
                                }
                                ?>
                            </div>
                        </div>
                        <?php
                    }
                }
            ?>

            <?php /*
                <!--            <div class="col-md-12 col_tabsPromo">-->
                <!--                <h2 class="ht_homepage">Produk Terbaru <a href="--><?php //echo home_url(); 
                                                                                        ?>
                <!--/shop/">Lihat Semua <span class="glyphicon glyphicon-menu-right"></span></a></h2>-->
                <!--                <div class="row row_bnpromo">-->
                <!--                    --><?php
                                            //                    $low_price = 1;
                                            //                    $high_price = 1000000;
                                            //                    $args = array(
                                            //                        'post_type' => 'product',
                                            //                        'posts_per_page' => 8,
                                            //                        'post_status' => 'publish',
                                            //                        'orderby' => 'date',
                                            //                        'order' => 'DESC',
                                            //                        'meta_query' => array(
                                            //                            array(
                                            //                                'key' => '_price',
                                            //                                'value' => $low_price,
                                            //                                'compare' => '>=',
                                            //                                'type' => 'NUMERIC'
                                            //                            ),
                                            //                            array(
                                            //                                'key' => '_price',
                                            //                                'value' => $high_price,
                                            //                                'compare' => '<=',
                                            //                                'type' => 'NUMERIC'
                                            //                            ),
                                            //                        ),
                                            //                    );
                                            //                    $the_query = new WP_Query($args);
                                            //                    while ($the_query->have_posts()) : $the_query->the_post();
                                            //                        $product_id = get_the_ID();
                                            //                        $product_name = get_the_title($product_id);
                                            //                        $short_name = get_the_title($product_id);
                                            //                        if (strlen($short_name) > 35) {
                                            //                            $short_name = substr($short_name, 0, 35) . '...';
                                            //                        }
                                            //                        $product_link = get_the_permalink($product_id);
                                            //                        $product_price = get_post_meta($product_id, '_regular_price', true);
                                            //                        $product_sale = get_post_meta($product_id, '_sale_price', true);
                                            //
                                            //                        $thumb = wp_get_attachment_image_src(get_post_thumbnail_id($product_id), 'full');
                                            //                        if ($thumb) {
                                            //                            $urlphoto = $thumb['0'];
                                            //                        } else {
                                            //                            $urlphoto = get_template_directory_uri() . '/library/images/default_sorry.jpg';
                                            //                        }
                                            //                        $def_image = get_template_directory_uri() . '/library/images/default_sorry.jpg';
                                            //
                                            //                        $alt = 'Ladara Indonesia Gallery';
                                            //
                                            //                        if (intval($product_sale) !== 0) {
                                            //                            $pr_diskon = (((int)$product_price - (int)$product_sale) / (int)$product_price) * 100;
                                            //                            $pr_diskon = round($pr_diskon, 2);
                                            //                        } else {
                                            //                            $pr_diskon = 0;
                                            //                        }
                                            //                        
                                            ?>
                <!--                        <div class="col-xs-6 col-md-3 col_bx_productList">-->
                <!--                            <div class="bx_productList">-->
                <!--                                --><?php
                                                        //                                if ($u_id != 0) {
                                                        //                                    global $wpdb;
                                                        //                                    $wpdb_query = "SELECT * FROM ldr_wishlist WHERE user_id = '$u_id' AND product_id = '$product_id' ";
                                                        //                                    $res_query = $wpdb->get_results($wpdb_query, OBJECT);
                                                        //                                    $count_res = count($res_query);
                                                        //                                    if ($count_res == 0) {
                                                        //                                        $love_1 = 'act';
                                                        //                                        $love_2 = ''; 
                                                        ?>
                <!--                                        <div class="mg_heart addWishlist love_--><?php //echo $product_id; 
                                                                                                        ?>
                <!--" title="Tambah ke wishlist" data-id="--><?php //echo $product_id; 
                                                                ?>
                <!--">-->
                <!--                                            <span class="glyphicon glyphicon-heart"></span>-->
                <!--                                        </div>-->
                <!--                                        --><?php
                                                                //                                    } else {
                                                                //                                        $love_2 = 'act'; 
                                                                ?>
                <!--                                        <div id="btn_" class="mg_heart removewishlist love_--><?php //echo $product_id; 
                                                                                                                    ?>
                <!-- act" title="Hapus dari wishlist" data-id="--><?php //echo $product_id; 
                                                                    ?>
                <!--">-->
                <!--                                            <span class="glyphicon glyphicon-heart"></span>-->
                <!--                                        </div>-->
                <!--                                        --><?php
                                                                //                                    }
                                                                //                                }
                                                                //                                
                                                                ?>
                <!--                                <a href="--><?php //echo $product_link; 
                                                                ?>
                <!--" title="Lihat --><?php //echo $product_name; 
                                        ?>
                <!--">-->
                <!--                                    <div class="mg_productList">-->
                <!--                                        <img class="" src="--><?php //echo $urlphoto; 
                                                                                    ?>
                <!--" alt="--><?php //echo $alt; 
                                ?>
                <!--">-->
                <!--                                    </div>-->
                <!--                                    <div class="bx_in_productList">-->
                <!--                                        <h4 class="ht_productList">--><?php //echo $short_name; 
                                                                                            ?>
                <!--</h4>-->
                <!--                                        --><?php //if (intval($product_sale) !== 0) { 
                                                                ?>
                <!--                                            <div class="tx_price">Rp --><?php //echo number_format($product_sale); 
                                                                                            ?>
                <!--</div>-->
                <!--                                            --><?php //if (intval($product_sale) !== intval($product_price)) : 
                                                                    ?>
                <!--                                                <div class="tx_price_sale" style="text-decoration: none;">-->
                <!--                                                    <span style="text-decoration: line-through;">Rp --><?php //echo number_format($product_price); 
                                                                                                                            ?>
                <!--</span>-->
                <!--                                                    <span class="discount">--><?php //echo $pr_diskon; 
                                                                                                    ?>
                <!--%</span>-->
                <!--                                                </div>-->
                <!--                                            --><?php //endif; 
                                                                    ?>
                <!--                                        --><?php //} else { 
                                                                ?>
                <!--                                            <div class="tx_price">Rp --><?php //echo number_format($product_price); 
                                                                                            ?>
                <!--</div>-->
                <!--                                        --><?php //} 
                                                                ?>
                <!--                                    </div>-->
                <!--                                </a>-->
                <!--                            </div>-->
                <!--                        </div>-->
                <!--                    --><?php
                                            //                    endwhile;
                                            //                    wp_reset_query();
                                            //                    
                                            ?>
                <!--                </div>-->
                <!--            </div>-->
            */ ?>

        </div>
<?php
endwhile;
else :
    get_template_part('content', '404pages');
endif;
get_footer();
?>