<?php
/*
Author: Drife 2019
URL: htp://themble.com/bones/

This is where you can drop your custom functions or
just edit things like thumbnail sizes, header images,
sidebars, comments, ect.
*/

use GuzzleHttp\Client;
use GuzzleHttp\Cookie\CookieJar;
use Ladara\Helpers\builder\OrderBuilder;
use Ladara\Helpers\builder\PaymentBuilder;
use Ladara\Helpers\builder\ShippingBuilder;
use Ladara\Models\OrderModel;
use Xendit\Exceptions\ApiException;
use Xendit\Invoice;
use Xendit\Xendit;
use Xendit\VirtualAccounts;
use Xendit\Disbursements;

use Ladara\Helpers\builder\UsersBuilder;
use Ladara\Helpers\email\EmailHelper;
use Ladara\Helpers\Jwt;
use Ladara\Models\Notifications;
use Shipper\Location;
use Shipper\Order;
use Shipper\Rates;
use Xendit\EWallets;
use Xendit\Retail;

if (!isset($_SESSION)) 
{
    session_start();
}

/************* INCLUDE NEEDED FILES ***************/

/*
1. library/bones.php
	- head cleanup (remove rsd, uri links, junk css, ect)
	- enqueueing scripts & styles
	- theme support functions
	- custom menu output & fallbacks
	- related post function
	- page-navi function
	- removing <p> from around images
	- customizing the post excerpt
	- custom google+ integration
	- adding custom fields to user profiles
*/
require_once( 'library/bones.php' ); // if you remove this, bones will break
/*
2. library/custom-post-type.php
	- an example custom post type
	- example custom taxonomy (like categories)
	- example custom taxonomy (like tags)
*/
require_once( 'library/custom-post-type.php' ); // you can disable this if you like
/*
3. library/admin.php
	- removing some default WordPress dashboard widgets
	- an example custom dashboard widget
	- adding custom login css
	- changing text in footer of admin
*/
// require_once( 'library/admin.php' ); // this comes turned off by default
/*
4. library/translation/translation.php
	- adding support for other languages
*/
// require_once( 'library/translation/translation.php' ); // this comes turned off by default
/*
5. custom/taxonomy/(any).php
	- add custom taxonomy without WCK
	- please create file first in 'custom/taxonomy' folder
	- change (any).php to your custom taxonomy file e.g custom/taxonomy/example.php

	please don't delete this, simply just comment this
*/
// require_once( 'custom/taxonomy/example.php' );
/*
6. custom/post-type/(any).php
	- add custom post-type without WCK
	- please create file first in 'custom/post-type' folder
	- change (any).php to your custom post-type file e.g custom/post-type/example.php

	please don't delete this, simply just comment this
*/
// require_once( 'custom/post-type/example.php' );
// require_once( 'custom/post-type/vouchers.php' );
/*
7. custom/meta/(any).php
	- add custom meta without Custom Fields
	- please create file first in 'custom/meta' folder
	- change (any).php to your custom meta file e.g custom/meta/example.php

	please don't delete this, simply just comment this
*/
// require_once( 'custom/meta/example.php' );

/************* THUMBNAIL SIZE OPTIONS *************/

// Thumbnail sizes
add_image_size( 'bones-thumb-600', 600, 150, true );
add_image_size( 'bones-thumb-300', 300, 100, true );

/*
to add more sizes, simply copy a line from above
and change the dimensions & name. As long as you
upload a "featured image" as large as the biggest
set width or height, all the other sizes will be
auto-cropped.

To call a different size, simply change the text
inside the thumbnail function.

For example, to call the 300 x 300 sized image,
we would use the function:
<?php the_post_thumbnail( 'bones-thumb-300' ); ?>
for the 600 x 100 image:
<?php the_post_thumbnail( 'bones-thumb-600' ); ?>

You can change the names and dimensions to whatever
you like. Enjoy!
*/

add_filter( 'image_size_names_choose', 'bones_custom_image_sizes' );

function bones_custom_image_sizes( $sizes ) {
    return array_merge( $sizes, array(
        'bones-thumb-600' => __('600px by 150px'),
        'bones-thumb-300' => __('300px by 100px'),
    ) );
}

/*
The function above adds the ability to use the dropdown menu to select 
the new images sizes you have just created from within the media manager 
when you add media to your content blocks. If you add more image sizes, 
duplicate one of the lines in the array and name it according to your 
new image size.
*/

/************* ACTIVE SIDEBARS ********************/

// Sidebars & Widgetizes Areas
function bones_register_sidebars() {
	register_sidebar(array(
		'id' => 'sidebar1',
		'name' => __( 'Sidebar 1', 'bonestheme' ),
		'description' => __( 'The first (primary) sidebar.', 'bonestheme' ),
		'before_widget' => '<div id="%1$s" class="widget %2$s">',
		'after_widget' => '</div>',
		'before_title' => '<h4 class="widgettitle">',
		'after_title' => '</h4>',
	));

	/*
	to add more sidebars or widgetized areas, just copy
	and edit the above sidebar code. In order to call
	your new sidebar just use the following code:

	Just change the name to whatever your new
	sidebar's id is, for example:

	register_sidebar(array(
		'id' => 'sidebar2',
		'name' => __( 'Sidebar 2', 'bonestheme' ),
		'description' => __( 'The second (secondary) sidebar.', 'bonestheme' ),
		'before_widget' => '<div id="%1$s" class="widget %2$s">',
		'after_widget' => '</div>',
		'before_title' => '<h4 class="widgettitle">',
		'after_title' => '</h4>',
	));

	To call the sidebar in your template, you can just copy
	the sidebar.php file and rename it to your sidebar's name.
	So using the above example, it would be:
	sidebar-sidebar2.php

	*/
} // don't remove this bracket!

/************* COMMENT LAYOUT *********************/

// Comment Layout
function bones_comments( $comment, $args, $depth ) {
   $GLOBALS['comment'] = $comment; ?>
	<li <?php comment_class(); ?>>
		<article id="comment-<?php comment_ID(); ?>" class="clearfix">
			<header class="comment-author vcard">
				<?php
				/*
					this is the new responsive optimized comment image. It used the new HTML5 data-attribute to display comment gravatars on larger screens only. What this means is that on larger posts, mobile sites don't have a ton of requests for comment images. This makes load time incredibly fast! If you'd like to change it back, just replace it with the regular wordpress gravatar call:
					echo get_avatar($comment,$size='32',$default='<path_to_url>' );
				*/
				?>
				<?php // custom gravatar call ?>
				<?php
					// create variable
					$bgauthemail = get_comment_author_email();
				?>
				<img data-gravatar="http://www.gravatar.com/avatar/<?php echo md5( $bgauthemail ); ?>?s=32" class="load-gravatar avatar avatar-48 photo" height="32" width="32" src="<?php echo get_template_directory_uri(); ?>/library/images/nothing.gif" />
				<?php // end custom gravatar call ?>
				<?php printf(__( '<cite class="fn">%s</cite>', 'bonestheme' ), get_comment_author_link()) ?>
				<time datetime="<?php echo comment_time('Y-m-j'); ?>"><a href="<?php echo htmlspecialchars( get_comment_link( $comment->comment_ID ) ) ?>"><?php comment_time(__( 'F jS, Y', 'bonestheme' )); ?> </a></time>
				<?php edit_comment_link(__( '(Edit)', 'bonestheme' ),'  ','') ?>
			</header>
			<?php if ($comment->comment_approved == '0') : ?>
				<div class="alert alert-info">
					<p><?php _e( 'Your comment is awaiting moderation.', 'bonestheme' ) ?></p>
				</div>
			<?php endif; ?>
			<section class="comment_content clearfix">
				<?php comment_text() ?>
			</section>
			<?php comment_reply_link(array_merge( $args, array('depth' => $depth, 'max_depth' => $args['max_depth']))) ?>
		</article>
	<?php // </li> is added by WordPress automatically ?>
<?php
} // don't remove this bracket!

/************* SEARCH FORM LAYOUT *****************/

// Search Form
function bones_wpsearch($form) {
	$form = '<form role="search" method="get" id="searchform" action="' . home_url( '/' ) . '" >
	<label class="screen-reader-text" for="s">' . __( 'Search for:', 'bonestheme' ) . '</label>
	<input type="text" value="' . get_search_query() . '" name="s" id="s" placeholder="' . esc_attr__( 'Search the Site...', 'bonestheme' ) . '" />
	<input type="submit" id="searchsubmit" value="' . esc_attr__( 'Search' ) .'" />
	</form>';
	return $form;
} // don't remove this bracket!

//---------------- start ----------------------------
function main_init_js(){
	if (!is_admin()){
		// $url = 'http://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js'; // the URL to check against
		$url = 'https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js';
		$test_url = @fopen($url,'r'); // test parameters
		if($test_url !== false){ // test if the URL exists
			wp_deregister_script('jquery'); // initiate the function  
			wp_register_script('jquery', $url, false, '1.10.2',true);
			wp_enqueue_script('jquery');
		}
		
		// don't remove this
		wp_enqueue_script('jquery-1', get_template_directory_uri().'/library/js/jquery-1.11.3.min.js', array('jquery'), '3',true);

		// $current_user = wp_get_current_user();
		// $u_id = $current_user->ID;
		// $u_roles = $current_user->roles;
		// $admin = 0;

		// if(isset($u_roles) AND !empty($u_roles)){
		// 	foreach ($u_roles as $key => $value) {
		// 	    if(strtolower($value) == 'administrator'){
		// 	        $admin = 1;
		// 	    }
		// 	}
		// }

		// if((isset($u_id) OR $u_id != 0) AND $admin == 1){ // for administrator only
		if ( is_page(
					array(
							'dashboard',
							'finance',
							'daftar-merchant',
							'detail',
							'completed',
							'admin-kategori',
							'edit-kategori',
							'add-kategori',
							'daftar-merchant',
							'manajemen-voucher',
							// Ladara Emas
							'ladara-emas', 'ladara-emas-transaction', 'ladara-emas-user', 'user-emas-detail', 'emas-transaction-detail', 'ladara-emas-statistic',
							'kategori-topup','add-kategori-topup','edit-kategori-topup',
							'so-report',
							// Topup & Tagihan
							'kategori-topup'
					)
			) ) {
	   		
	   		wp_enqueue_style('style1-css', get_template_directory_uri().'/library/css/style-1.css', false, '3', 'all' );

		    wp_enqueue_style('select2-css', get_template_directory_uri().'/library/css/select2.min.css', false, '3', 'all' );
		    wp_enqueue_script('select2-js', get_template_directory_uri().'/library/js/select2.min.js', array('jquery'), '3',true); 

		}else if(is_page( array( 'report-order',
	                            'report-mile',
	                            'report-member',
	                            'report-stock',
	                            'upload',
                        ))){
			wp_enqueue_style('jquery-ui-css', get_template_directory_uri().'/library/css/jquery-ui.min.css', false, '3', 'all' );
			wp_enqueue_style('select2-css', get_template_directory_uri().'/library/css/select2.min.css', false, '3', 'all' );
		    // wp_enqueue_script('select2-js', get_template_directory_uri().'/library/js/select2.min.js', array('jquery'), '3',true); 
		}else{ // for else administrator

			//add stylesheet below
			wp_enqueue_style('bootstrap-css', get_template_directory_uri().'/library/css/bootstrap.css', false, '3', 'all' );
			wp_enqueue_style( 'font-awesome-css', get_template_directory_uri() . '/library/css/all.min.css');
			wp_enqueue_style('animate-css', get_template_directory_uri().'/library/css/animate.css', false, '3', 'all' );
			
			wp_enqueue_script('bootstrap-js', get_template_directory_uri().'/library/js/bootstrap.min.js', array('jquery'), '3',true);

			wp_enqueue_script('lazy-js', get_template_directory_uri().'/library/js/jquery.lazy.min.js', array('jquery'), '3',true);
			wp_enqueue_script('lazy-av-js', get_template_directory_uri().'/library/js/jquery.lazy.av.min.js', array('jquery'), '3',true);

			 if(!is_page( array( 'cart',
			 		//ladara emas
					'emas', 'aktivasi', 'beli', 'riwayat-transaksi', 'transaksi-detail', 'faq-ladara-emas', 'jual', 'kebijakan-privasi', 'riwayat-transaksi', 'syarat-ketentuan', 'selesai', 'faktur', 'processing', 'gagal-bayar', 'invoice', 'sukses-bayar',
					//ladara emas
	                            'checkout',
                        ))){
		 		wp_enqueue_style('carousel-css', get_template_directory_uri().'/owlcarousel/assets/owl.carousel.min.css', false, '3', 'all' );
				wp_enqueue_style('carousel-theme-css', get_template_directory_uri().'/owlcarousel/assets/owl.theme.default.min.css', false, '3', 'all' );

				wp_enqueue_style('photoswipe-css', get_template_directory_uri().'/library/css/photoswipe.css', false, '3', 'all' );
				wp_enqueue_style('photoswipe-skin-css', get_template_directory_uri().'/library/css/default-skin/default-skin.css', false, '3', 'all' );
				wp_enqueue_script('font-awesome-js', get_template_directory_uri().'/library/js/all.min.js', array('jquery'), '3',true);
				// wp_enqueue_script('smoothscroll-js', get_template_directory_uri().'/library/js/smoothscroll.js', array('jquery'), '3',true);
				wp_enqueue_script('carousel-js', get_template_directory_uri().'/owlcarousel/owl.carousel.min.js', array('jquery'), '3',true);

				wp_enqueue_style('magnific-css', get_template_directory_uri().'/library/css/magnific-popup.css', false, '3', 'all' );
				wp_enqueue_script('magnific-js', get_template_directory_uri().'/library/js/jquery.magnific-popup.js', array('jquery'), '3',true);

				wp_enqueue_script('photoswipe-js', get_template_directory_uri().'/library/js/photoswipe.min.js', array('jquery'), '3',true);
				wp_enqueue_script('photoswipe-ui-js', get_template_directory_uri().'/library/js/photoswipe-ui-default.min.js', array('jquery'), '3',true);
			}

			//add javascript below

			wp_enqueue_style('style1-css', get_template_directory_uri().'/library/css/style-1.css', false, '3', 'all' );

		    wp_enqueue_style('select2-css', get_template_directory_uri().'/library/css/select2.min.css', false, '3', 'all' );
		    wp_enqueue_script('select2-js', get_template_directory_uri().'/library/js/select2.min.js', array('jquery'), '3',true); 

		    if ( is_page( array( 'my-order') ) ) {

		    	wp_enqueue_style('jquery-ui-css', get_template_directory_uri().'/library/css/jquery-ui.min.css', false, '3', 'all' );
			    wp_enqueue_style('datetimepicker-css', get_template_directory_uri().'/library/css/jquery.datetimepicker.min.css', false, '3', 'all' );
			    wp_enqueue_script('datepicker-js', get_template_directory_uri().'/library/js/jquery-datepicker.min.js', array('jquery'), '3',true); 
			    wp_enqueue_script('datetimepicker-js', get_template_directory_uri().'/library/js/jquery.datetimepicker.full.min.js', array('jquery'), '3',true);
			}

			wp_enqueue_style('jqueryui-css', get_template_directory_uri().'/library/css/jquery-ui.1.12.1.css', false, '3', 'all' );
			wp_enqueue_script('jqueryui-js', get_template_directory_uri().'/library/js/jquery-ui.1.12.1.js', array('jquery'), '3',true);

			//ladara emas
			if ( is_page( array( 'emas', 'aktivasi', 'beli', 'riwayat-transaksi', 'transaksi-detail', 'faq-ladara-emas', 'jual', 'kebijakan-privasi', 'riwayat-transaksi', 'syarat-ketentuan', 'selesai', 'faktur', 'processing', 'gagal-bayar', 'invoice', 'sukses-bayar') ) ) {
				wp_enqueue_style('style-ladara-emas-css', get_template_directory_uri().'/library/css/ladara-emas/style.css', false, '3', 'all' );
				wp_enqueue_script('scripts-ladara-emas-js', get_template_directory_uri().'/library/js/ladara-emas/scripts.js', array('jquery'), '3',true);
			}

			wp_enqueue_script('ajax-ladara-emas-js', get_template_directory_uri().'/library/js/ladara-emas/ajax.js', array('jquery'), '3',true);
			//ladara emas

		}


		wp_enqueue_style('style-faq-css', get_template_directory_uri().'/library/css/style-faq.css', false, '3', 'all' );
		

	}
}
add_action('wp_enqueue_scripts', 'main_init_js');



//----------------------------------------custom--------------------------------------------------------//
// require_once('library/project_posttype.php');
require_once('admcustom/product-post-type.php');
require_once('admcustom/promo-post-type.php');

add_filter( 'show_admin_bar', '__return_false' );
function hide_abar() {
?>
	<style type="text/css">
		.show-admin-bar {
			display: none;
		}
	</style>
<?php
}

function hide_a_bar() {
    add_filter( 'show_admin_bar', '__return_false' );
    add_action( 'admin_print_scripts-profile.php', 
         'hide_abar' );
}
add_action( 'init', 'hide_a_bar' , 9 );

add_action( 'admin_enqueue_scripts', 'load_custom_script' );
function load_custom_script() {
	wp_enqueue_script('custom_js_script', get_bloginfo('template_url').'/library/js/adminajax.js?r='.rand(), array('jquery'));
}

add_action('init', 'script_js');
function script_js() {
	if (!is_admin()) {
		wp_enqueue_script('script_js', get_bloginfo('template_url') . '/library/js/myajax-3.js', array('jquery'), '1',true);
		wp_enqueue_script('script2_js', get_bloginfo('template_url') . '/library/js/myajax-2.js', array('jquery'), '2',true);
		wp_enqueue_script('script3_js', get_bloginfo('template_url') . '/library/js/myajax-1.js', array('jquery'), '3',true);

		$protocol = isset( $_SERVER["HTTPS"]) ? 'https://' : 'http://';
		$params = array(
		'ajaxurl'=>admin_url('admin-ajax.php', $protocol)
		);
		wp_localize_script('script_js', 'ajaxscript', $params);
	}
}

// put your ajax script below
function base64url_encode($data) {
  return rtrim(strtr(base64_encode($data), '+/', '-_'), '=');
}

function base64url_decode($data) {
  return base64_decode(str_pad(strtr($data, '-_', '+/'), strlen($data) % 4, '=', STR_PAD_RIGHT));
}

function is_decimal( $val )
{
    return is_numeric( $val ) && floor( $val ) != $val;
}

function my_login_logo() { ?>
    <style type="text/css">
    	body.login{
    		background: #f3f3f3;
    	}
        #login h1 a, .login h1 a {
        height: 65px;
        width: auto;
        background-size: auto 90px;
        background-repeat: no-repeat;
        padding-bottom: 30px;
        background-image: url(<?php echo get_stylesheet_directory_uri(); ?>/library/images/login-logo.png)
        }
       /* #nav,#loginform{
          display: none !important;
        }*/
        .login #backtoblog, .login #nav{
          text-align: center;
        }
        #wp-submit{
            background: #FFCE00;
		    width: 100%;
		    line-height: 35px;
		    height: 35px;
		    font-size: 13px;
		    color: #fff;
		    font-weight: 600;
		    text-transform: uppercase;
		    letter-spacing: .3px;
		    text-align: center;
		    border: 0;
		    border-radius: 0px;
		    padding: 0;
		    text-shadow: none;
    		box-shadow: none;
    		margin-top: 15px;
        }
        .login label {
		    font-size: 13px;
		    font-weight: 600;
		}
		.login form .input, .login input[type=text]{
			line-height: 35px;
			font-size: 14px !important;
			padding: 0px 10px 0px 10px !important;
		}
        #login{

        	padding-top: 2% !important;
        }
        .login #backtoblog a, .login #nav a {
		    text-decoration: none;
		    color: #fff !important;
		}
    </style>
<?php 
}
add_action( 'login_enqueue_scripts', 'my_login_logo' );

// Redirect Login Error
add_action( 'wp_login_failed', 'my_front_end_login_fail' );
function my_front_end_login_fail( $username ) {
     $referrer = $_SERVER['HTTP_REFERER'];
     if ( !empty($referrer) && !strstr($referrer,'wp-login') && !strstr($referrer,'wp-admin')     ) {
      $linkParts = explode('?', $referrer);
      // $new_link = $linkParts[0]; // no use anymore
      $new_link = home_url(); // give the base url https://revamp.iqos.id/ 
          wp_redirect( $new_link . '/login/?login=failed' );
          exit();
     }
}

add_action('wp_login', 'setcookiemerchant', 10, 2);
function setcookiemerchant($userLogin, $user)
{
    $cookieValue = Jwt::generateToken($user);
    $expireIn = time() + (7 * 24 * 60 * 60 * 2);
    setcookie('tnahcremaradal', $cookieValue['token'], $expireIn, "/", $_SERVER['SERVER_NAME'], false, true);
}

function admin_style() {
  ?>
<style type="text/css">
  .order-status.status-preparing{
    background: #5b841b;
      color: #fff;
  }
  .order-status.status-ondelivery{
    background: #f15b59;
      color: #fff;
      opacity: .8;
  }
  .order-status.status-completed{
    background: #128865 !important;
      color: #fff !important;
  }
</style>
<?php
}
add_action('admin_enqueue_scripts', 'admin_style');

function register_preparing_order_status() {

    register_post_status( 'wc-preparing', array(
        'label'                     => 'preparing',
        'public'                    => true,
        'exclude_from_search'       => false,
        'show_in_admin_all_list'    => true,
        'show_in_admin_status_list' => true,
        'label_count'               => _n_noop( 'Preparing <span class="count">(%s)</span>', 'Preparing <span class="count">(%s)</span>' )
    ) );

}
add_action( 'init', 'register_preparing_order_status' );

// Add to list of WC Order statuses
function add_preparing_to_order_statuses( $order_statuses ) {

    $new_order_statuses = array();

    // add new order status after processing
    foreach ( $order_statuses as $key => $status ) {

        $new_order_statuses[ $key ] = $status;

        if ( 'wc-processing' === $key ) {
            $new_order_statuses['wc-preparing'] = 'Preparing';

        }
    }

    return $new_order_statuses;
}
add_filter( 'wc_order_statuses', 'add_preparing_to_order_statuses' );

function register_ondelivery_order_status() {
    register_post_status( 'wc-ondelivery', array(
        'label'                     => 'Ondelivery',
        'public'                    => true,
        'exclude_from_search'       => false,
        'show_in_admin_all_list'    => true,
        'show_in_admin_status_list' => true,
        'label_count'               => _n_noop( 'Ondelivery <span class="count">(%s)</span>', 'Ondelivery <span class="count">(%s)</span>' )
    ) );

}
add_action( 'init', 'register_ondelivery_order_status' );

// Add to list of WC Order statuses
function add_ondelivery_to_order_statuses( $order_statuses ) {

    $new_order_statuses = array();

    // add new order status after processing
    foreach ( $order_statuses as $key => $status ) {

        $new_order_statuses[ $key ] = $status;

        if ( 'wc-processing' === $key ) {
            $new_order_statuses['wc-ondelivery'] = 'Ondelivery';

        }
    }

    return $new_order_statuses;
}
add_filter( 'wc_order_statuses', 'add_ondelivery_to_order_statuses' );

// =============================== START HERE ==================================

// ============== Add Function Admin ===================
get_template_part( 'content', 'function-admin' );
get_template_part( 'content', 'function-emas' );
get_template_part( 'content', 'function-api' );
get_template_part( 'content', 'cron' );
get_template_part( 'function', 'ajax' );
// get_template_part( 'function', 'admin-ajax' );
// ============== Add Function Admin ===================

function ajax_newLoginNow(){
    global $wpdb;

	$nonce = $_REQUEST['_wpnonce'];
	if ( ! wp_verify_nonce( $nonce, 'login_submit' ) ) {
	  exit(); // Get out of here, the nonce is rotten!
	}

	date_default_timezone_set("Asia/Jakarta");
	$nowdate = date('Y-m-d H:i:s');

	$user_login = $_REQUEST['log'];
	$user_pass = $_REQUEST['pwd'];

    $user = get_user_by( 'email', $user_login );
    $valid = 0;

    if ($user === false) {
        $check = $wpdb->get_row("SELECT * FROM ldr_users_temp WHERE email = '$user_login'", OBJECT);
        
        if ($check !== null) {
            $valid = 2;
        } else {
            $valid = 3;
        }
    } else {
        if ( $user && wp_check_password( $user_pass, $user->data->user_pass, $user->ID ) ) {
            $result_user = wp_set_auth_cookie( $user->ID, true, is_ssl() );

            $cookieValue = Jwt::generateToken($user);
            $expireIn = time() + (7 * 24 * 60 * 60 * 2);
            setcookie('tnahcremaradal', $cookieValue['token'], $expireIn, "/", $_SERVER['SERVER_NAME'], false, true);

            $valid = 1;

            $cartData = $_COOKIE['user_cart'];

            if ($cartData !== null) {
                $carts = json_decode(stripslashes(base64_decode($cartData)));

                foreach ($carts as $cart) {
                    $check = $wpdb->get_row("SELECT * FROM ldr_cart WHERE user_id = $user->ID AND product_id = $cart->product_id AND stock_id = $cart->stock_id");

                    if ($check === null) {
                        $wpdb->insert('ldr_cart', [
                            'user_id' => $user->ID,
                            'product_id' => $cart->product_id,
                            'stock_id' => $cart->stock_id,
                            'qty' => $cart->qty,
                            'seller_id' => $cart->seller_id,
                            'created_date' => $nowdate
                        ]);
                    } else {
                        $wpdb->update('ldr_cart', [
                            'qty' => ($cart->qty + $check->qty)
                        ], [
                            'id' => $check->id
                        ]);
                    }
                }

                unset($_COOKIE['user_cart']);
                setcookie('user_cart', '', time() - 3600, "/", $_SERVER['SERVER_NAME']);
            }
        }
    }

	echo json_encode($valid);
	die();
}   
add_action('wp_ajax_ajax_newLoginNow','ajax_newLoginNow');
add_action('wp_ajax_nopriv_ajax_newLoginNow', 'ajax_newLoginNow');


function login_socialmedia($data_user,$account_type){

	$u_email = $data_user['user_email'];
	$u_fullname = $data_user['first_name'].' '.$data_user['last_name'];
	$acc_type = $data_user['acc_type'];
	$u_phone = $data_user['user_phone'];
	$u_gender = $data_user['user_gender'];
	$u_fullname = trim($u_fullname);
	$valid = 0;

	if(isset($account_type) AND $account_type == 'fb'){
		$acc_type = 1;
	}else if(isset($account_type) AND $account_type == 'google'){
		$acc_type = 2;
	}else{
		$acc_type = 0;
	}

	if( isset($u_email) AND $u_email != '' ){

		global $wpdb;
		$wpdb_query = "SELECT * FROM ldr_users WHERE user_login = '$u_email' OR user_email = '$u_email' ";
		$res_query = $wpdb->get_results($wpdb_query, OBJECT);
		$count_res = count($res_query);
		if($count_res == 0) {

			$new_password = wp_generate_password();
		    $user_id = wp_create_user( $u_email, $new_password, $u_email);

		    if(isset($user_id)){
		        wp_update_user( array( 'ID' => $user_id, 'display_name' => $u_fullname, 'user_nicename' => $u_fullname, 'first_name' => $u_fullname ) );

		        $update_result = $wpdb->update($wpdb->users, array('user_login' => $u_email), array('ID' => $user_id));
		        wp_update_user( array ( 'ID' => $user_id, 'role' => 'customer' ) ) ;
		        
		        //Insert into custom database
		        $insert_result = $wpdb->insert('ldr_user_profile', array(
		            'user_id' => $user_id,
		            'name' => $u_fullname,
		            'account_type' => $acc_type,
		            'email' => $u_email,
		            'phone' => $u_phone,
		            'created_date' => current_time( 'mysql', true),
		            'account_type' => $acc_type
		        ));

		        // ========= Email new register with new password for change password ============
                $userBuilder = new UsersBuilder($u_fullname, $u_email);
                $email = (new EmailHelper($userBuilder))->welcome();

		        // =========== insert notification ==============
				$orderId_notif = '0';
				$type_notif = 'info';
				$title_notif = 'Selamat Datang';
				$desc_notif = 'Terima kasih sudah bergabung bersama LaDaRa Indonesia. Dapatkan ribuan keperluan Anda hanya di LaDaRa Indonesia.';
				$data = [
				      'userId' => $user_id, //wajib
				      'title' => $title_notif, //wajib
				      'descriptions' => $desc_notif, //wajib
				      'type' => $type_notif, //pesanan, info, pembayaran, emas // wajib
				      'orderId' => $orderId_notif, // optional
				      'data' => [] //array optional bisa diisi dengan data lainnya
				];
				$addNotif = Notifications::addNotification($data);
				// =========== insert notification ==============

		        // ========= Email new register with new password for change password ============

				$user = get_user_by('email', $u_email);
				$result_user = wp_set_auth_cookie( $user->ID, true, is_ssl() );

				$valid = 1;
		    } 

		}else{

			$user = get_user_by('email', $u_email);
			$result_user = wp_set_auth_cookie( $user->ID, true, is_ssl() );
			$valid = 1;
		}

        $cookieValue = Jwt::generateToken($user);
        $expireIn = time() + (7 * 24 * 60 * 60 * 2);
        setcookie('tnahcremaradal', $cookieValue['token'], $expireIn, "/", $_SERVER['SERVER_NAME'], false, true);
	}

	return $valid;

}

function ajax_newRegister(){

	$nonce = $_REQUEST['_wpnonce'];
	if ( ! wp_verify_nonce( $nonce, 'NWRGST' ) ) {
	  exit(); // Get out of here, the nonce is rotten!
	}

	date_default_timezone_set("Asia/Jakarta");
	$nowdate = date('Y-m-d H:i:s');

	$u_email = htmlspecialchars(strtolower($_REQUEST['u_email']));
	$u_password = $_REQUEST['u_password'];
	$u_nama = htmlspecialchars($_REQUEST['u_nama']);
	$u_phone = htmlspecialchars($_REQUEST['u_phone']);
	if(isset($u_phone) AND $u_phone != ''){
		$u_phone = '';
	}
	$valid = 0;

	global $wpdb;

	$wpdb_query = "SELECT * FROM ldr_users WHERE user_login = '$u_email' OR user_email = '$u_email' ";
	$res_query = $wpdb->get_row($wpdb_query, OBJECT);
	
	if(empty($res_query)) {

		$wpdb_query2 = "SELECT * FROM ldr_users_temp WHERE email = '$u_email' AND password != '' ";
		$res_query2 = $wpdb->get_row($wpdb_query2, OBJECT);
		
		if(empty($res_query2)) {
			$activation_key = base64_encode( $u_email.'+'.$u_password.'+'.$nowdate );

			$query_insert = "INSERT INTO ldr_users_temp(email,password,fullname,phone,activation,created_date) VALUES('$u_email','$u_password','$u_nama','$u_phone','$activation_key','$nowdate')";
			$result_insert = $wpdb->query($query_insert);
			$valid = 1;

			$full_link = home_url().'/register/confirm/?ups='.$activation_key;

			// send mail
			$userBuilder = new UsersBuilder($u_nama, $u_email);
			$email = (new EmailHelper($userBuilder))->register($full_link);
		}

	}	

	echo json_encode($valid);
	die();
}   
add_action('wp_ajax_ajax_newRegister','ajax_newRegister');
add_action('wp_ajax_nopriv_ajax_newRegister', 'ajax_newRegister');



function ajax_forgotPassword(){

	$nonce = $_REQUEST['_wpnonce'];
	if ( ! wp_verify_nonce( $nonce, 'FGCKNLD' ) ) {
	  exit(); // Get out of here, the nonce is rotten!
	}

	date_default_timezone_set("Asia/Jakarta");
	$nowdate = date('Y-m-d H:i:s');

	$u_email = $_REQUEST['u_email'];
	$valid = 0;

	global $wpdb;
	$wpdb_query = "SELECT * FROM ldr_users WHERE user_login = '$u_email' OR user_email = '$u_email' ";
	$res_query = $wpdb->get_results($wpdb_query, OBJECT);
	$count_res = count($res_query);
	if($count_res > 0){

		foreach ($res_query as $key => $value) {
			$user_id = $value->ID;
			$display_name = $value->display_name;

			$new_password = wp_generate_password();
			wp_set_password( $new_password, $user_id );
			// send email
            $userBuilder = new UsersBuilder($display_name, $u_email);
            $email = (new EmailHelper($userBuilder))->newPassword($new_password);

			$valid = 1;
		}

	}	

	echo json_encode($valid);
	die();
}   
add_action('wp_ajax_ajax_forgotPassword','ajax_forgotPassword');
add_action('wp_ajax_nopriv_ajax_forgotPassword', 'ajax_forgotPassword');

function get_dbkota() {
    $provinceId =  $_REQUEST['province_id'];
    $response = [];

    try {
        $getCities = Location::getCities($provinceId);
        $response = $getCities->data->rows;
    } catch (Exception $e) {
        // Do nothing
    }

    echo json_encode($response);
    die();
}
add_action('wp_ajax_get_dbkota','get_dbkota');
add_action('wp_ajax_nopriv_get_dbkota', 'get_dbkota');

function get_dbkecamtan() {
    $cityId =  $_REQUEST['regency_id'];
    $response = [];

    try {
        $getSuburbs = Location::getSuburbs($cityId);
        $response = $getSuburbs->data->rows;
    } catch (Exception $e) {
        // Do nothing
    }

    echo json_encode($response);
    die();
}   
add_action('wp_ajax_get_dbkecamtan','get_dbkecamtan');
add_action('wp_ajax_nopriv_get_dbkecamtan', 'get_dbkecamtan');

function get_dbkelurahan() {
    $suburbId =  $_REQUEST['suburb_id'];
    $response = [];

    try {
        $getAreas = Location::getAreas($suburbId);
        $response = $getAreas->data->rows;
    } catch (Exception $e) {
        // Do nothing
    }

    echo json_encode($response);
    die();
}   
add_action('wp_ajax_get_dbkelurahan','get_dbkelurahan');
add_action('wp_ajax_nopriv_get_dbkelurahan', 'get_dbkelurahan');

function get_dbkodepos() {
    $suburbId =  $_REQUEST['kecamatan'];
    $response = [];

    try {
        $getAreas = Location::getAreas($suburbId);
        $response = $getAreas->data->rows;
    } catch (Exception $e) {
        // Do nothing
    }

    echo json_encode($response);
    die();
}   
add_action('wp_ajax_get_dbkodepos','get_dbkodepos');
add_action('wp_ajax_nopriv_get_dbkodepos', 'get_dbkodepos');

function ajax_changepass(){

date_default_timezone_set("Asia/Jakarta");
$nowdate = date('Y-m-d H:i:s');
$u_id = $_REQUEST['u_id'];
$pass_lama = $_REQUEST['pass_lama'];
$pass_baru = $_REQUEST['pass_baru'];
$user_login = $_REQUEST['login'];

$valid = 0;

$user = get_user_by( 'login', $user_login );
if ( $user && wp_check_password( $pass_lama, $user->data->user_pass, $user->ID ) ) {
   	$valid = 1;
	wp_set_password( $pass_baru, $u_id );
} else {
    $valid = 0;
}

$result = 'ajax masuk';
echo json_encode($valid);
die();
}   
add_action('wp_ajax_ajax_changepass','ajax_changepass');
add_action('wp_ajax_nopriv_ajax_changepass', 'ajax_changepass');


function ajax_saveProfile(){

date_default_timezone_set("Asia/Jakarta");
$nowdate = date('Y-m-d H:i:s');
$u_id = $_REQUEST['u_id'];
$email_login = htmlspecialchars($_REQUEST['email_login']);
$nama_lengkap = htmlspecialchars($_REQUEST['nama_lengkap']);
$nomor_hp = htmlspecialchars($_REQUEST['nomor_hp']);
$dob_day = htmlspecialchars($_REQUEST['dob_day']);
$dob_month = htmlspecialchars($_REQUEST['dob_month']);
$dob_year = htmlspecialchars($_REQUEST['dob_year']);
$gender = htmlspecialchars($_REQUEST['gender']);
$valid = 0;

if(isset($u_id) AND $u_id != ''){
    global $wpdb;
	$dob_full = $dob_year.'-'.$dob_month.'-'.$dob_day;

	wp_update_user( array( 'ID' => $u_id, 'display_name' => $nama_lengkap, 'user_nicename' => $nama_lengkap, 'first_name' => $nama_lengkap ) );

	update_field("nama_lengkap",$nama_lengkap,"user_".$u_id);
	update_field("tanggal_lahir",$dob_full,"user_".$u_id);
	update_field("telepon",$nomor_hp,"user_".$u_id);
	update_field("jenis_kelamin",$gender,"user_".$u_id);
	$dataProfile = [
	        'name' => $nama_lengkap,
            'birthdate' => $dob_full,
            'gender' => $gender,
            'phone' => $nomor_hp,
    ];
    $dataUser = [
        'display_name' => $nama_lengkap
    ];
	$wpdb->update('ldr_user_profile',$dataProfile, ['user_id' => $u_id]);
	$wpdb->update('ldr_users',$dataUser, ['user_id' => $u_id]);
	$valid = 1;
}


$result = 'ajax masuk';
echo json_encode($valid);
die();
}   
add_action('wp_ajax_ajax_saveProfile','ajax_saveProfile');
add_action('wp_ajax_nopriv_ajax_saveProfile', 'ajax_saveProfile');


function ajax_addAddress() {
    $addressId = isset($_REQUEST['ad_id']) ? intval($_REQUEST['ad_id']) : 0;
    $httpMethod = 'POST';
    $endpoint = MERCHANT_URL . '/api/address';

    if ($addressId !== 0) {
        $httpMethod = 'PATCH';
        $endpoint = MERCHANT_URL . '/api/address/' . $addressId;
    }

    $saveAddress = sendRequest($httpMethod, $endpoint, [
        'nama_alamat' => $_REQUEST['nama_alamat'],
        'nama_penerima' => $_REQUEST['nama_lengkap'],
        'telepon' => $_REQUEST['nomor_hp'],
        'alamat' => $_REQUEST['alamat'],
        'kode_pos' => $_REQUEST['kodepos'],
        'latitude' => $_REQUEST['latitude'],
        'longitude' => $_REQUEST['longitude'],
        'alamat_utama' => intval($_REQUEST['alamat_utama'])
    ]);

    echo json_encode($saveAddress);
    die();
}
add_action('wp_ajax_ajax_addAddress','ajax_addAddress');
add_action('wp_ajax_nopriv_ajax_addAddress', 'ajax_addAddress');


function ajax_deleteAddress() {
    $deleteAddress = sendRequest('DELETE', MERCHANT_URL . '/api/address/' . $_REQUEST['address_id']);

    echo json_encode($deleteAddress);
    die();
}   
add_action('wp_ajax_ajax_deleteAddress','ajax_deleteAddress');
add_action('wp_ajax_nopriv_ajax_deleteAddress', 'ajax_deleteAddress');



function ajax_applyVoucher(){
	$current_user = wp_get_current_user();
	$u_id = $current_user->ID;


	date_default_timezone_set("Asia/Jakarta");
	$nowdate = date('Y-m-d H:i:s');
	$vou_id = $_REQUEST['vou_id'];
	$code_coupon = get_field('code_coupon',$vou_id);
    $mile_coupon = get_field('mile_coupon',$vou_id);
    $mile_reward = 0;

	$log_text = '';
	$status = 0;
	global $wpdb;
	$wpdb_query = "SELECT * FROM ldr_voucher WHERE user_id = '$u_id' AND voucher_id = '$vou_id'";
	$res_query = $wpdb->get_results($wpdb_query, OBJECT);
	$count_res = count($res_query);
	if($count_res == 0){ // if not exist with same user_id
		if($mile_coupon <= $mile_reward){ // if user mile more than coupon mile
			// ==== insert to voucher list
			$query = "INSERT INTO ldr_voucher(user_id,voucher_id,created_date) VALUES('$u_id','$vou_id','$nowdate')";
			$result = $wpdb->query($query);
			// ==== insert to voucher log
			$voucher_name = get_the_title($vou_id);
			$log_text = 'Selamat! Anda telah mendapatkan voucher '.$voucher_name;
			$query2 = "INSERT INTO ldr_voucher_log(user_id,log,created_date) VALUES('$u_id','$log_text','$nowdate')";
			$result2 = $wpdb->query($query2);
			// ==== update user mile total
			$mile_total = (int)$mile_reward-(int)$mile_coupon;
			// update_field("mile_reward",$mile_total,"user_".$u_id);
			// ==== insert to mile log
			$log_text = 'Membayar -'.$mile_coupon.' mile untuk voucher '.$voucher_name;
			$log_status = '-';
			// $query3 = "INSERT INTO ldr_mile_log(user_id,log,status,user_mile,voucher_mile,created_date) VALUES('$u_id','$log_text','$log_status','$mile_reward','$mile_coupon','$nowdate')";
			// $result3 = $wpdb->query($query3);
			$status = 1;
		}else{

		}
	}
	$result = 'ajax masuk';
	echo json_encode($status);
	die();
}   
add_action('wp_ajax_ajax_applyVoucher','ajax_applyVoucher');
add_action('wp_ajax_nopriv_ajax_applyVoucher', 'ajax_applyVoucher');


function ajax_getCartList(){
	date_default_timezone_set("Asia/Jakarta");
    $client = new GuzzleHttp\Client();
	$nowdate = date('Y-m-d H:i:s');

	$current_user = wp_get_current_user();
	$u_id = $current_user->ID;
	$valid = 0;
	$html = '';

	$now_cart = $_REQUEST['cart_count'];
	$count_cart = get_itemCart_count();
        
    global $wpdb;
    if (isset($u_id) and $u_id != 0) {
        $wpdb_query = "SELECT * FROM ldr_cart WHERE user_id = '$u_id' ";
        $res_query = $wpdb->get_results($wpdb_query, OBJECT);
        $count_resdata = count($res_query);
    } else {
        $cartData = $_COOKIE['user_cart'];
    
        if ($cartData !== null) {
            $res_query = json_decode(stripslashes(base64_decode($cartData)));
            $count_resdata = count($res_query);
        }
    }

    if($count_resdata > 0){
        foreach ($res_query as $key => $value) {
            $key_id = $value->id;
            $product_id = $value->product_id;
            try {
                $req = $client->request('GET', MERCHANT_URL . '/api/product/detail/'.$product_id);
                $respProduct = json_decode($req->getBody());
            } catch (\GuzzleHttp\Exception\GuzzleException $e) {
                $respProduct = [];
            }
            if (count($respProduct->data) > 0 ){
                $respProduct = $respProduct->data;
                $product_name = $respProduct->name;
                $short_name = $respProduct->name;
                $product_link = '/product/'.$respProduct->slug.'-'.$respProduct->id;
                if(strlen($short_name) > 35) $short_name = substr($short_name, 0, 35).'...';

                if($respProduct->sale_price != 0){
                    $product_price = $respProduct->sale_price;
                }else{
                    $product_price = $respProduct->price;
                }

                $cart_qty = $value->qty;
                $stock_id = $value->stock_id;
                $ukuran_value = '';
                $nomor_value = '';
                $warna_value = '';
                $product_quantity = 0;
                global $wpdb;
                $wpdb_query = "SELECT * FROM ldr_stock WHERE id = $stock_id ";
                $res_query = $wpdb->get_results($wpdb_query, OBJECT);
                $count_res = count($res_query);
                if($count_res > 0){
                    foreach ($res_query as $key => $value){
                        $ukuran_value = $value->ukuran_value;
                        $nomor_value = $value->nomor_value;
                        $warna_value = $value->warna_value;
                        $product_quantity = $value->product_quantity;
                    }
                }
                if(count($respProduct->images) > 0){
                    $urlphoto = $respProduct->images[0]->path ?? get_template_directory_uri().'/library/images/sorry.png';
                }else{
                    $urlphoto = get_template_directory_uri().'/library/images/sorry.png';
                }
                $alt = 'Ladara Indonesia';

                $html .= '<div class="row row_topcart">
                        <div class="col_topcart_1">
                            <div class="mg_topcart">
                                <img src="'.$urlphoto.'" alt="'.$alt.'">
                            </div>
                        </div>
                        <div class="col_topcart_2">
                            <a href="'.$product_link.'" title="Lihat detail produk ini."><h4 class="ht_topcart">'.$short_name.'</h4></a>
                            <div class="var_topcart">';
                if(isset($warna_value) AND $warna_value != ''){
                    $html .= $warna_value;
                }
                if(isset($ukuran_value) AND $ukuran_value != ''){
                    $html .= " | ".$ukuran_value;
                }
                if(isset($nomor_value) AND $nomor_value != ''){
                    $html .= " | ".$nomor_value;
                }
                $html .= '</div>	
                                <div class="prc_topcart">Rp '.number_format($product_price).'</div>
                                <div class="qty_topcart">'.$cart_qty.' pcs</div>
                            </div>
                        </div>';

            }
        }
    
        $html .= '<div class="a_topcart">
                    <a href="'.home_url().'/cart/" title="Lihat semua keranjang belanja Anda.">Lihat keranjang</a>
                </div>';

        $valid = 1;
    }else{
    
        $html .= '<div class="mg_cartMenu">
                    <img src="'.get_template_directory_uri().'/library/images/ico_cartempty.svg">
                </div>
                <div class="info_cartMenu">
                    <b>Kosong?</b><br/>
                    Yuk segera isi keranjangnya dengan barang-barang impian kamu!
                </div>';
    }
					             	
	
	$return = array(
					'html' => $html,
					'valid' => $valid,
					'count_cart' => $count_cart
					);

	// $result = 'ajax masuk';
	echo json_encode($return);
	die();
}   
add_action('wp_ajax_ajax_getCartList','ajax_getCartList');
add_action('wp_ajax_nopriv_ajax_getCartList', 'ajax_getCartList');


function ajax_addCart(){
	date_default_timezone_set("Asia/Jakarta");
	$nowdate = date('Y-m-d H:i:s');

	$current_user = wp_get_current_user();
	$u_id = $current_user->ID;
	$pro_id = $_REQUEST['pro_id'];

	$seller_id = $_REQUEST['slr_id'];

	$warna = $_REQUEST['warna'];
	$ukuran = $_REQUEST['ukuran'];
	$nomor = $_REQUEST['nomor'];
	$qty = $_REQUEST['qty'];
	$valid = 0;
	$re_valid = 0;
	$product_quantity = 0;
	$product_id = '';
	$count_cart = 0;
	$stock_id = '';

	global $wpdb;
	$wpdb_query = "SELECT * FROM ldr_stock WHERE product_id = '$pro_id' AND ukuran_value = '$ukuran' AND nomor_value = '$nomor' AND warna_value = '$warna' ";
	$res_query = $wpdb->get_results($wpdb_query, OBJECT);
	$count_res = count($res_query);
	if($count_res > 0){
		foreach ($res_query as $key => $value) {
			$stock_id = $value->id;
			$product_id = $value->product_id;
			$product_quantity = $value->product_quantity;
		}
		$valid = 1;
    }

	if( $valid == 1 AND ( $product_quantity > 0 && $qty <= $product_quantity) ){
		global $woocommerce;
		
		$seller_data = get_field('seller_name',$pro_id);
//		if (isset($seller_data) AND !empty($seller_data)) {
//			$seller_id = $seller_data->ID;
//		}else{
//			$seller_id = 0;
//		}

		$mycart = array();
		$oneseller = 1;
		global $wpdb;
		$wpdb_query = "SELECT * FROM ldr_cart WHERE user_id = '$u_id' ";
		$res_query = $wpdb->get_results($wpdb_query, OBJECT);
		$count_res = count($res_query);
		foreach ($res_query as $key => $value) {
			$tb_id = $value->id;
			$tb_pro_id = $value->product_id;
			$tb_seller_id = $value->seller_id;
			$mycart[$tb_id] = array('pro_id'=>$tb_pro_id,'seller_id'=>$tb_seller_id);

			if($tb_seller_id != $seller_id){
				$oneseller = 0;
			}

		}
 
        if (isset($u_id) and $u_id != 0) {
            global $wpdb;
            $wpdb_query = "SELECT * FROM ldr_cart WHERE user_id = '$u_id' AND stock_id = '$stock_id' ";
            $res_query = $wpdb->get_results($wpdb_query, OBJECT);
            $count_res = count($res_query);

            if ($count_res == 0) {
                global $wpdb;
                $query_insert = "INSERT INTO ldr_cart(user_id,product_id,stock_id,qty,seller_id,created_date) VALUES($u_id,'$pro_id','$stock_id','$qty','$seller_id','$nowdate')";
                $result_insert = $wpdb->query($query_insert);
                $re_valid = 1;
            } else {
                $re_valid = 1;
                foreach ($res_query as $key => $value) {
                    $tb_id = $value->id;
                    $tb_pro_id = $value->product_id;
                    $tb_stock_id = $value->stock_id;
                    $tb_qty = $value->qty;

                    if ($tb_stock_id == $stock_id) {
                        $total_qty = (int)$tb_qty + (int)$qty;

                        if ($total_qty > $product_quantity) {
                            $total_qty = $product_quantity;
                        }

                        global $wpdb;
                        $wpdb_query_update = " UPDATE ldr_cart
                                    SET qty=$total_qty
                                    WHERE id = '$tb_id' AND user_id = '$u_id' AND stock_id = '$tb_stock_id' 
                                ";
                        $res_query_update = $wpdb->query($wpdb_query_update);

                        // $data[$key2] = array('pro_id'=>$ses_pro_id,'qty'=>$total_qty,'stock_id'=>$stock_id);
                    }
                }
            }
        } else {
            $cartData = $_COOKIE['user_cart'];
            $cartArr = array();
            $updateKey = null;

            if ($cartData !== null) {
                $cartArr = json_decode(stripslashes(base64_decode($cartData)));
                $cartSearch = array_filter($cartArr, function($fn) use ($pro_id, $stock_id) {
                    return ($fn->product_id == $pro_id && $fn->stock_id == $stock_id);
                });

                if (count($cartSearch) > 0) {
                    $updateKey = array_keys($cartSearch)[0];
                }
            }

            if ($updateKey === null) {
                $cartArr[] = [
                    'id' => count($cartArr) + 1,
                    'product_id' => $pro_id,
                    'stock_id' => $stock_id,
                    'qty' => $qty,
                    'seller_id' => $seller_id
                ];
            } else {
                $cartArr[$updateKey] = [
                    'id' => $cartArr[$updateKey]->id,
                    'product_id' => $pro_id,
                    'stock_id' => $stock_id,
                    'qty' => ($cartArr[$updateKey]->qty + $qty),
                    'seller_id' => $seller_id
                ];
            }

            setcookie('user_cart', base64_encode(json_encode($cartArr)), 2147483647, "/", $_SERVER['SERVER_NAME']);

            $re_valid = 1;
        }
	}else{
		$re_valid = 0;
	}

	$count_cart = get_itemCart_count();
	$result = array('valid'=>$re_valid,'count_cart'=>$count_cart,'max_sqt'=>$product_quantity);

	// $result = 'ajax masuk';
	echo json_encode($result);
	die();
}   
add_action('wp_ajax_ajax_addCart','ajax_addCart');
add_action('wp_ajax_nopriv_ajax_addCart', 'ajax_addCart');


function ajax_addWishlist(){

	date_default_timezone_set("Asia/Jakarta");
	$nowdate = date('Y-m-d H:i:s');
	$current_user = wp_get_current_user();
	$u_id = $current_user->ID;
	$pro_id = $_REQUEST['pro_id'];
	$valid = 0;
	global $wpdb;
	$wpdb_query = "SELECT * FROM ldr_wishlist WHERE user_id = '$u_id' AND product_id = '$pro_id' ";
	$res_query = $wpdb->get_results($wpdb_query, OBJECT);
	$count_res = count($res_query);
	if($count_res == 0){
		global $wpdb;
		$query_insert = "INSERT INTO ldr_wishlist(user_id,product_id,created_date) VALUES($u_id,'$pro_id','$nowdate')";
		$result_insert = $wpdb->query($query_insert);
		$valid = 1;
	}

	$result = 'ajax masuk';
	echo json_encode($valid);
	die();
}   
add_action('wp_ajax_ajax_addWishlist','ajax_addWishlist');
add_action('wp_ajax_nopriv_ajax_addWishlist', 'ajax_addWishlist');


function ajax_removeWishlist(){

	date_default_timezone_set("Asia/Jakarta");
	$nowdate = date('Y-m-d H:i:s');
	$current_user = wp_get_current_user();
	$u_id = $current_user->ID;
	$pro_id = $_REQUEST['pro_id'];
	$valid = 0;
	global $wpdb;
	$wpdb_query = "SELECT * FROM ldr_wishlist WHERE user_id = '$u_id' AND product_id = '$pro_id' ";
	$res_query = $wpdb->get_results($wpdb_query, OBJECT);
	$count_res = count($res_query);
	if($count_res > 0){
		global $wpdb;
		$query = "DELETE FROM ldr_wishlist WHERE user_id = '$u_id' AND product_id = '$pro_id' ";
		$result = $wpdb->query($query);
		$valid = 1;
	}

	$result = 'ajax masuk';
	echo json_encode($valid);
	die();
}   
add_action('wp_ajax_ajax_removeWishlist','ajax_removeWishlist');
add_action('wp_ajax_nopriv_ajax_removeWishlist', 'ajax_removeWishlist');

function get_itemCart_count(){

	date_default_timezone_set("Asia/Jakarta");
	$nowdate = date('Y-m-d H:i:s');
	$current_user = wp_get_current_user();
	$u_id = $current_user->ID;
    $count_res = 0;

	$totalcount = 0;

    global $wpdb;
    
    if (isset($u_id) and $u_id != 0) {
        $wpdb_query = "SELECT * FROM ldr_cart WHERE user_id = '$u_id' ";
        $res_query = $wpdb->get_results($wpdb_query, OBJECT);
        $count_res = count($res_query);
    } else {
        $cartData = $_COOKIE['user_cart'] ?? null;

        if ($cartData !== null) {
            $res_query = json_decode(stripslashes(base64_decode($cartData)));
            $count_res = count($res_query);
        }
    }

	if($count_res > 0){
		foreach ($res_query as $key => $value) {
			$cart_qty = $value->qty;
			if(isset($cart_qty) AND $cart_qty != ''){
				$totalcount = (int)$totalcount+(int)$cart_qty;
			}
		}
	}else{
			$totalcount = 0;
			$data_cart = '';
			remove_UserCart($u_id);
	}

	return $totalcount;

}

function get_countCart(){

	date_default_timezone_set("Asia/Jakarta");
	$nowdate = date('Y-m-d H:i:s');
	$current_user = wp_get_current_user();
	$u_id = $current_user->ID;
	$totalcount = 0;

	global $wpdb;
	$wpdb_query = "SELECT * FROM ldr_cart WHERE user_id = '$u_id' ";
	$res_query = $wpdb->get_results($wpdb_query, OBJECT);
	$count_res = count($res_query);
	if($count_res > 0){
		foreach ($res_query as $key => $value) {
			$cart_qty = $value->qty;
			if(isset($cart_qty) AND $cart_qty != ''){
				$totalcount = (int)$totalcount+(int)$cart_qty;
			}
		}
	}else{
			$totalcount = 0;
			$data_cart = '';
			remove_UserCart($u_id);
	}

	echo json_encode($totalcount);
	die();
}
add_action('wp_ajax_get_countCart','get_countCart');
add_action('wp_ajax_nopriv_get_countCart', 'get_countCart');

function get_productCart($userId) {
    global $wpdb;
    $cartIds = $_SESSION['cartIds'];
    $cartData = '';

    if (intval($userId) !== 0) {
        $queryCart = $wpdb->get_results("SELECT * FROM ldr_cart WHERE user_id = '$userId' AND id IN ($cartIds)");
        $count = count($queryCart);

        if ($count > 0) {
            $cartData = $queryCart;
        }
    }

    return $cartData;
}

function remove_UserCart($user_id){

	global $wpdb;
	$query = "DELETE FROM ldr_cart WHERE user_id = '$user_id' ";
	$result = $wpdb->query($query);

}


function ajax_updateCart(){
    global $wpdb;
	date_default_timezone_set("Asia/Jakarta");
	$nowdate = date('Y-m-d H:i:s');
	$current_user = wp_get_current_user();
	$u_id = $current_user->ID;
	$key_id = $_REQUEST['key_id'];
    $status = $_REQUEST['status'];
    $new_qty = 0;
    $max_qty = 0;
    $cart = null;

    try {
        if (isset($u_id) and $u_id != 0) {
            $cart = $wpdb->get_row("SELECT * FROM ldr_cart WHERE user_id = $u_id AND id = $key_id");
        } else {
            $cartData = $_COOKIE['user_cart'];

            if ($cartData !== null) {
                $cartArr = json_decode(stripslashes(base64_decode($cartData)));
                $cartKey = array_search($key_id, array_column($cartArr, 'id'));
                $cart = $cartArr[$cartKey];
            }
        }

        if ($cart === null) {
            throw new Exception('Ga ada IDnya bos');
        }

        $tb_id = $cart->id;
        $tb_pro_id = $cart->product_id;
        $tb_stock_id = $cart->stock_id;
        $tb_qty = $cart->qty;

        $stock = $wpdb->get_row("SELECT * FROM ldr_stock WHERE id = $tb_stock_id");
        $max_qty = intval($stock->product_quantity);

        if (isset($status) and strtolower($status) == 'plus') {
            $new_qty = (int) $tb_qty + 1;
        } elseif (isset($status) and strtolower($status) == 'minus') {
            $new_qty = (int) $tb_qty - 1;
        }

        if ($new_qty === $max_qty) {
            $new_qty = $max_qty;
        } else if ($new_qty < 0) {
            throw new Exception('Ga bisa kurang bos');
        }

        if (isset($u_id) and $u_id != 0) {
            $wpdb->update('ldr_cart', [
                'qty' => $new_qty
            ], [
                'id' => $tb_id,
                'user_id' => $u_id,
                'stock_id' => $tb_stock_id
            ]);
        } else {
            $cart->qty = $new_qty;

            setcookie('user_cart', base64_encode(json_encode($cartArr)), 2147483647, "/", $_SERVER['SERVER_NAME']);
        }
    } catch (Exception $e) {
        echo json_encode('Gagal');
        die();
    }

    echo json_encode([
        'new_qty' => $new_qty,
        'max_qty' => $max_qty
    ]);
    die();
}   
add_action('wp_ajax_ajax_updateCart','ajax_updateCart');
add_action('wp_ajax_nopriv_ajax_updateCart', 'ajax_updateCart');


function ajax_deleteCartProduct(){
	date_default_timezone_set("Asia/Jakarta");
	$nowdate = date('Y-m-d H:i:s');
	$current_user = wp_get_current_user();
	$u_id = $current_user->ID;
	$key_id = $_REQUEST['key_id'];

    if (isset($u_id) and $u_id != 0) {
        global $wpdb;
        $wpdb_query = "SELECT * FROM ldr_cart WHERE user_id = '$u_id' AND id = '$key_id' ";
        $res_query = $wpdb->get_results($wpdb_query, OBJECT);
        $count_res = count($res_query);

        if ($count_res > 0) {
            global $wpdb;
            $query = "DELETE FROM ldr_cart WHERE user_id = '$u_id' AND id = '$key_id' ";
            $result = $wpdb->query($query);
            $valid = 1;
        } else {
            $valid = 0;
        }
    } else {
        $cartData = $_COOKIE['user_cart'];

        if ($cartData !== null) {
            $cartArr = json_decode(stripslashes(base64_decode($cartData)));
            $cartKey = array_search($key_id, array_column($cartArr, 'id'));
            unset($cartArr[$cartKey]);
            $cartArr = array_values($cartArr);

            setcookie('user_cart', base64_encode(json_encode($cartArr)), 2147483647, "/", $_SERVER['SERVER_NAME']);
        }
    }

	echo json_encode($valid);
	die();
}   
add_action('wp_ajax_ajax_deleteCartProduct','ajax_deleteCartProduct');
add_action('wp_ajax_nopriv_ajax_deleteCartProduct', 'ajax_deleteCartProduct');


function ajax_deleteCartSeller(){

	date_default_timezone_set("Asia/Jakarta");
	$nowdate = date('Y-m-d H:i:s');
	$current_user = wp_get_current_user();
	$u_id = $current_user->ID;
	$seller_id = $_REQUEST['sel_id'];
	
	global $wpdb;
	$wpdb_query = "SELECT * FROM ldr_cart WHERE user_id = '$u_id' AND seller_id = '$seller_id' ";
	$res_query = $wpdb->get_results($wpdb_query, OBJECT);
	$count_res = count($res_query);
	if($count_res > 0){

		global $wpdb;
		$query = "DELETE FROM ldr_cart WHERE user_id = '$u_id' AND seller_id = '$seller_id' ";
		$result = $wpdb->query($query);
		$valid = 1;
	}else{
		$valid = 0;
	}

	echo json_encode($valid);
	die();
}   
add_action('wp_ajax_ajax_deleteCartSeller','ajax_deleteCartSeller');
add_action('wp_ajax_nopriv_ajax_deleteCartSeller', 'ajax_deleteCartSeller');


function ajax_checkoutCart() {
    global $wpdb;
    date_default_timezone_set("Asia/Jakarta");
    $currentUser = wp_get_current_user();
    $userId = $currentUser->ID;
    $cartIds = implode(', ', json_decode(stripslashes($_REQUEST['cartIds'])));
    $valid = 0;
    
    if ($userId === 0) {
        echo json_encode($valid);
        die();
    }

    // For tracking which Tab make the request
    if (isset($_REQUEST['identifier'])) {
        $identifier = $_REQUEST['identifier'];
    } else {
        echo json_encode(0);
        die();
    }

    $cartQuery = $wpdb->get_results("SELECT * FROM ldr_cart WHERE user_id = $userId AND id IN ($cartIds)");

    foreach ($cartQuery as $value) {
        $stockId = $value->stock_id;
        $cartQty = intval($value->qty);

        $stock = $wpdb->get_row("SELECT * FROM ldr_stock WHERE id = $stockId");

        if ($stock === null) {
            echo json_encode([
                'id' => $value->id,
                'keterangan' => 'Product tidak ditemukan.'
            ]);

            die();
        } else if ($cartQty > intval($stock->product_quantity)) {
            echo json_encode([
                'id' => $value->id,
                'keterangan' => "Max {$stock->product_quantity} pcs."
            ]);

            die();
        }

        $valid = 1;

        try {
            global $woocommerce;
            $woocommerce->cart->add_to_cart(2474, $cartQty);
        } catch (Exception $e) {
            // Do nothing
        }
    }
    
    if ($valid === 1) {
        $_SESSION['cartIds'] = $cartIds;
        $_SESSION['identifier'] = $identifier;
        unset($_SESSION['checkoutAddressId'][$identifier]);
        unset($_SESSION['couriers'][$identifier]);
        unset($_SESSION['orderRequestBody'][$identifier]);
        unset($_SESSION["voucherId"][$identifier]);
        unset($_SESSION["shippingNote"][$identifier]);
    }

	echo json_encode($valid);
    die();
}   
add_action('wp_ajax_ajax_checkoutCart','ajax_checkoutCart');
add_action('wp_ajax_nopriv_ajax_checkoutCart', 'ajax_checkoutCart');

function ajax_saveNoAddress() {
    $addressId = $_REQUEST['ad_id'];
    $identifier = $_REQUEST['identifier'];

    if (intval($addressId) !== 0) {
        $_SESSION['checkoutAddressId'][$identifier] = $addressId;
        unset($_SESSION['couriers'][$identifier]);
    }

    echo json_encode(1);
    die();
}   
add_action('wp_ajax_ajax_saveNoAddress','ajax_saveNoAddress');
add_action('wp_ajax_nopriv_ajax_saveNoAddress', 'ajax_saveNoAddress');

function ajax_saveShipping() {
    $rateId = $_REQUEST['shipment_id'];
    $sellerId = $_REQUEST['seller_id'];
    $identifier = $_REQUEST['identifier'];

    if (intval($rateId) !== 0) {
        $_SESSION['couriers'][$identifier][$sellerId] = [
            'rate_id' => intval($rateId),
            'name' => $_REQUEST['name'],
            'rate_name' => $_REQUEST['rate_name'],
            'logo_url' => $_REQUEST['logo_url'],
            'type' => $_REQUEST['type'],
            'use_insurance' => $_REQUEST['use_insurance'] === 'true'
        ];
    }

    echo json_encode(1);
    die();
}   
add_action('wp_ajax_ajax_saveShipping','ajax_saveShipping');
add_action('wp_ajax_nopriv_ajax_saveShipping', 'ajax_saveShipping');

function ajax_saveCatatan()
{
    $identifier = $_REQUEST['identifier'];
    $_SESSION['shippingNote'][$identifier] = $_REQUEST['catatan'];
    
    echo json_encode(1);
    die();
}   
add_action('wp_ajax_ajax_saveCatatan','ajax_saveCatatan');
add_action('wp_ajax_nopriv_ajax_saveCatatan', 'ajax_saveCatatan');

function ajax_checkoutOrder_normal() {
    $nonce = $_REQUEST['_wpnonce'];
    if ( ! wp_verify_nonce( $nonce, 'CKNBP' ) ) {
        exit(); // Get out of here, the nonce is rotten!
    }

    // For tracking which Tab make the request
    if (isset($_REQUEST['identifier'])) {
        $identifier = $_REQUEST['identifier'];
    } else {
        echo json_encode(0);
        die();
    }

    global $wpdb;
    date_default_timezone_set("Asia/Jakarta");

    $currentUser = wp_get_current_user();
    $userId = $currentUser->ID;
    $shippingNote = '';
    $checkoutAddressId = '';
    $requestBody = [];

    if (isset($_SESSION['checkoutAddressId'])) {
        $checkoutAddressId = $_SESSION['checkoutAddressId'][$identifier];
    }

    if (isset($_SESSION['shippingNote'][$identifier])) {
        $shippingNote = $_SESSION['shippingNote'][$identifier];
    }

    $requestBody = [
        'address_id' => intval($checkoutAddressId),
        'voucher_id' => $_SESSION['voucherId'][$identifier] ?? null,
        'shipping_note' => $shippingNote
    ];

    $couriers = (isset($_SESSION['couriers']) ? $_SESSION['couriers'][$identifier] : []);

    $cartData = get_productCart($userId);

    try {
        if ($identifier == '') {
            throw new Exception("Error Processing Request.");
        } else if (intval($checkoutAddressId) === 0) {
            throw new Exception("Alamat invalid.");
        }

        foreach ($cartData as $value) {
            $allDataCart[$value->seller_id][] = $value;
        }

        foreach ($allDataCart as $sellerId => $carts) {
            $seller = $wpdb->get_row("SELECT * FROM ldr_merchants WHERE id = $sellerId AND deleted_at IS NULL");
            $tempCart = [];

            if ($seller === null) {
                throw new Exception('Merchant tidak ditemukan.');
            }

            foreach ($carts as $cart) {
                $tempCart[] = [
                    'id' => intval($cart->id)
                ];
            }

            $requestBody['data'][] = [
                'carts' => $tempCart,
                'courier' => [
                    'rate_id' => $couriers[$seller->id]['rate_id'],
                    'name' => $couriers[$seller->id]['name'],
                    'rate_name' => $couriers[$seller->id]['rate_name'],
                    'logo_url' => $couriers[$seller->id]['logo_url'],
                    'type' => $couriers[$seller->id]['type'],
                    'use_insurance' => $couriers[$seller->id]['use_insurance']
                ]
            ];
        }

        $checkItemStock = sendRequest('POST', MERCHANT_URL . '/api/orders/check-item-stock', $requestBody);
        
        if ($checkItemStock->status !== 200) {
            echo json_encode($checkItemStock);
            die();
        } else {
            $checkMerchantStatus = sendRequest('POST', MERCHANT_URL . '/api/orders/check-merchant-status', $requestBody);

            if ($checkMerchantStatus->status !== 200) {
                echo json_encode($checkMerchantStatus);
                die();
            }
        }

        $_SESSION['orderRequestBody'][$identifier] = $requestBody;
    } catch (Exception $e) {
        http_response_code(500);

        echo json_encode($e->getMessage());
        die();
    }

    echo json_encode(1);
    die();
}   
add_action('wp_ajax_ajax_checkoutOrder_normal','ajax_checkoutOrder_normal');
add_action('wp_ajax_nopriv_ajax_checkoutOrder_normal', 'ajax_checkoutOrder_normal');

function ajax_updateProfilePicture(){

	date_default_timezone_set("Asia/Jakarta");
	$nowdate = date('Y-m-d H:i:s');
	$strnow = strtotime($nowdate);
	$current_user = wp_get_current_user();
	$u_id = $current_user->ID;

	$files = $_FILES['file'];
	$filename = $_FILES['file']['name'];

	// These files need to be included as dependencies when on the front end.
	require_once( ABSPATH . 'wp-admin/includes/image.php' );
	require_once( ABSPATH . 'wp-admin/includes/file.php' );
	require_once( ABSPATH . 'wp-admin/includes/media.php' );

	$newfilename = $u_id.'-'.$strnow.'.jpg';
	$uploadedfile = [
		'name' => esc_attr($newfilename),
	    'type' => $files['type'],
	    'tmp_name' => esc_attr($files['tmp_name']),
	    'error' => $files['error'],
	    'size' => $files['size']
	];


	$valid = 0;
	$upload_overrides = array( 'test_form' => false );
	// $time = "profile/";
	$movefile = wp_handle_upload($uploadedfile, $upload_overrides);
	if ( $movefile && ! isset( $movefile['error'] ) ) {
	    $src = media_sideload_image( $movefile['url'], null, null, 'src' );
	    $image_id = attachment_url_to_postid( $src );

	    update_field('user_avatar',$image_id, 'user_'.$u_id);

	    $valid = 1;
	    // ============= update field images order with id media ============
	}

	$result = 'Masuk pak eko';
	echo json_encode($valid);
	die();
}
add_action('wp_ajax_ajax_updateProfilePicture','ajax_updateProfilePicture');
add_action('wp_ajax_nopriv_ajax_updateProfilePicture', 'ajax_updateProfilePicture');

function ajax_getProductWatchlist(){

	$current_user = wp_get_current_user();
	$u_id = $current_user->ID;

	$keyword = $_REQUEST['keyword'];
	$offset = $_REQUEST['offset'];
	$limit_item = $_REQUEST['limit'];
	$html = '';
	$valid = 0;
	$count_allproduct = 0;

	$keyword = str_replace(' ', '%', $keyword);

	global $wpdb;
	$wpdb_query = "SELECT * 
					FROM ldr_wishlist
					WHERE user_id = '$u_id'
					ORDER BY product_id DESC
					LIMIT $limit_item OFFSET $offset";
	$res_query = $wpdb->get_results($wpdb_query, OBJECT);
	$count_res = count($res_query);

	if($count_res > 0){
		foreach ($res_query as $key => $value){
			$valid = 1;
			$product_id = $value->product_id;
			$product = wc_get_product( $product_id );

			$product_name = get_the_title($product_id);
			$short_name = get_the_title($product_id);
			if(strlen($short_name) > 35) $short_name = substr($short_name, 0, 35).'...';
			$product_link = get_the_permalink($product_id);
			$product_price = $product->get_regular_price();
			$product_sale = $product->get_sale_price();

			$thumb = wp_get_attachment_image_src( get_post_thumbnail_id($product_id), 'medium' );
			if($thumb){
				$urlphoto = $thumb['0'];
			}else{
				$urlphoto = get_template_directory_uri().'/library/images/default_sorry.jpg';
            }
            
            if (isset($product_sale) AND $product_sale != 0) {
                $pr_diskon = (((int)$product_price - (int)$product_sale ) / (int)$product_price ) * 100;
                $pr_diskon = round($pr_diskon, 2);
            } else {
                $pr_diskon = 0;
            }

			$def_image = get_template_directory_uri().'/library/images/default_sorry.jpg';

			$alt = 'Ladara Indonesia Gallery';

			$html .= '<div class="col-xs-6 col-md-4 col_bx_productList">
							<div class="bx_productList">';
							
								if ($u_id != 0) {
									global $wpdb;
									$wpdb_query = "SELECT * FROM ldr_wishlist WHERE user_id = '$u_id' AND product_id = '$product_id' ";
									$res_query = $wpdb->get_results($wpdb_query, OBJECT);
									$count_res = count($res_query);
									if($count_res == 0){
										$love_1 = 'act';
										$love_2 = '';
										
										$html .= '<div class="mg_heart addWishlist love_'.$product_id.'" title="Tambah ke wishlist" data-id="'.$product_id.'">
											<span class="glyphicon glyphicon-heart"></span>
										</div>';
									}else{
										$love_2 = 'act';	
										
										$html .= '<div id="btn_" class="mg_heart removewishlist love_'.$product_id.' act" title="Hapus dari wishlist" data-id="'.$product_id.'" >
											<span class="glyphicon glyphicon-heart"></span>
										</div>';
									}
								}
								
								$html .= '<a href="'.$product_link.'" title="Lihat '.$product_name.'">
											<div class="mg_productList">
												<img src="'.$urlphoto.'" alt="'.$alt.'">
											</div>
											<div class="bx_in_productList">
												<h4 class="ht_productList">'.$short_name.'</h4>';
								
											if(isset($product_sale) AND $product_sale != ''){ 
                                                $html .= '<div class="tx_price">Rp '.number_format($product_sale).'</div>';
                                                    
                                                if (intval($product_sale) !== intval($product_price)) {
                                                    $html .= '<div class="tx_price_sale" style="text-decoration: none;">
                                                                <span style="text-decoration: line-through;">Rp '.number_format($product_price).'</span>
                                                                <span class="discount">'.$pr_diskon.'%</span>
                                                             </div>';
                                                }
										 	}else{
										 		$html .= '<div class="tx_price">Rp '.number_format($product_price).'</div>';
											
											}

										$html .= '<div class="bx_rate_product">';
 										
											// $pro_rate = get_field('product_rate',$product_id);
											// $total_product_rate = get_field('total_product_rate',$product_id);
											// $count_rate = $total_product_rate;
											// $full_star = $pro_rate;
											// $empty_star = 5;
											// if($count_rate != 0){
											// 	for ($a=1; $a <= $empty_star; $a++) { 
											// 		if($a <= $full_star){
											// 			$html .= '<img src="'.get_template_directory_uri().'/library/images/star1.svg">';
											// 		}else{
											// 			$html .= '<img src="'.get_template_directory_uri().'/library/images/star0.svg">';
											// 		}
											// 	} 
												
											// 	$html .= '<span class="sp_rate_info">('.$count_rate.')</span>';
											
											// }
											
								$html .= '</div>
									</div>
								</a>	
							</div>
						</div>';
				
		}
	}else{
		$valid = 0;
        $html .= '<div class="row row_masterInformation">
			            <div class="col-md-12 col_emptyInformation">
			                <div class="mg_emptyInformation">
			                    <img src="'.get_template_directory_uri().'/library/images/no_product.svg">
			                </div>
                            <h2 class="tx_emptyInformation">Oopss!!!</h2>
                            <h5>Kamu tidak memiliki daftar keinginan.</h5>
			            </div>
			        </div>';

	}


	global $wpdb;
	$wpdb_query2 = "SELECT * 
					FROM ldr_wishlist
					WHERE user_id = '$u_id'
					ORDER BY product_id DESC
					LIMIT $limit_item OFFSET $offset";
	$res_query2 = $wpdb->get_results($wpdb_query2, OBJECT);
	$count_allproduct = count($res_query2);

	$return = array(
					'html' => $html,
					'count_allproduct' => $count_allproduct,
					'valid' => $valid
					);

	
	// $valid = 'masuk pak eko';
	echo json_encode($return);
	die();
}   
add_action('wp_ajax_ajax_getProductWatchlist','ajax_getProductWatchlist');
add_action('wp_ajax_nopriv_ajax_getProductWatchlist', 'ajax_getProductWatchlist');


function get_info_noproducts(){

	$html = '';
	$html .= '<div class="row row_masterInformation">
				            <div class="col-md-12 col_emptyInformation">
				                <div class="mg_emptyInformation">
				                    <img src="'.get_template_directory_uri().'/library/images/no_product.svg">
				                </div>
				                <h2 class="tx_emptyInformation">Ups, produk yang kamu cari tidak ditemukan :(</h2>
				                <div class="inf_emptyInformation">Coba kata kunci lain.</div>
				            </div>
				        </div>';

	return $html;

}

/**
 * @deprecated
 * pindah ke ajax_getProductList
 */
function ajax_getProductListDeprecated(){
	$current_user = wp_get_current_user();
	$u_id = $current_user->ID;

	$keyword = $_REQUEST['keyword'];
	$offset = $_REQUEST['offset'] ?? 0;
	$limit_item = $_REQUEST['limit'] ?? 20;
	$category = $_REQUEST['category'];

	$minrange = $_REQUEST['minrange'];
	$maxrange = $_REQUEST['maxrange'];
	$filter_sortby = $_REQUEST['filter_sortby'];

	if(isset($filter_sortby) AND $filter_sortby == 'asc'){
		$sort_query = "ORDER BY a.post_title ASC";
	}else if(isset($filter_sortby) AND $filter_sortby == 'desc'){
		$sort_query = "ORDER BY a.post_title DESC";
	}else if(isset($filter_sortby) AND $filter_sortby == 'termurah'){
		// $sort_query = "ORDER BY b.price ASC";
		$sort_query = "ORDER BY CAST(price AS UNSIGNED) ASC";
	}else{
		$sort_query = "ORDER BY a.id DESC";
	}

	$html = '';
	$html_brand = '';
	$all_brand = array();
	$valid = 0;
	$count_allproduct = 0;

	if($keyword != ''){
		$keyword = str_replace(' ', '%', $keyword);
		// $like_query = "AND ( a.post_name LIKE '%".$keyword."%' OR a.post_content LIKE '%".$keyword."%' )";
		$like_query = "AND (a.post_name LIKE '%".$keyword."%' OR ldr_terms.name LIKE '$keyword%')";
	}else{
		$like_query = "";
	}

	if($minrange != '' AND $maxrange != ''){
		$rangeprice_query = "AND ( b.meta_key = '_regular_price' AND CAST(b.meta_value AS UNSIGNED) BETWEEN '$minrange' AND '$maxrange' )
							AND  ( c.meta_key = '_sale_price' AND CAST(c.meta_value AS UNSIGNED) BETWEEN '$minrange' AND '$maxrange' ) ";
	}else{
		$rangeprice_query = '';
	}

	if( (isset($category) AND $category != '') AND ($keyword == '')  ){ // for category product

		$cat_key = str_replace('-', '%', $category);
		$all_category = str_replace(' ', '%', $cat_key);

		global $wpdb;
		$wpdb_query = "SELECT DISTINCT a.id AS id,
	                  a.post_title AS post_title,
	                  IFNULL(NULLIF(c.meta_value, '' ), b.meta_value) AS price
		                FROM ldr_posts a
		                LEFT JOIN ldr_postmeta b
		                  ON b.post_id = a.ID
		                  AND b.meta_key = '_regular_price'
		               	LEFT JOIN ldr_postmeta c
							ON c.post_id = a.id
							AND c.meta_key = '_sale_price'
		                LEFT JOIN ldr_term_relationships
		                  ON ldr_term_relationships.object_id = a.ID
		                LEFT JOIN ldr_term_taxonomy
		                  ON ldr_term_relationships.term_taxonomy_id = ldr_term_taxonomy.term_taxonomy_id
		                  AND ldr_term_taxonomy.taxonomy = 'product_cat'
		                LEFT JOIN ldr_terms
		                  ON ldr_term_taxonomy.term_id = ldr_terms.term_id
		                WHERE a.post_type = 'product'
		                AND ldr_terms.name LIKE '%$all_category%'
						$rangeprice_query
						AND a.post_status = 'publish'
						$sort_query
						LIMIT $limit_item OFFSET $offset";

		// echo "<pre>";
		// print_r($wpdb_query);
		// echo "</pre>";

		$res_query = $wpdb->get_results($wpdb_query, OBJECT);
		$count_res = count($res_query);

		if($count_res > 0){
			foreach ($res_query as $key => $value){
				$valid = 1;
				$product_id = $value->id;

				// ======= get brand list ========
				$product_brand = get_field('product_brand',$product_id);
				if(isset($product_brand) AND !empty($product_brand)){
					$brand_id = $product_brand->ID;
					$brand_title = $product_brand->post_title;
					$all_brand[$brand_id] = $brand_title;
				}
				// ======= get brand list ========

				$product = get_post($product_id);
				$product_name = $value->post_title;
				$short_name = $value->post_title;
				if(strlen($short_name) > 35) $short_name = substr($short_name, 0, 35).'...';
				$product_link = get_the_permalink($product_id);
				$product_price = get_post_meta($product_id, '_regular_price', true);
				$product_sale = get_post_meta($product_id, '_sale_price', true);
				$thumb = wp_get_attachment_image_src( get_post_thumbnail_id($product_id), 'medium' );
				if($thumb){
					$urlphoto = $thumb['0'];
				}else{
					$urlphoto = get_template_directory_uri().'/library/images/default_sorry.jpg';
                }

                if (isset($product_sale) AND $product_sale != 0) {
                    $pr_diskon = (((int)$product_price - (int)$product_sale ) / (int)$product_price ) * 100;
                    $pr_diskon = round($pr_diskon, 2);
                } else {
                    $pr_diskon = 0;
                }

				$def_image = get_template_directory_uri().'/library/images/default_sorry.jpg';
				$alt = 'Ladara Indonesia Gallery';
				$html .= '<div class="col-xs-6 col-md-4 col_bx_productList">
								<div class="bx_productList">';

									if ($u_id != 0) {
										global $wpdb;
										$wpdb_query = "SELECT * FROM ldr_wishlist WHERE user_id = '$u_id' AND product_id = '$product_id' ";
										$res_query = $wpdb->get_results($wpdb_query, OBJECT);
										$count_res = count($res_query);
										if($count_res == 0){
											$love_1 = 'act';
											$love_2 = '';

											$html .= '<div class="mg_heart addWishlist love_'.$product_id.'" title="Tambah ke wishlist" data-id="'.$product_id.'">
												<span class="glyphicon glyphicon-heart"></span>
											</div>';
										}else{
											$love_2 = 'act';

											$html .= '<div id="btn_" class="mg_heart removewishlist love_'.$product_id.' act" title="Hapus dari wishlist" data-id="'.$product_id.'" >
												<span class="glyphicon glyphicon-heart"></span>
											</div>';
										}
									}

									$html .= '<a href="'.$product_link.'" title="Lihat '.$product_name.'">
												<div class="mg_productList">
													<img src="'.$urlphoto.'" alt="'.$alt.'">
												</div>
												<div class="bx_in_productList">
													<h4 class="ht_productList">'.$short_name.'</h4>';
									
												if(isset($product_sale) AND $product_sale != ''){ 
                                                    $html .= '<div class="tx_price">Rp '.number_format($product_sale).'</div>';
                                                    
                                                    if (intval($product_sale) !== intval($product_price)) {
                                                        $html .= '<div class="tx_price_sale" style="text-decoration: none;">
                                                                    <span style="text-decoration: line-through;">Rp '.number_format($product_price).'</span>
                                                                    <span class="discount">'.$pr_diskon.'%</span>
                                                                 </div>';
                                                    }
											 	} else {
											 		$html .= '<div class="tx_price">Rp '.number_format($product_price).'</div>';

												}

											$html .= '<div class="bx_rate_product">';

									$html .= '</div>
										</div>
									</a>
								</div>
							</div>';

			}
		}else{
			$valid = 0;
	        $html .= get_info_noproducts(); // get no products info
		}

		global $wpdb;
		$wpdb_query2 = "SELECT DISTINCT a.id AS id,
	                  a.post_title AS post_title,
	                  IFNULL(NULLIF(c.meta_value, '' ), b.meta_value) AS price
		                FROM ldr_posts a
		                LEFT JOIN ldr_postmeta b
		                  ON b.post_id = a.ID
		                  AND b.meta_key = '_regular_price'
		               	LEFT JOIN ldr_postmeta c
							ON c.post_id = a.id
							AND c.meta_key = '_sale_price'
		                LEFT JOIN ldr_term_relationships
		                  ON ldr_term_relationships.object_id = a.ID
		                LEFT JOIN ldr_term_taxonomy
		                  ON ldr_term_relationships.term_taxonomy_id = ldr_term_taxonomy.term_taxonomy_id
		                  AND ldr_term_taxonomy.taxonomy = 'product_cat'
		                LEFT JOIN ldr_terms
		                  ON ldr_term_taxonomy.term_id = ldr_terms.term_id
		                WHERE a.post_type = 'product'
		                AND ldr_terms.name LIKE '%$all_category%'
						$rangeprice_query
						AND a.post_status = 'publish'
						$sort_query";

		$res_query2 = $wpdb->get_results($wpdb_query2, OBJECT);
		$count_allproduct = count($res_query2);

	}else{ // for normal product
			// =========== most product viewed ==============
			global $wpdb;
			$wpdb_query = "SELECT DISTINCT a.id, a.post_date, a.post_title, b.meta_key , IFNULL(NULLIF(c.meta_value, '' ), b.meta_value) AS price
							FROM ldr_posts a
							LEFT JOIN ldr_postmeta b
							ON b.post_id = a.id
							AND b.meta_key = '_regular_price'
							LEFT JOIN ldr_postmeta c
							ON c.post_id = a.id
							AND c.meta_key = '_sale_price'
                            LEFT JOIN ldr_term_relationships
                              ON ldr_term_relationships.object_id = a.ID
                            LEFT JOIN ldr_term_taxonomy
                              ON ldr_term_relationships.term_taxonomy_id = ldr_term_taxonomy.term_taxonomy_id
                              AND ldr_term_taxonomy.taxonomy = 'product_tag'
                            LEFT JOIN ldr_terms
                              ON ldr_term_taxonomy.term_id = ldr_terms.term_id
							WHERE 1=1  
							AND a.post_type = 'product' 
							$like_query
							$rangeprice_query
							AND a.post_status = 'publish'
							$sort_query
							LIMIT $limit_item OFFSET $offset";

		// echo "<pre>";
		// print_r($wpdb_query);
		// echo "</pre>";

		// 	$wpdb_query = "SELECT
		// 						a.id,
		// 						a.post_date,
		// 						a.post_title,
		// 							(
		// 							SELECT
		// 								CAST(meta_value AS UNSIGNED)
		// 							FROM
		// 								ldr_postmeta
		// 							WHERE
		// 								post_id = a.id AND
		// 								CASE
		// 									WHEN (meta_key = '_sale_price'
		// 									AND meta_value = '') THEN meta_key = '_reguler_price'
		// 									ELSE meta_key = '_sale_price'
		// 								END
		// 							GROUP BY post_id
		// 							) AS price
		// 					FROM ldr_posts a
		// 					WHERE a.post_type = 'product'
		// 					$like_query
		// 					$rangeprice_query
		// 					$sort_query
		// 					LIMIT $limit_item OFFSET $offset";

		// echo "<pre>";
		// print_r($wpdb_query);
		// echo "</pre>";

		$res_query = $wpdb->get_results($wpdb_query, OBJECT);
		$count_res = count($res_query);

		if($count_res > 0){
			foreach ($res_query as $key => $value){
				$valid = 1;
				$product_id = $value->id;

				// ======= get brand list ========
				$product_brand = get_field('product_brand',$product_id);
				if(isset($product_brand) AND !empty($product_brand)){
					$brand_id = $product_brand->ID;
					$brand_title = $product_brand->post_title;
					$all_brand[$brand_id] = $brand_title;
				}
				// ======= get brand list ========

				$product = wc_get_product( $product_id );
				$product_name = $value->post_title;
				$short_name = $value->post_title;
				if(strlen($short_name) > 35) $short_name = substr($short_name, 0, 35).'...';
				$product_link = get_the_permalink($product_id);
				$product_price = $product->get_regular_price();
				$product_sale = $product->get_sale_price();
				$thumb = wp_get_attachment_image_src( get_post_thumbnail_id($product_id), 'medium' );
				if($thumb){
					$urlphoto = $thumb['0'];
				}else{
					$urlphoto = get_template_directory_uri().'/library/images/default_sorry.jpg';
                }

                if (isset($product_sale) AND $product_sale != 0) {
                    $pr_diskon = (((int)$product_price - (int)$product_sale ) / (int)$product_price ) * 100;
                    $pr_diskon = round($pr_diskon, 2);
                } else {
                    $pr_diskon = 0;
                }

				$def_image = get_template_directory_uri().'/library/images/default_sorry.jpg';
				$alt = 'Ladara Indonesia Gallery';
				$html .= '<div class="col-xs-6 col-md-4 col_bx_productList">
								<div class="bx_productList">';

									if ($u_id != 0) {
										global $wpdb;
										$wpdb_query = "SELECT * FROM ldr_wishlist WHERE user_id = '$u_id' AND product_id = '$product_id' ";
										$res_query = $wpdb->get_results($wpdb_query, OBJECT);
										$count_res = count($res_query);
										if($count_res == 0){
											$love_1 = 'act';
											$love_2 = '';

											$html .= '<div class="mg_heart addWishlist love_'.$product_id.'" title="Tambah ke wishlist" data-id="'.$product_id.'">
												<span class="glyphicon glyphicon-heart"></span>
											</div>';
										}else{
											$love_2 = 'act';

											$html .= '<div id="btn_" class="mg_heart removewishlist love_'.$product_id.' act" title="Hapus dari wishlist" data-id="'.$product_id.'" >
												<span class="glyphicon glyphicon-heart"></span>
											</div>';
										}
									}

									$html .= '<a href="'.$product_link.'" title="Lihat '.$product_name.'">
												<div class="mg_productList">
													<img src="'.$urlphoto.'" alt="'.$alt.'">
												</div>
												<div class="bx_in_productList">
													<h4 class="ht_productList">'.$short_name.'</h4>';

												if(isset($product_sale) AND $product_sale != ''){
													$html .= '<div class="tx_price">Rp '.number_format($product_sale).'</div>';

                                                    if (intval($product_sale) !== intval($product_price)) {
                                                        $html .= '<div class="tx_price_sale" style="text-decoration: none;">
                                                                    <span style="text-decoration: line-through;">Rp '.number_format($product_price).'</span>
                                                                    <span class="discount">'.$pr_diskon.'%</span>
                                                                 </div>';
                                                    }
											 	}else{
											 		$html .= '<div class="tx_price">Rp '.number_format($product_price).'</div>';

												}
											$html .= '<div class="bx_rate_product">';

									$html .= '</div>
										</div>
									</a>
								</div>
							</div>';

			}
		}else{
			$valid = 0;
	        $html .= get_info_noproducts(); // get no products info
		}

		global $wpdb;
		$wpdb_query2 = "SELECT DISTINCT a.id, a.post_date, a.post_title, b.meta_key , IFNULL(NULLIF(c.meta_value, '' ), b.meta_value) AS price
							FROM ldr_posts a
							LEFT JOIN ldr_postmeta b
							ON b.post_id = a.id
							AND b.meta_key = '_regular_price'
							LEFT JOIN ldr_postmeta c
							ON c.post_id = a.id
							AND c.meta_key = '_sale_price'
                            LEFT JOIN ldr_term_relationships
                              ON ldr_term_relationships.object_id = a.ID
                            LEFT JOIN ldr_term_taxonomy
                              ON ldr_term_relationships.term_taxonomy_id = ldr_term_taxonomy.term_taxonomy_id
                              AND ldr_term_taxonomy.taxonomy = 'product_tag'
                            LEFT JOIN ldr_terms
                              ON ldr_term_taxonomy.term_id = ldr_terms.term_id
							WHERE 1=1
							AND a.post_type = 'product'
							$like_query
							$rangeprice_query
							AND a.post_status = 'publish'
							$sort_query";
		$res_query2 = $wpdb->get_results($wpdb_query2, OBJECT);
		$count_allproduct = count($res_query2);

	}

	// ================ GET ALL BRAND ON LIST ================
	if(isset($all_brand) AND !empty($all_brand)){
		foreach ($all_brand as $key_brand => $value_brand) {
			$brand_id = $key_brand;
			$brand_title = $value_brand;
			if($brand_id != ''){
				$html_brand .= '<div class="f_aform f_aform_brand">
								<label class="cont_check">'.$brand_title.'
								<input type="checkbox" name="filter_brand" class="check_catVoucher click_brand" value="'.$brand_id.'">
								<span class="checkmark"></span>
								</label>
							</div>	';
			}

		}
	}
	// ================ GET ALL BRAND ON LIST ================

	$return = array(
					'html' => $html,
					'html_brand' => $html_brand,
					'count_allproduct' => $count_allproduct,
					'valid' => $valid
					);


	// $valid = 'masuk pak eko';
	echo json_encode($return);
	die();
}

function cardProduct($products=[]){
    $html = '';
    foreach ($products as $product){
        $product_id = $product->id ?? '';
        $product_name = $product->name ?? '';
        $product_link = home_url().'/product/'.$product->permalink.'-'.$product->id ?? '';
        $urlphoto = $product->images ?? '';
        $urlphoto = $product->images ?? '';
        $product_sale = $product->sale_price;
        $product_price = $product->price;

        $html .= '<div class="col-xs-6 col-md-4 col_bx_productList">
					<div class="bx_productList">';
            if(!$product->wishlist){
                $html .= '<div class="mg_heart addWishlist love_'.$product_id.'" title="Tambah ke wishlist" data-id="'.$product_id.'">
                            <span class="glyphicon glyphicon-heart"></span>
                        </div>';
            }else{
                $html .= '<div id="btn_" class="mg_heart removewishlist love_'.$product_id.' act" title="Hapus dari wishlist" data-id="'.$product_id.'" >
                            <span class="glyphicon glyphicon-heart"></span>
                        </div>';
            }

        $html .= '<a href="'.$product_link.'" title="Lihat '.$product_name.'">
                    <div class="mg_productList">
                        <img src="'.$urlphoto.'" alt="'.$product_name.'">
                    </div>
                    <div class="bx_in_productList">
                        <h4 class="ht_productList">'.$product_name.'</h4>';

        if(isset($product_sale) AND $product_sale != ''){
            $html .= '<div class="tx_price">Rp '.number_format($product_sale).'</div>';

            if (intval($product_sale) !== intval($product_price)) {
                $html .= '<div class="tx_price_sale" style="text-decoration: none;">
                                <span style="text-decoration: line-through;">Rp '.number_format($product_price).'</span>
                                <span class="discount">'.$product->discount->text.'</span>
                             </div>';
            }
        }else{
            $html .= '<div class="tx_price">Rp '.number_format($product_price).'</div>';

        }
        $html .= '<div class="bx_rate_product">';

        $html .= '</div>
                            </div>
                        </a>
                    </div>
                </div>';
    }

    return $html;

}

function getBrandsList($brands = []){
    $html_brand = '';
    foreach ($brands as $brand) {
        $html_brand .= '<div class="f_aform f_aform_brand">
                        <label class="cont_check">'.$brand->title.'
                        <input type="checkbox" name="filter_brand" class="check_catVoucher click_brand" value="'.$brand->id.'">
                        <span class="checkmark"></span>
                        </label>
                    </div>	';

    }

    return $html_brand;
}

function ajax_getProductList(){

    $client = new GuzzleHttp\Client();

    $keyword = $_REQUEST['keyword'];
    $offset = $_REQUEST['offset'] ?? 0;
    $limit_item = $_REQUEST['limit'] ?? 20;
    $page = $offset == 0 ? 1 : ($offset / $limit_item) + 1;
    $category = $_REQUEST['category'];

    $minrange = $_REQUEST['minrange'];
    $maxrange = $_REQUEST['maxrange'];
    $filter_sortby = $_REQUEST['filter_sortby']; //termurah, asc, desc, terkait
    $all_brand = $_REQUEST['all_data'];
    $brand_id = $_REQUEST['brand'];

    $params = [
        'limit' => (int)$limit_item,
        'page' => $page,
        'list_brand' => true
    ];
    if ($filter_sortby == 'termurah'){
        $params['price'] = 'asc';
    }
    if ($filter_sortby == 'asc' || $filter_sortby == 'desc'){
        $params['name'] = $filter_sortby;
    }
    //filter price range
    if ($minrange && $maxrange){
        $params['price_range'] = "[".(int)$minrange.",".(int)$maxrange."]";
    }

    //filter brands
    if ($all_brand){
        $all_brand = rtrim($all_brand,',');
        $all_brand = explode(',', $all_brand);
        $params['brand_id'] = json_encode($all_brand);
    }
    if ($brand_id){
        $all_brand = explode(',', $brand_id);
        $params['brand_id'] = json_encode($all_brand);
    }
    if ($category != ''){
        //pencarian by category
        $exCate = explode('-', $category);
        $params['category'] = (int)end($exCate);
    }else{
        //pencarian by keyword
        $params['title'] = $keyword;
    }
    try {
        $req = $client->request('GET', MERCHANT_URL . '/api/product/search-products',[
                'query' => $params
        ]);
        $response = json_decode($req->getBody());
    } catch (\GuzzleHttp\Exception\GuzzleException $e) {
        error_log($e->getMessage());
        $response = [];
    }

    $htmlProduct = '<div class="row row_masterInformation">
				            <div class="col-md-12 col_emptyInformation">
				                <div class="mg_emptyInformation">
				                    <img src="'.get_template_directory_uri().'/library/images/no_product.svg">
				                </div>
				                <h2 class="tx_emptyInformation">Tidak ada produk yang Anda cari.</h2>
				            </div>
				        </div>';

    if (count($response->data->products) > 0){
        $htmlProduct = cardProduct($response->data->products);
    }

    $return = [
        'html' => $htmlProduct,
        'html_brand' => count($response->data) > 0 ? getBrandsList($response->data->brands) : [],
        'count_allproduct' => $response->data->page->total ?? 0,
        'valid' => count($response->data->products) > 0 ? 1 : 0
   ];
    echo json_encode($return);
    die();
}
add_action('wp_ajax_ajax_getProductList','ajax_getProductList');
add_action('wp_ajax_nopriv_ajax_getProductList', 'ajax_getProductList');


function ajax_getProductPromo(){
	$current_user = wp_get_current_user();
	$u_id = $current_user->ID;

	$keyword = $_REQUEST['keyword'];
	$offset = $_REQUEST['offset'];
	$limit_item = $_REQUEST['limit'];
	$promo_id = $_REQUEST['promo_id'];
	$html = '';
	$valid = 0;
	$count_allproduct = 0;

	$off_1 = $offset;
	$off_2 = (int)$offset+(int)$limit_item;

	$keyword = str_replace(' ', '%', $keyword);

//	$data_product = get_field('list_promo_products',$promo_id);
	$data_product = get_field('produk_pilihan',$promo_id);



	if (isset($data_product) AND !empty($data_product)) {

		$count_allproduct = count($data_product);

        $resp = sendRequest('GET', MERCHANT_URL.'/api/product/search-products',[
            'id' => json_encode($data_product),
            'limit' => 10000
        ]);
        $products = $resp->data->products ?? [];

        foreach ($products as $key => $product){
            if( ((int)$key >= (int)$off_1) AND ((int)$key < (int)$off_2) ){
                $valid = 1;
                $product_id = $product->id;
                $product_link = '/product/'.$product->permalink.'-'.$product_id;
                $product_name = $product->name;
                $urlphoto = $product->images;
                $short_name = $product->name;
                $product_sale = $product->sale_price;
                $product_price = $product->price;
                $pr_diskon = $product->discount->text;

                $alt = 'Ladara Indonesia Gallery';

                $html .= '<div class="col-xs-6 col-md-3 col_bx_productList">
								<div class="bx_productList">';

                if ($u_id != 0) {
                    global $wpdb;
                    $wpdb_query = "SELECT * FROM ldr_wishlist WHERE user_id = '$u_id' AND product_id = '$product_id' ";
                    $res_query = $wpdb->get_results($wpdb_query, OBJECT);
                    $count_res = count($res_query);
                    if($count_res == 0){
                        $love_1 = 'act';
                        $love_2 = '';

                        $html .= '<div class="mg_heart addWishlist love_'.$product_id.'" title="Tambah ke wishlist" data-id="'.$product_id.'">
												<span class="glyphicon glyphicon-heart"></span>
											</div>';
                    }else{
                        $love_2 = 'act';

                        $html .= '<div id="btn_" class="mg_heart removewishlist love_'.$product_id.' act" title="Hapus dari wishlist" data-id="'.$product_id.'" >
												<span class="glyphicon glyphicon-heart"></span>
											</div>';
                    }
                }

                $html .= '<a href="'.$product_link.'" title="Lihat '.$product_name.'">
												<div class="mg_productList">
													<img src="'.$urlphoto.'" alt="'.$alt.'">
												</div>
												<div class="bx_in_productList">
													<h4 class="ht_productList">'.$short_name.'</h4>';

                if(isset($product_sale) AND $product_sale != ''){
                    $html .= '<div class="tx_price">Rp '.number_format($product_sale).'</div>';

                    if (intval($product_sale) !== intval($product_price)) {
                        $html .= '<div class="tx_price_sale" style="text-decoration: none;">
                                                                    <span style="text-decoration: line-through;">Rp '.number_format($product_price).'</span>
                                                                    <span class="discount">'.$pr_diskon.'</span>
                                                                 </div>';
                    }
                }else{
                    $html .= '<div class="tx_price">Rp '.number_format($product_price).'</div>';

                }

                $html .= '<div class="bx_rate_product">';
                $html .= '</div>
										</div>
									</a>	
								</div>
							</div>';
            }
        }

//		foreach ($data_product as $key => $value) {
//			$product_id = $value;
//			$valid = 1;
//
//			if( ((int)$key >= (int)$off_1) AND ((int)$key < (int)$off_2) ){
//
//				$product = wc_get_product( $product_id );
//				$product_name = get_the_title($product_id);
//				$short_name = get_the_title($product_id);
//				if(strlen($short_name) > 35) $short_name = substr($short_name, 0, 35).'...';
//				$product_link = get_the_permalink($product_id);
//				$product_price = get_post_meta($product_id, '_regular_price', true);
//				$product_sale = get_post_meta($product_id, '_sale_price', true);
//
//				$thumb = wp_get_attachment_image_src( get_post_thumbnail_id($product_id), 'medium' );
//				if($thumb){
//					$urlphoto = $thumb['0'];
//				}else{
//					$urlphoto = get_template_directory_uri().'/library/images/default_sorry.jpg';
//                }
//                
//                if (isset($product_sale) AND $product_sale != 0) {
//                    $pr_diskon = (((int)$product_price - (int)$product_sale ) / (int)$product_price ) * 100;
//                    $pr_diskon = round($pr_diskon, 2);
//                } else {
//                    $pr_diskon = 0;
//                }
//
//				$def_image = get_template_directory_uri().'/library/images/default_sorry.jpg';
//
//				$alt = 'Ladara Indonesia Gallery';
//			
//				$html .= '<div class="col-xs-6 col-md-3 col_bx_productList">
//								<div class="bx_productList">';
//								
//									if ($u_id != 0) {
//										global $wpdb;
//										$wpdb_query = "SELECT * FROM ldr_wishlist WHERE user_id = '$u_id' AND product_id = '$product_id' ";
//										$res_query = $wpdb->get_results($wpdb_query, OBJECT);
//										$count_res = count($res_query);
//										if($count_res == 0){
//											$love_1 = 'act';
//											$love_2 = '';
//											
//											$html .= '<div class="mg_heart addWishlist love_'.$product_id.'" title="Tambah ke wishlist" data-id="'.$product_id.'">
//												<span class="glyphicon glyphicon-heart"></span>
//											</div>';
//										}else{
//											$love_2 = 'act';	
//											
//											$html .= '<div id="btn_" class="mg_heart removewishlist love_'.$product_id.' act" title="Hapus dari wishlist" data-id="'.$product_id.'" >
//												<span class="glyphicon glyphicon-heart"></span>
//											</div>';
//										}
//									}
//									
//									$html .= '<a href="'.$product_link.'" title="Lihat '.$product_name.'">
//												<div class="mg_productList">
//													<img src="'.$urlphoto.'" alt="'.$alt.'">
//												</div>
//												<div class="bx_in_productList">
//													<h4 class="ht_productList">'.$short_name.'</h4>';
//									
//												if(isset($product_sale) AND $product_sale != ''){ 
//                                                    $html .= '<div class="tx_price">Rp '.number_format($product_sale).'</div>';
//
//													if (intval($product_sale) !== intval($product_price)) {
//                                                        $html .= '<div class="tx_price_sale" style="text-decoration: none;">
//                                                                    <span style="text-decoration: line-through;">Rp '.number_format($product_price).'</span>
//                                                                    <span class="discount">'.$pr_diskon.'%</span>
//                                                                 </div>';
//                                                    }
//											 	}else{
//											 		$html .= '<div class="tx_price">Rp '.number_format($product_price).'</div>';
//												
//												}
//
//											$html .= '<div class="bx_rate_product">';
//									$html .= '</div>
//										</div>
//									</a>	
//								</div>
//							</div>';
//
//			}
//
//		}
	}else{
		$valid = 0;
        $html .= '<div class="row row_masterInformation">
			            <div class="col-md-12 col_emptyInformation">
			                <div class="mg_emptyInformation">
			                    <img src="'.get_template_directory_uri().'/library/images/no_product.svg">
			                </div>
			                <h2 class="tx_emptyInformation">Tidak ada produk yang Anda cari.</h2>
			            </div>
			        </div>';

	}

	$return = array(
					'html' => $html,
					'count_allproduct' => $count_allproduct,
					'valid' => $valid
					);

	
	// $valid = 'masuk pak eko';
	echo json_encode($return);
	die();
}   
add_action('wp_ajax_ajax_getProductPromo','ajax_getProductPromo');
add_action('wp_ajax_nopriv_ajax_getProductPromo', 'ajax_getProductPromo');

function ajax_getSearchAuto(){

	$keyword = $_REQUEST['search'];
	$keyword = str_replace(' ', '%', $keyword);
	$return = array();

	global $wpdb;
	// $wpdb_query = "SELECT id, post_date, post_title
	// 				FROM ldr_posts 
	// 				WHERE post_type = 'product' 
	// 				AND ( post_name LIKE '%$keyword%' OR post_content LIKE '%keyword%' )
	// 				ORDER BY id DESC";
	$wpdb_query = "SELECT * FROM ldr_terms
					WHERE slug LIKE '%$keyword%'
					ORDER BY name ASC LIMIT 10";
	$res_query = $wpdb->get_results($wpdb_query, OBJECT);
	$count_res = count($res_query);

	if($count_res > 0){
		foreach ($res_query as $key => $value){
			$tag_id = $value->term_id;
			$tag_name = ucfirst(strtolower($value->name));
			$tag_slug = str_replace(' ', '+', $tag_name);
			// $tag_slug = $value->slug;
			$return['results'][] = array('id'=>$tag_slug,'text'=>$tag_name);
		}
	}
	
	// $valid = 'masuk pak eko';
	echo json_encode($return);
	die();
}   
add_action('wp_ajax_ajax_getSearchAuto','ajax_getSearchAuto');
add_action('wp_ajax_nopriv_ajax_getSearchAuto', 'ajax_getSearchAuto');


/**
 * @deprecated
 * pindah ke ajax_getProductList
 */
function ajax_getProductFilter(){
	$current_user = wp_get_current_user();
	$u_id = $current_user->ID;

	$keyword = $_REQUEST['keyword'];
	$offset = $_REQUEST['offset'];
	$limit_item = $_REQUEST['limit'];
	$category = $_REQUEST['category'];
	$all_brand = $_REQUEST['all_data'];
	$html = '';
	$valid = 0;
	$count_allproduct = 0;


	$minrange = $_REQUEST['minrange'];
	$maxrange = $_REQUEST['maxrange'];
	$filter_sortby = $_REQUEST['filter_sortby'];

	if(isset($filter_sortby) AND $filter_sortby == 'asc'){
		$sort_query = "ORDER BY a.post_title ASC";
	}else if(isset($filter_sortby) AND $filter_sortby == 'desc'){
		$sort_query = "ORDER BY a.post_title DESC";
	}else if(isset($filter_sortby) AND $filter_sortby == 'termurah'){
		$sort_query = "ORDER BY CAST(price AS UNSIGNED) ASC";
	}else{
		$sort_query = "ORDER BY a.id DESC";
	}

	if($keyword != ''){
		$keyword = str_replace(' ', '%', $keyword);
		// $like_query = "AND ( a.post_name LIKE '%".$keyword."%' OR a.post_content LIKE '%".$keyword."%' )";
		$like_query = "AND (a.post_name LIKE '%".$keyword."%' OR ldr_terms.name LIKE '$keyword%')";
	}else{
		$like_query = "";
	}

	if($minrange != '' AND $maxrange != ''){
		// $rangeprice_query = "AND (b.meta_key = '_regular_price' AND CAST(b.meta_value AS UNSIGNED) BETWEEN '$minrange' AND '$maxrange' )";
		$rangeprice_query = "AND ( b.meta_key = '_regular_price' AND CAST(b.meta_value AS UNSIGNED) BETWEEN '$minrange' AND '$maxrange' )
							AND  ( c.meta_key = '_sale_price' AND CAST(c.meta_value AS UNSIGNED) BETWEEN '$minrange' AND '$maxrange' ) ";
	}else{
		// $rangeprice_query = "AND b.meta_key = '_stock_status' AND b.meta_value = 'instock'";
		$rangeprice_query = "";
	}

	$all_brand = rtrim($all_brand,',');
	$all_brand = explode(',', $all_brand);
	$all_brand = join("','",$all_brand);
	$whereBrand = "";

	if(isset($category) AND $category != ''){ // for category product

		$cat_key = str_replace('-', '%', $category);
		$all_category = str_replace(' ', '%', $cat_key);

		global $wpdb;
		if ($all_brand != ''){
		    $whereBrand = "AND d.meta_key = 'product_brand' AND d.meta_value IN('$all_brand')";
        }
		$wpdb_query = "SELECT DISTINCT a.id AS id,
	                  a.post_title AS post_title,
	                  IFNULL(NULLIF(c.meta_value, '' ), b.meta_value) AS price
	                FROM ldr_posts a
	                LEFT JOIN ldr_postmeta b
	                  ON b.post_id = a.ID
	                  AND b.meta_key = '_regular_price'
					LEFT JOIN ldr_postmeta c
					ON c.post_id = a.id
					AND c.meta_key = '_sale_price'
	                LEFT JOIN ldr_postmeta d
						ON d.post_id = a.ID
						AND d.meta_key = 'product_brand'
	                LEFT JOIN ldr_term_relationships
	                  ON ldr_term_relationships.object_id = a.ID
	                LEFT JOIN ldr_term_taxonomy
	                  ON ldr_term_relationships.term_taxonomy_id = ldr_term_taxonomy.term_taxonomy_id
	                  AND ldr_term_taxonomy.taxonomy = 'product_cat'
	                LEFT JOIN ldr_terms
	                  ON ldr_term_taxonomy.term_id = ldr_terms.term_id
	                WHERE a.post_type = 'product'
	                AND ldr_terms.name LIKE '%$all_category%'
					$rangeprice_query
					$whereBrand
					AND a.post_status = 'publish'
					$sort_query
					LIMIT $limit_item OFFSET $offset";

		// echo "<pre>";
		// print_r($wpdb_query);
		// echo "</pre>";

		$res_query = $wpdb->get_results($wpdb_query, OBJECT);
		$count_res = count($res_query);

		if($count_res > 0){
			foreach ($res_query as $key => $value){
				$valid = 1;
				$product_id = $value->id;
				$product = wc_get_product( $product_id );

				$product_name = $value->post_title;
				$short_name = $value->post_title;
				if(strlen($short_name) > 35) $short_name = substr($short_name, 0, 35).'...';
				$product_link = get_the_permalink($product_id);
				$product_price = $product->get_regular_price();
				$product_sale = $product->get_sale_price();

				$thumb = wp_get_attachment_image_src( get_post_thumbnail_id($product_id), 'medium' );
				if($thumb){
					$urlphoto = $thumb['0'];
				}else{
					$urlphoto = get_template_directory_uri().'/library/images/default_sorry.jpg';
				}
                $def_image = get_template_directory_uri().'/library/images/default_sorry.jpg';
                
                if (isset($product_sale) AND $product_sale != 0) {
                    $pr_diskon = (((int)$product_price - (int)$product_sale ) / (int)$product_price ) * 100;
                    $pr_diskon = round($pr_diskon, 2);
                } else {
                    $pr_diskon = 0;
                }

				$alt = 'Ladara Indonesia Gallery';
				
				$html .= '<div class="col-xs-6 col-md-4 col_bx_productList">
								<div class="bx_productList">';
								
									if ($u_id != 0) {
										global $wpdb;
										$wpdb_query = "SELECT * FROM ldr_wishlist WHERE user_id = '$u_id' AND product_id = '$product_id' ";
										$res_query = $wpdb->get_results($wpdb_query, OBJECT);
										$count_res = count($res_query);
										if($count_res == 0){
											$love_1 = 'act';
											$love_2 = '';
											
											$html .= '<div class="mg_heart addWishlist love_'.$product_id.'" title="Tambah ke wishlist" data-id="'.$product_id.'">
												<span class="glyphicon glyphicon-heart"></span>
											</div>';
										}else{
											$love_2 = 'act';	
											
											$html .= '<div id="btn_" class="mg_heart removewishlist love_'.$product_id.' act" title="Hapus dari wishlist" data-id="'.$product_id.'" >
												<span class="glyphicon glyphicon-heart"></span>
											</div>';
										}
									}
									
									$html .= '<a href="'.$product_link.'" title="Lihat '.$product_name.'">
												<div class="mg_productList">
													<img src="'.$urlphoto.'" alt="'.$alt.'">
												</div>
												<div class="bx_in_productList">
													<h4 class="ht_productList">'.$short_name.'</h4>';
									
												if(isset($product_sale) AND $product_sale != ''){ 
                                                    $html .= '<div class="tx_price">Rp '.number_format($product_sale).'</div>';

													if (intval($product_sale) !== intval($product_price)) {
                                                        $html .= '<div class="tx_price_sale" style="text-decoration: none;">
                                                                    <span style="text-decoration: line-through;">Rp '.number_format($product_price).'</span>
                                                                    <span class="discount">'.$pr_diskon.'%</span>
                                                                 </div>';
                                                    }
											 	}else{
											 		$html .= '<div class="tx_price">Rp '.number_format($product_price).'</div>';
												
												}

											$html .= '<div class="bx_rate_product">';
	 										
												// $pro_rate = get_field('product_rate',$product_id);
												// $total_product_rate = get_field('total_product_rate',$product_id);
												// $count_rate = $total_product_rate;
												// $full_star = $pro_rate;
												// $empty_star = 5;
												// if($count_rate != 0){
												// 	for ($a=1; $a <= $empty_star; $a++) { 
												// 		if($a <= $full_star){
												// 			$html .= '<img src="'.get_template_directory_uri().'/library/images/star1.svg">';
												// 		}else{
												// 			$html .= '<img src="'.get_template_directory_uri().'/library/images/star0.svg">';
												// 		}
												// 	} 
													
												// 	$html .= '<span class="sp_rate_info">('.$count_rate.')</span>';
												
												// }
												
									$html .= '</div>
										</div>
									</a>	
								</div>
							</div>';
			}
		}else{
			$valid = 0;
	        $html .= '<div class="row row_masterInformation">
				            <div class="col-md-12 col_emptyInformation">
				                <div class="mg_emptyInformation">
				                    <img src="'.get_template_directory_uri().'/library/images/no_product.svg">
				                </div>
				                <h2 class="tx_emptyInformation">Tidak ada produk yang Anda cari.</h2>
				            </div>
				        </div>';

		}


		global $wpdb;
        if ($all_brand != ''){
            $whereBrand = "AND d.meta_key = 'product_brand' AND d.meta_value IN('$all_brand')";
        }
		$wpdb_query2 = "SELECT DISTINCT a.id AS id,
	                  a.post_title AS post_title,
	                  IFNULL(NULLIF(c.meta_value, '' ), b.meta_value) AS price
	                FROM ldr_posts a
	                LEFT JOIN ldr_postmeta b
	                  ON b.post_id = a.ID
	                  AND b.meta_key = '_regular_price'
					LEFT JOIN ldr_postmeta c
					ON c.post_id = a.id
					AND c.meta_key = '_sale_price'
	                LEFT JOIN ldr_postmeta d
						ON d.post_id = a.ID
						AND d.meta_key = 'product_brand'
	                LEFT JOIN ldr_term_relationships
	                  ON ldr_term_relationships.object_id = a.ID
	                LEFT JOIN ldr_term_taxonomy
	                  ON ldr_term_relationships.term_taxonomy_id = ldr_term_taxonomy.term_taxonomy_id
	                  AND ldr_term_taxonomy.taxonomy = 'product_cat'
	                LEFT JOIN ldr_terms
	                  ON ldr_term_taxonomy.term_id = ldr_terms.term_id
	                WHERE a.post_type = 'product'
	                AND ldr_terms.name LIKE '%$all_category%'
					$rangeprice_query
					$whereBrand
					AND a.post_status = 'publish'
					$sort_query";

		$res_query2 = $wpdb->get_results($wpdb_query2, OBJECT);
		$count_allproduct = count($res_query2);

	}else{ // for normal product

		global $wpdb;
        if ($all_brand != ''){
            $whereBrand = "AND d.meta_key = 'product_brand' AND d.meta_value IN('$all_brand')";
        }
		$wpdb_query = "SELECT DISTINCT a.id, a.post_date, a.post_title
						FROM ldr_posts a
						LEFT JOIN ldr_postmeta b ON a.id = b.post_id
						AND b.meta_key = '_regular_price'
						LEFT JOIN ldr_postmeta c
						ON c.post_id = a.id
						AND c.meta_key = '_sale_price'
						LEFT JOIN ldr_postmeta d
						ON d.post_id = a.id
						AND d.meta_key = 'product_brand'
                        LEFT JOIN ldr_term_relationships
                          ON ldr_term_relationships.object_id = a.ID
                        LEFT JOIN ldr_term_taxonomy
                          ON ldr_term_relationships.term_taxonomy_id = ldr_term_taxonomy.term_taxonomy_id
                          AND ldr_term_taxonomy.taxonomy = 'product_tag'
                        LEFT JOIN ldr_terms
                          ON ldr_term_taxonomy.term_id = ldr_terms.term_id
						WHERE 1=1  
						AND a.post_type = 'product' 
						$like_query
						$rangeprice_query
						$whereBrand
						AND a.post_status = 'publish'
						$sort_query
						LIMIT $limit_item OFFSET $offset";
		$res_query = $wpdb->get_results($wpdb_query, OBJECT);
		$count_res = count($res_query);

		// echo "<pre>";
		// print_r($wpdb_query);
		// echo "</pre>";

		if($count_res > 0){
			foreach ($res_query as $key => $value){
				$valid = 1;
				$product_id = $value->id;
				$product = wc_get_product( $product_id );

				$product_name = $value->post_title;
				$short_name = $value->post_title;
				if(strlen($short_name) > 35) $short_name = substr($short_name, 0, 35).'...';
				$product_link = get_the_permalink($product_id);
				$product_price = $product->get_regular_price();
				$product_sale = $product->get_sale_price();

				$thumb = wp_get_attachment_image_src( get_post_thumbnail_id($product_id), 'medium' );
				if($thumb){
					$urlphoto = $thumb['0'];
				}else{
					$urlphoto = get_template_directory_uri().'/library/images/default_sorry.jpg';
                }
                
                if (isset($product_sale) AND $product_sale != 0) {
                    $pr_diskon = (((int)$product_price - (int)$product_sale ) / (int)$product_price ) * 100;
                    $pr_diskon = round($pr_diskon, 2);
                } else {
                    $pr_diskon = 0;
                }

				$def_image = get_template_directory_uri().'/library/images/default_sorry.jpg';

				$alt = 'Ladara Indonesia Gallery';
		
				$html .= '<div class="col-xs-6 col-md-4 col_bx_productList">
								<div class="bx_productList">';
								
									if ($u_id != 0) {
										global $wpdb;
										$wpdb_query = "SELECT * FROM ldr_wishlist WHERE user_id = '$u_id' AND product_id = '$product_id' ";
										$res_query = $wpdb->get_results($wpdb_query, OBJECT);
										$count_res = count($res_query);
										if($count_res == 0){
											$love_1 = 'act';
											$love_2 = '';
											
											$html .= '<div class="mg_heart addWishlist love_'.$product_id.'" title="Tambah ke wishlist" data-id="'.$product_id.'">
												<span class="glyphicon glyphicon-heart"></span>
											</div>';
										}else{
											$love_2 = 'act';	
											
											$html .= '<div id="btn_" class="mg_heart removewishlist love_'.$product_id.' act" title="Hapus dari wishlist" data-id="'.$product_id.'" >
												<span class="glyphicon glyphicon-heart"></span>
											</div>';
										}
									}
									
									$html .= '<a href="'.$product_link.'" title="Lihat '.$product_name.'">
												<div class="mg_productList">
													<img src="'.$urlphoto.'" alt="'.$alt.'">
												</div>
												<div class="bx_in_productList">
													<h4 class="ht_productList">'.$short_name.'</h4>';
									
												if(isset($product_sale) AND $product_sale != ''){ 
													$html .= '<div class="tx_price">Rp '.number_format($product_sale).'</div>';
                                                    
                                                    if (intval($product_sale) !== intval($product_price)) {
                                                        $html .= '<div class="tx_price_sale" style="text-decoration: none;">
                                                                    <span style="text-decoration: line-through;">Rp '.number_format($product_price).'</span>
                                                                    <span class="discount">'.$pr_diskon.'%</span>
                                                                 </div>';
                                                    }
											 	}else{
											 		$html .= '<div class="tx_price">Rp '.number_format($product_price).'</div>';
												
												}

											$html .= '<div class="bx_rate_product">';
	 										
												$pro_rate = get_field('product_rate',$product_id);
												$total_product_rate = get_field('total_product_rate',$product_id);
												$count_rate = $total_product_rate;
												$full_star = $pro_rate;
												$empty_star = 5;
												if($count_rate != 0){
													for ($a=1; $a <= $empty_star; $a++) { 
														if($a <= $full_star){
															$html .= '<img src="'.get_template_directory_uri().'/library/images/star1.svg">';
														}else{
															$html .= '<img src="'.get_template_directory_uri().'/library/images/star0.svg">';
														}
													} 
													
													$html .= '<span class="sp_rate_info">('.$count_rate.')</span>';
												
												}
												
									$html .= '</div>
										</div>
									</a>	
								</div>
							</div>';

			}
		}else{
			$valid = 0;
	        $html .= '<div class="row row_masterInformation">
				            <div class="col-md-12 col_emptyInformation">
				                <div class="mg_emptyInformation">
				                    <img src="'.get_template_directory_uri().'/library/images/no_product.svg">
				                </div>
				                <h2 class="tx_emptyInformation">Tidak ada produk yang Anda cari.</h2>
				            </div>
				        </div>';

		}


		global $wpdb;
        if ($all_brand != ''){
            $whereBrand = "AND c.meta_key = 'product_brand' AND c.meta_value IN('$all_brand') ";
        }
		$wpdb_query2 = "SELECT DISTINCT a.id, a.post_date, a.post_title
						FROM ldr_posts a
						LEFT JOIN ldr_postmeta b
						ON a.id = b.post_id
						AND b.meta_key = '_regular_price'
						LEFT JOIN ldr_postmeta c
						ON c.post_id = a.id
						AND c.meta_key = 'product_brand'
                        LEFT JOIN ldr_term_relationships
                          ON ldr_term_relationships.object_id = a.ID
                        LEFT JOIN ldr_term_taxonomy
                          ON ldr_term_relationships.term_taxonomy_id = ldr_term_taxonomy.term_taxonomy_id
                          AND ldr_term_taxonomy.taxonomy = 'product_tag'
                        LEFT JOIN ldr_terms
                          ON ldr_term_taxonomy.term_id = ldr_terms.term_id
						WHERE 1=1  
						AND a.post_type = 'product' 
						$like_query
						$rangeprice_query
						$whereBrand
						AND a.post_status = 'publish'
						$sort_query";
		$res_query2 = $wpdb->get_results($wpdb_query2, OBJECT);
		$count_allproduct = count($res_query2);

	}

	$return = array(
					'html' => $html,
					'count_allproduct' => $count_allproduct,
					'valid' => $valid
					);

	
	// $valid = 'masuk pak eko';
	echo json_encode($return);
	die();
}   
add_action('wp_ajax_ajax_getProductFilter','ajax_getProductFilter');
add_action('wp_ajax_nopriv_ajax_getProductFilter', 'ajax_getProductFilter');

function ajax_confirmOrder(){

	$order_id = $_REQUEST['order_id'];
	$valid = 0;

	if(isset($order_id) AND $order_id != ''){
		$order = new WC_Order( $order_id );
	    $order->update_status('wc-processing');	
	    $valid = 1;

        // ==== insert to order log
        date_default_timezone_set("Asia/Jakarta");
        $nowdate = date('Y-m-d H:i:s');
        $order_status = 'processing';
        $log_text = 'Pembayaran diterima Order ID #'.$order_id.', Status menjadi processing';     
        
        global $wpdb;
        $query_log = "INSERT INTO ldr_order_log(order_id,log,created_date,order_status) VALUES('$order_id','$log_text','$nowdate','$order_status')";
        $result_log = $wpdb->query($query_log);

        global $wpdb;
		$wpdb_query_update = " UPDATE ldr_orders
		              SET order_status = 'wc-processing'
		              WHERE order_id = '$order_id'
		            ";
		$res_query_update = $wpdb->query($wpdb_query_update);


		
        // =========== insert notification ==============
        $user_id = $order->get_user_id(); // or $order->get_customer_id();
		$orderId_notif = $order_id;
		$type_notif = 'pesanan';
		$title_notif = 'Pembayaran '.$orderId_notif.' di konfirmasi.';
		$desc_notif = 'Pembayaran Anda sudah di konfirmasi, kami akan segera menyiapkan pesanan Anda.';
		$data = [
		      'userId' => $user_id, //wajib
		      'title' => $title_notif, //wajib
		      'descriptions' => $desc_notif, //wajib
		      'type' => $type_notif, //pesanan, info, pembayaran, emas // wajib
		      'orderId' => $orderId_notif, // optional
		      'data' => [] //array optional bisa diisi dengan data lainnya
		];
		$addNotif = Notifications::addNotification($data);
		// =========== insert notification ==============

	}
	
	// $valid = 'masuk pak eko';
	echo json_encode($valid);
	die();
}   
add_action('wp_ajax_ajax_confirmOrder','ajax_confirmOrder');
add_action('wp_ajax_nopriv_ajax_confirmOrder', 'ajax_confirmOrder');

function ajax_confirmCancelled(){

	date_default_timezone_set("Asia/Jakarta");
    $nowdate = date('Y-m-d H:i:s');
    
	$order_id = $_REQUEST['order_id'];
	$valid = 0;

	if(isset($order_id) AND $order_id != ''){

		global $wpdb;
		$wpdb_query = "SELECT * FROM ldr_orders_item WHERE order_id = '$order_id'";
		$res_query = $wpdb->get_results($wpdb_query, OBJECT);
		$count_res = count($res_query);

		if($count_res > 0){ // if not exist with same user_id
			foreach ($res_query as $key => $value) {
				$product_id = $value->product_id;
				$stock_id = $value->stock_id;
				$product_qty = $value->qty;

				global $wpdb;
				$wpdb_query_update = " UPDATE ldr_orders_item
				              SET qty = '0'
				              WHERE order_id = '$order_id' AND product_id = '$product_id'
				            ";
				$res_query_update = $wpdb->query($wpdb_query_update);

			    global $wpdb;
			    $wpdb_query = "SELECT * FROM ldr_stock WHERE id = '$stock_id' ";
			    $res_query = $wpdb->get_results($wpdb_query, OBJECT);
			    $count_res = count($res_query);
			    if($count_res > 0){
			        foreach ($res_query as $key => $value){
						$id_stock = $value->id;
						// $product_id = $value->product_id;
						$product_quantity = $value->product_quantity;

						$total_qty = (int)$product_quantity+(int)$product_qty;

			            if((int)$product_quantity >= 0){

			            	// =============== UPDATE STOCK ==================
				                global $wpdb;
								$wpdb_query_update = " UPDATE ldr_stock
								              SET product_quantity=$total_qty
								              WHERE id = '$id_stock'
								            ";
								$res_query_update = $wpdb->query($wpdb_query_update);
							// =============== UPDATE STOCK ==================

							// ============== Insert STOCK LOG ====================
					 			$status = 'cancelled';
					 			$from_stock = $product_quantity;
					 			$to_stock = $total_qty;
					        	$query = "INSERT INTO ldr_stock_log(created_date,product_id,stock_id,from_stock,to_stock,status,admin_name) 
						                VALUES('$nowdate','$product_id','$id_stock','$from_stock','$to_stock','$status','$order_id')";
						    	$result = $wpdb->query($query);
							// ============== Insert STOCK LOG ====================	

						    $valid = 1;

						}
					} 
			    }
			}
		}
              
        // ===== update status to cancell
        $order = new WC_Order( $order_id );
        $order->update_status( 'cancelled' );

		global $wpdb;
		$wpdb_query = "SELECT * FROM ldr_orders WHERE order_id = '$order_id'";
		$res_query = $wpdb->get_results($wpdb_query, OBJECT);
		$count_res = count($res_query);
		if($count_res > 0){ // if not exist with same user_id

			$user_id = '';
			foreach ($res_query as $key => $value_or) {
				$user_id = $value_or->user_id;
			}

			$wpdb_query_update = " UPDATE ldr_orders
			              SET order_status = 'wc-cancelled'
			              WHERE order_id = '$order_id'
			            ";
			$res_query_update = $wpdb->query($wpdb_query_update);

	        // ==== insert to order log
	        date_default_timezone_set("Asia/Jakarta");
	        $nowdate = date('Y-m-d H:i:s');
	        $order_status = 'cancelled';
	        $log_text = 'Order ID #'.$order_id.' sudah di batalkan, karena tidak ada transaksi dalam 2 hari.';   
	        
	        $query_log = "INSERT INTO ldr_order_log(order_id,log,created_date,order_status) VALUES('$order_id','$log_text','$nowdate','$order_status')";
	        $result_log = $wpdb->query($query_log);
          /* ============ create log ============*/ 

		}

        // =========== insert notification ==============
        $user_id = $order->get_user_id(); // or $order->get_customer_id();
        $orderId_notif = $order_id;
        $type_notif = 'pesanan';
        $title_notif = 'Pesanan '.$orderId_notif.' telah dibatalkan.';
        $desc_notif = 'Pesanan Anda telah dibatalkan.';
        $data = [
              'userId' => $user_id, //wajib
              'title' => $title_notif, //wajib
              'descriptions' => $desc_notif, //wajib
              'type' => $type_notif, //pesanan, info, pembayaran, emas // wajib
              'orderId' => $orderId_notif, // optional
              'data' => [] //array optional bisa diisi dengan data lainnya
        ];
        $addNotif = Notifications::addNotification($data);
        // =========== insert notification ==============

        // //kirim email batal trasaksi
        // $queryOrder = "SELECT * FROM ldr_orders o
        //                 LEFT JOIN ldr_payment_log pl ON pl.order_id = o.order_id
        //                 WHERE order_id = '$order_id'";
        // $resultOrder = $wpdb->get_row($queryOrder, OBJECT);
        // $userBuilder = new UsersBuilder($order->get_billing_first_name(), $order->get_billing_email());

        // $paymentBuilder = (new PaymentBuilder())
        //     ->setBank($resultOrder->bank_code);

        // $items = [];
        // foreach ($order->get_items() as $item){
        //     $data =$item->get_data();
        //     $items[]=[
        //         'name' => $item->get_name(),
        //         'qty'=> $item->get_quantity(),
        //         'price' => $data['total']
        //     ];
        // }

        // $explodeCourier = explode('(', $resultOrder->courier_name);
        // $courier = trim($explodeCourier[0]);
        // $service = trim(str_replace(')', '', $explodeCourier[1]));
        // $shippingBuilder = new ShippingBuilder($courier ?? '', $service,$resultOrder->shipping_price);

        // $orderBuilder = (new OrderBuilder())
        //     ->setDateCreated($resultOrder->created_date)
        //     ->setTotalPrice($resultOrder->paid_amount)
        //     ->setItems($items)
        //     ->setStatus('Pembayaran Dibatalkan')
        //     ->setInvoiceNo(OrderModel::invoice($order->get_date_created(),$order->get_id()))
        //     ->setPaymentDate($resultOrder->paid_at);

        // $email = (new EmailHelper($userBuilder))
        //     ->batalTransaksi($orderBuilder, $paymentBuilder, $shippingBuilder);
        // //END kirim email
	}
	
	// $valid = 'masuk pak eko';
	echo json_encode($valid);
	die();
}   
add_action('wp_ajax_ajax_confirmCancelled','ajax_confirmCancelled');
add_action('wp_ajax_nopriv_ajax_confirmCancelled', 'ajax_confirmCancelled');

function ajax_processingOrder(){

	$order_id = $_REQUEST['order_id'];
	$valid = 0;

	if(isset($order_id) AND $order_id != ''){
		$order = new WC_Order( $order_id );
	    $order->update_status('wc-preparing');	
	    $valid = 1;

        // =========== insert notification ==============
        $user_id = $order->get_user_id(); // or $order->get_customer_id();
        $orderId_notif = $order_id;
        $type_notif = 'pesanan';
        $title_notif = 'Pesanan '.$orderId_notif.' sedang disiapkan.';
        $desc_notif = 'Pesanan sedang di siapkan untuk dikirimkan ke alamat tujuan Anda.';
        $data = [
              'userId' => $user_id, //wajib
              'title' => $title_notif, //wajib
              'descriptions' => $desc_notif, //wajib
              'type' => $type_notif, //pesanan, info, pembayaran, emas // wajib
              'orderId' => $orderId_notif, // optional
              'data' => [] //array optional bisa diisi dengan data lainnya
        ];
        $addNotif = Notifications::addNotification($data);
        // =========== insert notification ==============

        // ==== insert to order log
        date_default_timezone_set("Asia/Jakarta");
        $nowdate = date('Y-m-d H:i:s');
        $order_status = 'preparing';
        $log_text = 'Pembayaran diterima Order ID #'.$order_id.', Status menjadi '.$order_status;     
        
        global $wpdb;
        $query_log = "INSERT INTO ldr_order_log(order_id,log,created_date,order_status) VALUES('$order_id','$log_text','$nowdate','$order_status')";
        $result_log = $wpdb->query($query_log);

        global $wpdb;
		$wpdb_query_update = " UPDATE ldr_orders
		              SET order_status = 'wc-preparing'
		              WHERE order_id = '$order_id'
		            ";
		$res_query_update = $wpdb->query($wpdb_query_update);

	}
	
	// $valid = 'masuk pak eko';
	echo json_encode($valid);
	die();
}   
add_action('wp_ajax_ajax_processingOrder','ajax_processingOrder');
add_action('wp_ajax_nopriv_ajax_processingOrder', 'ajax_processingOrder');


function ajax_deliveryOrder(){

	$order_id = $_REQUEST['order_id'];
	$valid = 0;

	if(isset($order_id) AND $order_id != ''){
		$order = new WC_Order( $order_id );
	    $order->update_status('wc-ondelivery');	
	    $valid = 1;

        // =========== insert notification ==============
        $user_id = $order->get_user_id(); // or $order->get_customer_id();
        $orderId_notif = $order_id;
        $type_notif = 'pesanan';
        $title_notif = 'Pesanan '.$orderId_notif.' telah dikirimkan.';
        $desc_notif = 'Pesanan telah di kirimkan ke alamat tujuan Anda. Silahkan cek dafftar belanja untuk status pengiriman.';
        $data = [
              'userId' => $user_id, //wajib
              'title' => $title_notif, //wajib
              'descriptions' => $desc_notif, //wajib
              'type' => $type_notif, //pesanan, info, pembayaran, emas // wajib
              'orderId' => $orderId_notif, // optional
              'data' => [] //array optional bisa diisi dengan data lainnya
        ];
        $addNotif = Notifications::addNotification($data);
        // =========== insert notification ==============

        // ==== insert to order log
        date_default_timezone_set("Asia/Jakarta");
        $nowdate = date('Y-m-d H:i:s');
        $order_status = 'ondelivery';
        $log_text = 'Order ID #'.$order_id.' dikirimkan & resi(awb) sedang di proses, Status menjadi '.$order_status;     
        
        global $wpdb;
        $query_log = "INSERT INTO ldr_order_log(order_id,log,created_date,order_status) VALUES('$order_id','$log_text','$nowdate','$order_status')";
        $result_log = $wpdb->query($query_log);

        global $wpdb;
		$wpdb_query_update = " UPDATE ldr_orders
		              SET order_status = 'wc-ondelivery'
		              WHERE order_id = '$order_id'
		            ";
		$res_query_update = $wpdb->query($wpdb_query_update);

	}
	
	// $valid = 'masuk pak eko';
	echo json_encode($valid);
	die();
}   
add_action('wp_ajax_ajax_deliveryOrder','ajax_deliveryOrder');
add_action('wp_ajax_nopriv_ajax_deliveryOrder', 'ajax_deliveryOrder');

function ajax_updateResi(){

	$order_id = $_REQUEST['order_id'];
	$nomor_resi = $_REQUEST['nomor_resi'];
	$valid = 0;
	$user_id = '';

	if(isset($order_id) AND $order_id != '' AND $nomor_resi != ''){
	    
    	global $wpdb;
		$wpdb_query = "SELECT * FROM ldr_orders WHERE order_id = '$order_id'";
		$res_query = $wpdb->get_results($wpdb_query, OBJECT);
		$count_res = count($res_query);
		if($count_res > 0){ // if not exist with same user_id

			$courier_resi = '';
			foreach ($res_query as $key => $value_or) {
				$user_id = $value_or->user_id;
				$courier_resi = $value_or->courier_resi;
			}

			if($courier_resi != $nomor_resi){

		        global $wpdb;
				$wpdb_query_update = " UPDATE ldr_orders
				              SET courier_resi = '$nomor_resi'
				              WHERE order_id = '$order_id'
				            ";
				$res_query_update = $wpdb->query($wpdb_query_update);

				$valid = 1;
				
			}

	        // ==== insert to order log
	        date_default_timezone_set("Asia/Jakarta");
	        $nowdate = date('Y-m-d H:i:s');
	        $order_status = 'ondelivery';
	        $log_text = 'Order ID #'.$order_id.', Telah dikirimkan dengan Nomor Resi : '.$nomor_resi;   
	        global $wpdb;
	        $query_log = "INSERT INTO ldr_order_log(order_id,log,created_date,order_status) VALUES('$order_id','$log_text','$nowdate','$order_status')";
	        $result_log = $wpdb->query($query_log);

		}

		if($valid == 1){

	        // =========== insert notification ==============
	        // $user_id = $order->get_user_id(); // or $order->get_customer_id();
	        $orderId_notif = $order_id;
	        $type_notif = 'pesanan';
	        $title_notif = 'Nomor Resi Pesanan '.$orderId_notif.' telah ditambahkan.';
	        $desc_notif = 'Nomor Resi untuk Pesanan telah di tambahkan. Silahkan cek dafftar belanja untuk status pengiriman.';
	        $data = [
	              'userId' => $user_id, //wajib
	              'title' => $title_notif, //wajib
	              'descriptions' => $desc_notif, //wajib
	              'type' => $type_notif, //pesanan, info, pembayaran, emas // wajib
	              'orderId' => $orderId_notif, // optional
	              'data' => [] //array optional bisa diisi dengan data lainnya
	        ];
	        $addNotif = Notifications::addNotification($data);
	        // =========== insert notification ==============

			// =============================== update resi jne ===========================
		    $username = getenv('JNE_USERNAME');
		    $apikey = getenv('JNE_API_KEY');
		    $url_jne = getenv('JNE_SET_RESI_ENDPOINT');

		    $ORDER_ID = $order_id;
		    $AWB_NUMBER = $nomor_resi;
		    
		    $head_key = array(
		                "content-type: application/x-www-form-urlencoded"
		              );

		    $postfield = 'username='.$username.'&api_key='.$apikey.'&ORDER_ID='.$ORDER_ID.'&AWB_NUMBER='.$AWB_NUMBER;

		    $curl = curl_init();
		    curl_setopt_array($curl, array(
		      CURLOPT_URL => $url_jne,
		      CURLOPT_RETURNTRANSFER => true,
		      CURLOPT_ENCODING => "",
		      CURLOPT_MAXREDIRS => 10,
		      CURLOPT_TIMEOUT => 30,
		      CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
		      CURLOPT_CUSTOMREQUEST => "POST",
		      CURLOPT_POSTFIELDS => $postfield,
		      CURLOPT_HTTPHEADER => $head_key,
		    ));
		    $response = curl_exec($curl);
		    $err = curl_error($curl);
		    curl_close($curl);

		    $response = json_decode($response,true);

		    // =============================== update resi jne ===========================
		}
	}
	
	// $valid = 'masuk pak eko';
	echo json_encode($valid);
	die();
}   
add_action('wp_ajax_ajax_updateResi','ajax_updateResi');
add_action('wp_ajax_nopriv_ajax_updateResi', 'ajax_updateResi');


function ajax_completedOrder(){

	$order_id = $_REQUEST['order_id'];
	$valid = 0;

	if(isset($order_id) AND $order_id != ''){
		$order = new WC_Order( $order_id );
		$user_id = $order->user_id;
	    $order->update_status('wc-completed');	
	    $valid = 1;

        // =========== insert notification ==============
        // $user_id = $order->get_user_id(); // or $order->get_customer_id();
        $orderId_notif = $order_id;
        $type_notif = 'pesanan';
        $title_notif = 'Pesanan '.$orderId_notif.' telah selesai.';
        $desc_notif = 'Pesanan telah di konfirmasi sudah sampai dan selesai. Terima Kasih.';
        $data = [
              'userId' => $user_id, //wajib
              'title' => $title_notif, //wajib
              'descriptions' => $desc_notif, //wajib
              'type' => $type_notif, //pesanan, info, pembayaran, emas // wajib
              'orderId' => $orderId_notif, // optional
              'data' => [] //array optional bisa diisi dengan data lainnya
        ];
        $addNotif = Notifications::addNotification($data);
        // =========== insert notification ==============


        // ==== insert to order log
        date_default_timezone_set("Asia/Jakarta");
        $nowdate = date('Y-m-d H:i:s');
        $order_status = 'completed';
        $log_text = 'Order ID #'.$order_id.', Telah sampai pada tujuan & di konfirmasi oleh Admin.';     
        
        global $wpdb;
        $query_log = "INSERT INTO ldr_order_log(order_id,log,created_date,order_status) VALUES('$order_id','$log_text','$nowdate','$order_status')";
        $result_log = $wpdb->query($query_log);

    	global $wpdb;
		$wpdb_query = "SELECT * FROM ldr_orders WHERE order_id = '$order_id'";
		$res_query = $wpdb->get_results($wpdb_query, OBJECT);
		$count_res = count($res_query);
		if($count_res > 0){ // if not exist with same user_id

	        global $wpdb;
			$wpdb_query_update = " UPDATE ldr_orders
			              SET order_status = 'wc-completed'
			              WHERE order_id = '$order_id'
			            ";
			$res_query_update = $wpdb->query($wpdb_query_update);

		}

	}
	
	// $valid = 'masuk pak eko';
	echo json_encode($valid);
	die();
}   
add_action('wp_ajax_ajax_completedOrder','ajax_completedOrder');
add_action('wp_ajax_nopriv_ajax_completedOrder', 'ajax_completedOrder');


function get_calculateFeeXendit($amount, $shipping, $paymethod) {
    $valid = 0;
    $value_fee_cc = 0;
    $total_fee_cc = 0;
    $value_fee_va = 0;
    $total_fee_va = 0;
    $value_fee_ewallet = 0;
    $total_fee_ewallet = 0;
    $value_fee_retail = 0;
    $total_fee_retail = 0;
    $result = array();

    global $wpdb;
    $wpdb_query = "SELECT * FROM ldr_xendit_fee WHERE category = '$paymethod'";
    $value = $wpdb->get_row($wpdb_query, OBJECT);

    $category = $value->category;
    $fee_price = $value->fee_price;
    $fee_ppn = $value->fee_ppn;
    $fee_cc = $value->fee_cc;

    if (strtolower($category) == 'cc') {
        $v_fee = (float) $amount * (float) $fee_cc;
        $value_fee_cc = (float) $v_fee + (float) $fee_price + ( (float) $v_fee * (float) $fee_ppn ) + ( (float) $fee_price * (float) $fee_ppn ) ;
        $total_fee_cc = (float) $amount + (float) $value_fee_cc + (float) $shipping;
    } else if(strtolower($category) == 'va') {
        $value_fee_va = (float) $fee_price + ((float) $fee_price * (float) $fee_ppn);
        $total_fee_va = (float) $amount + (float) $value_fee_va + (float) $shipping;
    } else if(strtolower($category) == 'ewallet') {
        $v_fee = floatval($amount) * floatval($fee_cc) / 100;
        $value_fee_ewallet = floatval($v_fee) + (floatval($v_fee) * floatval($fee_ppn));
        $total_fee_ewallet = floatval($amount) + floatval($value_fee_ewallet) + floatval($shipping);
    } else if(strtolower($category) == 'retail') {
        $value_fee_retail = floatval($fee_price);
        $total_fee_retail = floatval($amount) + floatval($value_fee_retail) + floatval($shipping);
    }

    if (strtolower($category) == 'cc') {
        $result = [
            'type' => 'cc',
            'amount' => round($amount),
            'fee' => round($value_fee_cc),
            'total' => round($total_fee_cc)
        ];
    } else if (strtolower($category) == 'va') {
        $result = [
            'type' => 'va',
            'amount' => round($amount),
            'fee' => round($value_fee_va),
            'total' => round($total_fee_va)
        ];
    } else if (strtolower($category) == 'ewallet') {
        $result = [
            'type' => 'ewallet',
            'amount' => round($amount),
            'fee' => round($value_fee_ewallet),
            'total' => round($total_fee_ewallet)
        ];
    } else if (strtolower($category) == 'retail') {
        $result = [
            'type' => 'retail',
            'amount' => round($amount),
            'fee' => round($value_fee_retail),
            'total' => round($total_fee_retail)
        ];
    }

    return $result;
}

function get_calculateXendit() {
    if (isset($_REQUEST['identifier'])) {
        $identifier = $_REQUEST['identifier'];
    } else {
        echo json_encode(0);
        die();
    }

    $order = $_SESSION['insertOrder'][$identifier];
    $amount = floatval($order['totalAllItem']);
    $shipmentFee = floatval($order['totalShipmentFee']);
    $insuranceFee = floatval($order['totalInsuranceFee']);
    $paymethod = $_REQUEST['paymethod'];
    $totalFee = 0;
    $totalAmount = 0;
    $result = array();

    global $wpdb;
    $queryFee = $wpdb->get_row("SELECT * FROM ldr_xendit_fee WHERE category = '$paymethod'");
    $feePrice = floatval($queryFee->fee_price);
    $feePPN = floatval($queryFee->fee_ppn);
    $feeCC = floatval($queryFee->fee_cc);

    if ($paymethod === 'cc') {
        $tempFee = $amount * $feeCC;
        $totalFee = $tempFee + $feePrice + ($tempFee * $feePPN ) + ($feePrice * $feePPN);
    } else if ($paymethod === 'va') {
        $totalFee = $feePrice + ($feePrice * $feePPN);
    } else if ($paymethod === 'ewallet') {
        $tempFee = $amount * $feeCC / 100;
        $totalFee = $tempFee + $tempFee * $feePPN;
    } else if ($paymethod === 'retail') {
        $totalFee = $feePrice;
    }

    $totalAmount = $amount + $totalFee + $shipmentFee + $insuranceFee;

    echo json_encode([
        'type' => $paymethod,
        'amount' => round($amount),
        'fee' => round($totalFee),
        'total' => round($totalAmount)
    ]);

    die();
}   
add_action('wp_ajax_get_calculateXendit','get_calculateXendit');
add_action('wp_ajax_nopriv_get_calculateXendit', 'get_calculateXendit');

function createInvoice($paymentMethod, $orderId, $totalPrice, $selectedBank, $virtualAccountId, $noHp)
{
    if ($paymentMethod === 'va' || $paymentMethod === 'cc') {
        $requestInvoice = [
            'external_id' => 'invoice-'.$orderId,
            'amount' => intval($totalPrice),
            'description' => 'Invoice #'.$orderId,
            'payer_email' => 'Ladaraindonesia@gmail.com',
            'invoice_duration' => 7200,
            'payment_methods' => [$selectedBank],
            'should_send_email' => false,
            'success_redirect_url' => home_url().'/thank-you/?order='.$orderId,
            'failure_redirect_url' => home_url().'/failed-payment/?order='.$orderId
        ];
    
        if ($paymentMethod === 'va') {
            $requestInvoice['callback_virtual_account_id'] = $virtualAccountId;
        }

        $createInvoice = Invoice::create($requestInvoice);
    } else if ($paymentMethod === 'ewallet') {
        $requestInvoice = [
            'external_id' => 'invoice-'.time(),
            'amount' => intval($totalPrice),
            'ewallet_type' => $selectedBank
        ];

        if ($selectedBank === "DANA") {
            $requestInvoice['expiration_date'] = date(DATE_ATOM, strtotime("+1 hours"));
            $requestInvoice['callback_url'] = home_url().'/payment-confirmation';
            $requestInvoice['redirect_url'] = home_url().'/thank-you/?order='.$orderId;
        } else if ($selectedBank === "LINKAJA") {
            global $wpdb;

            $getOrderItems = $wpdb->get_results("SELECT * FROM ldr_orders_item WHERE order_id = $orderId");
            $orderDetails = [];

            foreach ($getOrderItems as $orderItem) {
                $product = get_post($orderItem->product_id);

                $orderDetails[] = [
                    'id' => strval($orderItem->id),
                    'name' => $product->post_title,
                    'price' => $orderItem->price,
                    'quantity' => $orderItem->qty
                ];
            }

            $requestInvoice['phone'] = $noHp;
            $requestInvoice['items'] = $orderDetails;
            $requestInvoice['callback_url'] = home_url().'/payment-confirmation';
            $requestInvoice['redirect_url'] = home_url().'/thank-you/?order='.$orderId;
        } else if ($selectedBank === "OVO") {
            $requestInvoice['phone'] = $noHp;
        }

        $createInvoice = EWallets::create($requestInvoice);
        $createInvoice['status'] = 'PENDING';
    } else if ($paymentMethod === 'retail') {
        $user = wp_get_current_user();

        $requestInvoice = [
            'external_id' => 'invoice-'.time(),
            'retail_outlet_name' => $selectedBank,
            'name' => $user->display_name,
            'expected_amount' => intval($totalPrice),
            'expiration_date' => date(DATE_ATOM, strtotime("+1 hours")),
            'is_single_use' => true
        ];

        $createInvoice = Retail::create($requestInvoice);
        $createInvoice['amount'] = $createInvoice['expected_amount'];
    }

    return $createInvoice;
}

function getTotalPayment() {
    if (isset($_REQUEST['identifier'])) {
        $identifier = $_REQUEST['identifier'];

        try {
            $invoiceCalculation = sendRequest('POST', MERCHANT_URL . '/api/xendit/get-invoice-calculation', $_SESSION['orderRequestBody'][$identifier]);
        } catch (Exception $e) {
            $invoiceCalculation = [];
        }

        echo json_encode($invoiceCalculation);
        die();
    } else {
        echo json_encode(0);
        die();
    }
}
add_action('wp_ajax_getTotalPayment','getTotalPayment');
add_action('wp_ajax_nopriv_getTotalPayment', 'getTotalPayment');

function get_checkoutPayment() {
    // For tracking which Tab make the request
    if (isset($_REQUEST['identifier'])) {
        $identifier = $_REQUEST['identifier'];
    } else {
        echo json_encode(0);
        die();
    }

    $requestBody = $_SESSION['orderRequestBody'][$identifier];
    $paymentMethod = strtolower($_REQUEST['paymethod']);
    $selectedPayment = $_REQUEST['chs_bank'];
    $phone = $_REQUEST['nohp'];
    $voucherId = isset($_SESSION['voucherId'][$identifier]) ? $_SESSION['voucherId'][$identifier] : null;
    
    if ($paymentMethod === 'ewallet') {
        $selectedPayment = $_REQUEST['chs_ewallet'];
    } else if ($paymentMethod === 'retail') {
        $selectedPayment = $_REQUEST['chs_retail'];
    }

    $requestBody['voucher_id'] = $voucherId;
    $requestBody['payment_method'] = $paymentMethod;
    $requestBody['selected_payment'] = $selectedPayment;
    $requestBody['phone_number'] = $phone;

    $createOrder = sendRequest('POST', MERCHANT_URL . '/api/orders', $requestBody);

    if (http_response_code() === 200) {
        $_SESSION['order_id'] = encryptString($createOrder->data->order_id);

        // ============== reset all session ==============
        unset($_SESSION['cartIds']);
        unset($_SESSION['checkoutAddressId'][$identifier]);
        unset($_SESSION['couriers'][$identifier]);
        unset($_SESSION['orderRequestBody'][$identifier]);
        unset($_SESSION['shippingNote'][$identifier]);
        unset($_SESSION["voucherId"][$identifier]);
        // ============== reset all session ==============
    }

    echo json_encode($createOrder);
    die();
}
add_action('wp_ajax_get_checkoutPayment','get_checkoutPayment');
add_action('wp_ajax_nopriv_get_checkoutPayment', 'get_checkoutPayment');

/**
 * fungsi get papyment instructions dari pembayaran virtual account xendit
 * @param $bankName
 * @param $noVa
 * @return array
 */
function xendit_payment_instructions($bankName, $noVa){
    $urlInstructions = "https://invoice-staging.xendit.co/static/locales/id/bni_instructions.json";

    if ($bankName === 'MANDIRI') {
        $urlInstructions = "https://invoice-staging.xendit.co/static/locales/id/mandiri_instructions.json";
    } elseif ($bankName === 'BCA') {
        $urlInstructions = "https://invoice-staging.xendit.co/static/locales/id/bca_instructions.json";
    } elseif ($bankName === 'PERMATA') {
        $urlInstructions = "https://invoice-staging.xendit.co/static/locales/id/permata_instructions.json";
    } elseif ($bankName === 'BRI') {
        $urlInstructions = "https://invoice-staging.xendit.co/static/locales/id/bri_instructions.json";
    }

    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $urlInstructions);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    $result = curl_exec($ch);
    curl_close($ch);

    $instructions = json_decode($result, true);
    $resultInstructions = [];

    foreach ($instructions as $key => $instruction) {
        $method = strtoupper($key);

        if (strpos($method, "ATM") !== false || strpos($method, "SMS") !== false) {
            $method = substr_replace($method, " ", 3, 0);
        } elseif (strpos($method, "MOBILEBANKING") !== false) {
            $method = substr_replace($method, " ", 6, 0);
            $method = substr_replace($method, " ", 14, 0);
        } elseif (strpos($method, "IBANKPERSONAL") !== false) {
            $method = substr_replace($method, " ", 5, 0);
            $method = substr_replace($method, " ", 14, 0);
        } elseif (strpos($method, "TELLER") !== false) {
            $method = substr_replace($method, " ", 6, 0);
        } elseif (strpos($method, "OTHER") !== false) {
            $method = 'BANK LAIN';
        } elseif (strpos($method, "PERMATAMOBILE") !== false) {
            $method = substr_replace($method, " ", 7, 0);
            $method = substr_replace($method, " ", 14, 0);
            $method = trim($method);
        } elseif (strpos($method, "INTERNETBANKING") !== false) {
            $method = substr_replace($method, " ", 8, 0);
            $method = substr_replace($method, " ", 16, 0);
        }

        $resultInstructions[$method] = [];

        foreach ($instruction as $step) {
            $fixedStep = str_replace('{{companyCode}}', '88608', $step);
            $fixedStep = str_replace('{{companyName}}', '88608 XENDIT', $fixedStep);
            $fixedStep = str_replace('{{- vaNumber}}', $noVa, $fixedStep);
            $resultInstructions[$method][] = $fixedStep;
        }
    }

     return $resultInstructions;
}

function ajax_userConfirmDelivered () {
    $currentUser = wp_get_current_user();
    $userId = $currentUser->ID;
    $orderMerchantId = $_REQUEST['order_id'];
    $response = 0;

    if ($orderMerchantId !== '' AND $userId != 0) {
        try {
            sendRequest('PATCH', MERCHANT_URL . '/api/orders/complete/' . $orderMerchantId);

            $response = 1;
        } catch (Exception $e) {
            // Do nothing
        }
    }

    echo json_encode($response);
    die();
}   
add_action('wp_ajax_ajax_userConfirmDelivered','ajax_userConfirmDelivered');
add_action('wp_ajax_nopriv_ajax_userConfirmDelivered', 'ajax_userConfirmDelivered');

function ajaxGetTracking()
{
    global $wpdb;

    $orderMerchantId = $_REQUEST['id'];
    $trackOrder = sendRequest('GET', MERCHANT_URL . '/api/orders/track-order/' . $orderMerchantId);

    echo json_encode($trackOrder->data);
    die();
}

add_action('wp_ajax_ajaxGetTracking','ajaxGetTracking');
add_action('wp_ajax_nopriv_ajaxGetTracking', 'ajaxGetTracking');

function get_updateNotification(){

	date_default_timezone_set("Asia/Jakarta");
	$nowdate = date('Y-m-d H:i:s');

	$current_user = wp_get_current_user();
	$u_id = $current_user->ID;

	$notif_id = $_REQUEST['notif_id'];

	// update notif status = 1
	$detail = Notifications::detail($notif_id);

	// $valid = 'masuk pak eko';
	echo json_encode('');
	die();
}   
add_action('wp_ajax_get_updateNotification','get_updateNotification');
add_action('wp_ajax_nopriv_get_updateNotification', 'get_updateNotification');


function ajax_getProductBrand(){
	$current_user = wp_get_current_user();
	$u_id = $current_user->ID;


	$offset = $_REQUEST['offset'];
	$limit_item = $_REQUEST['limit'];
	$brand_id = $_REQUEST['brand'];

	$minrange = $_REQUEST['minrange'];
	$maxrange = $_REQUEST['maxrange'];
	$filter_sortby = $_REQUEST['filter_sortby'];

	if(isset($filter_sortby) AND $filter_sortby == 'asc'){
		$sort_query = "ORDER BY a.post_title ASC";
	}else if(isset($filter_sortby) AND $filter_sortby == 'desc'){
		$sort_query = "ORDER BY a.post_title DESC";
	}else if(isset($filter_sortby) AND $filter_sortby == 'termurah'){
		$sort_query = "ORDER BY CAST(price AS UNSIGNED) ASC";
	}else{
		$sort_query = "ORDER BY a.id DESC";
	}

	if($minrange != '' AND $maxrange != ''){
		// $rangeprice_query = "AND (c.meta_key = '_regular_price' AND CAST(c.meta_value AS UNSIGNED) BETWEEN '$minrange' AND '$maxrange' )";
		$rangeprice_query = "AND ( c.meta_key = '_regular_price' AND CAST(c.meta_value AS UNSIGNED) BETWEEN '$minrange' AND '$maxrange' ) 
							AND  ( d.meta_key = '_sale_price' AND CAST(d.meta_value AS UNSIGNED) BETWEEN '$minrange' AND '$maxrange' ) ";
	}else{
		// $rangeprice_query = "AND b.meta_key = '_stock_status' AND b.meta_value = 'instock'";
		$rangeprice_query = "";
	}

	global $wpdb;
	$wpdb_query = "SELECT DISTINCT a.id, a.post_date, a.post_title, b.meta_key , b.meta_value, c.meta_key , IFNULL(NULLIF(d.meta_value, '' ), c.meta_value) AS price
					FROM ldr_posts a
					LEFT JOIN ldr_postmeta b
					ON a.id = b.post_id
					AND b.meta_key = 'product_brand'
					LEFT JOIN ldr_postmeta c
					ON a.id = c.post_id
					AND c.meta_key = '_regular_price'
					LEFT JOIN ldr_postmeta d
					ON d.post_id = a.id
					AND d.meta_key = '_sale_price'
					WHERE 1=1  
					AND a.post_type=  'product' 
					AND ( b.meta_key = 'product_brand' AND b.meta_value = '$brand_id' )
					$rangeprice_query
					AND a.post_status = 'publish'
					$sort_query
					LIMIT $limit_item OFFSET $offset";
	$res_query = $wpdb->get_results($wpdb_query, OBJECT);
	$count_res = count($res_query);

	if($count_res > 0){
		foreach ($res_query as $key => $value){
			$valid = 1;
			$product_id = $value->id;
			$product = wc_get_product( $product_id );

			$product_name = $value->post_title;
			$short_name = $value->post_title;
			if(strlen($short_name) > 35) $short_name = substr($short_name, 0, 35).'...';
			$product_link = get_the_permalink($product_id);
			$product_price = $product->get_regular_price();
			$product_sale = $product->get_sale_price();

			$thumb = wp_get_attachment_image_src( get_post_thumbnail_id($product_id), 'medium' );
			if($thumb){
				$urlphoto = $thumb['0'];
			}else{
				$urlphoto = get_template_directory_uri().'/library/images/default_sorry.jpg';
			}
            $def_image = get_template_directory_uri().'/library/images/default_sorry.jpg';
            
            if (isset($product_sale) AND $product_sale != 0) {
                $pr_diskon = (((int)$product_price - (int)$product_sale ) / (int)$product_price ) * 100;
                $pr_diskon = round($pr_diskon, 2);
            } else {
                $pr_diskon = 0;
            }

			$alt = 'Ladara Indonesia Gallery';
		
			$html = '<div class="col-xs-6 col-md-4 col_bx_productList">
							<div class="bx_productList">';
							
								if ($u_id != 0) {
									global $wpdb;
									$wpdb_query = "SELECT * FROM ldr_wishlist WHERE user_id = '$u_id' AND product_id = '$product_id' ";
									$res_query = $wpdb->get_results($wpdb_query, OBJECT);
									$count_res = count($res_query);
									if($count_res == 0){
										$love_1 = 'act';
										$love_2 = '';
										
										$html .= '<div class="mg_heart addWishlist love_'.$product_id.'" title="Tambah ke wishlist" data-id="'.$product_id.'">
											<span class="glyphicon glyphicon-heart"></span>
										</div>';
									}else{
										$love_2 = 'act';	
										
										$html .= '<div id="btn_" class="mg_heart removewishlist love_'.$product_id.' act" title="Hapus dari wishlist" data-id="'.$product_id.'" >
											<span class="glyphicon glyphicon-heart"></span>
										</div>';
									}
								}
								
								$html .= '<a href="'.$product_link.'" title="Lihat '.$product_name.'">
											<div class="mg_productList">
												<img src="'.$urlphoto.'" alt="'.$alt.'">
											</div>
											<div class="bx_in_productList">
												<h4 class="ht_productList">'.$short_name.'</h4>';
								
											if(isset($product_sale) AND $product_sale != ''){ 
                                                $html .= '<div class="tx_price">Rp '.number_format($product_sale).'</div>';
                                                
                                                if (intval($product_sale) !== intval($product_price)) {
                                                    $html .= '<div class="tx_price_sale" style="text-decoration: none;">
                                                                <span style="text-decoration: line-through;">Rp '.number_format($product_price).'</span>
                                                                <span class="discount">'.$pr_diskon.'%</span>
                                                             </div>';
                                                }
										 	}else{
										 		$html .= '<div class="tx_price">Rp '.number_format($product_price).'</div>';
											
											}

										$html .= '<div class="bx_rate_product">';
 										
											// $pro_rate = get_field('product_rate',$product_id);
											// $total_product_rate = get_field('total_product_rate',$product_id);
											// $count_rate = $total_product_rate;
											// $full_star = $pro_rate;
											// $empty_star = 5;
											// if($count_rate != 0){
											// 	for ($a=1; $a <= $empty_star; $a++) { 
											// 		if($a <= $full_star){
											// 			$html .= '<img src="'.get_template_directory_uri().'/library/images/star1.svg">';
											// 		}else{
											// 			$html .= '<img src="'.get_template_directory_uri().'/library/images/star0.svg">';
											// 		}
											// 	} 
												
											// 	$html .= '<span class="sp_rate_info">('.$count_rate.')</span>';
											
											// }
											
								$html .= '</div>
									</div>
								</a>	
							</div>
						</div>';

		}
	}else{
		$valid = 0;
        $html = get_info_noproducts(); // get no products info
	}

	global $wpdb;
	$wpdb_query2 = "SELECT DISTINCT a.id, a.post_date, a.post_title, b.meta_key , b.meta_value, c.meta_key , IFNULL(NULLIF(d.meta_value, '' ), c.meta_value) AS price
					FROM ldr_posts a
					LEFT JOIN ldr_postmeta b
					ON a.id = b.post_id
					AND b.meta_key = 'product_brand'
					LEFT JOIN ldr_postmeta c
					ON a.id = c.post_id
					AND c.meta_key = '_regular_price'
					LEFT JOIN ldr_postmeta d
					ON d.post_id = a.id
					AND d.meta_key = '_sale_price'
					WHERE 1=1  
					AND a.post_type=  'product' 
					AND ( b.meta_key = 'product_brand' AND b.meta_value = '$brand_id' )
					$rangeprice_query
					AND a.post_status = 'publish'
					$sort_query";
	$res_query2 = $wpdb->get_results($wpdb_query2, OBJECT);
	$count_allproduct = count($res_query2);

	$return = array(
					'html' => $html,
					'count_allproduct' => $count_allproduct,
					'valid' => $valid
					);

	
	// $valid = 'masuk pak eko';
	echo json_encode($return);
	die();
}   
add_action('wp_ajax_ajax_getProductBrand','ajax_getProductBrand');
add_action('wp_ajax_nopriv_ajax_getProductBrand', 'ajax_getProductBrand');

function encryptString($string)
{
    $secretKey = 'ladaradharmabakti';
    $secretIv = 'baktidharmaladara';
    $encrypt_method = "AES-256-CBC";
    $key = hash('sha256', $secretKey);
    $iv = substr(hash('sha256', $secretIv), 0, 16);

    try {
        $output = base64_encode(openssl_encrypt($string, $encrypt_method, $key, 0, $iv));
    } catch (Exception $e) {
        $output = '';
    }

    return $output;
}

function decryptString($string)
{
    $secretKey = 'ladaradharmabakti';
    $secretIv = 'baktidharmaladara';
    $encrypt_method = "AES-256-CBC";
    $key = hash( 'sha256', $secretKey );
    $iv = substr( hash( 'sha256', $secretIv ), 0, 16 );

    try {
        $output = openssl_decrypt(base64_decode($string), $encrypt_method, $key, 0, $iv);
    } catch (Exception $e) {
        $output = '';
    }
 
    return $output;
}

function ajaxSearchKotaMerchant()
{
    $searchQuery = $_REQUEST['term']['term'];

    $sendRequest = sendRequest('GET', MERCHANT_URL . '/api/shipper/search-suburb?substring=' . $searchQuery);
    $response = $sendRequest->data ?? [];

    echo json_encode($response);
    die();
}

add_action('wp_ajax_ajaxSearchKotaMerchant','ajaxSearchKotaMerchant');
add_action('wp_ajax_nopriv_ajaxSearchKotaMerchant', 'ajaxSearchKotaMerchant');

function ajaxSearchKodePosMerchant()
{
    $suburbId = $_REQUEST['suburb'];

    $sendRequest = sendRequest('GET', MERCHANT_URL . '/api/shipper/search-postcodes?suburb_id=' . $suburbId);
    $response = $sendRequest->data ?? [];

    echo json_encode($response);
    die();
}

add_action('wp_ajax_ajaxSearchKodePosMerchant','ajaxSearchKodePosMerchant');
add_action('wp_ajax_nopriv_ajaxSearchKodePosMerchant', 'ajaxSearchKodePosMerchant');

function ajaxSearchLocationMerchant()
{
    $postalCode = $_REQUEST['postal_code'];

    $sendRequest = sendRequest('GET', MERCHANT_URL . '/api/shipper/search-location?postal_code=' . $postalCode);
    $response = $sendRequest->data ?? [];

    echo json_encode($response);
    die();
}

add_action('wp_ajax_ajaxSearchLocationMerchant','ajaxSearchLocationMerchant');
add_action('wp_ajax_nopriv_ajaxSearchLocationMerchant', 'ajaxSearchLocationMerchant');

function ajaxRegisterMerchant()
{
    $submit = sendRequest('POST', MERCHANT_URL . '/api/merchant/register', $_REQUEST);

    echo json_encode($submit);
    die();
}

add_action('wp_ajax_ajaxRegisterMerchant','ajaxRegisterMerchant');
add_action('wp_ajax_nopriv_ajaxRegisterMerchant', 'ajaxRegisterMerchant');


function ajaxUlasan()
{
    $order_merchant_id = htmlspecialchars($_POST['order_merchant_id']);
    $product_id = htmlspecialchars($_POST['product_id']);
    $rating = htmlspecialchars($_POST['rating']);
    $review = htmlspecialchars($_POST['review']);


    $httpClient = new Client();
    $jar = CookieJar::fromArray([
        'tnahcremaradal' => $_COOKIE['tnahcremaradal'] ?? null
    ], '.' . $_SERVER['HTTP_HOST']);

    $response = [];

    $dataPost = [];
    foreach ($_FILES['images']['tmp_name'] as $key => $val){
        if ($val != ''){
            $dataPost[] = [
                'name'     => 'images[]',
                'contents' => fopen($val, 'r'),
                'filename' => $_FILES['images']['name'][$key],
            ];
        }
    }

    $dataPost[] = [
        'name'     => 'order_merchant_id',
        'contents' => $order_merchant_id
    ];
    $dataPost[] = [
        'name'     => 'product_id',
        'contents' => $product_id
    ];
    $dataPost[] = [
        'name'     => 'rating',
        'contents' => $rating
    ];
    $dataPost[] = [
        'name'     => 'review',
        'contents' => $review
    ];

    try {
        $request = $httpClient->request('POST', MERCHANT_URL.'/api/review', [
            'headers' => [
                'Accept' => 'application/json'
            ],
            'cookies' => $jar,
            'multipart' => $dataPost
        ]);

        $response = json_decode($request->getBody());
        http_response_code($request->getStatusCode());
    } catch (\GuzzleHttp\Exception\RequestException $e) {
        $response = json_decode($e->getResponse()->getBody()->getContents());
        http_response_code($e->getResponse()->getStatusCode());
    }

    echo json_encode($response);
    die();
}

add_action('wp_ajax_ajaxUlasan','ajaxUlasan');
add_action('wp_ajax_nopriv_ajaxUlasan', 'ajaxUlasan');

function ajaxUpdateMerchant()
{
    $currentUser = wp_get_current_user();
    $userId = $currentUser->ID;

    $updateByAdmin = sendRequest('PATCH', MERCHANT_URL . '/api/merchant/update-by-admin/' . $_REQUEST['merchant_id'], [
        'id' => intval($userId),
        'name' => $_REQUEST['name'],
        'url' => $_REQUEST['url'],
        'status' => intval($_REQUEST['status']),
        'ladara_commission' => intval($_REQUEST['ladara_commission']),
        'province_id' => intval($_REQUEST['province_id']),
        'city_id' => intval($_REQUEST['city_id']),
        'suburb_id' => intval($_REQUEST['suburb_id']),
        'area_id' => intval($_REQUEST['area_id']),
        'postal_code' => intval($_REQUEST['postal_code']),
        'latitude' => $_REQUEST['latitude'],
        'longitude' => $_REQUEST['longitude']
    ]);

    echo json_encode($updateByAdmin);
    die();
}

add_action('wp_ajax_ajaxUpdateMerchant','ajaxUpdateMerchant');
add_action('wp_ajax_nopriv_ajaxUpdateMerchant', 'ajaxUpdateMerchant');

function ajaxStoreDiscussion()
{
    $submit = sendRequest('POST', MERCHANT_URL . '/api/discussions', [
        'discussion_id' => $_REQUEST['discussion_id'],
        'product_id' => $_REQUEST['product_id'],
        'content' => $_REQUEST['content']
    ]);

    echo json_encode($submit);
    die();
}

add_action('wp_ajax_ajaxStoreDiscussion','ajaxStoreDiscussion');
add_action('wp_ajax_nopriv_ajaxStoreDiscussion', 'ajaxStoreDiscussion');

function ajaxDeleteDiscussion()
{
    $discussionDetailId = $_REQUEST['discussion_detail_id'] ?? 0;

    $submit = sendRequest('DELETE', MERCHANT_URL . '/api/discussions/' . $discussionDetailId, [
        'type' => 1
    ]);

    echo json_encode($submit);
    die();
}

add_action('wp_ajax_ajaxDeleteDiscussion','ajaxDeleteDiscussion');
add_action('wp_ajax_nopriv_ajaxDeleteDiscussion', 'ajaxDeleteDiscussion');

function ajaxGetProductVouchers()
{
    $keyword = $_REQUEST['keyword'];

    global $wpdb;
    $products = $wpdb->get_results("SELECT * FROM ldr_products WHERE deleted_at IS NULL AND title LIKE '%$keyword%' LIMIT 10");

    $response = array_map(function($fn) {
        return [
            'label' => $fn->title,
            'value' => $fn->id
        ];
    }, $products);

    echo json_encode($response);
    die();
}

add_action('wp_ajax_ajaxGetProductVouchers','ajaxGetProductVouchers');
add_action('wp_ajax_nopriv_ajaxGetProductVouchers', 'ajaxGetProductVouchers');

function ajaxSaveVoucher()
{
    global $wpdb;
    $id = $_REQUEST['id'];
    $uploadDir = wp_upload_dir();

    $file_name = $_FILES['banner']['name'];
    $file_size = $_FILES['banner']['size'];
    $file_tmp = $_FILES['banner']['tmp_name'];
    $file_ext = strtolower(end(explode('.',$_FILES['banner']['name'])));
    $extensions= array("jpeg","jpg","png");
    $uploadError = '';
    $checkUpload = true;

    if ($id !== null) {
        $checkUpload = $_FILES['banner'] !== null;
    }
    
    if ($checkUpload) {
        if ($file_name === null) {
            $uploadError = 'Wajib Upload Banner';
        } elseif (in_array($file_ext, $extensions) === false) {
            $uploadError = "Hanya ekstensi JPEG, JPG, dan PNG yang diperbolehkan.";
        } elseif ($file_size > 2097152) {
            $uploadError = 'File size tidak boleh melebihi 2 MB';
        }
    }
    
    if ($uploadError !== '') {
        http_response_code(500);

        echo json_encode($uploadError);
        die();
    }

    $requestBody = [
        'target' => intval($_REQUEST['target']),
        'name' => $_REQUEST['name'],
        'code' => $_REQUEST['code'],
        'description' => $_REQUEST['description'],
        'free_shipping' => $_REQUEST['free_shipping'] === "true",
        'period_start_date' => $_REQUEST['period_start_date'],
        'period_start_time' => $_REQUEST['period_start_time'],
        'period_end_date' => $_REQUEST['period_end_date'],
        'period_end_time' => $_REQUEST['period_end_time'],
        'discount_type' => intval($_REQUEST['discount_type']),
        'amount_percent' => intval(str_replace(',', '', $_REQUEST['amount_percent'])),
        'amount_percent_max' => intval(str_replace(',', '', $_REQUEST['amount_percent_max'])),
        'amount_fix' => intval(str_replace(',', '', $_REQUEST['amount_fix'])),
        'min_spend' => intval(str_replace(',', '', $_REQUEST['min_spend'])),
        'max_spend' => intval(str_replace(',', '', $_REQUEST['max_spend'])),
        'quota' => intval(str_replace(',', '', $_REQUEST['quota'])),
        'quota_per_user' => intval(str_replace(',', '', $_REQUEST['quota_per_user'])),
        'products' => $_REQUEST['products'],
        'categories' => $_REQUEST['categories'],
        'administrator' => true
    ];

    if ($id === null) {
        $createVoucher = sendRequest('POST', MERCHANT_URL . '/api/vouchers', $requestBody);
    } else {
        $createVoucher = sendRequest('PATCH', MERCHANT_URL . '/api/vouchers/' . $id, $requestBody);
    }

    if ($checkUpload) {
        if (http_response_code() === 200) {
            move_uploaded_file($file_tmp, $uploadDir['path'] . '/' . time() . '_' . $file_name);

            $wpdb->update('ldr_vouchers', [
                'banner' => str_replace(home_url('/'), '', $uploadDir['url']) . '/' . time() . '_' . $file_name
            ], ['id' => $createVoucher->data->id]);
        }
    }

    echo json_encode($createVoucher);
    die();
}

add_action('wp_ajax_ajaxSaveVoucher','ajaxSaveVoucher');
add_action('wp_ajax_nopriv_ajaxSaveVoucher', 'ajaxSaveVoucher');

function ajaxDeleteVoucher()
{
    $id = $_REQUEST['id'];
    date_default_timezone_set("Asia/Jakarta");

    sendRequest('DELETE', MERCHANT_URL . '/api/vouchers/' . $id);

    echo json_encode(1);
    die();
}

add_action('wp_ajax_ajaxDeleteVoucher','ajaxDeleteVoucher');
add_action('wp_ajax_nopriv_ajaxDeleteVoucher', 'ajaxDeleteVoucher');

function ajaxDownloadInvoice()
{
    $id = isset($_REQUEST['id']) ? htmlspecialchars($_REQUEST['id']) : 0;

    $httpClient = new Client();
    $jar = CookieJar::fromArray([
        'tnahcremaradal' => $_COOKIE['tnahcremaradal']
    ], '.' . $_SERVER['HTTP_HOST']);

    try {
        $request = $httpClient->request('GET', MERCHANT_URL . '/api/orders/get-invoice-order/' . $id, [
            'headers' => [
                'Content-Type' => 'application/json',
                'Accept' => 'application/pdf'
            ],
            'cookies' => $jar
        ]);

        ob_end_clean();
        ob_start();
        header("Content-type:application/pdf");
        header("Content-Disposition:attachment;filename='downloaded.pdf'");

        echo $request->getBody()->getContents();
        die();
    } catch (\GuzzleHttp\Exception\RequestException $e) {
        http_response_code(500);
        echo json_encode(0);
        die();
    }
}

add_action('wp_ajax_ajaxDownloadInvoice','ajaxDownloadInvoice');
add_action('wp_ajax_nopriv_ajaxDownloadInvoice', 'ajaxDownloadInvoice');

function ajaxDownloadOrderReport()
{
    $startDate = isset($_REQUEST['start_date']) ? htmlspecialchars($_REQUEST['start_date']) : date('Y-m-d');
    $endDate = isset($_REQUEST['end_date']) ? htmlspecialchars($_REQUEST['end_date']) : date('Y-m-d');

    $httpClient = new Client();
    $jar = CookieJar::fromArray([
        'tnahcremaradal' => $_COOKIE['tnahcremaradal']
    ], '.' . $_SERVER['HTTP_HOST']);

    try {
        $request = $httpClient->request('POST', MERCHANT_URL . '/api/orders/download-order-report', [
            'headers' => [
                'Content-Type' => 'application/json',
                'Accept' => 'application/x-www-form-urlencoded'
            ],
            'cookies' => $jar,
            'json' => [
                'start_date' => $startDate,
                'end_date' => $endDate,
                'type' => 'All'
            ]
        ]);

        ob_end_clean();
        ob_start();
        header("Content-type:application/x-www-form-urlencoded");
        header("Content-Disposition:attachment;filename='downloaded.'");

        echo $request->getBody()->getContents();
        die();
    } catch (\GuzzleHttp\Exception\RequestException $e) {
        var_dump($e->getMessage());
        http_response_code(500);
        echo json_encode(0);
        die();
    }
}

add_action('wp_ajax_ajaxDownloadOrderReport','ajaxDownloadOrderReport');
add_action('wp_ajax_nopriv_ajaxDownloadOrderReport', 'ajaxDownloadOrderReport');

function ajaxConfirmPayment()
{
    $id = isset($_REQUEST['order_id']) ? htmlspecialchars($_REQUEST['order_id']) : 0;

    $confirmPayment = sendRequest('PATCH', MERCHANT_URL . '/api/orders/pay/' . $id);

    echo json_encode($confirmPayment);
    die();
}

add_action('wp_ajax_ajaxConfirmPayment','ajaxConfirmPayment');
add_action('wp_ajax_nopriv_ajaxConfirmPayment', 'ajaxConfirmPayment');

function ajaxCompleteOrder()
{
    $id = isset($_REQUEST['order_id']) ? htmlspecialchars($_REQUEST['order_id']) : 0;

    $completeOrder = sendRequest('PATCH', MERCHANT_URL . '/api/orders/complete/' . $id);

    echo json_encode($completeOrder);
    die();
}

add_action('wp_ajax_ajaxCompleteOrder','ajaxCompleteOrder');
add_action('wp_ajax_nopriv_ajaxCompleteOrder', 'ajaxCompleteOrder');

function getMerchant($userId)
{
    global $wpdb;
    $dataMerchant = $wpdb->get_row("SELECT * FROM ldr_merchants WHERE user_id = $userId AND deleted_at IS NULL");

    return $dataMerchant;
}

function getMerchantByOrderMerchant($orderMercahntId)
{
    global $wpdb;
    $dataMerchant = $wpdb->get_row("
        select * from ldr_order_merchants om
        LEFT JOIN ldr_merchants m on om.merchant_id = m.id
        where om.id = $orderMercahntId
    ");

    return $dataMerchant;
}

function getUlasan($productId, $rate=0)
{
    $httpClient = new Client();
    try {
        $request = $httpClient->request('GET', MERCHANT_URL.'/api/review', [
            'headers' => [
                'Accept' => 'application/json'
            ],
            'query' => [
                'product_id' => $productId,
                'rating' => ($rate != 0) ? $rate : ''
            ]
        ]);

        $review = json_decode($request->getBody());
    } catch (\GuzzleHttp\Exception\RequestException $e) {
        $review = json_decode($e->getResponse()->getBody()->getContents());
    }

    try {
        $request = $httpClient->request('GET', MERCHANT_URL.'/api/review/summary', [
            'headers' => [
                'Accept' => 'application/json'
            ],
            'query' => [
                'product_id' => $productId
            ]
        ]);

        $summary = json_decode($request->getBody());
    } catch (\GuzzleHttp\Exception\RequestException $e) {
        $summary = json_decode($e->getResponse()->getBody()->getContents());
    }

    $data = [
        'ulasan' => $review,
        'summary' => $summary
    ];

//    print_r($data['ulasan']->data);die();
    return $data;
}

function insertPostViewed($post_id){

	date_default_timezone_set("Asia/Jakarta");
	$nowdate = date('Y-m-d H:i:s');

	if(isset($post_id) AND $post_id != ''){

		global $wpdb;
		$wpdb_query = "SELECT * FROM ldr_postviewed WHERE post_id = '$post_id'";
		$res_query = $wpdb->get_results($wpdb_query, OBJECT);
		$count_res = count($res_query);
		if($count_res > 0){ // if not exist with same user_id

			foreach ($res_query as $key => $value) {

				$total = $value->total;
				$count_total = $total+1;

				global $wpdb;
				$wpdb_query_update = " UPDATE ldr_postviewed
				              SET total = $count_total, created_date = '$nowdate'
				              WHERE post_id = '$post_id'
				            ";
				$res_query_update = $wpdb->query($wpdb_query_update);

				update_field("product_viewed",$count_total,$post_id);
			}
	       

		}else{

			$count_total = 1;

			global $wpdb;
			$query_insert = "INSERT INTO ldr_postviewed(post_id,total,created_date) VALUES('$post_id','$count_total','$nowdate')";
			$result_insert = $wpdb->query($query_insert);

			update_field("product_viewed",$count_total,$post_id);
		}		
	}
}

function ajax_get_products(){
    $title = $_GET['q'];
    $req = sendRequest('GET', MERCHANT_URL.'/api/product/search-products',['title' => $title]);
    $resp = [];
    if ($req->status == 200){
        $products = $req->data->products;
        foreach ($products as $product){
            $resp[] = [
                'text' => $product->name,
                'id' => $product->id
            ];
        }
    }
    echo json_encode($resp);
    die();
}

add_action('wp_ajax_ajax_get_products','ajax_get_products');
add_action('wp_ajax_nopriv_ajax_get_products', 'ajax_get_products');

function getProductById($productId)
{
    global $wpdb;
    $product = $wpdb->get_row("
            SELECT * FROM ldr_products p
            LEFT JOIN ldr_product_images pi ON pi.products_id = p.id
            WHERE pi.name = 'imageUtama' and p.id = $productId
        ");

    return $product;
}

/**
 * Wrapper for guzzle
 *
 * @param  string  $httpMethod
 * @param  string  $endpoint
 * @param  array  $data
 * @return array|object
 */
function sendRequest($httpMethod, $endpoint, $data = [])
{
    $httpClient = new Client();
    $jar = CookieJar::fromArray([
        'tnahcremaradal' => $_COOKIE['tnahcremaradal'] ?? null
    ], '.' . $_SERVER['HTTP_HOST']);

    $response = [];

    try {
        $request = $httpClient->request($httpMethod, $endpoint, [
            'headers' => [
                'Accept' => 'application/json'
            ],
            'cookies' => $jar,
            'json' => $data
        ]);

        $response = json_decode($request->getBody());
        http_response_code($request->getStatusCode());
    } catch (\GuzzleHttp\Exception\RequestException $e) {
        $response = json_decode($e->getResponse()->getBody()->getContents());
        http_response_code($e->getResponse()->getStatusCode());
    }

    return $response;
}

add_action('init', function() {
    global $wp_rewrite;

    add_rewrite_rule( 
        '^seller/([^/]+)([/]?)(.*)', 
        'index.php?pagename=seller&merchant=$matches[1]', 
        'top'
    );

    add_rewrite_rule(
        '^product/([^/]+)([/]?)(.*)',
        'index.php?pagename=product-detail&slug=$matches[1]',
        'top'
    );

    add_rewrite_rule(
        '^brand/([^/]+)([/]?)(.*)',
        'index.php?pagename=brand&slug=$matches[1]',
        'top'
    );

    $wp_rewrite->flush_rules(false);  // This should really be done in a plugin activation
});

if($_SERVER['SERVER_NAME'] === 'ladara.id') 
{
    // Replace src paths
    add_filter('wp_get_attachment_url', function ($url) 
    {
        if(file_exists($url)) 
        {
            return $url;
        }
        return str_replace(get_site_url(), 'https://images.ladara.id', $url);
    });
}

if ($_SERVER['REMOTE_ADDR'] === '127.0.0.1')
{
    // Replace src paths
    add_filter('wp_get_attachment_url', function ($url) 
    {
        if(file_exists($url)) 
        {
            return $url;
        }
        return str_replace(get_site_url(), 'https://images.ladara.id', $url);
    });

}

try {
    add_filter( 'woocommerce_checkout_redirect_empty_cart', '__return_false' );
    add_filter( 'woocommerce_checkout_update_order_review_expired', '__return_false' );
} catch (\Exception $e) {
    // Do nothing
}

// ======================= REMOVE PRODUCT TAGS WOOCOMMERCE =================
add_action( 'admin_menu', 'misha_hide_product_tags_admin_menu', 9999 );
 
function misha_hide_product_tags_admin_menu() {
	remove_submenu_page( 'edit.php?post_type=product', 'edit-tags.php?taxonomy=product_tag&amp;post_type=product' );
}
add_filter('manage_product_posts_columns', 'misha_hide_product_tags_column', 999 );

add_action( 'admin_menu', 'misha_hide_product_tags_metabox' );
 
function misha_hide_product_tags_metabox() {
	remove_meta_box( 'tagsdiv-product_tag', 'product', 'side' );
}

function misha_hide_product_tags_column( $product_columns ) {
	unset( $product_columns['product_tag'] );
	return $product_columns;
}
add_filter( 'quick_edit_show_taxonomy', 'misha_hide_product_tags_quick_edit', 10, 2 );
 
function misha_hide_product_tags_quick_edit( $show, $taxonomy_name ) {
 
    if ( 'product_tag' == $taxonomy_name )
        $show = false;
 
    return $show;
 
}
add_action( 'widgets_init', 'misha_remove_product_tag_cloud_widget' );
 
function misha_remove_product_tag_cloud_widget(){
	unregister_widget('WC_Widget_Product_Tag_Cloud');
}
// ======================= REMOVE PRODUCT TAGS WOOCOMMERCE =================

?>