<?php
/*
Template Name: Page Merchant
*/

use Shipper\Location;

?>
<?php get_header(); ?>
<?php
if (have_posts()) :
    while (have_posts()) :
        the_post();

        global $wpdb;

        $currentUser = wp_get_current_user();
        $userId = $currentUser->ID;

        $tab = isset($_GET['tab']) ? htmlspecialchars($_GET['tab']) : 'products';
        $products = isset($_GET['products']) ? htmlspecialchars($_GET['products']) : 'all';
        $sort = isset($_GET['sort']) ? htmlspecialchars($_GET['sort']) : 'newest';
        $search = isset($_GET['search']) ? htmlspecialchars($_GET['search']) : '';

        $url = preg_replace('/\?.*/', '', $_SERVER['REQUEST_URI']);

        $getMerchant = $wpdb->get_row("SELECT * FROM ldr_merchants WHERE deleted_at IS NULL AND url LIKE '%$url'");

        if ($getMerchant !== null) {
            $merchantId = $getMerchant->id;
            $requestMerchant = sendRequest('GET', MERCHANT_URL . '/api/merchant/' . $merchantId);
            $queryMerchant = $requestMerchant->data;

            $queryOrderItem = $wpdb->get_row("SELECT sum(qty) AS total FROM ldr_order_items WHERE deleted_at IS NULL AND order_merchant_id IN (SELECT id FROM ldr_order_merchants WHERE deleted_at IS NULL AND status > 0 AND merchant_id = $merchantId)");

            try {
                $getLocation = Location::searchLocation($queryMerchant->postal_code);
                $cityName = $getLocation->data->rows[0]->city_name;
            } catch (Exception $e) {
                $cityName = '';
            }

            // Get Discussions
            if ($tab === 'discussions') {
                $page = isset($_GET['page_number']) ? htmlspecialchars($_GET['page_number']) : 1;
            } else {
                $page = 1;
            }

            $responseDiscussions = sendRequest('GET', MERCHANT_URL . '/api/discussions/get-merchant-discussions/' . $merchantId . '?page=' . $page); ?>

            <style>
                .merchant-header {
                    border-radius: 20px;
                    border: 1px solid #e5e5e5;
                    margin-top: -20px;
                    display: flex;
                    padding: 20px;
                    background-color: white;
                    position: relative;
                    box-shadow: 0px 3px 6px #6B6B6B29;
                }

                .btn-new {
                    background-color: white;
                    color: #0080FF;
                    border: 2px solid #0080FF;
                    font-weight: bold;
                    font-size: 12px;
                    width: 18%;
                }

                .left-widget {
                    border: 1px solid #e5e5e5;
                    box-shadow: 0px 0px 6px #00000029;
                    border-radius: 20px;
                }

                .time-widget {
                    background-color: #0080FF;
                    color: white;
                    font-size: 11px;
                    display: flex;
                    justify-content: space-between;
                    padding: 10px 20px;
                }

                .menu-left-widget {
                    padding: 15px 20px;
                    font-weight: bold;
                    color: #878787;
                    cursor: pointer;
                }

                .menu-left-widget span.active {
                    color: black;
                }

                .menu-left-widget img {
                    width: 2.5rem;
                    margin-right: 10px;
                }

                .proTabs li {
                    width: 15%;
                }

                .proTabs li.act {
                    border-bottom: 2px solid #0080FF;
                    color: #0080FF;
                }

                .proTabs li a {
                    color: #878787;
                    text-decoration: none;
                }

                .proTabs li.act a {
                    color: #0080FF;
                }

                .select-order {
                    border-radius: 25px;
                    height: 3.5rem;
                    width: 12rem;
                    padding: 0 5px;
                    border: 1px solid #0080ff;
                    background: white;
                    color: #0080ff;
                }

                .info-toko-header {
                    font-weight: bold;
                    opacity: 0.6;
                    font-size: 90%;
                }

                .info-toko-body {
                    font-weight: 500;
                }

                .wishlist-wrapper {
                    border: 1px solid #e5e5e5;
                    border-radius: 50%;
                    width: 40px;
                    height: 40px;
                    display: inline-flex;
                    margin-right: 10px;
                    cursor: pointer;
                }

                .wishlist-wrapper svg {
                    width: 1rem;
                    height: 1rem;
                    margin: auto;
                    color: #434343;
                }

                .wishlist-wrapper svg.active {
                    color: #FF1E00;
                }

                .content-discussion {
                    display: flex;
                    padding: 1rem;
                    margin: 10px -30px;
                }

                .content-discussion:nth-child(even) {
                    background-color: #f9f9f9;
                }
            </style>

            <div class="row" style="background-color: white; padding: 40px 100px;">
                <div class="col-md-12">
                    <?php
                    if ($queryMerchant->status < 0) :
                    ?>
                        <div class="bx-closed-merchant">
                            <div style="top:-37px; left:-15px;"></div>
                            <div>
                                <img src="<?php bloginfo('template_directory'); ?>/library/images/icon-warning.svg" />
                                <div>
                                    <h4>Toko Sedang Tutup</h4>
                                    
                                    <?php if ($queryMerchant->status === -2) : ?>
                                        <p>
                                            Toko buka kembali pada <?= ($queryMerchant->opened_at) ?> atau kunjungi 
                                            Pusat Bantuan Ladara jika ada transaksi yang tertunda.
                                        </p>
                                    <?php endif; ?>
                                </div>
                            </div>
                        </div>
                    <?php
                    endif;

                    if ($queryMerchant->cover !== home_url('/')) :
                    ?>

                        <img src="<?= $queryMerchant->cover ?>" alt="<?= $queryMerchant->name ?>" style="width: 100%; border-radius: 25px 25px 0 0;">

                    <?php
                    endif; ?>

                    &nbsp;
                    <div class="merchant-header">
                        <div style="width: 12%;">
                            <img src="<?= $queryMerchant->picture ?>" alt="<?= $queryMerchant->name ?>" style="width: 80%; border-radius: 50%;">
                        </div>
                        <div style="display: inline-block; width: 68%;">
                            <div style="font-weight: bold; font-size: 22px;"><?= $queryMerchant->name ?>
                            </div>
                            <div style="font-weight: 500;">
                                <span class="glyphicon glyphicon-map-marker"></span>
                                &ensp;
                                <?= $cityName ?>
                            </div>
                            <div style="margin-top: 10px;">
                                <button type="button" style="margin-right: 10px;" class="sub_aform btn-new" data-toggle="modal" data-target="#infoToko">Info Toko</button>
                                <button type="button" class="sub_aform btn-new" data-toggle="modal" data-target="#catatanToko">Catatan</button>
                            </div>
                        </div>
                        <div style="display: inline-block; width: 20%; border-left: 1px solid #e5e5e5; padding-left: 15px;">
                            <span>
                                <img src="<?php bloginfo('template_directory'); ?>/library/images/cost_active.png" style="width: 1.5rem;">
                            </span>
                            &ensp;
                            <span style="color: #A7A7A7; font-weight: 500;">Produk Terjual</span>
                            <div style="margin-top: 10px;">
                                <h4>
                                    <b><?= $queryOrderItem->total ?? 0 ?> Produk</b>
                                </h4>
                            </div>
                            <!-- <div style="color: #FE5461; margin-top: 30px;">
                                <b>Lihat Statistik Toko</b>
                            </div> -->
                        </div>
                    </div>
                </div>
                <div class="col-md-12" style="margin: 20px 0;">
                    <div style="border-bottom: 1px solid #dddddd; height: 45px;">
                        <ul class="proTabs">
                            <li class="<?= ($tab === 'products' ? 'act' : '') ?>" data-id="tabs1">
                                <a href="javascript:;">Produk</a>
                            </li>
                            <?php
                            if ($userId) :
                            ?>
                                <li class="<?= ($tab === 'discussions' ? 'act' : '') ?>" data-id="tabs2">
                                    <a href="javascript:;">Diskusi</a>
                                </li>
                            <?php
                            endif; ?>
                        </ul>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="left-widget">
                        <div style="font-weight: bold; color: #0080FF; padding: 15px 20px;">Etalase Toko</div>
                        <div class="time-widget">
                            <span id="time-date"></span>
                            <span id="time-clock"></span>
                        </div>
                        <div class="menu-left-widget">
                            <a href="<?= home_url($url) ?>?tab=products&products=all" style="color: inherit;">
                                <img src="<?php bloginfo('template_directory'); ?>/library/images/<?= ($products === 'all' ? 'shop_active' : 'shop') ?>.png">
                                <span class="<?= ($products === 'all' ? 'active' : '') ?>">Semua Produk</span>
                            </a>
                        </div>
                        <div class="menu-left-widget">
                            <a href="<?= home_url($url) ?>?tab=products&products=sold" style="color: inherit;">
                                <img src="<?php bloginfo('template_directory'); ?>/library/images/<?= ($products === 'sold' ? 'cost_active' : 'cost') ?>.png">
                                <span class="<?= ($products === 'sold' ? 'active' : '') ?>">Produk Terjual</span>
                            </a>
                        </div>
                    </div>
                </div>
                <div class="col-md-9">
                    <div class="d-flex">
                        <div style="display: inline-flex; width: 50%;">
                            <input id="searchNow" type="text" name="search" class="txt_searchNow" placeholder="Cari produk toko ini" onkeypress="checkKeyStroke(event)" value="<?= $search ?>">
                            <span class="glyphicon glyphicon-search sp_searchNow" title="Cari sekarang." onclick="submitSearch()"></span>
                            <button type="button" class="sub_search hide" onclick="submitSearch()">
                                <img src="<?php bloginfo('template_directory'); ?>/library/images/logo-cari.png"> Cari
                            </button>
                        </div>
                        <div style="display: inline-block; width: 50%;" align="right">
                            <span style="font-weight: bold; color: #0080FF; margin-right: 15px;">Urut Berdasarkan</span>
                            <select name="sort" class="select-order" onchange="submitSearch()">
                                <option value="newest" <?= ($sort === 'newest' ? 'selected' : '') ?>>Terbaru</option>
                                <option value="lowest" <?= ($sort === 'lowest' ? 'selected' : '') ?>>Termurah</option>
                                <option value="highest" <?= ($sort === 'highest' ? 'selected' : '') ?>>Termahal</option>
                            </select>
                        </div>
                    </div>
                    <div id="tabs1" class="col-md-12 box_detailProduct animated fadeIn <?= ($tab === 'products' ? 'act' : '') ?>">
                        <?php
                        if ($sort === 'newest') {
                            $orderBy = 'ORDER BY created_at DESC';
                        } elseif ($sort === 'highest') {
                            $orderBy = 'ORDER BY price DESC';
                        } else {
                            $orderBy = 'ORDER BY price ASC';
                        }

                        $sql = $wpdb->prepare("SELECT * FROM ldr_products WHERE deleted_at IS NULL AND status = 1 AND merchant_id = $merchantId AND title LIKE '%s' $orderBy", ["%$search%"]);

                        if ($products === 'sold') {
                            $sql = $wpdb->prepare("SELECT * FROM ldr_products WHERE id IN (SELECT product_id FROM ldr_order_items) AND deleted_at IS NULL AND status = 1 AND merchant_id = $merchantId AND title LIKE '%s' $orderBy", ["%$search%"]);
                        }

                        $queryProducts = $wpdb->get_results($sql);

                        if (count($queryProducts)) {
                            foreach ($queryProducts as $product) {
                                $queryProductImage = $wpdb->get_row("SELECT * FROM ldr_product_images WHERE deleted_at IS NULL AND name = 'imageUtama' AND products_id = " . $product->id);

                                $date = date('Y-m-d');
                                $cekDiscount = $wpdb->get_row("
                                                    select * from ldr_discount_product dp
                                                    left join ldr_discount ld on dp.discount_id = ld.id
                                                    where dp.product_id = $product->id
                                                    and dp.deleted_at is null
                                                    and ld.start_date <= DATE('$date')
                                                    and ld.end_date >= DATE('$date')
                                                    ");
                                $percent = 0;
                                if ($cekDiscount){
                                    $percent =  round(($product->price - $cekDiscount->discount_price) / $product->price * 100);
                                }
                                ?>

                                <div class="col-xs-6 col-md-3 col_bx_productList">
                                    <div class="bx_productList">
                                        <a href="/product/<?= $product->permalink.'-'.$product->id ?>" title="Lihat <?= $product->title ?>">
                                            <div class="mg_productList">
                                                <img class="" src="<?= IMAGE_URL.'/' . $queryProductImage->path ?>" alt="Ladara Indonesia Gallery">
                                            </div>
                                            <div class="bx_in_productList">
                                                <h4 class="ht_productList"><?= $product->title ?></h4>
                                                <?php if($cekDiscount):?>
                                                    <div class="tx_price">Rp <?php echo number_format($cekDiscount->discount_price); ?></div>
                                                    <div class="tx_price_sale" style="text-decoration: none;">
                                                        <span style="text-decoration: line-through;">Rp <?php echo number_format($product->price); ?></span>
                                                        <span class="discount"><?php echo $percent; ?>%</span>
                                                    </div>
                                                <?php else:?>
                                                    <div class="tx_price ss">Rp <?= number_format($product->price) ?></div>
                                                <?php endif;?>
                                                <div class="bx_rate_product"></div>
                                            </div>
                                        </a>
                                    </div>
                                </div>

                            <?php
                            }
                        } else {
                            ?>
                            <div class="row row_masterInformation">
                                <div class="col-md-12 col_emptyInformation">
                                    <div class="mg_emptyInformation">
                                        <img src="<?php bloginfo('template_directory'); ?>/library/images/ico_noorder.svg">
                                    </div>
                                    <h2 class="tx_emptyInformation">Tidak ada produk saat ini.</h2>
                                </div>
                            </div>
                        <?php
                        }
                        ?>
                    </div>

                    <?php
                    if ($userId) :
                    ?>
                        <div id="tabs2" class="col-md-12 box_detailProduct animated fadeIn <?= ($tab === 'discussions' ? 'act' : '') ?>">
                            <?php
                            $currentUserAvatar = get_field('user_avatar', 'user_' . $userId);
                            if ($currentUserAvatar !== '') {
                                $linkpoto = wp_get_attachment_image_src($currentUserAvatar, 'thumbnail');
                                $currentUrlProfilePhoto = $linkpoto[0];
                            } else {
                                $currentUrlProfilePhoto = get_template_directory_uri() . '/library/images/icon_profile.png';
                            }

                            foreach ($responseDiscussions->data->data as $discussion) {
                            ?>
                                <div style="border: 1px solid #dddddd; border-radius: 20px; padding: 20px 30px; margin-bottom: 20px;">
                                    <div style="display: flex;">
                                        <div style="width: 15%;">
                                            <img src="<?= $discussion->product_image ?>" style="width: 60%; border-radius: 20px; margin-left: 25%;">
                                        </div>
                                        <div style="width: 55%;">
                                            <div style="font-weight: 600; font-size: 14px;">
                                                <?= $discussion->product_title ?>
                                            </div>
                                            <div style="font-weight: 600; color: #FF1E00; margin: 3px 0;">
                                                Rp <?= $discussion->product_price ?>
                                            </div>
                                            <div><?= $discussion->product_brand ?>
                                            </div>
                                        </div>
                                        <div style="width: 30%;">
                                            <div class="wishlist-wrapper">
                                                <svg class="<?= ($discussion->isWishlisted ? 'active' : '') ?>" viewBox="0 0 16 16" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                                                    <path fill-rule="evenodd" d="M8 1.314C12.438-3.248 23.534 4.735 8 15-7.534 4.736 3.562-3.248 8 1.314z" />
                                                </svg>
                                            </div>
                                            <div class="wishlist-wrapper">
                                                <svg viewBox="0 0 16 16" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                                                    <path fill-rule="evenodd" d="M0 1.5A.5.5 0 0 1 .5 1H2a.5.5 0 0 1 .485.379L2.89 3H14.5a.5.5 0 0 1 .491.592l-1.5 8A.5.5 0 0 1 13 12H4a.5.5 0 0 1-.491-.408L2.01 3.607 1.61 2H.5a.5.5 0 0 1-.5-.5zM5 12a2 2 0 1 0 0 4 2 2 0 0 0 0-4zm7 0a2 2 0 1 0 0 4 2 2 0 0 0 0-4zm-7 1a1 1 0 1 0 0 2 1 1 0 0 0 0-2zm7 0a1 1 0 1 0 0 2 1 1 0 0 0 0-2z" />
                                                </svg>
                                            </div>
                                            <button type="button" class="btn btn-primary" style="width: 50%; border-radius: 20px; margin-top: -9px;">
                                                <b>Beli</b>
                                            </button>
                                        </div>
                                    </div>
                                    <?php
                                    foreach ($discussion->discussionDetails as $discussionDetail) {
                                    ?>
                                        <div class="content-discussion">
                                            <div style="width: 25%; padding-left: 15%;">
                                                <img src="<?= $discussionDetail->picture ?>" style="width: 3.5rem; height: 3.5rem; border-radius: 50%;">
                                            </div>
                                            <div style="width: 60%">
                                                <span style="font-weight: bold;"><?= $discussionDetail->name ?> &#8226; </span>
                                                <span style="font-size: 11px;"><?= $discussionDetail->date ?> pukul <?= $discussionDetail->time ?></span>
                                                <div style="font-weight: 500;">
                                                    <?= $discussionDetail->content ?>
                                                </div>
                                            </div>
                                            <div style="width: 15%">
                                                <?php
                                                if ($discussionDetail->canDeleted) :
                                                ?>
                                                    <button class="btn btn-outline-secondary br-20" type="button">
                                                        <img src="<?= MERCHANT_URL . '/images/delete.svg' ?>" style="width: 20px;">
                                                        Hapus
                                                    </button>
                                                <?php
                                                endif; ?>
                                            </div>
                                        </div>
                                    <?php
                                    } ?>
                                    <div class="content-discussion">
                                        <div style="width: 25%; padding-left: 15%;">
                                            <img src="<?= $currentUrlProfilePhoto ?>" style="width: 3.5rem; height: 3.5rem; border-radius: 50%;">
                                        </div>
                                        <div style="width: 75%;">
                                            <textarea class="form-control" rows="3" maxlength="400"></textarea>
                                        </div>
                                    </div>
                                    <div class="mt-4" align="right">
                                        <button type="button" class="btn btn-primary" style="width: 15%; border-radius: 20px;">
                                            <b>Kirim</b>
                                        </button>
                                    </div>
                                </div>
                            <?php
                            }

                            if (count($responseDiscussions->data->data)) :
                            ?>
                                <div align="center">
                                    <ul class="pagination">
                                        <?php
                                        for ($i = 1; $i <= $responseDiscussions->data->last_page; $i++) {
                                        ?>
                                            <li class="<?= ($i === $responseDiscussions->data->current_page ? 'active' : '') ?>">
                                                <a href="<?= home_url($url) ?>?tab=discussions&page_number=<?= $i ?>"><?= $i ?></a>
                                            </li>
                                        <?php
                                        } ?>
                                    </ul>
                                </div>
                            <?php
                            endif; ?>
                        </div>
                    <?php
                    endif; ?>

                </div>
            </div>
            <div id="infoToko" class="modal fade" role="dialog">
                <div class="modal-dialog" style="width: 75%;">
                    <div class="modal-content">
                        <div class="modal-body">
                            <h2>
                                <b>Info Toko</b>
                            </h2>
                            <div class="d-flex">
                                <div style="width: 50%;">
                                    <div style="margin-top: 5%;">
                                        <span class="info-toko-header">Slogan Toko</span>
                                        <div class="info-toko-body">
                                            <?= $queryMerchant->slogan === '' ? '<i>Tidak Ada</i>' : $queryMerchant->slogan ?>
                                        </div>
                                    </div>
                                    <div style="margin-top: 5%;">
                                        <span class="info-toko-header">Deskripsi Toko</span>
                                        <div class="info-toko-body">
                                            <?= $queryMerchant->description === '' ? 'Tidak Ada' : $queryMerchant->description ?>
                                        </div>
                                    </div>
                                    <div style="margin-top: 5%;">
                                        <span class="info-toko-header">Alamat Toko</span>
                                        <div class="info-toko-body">
                                            <?= $queryMerchant->address ?>
                                        </div>
                                    </div>
                                </div>
                                <div style="width: 50%;">
                                    <?php
                                    $queryMerchantShipment = $wpdb->get_results("SELECT * FROM ldr_shipper_couriers WHERE id IN (SELECT shipper_courier_id FROM ldr_merchant_shipments WHERE merchant_id = $merchantId) AND deleted_at IS NULL");
                                    $tempEcho = []; ?>

                                    <div style="margin-top: 5%;">
                                        <span class="info-toko-header">Layanan Pengiriman</span>
                                        <div class="info-toko-body">
                                            <div class="row">
                                                <?php
                                                foreach ($queryMerchantShipment as $merchantShipment) {
                                                    if (!in_array($merchantShipment->name, $tempEcho)) {
                                                        $tempEcho[] = $merchantShipment->name;

                                                        $listRateNames = array_filter($queryMerchantShipment, function ($fn) use ($merchantShipment) {
                                                            return $fn->name === $merchantShipment->name;
                                                        });

                                                        $listRateNames = array_unique(array_map(function ($fn) {
                                                            return $fn->type;
                                                        }, $listRateNames));
                                                    } else {
                                                        continue;
                                                    } ?>

                                                    <div class="col-md-6">
                                                        <div style="border: 1px solid #e5e5e5; border-radius: 15px; margin: 10px 0;" align="center">
                                                            <img src="<?= MERCHANT_URL ?>/images/<?= $merchantShipment->code ?>.png" alt="<?= $merchantShipment->name ?>" style="width: 45%;">
                                                            <br>
                                                            <b><?= $merchantShipment->name ?></b>
                                                            <br>
                                                            <span style="font-size: 85%;"><?= implode(', ', $listRateNames) ?></span>
                                                        </div>
                                                    </div>

                                                <?php
                                                } ?>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div id="catatanToko" class="modal fade" role="dialog">
                <div class="modal-dialog" style="width: 85%;">
                    <div class="modal-content">
                        <div class="modal-body">
                            <h2>
                                <b>Catatan</b>
                            </h2>
                            <div>
                                <?php
                                $data = $wpdb->get_row("SELECT * FROM ldr_merchant_notes WHERE deleted_at IS NULL AND merchant_id = " . $merchantId); ?>
                                <p><?= $data->body ?>
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <script>
                document.title = "<?= $queryMerchant->name ?>"

                if (window.history.replaceState) {
                    window.history.replaceState(null, null, window.location.href);
                }

                function checkKeyStroke(e) {
                    if (e.keyCode == 13) {
                        submitSearch()
                    }
                }

                function submitSearch() {
                    const search = document.getElementsByName('search')[0].value
                    const sort = document.getElementsByName('sort')[0].value
                    const urlParams = new URLSearchParams(window.location.search)
                    let url = `${window.location.href.split('?')[0]}?tab=products`

                    if (search !== '') {
                        url += `&search=${search}`
                    }

                    if (urlParams.get('products') !== null) {
                        url += `&products=${urlParams.get('products')}`
                    }

                    if (sort !== '') {
                        url += `&sort=${sort}`
                    } else {
                        url += `&sort=${urlParams.get('sort')}`
                    }

                    location.replace(url)
                }

                let arrDays = ['Minggu', 'Senin', 'Selasa', 'Rabu', 'Kamis', 'Jumat', 'Sabtu']
                let arrMonths = ['Jan', 'Feb', 'Mar', 'Apr', 'Mei', 'Jun', 'Jul', 'Agu', 'Sep', 'Okt', 'Nov', 'Des']

                function startTime() {
                    let today = new Date()
                    let dayNumber = today.getDay()
                    let monthNumber = today.getMonth()
                    let h = today.getHours()
                    let m = today.getMinutes()
                    m = checkTime(m);
                    day = arrDays[dayNumber]
                    month = arrMonths[monthNumber]

                    document.getElementById('time-date').innerHTML = `${day}, ${today.getDate()} ${month} ${today.getFullYear()}`
                    document.getElementById('time-clock').innerHTML = `${h}:${m} WIB`

                    let t = setTimeout(startTime, 500);
                }

                function checkTime(i) {
                    if (i < 10) {
                        i = "0" + i
                    }

                    return i;
                }

                startTime()
            </script>
        <?php
        } else {
        ?>
            <div id="content">
                <div id="inner-content" class="wrap clearfix">
                    <div id="main" class="eightcol first clearfix" role="main">
                        <article id="post-not-found" class="hentry clearfix">
                            <div class="row row_finishCheckout">
                                <div class="col-md-12 col_finishCheckout">
                                    <a href="<?php echo home_url(); ?>">
                                        <div class="bx_backShop">
                                            <span class="glyphicon glyphicon-menu-left"></span> Kembali ke Home
                                        </div>
                                    </a>
                                    <div class="bx_finishCheckout">
                                        <div class="mg_registerIcon">
                                            <img class="lazy" data-src="<?php bloginfo('template_directory'); ?>/library/images/ico_404.svg">
                                        </div>
                                        <div class="ht_register">Ups, halaman yang kamu cari belum bisa ditemukan :(</div>

                                        <div class="ht_sucs_register">
                                            Coba lagi dengan menekan tombol dibawah ini ya!
                                        </div>
                                        <div class="bx_def_checkout">
                                            <a href="<?php echo home_url(); ?>/">
                                                <button class="btn_def_checkout">Kembali ke Beranda</button>
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </article>
                    </div>
                </div>
            </div>
<?php
        }
    endwhile;
else :
    get_template_part('content', '404pages');
endif;
get_footer();
