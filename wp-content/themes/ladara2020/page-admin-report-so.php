<?php
/*
Template Name: Admin Report SO
*/
?>
<?php get_header('admin'); 
 if (have_posts()) : while (have_posts()) : the_post(); ?>

<style type="text/css">
.box_filterReport{
    margin-bottom: 30px;
}
.sel_filterReport{
    width: 100%;
    height: 30px;
    line-height: 35px;
    font-size: 12px;
    color: #000;
    font-weight: 500;
    border-radius: 3px;
    border: 1px solid rgba(0,0,0,.2);
    padding-left: 10px;
    padding-right: 10px;
}
.txt_filterReport{
    width: 100%;
    height: 30px;
    line-height: 35px;
    font-size: 12px;
    color: #000;
    font-weight: 500;
    border-radius: 3px;
    border: 1px solid rgba(0,0,0,.2);
    padding-left: 10px;
    padding-right: 10px;
}
.sub_filterReport{
    background: #2a96a5;
    font-size: 12px;
    font-weight: 600;
    text-transform: uppercase;
    letter-spacing: .3px;
    color: #fff;
    width: auto;
    line-height: 30px;
    border: 0;
    border-radius: 3px;
    padding-left: 15px;
    padding-right: 15px;
    margin-top: 20px;
}
</style>

<?php 
date_default_timezone_set("Asia/Jakarta");
$nowdate = date('Y-m-d');

$current_user = wp_get_current_user();
$u_id = $current_user->ID;
$u_roles = $current_user->roles;
$admin = 0;

if(isset($u_roles) AND !empty($u_roles)){
  foreach ($u_roles as $key => $value) {
      if(strtolower($value) == 'administrator'){
          $admin = 1;
      }
  }
}

if((isset($u_id) OR $u_id != 0) AND $admin == 1){

if(isset($_GET['from_date'])){
  $from_date = $_GET['from_date'];
}else{
  $from_date = $nowdate;
}
if(isset($_GET['to_date'])){
  $to_date = $_GET['to_date'];
}else{
  $to_date = $nowdate;
}

if(isset($_GET['us_id']) AND $_GET['us_id'] != ''){
  $us_id = $_GET['us_id'];
  $query_user = "AND user_id = '$us_id'";
  $user_data = get_userdata( $us_id );
  $user_name = $user_data->data->display_name;
}else{
  $us_id = '';
  $query_user = " ";
  $user_name = 'Semua';
}

// $data_status = array();
// $data_status['wc-all'] = 'All';
// $data_status['wc-pending'] = 'Pending';
// $data_status['wc-on-hold'] = 'On Hold';
// $data_status['wc-processing'] = 'Processing';
// $data_status['wc-preparing'] = 'Preparing';
// $data_status['wc-ondelivery'] = 'On Delivery';
// $data_status['wc-completed'] = 'Completed';
// $data_status['wc-cancelled'] = 'Cancelled';



$status_arr = array();
$status_arr['-3'] = 'Refunded';
$status_arr['-2'] = 'Rejected';
$status_arr['-1'] = 'Cancelled';
$status_arr['0'] = 'Pending';
$status_arr['1'] = 'Paid';
$status_arr['2'] = 'Prepared';
$status_arr['3'] = 'On_Delivery';
$status_arr['4'] = 'Completed';

if(isset($_GET['so'])){
  $so_status = $_GET['so'];
  $tx_status = $status_arr[$so_status];
}else{
  $so_status = '';
  $tx_status = 'Semua Status';
}


?>

  <!-- Begin Page Content -->
  <div class="container-fluid">

    <!-- Page Heading -->
    <h1 class="h3 mb-2 text-gray-800">Report Order</h1>

    <!-- DataTales Example -->
    <div class="card shadow mb-4">
      <div class="card-header py-3">
        <h6 class="m-0 font-weight-bold text-primary">Order Status : <?php echo $tx_status; ?></h6>
      </div>
      <div class="card-body">

        <form class="form_filterReport" onsubmit="form_filterReport(event);" method="post" id="form_filterReport">
            <div class="row box_filterReport">

              <div class="col-md-2 col_filterReport">
                  <div class="label_filterReport">Pilih Status :</div>
                  <select name="sel_orderstatus" class="sel_filterReport">
                      <option value="">Semua Status</option>
                      <?php foreach ($status_arr as $key => $value) { 
                              if(strtolower($key) == strtolower($so_status)){
                              ?>
                                  <option value="<?php echo $key; ?>" selected="selected"><?php echo $value; ?></option>
                              <?php
                              }else{
                              ?>
                                  <option value="<?php echo $key; ?>"><?php echo $value; ?></option>
                              <?php
                              }
                            } 
                      ?>
                  </select>
              </div>

              <div class="col-md-2 col_filterReport">
                  <div class="label_filterReport">Dari Tanggal :</div>
                  <input type="text" name="from_date" class="txt_filterReport all_datepicker" value="<?php echo $from_date; ?>" readonly>
              </div>
              <div class="col-md-2 col_filterReport">
                  <div class="label_filterReport">Sampai Tanggal :</div>
                  <input type="text" name="to_date" class="txt_filterReport all_datepicker" value="<?php echo $to_date; ?>" readonly>
              </div>
              <div class="col-md-2 col_filterReport">
                  <input type="submit" class="sub_filterReport" value="Submit">
              </div>
            </div>
        </form>

        <div class="table-responsive">
          <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0" data-order="[[ 0, &quot;desc&quot; ]]">
            <thead>
              <tr>
                <th>Invoice</th>
                <th>Customer (user ID)</th>
                <th>Total</th>
                <th>Status</th>
                <th>Order Date</th>
                <th>Kurir</th>
              </tr>
            </thead>
            
            <tbody>
            <?php 
              $total_order = 0;
              $a = 0;
              $from_date = $from_date.' 00:00:00';
              $to_date = $to_date.' 23:59:59';

              global $wpdb;

              if(isset($so_status) AND $so_status != '' ){
                  $query = "SELECT *
                            FROM ldr_order_merchants
                            WHERE status = $so_status
                            AND created_at BETWEEN '$from_date' AND '$to_date'
                            ORDER BY id DESC ";
              }else{
                  $query = "SELECT *
                            FROM ldr_order_merchants
                            WHERE created_at BETWEEN '$from_date' AND '$to_date'
                            ORDER BY id DESC ";          
              }

              $res_query = $wpdb->get_results($query, OBJECT);
              $res_count = count($res_query);

              if ($res_count > 0){
                  foreach ($res_query as $key => $value) {
                    $no_in = $value->id;
                    $no_invoice = $value->no_invoice;
                    $order_id = $value->order_id;
                    $merchant_id = $value->merchant_id;
                    $shipper_courier_id = $value->shipper_courier_id; 

                    $courier_name = "";
                    $courier_rate_name = "";
                    if(isset($shipper_courier_id) AND $shipper_courier_id != ''){
                      $query3 = "SELECT *
                                FROM ldr_shipper_couriers
                                WHERE id = '$shipper_courier_id'
                                ORDER BY id DESC ";
                      $res_query3 = $wpdb->get_results($query3, OBJECT);
                      $res_count3 = count($res_query3);
                      if ($res_count3 > 0){
                        foreach ($res_query3 as $key3 => $value3){
                            $courier_name = $value3->name;
                            $courier_rate_name = $value3->rate_name;
                        }
                      }
                    }

                    $shipping_price = $value->shipping_price;
                    $insurance_price = $value->insurance_price;
                    $created_at = $value->created_at;
                    $status_id = $value->status;
                    $status_order = $status_arr[$status_id];


                  $query2 = "SELECT *
                            FROM ldr_orders
                            WHERE id = '$order_id'
                            ORDER BY id DESC ";
                  $res_query2 = $wpdb->get_results($query2, OBJECT);
                  $res_count2 = count($res_query2);

                  if ($res_count2 > 0){
                      foreach ($res_query2 as $key2 => $value2){
                        $user_id = $value2->user_id;
                        $user_data = get_userdata( $user_id );
                                $user_name = $user_data->data->display_name;
                        $subtotal_price = $value2->subtotal_price;
                        $discount_voucher_price = $value2->discount_voucher_price;
                        $xendit_fee = $value2->xendit_fee;
                      }
                    }else{
                        $user_id = '-';
                        $user_name = '';
                        $subtotal_price = 0;
                        $discount_voucher_price = 0;
                        $xendit_fee = 0;
                    }

                    ?>
                      <tr>
                        <td>
                          <a target="_blank" href="<?php echo home_url(); ?>/dashboard/finance/detail?id=<?php echo $no_in; ?>" title="Lihat detail transaksi">
                            <?php echo strtoupper($no_invoice); ?>
                          </a>
                        </td>
                        <td><?php echo $user_name.' ( '.$user_id.' )'; ?></td>
                        <td><?php echo number_format($subtotal_price); ?></td>

                        <?php if(strtolower($status_order) == 'completed'){ ?>
                            <td style="color: green;"><?php echo $status_order; ?></td>
                          <?php }else{ ?>
                            <td><?php echo strtolower($status_order); ?></td>
                        <?php } ?>
                        <td><?php echo $created_at; ?></td>
                        <td><?php echo $courier_name.' ( '.$courier_rate_name.' )' ?></td>
                      </tr>
                    <?php
                  } 
              }
            ?>
            </tbody>
          </table>
        </div>

      </div>
    </div>

  </div>
  <!-- /.container-fluid -->

<?php }else{ ?>
    <script>
        // 'Getting' data-attributes using getAttribute
        var plant = document.getElementById('body');
        console.log(plant);
        var hurl = plant.getAttribute('data-hurl'); 
        location.replace(hurl+'/login/'); 
    </script>
<?php } ?>


<?php endwhile; ?>
<?php else : ?>
    <?php get_template_part( 'content', '404pages' ); ?>  
<?php endif; ?>
<?php get_footer('admin'); ?>