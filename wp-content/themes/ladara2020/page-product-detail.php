<?php
/*
Template Name: product detail
*/
?>
<?php get_header(); ?>
<?php
if (have_posts()) : while (have_posts()) : the_post();
date_default_timezone_set("Asia/Jakarta");
$nowdate = date('Y-m-d');

$client = new GuzzleHttp\Client();
$slug = $_SERVER;
$current_user = wp_get_current_user();
$u_id = $current_user->ID;
$product = [];

$url1 = explode('?',$_SERVER['REQUEST_URI']);
$url = explode('/',$url1[0]);

// live
$slug = $url[2] ?? '';
// localhost
// $slug = $url[3] ?? '';

$idPost = '';
if ($slug != ''){
    $exSlug = explode('-', $slug);
    $idPost = end($exSlug);
}
$response = sendRequest('GET', MERCHANT_URL . '/api/product/detail/'.$idPost);
$product = $response->data;

if(isset($product) AND !empty($product)){

    $tab = isset($_GET['tab']) ? htmlspecialchars($_GET['tab']) : 'description';

    // for seo meta tags ====
    $pro_title = $product->name;
    $pro_desc = nl2br($product->description);
    $pro_img = '';
    $pro_price = $product->price;
    $pro_sku = $product->sku;
    $pro_brand = $product->brand->ID ?? 0;
    $pro_link = $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'];
    $pro_valid_date = date("Y-m-d", strtotime(($nowdate) . " +6 months"));
    $pro_category = array();

    $rate = isset($_GET['rate']) ? htmlspecialchars($_GET['rate']) : 0;
    $ulasan = getUlasan($idPost, $rate);

    $summary = $ulasan['summary']->data;
    $reviews = $ulasan['ulasan']->data->data;

    if(isset($summary) AND !empty($summary)){
        $total_rate = $summary->rating;
        $total_arr_rev = '';
    }else{
        $total_rate = 0;
        $total_arr_rev = '';
    }

    // for seo meta tags ====
    ?>
    <style type="text/css">
    @media only screen and (max-width:769px) {
        .row_fix_mobmenu {
            display: none !important;
        }
    }
    .ulasan-start{
        color: #FFC107;
        font-size: 22px;
        cursor: pointer;
    }
    .ulasan-start.abu{
        color: #E5E5E5;
    }
    .btn-rate{
        width: 140px;
        height: 50px;
        background: #FFFFFF;
        border-radius: 10px;
        margin: 0 5px 10px 0;
        color: #878787;
    }
    .btn-rate.active{
        border: 2px solid #0080FF;
        font-weight: bold;
        box-shadow: none;
    }
    .f-12{
        font-size: 12px;
    }
    .abu{
        color: #878787;
    }
    .mw-100{
        max-width: 100%;
    }
    </style>
    <article id="post-<?php echo $product->id ?? ''; ?>" <?php post_class('clearfix'); ?> role="article" itemscope itemtype="http://schema.org/BlogPosting">
        <div class="row row_singlePro">
            <div class="col-md-12 ht_proCategory">
                <div class="bx_shop_category">
                    <a href="<?php echo home_url(); ?>/product/"><span class="cate">Shop</span></a>
                    <?php if (isset($product->categories)) :?>
                        <?php foreach ($product->categories as $cate): ?>
                            <?php $pro_category[] = array('name'=>$cate->name, 'link'=>home_url().'/product?category='.$cate->slug.'-'.$cate->id); ?>
                            <span class="arrow"><img src="<?php bloginfo('template_directory'); ?>/library/images/next_shop.svg"></span>
                            <a href="<?php echo home_url(); ?>/product?category=<?php echo $cate->slug.'-'.$cate->id; ?>"><span class="cate"><?php echo $cate->name; ?></span></a>
                        <?php endforeach; ?>
                    <?php endif;?>
                    <span class="arrow"><img src="<?php bloginfo('template_directory'); ?>/library/images/next_shop.svg"></span>
                    <span class="cate"><?php echo $product->name; ?></span>
                </div>
            </div>
            <div class="col-md-12 col_singlePro">
                <div class="row row_top_singlePro">
                    <div class="col-md-5 col_left_singlePro">
                        <?php if ($product->wishlist):?>
                            <div id="btn_" class="visible-xs mg_heart removewishlist love_<?php echo $product->id; ?> act" title="Hapus dari wishlist" data-id="<?php echo $product->id; ?>" style="top: 5px;right: 25px;">
                                <span class="glyphicon glyphicon-heart"></span>
                            </div>
                        <?php else:?>
                            <div class="visible-xs mg_heart addWishlist love_<?php echo $product->id; ?>" title="Tambah ke wishlist" data-id="<?php echo $product->id; ?>" style="top: 5px;right: 25px;">
                                <span class="glyphicon glyphicon-heart"></span>
                            </div>
                        <?php endif;?>
                        <div id="slide_bigProduct" class="owl-carousel owl-theme popup-gallery">
                           <?php if (isset($product->images)):?>
                                <?php $pro_img = $product->images[0]->path; ?>
                                <?php foreach ($product->images as $image): ?>

                                   <div class="item" data-hash="photo-<?php echo $image->id; ?>">
                                       <a href="<?php echo $image->path; ?>" rel="<div class='tx_magnific'></div>" title="View Photo Gallery">
                                           <div class="mg_slider_product">
                                               <img itemprop="image" src="<?php echo $image->path; ?>"  title="klik untuk memperbesar foto.">
                                           </div>
                                       </a>
                                   </div>
                                <?php endforeach;?>
                           <?php endif;?>
                        </div>
                        <div class="box_bannerThumb">
                            <?php if (isset($product->images)):?>
                                <?php foreach ($product->images as $image): ?>
                                    <a href="#photo-<?php echo $image->id; ?>" title="klik untuk melihat foto.">
                                        <div class="mg_bannerThumb">
                                            <img style="max-width: 100%;" " src="<?php echo $image->path; ?>">
                                        </div>
                                    </a>
                                <?php endforeach;?>
                            <?php endif;?>
                        </div>
                    </div>
                    <div class="col-md-7 col_right_singlePro">
                        <?php if ($product->seller !== null) { ?>
                            <?php
                                $merchantId = $product->seller->id;
                                $requestMerchant = sendRequest('GET', MERCHANT_URL . '/api/merchant/' . $merchantId);
                                $queryMerchant = $requestMerchant->data;
                            ?>
                            <?php if ($queryMerchant->status < 0) : ?>
                                <div class="bx-closed-merchant">
                                    <div></div>
                                    <div>
                                        <img src="<?php bloginfo('template_directory'); ?>/library/images/icon-warning.svg" />
                                        <div>
                                            <h4>Toko Sedang Tutup</h4>
                                            <p>
                                                Toko buka kembali pada <?= ($queryMerchant->opened_at) ?> atau kunjungi 
                                                Pusat Bantuan Ladara jika ada transaksi yang tertunda.
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            <?php endif; ?>
                        <?php } ?>

                        <h1 class="title_singlePro" itemprop="headline"><?php echo $product->name ?? ''; ?></h1>
                        <div class="bx_rate_productDetail">
                            <span class="sp_brand">
                                <?php
                                if ($product->seller !== null) {
                                    ?>
                                    <a href="<?php echo $product->seller->url; ?>" title="<?php echo $product->seller->name; ?>" style="text-decoration: none;">
                                        <?php echo $product->seller->name; ?>
                                    </a>
                                    <?php
                                }
                                ?>
                            </span>
                        </div>
                        <div id="product_stop" class="row row_procont">
                            <div class="col-md-3 col_procont_r0">
                                <div class="xt_sub_product">Harga</div>
                            </div>
                            <div class="col-md-9 col_procont_r1">
                                <div class="bx_pro_price">
                                    <?php if (isset($product->sale_price) and $product->sale_price != 0) { ?>
                                        <div class="xt_price_product">
                                            <span class="pro_finalPrice"><span>Rp</span> <?php echo number_format($product->sale_price); ?></span>
                                        </div>
                                        <?php if (intval($product->price) !== intval($product->sale_price)) : ?>
                                            <div class="xt_price_product">
                                                <span class="pro_basePrice"><i>Rp <?php echo number_format($product->price); ?></i></span>
                                                <span class="sale_off">Hemat <?php echo $product->discount->text; ?></span>
                                            </div>
                                        <?php endif; ?>
                                    <?php } else { ?>
                                        <div class="xt_price_product">
                                            <span class="pro_finalPrice"><span>Rp</span> <?php echo number_format($product->price); ?></span>
                                            <span class="sale_off"><span><i class="fas fa-star star"></i></span> Termurah</span>
                                        </div>
                                    <?php } ?>
                                </div>
                            </div>
                        </div>
                        <form id="form_addtoCart" class="form_addtoCart" action="" method="get" onsubmit="submit_addtoCart(event);">
                            <input type="hidden" name="user_id" value="<?php echo $u_id; ?>">
                            <input type="hidden" name="pro_id" value="<?php echo $product->id; ?>">
                            <input type="hidden" name="slr_id" value="<?php echo $product->seller->id ?? 0; ?>">

                            <div class="row row_procont product_varit animated">
                                <div class="col-md-3 col_procont_r0">
                                    <div class="xt_sub_product">Variasi</div>
                                </div>
                                <div class="col-md-9 col_procont_r1">
                                    <?php // ================ pilih warna =============
                                    global $wpdb;
                                    $ab = 0;
                                    $ready = 0;
                                    $wpdb_query = "SELECT DISTINCT warna_value FROM ldr_stock WHERE product_id = $product->id ";
                                    $res_query = $wpdb->get_results($wpdb_query, OBJECT);
                                    $count_res = count($res_query);
                                    if ($count_res > 0) {
                                        $ready = 1; ?>
                                        <div class="bx_in_variation">
                                            <div class="ht_variation">Pilih warna:</div>
                                            <div class="bx_rad_variation">
                                                <?php
                                                foreach ($res_query as $key => $value) {
                                                    $ab++;
                                                    $warna_value = $value->warna_value; ?>
                                                    <input type="radio" id="rad_<?php echo $ab; ?>" name="warna" value="<?php echo strtolower($warna_value); ?>" class="rad_var">
                                                    <label class="label_var" for="rad_<?php echo $ab; ?>"><?php echo ucfirst($warna_value); ?></label>
                                                    <?php
                                                } ?>
                                            </div>
                                        </div>
                                        <?php
                                    }
                                    ?>
                                    <?php // ================ pilih ukuran =============
                                    global $wpdb;
                                    $wpdb_query = "SELECT DISTINCT ukuran_value FROM ldr_stock WHERE product_id = $product->id ";
                                    $res_query = $wpdb->get_results($wpdb_query, OBJECT);
                                    $count_res = count($res_query);
                                    if ($count_res > 0) {
                                        $detect_value = $res_query[0]->ukuran_value;
                                        if (isset($detect_value) and $detect_value != '') {
                                            $ready = 1; ?>
                                            <div class="bx_in_variation">
                                                <div class="ht_variation">Pilih Ukuran:</div>
                                                <div class="bx_rad_variation">
                                                    <?php
                                                    foreach ($res_query as $key => $value) {
                                                        $ab++;
                                                        $ukuran_value = $value->ukuran_value; ?>
                                                        <input type="radio" id="rad_<?php echo $ab; ?>" name="ukuran" value="<?php echo strtolower($ukuran_value); ?>" class="rad_var">
                                                        <label class="label_var1" for="rad_<?php echo $ab; ?>"><?php echo ucfirst($ukuran_value); ?></label>
                                                        <?php
                                                    } ?>
                                                </div>
                                            </div>
                                            <?php
                                        }
                                    }
                                    ?>
                                    <?php // ================ pilih nomor =============
                                    global $wpdb;
                                    $wpdb_query = "SELECT DISTINCT nomor_value FROM ldr_stock WHERE product_id = $product->id ";
                                    $res_query = $wpdb->get_results($wpdb_query, OBJECT);
                                    $count_res = count($res_query);
                                    if ($count_res > 0) {
                                        $detect_value = $res_query[0]->nomor_value;
                                        if (isset($detect_value) and $detect_value != '') {
                                            $ready = 1; ?>
                                            <div class="bx_in_variation">
                                                <div class="ht_variation">Pilih Nomor:</div>
                                                <div class="bx_rad_variation">
                                                    <?php
                                                    foreach ($res_query as $key => $value) {
                                                        $ab++;
                                                        $nomor_value = $value->nomor_value; ?>
                                                        <input type="radio" id="rad_<?php echo $ab; ?>" name="nomor" value="<?php echo strtolower($nomor_value); ?>" class="rad_var">
                                                        <label class="label_var2" for="rad_<?php echo $ab; ?>"><?php echo ucfirst($nomor_value); ?></label>
                                                        <?php
                                                    } ?>
                                                </div>
                                            </div>
                                            <?php
                                        }
                                    }
                                    ?>
                                    <div class="err_msg_variation">Maaf, silahkan pilih variasi produk</div>
                                </div>
                            </div>
                            <?php if ($product->seller->user_id != $u_id):?>
                            <div id="row_putQty" class="row row_procont">
                                <div class="col-md-3 col_procont_r0">
                                    <div class="xt_sub_product">Jumlah</div>
                                </div>
                                <div class="col-md-9 col_procont_r1">

                                    <div class="bx_ch_qty">
                                        <span class="sp_minus_qty">-</span>
                                        <input type="number" name="qty" value="1" class="txt_putqty" min="1" max="100" required="required" readonly="readonly">
                                        <span class="sp_plus_qty act">+</span>
                                        <span class="v_checkqty "></span>
                                    </div>
                                </div>
                            </div>
                            <div class="row row_buyProduct">
                                <div class="col-md-12 col_procont_r0">
                                    <?php if ($ready == 0) { ?>
                                        <style type="text/css">
                                            .product_varit,
                                            #row_putQty {
                                                display: none !important;
                                            }
                                        </style>
                                        <div class="bx_err_msg_addtocart">
                                            <div class="alert alert-danger" role="alert" style="font-weight: 500;">
                                                <B>Maaf,</B> seluruh stok produk ini habis.
                                            </div>
                                            <div class="err_msg_addtocart" style="display: block;"></div>
                                        </div>
                                    <?php } else { // if stock variation ready
                                        ?>
                                        <div class="bx_err_msg_addtocart">
                                            <div class="err_msg_addtocart">Maaf, stok untuk variasi di atas tidak tersedia.</div>
                                            <div class="sucs_msg_addtocart">Sukses menambahkan produk ini ke keranjang belanja Anda.</div>
                                        </div>
                                        <div class="bx_submit_addtocart row_fix_addtocart">
                                            <span>
                                                <input type="submit" data-sub="1" class="sub_buyProduct sub_addtoCart" value="Beli Sekarang">
                                            </span>
                                            <span id="addtocart_detail" class="bx_iconcart"><img src="<?php bloginfo('template_directory'); ?>/library/images/cart_black.svg"></span>
                                            <?php if (isset($u_id) and $u_id != 0) {
                                                global $wpdb;
                                                $wpdb_query = "SELECT * FROM ldr_wishlist WHERE user_id = '$u_id' AND product_id = '$product->id' ";
                                                $res_query = $wpdb->get_results($wpdb_query, OBJECT);
                                                $count_res = count($res_query);
                                                if ($count_res == 0) {
                                                    $love_1 = 'act';
                                                    $love_2 = ''; ?>
                                                    <span id="addWishlist" class="bx_iconlove <?php echo $love_1; ?>" title="Tambah ke wishlist"><img src="<?php bloginfo('template_directory'); ?>/library/images/heart_2.svg"> tambah ke wishlist</span>
                                                    <span id="removewishlist" class="bx_iconlove <?php echo $love_2; ?>" title="Hapus dari wishlist"><img src="<?php bloginfo('template_directory'); ?>/library/images/heart_1.svg"></span>
                                                    <?php
                                                } else {
                                                    $love_2 = 'acts'; ?>
                                                    <span id="removewishlist" class="bx_iconlove <?php echo $love_2; ?> hidden-xs" title="Hapus dari wishlist"><img src="<?php bloginfo('template_directory'); ?>/library/images/heart_1.svg"> sudah di wishlist</span>
                                                    <?php
                                                }
                                            }
                                            ?>
                                        </div>
                                    <?php } ?>
                                </div>
                            </div>
                            <?php endif;?>
                        </form>
                    </div>
                    <div class="col-md-12 col_singleProTabs">
                        <div class="row row_singleProTabs">
                            <ul class="proTabs">
                                <li class="<?= ($tab === 'description' ? 'act' : '') ?>" data-id="tabs1"><a href="javascript:;">Deskripsi</a></li>
                                <li data-id="tabs2"><a href="javascript:;">Spesifikasi Lengkap</a></li>
                                <?php if (isset($u_id) and $u_id != 0) { ?>
                                    <li class="<?= ($tab === 'discussions' ? 'act' : '') ?>" data-id="tabs3"><a href="javascript:;">Diskusi</a></li>
                                <?php } ?>
                            </ul>
                        </div>
                        <div class="row row_singleProContent">
                            <?php // ============= TABS 1 ============
                            ?>
                            <div id="tabs1" class="col-md-12 box_detailProduct animated fadeIn <?= ($tab === 'description' ? 'act' : '') ?>">
                                <h3 class="ht_ProTabs">Deskripsi Produk</h3>
                                <div class="ht_nameProTabs"><?php echo $product->name; ?></div>
                                <div class="box_tabsinfo2">
                                    <?php echo nl2br($product->description); ?>
                                </div>
                            </div>
                            <?php // ============= TABS 2 ============
                            ?>
                            <div id="tabs2" class="col-md-12 box_detailProduct  animated fadeIn">
                                <h3 class="ht_ProTabs">Spesifikasi Produk</h3>
                                <div class="ht_nameProTabs"><?php echo $product->name; ?></div>
                                <div class="row row_specsTabs">
                                    <?php if (isset($product->brand->ID)) { ?>
                                        <div class="bx_specsTabs">
                                            <span class="ht_specsTabs">Brand</span>
                                            <span class="tx_specsTabs"><?php echo $product->brand->name; ?></span>
                                        </div>
                                    <?php } ?>
                                    <?php if (isset($product->bahan)) { ?>
                                        <div class="bx_specsTabs">
                                            <span class="ht_specsTabs">Bahan</span>
                                            <span class="tx_specsTabs"><?php echo $product->bahan; ?></span>
                                        </div>
                                    <?php } ?>
                                    <?php if (isset($product->ukuran)) { ?>
                                        <div class="bx_specsTabs">
                                            <span class="ht_specsTabs">Ukuran</span>
                                            <span class="tx_specsTabs"><?php echo $product->ukuran; ?></span>
                                        </div>
                                    <?php } ?>
                                    <?php if (isset($product->weight)) { ?>
                                        <div class="bx_specsTabs">
                                            <span class="ht_specsTabs">Berat</span>
                                            <span class="tx_specsTabs"><?php echo $product->weight; ?> gram</span>
                                        </div>
                                    <?php } ?>
                                    <?php if (isset($product->sku)) { ?>
                                        <div class="bx_specsTabs">
                                            <span class="ht_specsTabs">SKU</span>
                                            <span class="tx_specsTabs"><?php echo $product->sku; ?></span>
                                        </div>
                                    <?php } ?>
                                </div>
                            </div>
                            <?php // ============= TABS 3 ============
                            ?>
                            <?php if (isset($u_id) and $u_id != 0) { ?>
                            <div id="tabs3" class="col-md-12 box_detailProduct animated fadeIn <?= ($tab === 'discussions' ? 'act' : '') ?>">
                                <?php
                                global $wpdb;
                                $wpdb_query = "SELECT * FROM eod_comments WHERE product_id = '$product->id' AND review_type = '1' AND reply_id = '0' AND user_id = '$u_id' ";
                                $res_query_main = $wpdb->get_results($wpdb_query, OBJECT);
                                $count_res_main = count($res_query_main);

                                ?>
                                <div class="box_allulasan">
                                    <h3 class="ht_ProTabs">Diskusi Produk</h3>
                                    <div class="ht_nameProTabs"><?php echo $product->name ?? ''; ?></div>
                                    <?php if ($count_res_main == 0) { ?>
                                        <div class="row row_diskusi">
                                            <div class="col-md-6 col_diskusi">
                                                <form class="form_diskusi" data-id="0">
                                                    <input type="hidden" name="pro_id" value="<?php echo $product->id; ?>">
                                                    <div class="f_diskusi">
                                                        <textarea name="diskusi_com" class="area_diskusi" placeholder="Tulis pesan..." minlength="10" maxlength="300" required="required"></textarea>
                                                        <label class="lab_error">Maksimal 300 karakter.</label>
                                                        <label class="lab_success"></label>
                                                    </div>
                                                    <input type="button" class="sub_diskusi" value="Kirim" onclick="submit_diskusi(0);">
                                                </form>
                                            </div>
                                        </div>
                                    <?php } ?>
                                </div>
                                <div class="row box_content_ulasan">
                                    <?php
                                    global $wpdb;

                                    $currentUserAvatar = get_field('user_avatar', 'user_' . $u_id);
                                    if ($currentUserAvatar !== '') {
                                        $linkpoto = wp_get_attachment_image_src($currentUserAvatar, 'thumbnail');
                                        $currentUrlProfilePhoto = $linkpoto[0];
                                    } else {
                                        $currentUrlProfilePhoto = get_template_directory_uri() . '/library/images/icon_profile.png';
                                    }

                                    if ($tab === 'discussions') {
                                        $page = isset($_GET['page_number']) ? htmlspecialchars($_GET['page_number']) : 1;
                                    } else {
                                        $page = 1;
                                    }

                                    $discussions = sendRequest('GET', MERCHANT_URL . '/api/discussions/get-product-discussions/' . $product->id . '?page=' . $page);

                                    foreach ($discussions->data->data as $discussion) {
                                        $queryDicussionDetail = $wpdb->get_results("SELECT * FROM ldr_discussion_details WHERE deleted_at IS NULL AND discussion_id = " . $discussion->id); ?>

                                        <div class="col-md-12 col_userDiskusi">

                                            <?php
                                            foreach ($queryDicussionDetail as $key => $discussionDetail) {
                                            $postDate = date('d/m/Y H:i', strtotime($discussionDetail->created_at));
                                            $canDeleted = false;

                                            if ($discussionDetail->discussionner_type === 'App\User') {
                                                $userDiscussion = get_userdata($discussionDetail->discussionner_id);
                                                $shortName = substr($userDiscussion->display_name ?? '', 0, -3) . '*****';
                                                $userAvatar = get_field('user_avatar', 'user_' . $discussionDetail->discussionner_id);

                                                if ($userAvatar !== '' && $userAvatar !== null) {
                                                    $linkpoto = wp_get_attachment_image_src($userAvatar, 'thumbnail');
                                                    $urlProfilePhoto = $linkpoto[0];
                                                } else {
                                                    $urlProfilePhoto = get_template_directory_uri() . '/library/images/icon_profile.png';
                                                }

                                                if ($discussionDetail->discussionner_id == $u_id) {
                                                    $canDeleted = true;
                                                }
                                            } else {
                                                $userDiscussion = $wpdb->get_row("SELECT * FROM ldr_merchants WHERE id = " . $discussionDetail->discussionner_id);
                                                $shortName = $userDiscussion->name;

                                                $urlProfilePhoto = home_url('/') . $userDiscussion->picture;
                                            }

                                            if ($key === 0) :
                                            ?>
                                            <div class="box_left_diskusi">
                                                <div class="mg_kprofile">
                                                    <img src="<?php echo $urlProfilePhoto; ?>">
                                                </div>
                                            </div>
                                            <div class="box_right_diskusi">
                                                <div class="d-flex">
                                                    <div class="comment_disuser" style="width: 95%;">
                                                        <?php echo $discussionDetail->content; ?>
                                                    </div>
                                                    <?php if ($canDeleted) : ?>
                                                        <div class="del_itemCart" title="Hapus diskusi ini" data-id="<?= $discussionDetail->id ?>" style="width: 5%; height: 3rem;" onclick="delete_diskusi(<?= $discussionDetail->id ?>)">
                                                            <span><i class="fas fa-trash"></i></span>
                                                        </div>
                                                    <?php endif; ?>
                                                </div>
                                                <div class="ht_kprofile"><?php echo $shortName . ' . <span>' . $postDate . '</span>'; ?></div>
                                                <?php else : ?>
                                                    <div class="row row_admDisReply">
                                                        <div class="bx_left_repdis">
                                                            <div class="mg_kprofile">
                                                                <img src="<?php echo $urlProfilePhoto; ?>">
                                                            </div>
                                                        </div>
                                                        <div class="bx_right_repdis">
                                                            <div class="d-flex">
                                                                <div class="repdis_com" style="width: 93%;"><?php echo $discussionDetail->content; ?></div>
                                                                <?php if ($canDeleted) : ?>
                                                                    <div class="del_itemCart" title="Hapus diskusi ini" data-id="<?= $discussionDetail->id ?>" style="width: 4rem; height: 3rem;" onclick="delete_diskusi(<?= $discussionDetail->id ?>)">
                                                                        <span><i class="fas fa-trash"></i></span>
                                                                    </div>
                                                                <?php endif; ?>
                                                            </div>
                                                            <span class="repdis_prof"><?php echo $shortName . ' . <span>' . $postDate . '</span>'; ?></span>
                                                        </div>
                                                    </div>
                                                <?php
                                                endif;
                                                }
                                                ?>
                                                <div class="row row_diskusi">
                                                    <div class="col-md-2">
                                                        <div class="mg_kprofile" style="float: none; margin-left: 45px;">
                                                            <img src="<?php echo $currentUrlProfilePhoto; ?>">
                                                        </div>
                                                    </div>
                                                    <div class="col-md-10 col_diskusi">
                                                        <form class="form_diskusi" data-id="<?php echo $discussion->id ?>">
                                                            <input type="hidden" name="pro_id" value="<?php echo $product->id; ?>">
                                                            <div class="f_diskusi">
                                                                <textarea name="diskusi_com" class="area_diskusi" placeholder="Tulis pesan..." minlength="10" maxlength="300" required="required" rows="2"></textarea>
                                                                <label class="lab_error">Maksimal 300 karakter.</label>
                                                                <label class="lab_success"></label>
                                                            </div>
                                                            <input type="button" class="sub_diskusi" value="Kirim" onclick="submit_diskusi(<?php echo $discussion->id ?>);">
                                                        </form>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <?php
                                    }
                                    }
                                    ?>

                                    <div align="center">
                                        <ul class="pagination">
                                            <?php
                                            for ($i = 1; $i <= $discussions->data->last_page; $i++) {
                                                $url = parse_url($_SERVER['REQUEST_URI'], PHP_URL_PATH) . '?tab=discussions&page_number=' . $i;
                                            ?>
                                                <li class="<?= ($i === $discussions->data->current_page ? 'active' : '') ?>">
                                                    <a href="<?= ($i === $discussions->data->current_page ? '#' : $url) ?>"><?= $i ?></a>
                                                </li>
                                            <?php
                                            }
                                            ?>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
<!--                <div class="row row_relatedProduct">-->
<!--                    <div class="col-md-12">-->
<!--                        <h3>Ulasan Produk</h3>-->
<!--                    </div>-->
<!--                    <div class="col-md-12">-->
<!--                        <div style="background: #F5F5F5 0% 0% no-repeat padding-box;border-radius: 10px;padding: 20px;">-->
<!--                            <div class="row">-->
<!--                                <div class="col-md-2">-->
<!--                                    <span style="font-size: 30px;color: #313131;font-weight: bold;">--><?php //echo $summary->rating ?? 0 ?><!--</span><span>/5</span>-->
<!--                                    <div>-->
<!--                                        --><?php //for ($i=1;$i<=5;$i++):?>
<!--                                            --><?php //if($i <= round($summary->rating)):?>
<!--                                                <span class="glyphicon glyphicon-star ulasan-start"></span>-->
<!--                                            --><?php //else: ?>
<!--                                                <span class="glyphicon glyphicon-star ulasan-start abu"></span>-->
<!--                                            --><?php //endif;?>
<!--                                        --><?php //endfor;?>
<!--                                    </div>-->
<!--                                    <div>-->
<!--                                        <span>(--><?php //echo $summary->total_review ?? 0 ?><!-- ulasan)</span>-->
<!--                                    </div>-->
<!--                                </div>-->
<!--                                <div class="col-md-10">-->
<!--                                    <div>-->
<!--                                        <a class="btn btn-rate --><?php //echo $rate == '' ? 'active' : '' ?><!--" href="--><?php //echo '/product/'.$slug;?><!--">-->
<!--                                            <span class="d-block">Semua</span><br/>-->
<!--                                            <span class="d-block abu">(--><?php //echo $summary->total_review ?? 0 ?><!--)</span>-->
<!--                                        </a>-->
<!--                                        <a class="btn btn-rate --><?php //echo $rate == 6 ? 'active' : '' ?><!--" href="--><?php //echo '/product/'.$slug.'?rate=6';?><!--">-->
<!--                                            <span class="d-block">dengan Foto</span><br/>-->
<!--                                            <span class="d-block abu">(--><?php //echo $summary->with_image ?? 0 ?><!--)</span>-->
<!--                                        </a>-->
<!--                                        <a class="btn btn-rate --><?php //echo $rate == 5 ? 'active' : '' ?><!--" href="--><?php //echo '/product/'.$slug.'?rate=5';?><!--">-->
<!--                                            <span class="glyphicon glyphicon-star ulasan-start f-12"></span>-->
<!--                                            <span class="glyphicon glyphicon-star ulasan-start f-12"></span>-->
<!--                                            <span class="glyphicon glyphicon-star ulasan-start f-12"></span>-->
<!--                                            <span class="glyphicon glyphicon-star ulasan-start f-12"></span>-->
<!--                                            <span class="glyphicon glyphicon-star ulasan-start f-12"></span>-->
<!--                                            <br/>-->
<!--                                            <span class="d-block abu">(--><?php //echo $summary->detail->{'5'} ?? 0 ?><!--)</span>-->
<!--                                        </a>-->
<!--                                        <a class="btn btn-rate --><?php //echo $rate == 4 ? 'active' : '' ?><!--" href="--><?php //echo '/product/'.$slug.'?rate=4';?><!--">-->
<!--                                            <span class="glyphicon glyphicon-star ulasan-start f-12"></span>-->
<!--                                            <span class="glyphicon glyphicon-star ulasan-start f-12"></span>-->
<!--                                            <span class="glyphicon glyphicon-star ulasan-start f-12"></span>-->
<!--                                            <span class="glyphicon glyphicon-star ulasan-start f-12"></span>-->
<!--                                            <span class="glyphicon glyphicon-star ulasan-start abu f-12"></span>-->
<!--                                            <br/>-->
<!--                                            <span class="d-block abu">(--><?php //echo $summary->detail->{'4'} ?? 0 ?><!--)</span>-->
<!--                                        </a>-->
<!--                                        <a class="btn btn-rate --><?php //echo $rate == 3 ? 'active' : '' ?><!--" href="--><?php //echo '/product/'.$slug.'?rate=3';?><!--">-->
<!--                                            <span class="glyphicon glyphicon-star ulasan-start f-12"></span>-->
<!--                                            <span class="glyphicon glyphicon-star ulasan-start f-12"></span>-->
<!--                                            <span class="glyphicon glyphicon-star ulasan-start f-12"></span>-->
<!--                                            <span class="glyphicon glyphicon-star ulasan-start abu f-12"></span>-->
<!--                                            <span class="glyphicon glyphicon-star ulasan-start abu f-12"></span>-->
<!--                                            <br/>-->
<!--                                            <span class="d-block abu">(--><?php //echo $summary->detail->{'3'} ?? 0 ?><!--)</span>-->
<!--                                        </a>-->
<!--                                        <a class="btn btn-rate --><?php //echo $rate == 2 ? 'active' : '' ?><!--" href="--><?php //echo '/product/'.$slug.'?rate=2';?><!--">-->
<!--                                            <span class="glyphicon glyphicon-star ulasan-start f-12"></span>-->
<!--                                            <span class="glyphicon glyphicon-star ulasan-start f-12"></span>-->
<!--                                            <span class="glyphicon glyphicon-star ulasan-start abu f-12"></span>-->
<!--                                            <span class="glyphicon glyphicon-star ulasan-start abu f-12"></span>-->
<!--                                            <span class="glyphicon glyphicon-star ulasan-start abu f-12"></span>-->
<!--                                            <br/>-->
<!--                                            <span class="d-block abu">(--><?php //echo $summary->detail->{'2'} ?? 0 ?><!--)</span>-->
<!--                                        </a>-->
<!--                                        <a class="btn btn-rate --><?php //echo $rate == 1 ? 'active' : '' ?><!--" href="--><?php //echo '/product/'.$slug.'?rate=1';?><!--">-->
<!--                                            <span class="glyphicon glyphicon-star ulasan-start f-12"></span>-->
<!--                                            <span class="glyphicon glyphicon-star ulasan-start abu f-12"></span>-->
<!--                                            <span class="glyphicon glyphicon-star ulasan-start abu f-12"></span>-->
<!--                                            <span class="glyphicon glyphicon-star ulasan-start abu f-12"></span>-->
<!--                                            <span class="glyphicon glyphicon-star ulasan-start abu f-12"></span>-->
<!--                                            <br/>-->
<!--                                            <span class="d-block abu">(--><?php //echo $summary->detail->{'1'} ?? 0 ?><!--)</span>-->
<!--                                        </a>-->
<!--                                    </div>-->
<!--                                </div>-->
<!--                            </div>-->
<!--                        </div>-->
<!--                    </div>-->
<!--                    <div class="col-md-12" style="margin-top: 20px;">-->
<!--                        --><?php //foreach ($reviews as $review):?>
<!--                        <div class="row" style="border-bottom: 1px solid #A7A7A7;margin-bottom: 30px;padding-bottom: 30px;">-->
<!--                            <div class="col-md-1">-->
<!--                                <img style="width: 100%;border-radius: 50%;border: 1px solid #eeeeee;" src="https://ladara.id/wp-content/themes/ladara2020/library/images/icon_profile.png" alt="">-->
<!--                            </div>-->
<!--                            <div class="col-md-10">-->
<!--                                <span>--><?php //echo $review->user->display_name?><!--</span>-->
<!--                                <div>-->
<!--                                    --><?php //for ($i=1;$i<=5;$i++):?>
<!--                                        --><?php //if($i <= round($review->rating)):?>
<!--                                            <span class="glyphicon glyphicon-star ulasan-start f-12"></span>-->
<!--                                        --><?php //else: ?>
<!--                                            <span class="glyphicon glyphicon-star ulasan-start abu f-12"></span>-->
<!--                                        --><?php //endif;?>
<!--                                    --><?php //endfor;?>
<!--                                </div>-->
<!--                                <span class="abu">--><?php //echo $review->created_at?><!--</span>-->
<!--                                <p>-->
<!--                                    --><?php //echo $review->review?>
<!--                                </p>-->
<!--                                --><?php //if (count($review->images) > 0):?>
<!--                                <div style="display: flex;">-->
<!--                                    --><?php //foreach ($review->images as $image):?>
<!--                                    <div style="width: 10%;margin: 5px;">-->
<!--                                        <img class="mw-100" src="--><?php //echo $image?><!--" alt="">-->
<!--                                    </div>-->
<!--                                    --><?php //endforeach;?>
<!--                                </div>-->
<!--                                --><?php //endif;?>
<!--                            </div>-->
<!--                        </div>-->
<!--                        --><?php //endforeach;?>
<!--                    </div>-->
<!--                </div>-->
                <?php // ================================== RELATED PRODUCT =============================
                ?>
                <div class="row row_relatedProduct">
                    <h3 class="ht_sub_productdetail">Rekomendasi Produk</h3>
                    <?php
                    $client = new GuzzleHttp\Client();
                    try {
                        $params = [
                            'limit' => 8,
                        ];
                        $req = $client->request('GET', MERCHANT_URL . '/api/product/search-products', [
                            'query' => $params
                        ]);
                        $response = json_decode($req->getBody());
                    } catch (\GuzzleHttp\Exception\GuzzleException $e) {
                        $response = [];
                    }
                    ?>

                    <div id="slide_relatedProduct" class="owl-carousel owl-theme">
                        <?php foreach ($response->data->products as $product):?>
                        <div class="item col_bx_productList">
                            <div class="bx_productList">
                                <a href="<?php echo $product->permalink.'-'.$product->id; ?>" title="Lihat <?php echo $product->name; ?>">
                                    <?php if ($product->wishlist):?>
                                        <div id="btn_" class="mg_heart removewishlist love_<?php echo $product->id; ?> act" title="Hapus dari wishlist" data-id="<?php echo $product->id; ?>">
                                            <span class="glyphicon glyphicon-heart"></span>
                                        </div>
                                    <?php else: ?>
                                        <div class="mg_heart addWishlist love_<?php echo $product->id; ?>" title="Tambah ke wishlist" data-id="<?php echo $product->id; ?>">
                                            <span class="glyphicon glyphicon-heart"></span>
                                        </div>
                                    <?php endif;?>
                                    <div class="mg_productList">
                                        <img class="" src="<?php echo $product->images; ?>" alt="<?php echo $product->name; ?>">
                                    </div>
                                    <div class="bx_in_productList">
                                        <h4 class="ht_productList"><?php echo $product->name; ?></h4>
                                        <?php if ($product->sale_price): ?>
                                            <div class="tx_price">Rp <?php echo number_format($product->sale_price); ?></div>
                                            <div class="tx_price_sale">Rp <?php echo number_format($product->price); ?></div>
                                        <?php  else:  ?>
                                            <div class="tx_price">Rp <?php echo number_format($product->price); ?></div>
                                        <?php endif; ?>
                                        <div class="bx_rate_product">
                                        </div>
                                    </div>
                            </div>
                            </a>
                        </div>
                        <?php endforeach;?>
                    </div>
                </div>
            </div>
            <div class="col-md-12 col_kontenPro">
            </div>
        </div>
        <?php // ============= Pop up Gallery ==================
        ?>
        <?php insertPostViewed($product->id); ?>
        <?php // =============================== POP UP SUCESS ======================
        ?>
        <?php get_template_part('content', 'pop-addcart'); ?>
        <?php // =============================== POP UP SUCESS ======================
        ?>
    </article>

    <?php 
    $pro_desc = strip_tags($pro_desc);
    $pro_desc = str_replace('"', '', $pro_desc);
    $pro_rate = rand ( 4,5 );
    $pro_review = rand ( 10,300 );
    ?>
    <script type="application/ld+json">
    {
      "@context": "https://schema.org",
      "@type": "Product",
      "aggregateRating": {
        "@type": "AggregateRating",
          "ratingValue": "<?php echo $pro_rate; ?>",
          "reviewCount": "<?php echo $pro_review; ?>"
      },
      "description": "<?php echo $pro_desc; ?>",
      "name": "<?php echo $pro_title; ?>",
      "image": "<?php echo $pro_img; ?>",
      "sku": "<?php echo $pro_sku; ?>",
      "brand": "<?php echo $pro_brand; ?>",
      "offers": {
        "@type": "Offer",
        "availability": "https://schema.org/InStock",
        "price": "<?php echo $pro_price; ?>",
        "priceCurrency": "IDR",
        "url": "<?php echo $pro_link; ?>",
        "priceValidUntil": "<?php echo $pro_valid_date; ?>"
      }
    }
    </script>
    <script type="application/ld+json">
    {
      "@context": "http://schema.org/", 
      "@type": "BreadcrumbList", 
      "itemListElement": [
        { 
          "@type": "ListItem", 
          "position": "1", 
          "item": { 
            "@id": "<?php echo home_url().'/product/'; ?>", 
            "name": "Shop" 
          } 
        },
        { 
          "@type": "ListItem", 
          "position": "2", 
          "item": { 
            "@id": "<?php echo $pro_category[0]['link']; ?>", 
            "name": "<?php echo $pro_category[0]['name']; ?>" 
          } 
        },
        { 
          "@type": "ListItem", 
          "position": "3", 
          "item": { 
            "@id": "<?php echo $pro_link; ?>", 
            "name": "<?php echo $pro_title; ?>" 
          } 
        }
      ]
    }
    </script>

<?php   
}else{
?>
    <script>
        // 'Getting' data-attributes using getAttribute
        var plant = document.getElementById('body');
        console.log(plant);
        var hurl = plant.getAttribute('data-hurl'); 
        location.replace(hurl); 
    </script>
<?php
}
?>

<?php endwhile; ?>
<?php else : ?>
    <article id="post-not-found" class="hentry clearfix">
        <header class="article-header">
            <h1><?php _e('Oops, Post Not Found!', 'bonestheme'); ?></h1>
        </header>
        <section class="entry-content">
            <p><?php _e('Uh Oh. Something is missing. Try double checking things.', 'bonestheme'); ?></p>
        </section>
        <footer class="article-footer">
            <p><?php _e('This is the error message in the single.php template.', 'bonestheme'); ?></p>
        </footer>
    </article>
<?php endif; ?>
<?php get_footer(); ?>