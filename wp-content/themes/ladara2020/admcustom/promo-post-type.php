<?php // JANGAN LUPA CEK FUNCTION.PHP INCLUDE INI FILE YAK....!!!


add_action("admin_init", "detailpromo_add_meta");
function detailpromo_add_meta(){
	add_meta_box("detailpromo-meta", "Promo Products", "detailpromo_meta_options", "promo", "normal", "high");
}


function detailpromo_meta_options(){
	global $post;
	if( defined('DOING_AUTOSAVE') && DOING_AUTOSAVE ){
		return $post_id;
	}

	// UNTUK GET DATA DARI DATABASE wordpress, kalo ambil dari database ga usa di pake.
	// ini seperti 
	
	$custom = get_post_custom($post->ID);
	$produk_pilihan = $custom["produk_pilihan"][0];

	// UNTUK GET DATA DARI DATABASE wordpress, kalo ambil dari database ga usa di pake.


	?>

	<?php // UNTUK TAMPILANNYA ?>
	<style type="text/css">
	<?php include('product-post-type.css'); ?>
	</style>
    <div class="product-extras meta-box-wrapper">

		<?php // teknik / tips buat ambil ID current page
			$productId = $post->ID; // kalo promo, berarti ini promo ID
            $productSelected = get_field('produk_pilihan', $post->ID);
            $resp = sendRequest('GET', MERCHANT_URL.'/api/product/search-products',[
                    'id' => json_encode($productSelected),
                    'limit' => 10000
            ]);
            $products = $resp->data->products ?? [];
		?>

		<div class="row">
			<?php // contoh di product kan select dari database tuh, jadi tinggal ngikutin aja kalo perlu ?>

			<h4>Pilih Produk</h4>
            <select id="promo_list_product" name="list_product[]" style="width: 100%;" multiple>
                <?php foreach ($products as $product):?>
                    <option selected value="<?php echo $product->id?>"><?php echo $product->name?></option>
                <?php endforeach;?>
            </select>
        </div>

	</div>
	<?php // UNTUK TAMPILANNYA ?>


	<?php
}
// End Add Meta Box - Custom Field

// =====================================================================================================
// Start Save Post
add_action('save_post', 'detailpromo_save_extras');
function detailpromo_save_extras(){
	global $post;
	
	if( defined('DOING_AUTOSAVE') && DOING_AUTOSAVE ){
		return $post_id;
	}else{

		// ============ INI FUNGSI UNTUK SAVE CUSTOM FIELD KE DATABASE WORDPRESS ==============
		$allvalue = array(		
				"produk_pilihan" => $_POST['list_product'],

		// ============ INI FUNGSI UNTUK SAVE CUSTOM FIELD KE DATABASE WORDPRESS ==============
				);
		$key = 'produk_pilihan';
        $value = $_POST['list_product'];
//		foreach ($allvalue as $key => $value) { // Cycle through the $events_meta array!
			if( $post->post_type == 'revision' ) return; // Don't store custom data twice
			//$value = implode(',', (array)$value); // If $value is an array, make it a CSV (unlikely)
			if(get_post_meta($post->ID, $key, FALSE)) { // If the custom field already has a value
				update_post_meta($post->ID, $key, $value);
			} else { // If the custom field doesn't have a value
				add_post_meta($post->ID, $key, $value);
			}
			if(!$value) delete_post_meta($post->ID, $key); // Delete if blank
//		}
	}
}
// End Save Post
// =====================================================================================================


add_action("admin_init", "detailfeatured_add_meta");
function detailfeatured_add_meta(){
	add_meta_box("detailfeature-meta", "Featured Products", "detailpromo_meta_options", "featured", "normal", "high");
}


function detailfeatured_meta_options(){
	global $post;
	if( defined('DOING_AUTOSAVE') && DOING_AUTOSAVE ){
		return $post_id;
	}

	// UNTUK GET DATA DARI DATABASE wordpress, kalo ambil dari database ga usa di pake.
	// ini seperti 
	
	$custom = get_post_custom($post->ID);
	$produk_pilihan = $custom["produk_pilihan"][0];

	// UNTUK GET DATA DARI DATABASE wordpress, kalo ambil dari database ga usa di pake.


	?>

	<?php // UNTUK TAMPILANNYA ?>
	<style type="text/css">
	<?php include('product-post-type.css'); ?>
	</style>
    <div class="product-extras meta-box-wrapper">

		<?php // teknik / tips buat ambil ID current page
			$productId = $post->ID; // kalo promo, berarti ini promo ID
            $productSelected = get_field('produk_pilihan', $post->ID);
            $resp = sendRequest('GET', MERCHANT_URL.'/api/product/search-products',[
                    'id' => json_encode($productSelected)
            ]);
            $products = $resp->data->products ?? [];
		?>

		<div class="row">
			<?php // contoh di product kan select dari database tuh, jadi tinggal ngikutin aja kalo perlu ?>

			<h4>Pilih Produk</h4>
            <select id="promo_list_product" name="list_product[]" style="width: 100%;" multiple>
                <?php foreach ($products as $product):?>
                    <option selected value="<?php echo $product->id?>"><?php echo $product->name?></option>
                <?php endforeach;?>
            </select>
        </div>

	</div>
	<?php // UNTUK TAMPILANNYA ?>


	<?php
}
// End Add Meta Box - Custom Field

// =====================================================================================================
// Start Save Post
add_action('save_post', 'detailfeatured_save_extras');
function detailfeatured_save_extras(){
	global $post;
	
	if( defined('DOING_AUTOSAVE') && DOING_AUTOSAVE ){
		return $post_id;
	}else{

		// ============ INI FUNGSI UNTUK SAVE CUSTOM FIELD KE DATABASE WORDPRESS ==============
		$allvalue = array(		
				"produk_pilihan" => $_POST['list_product'],

		// ============ INI FUNGSI UNTUK SAVE CUSTOM FIELD KE DATABASE WORDPRESS ==============
				);		
		foreach ($allvalue as $key => $value) { // Cycle through the $events_meta array!
			if( $post->post_type == 'revision' ) return; // Don't store custom data twice
			//$value = implode(',', (array)$value); // If $value is an array, make it a CSV (unlikely)
			if(get_post_meta($post->ID, $key, FALSE)) { // If the custom field already has a value
				update_post_meta($post->ID, $key, $value);
			} else { // If the custom field doesn't have a value
				add_post_meta($post->ID, $key, $value);
			}
			if(!$value) delete_post_meta($post->ID, $key); // Delete if blank
		}
	}
}
// End Save Post
// =====================================================================================================

?>