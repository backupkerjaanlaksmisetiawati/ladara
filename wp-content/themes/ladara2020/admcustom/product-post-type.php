<?php
add_action("admin_init", "detProduct_add_meta");
function detProduct_add_meta(){
	add_meta_box("detProduct-meta", "Product Stock", "detProduct_meta_options", "product", "normal", "high");
}

function detProduct_meta_options(){
	global $post;
	if( defined('DOING_AUTOSAVE') && DOING_AUTOSAVE ){
		return $post_id;
	}
	
	$custom = get_post_custom($post->ID);
	$post_adminid = $custom["post_adminid"][0];
	$dealer_id = $custom["dealer_id"][0];
	$post_kotacabang = $custom["post_kotacabang"][0];
	$expired_post = $custom["expired_post"][0];
	$postscripts = $custom["postscripts"][0];
	$formcs7 = $custom["formcs7"][0];
	$del_newsletter = $custom["del_newsletter"][0];
	$minidescription = $custom["minidescription"][0];
	$listcarid = $custom["listcarid"][0];
	
?>

<style type="text/css">
<?php include('product-post-type.css'); ?>
</style>

<div class="product-extras meta-box-wrapper">

<?php $product_id = $post->ID; ?>

<div class="row">
	<div class="col_variasi">
		<input type="hidden" name="product_id" value="<?php echo $product_id; ?>">

 		<table id="admin_stock" class="powerfull_table" border="1px">
 			<thead>
 				<tr>
 					<th>Stock ID</th>
 					<th>Warna</th>
 					<th>Ukuran</th>
 					<th>Nomor</th>
 					<th>Stock (pcs)</th>
 					<th>Update / Delete</th>
 				</tr>
 			</thead>
 			<tbody>

			<?php 
			$tob = 0;
			global $wpdb;
			$wpdb_query = "SELECT * FROM ldr_stock WHERE product_id = $product_id ";
			$res_query = $wpdb->get_results($wpdb_query, OBJECT);
			$count_res = count($res_query);
			if($count_res > 0){
				foreach ($res_query as $key => $value){
					$stock_id = $value->id;

					if($tob == 0){	
					?>
						<tr class="tr_0">
					<?php
						$tob = 1;
					}else if($tob == 1){
					?>
						<tr class="tr_1">
					<?php
						$tob = 0;
					}
			?>
							<td><?php echo $stock_id; ?></td>
							<td><?php echo ($value->warna_value ? ucfirst($value->warna_value) : ''); ?></td>
							<td><?php echo ($value->ukuran_value ? ucfirst($value->ukuran_value) : ''); ?></td>
							<td><?php echo ($value->nomor_value ? ucfirst($value->nomor_value) : ''); ?></td>
							<td><input type="number" name="qty_stock_<?php echo $stock_id; ?>" class="txt_qty" value="<?php echo ($value->product_quantity ? $value->product_quantity : '0'); ?>" min="0"></td>
							<td>
								<input type="button" class="btn_admvariation btn_adm_updateStock" data-action="ubah" data-id="<?php echo $stock_id; ?>" value="ubah">
								<input type="button" class="btn_admvariation del btn_adm_updateStock" data-action="hapus" data-id="<?php echo $stock_id; ?>" value="hapus">
							</td>
						</tr>
			<?php
			
				}
			}
			?>


 			</tbody>
 		</table>

 		<div class="info_adm_product">*Untuk tidak menampilkan variasi di produk, Silahkan ubah quantity = 0</div>


 		<div class="col_adm_addvariation">
 			<div class="ht_adm_addvariation">Tambah Variasi Stock</div>

 			<div class="f_adm_field">
 				<label>Warna</label>
 				<input type="text" class="txt_adm_field" name="adm_warna" placeholder="warna produk">
 			</div>

 			<div class="f_adm_field">
 				<label>Ukuran</label>
 				<input type="text" class="txt_adm_field" name="adm_ukuran" placeholder="ukuran produk">
 			</div>

 			<div class="f_adm_field">
 				<label>Nomor</label>
 				<input type="text" class="txt_adm_field" name="adm_nomor" placeholder="nomor ukuran, nomor pakaian">
 			</div>

 			<div class="f_adm_field">
 				<label>Stock</label>
 				<input type="number" class="txt_adm_field" name="adm_stock" value="0" min="0"> <span>pcs</span>
 			</div>

 			<input type="button" class="sub_adm_addvariation" value="Tambah Variasi" >

 			<div class="info_adm_product">*Silahkan isi sesuai dengan ketentuan variasi produk, kosongkan yang tidak perlu.</div>
 		</div>

 		<div class="col_adm_addvariation">
 		
	 		<div class="ht_adm_addvariation">History Stock</div>

	 		<table id="admin_stock" class="powerfull_table" border="1px">
	 			<thead>
	 				<tr>
	 					<th>Stock ID</th>
	 					<th>Tanggal</th>
	 					<th>Action</th>
	 					<th>From Qty</th>
	 					<th>To Qty</th>
	 					<th>Admin</th>
	 				</tr>
	 			</thead>
	 			<tbody>

				<?php 
				$tob = 0;
				global $wpdb;
				$wpdb_query = "SELECT * FROM ldr_stock_log WHERE product_id = $product_id ORDER BY created_date DESC ";
				$res_query = $wpdb->get_results($wpdb_query, OBJECT);
				$count_res = count($res_query);
				if($count_res > 0){
					foreach ($res_query as $key => $value){
						$created_date = $value->created_date;
						$stock_id = $value->stock_id;
						$from_stock = $value->from_stock;
						$to_stock = $value->to_stock;
						$status = $value->status;
						$admin_name = $value->admin_name;

				?>
							<tr>
								<td><?php echo $stock_id; ?></td>
			 					<td><?php echo $created_date; ?></td>
			 					<?php if(strtolower($status) == 'new'){ ?>
			 						<td style="color: green;"><?php echo $status; ?></td>
			 					<?php }else if(strtolower($status) == 'delete'){ ?>
			 						<td style="color: red;"><?php echo $status; ?></td>
			 					<?php }else{ ?>
			 						<td><?php echo $status; ?></td>
			 					<?php } ?>
			 					<td><?php echo $from_stock; ?></td>
			 					<td><?php echo $to_stock; ?></td>
			 					<td><?php echo $admin_name; ?></td>
							</tr>
				<?php
				
					}
				}
				?>


	 			</tbody>
	 		</table>

 		</div>


	</div>
</div>

</div>




<?php
}
// End Add Meta Box - Custom Field

// =====================================================================================================
// Start Save Post
add_action('save_post', 'detProduct_save_extras');
function detProduct_save_extras(){
	global $post;
	
	if( defined('DOING_AUTOSAVE') && DOING_AUTOSAVE ){
		return $post_id;
	}else{
		// $del_newsletter= (isset($_POST['del_newsletter'])) ? $_POST['del_newsletter'] : "";
		$allvalue = array(		
				"post_adminid" => $_POST['post_adminid'],
				"dealer_id" => $_POST['dealer_id'],
				"post_kotacabang" => $_POST['post_kotacabang'],
				"expired_post" => $_POST['expired_post'],
				"postscripts" => $_POST['postscripts'],
				"formcs7" => $_POST['formcs7'],
				"del_newsletter" => $del_newsletter,
				"minidescription" => $_POST['minidescription'],
				"listcarid" => $_POST['listcarid'],

				);		
		foreach ($allvalue as $key => $value) { // Cycle through the $events_meta array!
			if( $post->post_type == 'revision' ) return; // Don't store custom data twice
			//$value = implode(',', (array)$value); // If $value is an array, make it a CSV (unlikely)
			if(get_post_meta($post->ID, $key, FALSE)) { // If the custom field already has a value
				update_post_meta($post->ID, $key, $value);
			} else { // If the custom field doesn't have a value
				add_post_meta($post->ID, $key, $value);
			}
			if(!$value) delete_post_meta($post->ID, $key); // Delete if blank
		}
	}
}
// End Save Post
// =====================================================================================================

?>