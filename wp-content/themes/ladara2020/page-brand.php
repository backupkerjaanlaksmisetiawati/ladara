<?php
/*
Template Name: page brand
*/
?>
<?php get_header(); ?>
<?php if (have_posts()) : while (have_posts()) : the_post(); ?>

    <article id="post-<?php the_ID(); ?>" <?php post_class('clearfix'); ?> role="article" itemscope itemtype="http://schema.org/BlogPosting">

        <style type="text/css">
            .header{
                height: 135px;
            }
            .header_list_tops{
                display: none!important;
            }
        </style>
        <?php
        date_default_timezone_set("Asia/Jakarta");
        $nowdate = date('Y-m-d');
        $now_time = strtotime($nowdate);

        $current_user = wp_get_current_user();
        $u_id = $current_user->ID;

        if(isset($_GET['keyword'])){
            $keyword = htmlspecialchars($_GET['keyword']);
        }else{
            $keyword = '';
        }

        $url = explode('/',$_SERVER['REQUEST_URI']);
        $slug = $url[2] ?? '';
        $brand_id = '';
        $brand_name= '';
        if ($slug != ''){
            $exSlug = explode('-', $slug);
            $brand_id = end($exSlug);
            array_pop($exSlug);
            $brand_name = implode(' ', $exSlug);
        }

        $offset = 0;
        $limit_item = 30;
        if(isset($_GET['pg'])){
            $page = $_GET['pg'];
            $page_now = (int)$page-1;
            $offset = (int)$page_now*(int)$limit_item;
        }else{
            $page = 1;
            $offset = (int)$page*0;
        }

        if(isset($_GET['by'])){
            $by = $_GET['by'];
            $_SESSION['by'] = $by;
        }else{
            $by = 'terkait';
        }

        if(isset($_SESSION['by'])){
            $by = $_SESSION['by'];
        }

        global $wp;
        $cur_page = home_url( $wp->request );

        ?>
        <style type="text/css">
            .second_subfilter{
                display: none !important;
            }
        </style>

        <input type="hidden" name="keyword_s" value="">
        <input type="hidden" name="offset" value="<?php echo $offset; ?>">
        <input type="hidden" name="limit" value="<?php echo $limit_item; ?>">
        <input type="hidden" name="pagination" value="<?php echo $page; ?>">
        <input type="hidden" name="brand" value="<?php echo $brand_id; ?>">
        <input type="hidden" name="page_url" value="<?php echo $cur_page; ?>">
        <input type="hidden" name="category" value="">


        <div id="single_brands" class="row row_masterPage" data-url="<?php echo home_url(); ?>">

            <div class="col-md-3 col_shop_filter">

                <?php get_template_part( 'content', 'filter-shop' ); ?>

            </div>


            <div class="col-md-9 col_product_shoplist">

                <div class="box_head_shoplist">


                    <h2 class="ht_head_shoplist">
                        Produk <?php echo $brand_name; ?>
                    </h2>


                    <?php
                    $sortby = array();
                    $sortby['terkait'] = 'Paling Sesuai';
                    $sortby['asc'] = 'Produk A-Z';
                    $sortby['desc'] = 'Produk Z-A';
                    $sortby['termurah'] = 'Termurah';
                    ?>
                    <div class="ht_head_sortby pull_right">
                        <div class="label">Urutkan Berdasarkan </div>
                        <select name="filter_sortby" class="sel_sortby">
                            <?php
                            foreach ($sortby as $key => $value) {
                                if($key == $by){
                                    ?>
                                    <option value="<?php echo $key; ?>" selected="selected"><?php echo $value; ?></option>
                                    <?php
                                }else{
                                    ?>
                                    <option value="<?php echo $key; ?>"><?php echo $value; ?></option>
                                    <?php
                                }
                            }
                            ?>
                        </select>
                    </div>


                    <div class="bx_mob_sortby mob_display">
                        <div class="bx_mob_insortby">
                            <div class="ht_mob_sortby">Urutkan Berdasarkan:</div>
                            <ul class="ul_mob_sortby">
                                <?php
                                foreach ($sortby as $key => $value) {
                                    if($key == $by){
                                        ?>
                                        <li class="act" data-val="<?php echo $key; ?>"><a><?php echo $value; ?></a></li>
                                        <?php
                                    }else{
                                        ?>
                                        <li data-val="<?php echo $key; ?>"><a><?php echo $value; ?></a></li>
                                        <?php
                                    }
                                }
                                ?>
                            </ul>

                            <input type="button" class="sub_subfilter close_mob_sortby" value="Tutup">
                        </div>
                    </div>
                </div>

                <div id="view_resProduct" class="row row_resProduct">

                </div>

                <div class="row">
                    <div class="col-md-12 col_pagination_shop view_pagi_shops">
                        <?php // view pagi shops my ajax ?>
                    </div>
                </div>

            </div>
        </div>

    </article>
<?php endwhile; ?>
<?php else : ?>
    <article id="post-not-found" class="hentry clearfix">
        <header class="article-header">
            <h1><?php _e( 'Oops, Post Not Found!', 'bonestheme' ); ?></h1>
        </header>
        <section class="entry-content">
            <p><?php _e( 'Uh Oh. Something is missing. Try double checking things.', 'bonestheme' ); ?></p>
        </section>
        <footer class="article-footer">
            <p><?php _e( 'This is the error message in the single.php template.', 'bonestheme' ); ?></p>
        </footer>
    </article>
<?php endif; ?>
<?php get_footer(); ?>