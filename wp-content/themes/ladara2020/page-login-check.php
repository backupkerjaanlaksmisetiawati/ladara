<?php
/*
Template Name: Login Check Page
*/
?>
<?php 
// ================ custom google auth ================
include('content-function-login.php'); 
$login_button = '';
$valid_login = 0;
//This $_GET["code"] variable value received after user has login into their Google Account redirct to PHP script then this variable value has been received
if(isset($_GET["code"])){
	//It will Attempt to exchange a code for an valid authentication token.
	$token = $google_client->fetchAccessTokenWithAuthCode($_GET["code"]);

	//This condition will check there is any error occur during geting authentication token. If there is no any error occur then it will execute if block of code/
	if(!isset($token['error'])){
		//Set the access token used for requests
		$google_client->setAccessToken($token['access_token']);

		//Store "access_token" value in $_SESSION variable for future use.
		$_SESSION['access_token'] = $token['access_token'];

		//Create Object of Google Service OAuth 2 class
		$google_service = new Google_Service_Oauth2($google_client);

		//Get user profile data from google
		$data_google = $google_service->userinfo->get();
		$_SESSION['alldata'] = $data_google;

		//Below you can find Get profile data and store into $_SESSION variable
		$data_user = array();
		if(isset($data_google) AND !empty($data_google)){
				// $data_user['verifiedEmail'] = $data_google['verifiedEmail'];
				$data_user['first_name'] = $data_google['given_name'];
				$data_user['last_name'] = $data_google['family_name'];
				$data_user['user_email'] = $data_google['email'];
				$data_user['user_gender'] = $data_google['gender'];
				// $data_user['user_image'] = $data_google['picture'];

				$data_user['user_phone'] = '';
				$data_user['acc_type'] = 2; // 2 for google

				// === login now ===
				if(isset($data_google['email']) AND $data_google['email'] != ''){
					$valid_login = login_socialmedia($data_user,'google');
				}
				
		}
	}
}
// ================ custom google auth ================
if(isset($_SESSION['rfr']) AND $_SESSION['rfr'] != ''){
	$referer = $_SESSION['rfr'];
}else{

	$rfr = wp_get_referer();
	if(isset($rfr) AND $rfr != ''){
		$referer = wp_get_referer();
		$_SESSION['rfr'] = $referer;
	}else{
		$referer = home_url();
	}
}

if($valid_login == 0){ // if failed login google
	session_destroy();
	?>
	<div id="redirect_ok" data-hurl="<?php echo home_url(); ?>/login/?login=failed"></div>
	<script>
	var plant = document.getElementById('redirect_ok');
	var hurl = plant.getAttribute('data-hurl'); 
	  setTimeout(function(){
	    location.replace(hurl); 
	  }, 2000);
	</script>
	<?php
}else{ // if succcess
	?>
	<div id="redirect_ok" data-hurl="<?php echo $referer; ?>"></div>
	<script>
		var plant = document.getElementById('redirect_ok');
		var hurl = plant.getAttribute('data-hurl'); 
	    setTimeout(function(){
	      location.replace(hurl); 
	    }, 2000);
	</script>
	<?php
}
?>
