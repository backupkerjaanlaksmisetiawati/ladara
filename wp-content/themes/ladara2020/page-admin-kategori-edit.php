<?php
/*
Template Name: admin kategori edit
*/
?>

<?php get_header('admin'); ?>

<?php if (have_posts()) : while (have_posts()) : the_post(); ?>

    <?php
    date_default_timezone_set("Asia/Jakarta");
    $nowdate = date('Y-m-d H:i:s');

    $current_user = wp_get_current_user();
    $u_id = $current_user->ID;
    $u_roles = $current_user->roles;
    $admin = 0;

    if(isset($u_roles) AND !empty($u_roles)){
        foreach ($u_roles as $key => $value) {
            if(strtolower($value) == 'administrator'){
                $admin = 1;
            }
            if(strtolower($value) == 'shop_manager'){
                $admin = 1;
            }
        }
    }

    if((isset($u_id) AND $u_id != 0) AND $admin == 1){
        $arrCategories = [];
        function treeCate($categories, $lvl = 1)
        {
            global $arrCategories;
            foreach ($categories as $cate){
                $arrCategories[] = [
                    'id' => $cate->id,
                    'name' => $cate->name,
                    'level' => $lvl
                ];
                if (count($cate->children) > 0){
                    treeCate($cate->children, $lvl+1);
                }
            }
        }

        $client = new GuzzleHttp\Client();
        try {
            $req = $client->request('GET', MERCHANT_URL . '/api/categories');
            $cate = json_decode($req->getBody());
            treeCate($cate->data);
        } catch (\GuzzleHttp\Exception\GuzzleException $e) {
            $categories = [];
        }

        try {
            $id = $_GET['id'] ?? 0;
            $req = $client->request('GET', MERCHANT_URL . '/api/categories/'.$id);
            $end = json_decode($req->getBody());
            $category = $end->data;
        } catch (\GuzzleHttp\Exception\GuzzleException $e) {
            $category = [];
        }

//        echo '<pre>';
//        print_r($arrCategories);exit();

        ?>
        <style>
            .header{
                background: #EDEFF6;
                color: #607FC2;
                font-weight: bold;
                padding: 26px;
                border-radius: 10px 10px 0 0 ;
                box-shadow: 0px 3px 6px #A1A1A129;
            }
            .body-form{
                padding: 31px 5px;
                width: 100%;
                background: #ffffff;
                box-shadow: 0px 3px 6px #A1A1A129;
            }
            .d-block{
                display: block;
            }
            .ldr.text-info{
                color: #313131 !important;
                font-size: 11px;
                font-weight: 100;
            }
            .mb-50{
                margin-bottom: 50px;
            }
            .btn-submit{
                background: #0080FF 0% 0% no-repeat padding-box;
                border-radius: 147px;
                color: #ffffff;
                padding: 10px 30px;
            }
            .btn-submit:hover{
                color: #ffffff;
            }
            .btn-cancle{
                background: #FFFFFF 0% 0% no-repeat padding-box;
                border: 2px solid #0080FF;
                border-radius: 147px;
                color: #0080FF;
                padding: 10px 30px;
                width: 120px;
            }
            .btn-cancle:hover{
                color: #0080FF;
            }
        </style>
        <!-- Begin Page Content -->
        <div class="container-fluid">

            <!-- Page Heading -->
            <div class="row" style="margin-bottom: 2em;">
                <div class="col-md-6">
                    <h1 class="h3 mb-2 text-gray-800">kategori</h1>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="header">
                        Edit kategori
                    </div>
                </div>
                <div class="col-md-12">
                    <div class=" body-form">
                        <form id="formKategori-edit" action="<?php echo MERCHANT_URL.'/api/categories/edit/'.$id?>" method="put" enctype="multipart/form-data">
                            <div class="form-group row mb-50">
                                <label class="col-md-4 col-form-label">
                                    Nama
                                    <span class="d-block ldr text-info">Nama kategori akan muncul pada menu pilihan kategori</span>
                                </label>
                                <div class="col-md-8">
                                    <input required name="name" value="<?php echo $category->name ?? ''?>" type="text" class="form-control" >
                                </div>
                            </div>
                            <div class="form-group row mb-50">
                                <label class="col-md-4 col-form-label">
                                    Slug
                                    <span class="d-block ldr text-info">Slug merupakan versi nama yang url-friendly. Terdiri dari karakter lowercase dan hanya berisi huruf, angka dan tanda hubung(-)</span>
                                </label>
                                <div class="col-md-8">
                                    <input name="slug" value="<?php echo $category->slug ?? ''?>" required type="text" class="form-control">
                                </div>
                            </div>
                            <div class="form-group row mb-50">
                                <label class="col-md-4 col-form-label">
                                    Kategori Induk
                                    <span class="d-block ldr text-info">Menunjukkan hierarki dari kategori. Contoh : Buah merupakan kategori induk dari Apel dan Jeruk</span>
                                </label>
                                <div class="col-md-4">
                                    <select class="form-control" name="parent" >
                                        <option value="0">Tidak ada</option>
                                        <?php
                                        foreach ($arrCategories as $cate){
                                            $space= '';
                                            if ($cate['level'] > 1){
                                                for ($i=0;$i<$cate['level'];$i++){
                                                    $space .= '&nbsp;&nbsp;';
                                                }
                                            }
                                            ?>
                                            <option value="<?php echo $cate['id']?>" <?php echo $category->parent == $cate['id'] ? 'selected' : ''?> ><?php echo $space.''.$cate['name']?></option>

                                            <?php
                                        }
                                        ?>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group row mb-50">
                                <label class="col-md-4 col-form-label">
                                    Tampilkan di home
                                    <span class="d-block ldr text-info">Atur kategori sebagai kategori utama</span>
                                </label>
                                <div class="col-md-8">
                                    <div class="form-check form-check-inline" style="margin-right: 30px;">
                                        <input class="form-check-input" type="radio" name="featured" id="inlineRadio1" <?php echo $category->featured == 1 ? 'checked' : ''?> value="1">
                                        <label class="form-check-label" for="inlineRadio1">Ya</label>
                                    </div>
                                    <div class="form-check form-check-inline">
                                        <input class="form-check-input" type="radio" name="featured" id="inlineRadio2" <?php echo $category->featured == 0 ? 'checked' : ''?> value="0">
                                        <label class="form-check-label" for="inlineRadio2">Tidak</label>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group row mb-50">
                                <label class="col-md-4 col-form-label">
                                    Thumbnail Kategori
                                    <span class="d-block ldr text-info">Menunjukkan hierarki dari kategori. Contoh : Buah merupakan kategori induk dari Apel dan Jeruk</span>
                                </label>
                                <div class="col-md-8">
                                    <div class="row">
                                        <div class="col-md-2">
                                            <img style="border-radius: 50%;max-width: 100%;" src="<?php echo $category->image?>" alt="">
                                        </div>
                                        <div class="col-md-4">
                                            <span class="d-block ldr text-info" style="margin-bottom: 10px;">Besar file maksimal adalah 100 kB dengan rasio gambar 1:1. Ekstensi file yang diperbolehkan: PNG</span>
                                            <input type="file" name="image"  id="image">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="footer-form" style="width: 100%;border-top: 1px solid #DDDDDD;padding: 10px 0;text-align: right;">
                                <div>
                                    <a href="/dashoard/admin-kategori">
                                        <input value="Batal" class="btn btn-cancle">
                                    </a>
                                    <button  type="submit" class="btn btn-submit">Ubah</button>
                                </div>
                            </div>
                        </form>
                    </div>


                </div>
            </div>

            <!-- DataTales Example -->
        </div>
        <!-- /.container-fluid -->

    <?php }else{ ?>
        <script>
            // 'Getting' data-attributes using getAttribute
            var plant = document.getElementById('body');
            console.log(plant);
            var hurl = plant.getAttribute('data-hurl');
            location.replace(hurl+'/login/');
        </script>
    <?php } ?>

<?php endwhile; ?>
<?php else : ?>
    <?php get_template_part( 'content', '404pages' ); ?>
<?php endif; ?>
<?php get_footer('admin'); ?>
