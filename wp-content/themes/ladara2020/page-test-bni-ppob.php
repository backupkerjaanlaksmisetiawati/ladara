<?php /* Template Name: Ladara Test PPOB BNI */ ?>

<?php
if(isset($_GET["ppob"]) && $_GET["ppob"] == "GET_TOKEN") {
    //https://staging.ladara.id/testing-bni?ppob=GET_TOKEN
    
    $Authorization = base64_encode(getenv('BNI_CLIENT_ID').":".getenv('BNI_CLIENT_SECRET'));

    $curl_header = array(
        "Authorization: Basic ".$Authorization,
        "Content-Type: application/x-www-form-urlencoded",
    );

    $curl_post = array(
        "grant_type" => "client_credentials"
    );

    $curl_url = getenv('BNI_BASE_URL').":".getenv('BNI_PORT')."/api/oauth/token";

    $curl = curl_init();

    curl_setopt($curl, CURLOPT_URL, $curl_url);
    curl_setopt($curl, CURLOPT_HTTPHEADER, $curl_header);
    curl_setopt($curl, CURLOPT_FOLLOWLOCATION, true);
    curl_setopt($curl, CURLOPT_HTTP_VERSION, CURL_HTTP_VERSION_1_1);
    curl_setopt($curl, CURLOPT_CUSTOMREQUEST, "POST");
    curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($curl, CURLOPT_ENCODING, "");
    curl_setopt($curl, CURLOPT_POSTFIELDS, "grant_type=client_credentials");

    $curl_response = curl_exec($curl);

    $header_size = curl_getinfo($curl, CURLINFO_HEADER_SIZE);

    curl_close($curl);

    $json_response = json_decode($curl_response);

    $header = substr($curl_response, 0, $header_size);

    echo "Header";
    ddbug($header, false);

    echo "Body";
    ddbug($json_response);
}
?>
