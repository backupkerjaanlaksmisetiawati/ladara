			<div id="content">

				<div id="inner-content" class="wrap clearfix">

					<div id="main" class="eightcol first clearfix" role="main">

						<article id="post-not-found" class="hentry clearfix">
					        <div class="row row_finishCheckout">
					            <div class="col-md-12 col_finishCheckout">
					                <a href="<?php echo home_url(); ?>">
					                    <div class="bx_backShop">
					                        <span class="glyphicon glyphicon-menu-left"></span> Kembali ke Home
					                    </div>
					                </a>
					                <div class="bx_finishCheckout">
					                    <div class="mg_registerIcon">
					                        <img class="lazy" data-src="<?php bloginfo('template_directory'); ?>/library/images/ico_404.svg">
					                    </div>
					                    <div class="ht_register">Ups, halaman yang kamu cari belum bisa ditemukan :(</div>

					                    <div class="ht_sucs_register">
					                        Coba lagi dengan menekan tombol dibawah ini ya!
					                    </div>
					                    <div class="bx_def_checkout">
					                        <a href="<?php echo home_url(); ?>/">
					                            <button class="btn_def_checkout">Kembali ke Beranda</button>
					                        </a>
					                    </div>
					                </div>
					            </div>
					        </div>
						</article>

					</div>

				</div>

			</div>