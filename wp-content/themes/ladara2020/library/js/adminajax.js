// var $ = jQuery;
(function($){
	jQuery(document).ready(function(){
		$('.btn_adm_updateStock').click(function(){
			var stock_id = $(this).data('id');
			var action = $(this).data('action');
			var product_qty = $('input[name="qty_stock_'+stock_id+'"]').val();

			if(action == 'ubah'){

				if(confirm("Apakah Anda merubah variasi stock ini?")){

					$.ajax({
						  // url : ajaxscript.ajaxurl,
						  // url : '/wp-admin/admin-ajax.php',
						  url : ajaxurl, //pakai url ajax yang dari global var
						  data: {
								'action':'ajax_adm_updateVariation',
								'stock_id':stock_id,
								'product_qty':product_qty,
						  },
						  success:function(result) {
							console.log(result);

							if(result == 1){
								alert('Variasi & Stock ini telah di perbaruhi.');
								// location.reload();
							}else{
								alert('Gagal, field salah / kurang / variasi tidak ada.');
							}

						  }
					});

				}else{
					return false;
				}


			}else if(action == 'hapus'){

				if(confirm("Apakah Anda yakin ingin menghapus variasi stock ini?")){

					$.ajax({
						  // url : ajaxscript.ajaxurl,
						  // url : '/wp-admin/admin-ajax.php',
						  url : ajaxurl, //pakai url ajax yang dari global var
						  data: {
								'action':'ajax_adm_deleteVariation',
								'stock_id':stock_id,
								'product_qty':product_qty,
						  },
						  success:function(result) {
							console.log(result);

							if(result == 1){
								alert('Variasi & Stock ini telah di HAPUS.');
								location.reload();
							}else{
								alert('Gagal, field salah / kurang / variasi tidak Ada.');
							}

						  }
					});

				}else{
					return false;
				}

			}

		});


		$('.sub_adm_addvariation').click(function(){

			var product_id = $('input[name="product_id"]').val();
			var adm_warna = $('input[name="adm_warna"]').val();
			var adm_ukuran = $('input[name="adm_ukuran"]').val();
			var adm_nomor = $('input[name="adm_nomor"]').val();
			var adm_stock = $('input[name="adm_stock"]').val();

			if(confirm("Apakah Anda ingin menambah variasi ini?")){

				$.ajax({
					  // url : ajaxscript.ajaxurl,
					  // url : '/wp-admin/admin-ajax.php',
					  url : ajaxurl, //pakai url ajax yang dari global var
					  data: {
							'action':'ajax_adm_addVariation',
							'product_id':product_id,
							'adm_warna':adm_warna,
							'adm_ukuran':adm_ukuran,
							'adm_nomor':adm_nomor,
							'adm_stock':adm_stock,
					  },
					  success:function(result) {
						console.log(result);

						if(result == 1){
							alert('Variasi & Stock Baru telah di tambahkan.');
							// location.reload();

							$('input[name="adm_warna"]').val('');
							$('input[name="adm_ukuran"]').val('');
							$('input[name="adm_nomor"]').val('');
							$('input[name="adm_stock"]').val('');

						}else{
							alert('Gagal, field salah / kurang / variasi sudah Ada.');
						}

					  }
				});

			}else{
				return false;
			}
		});

		if ($('#promo_list_product').length > 0 ){
			$('#promo_list_product').select2({
				placeholder: 'Cari produk',
				minimumInputLength:3,
				ajax: {
					url: ajaxurl+'?action=ajax_get_products',
					dataType: 'json',
					delay: 3000,
					processResults: function (data) {
						return {
							results: data
						};
					},
					cache: true,
				}
			});
		}
	});
})(jQuery);

