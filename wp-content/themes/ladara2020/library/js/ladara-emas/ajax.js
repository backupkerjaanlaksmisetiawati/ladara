//proses minify. function yang sudah fix harus di minify. use https://codebeautify.org/jsviewer
// NOTES: function atau event yg sama digabung
function refreshGldBalance() {
  $("#wrapgldBalanceRp").html("please wait...");
  $("#wrapmgldBalanceRp").html("please wait...");
  $.ajax({
    url: ajaxscript.ajaxurl,
    type: "POST",
    data: "action=ajax_gld_balance",
    success: function (res) {
      var res = $.parseJSON(res);
      var formatter = new Intl.NumberFormat("id-ID");
      var currency_balance = parseFloat(res.data.balance_currency).toFixed();
      $("#wrapgldBalanceRp").html('Rp <span id="gldBalanceRp"></span>');
      $("#gldBalanceRp").html(formatter.format(currency_balance));
      $("#wrapmgldBalanceRp").html('Rp <span id="mgldBalanceRp"></span>');
      $("#mgldBalanceRp").html(formatter.format(currency_balance));
      // var unit_value = parseFloat(res.data.balance_gram).toFixed(4);
      // unit_value = unit_value.replace(".", ",");
      // $("#gldBalanceGr").html(unit_value);
      // $("#mgldBalanceGr").html(unit_value);
      setTimeout(refreshGldBalance, 120000);
    },
    error: function (res) {
      console.log("error refresh gold balance");
      console.log(res);
    }
  });
}

function refreshGldRate() {
  $("#wrapgldRateBuy").html("please wait...");
  $("#wrapmgldRateBuy").html("please wait...");
  $("#wrapgldRateSell").html("please wait...");
  $("#wrapmgldRateSell").html("please wait...");
  $.ajax({
    url: ajaxscript.ajaxurl,
    type: "POST",
    data: "action=ajax_gld_rate",
    success: function (res) {
      var res = $.parseJSON(res);
      if (res.code === 200) {
        var formatter = new Intl.NumberFormat("id-ID");
        var value_buy_price = parseFloat(res.data.buy_price).toFixed();
        $("#wrapgldRateBuy").html('Rp <span id="gldRateBuy"></span>/gr');
        $("#wrapmgldRateBuy").html('Rp <span id="mgldRateBuy"></span>/gr');
        $("#gldRateBuy").html(formatter.format(value_buy_price));
        $("#mgldRateBuy").html(formatter.format(value_buy_price));
        var value_sell_price = parseFloat(res.data.sell_price).toFixed();
        $("#wrapgldRateSell").html('Rp <span id="gldRateSell"></span>/gr');
        $("#wrapmgldRateSell").html('Rp <span id="mgldRateSell"></span>/gr');
        $("#gldRateSell").html(formatter.format(value_sell_price));
      }
      setTimeout(refreshGldRate, 60000);
    },
    error: function (res) {
      console.log("error refresh gold rate");
      console.log(res);
    }
  });
}

function ajax_linking_treasury(e) {
  $(".fix_loading").css("display", "block");
  $("#form_linktreasury #wp-submit").attr("disabled", true);

  var email = $("#form_linktreasury input[name=email]").val();
  var pass = $("#form_linktreasury input[name=password]").val();
  var is_update = $("#form_linktreasury input[name=is_update]").val();

  if (email == "") {
    $(".fix_loading").css("display", "none");
    $("#form_linktreasury .error_email").show().html("Email Treasury tidak boleh kosong.");
  }

  if (pass == "") {
    $(".fix_loading").css("display", "none");
    $("#form_linktreasury .error_password").show().html("Password Treasury tidak boleh kosong.");
  }

  if (pass != "" && email != "") {
    $.ajax({
      url: ajaxscript.ajaxurl,
      type: "POST",
      data: "email=" + email + "&password=" + pass + "&is_update=" + is_update + "&action=ajax_linking_treasury",
      success: function (res) {
        var res = $.parseJSON(res);
        $("#form_linktreasury #wp-submit").attr("disabled", false);
        $(".fix_loading").css("display", "none");
        if (res.code === 200) {
          $("#form_linktreasury .err_info.alert-success").show().html("<b>Selamat!</b> kamu sudah menghubungkan akun Treasury kamu dengan Ladara Emas.");
        } else {
          if (res.message == "Validation") {
            $.each(res.data, function (i, val) {
              console.log(val);
              $("#form_linktreasury .error_" + i).show().html(val);
            });
          } else {
            $("#form_linktreasury .error_link_to_treasury").show().html(res.message);
          }
        }
      },
      error: function (res) {
        $(".fix_loading").css("display", "none");
        console.log("error linking ladara emas with treasury");
        console.log(res);
      }
    });
  }
}

function ajax_buy_emas(viewType) {
  $(".bx_gldLoading").css("display", "flex");
  if (viewType === "mobile") {
    var typePaymentBeliEmas = $("input[name=mtype_payment]:checked").val();
  }
  if (viewType === "desktop") {
    var typePaymentBeliEmas = $("input[name=type_payment]:checked").val();
  }
  if (typePaymentBeliEmas == "va") {
    if (viewType === "mobile") {
      var vaBeliEmas = $("select[name=mva]").val();
    }
    if (viewType === "desktop") {
      var vaBeliEmas = $("select[name=va]").val();
    }
    if (vaBeliEmas == 0) {
      $(".error_beliemas_va").html("Silahkan pilih virtual account terlebih dahulu.").css({
        "display": "block",
        "color": "#FE5461",
        "margin-top": "5px"
      });
      $(".bx_gldLoading").css("display", "none");
      $(".bx_leBeliPopup").css("display", "none");
      $("body").css("overflow", "auto");
      $("html").css("overflow-x", "hidden");
      $("html").css("overflow-y", "auto");
    } else {
      if (viewType === "mobile") {
        $("#mlecheckoutbeliemas").submit();
      }
      if (viewType === "desktop") {
        $("#lecheckoutbeliemas").submit();
      }
    }
  } else if (typePaymentBeliEmas == "ewallet") {
    if (viewType === "mobile") {
      var ewalletBeliEmas = $("select[name=mewallet]").val();
    }
    if (viewType === "desktop") {
      var ewalletBeliEmas = $("select[name=ewallet]").val();
    }
    if (viewType === "mobile") {
      var ewalletPhoneBeliEmas = $("input[name=mewallet_phone]").val();
    }
    if (viewType === "desktop") {
      var ewalletPhoneBeliEmas = $("input[name=ewallet_phone]").val();
    }
    if (ewalletBeliEmas == 0) {
        $(".error_beliemas_ewallet").html("Silahkan pilih e-wallet terlebih dahulu.").css({
          "display": "block",
          "color": "#FE5461",
          "margin-top": "5px"
        });
        $(".bx_gldLoading").css("display", "none");
        $(".bx_leBeliPopup").css("display", "none");
        $("body").css("overflow", "auto");
        $("html").css("overflow-x", "hidden");
        $("html").css("overflow-y", "auto");
    // } else if (ewalletPhoneBeliEmas == '') {
    //   var htmlErrorEwalletPhoneBeliEmas = "Masukan no hp yang sudah terdaftar di " + ewalletBeliEmas + " terlebih dahulu.";
    //   // if (ewalletBeliEmas == "LINKAJA") {
    //   //   htmlErrorEwalletPhoneBeliEmas = "Masukan no hp yang sudah terdaftar di LinkAja terlebih dahulu.";
    //   // }
    //   $(".error_beliemas_ewallet").html(htmlErrorEwalletPhoneBeliEmas).css({
    //     "display": "block",
    //     "color": "#FE5461",
    //     "margin-top": "5px"
    //   });
    //   $(".bx_gldLoading").css("display", "none");
    //   $(".bx_leBeliPopup").css("display", "none");
    //   $("body").css("overflow", "auto");
    //   $("html").css("overflow-x", "hidden");
    //   $("html").css("overflow-y", "auto");
    } else {
      if (viewType === "mobile") {
        $("#mlecheckoutbeliemas").submit();
      }
      if (viewType === "desktop") {
        $("#lecheckoutbeliemas").submit();
      }
    }
  } else {
    if (viewType === "mobile") {
      $("#mlecheckoutbeliemas").submit();
    }
    if (viewType === "desktop") {
      $("#lecheckoutbeliemas").submit();
    }
  }
}

function nilaiRpJual() {
  calculateGldJual("currency", "desktop");
}

function nilaiGrJual() {
  calculateGldJual("gold", "desktop");
}

function mnilaiRpJual() {
  calculateGldJual("currency", "mobile");
}

function mnilaiGrJual() {
  calculateGldJual("gold", "mobile");
}

function calculateGldJual(amountType, viewType) {
  $(".loadingGldJual").html("Loading...");
  if (amountType === "currency") {
    if (viewType === "desktop") {
      var nilaiRpJual = $("#valueRpJual").val();
    }
    if (viewType === "mobile") {
      var nilaiRpJual = $("#mvalueRpJual").val();
    }
    var ajaxData = {
      action: "ajax_calculate_emas",
      emasRpValue: nilaiRpJual,
      emasGrValue: 0,
      typeCalculate: "sell",
      valueType: "currency"
    };
  } else {
    if (viewType === "desktop") {
      var nilaiGrJual = $("#valueGrJual").val();
    }
    if (viewType === "mobile") {
      var nilaiGrJual = $("#mvalueGrJual").val();
    }
    var ajaxData = {
      action: "ajax_calculate_emas",
      emasRpValue: 0,
      emasGrValue: nilaiGrJual,
      typeCalculate: "sell",
      valueType: "gold"
    };
  }
  $.ajax({
    url: ajaxscript.ajaxurl,
    type: "POST",
    data: ajaxData,
    success: function (res) {
      var res = $.parseJSON(res);
      if (res.code === 200) {
        $(".loadingGldJual").html("");
        $(".jualemassekarang").attr("disabled", false);
        $(".mjualemassekarang").attr("disabled", false);
        var unit_value = parseFloat(res.data.unit).toFixed(4);
        unit_value = unit_value.replace(".", ",");
        var currency_value = parseFloat(res.data.sell_price).toFixed();
        var formatter = new Intl.NumberFormat("id-ID");
        $(".harga_jual_emas").val(res.data.sell_price);
        if (amountType === "currency") {
          if (viewType === "desktop") {
            $("#valueGrJual").val(unit_value);
          }
          if (viewType === "mobile") {
            $("#mvalueGrJual").val(unit_value);
          }
          $(".calculate_type").val("currency");
        } else {
          if (viewType === "desktop") {
            $("#valueRpJual").val(formatter.format(currency_value));
          }
          if (viewType === "mobile") {
            $("#mvalueRpJual").val(formatter.format(currency_value));
          }
          $(".calculate_type").val("gold");
        }
      } else {
        if (amountType === "currency") {
          $("#valueGrJual").val("");
          $("#mvalueGrJual").val("");
        } else {
          $("#valueRpJual").val("");
          $("#mvalueRpJual").val("");
        }
        $(".loadingGldJual").html(res.message);
        $(".jualemassekarang").attr("disabled", true);
        $(".mjualemassekarang").attr("disabled", true);
      }
    },
    error: function (res) {
      $(".loadingGldJual").html("");
      if (amountType === "currency") {
        console.log("error calculate gold for selling gold, from rupiah to gram");
      } else {
        console.log("error calculate gold for selling gold, from gram to rupiah");
      }
      console.log(res);
    }
  });
}

function nilaiRpBeli() {
  calculateGldBeli("currency", "desktop");
}

function nilaiGrBeli() {
  calculateGldBeli("gold", "desktop");
}

function mnilaiRpBeli() {
  calculateGldBeli("currency", "mobile");
}

function mnilaiGrBeli() {
  calculateGldBeli("gold", "mobile");
}

function calculateGldBeli(amountType, viewType) {
  $(".loadingGldBeli").html("Loading...");
  if (amountType === "currency") {
    if (viewType === "desktop") {
      var nilaiRpBeli = $("#valueRpBeli").val();
    }
    if (viewType === "mobile") {
      var nilaiRpBeli = $("#mvalueRpBeli").val();
    }
    var ajaxData = {
      action: "ajax_calculate_emas",
      emasRpValue: nilaiRpBeli,
      emasGrValue: 0,
      typeCalculate: "buy",
      valueType: "currency"
    };
  } else {
    if (viewType === "desktop") {
      var nilaiGrBeli = $("#valueGrBeli").val();
    }
    if (viewType === "mobile") {
      var nilaiGrBeli = $("#mvalueGrBeli").val();
    }
    var ajaxData = {
      action: "ajax_calculate_emas",
      emasRpValue: 0,
      emasGrValue: nilaiGrBeli,
      typeCalculate: "buy",
      valueType: "gold"
    }
  }
  $.ajax({
    url: ajaxscript.ajaxurl,
    type: "POST",
    data: ajaxData,
    success: function (res) {
      var res = $.parseJSON(res);
      if (res.code === 200) {
        $(".loadingGldBeli").html("");
        $(".beliemassekarang").attr("disabled", false);
        $(".mbeliemassekarang").attr("disabled", false);
        var currency_value = parseFloat(res.data.currency).toFixed();
        $(".harga_beli_emas").val(res.data.buy_price);
        var formatter = new Intl.NumberFormat("id-ID");
        if (amountType === "currency") {
          var unit_value = parseFloat(res.data.unit).toFixed(4);
          unit_value = unit_value.replace(".", ",");
          if (viewType === "desktop") {
            $("#valueGrBeli").val(unit_value);
          }
          if (viewType === "mobile") {
            $("#mvalueGrBeli").val(unit_value);
          }
          $(".calculate_type").val("currency");
        } else {
          var unit_value = parseFloat(res.data.unit).toFixed(1);
          unit_value = unit_value.replace(".", ",");
          if (viewType === "desktop") {
            $("#valueRpBeli").val(formatter.format(currency_value));
          }
          if (viewType === "mobile") {
            $("#mvalueRpBeli").val(formatter.format(currency_value));
          }
          $(".calculate_type").val("gold");
        }
      } else {
        if (amountType === "currency") {
          $("#valueGrBeli").val("");
          $("#mvalueGrBeli").val("");
        } else {
          $("#valueRpBeli").val("");
          $("#mvalueRpBeli").val("");
        }
        $(".loadingGldBeli").html(res.message);
        $(".beliemassekarang").attr("disabled", true);
        $(".mbeliemassekarang").attr("disabled", true);
      }
    },
    error: function (res) {
      $(".loadingGldBeli").html("");
      if (amountType === "currency") {
        console.log("error calculate gold for buying gold, from rupiah to gram");
      } else {
        console.log("error calculate gold for buying gold, from gram to rupiah");
      }
      console.log(res);
    }
  });
}

function ajax_gld_cancel_buy(invoice) {
  $(".bx_gldLoading").css("display", "flex");
  var plant = document.getElementById('body');
  var hurl = plant.getAttribute('data-hurl'); 
  $.ajax({
    url: ajaxscript.ajaxurl,
    type: "POST",
    data: "invoice=" + invoice + "&action=ajax_gld_cancel_buy",
    success: function (res) {
      var obj = $.parseJSON(res);
      console.log("success cancel invoice");
      console.log(obj);
      $(".bx_gldLoading").css("display", "none");
      $(".bx_leBeliPopup").css("display", "none");
      $("body").css("overflow", "auto");
      $("html").css("overflow-x", "hidden");
      $("html").css("overflow-y", "auto");
      setTimeout(function () {
        location.replace(hurl);
      }, "fast");
    },
    error: function (res) {
      $(".bx_gldLoading").css("display", "none");
      $(".bx_leBeliPopup").css("display", "none");
      console.log("failed cancel invoice");
      console.log(res);
    }
  });
}
function ajax_gld_register(viewType) {
  if (viewType === "desktop") {
    var dataRegisterEmas = $("#registerEmas").serialize();
  }
  if (viewType === "mobile") {
    var dataRegisterEmas = $("#mregisterEmas").serialize();
  }
  $(".bx_gldLoading").css("display", "flex");
  $.ajax({
    url: ajaxscript.ajaxurl,
    type: "POST",
    data: dataRegisterEmas + "&action=ajax_gld_register",
    success: function (res) {
      var res = $.parseJSON(res);
      $(".bx_gldLoading").css("display", "none");
      if (res.code === 200) {
        if (viewType === "desktop") {
          $("#registerEmas").submit();
        }
        if (viewType === "mobile") {
          $("#mregisterEmas").submit();
        }
      } else {
        if (res.message == "Validation") {
          $.each(res.data, function (i, val) {
            $(".error_" + i).show().html(val);
          });
        } else {
          console.log(res.message);
        }
      }
    },
    error: function (res) {
      $(".bx_gldLoading").css("display", "none");
      console.log("error register ladara emas");
      console.log(res);
    }
  });
}
function ajax_sell_emas(viewType) {
  $(".bx_gldLoading").css("display", "flex");
  if (viewType === "mobile") {
    $("#mlecheckoutjualemas").submit();
  }
  if (viewType === "desktop") {
    $("#lecheckoutjualemas").submit();
  }
}

$(document).ready(function () {
  if ($("#gldBalanceRp").length > 0) {
    refreshGldBalance();
  }
  if ($("#gldRateBuy").length > 0 && $("#gldRateSell").length > 0) {
    refreshGldRate();
  }
  $(".page_emas").on("click", function (e) {
    let type = $(this).data("type");
    let page = $(this).attr("rel");
    let next = parseInt(page) + 1;
    let perpage = $(this).data("perpage");
    $(this).html("Loading...").attr("rel", next);
    $.ajax({
      url: ajaxscript.ajaxurl,
      type: "POST",
      data: "type=" + type + "&per_page=" + perpage + "&page=" + page + "&action=ajax_gld_load_history",
      success: function (res) {
        var obj = $.parseJSON(res);
        if (obj.code === 200) {
          if (obj.html != "") {
            $("#gld_history_" + type).before(obj.html).html("Transaksi Sebelumnya");
            e.stopImmediatePropagation();
          } else {
            $("#gld_history_" + type).html("Transaksi Sebelumnya").css("display", "none");
          }
        } else {
          $("#gld_history_" + type).html("Transaksi Sebelumnya").css("display", "none");
        }
      },
      error: function (res) {
        $("#gld_history_" + type).html("Transaksi Sebelumnya");
        console.log("error get history");
        console.log(res);
      }
    });
  });
  if ($("#valueRpBeli").length > 0) {
    var regExp = /[0-9\.\,]/;
    var typingTimer; /**timer identifier*/
    var doneTypingInterval = 1000; /**time in ms */
    $(".valueRpBeli").on("keyup", function (e) {
      let idRpBeli = $(this).attr("id");
      $("#valueGrBeli").val("");
      $("#mvalueGrBeli").val("");
      $(".beliemassekarang").attr("disabled", true);
      $(".mbeliemassekarang").attr("disabled", true);
      $(this).val(function (index, value) {
        return value.replace(/\D/g, "").replace(/\B(?=(\d{3})+(?!\d))/g, ".");
      });
      if ($(this).val()) {
        if (idRpBeli === "valueRpBeli") {
          typingTimer = setTimeout(nilaiRpBeli, doneTypingInterval);
        } else if (idRpBeli === "mvalueRpBeli") {
          typingTimer = setTimeout(mnilaiRpBeli, doneTypingInterval);
        }
        e.stopImmediatePropagation();
      }
    });
    $(".valueGrBeli").on("keyup", function (e) {
      let idGrBeli = $(this).attr("id");
      $("#valueRpBeli").val("");
      $("#mvalueRpBeli").val("");
      $(".beliemassekarang").attr("disabled", true);
      $(".mbeliemassekarang").attr("disabled", true);
      $(this).val(function (index, value) {
        if ($(this).val().length < 6) {
          return value.replace(/^0+/, '').replace(/\D/g, "").replace(/\B(?=(\d{4})+(?!\d))/g, ",");
        }
      });
      if ($(this).val()) {
        if ($(this).val().length < 7) {
          if (idGrBeli === "valueGrBeli") {
            typingTimer = setTimeout(nilaiGrBeli, doneTypingInterval);
          } else if (idGrBeli === "mvalueGrBeli") {
            typingTimer = setTimeout(mnilaiGrBeli, doneTypingInterval);
          }
          e.stopImmediatePropagation();
        }
      }
    });
    $(".valueRpJual").on("keyup", function (e) {
      let idRpJual = $(this).attr("id");
      $("#valueGrJual").val("");
      $("#mvalueGrJual").val("");
      $(".jualemassekarang").attr("disabled", true);
      $(".mjualemassekarang").attr("disabled", true);
      $(".jualemassekarang").attr("disabled", true);
      $(".mjualemassekarang").attr("disabled", true);
      $(this).val(function (index, value) {
        return value.replace(/\D/g, "").replace(/\B(?=(\d{3})+(?!\d))/g, ".");
      });
      if ($(this).val()) {
        if (idRpJual === "valueRpJual") {
          typingTimer = setTimeout(nilaiRpJual, doneTypingInterval);
        } else if (idRpJual === "mvalueRpJual") {
          typingTimer = setTimeout(mnilaiRpJual, doneTypingInterval);
        }
        e.stopImmediatePropagation();
      }
    });
    $(".valueGrJual").on("keyup", function (e) {
      let idGrJual = $(this).attr("id");
      $("#valueRpJual").val("");
      $("#mvalueRpJual").val("");
      $(".jualemassekarang").attr("disabled", true);
      $(".mjualemassekarang").attr("disabled", true);
      $(this).val(function (index, value) {
        if ($(this).val().length < 6) {
          return value.replace(/^0+/, '').replace(/\D/g, "").replace(/\B(?=(\d{4})+(?!\d))/g, ",");
        }
      })
      if ($(this).val()) {
        if ($(this).val().length < 6) {
          if (idGrJual === "valueGrJual") {
            typingTimer = setTimeout(nilaiGrJual, doneTypingInterval);
          } else if (idGrJual === "mvalueGrJual") {
            typingTimer = setTimeout(mnilaiGrJual, doneTypingInterval);
          }
          e.stopImmediatePropagation();
        }
      }
    });
    $(".gldCal").on("keyup keydown", function (e) {
      var value = String.fromCharCode(e.which) || e.key;
      if (!regExp.test(value) && e.which != 188 && e.which != 190 && e.which != 8 && e.which != 46 && (e.which < 37 || e.which > 40)) {
        e.preventDefault();
        return false;
      }
    });
    $(".gldCal").on("keydown", function (e) {
      clearTimeout(typingTimer);
    });
  }
});