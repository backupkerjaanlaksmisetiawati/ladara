//proses minify. function yang sudah fix harus di minify. use https://codebeautify.org/jsviewer
// NOTES: function atau event yg sama digabung
function preview_image(event, id) {
  var reader = new FileReader();
  reader.onload = function () {
    var output = document.getElementById(id);
    output.src = reader.result;
  }
  reader.readAsDataURL(event.target.files[0]);
}
function gldAktivasiBank(viewType) {
  $(".bx_gldLoading").css("display", "flex");
  if (viewType === "desktop") {
    $(".wrap_gldAktivasi").css("height", "665px");
    $("#aktivasiLE_foto_ktp").fadeOut(1000, function () {
      $("#aktivasiLE_bank").fadeIn(500, function () {
        $(".bx_gldLoading").css("display", "none");
      });
    });
  }
  if (viewType === "mobile") {
    window.scrollTo(0, 0);
    $(".wrap_mgldAktivasi").css("height", "865px");
    $("#maktivasigld_foto_ktp").fadeOut(1000, function () {
      $("#maktivasigld_bank").fadeIn(500, function () {
        $(".bx_gldLoading").css("display", "none");
      });
    });
  }
}
function verifikasiGldBuy(x) {
  window.scrollTo(0, 0);
  if (x === "showpopup") {
    $("#popUpBuyGld").css("display", "flex");
    $("body, html").css("overflow", "hidden");
  } else {
    $("#popUpBuyGld").css("display", "none");
    $("body").css("overflow", "auto");
    $("html").css("overflow-x", "hidden");
    $("html").css("overflow-y", "auto");
  }
}
function verifikasiGldSell(x) {
  window.scrollTo(0, 0);
  if (x === "showpopup") {
    $("#popUpSellGld").css("display", "flex");
    $("body, html").css("overflow", "hidden");
  } else {
    $("#popUpSellGld").css("display", "none");
    $("body").css("overflow", "auto");
    $("html").css("overflow-x", "hidden");
    $("html").css("overflow-y", "auto");
  }
}
function metodePembayaranGld(x) {
  window.scrollTo(0, 0);
  if (x === "showpopup") {
    $("#popUpPaymentGld").css("display", "flex");
    $("body, html").css("overflow", "hidden");
  } else if (x === "hidepopup") {
    $("#popUpPaymentGld").css("display", "none");
  } else {
    $("#popUpPaymentGld").css("display", "none");
    $("body").css("overflow", "auto");
    $("html").css("overflow-x", "hidden");
    $("html").css("overflow-y", "auto");
  }
}
function noHpEwalletGld(x, ewallet) {
  var plant = document.getElementById('body');
  var hurl = plant.getAttribute('data-hurl');
  var template_directory = hurl.replace("emas", "wp-content/themes/ladara2020/") + "library/images";
  window.scrollTo(0, 0);
  if (x === "showpopup") {
    $("#popUpNoHpEwalletGld").css("display", "flex");
    $(".namaNoHpEwalletGld").html(ewallet);
    if (ewallet === "LINKAJA") {
      $(".iconNoHpEwalletGld").attr("src", template_directory + "/emas/icon_ewallet_" + ewallet.toLowerCase() + "2.png");
    }
    if (ewallet === "OVO") {
      $(".iconNoHpEwalletGld").attr("src", template_directory + "/emas/icon_ewallet_" + ewallet.toLowerCase() + "2.png");
    }
  } else if (x === "closepopup") {
    $("#popUpNoHpEwalletGld").css("display", "none");
    $(".metodePembayaranGldEwallet").css("display", "none");
    $(".radio_pembayaran_emas[value=ewallet]").attr("checked", false);
    metodePembayaranGld("showpopup");
  } else {
    var popup_ewallet_phone = $("#popup_ewallet_phone").val();
    if (popup_ewallet_phone != "") {
      $("#popUpNoHpEwalletGld").css("display", "none");
      $(".ewallet_phone").val(popup_ewallet_phone);
      $("body").css("overflow", "auto");
      $("html").css("overflow-x", "hidden");
      $("html").css("overflow-y", "auto");
    } else {
      $(".error_beliemas_ewallet").html("No HP harus diisi.").css("display", "block");
    }
  }
}
function gld_upload_ktp() {
  $("#upload_ktp_LE_file").click();
  $("#upload_ktp_LE_file").change(function (e) {
    var fileName = e.target.files[0].name;
    $("#upload_ktp_LE .step_bottom").addClass("active");
    $("#upload_ktp_LE button").hide();
    $("#preview_ktp").css("display", "flex");
    $("#preview_ktp span").html(fileName);
    $("#upload_foto_LE button").removeAttr("disabled");
  });
}
function mgld_upload_ktp() {
  $("#mgld_upload_ktp_file").click();
  $("#mgld_upload_ktp_file").change(function (e) {
    var fileName = e.target.files[0].name;
    $("#mgld_upload_ktp button").hide();
    $("#mgld_preview_ktp").css("display", "flex");
    $("#mgld_preview_ktp span").html(fileName);
    $("#mgld_upload_foto button").removeAttr("disabled");
  });
}
function upload_selfie_emas() {
  $("#upload_foto_LE_file").click();
  $("#upload_foto_LE_file").change(function (e) {
    var fileName = e.target.files[0].name;
    $("#upload_foto_LE .step_bottom").addClass("active");
    $("#lanjut_regis_LE .step_bottom").addClass("active");
    $("#upload_foto_LE button").hide();
    $("#preview_selfie").css("display", "flex");
    $("#preview_selfie span").html(fileName);
    $("#lanjut_regis_LE button").removeAttr("disabled");
  });
}
function mgld_upload_selfie() {
  $("#mgld_upload_foto_file").click();
  $("#mgld_upload_foto_file").change(function (e) {
    var fileName = e.target.files[0].name;
    $("#mgld_upload_foto button").hide();
    $("#mgld_preview_selfie").css("display", "flex");
    $("#mgld_preview_selfie span").html(fileName);
    $("#mgld_lanjut_regis button").removeAttr("disabled");
  });
}
function showGldTutorialPayment(rel) {
  $('.tutorial_paymentEmas .step > div').removeClass('active').hide();
  $('.tutorial_paymentEmas .step').removeClass('active');
  $('.tutorial_paymentEmas .step > div.step_' + rel).addClass('active').show();
  $('.tutorial_paymentEmas .step.box_step_' + rel).addClass('active');
}
function show_detailHistoryEmas(rel) {
  if ($('.col_heDetailTransaksi .jmlh.' + rel).hasClass('active')) {
    $('.col_heDetailTransaksi .jmlh').removeClass('active');
    $('.col_heDetailTransaksi .title').removeClass('active');
  } else {
    $('.col_heDetailTransaksi .jmlh').removeClass('active');
    $('.col_heDetailTransaksi .jmlh.' + rel).addClass('active');
    $('.col_heDetailTransaksi .title').removeClass('active');
    $('.col_heDetailTransaksi .title.' + rel).addClass('active');
  }
}
function afterGldPageLoad() {
  if ($("#gldLoading").length > 0) {
    $("#gldLoading").fadeOut(function () {
      $("#menuGld").fadeIn(function () {
        if ($("#gldHistoryTabs").length > 0) {
          $("#gldHistoryTabs").fadeIn();
          $("#mgldHistoryTabs").fadeIn();
        }
        if ($("#gldBalance").length > 0) {
          $("#mgldBalance").fadeIn();
          $("#gldBalance").fadeIn(function () {
            if ($("#gldRateTabs").length > 0) {
              $("#gldRateTabs").fadeIn();
              $("#mgldRateTabs").fadeIn();
            }
          });
        }
      });
    });
  }
}
function gldAlertBack(x) {
  if (x === "showpopup") {
    $(".bx_leBeliPopup").css("display", "flex");
    $("body, html").css("overflow", "hidden");
    window.scrollTo(0, 0);
  } else {
    $(".bx_leBeliPopup").css("display", "none");
    $("body").css("overflow", "auto");
    $("html").css("overflow-x", "hidden");
    $("html").css("overflow-y", "auto");
  }
}
function copyVirtualAccount() {
  copyText = document.getElementById("noVirtualAccount");
  copyText.select();
  copyText.setSelectionRange(0, 99999); /*For mobile devices*/
  document.execCommand("copy");
  console.log("Copied the text: " + copyText.value);
}
function format_va_emas(opt) {
  if (!opt.id) {
    return opt.text.toUpperCase();
  }

  var optimage = $(opt.element).attr('data-image');
  console.log(optimage)
  if (!optimage) {
    return opt.text.toUpperCase();
  } else {
    var $opt = $(
      '<span class="pembayaran-emas"><label><img src="' + optimage + '" /></label> ' + opt.text.toUpperCase() + '</span>'
    );
    return $opt;
  }
};
$(document).ready(function () {
  var plant = document.getElementById('body');
  var hurl = plant.getAttribute('data-hurl');
  var template_directory = hurl.replace("emas", "wp-content/themes/ladara2020/") + "library/images";
  if ($("#LadaraEmas").length > 0) {
    if ($(".jsselect_le").length > 0) {
      $(".jsselect_le").select2();
    }
    if ($(".jsselect_le_hide_search").length > 0) {
      $(".jsselect_le_hide_search").select2({
        minimumResultsForSearch: Infinity
      });
    }
  }
  if ($("#gldHistoryTabs").length > 0) {
    $("#gldHistoryTabs").tabs();
    $("#mgldHistoryTabs").tabs();
  }
  $("#btnGldMenu").on("click", function (e) {
    if ($(".gldMenu").hasClass("active")) {
      /*$("body").css({ "overflow": "auto" });*/
      $(".gldMenu").removeClass("active");
      $("#container.ladaraEmas").removeClass("hid");
    } else {
      /*$("body").css({"overflow":"hidden"});*/
      $(".gldMenu").addClass("active");
      $("#container.ladaraEmas").addClass("hid");
    }
    e.stopImmediatePropagation();
  });
  $("#popUpPaymentGld ul li").on("click", function (e) {
    if (!$(this).hasClass("disabled")) {
      $("body, html").css("overflow", "hidden");
      var va_payment = "",
        ewallet_payment = "",
        retail_payment = "";
      var type_payment = $(this).attr("data-typepayment");
      $(".metodePembayaranGld").css("display", "block");
      $(".metodePembayaranGld > div > div").css("display", "none");
      $(".radio_pembayaran_emas[value=" + type_payment + "]").attr("checked", true);
      if (type_payment === "va") {
        va_payment = $(this).attr("data-va");
        $(".metodePembayaranGldVa").css("display", "block");
        $(".dropDownVaEmas option[value='" + va_payment + "']").attr("selected", true);
        $(".selectedVaEmas").html("<span><img src=\"" + template_directory + "/emas/icon_va_" + va_payment.toLowerCase() + "2.png\" /></span><span>" + va_payment + " Virtual Account</span>");
        metodePembayaranGld("closepopup");
      }
      if (type_payment === "ewallet") {
        ewallet_payment = $(this).attr("data-ewallet");
        $(".metodePembayaranGldEwallet").css("display", "block");
        $(".dropDownEwalletEmas option[value='" + ewallet_payment + "']").attr("selected", true);
        $(".selectedEwalletEmas").html("<span><img src=\"" + template_directory + "/emas/icon_ewallet_" + ewallet_payment.toLowerCase() + "2.png\" /></span><span>" + ewallet_payment + "</span>");
        if (ewallet_payment === "OVO" || ewallet_payment === "LINKAJA") {
          noHpEwalletGld("showpopup", ewallet_payment);
          metodePembayaranGld("hidepopup");
        } else {
          metodePembayaranGld("closepopup");
        }
      }
      if (type_payment === "retail") {
        retail_payment = $(this).attr("data-retail");
        $(".metodePembayaranGldRetail").css("display", "block");
        $(".dropDownRetailEmas option[value='" + retail_payment + "']").attr("selected", true);
        $(".selectedRetailEmas").html("<span><img src=\"" + template_directory + "/emas/icon_retail_" + retail_payment.toLowerCase() + "2.png\" /></span><span>" + retail_payment + "</span>");
        metodePembayaranGld("closepopup");
      }
      if (type_payment === "cc") {
        $(".metodePembayaranGldCc").css("display", "block");
        metodePembayaranGld("closepopup");
      }
      $(".btnMetodePembayaranGld").html("Ubah Metode Pembayaran");
      $(".btnVerifikasiGldBuy").attr("disabled", false);
      e.stopImmediatePropagation();      
    }
  });
  if ($(".gldHeader").length > 0) {
    window.onscroll = function () {
      gldHeaderScroll()
    };
    var gldHeader = $(".gldHeader");
    var gldSticky = gldHeader.offset();
    gldSticky = gldSticky.top;
    function gldHeaderScroll() {
      if (window.pageYOffset > gldSticky) {
        gldHeader.addClass("sticky");
      } else {
        gldHeader.removeClass("sticky");
      }
    }
  }
  if ($("#gldOSec").length > 0) {
    setInterval(function () {
      var seconds = new Date().getSeconds();
      $("#gldOSec").html((seconds < 10 ? "0" : "") + seconds);
    }, 1000);
    setInterval(function () {
      var minutes = new Date().getMinutes();
      $("#gldOMin").html((minutes < 10 ? "0" : "") + minutes);
    }, 1000);
    setInterval(function () {
      var hours = new Date().getHours();
      $("#gldOHours").html((hours < 10 ? "0" : "") + hours);
    }, 1000);
  }
});