$("#formKategori").submit(function(e) {
    e.preventDefault(); // avoid to execute the actual submit of the form.
    var formData = new FormData(this);
    var url = $(this).attr('action');

    $.ajax({
        type: "POST",
        url: url,
        data: formData, // serializes the form's elements.
        xhrFields: {
            withCredentials: true
        },
        success: function(data)
        {
            alert(data.message)
            $('#formKategori')[0].reset();
        },
        cache: false,
        contentType: false,
        processData: false
    });
});

$("#formKategori-edit").submit(function(e) {
    e.preventDefault(); // avoid to execute the actual submit of the form.
    var formData = new FormData(this);
    var url = $(this).attr('action');

    $.ajax({
        type: "POST",
        url: url,
        data: formData, // serializes the form's elements.
        xhrFields: {
            withCredentials: true
        },
        success: function(data)
        {
            alert(data.message)
            location.href = '/dashboard/admin-kategori';
        },
        cache: false,
        contentType: false,
        processData: false
    });
});

$('.btn-hapus-cate').click(function (e){
    e.preventDefault();
    var id = $(this).data('id')
    if (confirm('Apakah anda ingin menghapus kategori ini?')) {
        $(this).val('Loading...')
        $(this).prop('disabled', true)
        var url = $(this).attr('href')

        $.ajax({
            type: "DELETE",
            url: url+'/'+id,
            xhrFields: {
                withCredentials: true
            },
            success: function(data)
            {
                alert(data.message)
                location.reload(true)
            },
            cache: false,
            contentType: false,
            processData: false
        });
    }
})

$(".aj-ulasan-start").hover(function(){
    const rating = $(this).data('rating');
    $("#rating").val(rating)
    $( ".aj-ulasan-start" ).each(function( index ) {
        if (index < rating){
            $( this ).removeClass('glyphicon-star-empty').addClass('glyphicon-star');
        }else{
            $( this ).removeClass('glyphicon-star').addClass('glyphicon-star-empty');
        }
    });
},
function (){
    const rating = $("#rating").val()
    $( ".aj-ulasan-start" ).each(function( index ) {
        if (index < rating){
            $( this ).removeClass('glyphicon-star-empty').addClass('glyphicon-star');
        }else{
            $( this ).removeClass('glyphicon-star').addClass('glyphicon-star-empty');
        }
    });
});


$("#form-ulasan").submit(function(e) {
    e.preventDefault(); // avoid to execute the actual submit of the form.
    var formData = new FormData(this);
    $('.fix_loading').fadeIn()
    $.ajax({
        type: "POST",
        url: ajaxscript.ajaxurl+'?action=ajaxUlasan',
        data: formData, // serializes the form's elements.
        success: function(data)
        {
            const resp = JSON.parse(data);
            $('.ulasan-error').removeClass('show').addClass('hide');
            $('.ulasan-success').removeClass('hide').addClass('show').html(resp.message);
            location.href = '/my-order';
        },
        error: function (errors){
            const err = JSON.parse(errors.responseText);
            let errTxt= '';
            for (const errs in err.errors) {
                for (const value of err.errors[errs]) {
                    errTxt += value+ '<br/>'
                }
            }
            $('.ulasan-success').removeClass('show').addClass('hide');
            $('.ulasan-error').removeClass('hide').addClass('show').html(errTxt)
        },
        cache: false,
        contentType: false,
        processData: false
    });
    $('.fix_loading').hide()

});

$(".ulasan-images").change(function() {
    readURL(this);
});

function readURL(input) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();

        reader.onload = function(e) {
            // $('#blah').attr('src', e.target.result);
            // $('.img-preview').css('background-image', 'url('+e.target.result +')');
            // $('.'+$(input).parent().prop('className')+' .img-preview').show()
            $(input).parent().find('.div-preview .img-preview').attr('src', e.target.result);
            $(input).parent().find('.icon').hide()
            $(input).parent().find('.div-preview').show()
        }

        reader.readAsDataURL(input.files[0]); // convert to base64 string
    }
}