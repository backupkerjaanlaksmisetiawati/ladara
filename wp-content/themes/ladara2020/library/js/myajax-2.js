function ajax_ladara_emas_status_jual(i) {
  $('#ajax_ladara_emas_status_jual').html('Loading...').attr('disabled', true);
  $("#dashboard_alert").hide();
  $.ajax({
    url: ajaxscript.ajaxurl,
    type: 'POST',
    data: {
      action: 'ajax_gld_status_jual',
      invoice: i,
    },
    success: function (res) {
      var obj = $.parseJSON(res);
      window.scrollTo(0, 0);
      $("#ajax_ladara_emas_status_jual").html("Cek Status Jual").attr('disabled', false);
      if (obj.code === 200) {
        $("#dashboard_alert").show().addClass("alert-success");
      }
      if (obj.code !== 200) {
        $("#dashboard_alert").show().addClass("alert-danger");
      }
      $("#dashboard_alert label").html(obj.message);
      if (obj.code === 200) {
        setTimeout(function () {
          var plant = document.getElementById('body');
          var hurl = plant.getAttribute('data-hurl');
          location.replace(hurl + '/dashboard/ladara-emas-transaction/emas-transaction-detail?id=' + i);
        }, 5000);
      }
    },
    error: function (res) {
      console.log(res);
      window.scrollTo(0, 0);
      $("#ajax_ladara_emas_status_jual").html("Cek Status Jual").attr('disabled', false);
      $("#dashboard_alert label").html("Gagal mengecek status penjualan. Silahkan coba lagi beberapa saat lagi.");
      $("#dashboard_alert").show().addClass("alert-danger");
    }
  })
}

function ajax_ladara_emas_check_user_verifikasi(i) {
  $("#ajax_ladara_emas_check_user_verifikasi").html("Loading...").attr('disabled', true);
  $("#dashboard_alert").hide();
  $.ajax({
    url: ajaxscript.ajaxurl,
    type: "POST",
    data: "user_id=" + i + "&action=ajax_gld_user_verifikasi",
    success: function (res) {
      var obj = $.parseJSON(res);
      window.scrollTo(0, 0);
      $("#ajax_ladara_emas_check_user_verifikasi").html("Check verifikasi").attr('disabled', false);
      if (obj.code === 200) {
        $("#dashboard_alert").show().addClass("alert-success");
      }
      if (obj.code !== 200) {
        $("#dashboard_alert").show().addClass("alert-danger");
      }
      $("#dashboard_alert label").html(obj.message);
      if (obj.code === 200) {
        setTimeout(function () {
          var plant = document.getElementById('body');
          var hurl = plant.getAttribute('data-hurl');
          location.replace(hurl + '/dashboard/ladara-emas-user/user-emas-detail?id=' + i);
        }, 5000);
      }
    },
    error: function (res) {
      console.log(res);
      window.scrollTo(0, 0);
      $("#ajax_ladara_emas_check_user_verifikasi").html("Check verifikasi").attr('disabled', false);
      $("#dashboard_alert label").html("Gagal mengecek status verifikasi. Silahkan coba lagi beberapa saat lagi.");
      $("#dashboard_alert").show().addClass("alert-danger");
    }
  });
}

function ajax_ladara_emas_retrieve_payment(i, u) {
  $("#ajax_ladara_emas_retrieve_payment").html("Loading...").attr('disabled', true);
  $("#dashboard_alert").hide();
  $.ajax({
    url: ajaxscript.ajaxurl,
    type: "POST",
    data: {
      action: 'ajax_gld_retrieve_payment',
      invoice: i,
      user: u
    },
    success: function (res) {
      var obj = $.parseJSON(res);
      window.scrollTo(0, 0);
      $("#ajax_ladara_emas_retrieve_payment").html("Retrieve Payment*").attr('disabled', false);
      if (obj.code === 200) {
        $("#dashboard_alert").show().addClass("alert-success");
      }
      if (obj.code !== 200) {
        $("#dashboard_alert").show().addClass("alert-danger");
      }
      $("#dashboard_alert label").html(obj.message);
      if (obj.code === 200) {
        setTimeout(function () {
          var plant = document.getElementById('body');
          var hurl = plant.getAttribute('data-hurl');
          location.replace(hurl + '/dashboard/ladara-emas-transaction/emas-transaction-detail?id=' + i);
        }, 5000);
      }
    },
    error: function (res) {
      console.log(res);
      window.scrollTo(0, 0);
      $("#ajax_ladara_emas_retrieve_payment").html("Retrieve Payment*").attr('disabled', false);
      $("#dashboard_alert label").html("Gagal melakukan retrieve payment. Silahkan coba lagi beberapa saat lagi.");
      $("#dashboard_alert").show().addClass("alert-danger");
    }
  });
}

function popupVoucher(x) {
  window.scrollTo(0, 0)
  if (x === 'showpopup') {
    $('#popupVoucher').css('display', 'flex')
    $('body, html').css('overflow', 'hidden')
  } else {
    $('#popupVoucher').css('display', 'none')
    $('body').css('overflow', 'auto')
    $('html').css('overflow-x', 'hidden')
    $('html').css('overflow-y', 'auto')
  }
}

function popupVoucherDetail(x, c) {
  window.scrollTo(0, 0)
  if (x === 'showpopup') {
    $('.voucher_detail_' + c).html('Loading...')
    $.ajax({
      url: ajaxscript.ajaxurl,
      type: 'POST',
      data: {
        action: 'ajax_voucherDetail',
        voucher_code: c,
      },
      success: function (result) {
        var obj = $.parseJSON(result)
        $('.voucher_detail_' + c).html('Lihat Detail')
        $('#popupVoucherDetail').css('display', 'flex').html(obj.html)
        $('#popupVoucher').css('display', 'none')
      },
    })
  } else {
    $('.voucher_detail_' + c).html('Lihat Detail')
    $('#popupVoucherDetail').css('display', 'none')
    $('#popupVoucher').css('display', 'flex')
  }
}

function selectVoucherDetail(voucherCode, voucherId) {
  $('.vouched-cd').removeClass('selected')
  $('.cd-' + voucherCode).addClass('selected')
  $('#voucherCode').val(voucherCode)
  $('#voucher-id').val(voucherId)
  $('#btnVoucherCode').attr('disabled', false).removeClass('disabled')
  $('.alert-voucher').hide().html('')
}

function searchVoucher() {
  let voucherCode = $("#voucherCode").val();
  $('#btnVoucherCode').html('Loading...')

  if (voucherCode == '') {
    $('.alert-voucher')
      .show()
      .html('Kamu harus memilih atau memasukan kode voucher!')
    $('#btnVoucherCode').html('Terapkan')
  } else {
    $('.alert-voucher').hide().html('')
    $.ajax({
      url: ajaxscript.ajaxurl,
      type: 'POST',
      data: {
        action: 'ajax_searchVoucher',
        voucher_code: voucherCode
      },
      statusCode: {
        200: function (xhr) {
          $('#voucher-id').val(xhr)

          applyVoucher()
        },
        404: function (xhr) {
          let obj = JSON.parse(xhr.responseText)

          $('.alert-voucher').show().html(obj.message)
          $('#btnVoucherCode').html('Terapkan')
        },
        422: function (xhr) {
          let obj = JSON.parse(xhr.responseText)

          $('.alert-voucher').show().html(obj.message)
          $('#btnVoucherCode').html('Terapkan')
        }
      },
    })
  }
}

function applyVoucher() {
  let id = $('#voucher-id').val()
  $('#btnVoucherCode').html('Loading...')

  if (id == '') {
    $('.alert-voucher')
      .show()
      .html('Kamu harus memilih atau memasukan kode voucher!')
    $('#btnVoucherCode').html('Terapkan')
  } else {
    $('.alert-voucher').hide().html('')
    $.ajax({
      url: ajaxscript.ajaxurl,
      type: 'POST',
      data: {
        action: 'ajax_checkVoucher',
        voucher_id: id,
      },
      statusCode: {
        200: function (xhr) {
          let obj = JSON.parse(xhr)
          let amount = obj.data.amount

          if (obj.data.type === 1) {
            amount = Number(obj.data.amount) * Number(obj.data.max) / 100
          }

          $('.rv_discounttype').val(obj.data.type)
          $('.rv_discountamount').val(obj.data.amount)
          $('.rv_discountmax').val(obj.data.max)
          $('.rv_discountfreeshipping').val(obj.data.free_shipping)

          $('.alert-voucher').hide().html('')
          $('#popupVoucher').css('display', 'none')

          $('.rv_priceTotalWithDiscount').closest('div').hide()
          $('.rv_shippingTotalWithDiscount').closest('div').hide()
          $('.rv_pricetotal').removeClass('line-through')
          $('.rv_shippingtotal').removeClass('line-through')

          $('body').css('overflow', 'auto')
          $('html').css('overflow-x', 'hidden')
          $('html').css('overflow-y', 'auto')
          $('#txtVoucher').html(
            'Kamu hemat <strong id="text-amount-voucher">Rp ' +
            0 +
            '</strong><br /><span> Kode voucher: ' +
            obj.data.code +
            '</span>'
          )
          $('.rv_priceTotalWithDiscount')
            .show()
            .html('Rp ' + obj.amount + '')
          $('#btnVoucherCode').html('Terapkan')

          if (!obj.data.free_shipping) {
            $('.rv_pricetotal').addClass('line-through')
          } else {
            $('.rv_shippingtotal').addClass('line-through')
          }

          $('.sel_btc_shipping').change()
        },
        422: function (xhr) {
          let obj = JSON.parse(xhr.responseText)

          $('.alert-voucher').show().html(obj.message)
          $('#btnVoucherCode').html('Terapkan')
        },
        500: function (xhr) {
          location.replace(hurl + '/cart/')
        }
      },
    })
  }
}

$(document).ready(function () {
  $("#dashboard_alert button").on('click', function () {
    $('#dashboard_alert').hide();
    $('#dashboard_alert label').html('');
  })

  if ($('#voucherCode').length > 0) {
    $('#voucherCode').on('change', function () {
      $('#btnVoucherCode').attr('disabled', false).removeClass('disabled')
      $('.alert-voucher').hide().html('')
    })
  }

  $(".row_topup > div").on('click', function (e) {
    e.preventDefault();
    var classDiv = $(this).attr("id");
    $(".row_topup > div").removeClass("active");
    $(this).addClass("active");
    $(".row_formTopup > form").fadeOut("fast").removeClass("active");
    $(".row_formTopup > form." + classDiv).addClass("active").fadeIn(500);
  })
})

// add 14/10/2020
var plant = document.getElementById('body')
var hurl = plant.getAttribute('data-hurl')

function form_filterReport(e) {
    e.preventDefault();

      var sel_orderstatus = $('select[name="sel_orderstatus"] option:selected').val();
      var from_date = $('input[name="from_date"]').val();
      var to_date = $('input[name="to_date"]').val();
      
      window.location=hurl+'/so-report/?so='+sel_orderstatus+'&from_date='+from_date+'&to_date='+to_date;
    
}
function form_filterReport_old(e) {
    e.preventDefault();

      var from_date = $('input[name="from_date"]').val();
      var to_date = $('input[name="to_date"]').val();
      
      window.location=hurl+'/so-report-old/?from_date='+from_date+'&to_date='+to_date;
    
}

function form_productReport(e) {
    e.preventDefault();

      var sel_category = $('select[name="sel_category"] option:selected').val();
      var sel_merchant = $('select[name="sel_merchant"] option:selected').val();
      var from_date = $('input[name="from_date"]').val();
      var to_date = $('input[name="to_date"]').val();
      
      window.location=hurl+'/daftar-produk/?cat='+sel_category+'&merchant='+sel_merchant+'&from_date='+from_date+'&to_date='+to_date;
    
}

$('.open_abtus').click(function(){


  if($(this).hasClass('act')){
      $(this).removeClass('act');
      $(this).find('span').removeClass('glyphicon-menu-up').addClass('glyphicon-menu-down');
      $(this).parent().find('.cont_abtus').removeClass('act');
  }else{
      $(this).addClass('act');
      $(this).find('span').removeClass('glyphicon-menu-down').addClass('glyphicon-menu-up');
      $(this).parent().find('.cont_abtus').addClass('act');
  }

});

$('#open_download_apps').click(function(){
    $('#box_hd_download_apps').toggle("medium");
});


$('.open_faq').click(function(){


  if($(this).hasClass('act')){
      $(this).removeClass('act');
      $(this).find('span').removeClass('glyphicon-menu-up').addClass('glyphicon-menu-down');
      $(this).find('.child_qna_cat').removeClass('act');
  }else{
      $(this).addClass('act');
      $(this).find('span').removeClass('glyphicon-menu-down').addClass('glyphicon-menu-up');
      $(this).find('.child_qna_cat').addClass('act');
  }

});