// jquery script disini....
function validateEmail($email) {
  var emailReg = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/
  return emailReg.test($email)
}

function validate_email(e) {
  $this = $(e)
  $email_val = $(e).val()
  emailReg = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/
  $email = emailReg.test($email_val)
  if ($email == false) {
    $('.error_email').removeClass('hide')
  } else {
    $('.error_email').addClass('hide')
  }
}

function addCommas(nStr) {
  nStr += ''
  x = nStr.split('.')
  x1 = x[0]
  x2 = x.length > 1 ? '.' + x[1] : ''
  var rgx = /(\d+)(\d{3})/
  while (rgx.test(x1)) {
    x1 = x1.replace(rgx, '$1' + ',' + '$2')
  }
  return x1 + x2
}

$('input.onlyphone').keyup(function () {
  var nilai = $(this).val()
  var hasil = nilai.replace(/[\D\s\._\-]+/g, '')
  if (nilai == '') {
    // $(this).val("08")
  } else {
    $(this).val(hasil)
  }
})

$('input.onlynumber').keyup(function () {
  var nilai = $(this).val()
  var hasil = nilai.replace(/[\D\s\._\-]+/g, '')
  if (nilai == '') {
    // $(this).val("08")
  } else {
    $(this).val(hasil)
  }
})

$('input#minrange').keyup(function () {
  var min_range = $(this).val()
  if (min_range == '') {
    $('#maxrange').val('')
  } else {
    var total = parseInt(min_range) + 100000
    $('#maxrange').val(total)
  }
})

function alphanumeric(inputtxt) {
  var letters = /^(?=.*[0-9])(?=.*[a-zA-Z])([a-zA-Z0-9]+)$/

  if (inputtxt.match(letters)) {
    return 'true'
  } else {
    return 'false'
  }
}

$(document).ready(function () {
  if ($('#dataTable').length > 0) {
    $('#dataTable').DataTable()
  }

  if ($('.dataTable').length > 0) {
    $('.dataTable').DataTable()
  }

  if ($('.jsselect').length > 0) {
    $('.jsselect').select2()
  }

  if ($('.jsselectHideSearch').length > 0) {
    $('.jsselectHideSearch').select2({
      minimumResultsForSearch: -1
    });
  }

  if ($('.jsselect2').length > 0) {
    $('.jsselect2').select2()
  }

  if ($('.jsselect3').length > 0) {
    $('.jsselect3').select2()
  }

  if ($('.jsselect4').length > 0) {
    $('.jsselect4').select2()
  }

  if ($('.merchant_postcode_search').length > 0) {
    $('.merchant_postcode_search').select2({
      placeholder: 'Kode Pos'
    }).change(function() {
      $.ajax({
        url: `${ajaxscript.ajaxurl}?action=ajaxSearchLocationMerchant`,
        data: {
          postal_code: $('.merchant_postcode_search').val()
        },
        success: function (result) {
          let location = JSON.parse(result)

          $('.merchant-register-form [name="province_id"]').val(location.province_id)
          $('.merchant-register-form [name="city_id"]').val(location.city_id)
          $('.merchant-register-form [name="area_id"]').val(location.area_id)
        },
      })
    })

    let url = `${ajaxscript.ajaxurl}?action=ajaxSearchKotaMerchant`

    $('.merchant_city_search')
      .select2({
        ajax: {
          url: url,
          dataType: 'json',
          cache: true,
          data: function (term) {
            return {
              term: term,
            }
          },
          processResults: function (data) {
            return {
              results: $.map(data, function (item) {
                return {
                  text: item.label,
                  id: item.value,
                }
              }),
            }
          },
        },
        placeholder: 'Kecamatan',
        minimumInputLength: 3,
      })
      .change(function () {
        $('.merchant_postcode_search').val(null).trigger('change').empty()

        //set
        setMapByCity($('.merchant_city_search').text())
        //end

        let selectedCity = $('.merchant_city_search').val()
        let suburbId = selectedCity.split('|')[0]
        let url = `${ajaxscript.ajaxurl}?action=ajaxSearchKodePosMerchant`

        $.ajax({
          url: url,
          data: {
            suburb: suburbId
          },
          success: function (result) {
            let areasData = JSON.parse(result)

            if (areasData.length > 0) {
              areasData.forEach(data => {
                let newOption = new Option(data.label, data.value, false, false);
                $('.merchant_postcode_search').append(newOption).trigger('change');
              })
            }
          },
        })
      })
  }

  if ($('.lazy').length > 0) {
    $('.lazy').lazy({
      placeholder:
        'https://ladara.id/wp-content/themes/ladara2020/library/images/default_sorry.jpg',
    })
  }

  if ($('#myHeaderDesktop').length > 0 && $('#myHeaderMobile').length > 0) {
    window.onscroll = function () {
      myFunction()
    }

    var header = document.getElementById('myHeaderDesktop')
    var headerMobile = document.getElementById('myHeaderMobile')
    var sticky = header.offsetTop
    // console.log(sticky);

    function myFunction() {
      // console.log(window.pageYOffset);
      if (window.pageYOffset > sticky) {
        $('.header_list_tops').hide()
        header.classList.add('sticky')
        headerMobile.classList.add('sticky')
      } else {
        $('.header_list_tops').fadeIn()
        header.classList.remove('sticky')
        headerMobile.classList.remove('sticky')

        // hide profile menu popup
        // $('.fix_profileMenu').removeClass('open')
        // $('.fix_topnotif').removeClass('open')
        // hide cart popup
        // $('.fix_cartMenu').removeClass('open')
        // $('.fix_catMenu').removeClass('open')
      }
    }
  }

  $('.kecamatan-profile').change(function (){
    let kecamatan = $(this).children("option:selected").text();
    let kota = $('.kota-profile').children("option:selected").text();
    setMapByCity(kecamatan+','+kota)
  })

  function setMapByCity(address){
    map.setZoom(15)
    const geocoder = new google.maps.Geocoder();
    geocodeAddress(geocoder, map, address)
  }

  function geocodeAddress(geocoder, resultsMap, address) {
    geocoder.geocode({ address: address }, (results, status) => {
      if (status === "OK") {
        resultsMap.setCenter(results[0].geometry.location);
        markers.push(
            new google.maps.Marker({
              map: resultsMap,
              position: results[0].geometry.location,
            })
        );
        placeMarker(results[0].geometry.location, map)
      } else {
        alert("Geocode was not successful for the following reason: " + status);
      }
    });
  }


})
