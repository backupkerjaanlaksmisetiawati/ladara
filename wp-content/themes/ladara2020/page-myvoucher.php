<?php
/*
Template Name: My Voucher Page
*/
?>

<?php get_header(); ?>
<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
<?php 
date_default_timezone_set("Asia/Jakarta");
$nowdate = date('Y-m-d');
$now_time = strtotime($nowdate);

$current_user = wp_get_current_user();
$u_id = $current_user->ID;
$user_login = $current_user->user_login;
$user_avatar = get_field('user_avatar', 'user_'.$u_id);
$mile_reward = get_field('mile_reward', 'user_'.$u_id);
$nama_lengkap = get_field('nama_lengkap', 'user_'.$u_id);
$tanggal_lahir = get_field('tanggal_lahir', 'user_'.$u_id);
if(isset($tanggal_lahir)){
	$dob_data = explode('-', $tanggal_lahir);
	$dob_year = $dob_data[0];
	$dob_month = $dob_data[1];
	$dob_day = $dob_data[2];
}
$telepon = get_field('telepon', 'user_'.$u_id);
$jenis_kelamin = get_field('jenis_kelamin', 'user_'.$u_id);
// $status_afilasi = get_field('status_afilasi', 'user_'.$u_id);

if(isset($u_id) AND $u_id != 0){
?>

<div id="voucherpage" class="row row_profile">
	<div class="col-md-3 col_profile">
		
		<?php get_template_part( 'content', 'menu-profile' ); ?>

	</div>
	<div class="col-md-9 col_profile">
		
		<div class="row row_cont_tab_profile">
			<h1 class="ht_profile ht_myvoucher">Semua Voucherku</h1>

			<div class="row">
				<?php 
					global $wpdb;
					$wpdb_query = "SELECT * FROM mlm_voucher WHERE user_id = '$u_id' AND used = '0' ";
					$res_query = $wpdb->get_results($wpdb_query, OBJECT);
					$count_res = count($res_query);

					if($count_res > 0){ // if not exist with same user_id

						foreach ($res_query as $key => $value) {
							
							$voucher_id = $value->voucher_id;
							$voucher_name = get_the_title($voucher_id);
							$short_name = get_the_title($voucher_id);
							if(strlen($short_name) > 45) $short_name = substr($short_name, 0, 45).'...';
							$voucher_link = get_the_permalink($voucher_id);
							$mile_coupon = get_field('mile_coupon',$voucher_id);
							$thumb = wp_get_attachment_image_src( get_post_thumbnail_id($voucher_id), 'medium' );
							if($thumb){
								$urlphoto = $thumb['0'];
							}else{
								$urlphoto = get_template_directory_uri().'/library/images/sorry.png';
							}
							$alt = get_post_meta(get_post_thumbnail_id($voucher_id), '_wp_attachment_image_alt', true);
							// if(count($alt));

							$start_coupon = get_field('start_coupon',$voucher_id);
							$start_textdate = date("d-m-Y", strtotime($start_coupon));
							$str_startdate = strtotime( date("Y-m-d", strtotime($start_coupon)) );
							
							$end_coupon = get_field('end_coupon',$voucher_id);
							$str_enddate = strtotime( date("Y-m-d", strtotime($end_coupon)) );
							
							if($now_time <= $str_startdate){
							?>
								<div class="col-md-4 col_list_voc">
									<div class="box_list_voc box_myVoucher">
										<div class="mg_list_voc">
											<img src="<?php echo $urlphoto; ?>" alt="<?php echo $alt; ?>">
										</div>
										<h4 class="ht_list_voc"><?php echo $voucher_name; ?></h4>
										<a class="a_useVoucher segera" href="#segera" title="Belanja sekarang dan gunakan voucher ini!">Segera : <?php echo $start_textdate; ?></a>
										<a href="<?php echo $voucher_link; ?>" title="Lihat voucher ini.">
											<span class="span_ambilvoucher">Lihat s&k</span>
										</a>
									</div>
								</div>
							<?php
							}else if($now_time > $str_startdate AND $now_time < $str_enddate){
							?>
								<div class="col-md-4 col_list_voc">
									<div class="box_list_voc box_myVoucher">
										<div class="mg_list_voc">
											<img src="<?php echo $urlphoto; ?>" alt="<?php echo $alt; ?>">
										</div>
										<h4 class="ht_list_voc"><?php echo $voucher_name; ?></h4>
										<a class="a_useVoucher " href="<?php echo home_url(); ?>/cart/" title="Belanja sekarang dan gunakan voucher ini!">Gunakan</a>
										<a href="<?php echo $voucher_link; ?>" title="Lihat voucher ini.">
											<span class="span_ambilvoucher">Lihat s&k</span>
										</a>
									</div>
								</div>
							<?php
							}else if($now_time >= $str_enddate){
							?>
								<div class="col-md-4 col_list_voc">
									<div class="box_list_voc box_myVoucher expired">
										<div class="mg_list_voc">
											<img src="<?php echo $urlphoto; ?>" alt="<?php echo $alt; ?>">
										</div>
										<h4 class="ht_list_voc"><?php echo $voucher_name; ?></h4>
										<a class="a_useVoucher expired" title="Belanja sekarang dan gunakan voucher ini!">Expired</a>
										<a href="<?php echo $voucher_link; ?>" title="Lihat voucher ini.">
											<span class="span_ambilvoucher">Lihat s&k</span>
										</a>
									</div>
								</div>
							<?php
							}
						}
					}
				?>
			</div>


			<a href="<?php echo home_url(); ?>/voucher/">
				<div class="box_btn_tukarvoucher">
					<input type="button" class="btn_tukarvoucher" value="Ambil Voucher Lainnya">
				</div>
			</a>

		</div>


	</div>
</div>


<?php }else{ ?>
    <script>
        // 'Getting' data-attributes using getAttribute
        var plant = document.getElementById('body');
        console.log(plant);
        var hurl = plant.getAttribute('data-hurl'); 
        location.replace(hurl+'/login/'); 
    </script>
<?php } ?>

<?php endwhile; ?>
<?php else : ?>
		<?php get_template_part( 'content', '404pages' ); ?>	
<?php endif; ?>
<?php get_footer(); ?>