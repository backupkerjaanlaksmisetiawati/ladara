<?php get_header(); ?>
<?php if (have_posts()) : while (have_posts()) : the_post(); ?>

<article id="post-<?php the_ID(); ?>" <?php post_class( 'clearfix' ); ?> role="article" itemscope itemtype="http://schema.org/BlogPosting">
	
<div class="row row_globalPage">
	<div class="col-md-12 col_globalPage">

        <?php if (!$_GET['mobile']){ ?>
            <a href="<?php echo home_url(); ?>">
                <div class="bx_backShop">
                    <span class="glyphicon glyphicon-menu-left"></span> Kembali ke Home
                </div>
            </a>
        <?php } ?>

        <div class="bx_globalPage">

        	<div class="row row_title_globalPage">
        		<div class="col-md-8 col_globalPage">
        			<h1 class="ht_globalPage"><?php echo get_the_title($post->ID); ?></h1>
        		</div>
        		<div class="col-md-4 ">
        			<div class="mg_globalPage animated fadeIn">
						<img src="<?php bloginfo('template_directory'); ?>/library/images/main_logo.png">
					</div>
        		</div>
        	</div>

        	<div class="content_globalPage">
				<?php the_content(); ?>
			</div>
			
        </div>
	</div>
</div>

</article>

<?php endwhile; else : ?>
		<article id="post-not-found" class="hentry clearfix">
			<header class="article-header">
				<h1><?php _e( 'Oops, Post Not Found!', 'bonestheme' ); ?></h1>
			</header>
			<section class="entry-content">
				<p><?php _e( 'Uh Oh. Something is missing. Try double checking things.', 'bonestheme' ); ?></p>
			</section>
			<footer class="article-footer">
					<p><?php _e( 'This is the error message in the page.php template.', 'bonestheme' ); ?></p>
			</footer>
		</article>
<?php endif; ?>
<?php get_footer(); ?>