<?php /* Template Name: Dashboard Ladara Emas - Transaction List */ ?>

<?php get_header('admin'); ?>

<?php if (have_posts()) : while (have_posts()) : the_post(); ?>

  <?php
    $current_user = wp_get_current_user();
    $u_id = $current_user->ID;
    $u_roles = $current_user->roles;
    $admin = 0;

    if(isset($u_roles[0]) AND !empty($u_roles)){
      $u_roles = $u_roles[0];
      if(strtolower($u_roles) == 'administrator'){
        $admin = 1;
      }
    }
  ?>

  <?php if($u_id == 0 && $admin != 1) { ?>
    <style>
      header, body {
        display: none;
      }
    </style>
    <script>
      // 'Getting' data-attributes using getAttribute
      var plant = document.getElementById('body');
      var hurl = plant.getAttribute('data-hurl'); 
      location.replace(hurl+'/login/'); 
    </script>
  <?php } ?>

  <style>
    .select2-container {
      width: auto !important;
    }
  </style>


  <?php
    global $wpdb;

    $user_emas = $wpdb->get_results("SELECT id, name FROM  ldr_user_emas ORDER BY name ASC", OBJECT);

    $param_get = isset($_GET) ? $_GET : [];

    if(!isset($param_get['pg'])) {
      $param_get['pg'] = 1;
    } else {
      $param_get['pg'] = (int)$param_get["pg"];
    }

    $datas = adm_gld_transaction_list($param_get);
    $history_emas = $datas["datas"];
    $total = $datas["total"];

    $current_page = $param_get['pg'];
    $pagination_prep = gld_pagination_prep($total, $current_page);

    $page_link = gld_pagination_link('ladara-emas-transaction', $param_get);

    $base_sort_link = gld_pagination_sort_link('ladara-emas-transaction', $param_get);
  ?>


  <div class="container-fluid" id="ladara-emas-dashboard">

    <h1 class="h3 mb-4 text-gray-800">
      Daftar Transaksi Ladara Emas
      <br />
      <?php if( ( isset($param_get["date_from"]) && !empty($param_get["date_from"]) ) && ( isset($param_get["date_to"]) && !empty($param_get["date_to"]) )) { ?>
        <span style="font-size:1rem;">From <?php echo date("d F Y", strtotime($param_get["date_from"])); ?> - <?php echo date("d F Y", strtotime($param_get["date_to"])); ?></span>
      <?php } else if( ( isset($param_get["date_from"]) && !empty($param_get["date_from"]) ) && ( isset($param_get["date_to"]) && empty($param_get["date_to"]) )) { ?>
        <span style="font-size:1rem;">From <?php echo date("d F Y", strtotime($param_get["date_from"])); ?> - <?php echo date("d F Y"); ?></span>
      <?php } else { ?>
        <span style="font-size:1rem;">From 1 March 2020 - <?php echo date("d F Y"); ?></span>
      <?php } ?>
    </h1>

    <!-- DataTales Example -->
    <div class="card shadow mb-4">
      <div class="card-header py-3">
        <h6 class="m-0 font-weight-bold text-primary ht_admdash">
          Search
        </h6>

        <form>
          <div class="row">
            <div class="col-sm-12 col-md-3">
              <label>Type</label>
              <select name="type" class="jsselectHideSearch">
                <option value="all" <?php echo (isset($param_get["type"]) && $param_get["type"] === "all") ? "selected=\"selected\"" : ""; ?>>All</option>
                <option value="buy" <?php echo (isset($param_get["type"]) && $param_get["type"] === "buy") ? "selected=\"selected\"" : ""; ?>>Beli</option>
                <option value="sell" <?php echo (isset($param_get["type"]) && $param_get["type"] === "sell") ? "selected=\"selected\"" : ""; ?>>Jual</option>
              </select>
            </div>
            <div class="col-sm-12 col-md-3">
              <label>Status</label>
              <select name="status" class="jsselectHideSearch">
                <option value="all" <?php echo (isset($param_get["status"]) && $param_get["status"] === "all") ? "selected=\"selected\"" : ""; ?>>All</option>
                <option value="Approved" <?php echo (isset($param_get["status"]) && $param_get["status"] === "Approved") ? "selected=\"selected\"" : ""; ?>>Approved</option>
                <option value="Canceled" <?php echo (isset($param_get["status"]) && $param_get["status"] === "Canceled") ? "selected=\"selected\"" : ""; ?>>Canceled</option>
                <option value="Pending" <?php echo (isset($param_get["status"]) && $param_get["status"] === "Pending") ? "selected=\"selected\"" : ""; ?>>Pending</option>
                <option value="Rejected" <?php echo (isset($param_get["status"]) && $param_get["status"] === "Rejected") ? "selected=\"selected\"" : ""; ?>>Rejected</option>
                <option value="Success" <?php echo (isset($param_get["status"]) && $param_get["status"] === "Success") ? "selected=\"selected\"" : ""; ?>>Success</option>
              </select>
            </div>
            <div class="col-sm-12 col-md-3">
              <label>Invoice</label>
              <input type="text" name="invoice" <?php echo (isset($param_get["invoice"]) && isset($invoice)) ? "value=\"" . $invoice . "\"" : ""; ?> />
            </div>
            <div class="col-sm-12 col-md-3">
              <label>Customer</label>
              <select name="customer" class="jsselect">
                <option value="all">All</option>
                <?php foreach($user_emas as $user) { ?>
                  <option value="<?php echo $user->id; ?>" <?php echo (isset($param_get["customer"]) && $param_get["customer"] === $user->id) ? "selected=\"selected\"" : ""; ?>><?php echo $user->name; ?></option>
                <?php } ?>
              </select>
            </div>
          </div>

          <div class="row" style="align-items: flex-end;">
            <div class="col-sm-12 col-md-6">
              <label>Tanggal Transaksi</label>

              <div class="row range-date">
                <div class="no-margin date">
                  <input type="text" name="date_from" id="period_start_date" <?php echo (isset($param_get["date_from"])) ? "value=\"" . $param_get["date_from"] . "\"" : ""; ?> placeholder="range dari tanggal..." />
                </div>
                
                <div class="no-margin">
                  <p class="no-margin">-</p>
                </div>
                
                <div class="no-margin date">
                  <?php
                    if( (isset($param_get["date_from"]) && isset($param_get["date_to"])) && !empty($param_get["date_from"]) && empty($param_get["date_to"])) {
                      $date_to = date("Y-m-d H:i:s");
                    } elseif( (isset($param_get["date_from"]) && isset($param_get["date_to"])) && !empty($param_get["date_from"]) && !empty($param_get["date_to"])) {
                      $date_to = $param_get["date_to"];
                    } else {
                      $date_to = "";
                    }
                  ?>
                  <input type="text" name="date_to" id="period_end_date" <?php echo (isset($param_get["date_to"])) ? "value=\"" . $date_to . "\"" : ""; ?> placeholder="sampai tanggal..." />
                </div>
              </div>            
            </div>

            <div class="col-sm-12 col-md-3" style="padding-bottom: 3px;">
              <button type="submit" name="action" value="search" class="btn-base btn-blue">Search</button>
              <a href="<?php echo home_url() . "/dashboard/ladara-emas-transaction"; ?>" class="btn-base btn-blue">See All</a>
            </div>
          </div>
        </form>
      </div>

      <div class="card-body">
        <div class="table-responsive">
          <div class="row">
            <div class="col-sm-12 col-md-5 ladara-emas-total-data">
              <div class="dataTables_info" id="dataTable_info" role="status" aria-live="polite">
                <?php if($total != 0) { ?>
                  Showing <?php echo $pagination_prep["showing_from"]; ?> to <?php echo $pagination_prep["showing_to"]; ?> of
                <?php } ?> 
                <?php echo $total; ?> entries
              </div>
            </div>
            <?php if($total != 0) { ?>
              <div class="col-sm-12 col-md-7 ladara-emas-dashboard-pagination">
                <div>
                  <?php
                    gld_pagination(
                      array(
                        'base'				=> $page_link,
                        'page'				=> $pagination_prep["page"],
                        'pages' 			=> $pagination_prep["pages"],
                        'key'					=> 'pg',
                        'next_text'		=> 'Next',
                        'prev_text'		=> 'Previous',
                        'first_text'	=> 'First',
                        'last_text'		=> 'Last',
                        'show_dots'   => true
                      )
                    );
                  ?>
                </div>
              </div>
            <?php } ?> 
          </div>

          <table class="table table-bordered ladara-emas-table" width="100%" cellspacing="0">
            <thead>
              <tr>
                <th>
                  <?php
                    if( (isset($param_get["order"]) && $param_get["order"] === "type") && (isset($param_get["sort"]) && $param_get["sort"] === "ASC") ) {
                      $type_order = "order=type&sort=DESC";
                      $type_class = "desc";
                    } else {
                      $type_order = "order=type&sort=ASC";
                      $type_class = "asc";
                    }
                  ?>
                  <a href="<?php echo $base_sort_link . $type_order; ?>" class="<?php echo $type_class; ?> <?php echo ( (isset($param_get["order"]) && $param_get["order"] === "type") ) ? "active" : ""; ?>">
                    Tipe
                  </a>
                </th>
                <th>
                  <?php
                    if( (isset($param_get["order"]) && $param_get["order"] === "invoice") && (isset($param_get["sort"]) && $param_get["sort"] === "ASC") ) {
                      $invoice_order = "order=invoice&sort=DESC";
                      $invoice_class = "desc";
                    } else {
                      $invoice_order = "order=invoice&sort=ASC";
                      $invoice_class = "asc";
                    }
                  ?>
                  <a href="<?php echo $base_sort_link . $invoice_order; ?>" class="<?php echo $invoice_class; ?> <?php echo ( (isset($param_get["order"]) && $param_get["order"] === "invoice") ) ? "active" : ""; ?>">
                    Invoice/Faktur
                  </a>
                </th>
                <th>
                  <?php
                    if( (isset($param_get["order"]) && $param_get["order"] === "customer") && (isset($param_get["sort"]) && $param_get["sort"] === "ASC") ) {
                      $customer_order = "order=customer&sort=DESC";
                      $customer_class = "desc";
                    } else {
                      $customer_order = "order=customer&sort=ASC";
                      $customer_class = "asc";
                    }
                  ?>
                  <a href="<?php echo $base_sort_link . $customer_order; ?>" class="<?php echo $customer_class; ?> <?php echo ( (isset($param_get["order"]) && $param_get["order"] === "customer") ) ? "active" : ""; ?>">
                    User
                  </a>
                </th>
                <th style="width:105px;">
                  <?php
                    if( (isset($param_get["order"]) && $param_get["order"] === "order_date") && (isset($param_get["sort"]) && $param_get["sort"] === "ASC") ) {
                      $order_date_order = "order=order_date&sort=DESC";
                      $order_date_class = "desc";
                    } else {
                      $order_date_order = "order=order_date&sort=ASC";
                      $order_date_class = "asc";
                    }
                  ?>
                  <a href="<?php echo $base_sort_link . $order_date_order; ?>" class="<?php echo $order_date_class; ?> <?php echo ( (isset($param_get["order"]) && $param_get["order"] === "order_date") ) ? "active" : ""; ?>">
                    Tanggal<br />Transaksi
                  </a>
                </th>
                <th>
                  <?php
                    if( (isset($param_get["order"]) && $param_get["order"] === "total_gold") && (isset($param_get["sort"]) && $param_get["sort"] === "ASC") ) {
                      $total_gold_order = "order=total_gold&sort=DESC";
                      $total_gold_class = "desc";
                    } else {
                      $total_gold_order = "order=total_gold&sort=ASC";
                      $total_gold_class = "asc";
                    }
                  ?>
                  <a href="<?php echo $base_sort_link . $total_gold_order; ?>" class="<?php echo $total_gold_class; ?> <?php echo ( (isset($param_get["order"]) && $param_get["order"] === "total_gold") ) ? "active" : ""; ?>">
                    Total Emas
                  </a>
                </th>
                <th style="width:105px;">
                  <?php
                    if( (isset($param_get["order"]) && $param_get["order"] === "total_payment") && (isset($param_get["sort"]) && $param_get["sort"] === "ASC") ) {
                      $total_payment_order = "order=total_payment&sort=DESC";
                      $total_payment_class = "desc";
                    } else {
                      $total_payment_order = "order=total_payment&sort=ASC";
                      $total_payment_class = "asc";
                    }
                  ?>
                  <a href="<?php echo $base_sort_link . $total_payment_order; ?>" class="<?php echo $status_class; ?> <?php echo ( (isset($param_get["order"]) && $param_get["order"] === "total_payment") ) ? "active" : ""; ?>">
                    Total
                  </a>
                </th>
                <th>
                  <?php
                    if( (isset($param_get["order"]) && $param_get["order"] === "status") && (isset($param_get["sort"]) && $param_get["sort"] === "ASC") ) {
                      $status_order = "order=status&sort=DESC";
                      $status_class = "desc";
                    } else {
                      $status_order = "order=status&sort=ASC";
                      $status_class = "asc";
                    }
                  ?>
                  <a href="<?php echo $base_sort_link . $status_order; ?>" class="<?php echo $status_class; ?> <?php echo ( (isset($param_get["order"]) && $param_get["order"] === "status") ) ? "active" : ""; ?>">
                    Status
                  </a>
                </th>
                <th style="width:125px;">
                  <a class="disabled">
                    Action
                  </a>
                </th>
              </tr>
            </thead>

            <tbody>
              <?php if(!empty($history_emas)) { ?>
                <?php foreach($history_emas as $key => $history) { ?>
                  <tr>
                    <td>
                      <?php echo $history->type; ?>
                    </td>
                    <td>
                      <?php echo $history->invoice_order; ?>
                    </td>
                    <td>
                      <?php echo $history->user_name; ?>
                    </td>
                    <td>
                      <?php echo $history->created_date; ?>
                    </td>
                    <td>
                      <?php echo str_replace(".", ",", round($history->total_gold, 4, PHP_ROUND_HALF_UP)); ?> gr
                    </td>
                    <td>
                      <?php echo number_format(round($history->total_payment), 0, ".", "."); ?>
                    </td>
                    <td>
                      <?php
                        if(strtolower($history->status) === "pending") {
                          $status_color = "FFA600";
                        } elseif(strtolower($history->status) === "canceled" || strtolower($history->status) === "rejected") {
                          $status_color = "FF1E00";
                        } else {
                          $status_color = "5CC450";
                        }
                      ?>
                      <span style="color:#<?php echo $status_color; ?>">
                        <?php echo ucwords($history->status); ?>
                      </span>
                    </td>
                    <td>
                      <p class="bx-btn-base">
                        <a href="<?php echo home_url() . "/dashboard/ladara-emas-transaction/emas-transaction-detail?id=" . $history->id; ?>" class="btn-base btn-blue">
                          Detail
                        </a>
                      </p>

                      <?php /*if(strtolower($history->type) === "buy" && strtolower($history->status) === "pending") { ?>
                        <p>
                          <a href="<?php echo $history->xendit_url; ?>" class="btn_ca_confirm blue" style="display:inline-block; text-align:center; color:white;" target="_blank">
                            Bayar
                          </a>
                        </p>
                      <?php }*/ ?>
                    </td>
                  </tr>
                <?php } ?>
              <?php } else { ?>
                <tr>
                  <td colspan="8" style="text-align:center;">Empty</td>
                </tr>
              <?php } ?>
            </tbody>

            <tfoot>
              <tr>
                <th>Tipe</th>
                <th>Invoice/Faktur</th>
                <th>User</th>
                <th>Tanggal<br />Transaksi</th>
                <th>Total Emas</th>
                <th>Total</th>
                <th>Status</th>
                <th>Action</th>
              </tr>
            </tfoot>
          </table>

          <div class="row">
            <div class="col-sm-12 col-md-5 ladara-emas-total-data">
              <div class="dataTables_info" id="dataTable_info" role="status" aria-live="polite">
                <?php if($total != 0) { ?>
                  Showing <?php echo $pagination_prep["showing_from"]; ?> to <?php echo $pagination_prep["showing_to"]; ?> of
                <?php } ?> 
                <?php echo $total; ?> entries
              </div>
            </div>
            <?php if($total != 0) { ?>
              <div class="col-sm-12 col-md-7 ladara-emas-dashboard-pagination">
                <div>
                  <?php
                    gld_pagination(
                      array(
                        'base'				=> $page_link,
                        'page'				=> $pagination_prep["page"],
                        'pages' 			=> $pagination_prep["pages"],
                        'key'					=> 'pg',
                        'next_text'		=> 'Next',
                        'prev_text'		=> 'Previous',
                        'first_text'	=> 'First',
                        'last_text'		=> 'Last',
                        'show_dots'   => true
                      )
                    );
                  ?>
                </div>
              </div>
            <?php } ?> 
          </div>
        </div>
      </div>
    </div>

  </div>


<?php endwhile; ?>
<?php else : ?>
    <?php get_template_part( 'content', '404pages' ); ?>  
<?php endif; ?>

<?php get_footer('admin'); ?>

<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/flatpickr/dist/flatpickr.min.css">
<script src="https://cdn.jsdelivr.net/npm/flatpickr"></script>
<script>
  flatpickr("#period_start_date, #period_end_date");
</script>