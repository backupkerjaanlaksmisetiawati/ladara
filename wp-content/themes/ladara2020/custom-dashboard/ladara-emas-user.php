<?php /* Template Name: Dashboard Ladara Emas - User List */ ?>

<?php get_header('admin'); ?>

<?php if (have_posts()) : while (have_posts()) : the_post(); ?>

  <?php
    $current_user = wp_get_current_user();
    $u_id = $current_user->ID;
    $u_roles = $current_user->roles;
    $admin = 0;

    if(isset($u_roles[0]) AND !empty($u_roles)){
      $u_roles = $u_roles[0];
      if(strtolower($u_roles) == 'administrator'){
        $admin = 1;
      }
    }
  ?>

  <?php if($u_id == 0 && $admin != 1) { ?>
    <style>
      header, body {
        display: none;
      }
    </style>
    <script>
      // 'Getting' data-attributes using getAttribute
      var plant = document.getElementById('body');
      var hurl = plant.getAttribute('data-hurl'); 
      location.replace(hurl+'/login/'); 
    </script>
  <?php } ?>

  <style>
    .select2-container {
      width: auto !important;
    }
  </style>

  <?php
    global $wpdb;

    $param_get = isset($_GET) ? $_GET : [];

    if(!isset($param_get['pg'])) {
      $param_get['pg'] = 1;
    } else {
      $param_get['pg'] = (int)$param_get["pg"];
    }

    $datas = adm_gld_user_list($param_get);
    $user_emas = $datas["datas"];
    $total = $datas["total"];

    $current_page = $param_get['pg'];
    $pagination_prep = gld_pagination_prep($total, $current_page);

    $page_link = gld_pagination_link('ladara-emas-user', $param_get);

    $base_sort_link = gld_pagination_sort_link('ladara-emas-user', $param_get);
  ?>


  <div class="container-fluid" id="ladara-emas-dashboard">

    <h1 class="h3 mb-4 text-gray-800">
      Daftar User Ladara Emas
      <br />
      <?php if( ( isset($param_get["date_from"]) && !empty($param_get["date_from"]) ) && ( isset($param_get["date_to"]) && !empty($param_get["date_to"]) )) { ?>
        <span style="font-size:1rem;">From <?php echo date("d F Y", strtotime($param_get["date_from"])); ?> - <?php echo date("d F Y", strtotime($param_get["date_to"])); ?></span>
      <?php } else if( ( isset($param_get["date_from"]) && !empty($param_get["date_from"]) ) && ( isset($param_get["date_to"]) && empty($param_get["date_to"]) )) { ?>
        <span style="font-size:1rem;">From <?php echo date("d F Y", strtotime($param_get["date_from"])); ?> - <?php echo date("d F Y"); ?></span>
      <?php } else { ?>
        <span style="font-size:1rem;">From 1 March 2020 - <?php echo date("d F Y"); ?></span>
      <?php } ?>
    </h1>

    <!-- DataTales Example -->
    <div class="card shadow mb-4">
      <div class="card-header py-3">
        <h6 class="m-0 font-weight-bold text-primary ht_admdash">
          Search
        </h6>

        <form>
          <div class="row">
            <div class="col-sm-12 col-md-3">
              <label>Nama</label>
              <input type="text" name="nama" <?php echo (isset($param_get["nama"])) ? "value=\"" . $param_get["nama"] . "\"" : ""; ?> />
            </div>

            <div class="col-sm-12 col-md-3">
              <label>Email</label>
              <input type="text" name="email" <?php echo (isset($param_get["email"])) ? "value=\"" . $param_get["email"] . "\"" : ""; ?> />
            </div>
            
            <div class="col-sm-12 col-md-3">
              <label>Jenis Kelamin</label>
              <select name="gender" class="jsselectHideSearch">
                <option value="all" <?php echo (isset($param_get["gender"]) && $param_get["gender"] === "all") ? "selected=\"selected\"" : ""; ?>>All</option>
                <option value="pria" <?php echo (isset($param_get["gender"]) && $param_get["gender"] === "pria") ? "selected=\"selected\"" : ""; ?>>Pria</option>
                <option value="wanita" <?php echo (isset($param_get["gender"]) && $param_get["gender"] === "wanita") ? "selected=\"selected\"" : ""; ?>>Wanita</option>
              </select>
            </div>

            <div class="col-sm-12 col-md-3">
              <label>Status Verifikasi</label>
              <select name="verified" class="jsselectHideSearch">
                <option value="all" <?php echo (isset($param_get["verified"]) && $param_get["verified"] === "all") ? "selected=\"selected\"" : ""; ?>>All</option>
                <option value="yes" <?php echo (isset($param_get["verified"]) && $param_get["verified"] === "yes") ? "selected=\"selected\"" : ""; ?>>Terverifikasi</option>
                <option value="no" <?php echo (isset($param_get["verified"]) && $param_get["verified"] === "no") ? "selected=\"selected\"" : ""; ?>>Belum terverifikasi</option>
              </select>
            </div>
          </div>

          <div class="row" style="align-items: flex-end;">
            <div class="col-sm-12 col-md-3">
              <label>Status Aktivasi</label>
              <select name="activated" class="jsselectHideSearch">
                <option value="all" <?php echo (isset($param_get["activated"]) && $param_get["activated"] === "all") ? "selected=\"selected\"" : ""; ?>>All</option>
                <option value="yes" <?php echo (isset($param_get["activated"]) && $param_get["activated"] === "yes") ? "selected=\"selected\"" : ""; ?>>Sukses aktivasi</option>
                <option value="no" <?php echo (isset($param_get["activated"]) && $param_get["activated"] === "no") ? "selected=\"selected\"" : ""; ?>>Gagal aktivasi</option>
              </select>
            </div>

            <div class="col-sm-12 col-md-6">
              <label>Tanggal Aktivasi</label>

              <div class="row range-date">
                <div class="no-margin date">
                  <input type="text" name="date_from" id="period_start_date" <?php echo (isset($param_get["date_from"])) ? "value=\"" . $param_get["date_from"] . "\"" : ""; ?> placeholder="range dari tanggal..." />
                </div>
                
                <div class="no-margin">
                  <p class="no-margin">-</p>
                </div>
                
                <div class="no-margin date">
                  <?php
                    if( (isset($param_get["date_from"]) && isset($param_get["date_to"])) && !empty($param_get["date_from"]) && empty($param_get["date_to"])) {
                      $date_to = date("Y-m-d H:i:s");
                    } elseif( (isset($param_get["date_from"]) && isset($param_get["date_to"])) && !empty($param_get["date_from"]) && !empty($param_get["date_to"])) {
                      $date_to = $param_get["date_to"];
                    } else {
                      $date_to = "";
                    }
                  ?>
                  <input type="text" name="date_to" id="period_end_date" <?php echo (isset($param_get["date_to"])) ? "value=\"" . $date_to . "\"" : ""; ?> placeholder="sampai tanggal..." />
                </div>
              </div>            
            </div>
            
            <div class="col-sm-12 col-md-3" style="padding-bottom: 3px;">
              <button type="submit" name="action" value="search" class="btn-base btn-blue">Search</button>
              <a href="<?php echo home_url() . "/dashboard/ladara-emas-user"; ?>" class="btn-base btn-blue">See All</a>
            </div>
          </div>

          <?php /*<div class="row" style="justify-content:flex-end;">
          </div>*/ ?>
        </form>
      </div>

      <div class="card-body">
        <div class="table-responsive">
          <div class="row">
            <div class="col-sm-12 col-md-5 ladara-emas-total-data">
              <div class="dataTables_info" id="dataTable_info" role="status" aria-live="polite">
                <?php if($total != 0) { ?>
                  Showing <?php echo $pagination_prep["showing_from"]; ?> to <?php echo $pagination_prep["showing_to"]; ?> of
                <?php } ?> 
                <?php echo $total; ?> entries
              </div>
            </div>
            <?php if($total != 0) { ?>
              <div class="col-sm-12 col-md-7 ladara-emas-dashboard-pagination">
                <div>
                  <?php
                    gld_pagination(
                      array(
                        'base'				=> $page_link,
                        'page'				=> $pagination_prep["page"],
                        'pages' 			=> $pagination_prep["pages"],
                        'key'					=> 'pg',
                        'next_text'		=> 'Next',
                        'prev_text'		=> 'Previous',
                        'first_text'	=> 'First',
                        'last_text'		=> 'Last',
                        'show_dots'   => true
                      )
                    );
                  ?>
                </div>
              </div>
            <?php } ?> 
          </div>

          <table class="table table-bordered ladara-emas-table" width="100%" cellspacing="0">
            <thead>
              <tr>
                <th>
                  <?php
                    if( (isset($param_get["order"]) && $param_get["order"] === "nama") && (isset($param_get["sort"]) && $param_get["sort"] === "ASC") ) {
                      $nama_order = "order=nama&sort=DESC";
                      $nama_class = "desc";
                    } else {
                      $nama_order = "order=nama&sort=ASC";
                      $nama_class = "asc";
                    }
                  ?>
                  <a href="<?php echo $base_sort_link . $nama_order; ?>" class="<?php echo $nama_class; ?> <?php echo ( (isset($param_get["order"]) && $param_get["order"] === "nama") ) ? "active" : ""; ?>">
                    Nama
                  </a>
                </th>
                <th>
                  <?php
                    if( (isset($param_get["order"]) && $param_get["order"] === "gender") && (isset($param_get["sort"]) && $param_get["sort"] === "ASC") ) {
                      $gender_order = "order=gender&sort=DESC";
                      $gender_class = "desc";
                    } else {
                      $gender_order = "order=gender&sort=ASC";
                      $gender_class = "asc";
                    }
                  ?>
                  <a href="<?php echo $base_sort_link . $gender_order; ?>" class="<?php echo $gender_class; ?> <?php echo ( (isset($param_get["order"]) && $param_get["order"] === "gender") ) ? "active" : ""; ?>">
                    Jenis<br />Kelamin
                  </a>
                </th>
                <th>
                  <?php
                    if( (isset($param_get["order"]) && $param_get["order"] === "email") && (isset($param_get["sort"]) && $param_get["sort"] === "ASC") ) {
                      $email_order = "order=email&sort=DESC";
                      $email_class = "desc";
                    } else {
                      $email_order = "order=email&sort=ASC";
                      $email_class = "asc";
                    }
                  ?>
                  <a href="<?php echo $base_sort_link . $email_order; ?>" class="<?php echo $email_class; ?> <?php echo ( (isset($param_get["order"]) && $param_get["order"] === "email") ) ? "active" : ""; ?>">
                    Email
                  </a>
                </th>
                <th style="width:100px;">
                  <?php
                    if( (isset($param_get["order"]) && $param_get["order"] === "verified") && (isset($param_get["sort"]) && $param_get["sort"] === "ASC") ) {
                      $verified_order = "order=verified&sort=DESC";
                      $verified_class = "desc";
                    } else {
                      $verified_order = "order=verified&sort=ASC";
                      $verified_class = "asc";
                    }
                  ?>
                  <a href="<?php echo $base_sort_link . $verified_order; ?>" class="<?php echo $verified_class; ?> <?php echo ( (isset($param_get["order"]) && $param_get["order"] === "verified") ) ? "active" : ""; ?>">
                    Verified
                  </a>
                </th>
                <th style="width:105px;">
                  <?php
                    if( (isset($param_get["order"]) && $param_get["order"] === "activated") && (isset($param_get["sort"]) && $param_get["sort"] === "ASC") ) {
                      $activated_order = "order=activated&sort=DESC";
                      $activated_class = "desc";
                    } else {
                      $activated_order = "order=activated&sort=ASC";
                      $activated_class = "asc";
                    }
                  ?>
                  <a href="<?php echo $base_sort_link . $activated_order; ?>" class="<?php echo $activated_class; ?> <?php echo ( (isset($param_get["order"]) && $param_get["order"] === "activated") ) ? "active" : ""; ?>">
                    Activated
                  </a>
                </th>
                <th style="width:105px;">
                  <?php
                    if( (isset($param_get["order"]) && $param_get["order"] === "created_date") && (isset($param_get["sort"]) && $param_get["sort"] === "ASC") ) {
                      $created_date_order = "order=created_date&sort=DESC";
                      $created_date_class = "desc";
                    } else {
                      $created_date_order = "order=created_date&sort=ASC";
                      $created_date_class = "asc";
                    }
                  ?>
                  <a href="<?php echo $base_sort_link . $created_date_order; ?>" class="<?php echo $created_date_class; ?> <?php echo ( (isset($param_get["order"]) && $param_get["order"] === "created_date") ) ? "active" : ""; ?>">
                    Tanggal<br />Aktivasi
                  </a>
                </th>
                <th style="width:125px;">
                  <a class="disabled">
                    Action
                  </a>
                </th>
              </tr>
            </thead>

            <tbody>
              <?php if(!empty($user_emas)) { ?>
                <?php foreach($user_emas as $key => $user) { ?>
                  <tr>
                    <td>
                      <?php echo $user->name; ?>
                    </td>
                    <td>
                      <?php echo $user->gender; ?>
                    </td>
                    <td>
                      <?php echo $user->email; ?>
                    </td>
                    <td style="text-align:center" title="<?php echo ($user->verified_user == 1) ? "Terverifikasi" : "Belum terverifikasi"; ?>">
                      <strong style="color:<?php echo ($user->verified_user == 1) ? "#5CC450" : "#FF1E00"; ?>">
                        <?php echo ($user->verified_user == 1) ? "&check;" : "x"; ?>
                      </strong>
                    </td>
                    <td style="text-align:center" title="<?php echo ($user->activated_user == 1) ? "Sukses aktivasi" : "Gagal aktivasi"; ?>">
                      <strong style="color:<?php echo ($user->activated_user == 1) ? "#5CC450" : "#FF1E00"; ?>">
                        <?php echo ($user->activated_user == 1) ? "&check;" : "x"; ?>
                      </strong>
                    </td>
                    <td>
                      <?php echo $user->created_date; ?>
                    </td>
                    <td>
                      <?php
                        $detail = home_url() . "/dashboard/ladara-emas-user/user-emas-detail?id=".$user->id;
                      ?>
                      <p class="bx-btn-base">
                        <a href="<?php echo $detail; ?>" class="btn-base btn-blue">
                          Detail
                        </a>
                      </p>
                    </td>
                  </tr>
                <?php } ?>
              <?php } else { ?>
                <tr>
                  <td colspan="7" style="text-align:center;">Empty</td>
                </tr>
              <?php } ?>
            </tbody>

            <tfoot>
              <tr>
                <th>Nama</th>
                <th>Jenis<br />Kelamin</th>
                <th>Email</th>
                <th>Verified</th>
                <th>Activated</th>
                <th>Tanggal<br />Aktivasi</th>
                <th>Action</th>
              </tr>
            </tfoot>
          </table>

          <div class="row">
            <div class="col-sm-12 col-md-5 ladara-emas-total-data">
              <div class="dataTables_info" id="dataTable_info" role="status" aria-live="polite">
                <?php if($total != 0) { ?>
                  Showing <?php echo $pagination_prep["showing_from"]; ?> to <?php echo $pagination_prep["showing_to"]; ?> of
                <?php } ?> 
                <?php echo $total; ?> entries
              </div>
            </div>
            <?php if($total != 0) { ?>
              <div class="col-sm-12 col-md-7 ladara-emas-dashboard-pagination">
                <div>
                  <?php
                    gld_pagination(
                      array(
                        'base'				=> $page_link,
                        'page'				=> $pagination_prep["page"],
                        'pages' 			=> $pagination_prep["pages"],
                        'key'					=> 'pg',
                        'next_text'		=> 'Next',
                        'prev_text'		=> 'Previous',
                        'first_text'	=> 'First',
                        'last_text'		=> 'Last',
                        'show_dots'   => true
                      )
                    );
                  ?>
                </div>
              </div>
            <?php } ?> 
          </div>
        </div>
      </div>
    </div>

  </div>


<?php endwhile; ?>
<?php else : ?>
  <?php get_template_part( 'content', '404pages' ); ?>  
<?php endif; ?>

<?php get_footer('admin'); ?>

<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/flatpickr/dist/flatpickr.min.css">
<script src="https://cdn.jsdelivr.net/npm/flatpickr"></script>
<script>
  flatpickr("#period_start_date, #period_end_date");
</script>