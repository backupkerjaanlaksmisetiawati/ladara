<?php /* Template Name: Dashboard Ladara Emas */ ?>

<?php get_header('admin'); ?>

<?php if (have_posts()) : while (have_posts()) : the_post(); ?>

  <?php
    $current_user = wp_get_current_user();
    $u_id = $current_user->ID;
    $u_roles = $current_user->roles;
    $admin = 0;

    if(isset($u_roles[0]) AND !empty($u_roles)){
      $u_roles = $u_roles[0];
      if(strtolower($u_roles) == 'administrator'){
        $admin = 1;
      }
    }
  ?>

  <?php if($u_id == 0 && $admin != 1) { ?>
    <style>
      header, body {
        display: none;
      }
    </style>
    <script>
      // 'Getting' data-attributes using getAttribute
      var plant = document.getElementById('body');
      var hurl = plant.getAttribute('data-hurl'); 
      location.replace(hurl+'/login/'); 
    </script>
  <?php } ?>

  <?php
    global $wpdb;

    $jumlah_income = $wpdb->get_row("SELECT SUM(ldr_transaksi_emas.partner_fee) AS income FROM ldr_transaksi_emas JOIN ldr_user_emas ON ldr_transaksi_emas.user_id=ldr_user_emas.id WHERE status='Success' AND type='buy'", OBJECT);
    $jumlah_transaksi_beli = $wpdb->get_row("SELECT count(ldr_transaksi_emas.id) AS total_buy FROM ldr_transaksi_emas JOIN ldr_user_emas ON ldr_transaksi_emas.user_id=ldr_user_emas.id WHERE status!='Create Invoice' AND type='buy'", OBJECT);
    $jumlah_transaksi_jual = $wpdb->get_row("SELECT count(ldr_transaksi_emas.id) AS total_sell FROM ldr_transaksi_emas JOIN ldr_user_emas ON ldr_transaksi_emas.user_id=ldr_user_emas.id WHERE status!='Create Invoice' AND type='sell'", OBJECT);
    $jumlah_user = $wpdb->get_row("SELECT count(id) AS total_user FROM ldr_user_emas", OBJECT);
    
    $new_transaksi_beli = $wpdb->get_results("SELECT ldr_transaksi_emas.*, ldr_user_emas.name AS user_name FROM ldr_transaksi_emas 
      JOIN ldr_user_emas ON ldr_transaksi_emas.user_id=ldr_user_emas.id
      WHERE status!='Create Invoice' AND type='buy' ORDER BY ldr_transaksi_emas.id DESC LIMIT 0,5", OBJECT);
    $new_transaksi_jual = $wpdb->get_results("SELECT ldr_transaksi_emas.*, ldr_user_emas.name AS user_name FROM ldr_transaksi_emas 
      JOIN ldr_user_emas ON ldr_transaksi_emas.user_id=ldr_user_emas.id
      WHERE status!='Create Invoice' AND type='sell' ORDER BY ldr_transaksi_emas.id DESC LIMIT 0,5", OBJECT);
    $new_user = $wpdb->get_results("SELECT * FROM ldr_user_emas LIMIT 0,5", OBJECT);
  ?>


  <div class="container-fluid" id="ladara-emas-dashboard">

    <h1 class="h3 mb-5 text-gray-800">
      Dashboard Ladara Emas
      <br />
      <span style="font-size:1rem;">From 1 March 2020 - <?php echo date("d F Y"); ?></span>
    </h1>

    <h3 class="h4 mb-2 text-gray-800 title-table-dashboard-emas">
      Ringkasan total data
    </h3>

    <div class="card shadow mb-5">
      <div class="card-body">
        <div class="row dashboard-emas">
          <div class="col-3">
            <div class="list income">
              <div>
                <i class="fas fa-money-bill-wave"></i>
              </div>
              <div>
                <p class="no-margin">Income Ladara Emas</p>
                <p class="no-margin">
                  Rp <?php echo number_format(round($jumlah_income->income, 0, PHP_ROUND_HALF_UP), 0, ".", "."); ?>
                  <?php //echo gld_terbilang($jumlah_income->income, true, false); ?>
                </p>
              </div>
            </div>
          </div>
          
          <div class="col-3">
            <div class="list buy">
              <div>
                <i class="fas fa-coins"></i>
              </div>
              <div>
                <p class="no-margin">Jumlah Transaksi Beli</p>
                <p class="no-margin">
                  <?php echo $jumlah_transaksi_beli->total_buy; ?>
                </p>
              </div>
            </div>
          </div>
          
          <div class="col-3">
            <div class="list sell">
              <div>
                <i class="fas fa-coins"></i>
              </div>
              <div>
                <p class="no-margin">Jumlah Transaksi Jual</p>
                <p class="no-margin">
                  <?php echo $jumlah_transaksi_jual->total_sell; ?>
                </p>
              </div>
            </div>
          </div>
          
          <div class="col-3">
            <div class="list user">
              <div>
                <i class="fas fa-user-circle"></i>
              </div>
              <div>
                <p class="no-margin">Jumlah User</p>
                <p class="no-margin">
                  <?php echo $jumlah_user->total_user; ?> user
                </p>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>

    <div class="row">
      <div class="col-6 mb-5">
        <h3 class="h4 mb-2 text-gray-800 title-table-dashboard-emas">
          New Buy Transaction

          <a title="See All Buy Transaction" href="<?php echo home_url() . "/dashboard/ladara-emas-transaction?type=buy&status=all&invoice=&customer=all&action=search"; ?>">
            <i class="fas fa-arrow-circle-right"></i>
          </a>
        </h3>

        <div class="card shadow mb-4">
          <div class="card-body">
            <div class="table-responsive">
              <table class="table table-bordered ladara-emas-table" width="100%" cellspacing="0">
                <thead>
                  <tr>
                    <th>
                      <a class="disabled">Invoice/Faktur</a>
                    </th>
                    <th>
                      <a class="disabled">User</a>
                    </th>
                    <th style="width:105px;">
                      <a class="disabled">Tanggal<br />Transaksi</a>
                    </th>
                    <th>
                      <a class="disabled">Total Emas</a>
                    </th>
                    <th>
                      <a class="disabled">Status</a>
                    </th>
                  </tr>
                </thead>

                <tbody>
                  <?php if(!empty($new_transaksi_beli)) { ?>
                    <?php foreach($new_transaksi_beli as $key => $history) { ?>
                      <tr>
                        <td>
                          <a href="<?php echo home_url() . "/dashboard/ladara-emas-transaction/emas-transaction-detail?id=" . $history->id; ?>">
                            <?php echo $history->invoice_order; ?>
                          </a>
                        </td>
                        <td>
                          <?php echo $history->user_name; ?>
                        </td>
                        <td>
                          <?php echo $history->created_date; ?>
                        </td>
                        <td>
                          <?php echo str_replace(".", ",", round($history->total_gold, 4, PHP_ROUND_HALF_UP)); ?> gr
                        </td>
                        <td>
                          <?php
                            if(strtolower($history->status) === "pending") {
                              $status_color = "FFA600";
                            } elseif(strtolower($history->status) === "canceled" || strtolower($history->status) === "rejected") {
                              $status_color = "FF1E00";
                            } else {
                              $status_color = "5CC450";
                            }
                          ?>
                          <span style="color:#<?php echo $status_color; ?>">
                            <?php echo ucwords($history->status); ?>
                          </span>
                        </td>
                      </tr>
                    <?php } ?>
                  <?php } else { ?>
                    <tr>
                      <td colspan="8" style="text-align:center;">Empty</td>
                    </tr>
                  <?php } ?>
                </tbody>

                <tfoot>
                  <tr>
                    <th>Invoice/Faktur</th>
                    <th>User</th>
                    <th>Tanggal<br />Transaksi</th>
                    <th>Total Emas</th>
                    <th>Status</th>
                  </tr>
                </tfoot>
              </table>
            </div>
          </div>
        </div>
      </div>

      <div class="col-6 mb-5">
        <h3 class="h4 mb-2 text-gray-800 title-table-dashboard-emas">
          New Sell Transaction

          <a title="See All Sell Transaction" href="<?php echo home_url() . "/dashboard/ladara-emas-transaction?type=sell&status=all&invoice=&customer=all&action=search"; ?>">
            <i class="fas fa-arrow-circle-right"></i>
          </a>
        </h3>

        <div class="card shadow mb-4">
          <div class="card-body">
            <div class="table-responsive">
              <table class="table table-bordered ladara-emas-table" width="100%" cellspacing="0">
                <thead>
                  <tr>
                    <th>
                      <a class="disabled">Invoice/Faktur</a>
                    </th>
                    <th>
                      <a class="disabled">User</a>
                    </th>
                    <th style="width:105px;">
                      <a class="disabled">Tanggal<br />Transaksi</a>
                    </th>
                    <th>
                      <a class="disabled">Total Emas</a>
                    </th>
                    <th>
                      <a class="disabled">Status</a>
                    </th>
                  </tr>
                </thead>

                <tbody>
                  <?php if(!empty($new_transaksi_jual)) { ?>
                    <?php foreach($new_transaksi_jual as $key => $history) { ?>
                      <tr>
                        <td>
                          <a href="<?php echo home_url() . "/dashboard/ladara-emas-transaction/emas-transaction-detail?id=" . $history->id; ?>">
                            <?php echo $history->invoice_order; ?>
                          </a>
                        </td>
                        <td>
                          <?php echo $history->user_name; ?>
                        </td>
                        <td>
                          <?php echo $history->created_date; ?>
                        </td>
                        <td>
                          <?php echo str_replace(".", ",", round($history->total_gold, 4, PHP_ROUND_HALF_UP)); ?> gr
                        </td>
                        <td>
                          <?php
                            if(strtolower($history->status) === "pending") {
                              $status_color = "FFA600";
                            } elseif(strtolower($history->status) === "canceled" || strtolower($history->status) === "rejected") {
                              $status_color = "FF1E00";
                            } else {
                              $status_color = "5CC450";
                            }
                          ?>
                          <span style="color:#<?php echo $status_color; ?>">
                            <?php echo ucwords($history->status); ?>
                          </span>
                        </td>
                      </tr>
                    <?php } ?>
                  <?php } else { ?>
                    <tr>
                      <td colspan="8" style="text-align:center;">Empty</td>
                    </tr>
                  <?php } ?>
                </tbody>

                <tfoot>
                  <tr>
                    <th>Invoice/Faktur</th>
                    <th>User</th>
                    <th>Tanggal<br />Transaksi</th>
                    <th>Total Emas</th>
                    <th>Status</th>
                  </tr>
                </tfoot>
              </table>
            </div>
          </div>
        </div>
      </div>
      
      <div class="col-12">
        <h3 class="h4 mb-2 text-gray-800 title-table-dashboard-emas">
          New User

          <a title="See All User" href="<?php echo home_url() . "/dashboard/ladara-emas-user"; ?>">
            <i class="fas fa-arrow-circle-right"></i>
          </a>
        </h3>

        <div class="card shadow mb-4">
          <div class="card-body">
            <div class="table-responsive">
              <table class="table table-bordered ladara-emas-table" width="100%" cellspacing="0">
                <thead>
                  <tr>
                    <th>
                      <a class="disabled">Nama</a>
                    </th>
                    <th>
                      <a class="disabled">Jenis<br />Kelamin</a>
                    </th>
                    <th>
                      <a class="disabled">Email</a>
                    </th>
                    <th style="width:100px;">
                      <a class="disabled">Verified</a>
                    </th>
                    <th style="width:105px;">
                      <a class="disabled">Activated</a>
                    </th>
                    <th style="width:105px;">
                      <a class="disabled">Tanggal<br />Aktivasi</a>
                    </th>
                  </tr>
                </thead>

                <tbody>
                  <?php if(!empty($new_user)) { ?>
                    <?php foreach($new_user as $key => $user) { ?>
                      <tr>
                        <td>
                          <a href="<?php echo home_url() . "/dashboard/ladara-emas-user/user-emas-detail?id=".$user->id; ?>">
                            <?php echo $user->name; ?>
                          </a>
                        </td>
                        <td>
                          <?php echo $user->gender; ?>
                        </td>
                        <td>
                          <?php echo $user->email; ?>
                        </td>
                        <td style="text-align:center" title="<?php echo ($user->verified_user == 1) ? "Terverifikasi" : "Belum terverifikasi"; ?>">
                          <strong style="color:<?php echo ($user->verified_user == 1) ? "#5CC450" : "#FF1E00"; ?>">
                            <?php echo ($user->verified_user == 1) ? "&check;" : "x"; ?>
                          </strong>
                        </td>
                        <td style="text-align:center" title="<?php echo ($user->activated_user == 1) ? "Sukses aktivasi" : "Gagal aktivasi"; ?>">
                          <strong style="color:<?php echo ($user->activated_user == 1) ? "#5CC450" : "#FF1E00"; ?>">
                            <?php echo ($user->activated_user == 1) ? "&check;" : "x"; ?>
                          </strong>
                        </td>
                        <td>
                          <?php echo $user->created_date; ?>
                        </td>
                      </tr>
                    <?php } ?>
                  <?php } else { ?>
                    <tr>
                      <td colspan="7" style="text-align:center;">Empty</td>
                    </tr>
                  <?php } ?>
                </tbody>              

                <tfoot>
                  <tr>
                    <th>Nama</th>
                    <th>Jenis<br />Kelamin</th>
                    <th>Email</th>
                    <th>Verified</th>
                    <th>Activated</th>
                    <th>Tanggal<br />Aktivasi</th>
                  </tr>
                </tfoot>
              </table>
            </div>
          </div>
        </div>
      </div>
    </div>

  </div>


<?php endwhile; ?>
<?php else : ?>
    <?php get_template_part( 'content', '404pages' ); ?>  
<?php endif; ?>

<?php get_footer('admin'); ?>