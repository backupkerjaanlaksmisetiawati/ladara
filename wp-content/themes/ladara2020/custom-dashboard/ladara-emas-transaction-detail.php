<?php /* Template Name: Dashboard Ladara Emas - Transaction Detail */ ?>

<?php get_header('admin'); ?>

<?php if (have_posts()) : while (have_posts()) : the_post(); ?>

  <?php
    $current_user = wp_get_current_user();
    $u_id = $current_user->ID;
    $u_roles = $current_user->roles;
    $admin = 0;

    if(isset($u_roles[0]) AND !empty($u_roles)){
      $u_roles = $u_roles[0];
      if(strtolower($u_roles) == 'administrator'){
        $admin = 1;
      }
    }
  ?>

  <?php if($u_id == 0 && $admin != 1) { ?>
    <style>
      header, body {
        display: none;
      }
    </style>
    <script>
      // 'Getting' data-attributes using getAttribute
      var plant = document.getElementById('body');
      var hurl = plant.getAttribute('data-hurl'); 
      location.replace(hurl+'/login/'); 
    </script>
  <?php } ?>

  <?php if(!isset($_GET["id"])) { ?>
    <style>
      header, body {
        display: none;
      }
    </style>
    <script>
      // 'Getting' data-attributes using getAttribute
      var plant = document.getElementById('body');
      var hurl = plant.getAttribute('data-hurl'); 
      location.replace(hurl+'/dashboard/ladara-emas-user/'); 
    </script>
  <?php } ?>

  <?php
    global $wpdb;

    $transaksi_id = $_GET["id"];    

    $transaksi = $wpdb->get_row(
      "SELECT ldr_transaksi_emas.*, ldr_user_emas.name, ldr_user_emas.email FROM ldr_transaksi_emas JOIN ldr_user_emas ON ldr_transaksi_emas.user_id = ldr_user_emas.id WHERE ldr_transaksi_emas.id=$transaksi_id",
      OBJECT
    );
  ?>

  <div class="container-fluid" id="ladara-emas-dashboard">

    <h1 class="h3 mb-4 text-gray-800">Detail Transaksi <?php echo $transaksi->invoice_order; ?> Ladara Emas</h1>

    <!-- DataTales Example -->
    <div class="card shadow mb-4">
      <div class="card-body">
        <div class="table-responsive">

          <div id="dashboard_alert" class="alert alert-dismissible box_info_detailWA">
            <label> </label>
            <button type="button" class="close" aria-label="Close">
              <span aria-hidden="true">×</span>
            </button>
          </div>

          <h5 class="dashboard_title_detail text-gray-800">Data User</h5>

          <table class="ladara-emas-dashboard-table-detail">
            <tr>
              <td>Nama</td>
              <td><?php echo $transaksi->name; ?></td>
            </tr>
            
            <tr>
              <td>Email</td>
              <td><?php echo $transaksi->email; ?></td>
            </tr>
          </table>

          <h5 class="text-gray-800">Data Transaksi</h5>

          <table class="ladara-emas-dashboard-table-detail">
            <tr>
              <td>Invoice</td>
              <td><?php echo $transaksi->invoice_order; ?></td>
            </tr>

            <tr>
              <td>Waktu</td>
              <td>
                <?php echo date("j", strtotime($transaksi->created_date)); ?> <?php echo month_indonesia(date("n", strtotime($transaksi->created_date))); ?> <?php echo date("Y", strtotime($transaksi->created_date)); ?> (<?php echo date("H:i", strtotime($transaksi->created_date)); ?> WIB)
              </td>
            </tr>

            <?php if(strtolower($transaksi->type) === "buy") { ?>
              <tr>
                <td>Metode Pembayaran</td>
                <td><?php echo $transaksi->payment_method; ?></td>
              </tr>

              <?php if(!empty($transaksi->xendit_va)) { ?>
                <tr>
                  <td>Virtual Account</td>
                  <td><?php echo $transaksi->xendit_va; ?></td>
                </tr>
              <?php } ?>

              <?php if(!empty($transaksi->xendit_payment_code)) { ?>
                <tr>
                  <td>Kode Pembayaran</td>
                  <td><?php echo $transaksi->xendit_payment_code; ?></td>
                </tr>
              <?php } ?>

              <tr>
                <td>Batas Waktu Pembayaran</td>
                <td><?php echo date("j", strtotime($transaksi->due_date)); ?> <?php echo month_indonesia(date("n", strtotime($transaksi->due_date))); ?> <?php echo date("Y", strtotime($transaksi->due_date)); ?> (<?php echo date("H:i", strtotime($transaksi->due_date)); ?> WIB)</td>
              </tr>
            <?php } ?>

            <?php if(strtolower($transaksi->type) === "sell") { ?>
              <tr>
                <td>No Rekening</td>
                <td><?php echo $transaksi->bank_account; ?></td>
              </tr>

              <tr>
                <td>Pemilik Rekening</td>
                <td><?php echo $transaksi->bank_account_name; ?></td>
              </tr>

              <tr>
                <td>Nama Bank</td>
                <td><?php echo $transaksi->bank_name; ?></td>
              </tr>
            <?php } ?>

            <tr>
              <?php if(strtolower($transaksi->type) === "buy") { ?>
                <td>Rate Beli Emas</td>
                <td>Rp <?php echo number_format(round($transaksi->buying_rate), 0, ".", "."); ?>/gr</td>
              <?php } else { ?>
                <td>Rate Jual Emas</td>
                <td>Rp <?php echo number_format(round($transaksi->selling_rate), 0, ".", "."); ?>/gr</td>
              <?php } ?>
            </tr>
          </table>

          <?php if(strtolower($transaksi->type) === "buy") { ?>
            <h5 class="text-gray-800">Perincian Pembayaran</h5>
          <?php } else { ?>
            <h5 class="text-gray-800">Perincian Penjualan</h5>
          <?php } ?>

          <table class="ladara-emas-dashboard-table-detail">
            <tr>
              <td>Nilai Beli Emas</td>
              <td>Rp <?php echo number_format(round($transaksi->total_price), 0, ".", "."); ?></td>
            </tr>
            <tr class="red">
              <td>Jumlah Emas</td>
              <td>
                <strong style="color:#0080ff;">
                  <?php echo str_replace(".", ",", round($transaksi->total_gold, 4, PHP_ROUND_HALF_UP)); ?> gram
                </strong>
              </td>
            </tr>
            <?php if(strtolower($transaksi->type) === "buy") { ?>
              <tr>
                <td>Biaya Pemesanan Emas</td>
                <td><?php echo number_format(round($transaksi->booking_fee), 0, ".", "."); ?></td>
              </tr>
              <tr>
                <td>PPN</td>
                <td><?php echo number_format(round($transaksi->tax), 0, ".", "."); ?></td>
              </tr>
            <?php } ?>
            <tr>
              <td>Biaya Admin</td>
              <td><?php echo number_format(round($transaksi->partner_fee), 0, ".", "."); ?></td>
            </tr>
            <?php if(strtolower($transaksi->type) === "buy") { ?>
              <tr>
                <td>Biaya Bank</td>
                <td><?php echo number_format(round($transaksi->xendit_fee), 0, ".", "."); ?></td>
              </tr>
            <?php } ?>
            <tr class="total">
              <td><strong>Total</td>
              <td>
                <strong style="color:#0080ff;">
                  Rp <?php echo number_format(round($transaksi->total_payment), 0, ".", "."); ?>
                </strong>
              </td>
            </tr>
          </table>

          <h5 class="text-gray-800">Status Transaksi</h5>

          <table class="ladara-emas-dashboard-table-detail">
            <tr>
              <td colspan="2">
                <?php
                  if(strtolower($transaksi->status) === "pending") {
                    $status_color = "FFA600";
                  } elseif(strtolower($transaksi->status) === "canceled" || strtolower($transaksi->status) === "rejected") {
                    $status_color = "FF1E00";
                  } else {
                    $status_color = "5CC450";
                  }
                ?>
                <strong style="color:#<?php echo $status_color; ?>">
                  <?php if(strtolower($transaksi->type) === "buy" && strtolower($transaksi->status) == "pending") { ?>
                    Pembelian Menunggu Pembayaran
                  <?php } else if(strtolower($transaksi->type) === "buy" && strtolower($transaksi->status) == "canceled") { ?>
                    Pembelian Telah Dibatalkan
                  <?php } else if(strtolower($transaksi->type) === "buy" && strtolower($transaksi->status) == "success") { ?>
                    Pembelian Berhasil
                  <?php } else if(strtolower($transaksi->type) === "buy" && strtolower($transaksi->status) == "failed") { ?>
                    Pembelian Telah Gagal
                  <?php } else if(strtolower($transaksi->type) === "sell" && strtolower($transaksi->status) == "approved") { ?>
                    Penjualan Telah Disetujui
                  <?php } else if(strtolower($transaksi->type) === "sell" && strtolower($transaksi->status) == "pending") { ?>
                    Penjualan Sedang Diproses
                  <?php } else if(strtolower($transaksi->type) === "sell" && strtolower($transaksi->status) == "rejected") { ?>
                    Penjualan Telah Ditolak
                  <?php } ?>
                  <br />
                  <?php if( (strtolower($transaksi->status) == "canceled" || strtolower($transaksi->status) == "rejected" || strtolower($transaksi->status) == "failed") && !empty($transaksi->status_reason)) { ?>
                    (<?php echo $transaksi->status_reason; ?>)
                  <?php } ?>
                </strong>
              </td>
            </tr>
          </table>

          <div class="row">
            <div class="col-sm-12 col-md-6">
              <a href="<?php echo home_url() . "/dashboard/ladara-emas-transaction"; ?>" class="btn-base btn-blue">Kembali</a>
              <?php if(strtolower($transaksi->type) === "sell" && strtolower($transaksi->status) === "pending") { ?>
                <button type="button" id="ajax_ladara_emas_status_jual" class="btn-base btn-blue" onclick="ajax_ladara_emas_status_jual(<?php echo $transaksi->id; ?>)" style="width: auto;">Cek Status Jual</button>
              <?php } ?>
              <?php if(strtolower($transaksi->type) === "buy" && strtolower($transaksi->status) === "canceled") { ?>
                <button type="button" id="ajax_ladara_emas_retrieve_payment" class="btn-base btn-blue" onclick="ajax_ladara_emas_retrieve_payment(<?php echo $transaksi->id; ?>, <?php echo $transaksi->user_id; ?>)" style="width: auto;">Retrieve Payment*</button>
              <?php } ?>
            </div>
          </div>

          <?php if(strtolower($transaksi->type) === "buy" && strtolower($transaksi->status) === "canceled") { ?>
            <div class="row">
              <div class="col-sm-12 col-md-6">
                <p style="margin-top:1.2rem; font-size:12px; color:red;">
                  *Retrieve Payment hanya dapat digunakan apabila customer telah mentransfer pembelian emas tetapi order dibatalkan.
                </p>
              </div>
            </div>
          <?php } ?>
        </div>
      </div>
    </div>

  </div>


<?php endwhile; ?>
<?php else : ?>
    <?php get_template_part( 'content', '404pages' ); ?>  
<?php endif; ?>

<?php get_footer('admin'); ?>