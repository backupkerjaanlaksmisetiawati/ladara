<?php /* Template Name: Dashboard - Kategori Topup */ ?>

<?php get_header('admin'); ?>

<?php if (have_posts()) : while (have_posts()) : the_post(); ?>

  <?php
    $current_user = wp_get_current_user();
    $u_id = $current_user->ID;
    $u_roles = $current_user->roles;
    $admin = 0;

    if(isset($u_roles[0]) AND !empty($u_roles)){
      $u_roles = $u_roles[0];
      if(strtolower($u_roles) == 'administrator'){
        $admin = 1;
      }
    }
  ?>

  <?php if($u_id == 0 && $admin != 1) { ?>
    <style>
      header, body {
        display: none;
      }
    </style>
    <script>
      // 'Getting' data-attributes using getAttribute
      var plant = document.getElementById('body');
      var hurl = plant.getAttribute('data-hurl'); 
      location.replace(hurl+'/login/'); 
    </script>
  <?php } ?>

  <style>
    .select2-container {
      width: auto !important;
    }
  </style>

  <?php
    global $wpdb;

    $param_get = isset($_GET) ? $_GET : [];

    if(!isset($param_get['pg'])) {
      $param_get['pg'] = 1;
    } else {
      $param_get['pg'] = (int)$param_get["pg"];
    }

    $datas = adm_kategori_topup_list($param_get);
    $data = $datas["datas"];
    $total = $datas["total"];

    $current_page = $param_get['pg'];
    $pagination_prep = gld_pagination_prep($total, $current_page);

    $page_link = gld_pagination_link('kategori-topup', $param_get);

    $base_sort_link = gld_pagination_sort_link('kategori-topup', $param_get);
  ?>

  <div class="container-fluid" id="ladara-emas-dashboard">

    <div class="mb-4" style="display:flex; justify-content:space-between">
      <h1 class="h3 text-gray-800 mb-0">
        Daftar Kategori Topup & Tagihan
      </h1>

      <a href="<?php echo home_url() . "/dashboard/kategori-topup/add-kategori-topup"; ?>" class="btn_ca_confirm" style="display:flex;color:white;justify-content:center;align-items:center;">
        Add
      </a>
    </div>

    <!-- DataTales Example -->
    <div class="card shadow mb-4">
      <div class="card-header py-3">
        <h6 class="m-0 font-weight-bold text-primary ht_admdash">
          Search
        </h6>

        <form>
          <div class="row">
            <div class="col-sm-12 col-md-3">
              <label>Nama</label>
              <input type="text" name="nama" <?php echo (isset($param_get["nama"])) ? "value=\"" . $param_get["nama"] . "\"" : ""; ?> />
            </div>

            <div class="col-sm-12 col-md-3">
              <label>Status</label>
              <select name="status" class="jsselectHideSearch">
                <option value="all" <?php echo (isset($param_get["status"]) && $param_get["status"] === "all") ? "selected=\"selected\"" : ""; ?>>Semua</option>
                <option value="1" <?php echo (isset($param_get["status"]) && $param_get["status"] == 1) ? "selected=\"selected\"" : ""; ?>>Active</option>
                <option value="0" <?php echo (isset($param_get["status"]) && $param_get["status"] == 0) ? "selected=\"selected\"" : ""; ?>>Inactive</option>
              </select>
            </div>       
     
            <div class="col-sm-12 col-md-3" style="padding-bottom: 3px;">
              <label>&nbsp;</label>
              <button type="submit" name="action" value="search" class="btn-base btn-blue">Search</button>
              <a href="<?php echo home_url() . "/dashboard/kategori-topup"; ?>" class="btn-base btn-blue">See All</a>
            </div>
          </div>

          <div class="row" style="align-items: flex-end;">
          </div>

          <?php /*<div class="row" style="justify-content:flex-end;">
          </div>*/ ?>
        </form>
      </div>

      <div class="card-body">
        <div id="dashboard_alert" class="alert alert-dismissible box_info_detailWA">
          <label>Hi!</label>
          <button type="button" class="close" aria-label="Close">
              <span aria-hidden="true">×</span>
          </button>
        </div>

        <div class="table-responsive">
          <div class="row">
            <div class="col-sm-12 col-md-5 ladara-emas-total-data">
              <div class="dataTables_info" id="dataTable_info" role="status" aria-live="polite">
                <?php if($total != 0) { ?>
                  Showing <?php echo $pagination_prep["showing_from"]; ?> to <?php echo $pagination_prep["showing_to"]; ?> of
                <?php } ?> 
                <?php echo $total; ?> entries
              </div>
            </div>
            <?php if($total != 0) { ?>
              <div class="col-sm-12 col-md-7 ladara-emas-dashboard-pagination">
                <div>
                  <?php
                    gld_pagination(
                      array(
                        'base'				=> $page_link,
                        'page'				=> $pagination_prep["page"],
                        'pages' 			=> $pagination_prep["pages"],
                        'key'					=> 'pg',
                        'next_text'		=> 'Next',
                        'prev_text'		=> 'Previous',
                        'first_text'	=> 'First',
                        'last_text'		=> 'Last',
                        'show_dots'   => true
                      )
                    );
                  ?>
                </div>
              </div>
            <?php } ?> 
          </div>

          <table class="table table-bordered ladara-emas-table" width="100%" cellspacing="0">
            <thead>
              <tr>
                <th style="width:105px;">
                  <a class="disabled">
                    Icon
                  </a>
                </th>
                <th>
                  <?php
                    if( (isset($param_get["order"]) && $param_get["order"] === "nama") && (isset($param_get["sort"]) && $param_get["sort"] === "ASC") ) {
                      $nama_order = "order=nama&sort=DESC";
                      $nama_class = "desc";
                    } else {
                      $nama_order = "order=nama&sort=ASC";
                      $nama_class = "asc";
                    }
                  ?>
                  <a href="<?php echo $base_sort_link . $nama_order; ?>" class="<?php echo $nama_class; ?> <?php echo ( (isset($param_get["order"]) && $param_get["order"] === "nama") ) ? "active" : ""; ?>">
                    Nama
                  </a>
                </th>
                <th>
                  <?php
                    if( (isset($param_get["order"]) && $param_get["order"] === "slug") && (isset($param_get["sort"]) && $param_get["sort"] === "ASC") ) {
                      $slug_order = "order=slug&sort=DESC";
                      $slug_class = "desc";
                    } else {
                      $slug_order = "order=slug&sort=ASC";
                      $slug_class = "asc";
                    }
                  ?>
                  <a href="<?php echo $base_sort_link . $slug_order; ?>" class="<?php echo $slug_class; ?> <?php echo ( (isset($param_get["order"]) && $param_get["order"] === "slug") ) ? "active" : ""; ?>">
                    Slug
                  </a>
                </th>
                <th style="width:100px;">
                  <a class="disabled">
                    Featured
                  </a>
                </th>
                <th style="width:100px;">
                  <?php
                    if( (isset($param_get["order"]) && $param_get["order"] === "status") && (isset($param_get["sort"]) && $param_get["sort"] === "ASC") ) {
                      $status_order = "order=status&sort=DESC";
                      $status_class = "desc";
                    } else {
                      $status_order = "order=status&sort=ASC";
                      $status_class = "asc";
                    }
                  ?>
                  <a href="<?php echo $base_sort_link . $status_order; ?>" class="<?php echo $status_class; ?> <?php echo ( (isset($param_get["order"]) && $param_get["order"] === "status") ) ? "active" : ""; ?>">
                    Status
                  </a>
                </th>
                <th style="width:105px;">
                  <?php
                    if( (isset($param_get["order"]) && $param_get["order"] === "update_date") && (isset($param_get["sort"]) && $param_get["sort"] === "ASC") ) {
                      $update_date_order = "order=update_date&sort=DESC";
                      $update_date_class = "desc";
                    } else {
                      $update_date_order = "order=update_date&sort=ASC";
                      $update_date_class = "asc";
                    }
                  ?>
                  <a href="<?php echo $base_sort_link . $update_date_order; ?>" class="<?php echo $update_date_class; ?> <?php echo ( (isset($param_get["order"]) && $param_get["order"] === "update_date") ) ? "active" : ""; ?>">
                    Tanggal<br />Update
                  </a>
                </th>
                <th style="width:125px;">
                  <a class="disabled">
                    Action
                  </a>
                </th>
              </tr>
            </thead>

            <tbody>
              <?php if(!empty($data)) { ?>
                <?php foreach($data as $key => $val) { ?>
                  <tr>
                    <td style="text-align:center">
                      <img src="<?php echo $val->icon; ?>" style="width:70px;" />
                    </td>
                    <td>
                      <?php echo $val->name; ?>
                    </td>
                    <td>
                      <?php echo $val->slug; ?>
                    </td>
                    <td style="text-align:center">
                      <span style="color:<?php echo ($val->featured == 1) ? "#5CC450" : "#FF1E00"; ?>">
                        <?php echo ($val->featured == 1) ? "Ya" : "Tidak"; ?>
                      </span>
                    </td>
                    <td style="text-align:center">
                      <span style="color:<?php echo ($val->status == 1) ? "#5CC450" : "#FF1E00"; ?>">
                        <?php echo ($val->status == 1) ? "Active" : "Inactive"; ?>
                      </span>
                    </td>
                    <td>
                      <?php echo $val->updated_at; ?>
                    </td>
                    <td>
                      <p class="bx-btn-base">
                        <a href="<?php echo home_url() . "/dashboard/kategori-topup/edit-kategori-topup?id=".$val->id; ?>" class="btn-base btn-blue">
                          Detail
                        </a>
                      </p>
                      <p class="bx-btn-base">
                        <a onclick="delete_kategori_topup(<?php echo $val->id; ?>)" id="btnDelete<?php echo $val->id; ?>" class="btn-base btn-red">
                          Delete
                        </a>
                      </p>
                    </td>
                  </tr>
                <?php } ?>
              <?php } else { ?>
                <tr>
                  <td colspan="7" style="text-align:center;">Empty</td>
                </tr>
              <?php } ?>
            </tbody>

            <tfoot>
              <tr>
                <th>Icon</th>
                <th>Nama</th>
                <th>Slug</th>
                <th>Featured</th>
                <th>status</th>
                <th>Tanggal<br />Update</th>
                <th>Action</th>
              </tr>
            </tfoot>
          </table>

          <div class="row">
            <div class="col-sm-12 col-md-5 ladara-emas-total-data">
              <div class="dataTables_info" id="dataTable_info" role="status" aria-live="polite">
                <?php if($total != 0) { ?>
                  Showing <?php echo $pagination_prep["showing_from"]; ?> to <?php echo $pagination_prep["showing_to"]; ?> of
                <?php } ?> 
                <?php echo $total; ?> entries
              </div>
            </div>
            <?php if($total != 0) { ?>
              <div class="col-sm-12 col-md-7 ladara-emas-dashboard-pagination">
                <div>
                  <?php
                    gld_pagination(
                      array(
                        'base'				=> $page_link,
                        'page'				=> $pagination_prep["page"],
                        'pages' 			=> $pagination_prep["pages"],
                        'key'					=> 'pg',
                        'next_text'		=> 'Next',
                        'prev_text'		=> 'Previous',
                        'first_text'	=> 'First',
                        'last_text'		=> 'Last',
                        'show_dots'   => true
                      )
                    );
                  ?>
                </div>
              </div>
            <?php } ?> 
          </div>
        </div>
      </div>
    </div>

  </div>


<?php endwhile; ?>
<?php else : ?>
  <?php get_template_part( 'content', '404pages' ); ?>  
<?php endif; ?>

<?php get_footer('admin'); ?>

<script>
function delete_kategori_topup(kategoriId) {
  $("#dashboard_alert").hide().removeClass("alert-danger").removeClass("alert-success");
  if (confirm('Apakah anda yakin?')) {
    $('#btnDelete'+kategoriId).attr("disabled", true).html("Loading...");
    $.ajax({
      url: ajaxscript.ajaxurl,
      type: "POST",
      data: "action=ajax_delete_kategori_topup&id="+kategoriId,
      success: function (res) {
        window.scrollTo(0, 0);
        var res = $.parseJSON(res);

        $('#btnDelete'+kategoriId).removeAttr('disabled').html("Delete");

        if (res.status !== 200) {
          $("#dashboard_alert").show().addClass("alert-danger");
          $("#dashboard_alert label").html("Gagal menghapus data.");
        } else {
          $("#dashboard_alert").show().addClass("alert-success");
          $("#dashboard_alert label").html("Sukses.");
        }

        var plant = document.getElementById('body');
        var hurl = plant.getAttribute('data-hurl'); 
        location.replace(hurl+'/dashboard/kategori-topup/');
      },
      error: function (res) {
        window.scrollTo(0, 0);
        $('#btnDelete'+kategoriId).removeAttr('disabled').html("Delete");
        $("#dashboard_alert").show().addClass("alert-danger");
        $("#dashboard_alert label").html("Error.");
      }
    });
  } else {
    return false
  }
}
</script>