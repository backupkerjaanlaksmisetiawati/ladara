<?php /* Template Name: Dashboard - Kategori Topup - Edit */ ?>

<?php get_header('admin'); ?>

<?php if (have_posts()) : while (have_posts()) : the_post(); ?>

    <?php
    $current_user = wp_get_current_user();
    $u_id = $current_user->ID;
    $u_roles = $current_user->roles;
    $admin = 0;

    if(isset($u_roles[0]) AND !empty($u_roles)){
        $u_roles = $u_roles[0];
        if(strtolower($u_roles) == 'administrator'){
            $admin = 1;
        }
    }
    ?>

    <?php if($u_id == 0 && $admin != 1) { ?>
        <style>
            header, body {
                display: none;
            }
        </style>
        <script>
            // 'Getting' data-attributes using getAttribute
            var plant = document.getElementById('body');
            var hurl = plant.getAttribute('data-hurl'); 
            location.replace(hurl+'/login/'); 
        </script>
    <?php } ?>

    <style>
        .d-block{
            display: block;
        }
        .mb-50{
            margin-bottom: 50px;
        }
    </style>

    <?php
        global $wpdb;

        $id = $_GET["id"];    

        $data = $wpdb->get_row(
            "SELECT * FROM ldr_topup_categories WHERE id=$id",
            OBJECT
        );
    ?>

    <!-- Begin Page Content -->
    <div id="ladara-emas-dashboard" class="container-fluid">
        <h1 class="h3 mb-4 text-gray-800">Edit Kategori Topup</h1>

        <div id="dashboard_alert" class="alert alert-dismissible box_info_detailWA">
            <label> </label>
            <button type="button" class="close" aria-label="Close">
               <span aria-hidden="true">×</span>
            </button>
        </div>

        <div class="card shadow mb-4">
            <div class="card-header py-3">
                Tambah kategori
            </div>

            <div class="card-body">
                <form id="form" method="post" enctype="multipart/form-data">
                    <div class="form-group row mb-50">
                        <label class="col-md-4 col-form-label">
                            Nama <span style="color:red">*</span>
                            <span class="d-block ldr text-info">Nama kategori akan muncul pada menu pilihan kategori</span>
                        </label>
                        <div class="col-md-8">
                            <input required name="name" type="text" value="<?php echo $data->name; ?>" class="form-control" >
                            <div class="f_err name_err"></div>
                        </div>
                    </div>
                    <div class="form-group row mb-50">
                        <label class="col-md-4 col-form-label">
                            Slug <span style="color:red">*</span>
                            <span class="d-block ldr text-info">Slug merupakan versi nama yang url-friendly. Terdiri dari karakter lowercase dan hanya berisi huruf, angka dan tanda hubung(-)</span>
                        </label>
                        <div class="col-md-8">
                            <input name="slug" disabled type="text" value="<?php echo $data->slug; ?>" class="form-control">
                            <div class="f_err slug_err"></div>
                        </div>
                    </div>
                    <div class="form-group row mb-50">
                        <label class="col-md-4 col-form-label">
                            Tampilkan di home <span style="color:red">*</span>
                            <span class="d-block ldr text-info">Atur kategori sebagai kategori utama</span>
                        </label>
                        <div class="col-md-8">
                            <div class="form-check form-check-inline" style="margin-right: 30px;">
                                <input class="form-check-input" type="radio" <?php echo ($data->featured == 1) ? "checked" : ""; ?> name="featured" id="inlineRadio1" value="1">
                                <label class="form-check-label" for="inlineRadio1">Ya</label>
                            </div>
                            <div class="form-check form-check-inline">
                                <input class="form-check-input" type="radio" <?php echo ($data->featured == 0) ? "checked" : ""; ?> name="featured" id="inlineRadio2" value="0">
                                <label class="form-check-label" for="inlineRadio2">Tidak</label>
                            </div>
                        </div>
                    </div>
                    <div class="form-group row mb-50">
                        <label class="col-md-4 col-form-label">
                            Thumbnail Kategori <span style="color:red">*</span>
                            <span class="d-block ldr text-info">Menunjukkan hierarki dari kategori. Contoh : Buah merupakan kategori induk dari Apel dan Jeruk</span>
                        </label>
                        <div class="col-md-8">
                            <div class="row">
                                <div class="col-md-2">
                                    <img style="border-radius: 50%;" src="<?php echo $data->icon; ?> alt="">
                                </div>
                                <div class="col-md-4">
                                    <span class="d-block ldr text-info" style="margin-bottom: 10px;">Besar file maksimal adalah 100 kB dengan rasio gambar 1:1. Ekstensi file yang diperbolehkan: PNG</span>
                                    <input required type="file" name="image" id="image">
                                </div>
                            </div>
                            <div class="f_err image_err"></div>
                        </div>
                    </div>
                    <div class="form-group row mb-50">
                        <label class="col-md-4 col-form-label">
                            Status <span style="color:red">*</span>
                            <span class="d-block ldr text-info">Jika status tidak aktif, maka tidak akan ditampilkan pada halaman PPOB.</span>
                        </label>
                        <div class="col-md-8">
                            <div class="form-check form-check-inline" style="margin-right: 30px;">
                                <input class="form-check-input" type="radio" <?php echo ($data->status == 1) ? "checked" : ""; ?> name="status" id="statusActive" value="1">
                                <label class="form-check-label" for="statusActive">Active</label>
                            </div>
                            <div class="form-check form-check-inline">
                                <input class="form-check-input" type="radio" <?php echo ($data->status == 0) ? "checked" : ""; ?> name="status" id="statusInactive" value="0">
                                <label class="form-check-label" for="statusInactive">Inactive</label>
                            </div>
                        </div>
                    </div>
                    <div class="footer-form" style="width: 100%;border-top: 1px solid #DDDDDD;padding: 10px 0;text-align: right;">
                        <div>
                            <a href="/dashoard/kategori-topup" class="btn-base btn-white-blue">Batal</a>
                            <button type="button" onclick="edit_kategori_topup();" class="btnEdit btn-base btn-blue">Update</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>

        <!-- DataTales Example -->
    </div>
    <!-- /.container-fluid -->

<?php endwhile; ?>
<?php else : ?>
    <?php get_template_part( 'content', '404pages' ); ?>  
<?php endif; ?>

<?php get_footer('admin'); ?>

<script>
function edit_kategori_topup() {
    $('.btnEdit').attr("disabled", true).html("Loading...");
    $('.f_err').hide();
    $("#dashboard_alert").hide().removeClass("alert-danger").removeClass("alert-success");

    var datas = new FormData();
    $('#form').serializeArray().reduce(function (obj, item) {
        datas.append(item.name, item.value);
        return obj;
    }, {});
    var file_data = $('#image').prop('files')[0];
    datas.append('image', file_data);
    datas.append('action', 'ajax_edit_kategori_topup');
    datas.append('id', <?php echo $_GET['id']; ?>);
        
    $.ajax({
        url: ajaxscript.ajaxurl,
        type: "POST",
        contentType: false,
		processData: false,
        data: datas,
        enctype: 'multipart/form-data',
        success: function (res) {
            window.scrollTo(0, 0);
            var res = $.parseJSON(res);

            $('.btnEdit').removeAttr('disabled').html("Update");

            if (res.status !== 200) {
                $("#dashboard_alert").show().addClass("alert-danger");
                $("#dashboard_alert label").html("Terjadi kesalahan input. Silahkan periksa terlebih dahulu.");
                $.each(res.data.message, function (key, value) {
                    if (value !== '') {
                        $('.' + key + '_err').show().html(value);
                    } else {
                        $('.' + key + '_err').show().html("valid").css({ "text-indent":"100%", "overflow":"hidden" });
                    }
                });
            } else {
                $("#dashboard_alert").show().addClass("alert-success");
                $("#dashboard_alert label").html("Sukses.");
                $('.f_err').hide();
            }
        },
        error: function (res) {
            window.scrollTo(0, 0);
            $('.btnEdit').removeAttr('disabled').html("Update");
            $("#dashboard_alert").show().addClass("alert-danger");
            $("#dashboard_alert label").html("Error.");
        }
    });
}
</script>