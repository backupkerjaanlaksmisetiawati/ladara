<?php /* Template Name: Dashboard Ladara Emas - User Detail */ ?>

<?php get_header('admin'); ?>

<?php if (have_posts()) : while (have_posts()) : the_post(); ?>

  <?php
    $current_user = wp_get_current_user();
    $u_id = $current_user->ID;
    $u_roles = $current_user->roles;
    $admin = 0;

    if(isset($u_roles[0]) AND !empty($u_roles)){
      $u_roles = $u_roles[0];
      if(strtolower($u_roles) == 'administrator'){
        $admin = 1;
      }
    }
  ?>

  <?php if($u_id == 0 && $admin != 1) { ?>
    <style>
      header, body {
        display: none;
      }
    </style>
    <script>
      // 'Getting' data-attributes using getAttribute
      var plant = document.getElementById('body');
      var hurl = plant.getAttribute('data-hurl'); 
      location.replace(hurl+'/login/'); 
    </script>
  <?php } ?>

  <?php if(!isset($_GET["id"])) { ?>
    <style>
      header, body {
        display: none;
      }
    </style>
    <script>
      // 'Getting' data-attributes using getAttribute
      var plant = document.getElementById('body');
      var hurl = plant.getAttribute('data-hurl'); 
      location.replace(hurl+'/dashboard/ladara-emas-user/'); 
    </script>
  <?php } ?>

  <?php
    global $wpdb;

    $user_id = $_GET["id"];    

    $user = $wpdb->get_row(
      "SELECT * FROM ldr_user_emas WHERE id=" . $user_id . "",
      OBJECT
    );
  ?>


  <div class="container-fluid" id="ladara-emas-dashboard">

    <h1 class="h3 mb-4 text-gray-800">Detail User <?php echo $user->name; ?> di Ladara Emas</h1>

    <!-- DataTales Example -->
    <div class="card shadow mb-4">
      <div class="card-body">
        <div class="table-responsive">

          <div id="dashboard_alert" class="alert alert-dismissible box_info_detailWA">
            <label> </label>
            <button type="button" class="close" aria-label="Close">
              <span aria-hidden="true">×</span>
            </button>
          </div>

          <h5 class="dashboard_title_detail text-gray-800">Data User</h5>

          <table class="ladara-emas-dashboard-table-detail">
            <tr>
              <td>Nama</td>
              <td><?php echo $user->name; ?></td>
            </tr>
            
            <tr>
              <td>Email</td>
              <td><?php echo $user->email; ?></td>
            </tr>
            
            <tr>
              <td>No Telp</td>
              <td><?php echo $user->phone; ?></td>
            </tr>
            
            <tr>
              <td>Jenis Kelamin</td>
              <td><?php echo $user->gender; ?></td>
            </tr>
            
            <tr>
              <td>Tanggal Lahir</td>
              <td><?php echo $user->birthday; ?></td>
            </tr>
          </table>

          <h5 class="text-gray-800">Data Bank</h5>

          <table class="ladara-emas-dashboard-table-detail">
            <tr>
              <td>Nama Pemilik Rekening</td>
              <td><?php echo ($user->account_name) ? $user->account_name : "-"; ?></td>
            </tr>
            
            <tr>
              <td>No Rekening</td>
              <td><?php echo ($user->account_number) ? $user->account_number : "-"; ?></td>
            </tr>
            
            <tr>
              <td>Nama Bank</td>
              <td><?php echo ($user->bank_name) ? $user->bank_name : "-"; ?></td>
            </tr>
            
            <tr>
              <td>Cabang Bank</td>
              <td><?php echo ($user->branch) ? $user->branch : "-"; ?></td>
            </tr>
          </table>

          <h5 class="text-gray-800">Balance Emas</h5>

          <table class="ladara-emas-dashboard-table-detail">
            <tr>
              <td>Balance Gramasi</td>
              <td><?php echo str_replace(".", ",", round($user->gold_balance, 4, PHP_ROUND_HALF_UP)); ?> gram</td>
            </tr>
            
            <tr>
              <td>
                Balance Rupiah<br />
                <span style="font-size:10px; font-weight:bold;">Balance rupiah dapat berubah<br />sesuai rate jual emas</span>
              </td>
              <td>Rp <?php echo number_format(round($user->gold_balance_in_currency, 0), 0, ".", "."); ?></td>
            </tr>
          </table>

          <h5 class="text-gray-800">Status User</h5>

          <table class="ladara-emas-dashboard-table-detail">
            <tr>
              <td>Status Aktivasi</td>
              <td style="color:<?php echo ($user->activated_user == 1) ? "#5CC450" : "#FF1E00"; ?>">
                <?php echo ($user->activated_user == 1) ? "Sukses aktivasi" : "Gagal aktivasi<br /><span style=\"font-size:10px; font-weight:bold;\">Mohon follow up ke Treasury untuk aktivasi ulang</span>"; ?>
              </td>
            </tr>
            
            <tr>
              <td>Tanggal Aktivasi</td>
              <td>
                <?php echo $user->created_date; ?>
              </td>
            </tr>
            
            <tr>
              <td>Status Verifikasi</td>
              <td style="color:<?php echo ($user->verified_user == 1) ? "#5CC450" : "#FF1E00"; ?>">
                <?php echo ($user->verified_user == 1) ? "Terverifikasi" : "Belum terverifikasi<br /><span style=\"font-size:10px; font-weight:bold;\">Mohon follow up ke Treasury untuk mengupdate status verifikasi</span>"; ?>
              </td>
            </tr>
          </table>

          <div class="row">
            <div class="col-sm-12 col-md-6">
              <a href="<?php echo home_url() . "/dashboard/ladara-emas-user"; ?>" class="btn-base btn-blue">Kembali</a>
              <?php /*if($user->activated_user == 0) { ?>
                <button type="button" id="ajax_ladara_emas_aktivasi_ulang" class="btn-base btn-blue" onclick="ajax_ladara_emas_aktivasi_ulang(<?php echo $user->id; ?>)">Cek Status Aktivasi</button>
              <?php }*/ ?>
              <?php if($user->verified_user == 0) { ?>
                <button type="button" id="ajax_ladara_emas_check_user_verifikasi" class="btn-base btn-blue" onclick="ajax_ladara_emas_check_user_verifikasi(<?php echo $user->id; ?>)">Cek Status verifikasi</button>
              <?php } ?>
            </div>
          </div>
        </div>
      </div>
    </div>

  </div>


<?php endwhile; ?>
<?php else : ?>
    <?php get_template_part( 'content', '404pages' ); ?>  
<?php endif; ?>

<?php get_footer('admin'); ?>