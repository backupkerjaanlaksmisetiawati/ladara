<?php /* Template Name: Dashboard Ladara Emas - Statistic*/ ?>
  
<?php get_header('admin'); ?>

<?php if (have_posts()) : while (have_posts()) : the_post(); ?>

  <?php
    $current_user = wp_get_current_user();
    $u_id = $current_user->ID;
    $u_roles = $current_user->roles;
    $admin = 0;

    if(isset($u_roles[0]) AND !empty($u_roles)){
      $u_roles = $u_roles[0];
      if(strtolower($u_roles) == 'administrator'){
        $admin = 1;
      }
    }
  ?>

  <?php if($u_id == 0 && $admin != 1) { ?>
    <style>
      header, body {
        display: none;
      }
    </style>
    <script>
      // 'Getting' data-attributes using getAttribute
      var plant = document.getElementById('body');
      var hurl = plant.getAttribute('data-hurl'); 
      location.replace(hurl+'/login/'); 
    </script>
  <?php } ?>

  <script src="https://www.chartjs.org/dist/2.9.4/Chart.min.js"></script>
  <script src="https://www.chartjs.org/samples/latest/utils.js"></script>
  
  <?php
    global $wpdb;
    
    $currentYear = 2020;//date("Y");
    $lastYear = $currentYear - 1;

    /** */
      //karena ada transaksi "ghoib" yg usernya g ada
      $query_income = "SELECT SUM(ldr_transaksi_emas.partner_fee) AS income 
        FROM ldr_transaksi_emas JOIN ldr_user_emas ON ldr_transaksi_emas.user_id=ldr_user_emas.id 
        WHERE status='Success' AND type='buy'";
      $jumlah_income = $wpdb->get_row(
        $query_income." AND ldr_transaksi_emas.created_date>='".$currentYear."-01-01' AND ldr_transaksi_emas.created_date<='".$currentYear."-12-31'",
        OBJECT
      );
      $percentIncomeChange = 0;
      if($jumlah_income->income != 0) {
        $jumlah_income_lastyear = $wpdb->get_row(
          $query_income." AND ldr_transaksi_emas.created_date>='".$lastYear."-01-01' AND ldr_transaksi_emas.created_date<='".$lastYear."-12-31'",
          OBJECT
        );
        if($jumlah_income_lastyear->income != 0) {
          $percentIncomeChange = (($jumlah_income->income - $jumlah_income_lastyear->income) / $jumlah_income_lastyear->income) * 100;
          $percentIncomeChange = round($percentIncomeChange, 0, PHP_ROUND_HALF_UP);
        }
      }

      //karena ada transaksi "ghoib" yg usernya g ada
      $query_beli = "SELECT count(ldr_transaksi_emas.id) AS total_buy 
        FROM ldr_transaksi_emas 
        JOIN ldr_user_emas ON ldr_transaksi_emas.user_id=ldr_user_emas.id 
        WHERE status!='Create Invoice' AND type='buy'";
      $jumlah_transaksi_beli = $wpdb->get_row(
        $query_beli." AND ldr_transaksi_emas.created_date>='".$currentYear."-01-01' AND ldr_transaksi_emas.created_date<='".$currentYear."-12-31'",
        OBJECT
      );
      $percentBeliChange = 0;
      if($jumlah_transaksi_beli->total_buy != 0) {
        $jumlah_transaksi_beli_lastyear = $wpdb->get_row(
          $query_beli." AND ldr_transaksi_emas.created_date>='".$lastYear."-01-01' AND ldr_transaksi_emas.created_date<='".$lastYear."-12-31'",
          OBJECT
        );
        if($jumlah_transaksi_beli_lastyear->total_buy != 0) {
          $percentBeliChange = (($jumlah_transaksi_beli->total_buy - $jumlah_transaksi_beli_lastyear->total_buy) / $jumlah_transaksi_beli->total_buy) * 100;
          $percentBeliChange = round($percentBeliChange, 0, PHP_ROUND_HALF_UP);
        }
      }

      //karena ada transaksi "ghoib" yg usernya g ada
      $query_jual = "SELECT count(ldr_transaksi_emas.id) AS total_sell 
        FROM ldr_transaksi_emas 
        JOIN ldr_user_emas ON ldr_transaksi_emas.user_id=ldr_user_emas.id 
        WHERE status!='Create Invoice' AND type='sell'";
      $jumlah_transaksi_jual = $wpdb->get_row(
        $query_jual." AND ldr_transaksi_emas.created_date>='".$currentYear."-01-01' AND ldr_transaksi_emas.created_date<='".$currentYear."-12-31'",
        OBJECT
      );
      $percentJualChange = 0;
      if($jumlah_transaksi_jual->total_sell != 0) {
        $jumlah_transaksi_jual_lastyear = $wpdb->get_row(
          $query_jual." AND ldr_transaksi_emas.created_date>='".$lastYear."-01-01' AND ldr_transaksi_emas.created_date<='".$lastYear."-12-31'",
          OBJECT
        );
        if($jumlah_transaksi_jual_lastyear->total_sell != 0) {
          $percentJualChange = (($jumlah_transaksi_jual->total_sell - $jumlah_transaksi_jual_lastyear->total_sell) / $jumlah_transaksi_jual->total_sell) * 100;
          $percentJualChange = round($percentJualChange, 0, PHP_ROUND_HALF_UP);
        }
      }

      $query_user = "SELECT count(id) AS total_user FROM ldr_user_emas WHERE";
      $jumlah_user = $wpdb->get_row(
        $query_user." created_date>='".$currentYear."-01-01' AND created_date<='".$currentYear."-12-31'", 
        OBJECT
      );
      $percentUserChange = 0;
      if($jumlah_user->total_user != 0) {
        $jumlah_user_lastyear = $wpdb->get_row(
          $query_user." created_date>='".$lastYear."-01-01' AND created_date<='".$lastYear."-12-31'", 
          OBJECT
        );
        if($jumlah_user_lastyear->total_user != 0) {
          $percentUserChange = (($jumlah_user->total_user - $jumlah_user_lastyear->total_user) / $jumlah_user->total_user) * 100;
          $percentUserChange = round($percentUserChange, 0, PHP_ROUND_HALF_UP);
        }
      }
    /** */

    $user_chart = adm_gld_user_chart($currentYear);
    // ddbug($user_chart, false);
    $income_chart = adm_gld_income_chart($currentYear);
    // ddbug($income_chart);
  ?>

  <style>
    canvas {
      -moz-user-select: none;
      -webkit-user-select: none;
      -ms-user-select: none;
    }
  </style>

  <div class="container-fluid" id="ladara-emas-dashboard">

    <h1 class="h3 mb-5 text-gray-800">
      Statistic Ladara Emas
      <br />
      <span style="font-size:1rem;">From 1 January <?php echo $currentYear; ?> - <?php echo date("d F ") . $currentYear; ?></span>
    </h1>

    <h3 class="h4 mb-2 text-gray-800 title-table-dashboard-emas">
      Ringkasan statistik <?php echo $currentYear; ?>
    </h3>

    <div class="card shadow mb-5">
      <div class="card-body">
        <div class="row dashboard-emas">

          <div class="col-3">
            <div class="list income">
              <div>
                <i class="fas fa-money-bill-wave"></i>
              </div>
              <div>
                <p class="no-margin">Income Ladara Emas</p>
                <p class="no-margin">
                  Rp <?php echo number_format(round($jumlah_income->income, 0, PHP_ROUND_HALF_UP), 0, ".", "."); ?>
                  <?php //echo gld_terbilang($jumlah_income->income, true, false); ?>
                </p>
                <p class="no-margin">
                  <?php
                    if($percentIncomeChange != 0 && $percentIncomeChange < 0) {
                      $compare_income = "down";
                    } else if($percentIncomeChange != 0 && $percentIncomeChange > 0) {
                      $compare_income = "up";
                    }
                  ?>
                  <span class="compare <?php echo $compare_income; ?>">
                    <?php if($percentIncomeChange != 0 && $percentIncomeChange < 0) { ?>
                      <i class="fas fa-caret-down"></i> 
                    <?php } else if($percentIncomeChange != 0 && $percentIncomeChange > 0) { ?>
                      <i class="fas fa-caret-up"></i> 
                    <?php } ?>
                    <?php echo $percentIncomeChange; ?>% last year
                  </span>
                </p>
              </div>
            </div>
          </div>
          
          <div class="col-3">
            <div class="list buy">
              <div>
                <i class="fas fa-coins"></i>
              </div>
              <div>
                <p class="no-margin">Jumlah Transaksi Beli</p>
                <p class="no-margin">
                  <?php echo $jumlah_transaksi_beli->total_buy; ?>
                </p>
                <p class="no-margin">
                  <?php
                    if($percentBeliChange != 0 && $percentBeliChange < 0) {
                      $compare_beli = "down";
                    } else if($percentBeliChange != 0 && $percentBeliChange > 0) {
                      $compare_beli = "up";
                    }
                  ?>
                  <span class="compare <?php echo $compare_beli; ?>">
                    <?php if($percentBeliChange != 0 && $percentBeliChange < 0) { ?>
                      <i class="fas fa-caret-down"></i> 
                    <?php } else if($percentBeliChange != 0 && $percentBeliChange > 0) { ?>
                      <i class="fas fa-caret-up"></i> 
                    <?php } ?>
                    <?php echo $percentBeliChange; ?>% last year
                  </span>
                </p>
              </div>
            </div>
          </div>
          
          <div class="col-3">
            <div class="list sell">
              <div>
                <i class="fas fa-coins"></i>
              </div>
              <div>
                <p class="no-margin">Jumlah Transaksi Jual</p>
                <p class="no-margin">
                  <?php echo $jumlah_transaksi_jual->total_sell; ?>
                </p>
                <p class="no-margin">
                  <?php
                    if($percentJualChange != 0 && $percentJualChange < 0) {
                      $compare_jual = "down";
                    } else if($percentJualChange != 0 && $percentJualChange > 0) {
                      $compare_jual = "up";
                    }
                  ?>
                  <span class="compare <?php echo $compare_jual; ?>">
                    <?php if($percentJualChange != 0 && $percentJualChange < 0) { ?>
                      <i class="fas fa-caret-down"></i> 
                    <?php } else if($percentJualChange != 0 && $percentJualChange > 0) { ?>
                      <i class="fas fa-caret-up"></i> 
                    <?php } ?>
                    <?php echo $percentJualChange; ?>% last year
                  </span>
                </p>
              </div>
            </div>
          </div>          
          
          <div class="col-3">
            <div class="list user">
              <div>
                <i class="fas fa-user-circle"></i>
              </div>
              <div>
                <p class="no-margin">Jumlah User</p>
                <p class="no-margin">
                  <?php echo $jumlah_user->total_user; ?> user
                </p>
                <p class="no-margin">
                  <?php
                    if($percentUserChange != 0 && $percentUserChange < 0) {
                      $compare_user = "down";
                    } else if($percentUserChange != 0 && $percentUserChange > 0) {
                      $compare_user = "up";
                    }
                  ?>
                  <span class="compare <?php echo $compare_user; ?>">
                    <?php if($percentUserChange != 0 && $percentUserChange < 0) { ?>
                      <i class="fas fa-caret-down"></i> 
                    <?php } else if($percentUserChange != 0 && $percentUserChange > 0) { ?>
                      <i class="fas fa-caret-up"></i> 
                    <?php } ?>
                    <?php echo $percentUserChange; ?>% last year
                  </span>
                </p>
              </div>
            </div>
          </div>

        </div>
      </div>
    </div>

    <div class="row">

      <div class="col-md-6">
        <h3 class="h4 mb-2 text-gray-800 title-table-dashboard-emas">
          Statistik Income Ladara Emas <?php echo $currentYear; ?>
        </h3>

        <div class="card shadow mb-5">
          <div class="card-header py-3">
            <span class="m-0 font-weight-bold text-primary ht_admdash">
              Tahun: 
            </span>

            <select id="filterChartThnIncomeLadaraEmas" class="jsselect">
              <?php if($currentYear !== 2020) { ?>
                <?php $addCurrentYear = $currentYear+1; ?> 
                <?php for($thn=2020; $thn<$addCurrentYear; $thn++) { ?>
                  <option value="<?php echo $thn; ?>" <?php if($currentYear == $thn) { ?>selected<?php } ?>><?php echo $thn; ?></option>
                <?php } ?>
              <?php } else { ?>
                <option value="<?php echo $currentYear; ?>"><?php echo $currentYear; ?></option>
              <?php } ?>
            </select>
          </div>

          <div class="card-body">
            <div id="loadIncomeLadaraEmas">
              <canvas id="chartIncomeLadaraEmas"></canvas>
            </div>
          </div>
        </div>
      </div>

      <div class="col-md-6">
        <h3 class="h4 mb-2 text-gray-800 title-table-dashboard-emas">
          Statistik User Emas <?php echo $currentYear; ?>
        </h3>

        <div class="card shadow mb-5">
          <div class="card-header py-3">
            <span class="m-0 font-weight-bold text-primary ht_admdash">
              Tahun: 
            </span>

            <select id="filterChartThnLadaraEmasUser" class="jsselect">
              <?php if($currentYear !== 2020) { ?>
                <?php $addCurrentYear = $currentYear+1; ?> 
                <?php for($thn=2020; $thn<$addCurrentYear; $thn++) { ?>
                  <option value="<?php echo $thn; ?>" <?php if($currentYear == $thn) { ?>selected<?php } ?>><?php echo $thn; ?></option>
                <?php } ?>
              <?php } else { ?>
                <option value="<?php echo $currentYear; ?>"><?php echo $currentYear; ?></option>
              <?php } ?>
            </select>
          </div>

          <div class="card-body">
            <div id="loadChartUserEmas">
              <canvas id="chartUserEmas"></canvas>
            </div>
          </div>
        </div>
      </div>

    </div>

  </div>

            
  <script>
    var MONTHS = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'];
    var charOptions = {
        responsive: true,
        legend: { display: false },
        title: { display: false, }
      };
      charOptions.tooltips = {};
      charOptions.tooltips.enabled = true;

    var cile = document.getElementById('chartIncomeLadaraEmas').getContext('2d');
    var charOptionsIncome = charOptions;
      charOptionsIncome.scales = {
        yAxes: [{
          ticks: {
            stepSize: 30000
          }
        }]
      };
      charOptionsIncome.tooltips.callbacks = {
        label: function(tooltipItem, data) {
          var val = data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index];
          return 'Income: ' + new Intl.NumberFormat(['ban', 'id']).format(val) + ' (' + (100 * val / 130).toFixed(2) + '%)';
        }
      };
    var chartIncomeLadaraEmas = new Chart(cile, {
      type: 'line',
      data: {
        labels: MONTHS,
        datasets: [{
          label: {
            display: false,
          },
          borderWidth: 0,
          backgroundColor: "#f7b921",
          data: [<?php foreach($income_chart[$currentYear] as $iCYear) { echo $iCYear .",";  } ?>]
        }]
      },
      options: charOptionsIncome
    });
    
    var cue = document.getElementById('chartUserEmas').getContext('2d');    
    var charOptionUser = charOptions;
      charOptionUser.scales = {
        yAxes: [{
          ticks: {
            stepSize: 10
          }
        }]
      };
      charOptionUser.tooltips.callbacks = {
        label: function(tooltipItem, data) {
          var val = data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index];
          return 'Jumlah User: ' + val + ' (' + (100 * val / 130).toFixed(2) + '%)';
        }
      };    
    var chartUserEmas = new Chart(cue, {
      type: 'bar',
      data: {
        labels: MONTHS,
        datasets: [{
          label: {
            display: false,
          },
          borderWidth: 0,
          minBarLength: 2,
          backgroundColor: "#3f6ad9",
          data: [<?php foreach($user_chart[$currentYear] as $uCYear) { echo $uCYear .",";  } ?>]
        }]
      },
      options: charOptionUser
    });

    window.onload = function() {
      chartIncomeLadaraEmas;
      chartUserEmas;
    };
  </script>

<?php endwhile; ?>
<?php else : ?>
    <?php get_template_part( 'content', '404pages' ); ?>  
<?php endif; ?>

<?php get_footer('admin'); ?>