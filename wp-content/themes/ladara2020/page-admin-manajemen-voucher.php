<?php
/*
Template Name: Manajemen Voucher
*/
?>

<?php get_header('admin'); ?>

<?php if (have_posts()) :
    while (have_posts()) :
        the_post();
        date_default_timezone_set("Asia/Jakarta");
        $nowdate = date('Y-m-d H:i:s');

        $current_user = wp_get_current_user();
        $u_id = $current_user->ID;
        $u_roles = $current_user->roles;
        $admin = 0;

        if (isset($u_roles) and !empty($u_roles)) {
            foreach ($u_roles as $key => $value) {
                if (strtolower($value) == 'administrator') {
                    $admin = 1;
                }
                if (strtolower($value) == 'shop_manager') {
                    $admin = 1;
                }
            }
        }

        if ((isset($u_id) and $u_id != 0) and $admin == 1) {
            $pageNumber = isset($_GET['page_number']) ? htmlspecialchars($_GET['page_number']) : 1;
?>

            <!-- Begin Page Content -->
            <div class="container-fluid">

                <!-- Page Heading -->
                <div class="row">
                    <div class="col-md-6">
                        <h1 class="h3 mb-2 text-gray-800">Daftar Voucher</h1>
                    </div>
                    <div class="col-md-6 mb-2" align="right">
                        <a href="<?php echo home_url(); ?>/dashboard/manajemen-voucher/tambah-voucher">
                            <button class="btn btn-primary">
                                <b>Tambah Voucher</b>
                            </button>
                        </a>
                    </div>
                </div>

                <!-- DataTales Example -->
                <div class="card shadow mb-4">
                    <div class="card-header py-3">
                        <h6 class="m-0 font-weight-bold text-primary ht_admdash">&nbsp;</h6>
                    </div>
                    <div class="card-body">
                        <div class="table-responsive">
                            <table class="table table-bordered" width="100%" cellspacing="0" data-order="[[ 0, &quot;desc&quot; ]]">
                                <thead>
                                    <tr>
                                        <th>Nama</th>
                                        <th>Target</th>
                                        <th>Mulai</th>
                                        <th>Selesai</th>
                                        <th>Voucher Terpakai</th>
                                        <th>Aksi</th>
                                    </tr>
                                </thead>
                                <tbody>

                                    <?php
                                    $offset = (intval($pageNumber) - 1) * 10;

                                    global $wpdb;
                                    $res_query = $wpdb->get_results("SELECT * FROM ldr_vouchers WHERE merchant_id = 0 AND deleted_at IS NULL LIMIT 10 OFFSET $offset");

                                    foreach ($res_query as $key => $value) {
                                        $countUsage = $wpdb->get_var("SELECT count(*) FROM ldr_voucher_usages WHERE status = 1 AND voucher_id = " . $value->id);

                                        if ($value->limit_usage > 0) {
                                            $limitUsage = $countUsage . '/' . $value->limit_usage;
                                        } else {
                                            $limitUsage = $countUsage;
                                        }
                                    ?>
                                        <tr>
                                            <td><?php echo $value->name; ?></td>
                                            <td><?php echo $value->code; ?></td>
                                            <td><?php echo $value->period_start; ?></td>
                                            <td><?php echo $value->period_end; ?></td>
                                            <td><?php echo $limitUsage; ?></td>
                                            <td class="merchant-row" id="<?php echo $value->id; ?>" style="display: flex; justify-content: space-evenly; align-items: center;">
                                                <a href="<?php echo home_url(); ?>/dashboard/manajemen-voucher/edit-voucher?id=<?php echo $value->id; ?>">
                                                    <button class="btn btn-info">
                                                        <b>Edit</b>
                                                    </button>
                                                </a>
                                                <button class="btn btn-danger" onclick="deleteConfirmation(<?= $value->id ?>)">
                                                    <b>Hapus</b>
                                                </button>
                                            </td>
                                        </tr>
                                    <?php
                                    }

                                    $countQuery = $wpdb->get_var("SELECT count(*) FROM ldr_vouchers WHERE merchant_id = 0");
                                    $perPage = $countQuery / 10;
                                    ?>

                                </tbody>
                            </table>
                        </div>
                        <div style="display: flex;">
                            <ul class="pagination" style="margin: auto;">
                                <?php
                                if ($perPage > 1) :
                                    for ($i = 1; $i <= ceil($perPage); $i++) : ?>
                                        <li>
                                            <a href="<?= home_url("/dashboard/manajemen-voucher?page_number=$i") ?>">
                                                <?= $i ?>
                                            </a>
                                        </li>
                                <?php
                                    endfor;
                                endif;
                                ?>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            <!-- /.container-fluid -->
            <script>
                function deleteConfirmation(voucherId) {
                    if (confirm("Anda yakin?")) {
                        $.ajax({
                            url: ajaxscript.ajaxurl,
                            type: 'POST',
                            data: {
                                action: 'ajaxDeleteVoucher',
                                id: voucherId
                            },
                            success: function(result) {
                                location.reload()
                            }
                        })
                    } else {
                        return false
                    }
                }
            </script>
            <style>
            .pagination>li {
                border: 1px solid #e5e5e5;
                padding: 10px;
                width: 40px;
                text-align: center;
            }
            </style>
        <?php
        } else { ?>
            <script>
                // 'Getting' data-attributes using getAttribute
                var plant = document.getElementById('body');
                console.log(plant);
                var hurl = plant.getAttribute('data-hurl');
                location.replace(hurl + '/login/');
            </script>
        <?php } ?>

    <?php endwhile; ?>
<?php else : ?>
    <?php get_template_part('content', '404pages'); ?>
<?php endif; ?>
<?php get_footer('admin'); ?>