<?php
/*
Template Name: Payment Page
*/

require 'vendor/autoload.php';

use Xendit\VirtualAccounts;

?>
<?php get_header(); ?>

<?php if (have_posts()) :
    while (have_posts()) :
        the_post();

        global $wpdb;
        date_default_timezone_set("Asia/Jakarta");
        $nowdate = date('Y-m-d H:i:s');

        $currentUser = wp_get_current_user();
        $userId = $currentUser->ID;
        $identifier = $_SESSION['identifier'];
        $voucherId = isset($_SESSION['voucherId'][$identifier]) ? $_SESSION['voucherId'][$identifier] : 0;

        $result_bank = VirtualAccounts::getVABanks();

        $result_bank = array_filter((array) $result_bank, function ($fn) {
            return $fn['code'] !== 'BCA';
        });

        if ($order !== null) {
?>

            <style type="text/css">
                @media only screen and (max-width:769px) {

                    .row_fix_mobmenu,
                    footer,
                    .col_mn_search,
                    .col_mn_profile {
                        display: none !important;
                    }

                    .col_mn_logo {
                        width: 100%;
                    }
                }
            </style>

            <form action="" name="form_paynow" class="form_paynow" id="form_paynow" onsubmit="submitPayNow(event);" method="post" style="width: 100%;">
                <div class="row row_masterCart">
                    <div class="col-md-12 col_masterCart">
                        <a href="<?php echo home_url(); ?>/shop/">
                            <div class="bx_backShop">
                                <span class="glyphicon glyphicon-menu-left"></span> Lanjutkan Belanja
                            </div>
                        </a>
                        <div class="bx_tabsCheckout des_display">
                            <div class="tb_1chk act">
                                <div class="sp_tabsch act">1</div>Pengiriman
                            </div>
                            <span class="line_tabsch"></span>
                            <div class="tb_1chk act">
                                <div class="sp_tabsch act">2</div>Pembayaran
                            </div>
                        </div>
                        <div class="bx_checkout_page">
                            <h1 class="ht_masterCheckout">Pembayaran</h1>
                            <div class="alert alert-danger chk_err">
                                <!-- Checkout Errors -->
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-7 col_checkout_page1">
                                <div class="box_mainpay" style="pointer-events: none; opacity: 0.5;">
                                    <div class="ht_mainpay">Pilih Metode Pembayaran</div>
                                    <div class="box_listpay">
                                        <div class="col_listpay">
                                            <label class="cont_rad" data-type="tab_va">Transfer melalui Virtual Account
                                                <input type="radio" checked="checked" name="paymethod" value="va" class="select_payment">
                                                <span class="ckm"><span class="cir"></span></span>
                                            </label>
                                            <div id="tab_va" class="box_cont_listpay act">
                                                <input type="hidden" name="chs_bank" />
                                                <div class="div_select_bank">
                                                    <div id="selected_va">
                                                        Pilih Bank
                                                    </div>
                                                    <div class="bx_bankMenu" style="right: 15px;">
                                                        <ul class="mn_bankMenu">
                                                            <?php
                                                            if (isset($result_bank) and !empty($result_bank)) {
                                                                foreach ($result_bank as $key => $value) {
                                                                    $bank_name = $value['name'];
                                                                    $bank_code = $value['code'];
                                                            ?>
                                                                    <li class="select_payment" data-va="<?php echo strtoupper($bank_code); ?>" style="padding-left: 15px; padding: 10px; border-bottom: 1px solid rgba(0,0,0,.1);">
                                                                        <a>
                                                                            <div>
                                                                                <img class="mg_mn_voucher" src="<?php bloginfo('template_directory'); ?>/library/images/<?php echo strtolower($bank_code); ?>_va.png">
                                                                            </div>
                                                                            <div style="width: 80%;">
                                                                                <?php echo strtoupper($bank_code); ?> VIRTUAL ACCOUNT
                                                                            </div>
                                                                        </a>
                                                                    </li>
                                                            <?php
                                                                }
                                                            }
                                                            ?>
                                                        </ul>
                                                    </div>
                                                </div>
                                                <div class="info_listpay">
                                                    <span id="payment_method_desc_va">
                                                        <b>Ketentuan:</b> <br />
                                                        Harap memilih salah satu opsi pembayaran.
                                                    </span>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col_listpay">
                                            <label class="cont_rad" data-type="tab_cc">Kartu Kredit
                                                <input type="radio" name="paymethod" value="cc" class="select_payment">
                                                <span class="ckm"><span class="cir"></span></span>
                                            </label>
                                            <div id="tab_cc" class="box_cont_listpay">
                                                <div class="info_listpay">
                                                    <b>Ketentuan:</b> <br />
                                                    Selesaikan pembayaran dalam waktu 1 jam untuk menghindari pembatalan transaksi secara otomatis.
                                                </div>
                                                <div class="mg_cartBottom">
                                                    <img src="<?php bloginfo('template_directory'); ?>/library/images/icon_visa.png">
                                                    <img src="<?php bloginfo('template_directory'); ?>/library/images/icon_mastercard.png">
                                                    <img src="<?php bloginfo('template_directory'); ?>/library/images/icon_jcb.png">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col_listpay">
                                            <label class="cont_rad" data-type="tab_ewallet">Pembayaran Instan
                                                <input type="radio" name="paymethod" value="ewallet" class="select_payment">
                                                <span class="ckm"><span class="cir"></span></span>
                                            </label>
                                            <div id="tab_ewallet" class="box_cont_listpay">
                                                <input type="hidden" name="chs_ewallet" />
                                                <input type="hidden" name="nohp" />
                                                <div class="div_select_ewallet">
                                                    <div id="selected_ewallet">
                                                        Pilih E-Wallet
                                                    </div>
                                                    <div class="bx_ewalletMenu" style="right: 15px;">
                                                        <ul class="mn_ewalletMenu">
                                                            <li class="select_payment" data-ewallet="dana" style="padding-left: 15px; padding: 10px; border-bottom: 1px solid rgba(0,0,0,.1);">
                                                                <a>
                                                                    <div align="center">
                                                                        <img class="mg_mn_voucher" src="<?php bloginfo('template_directory'); ?>/library/images/dana_wallet.png">
                                                                    </div>
                                                                    <div style="width: 80%;">
                                                                        DANA
                                                                    </div>
                                                                </a>
                                                            </li>
                                                            <li class="select_payment" data-ewallet="linkaja" style="padding-left: 15px; padding: 10px; border-bottom: 1px solid rgba(0,0,0,.1);">
                                                                <a>
                                                                    <div align="center">
                                                                        <img class="mg_mn_voucher" src="<?php bloginfo('template_directory'); ?>/library/images/linkaja_wallet.png" style="max-width: 40px;">
                                                                    </div>
                                                                    <div style="width: 80%;">
                                                                        LinkAja
                                                                    </div>
                                                                </a>
                                                            </li>
                                                            <?php
                                                            /*
                                                            <li class="select_payment" data-ewallet="ovo" style="padding-left: 15px; padding: 10px; border-bottom: 1px solid rgba(0,0,0,.1);">
                                                                <a>
                                                                    <div align="center">
                                                                        <img class="mg_mn_voucher" src="<?php bloginfo('template_directory'); ?>/library/images/ovo_wallet.png">
                                                                    </div>
                                                                    <div style="width: 80%;">
                                                                        OVO
                                                                    </div>
                                                                </a>
                                                            </li>
                                                            */
                                                            ?>
                                                        </ul>
                                                    </div>
                                                </div>
                                                <div class="info_listpay">
                                                    <span id="payment_method_desc_ewallet">
                                                        <b>Ketentuan:</b> <br />
                                                        Harap memilih salah satu opsi pembayaran.
                                                    </span>
                                                </div>
                                            </div>
                                        </div>
                                        <?php
                                        /*
                                        <div class="col_listpay">
                                            <label class="cont_rad" data-type="tab_retail">Gerai Retail
                                                <input type="radio" name="paymethod" value="retail" class="select_payment">
                                                <span class="ckm"><span class="cir"></span></span>
                                            </label>
                                            <div id="tab_retail" class="box_cont_listpay">
                                                <input type="hidden" name="chs_retail" />
                                                <div class="div_select_retail">
                                                    <div id="selected_retail">
                                                        Pilih Gerai
                                                    </div>
                                                    <div class="bx_retailMenu" style="right: 15px;">
                                                        <ul class="mn_retailMenu">
                                                            <li class="select_payment" data-retail="alfamart" style="padding-left: 15px; padding: 10px; border-bottom: 1px solid rgba(0,0,0,.1);">
                                                                <a>
                                                                    <div align="center">
                                                                        <img class="mg_mn_voucher" src="https://dashboard.xendit.co/images/alfamart-logo.svg">
                                                                    </div>
                                                                    <div style="width: 80%;">
                                                                        Alfamart
                                                                    </div>
                                                                </a>
                                                            </li>
                                                            <li class="select_payment" data-retail="indomaret" style="padding-left: 15px; padding: 10px; border-bottom: 1px solid rgba(0,0,0,.1);">
                                                                <a>
                                                                    <div align="center">
                                                                        <img class="mg_mn_voucher" src="https://dashboard.xendit.co/images/indomaret-logo.svg">
                                                                    </div>
                                                                    <div style="width: 80%;">
                                                                        Indomaret
                                                                    </div>
                                                                </a>
                                                            </li>
                                                        </ul>
                                                    </div>
                                                </div>
                                                <div class="info_listpay">
                                                    <span id="payment_method_desc_retail">
                                                        <b>Ketentuan:</b> <br />
                                                        Harap memilih salah satu opsi pembayaran.
                                                    </span>
                                                </div>
                                            </div>
                                        </div>
                                        */
                                        ?>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-5 col_checkout_page2">
                                <div class="box_mainpay">
                                    <div class="ht_mainpay">Ringkasan Belanja</div>
                                    <div class="box_listpay">
                                        <div class="f_total_pembayaran">
                                            <strong>Total Pembayaran </strong>
                                            <strong>
                                                <div class="f_totalpay">
                                                    <span id="total-payment" class="f_totalpay <?= ($voucherId > 0 ? 'line-through' : '') ?>">
                                                        Rp 0
                                                    </span>
                                                    <?php if ($voucherId > 0) { ?>
                                                        <br />
                                                        <span id="total-payment-discount" class="f_totalpay">
                                                            Rp 0
                                                        </span>
                                                    <?php } ?>
                                                </div>
                                            </strong>
                                        </div>
                                        <div class="f_total_listpay">
                                            Total HARGA <span id="total-price">Rp 0</span>
                                        </div>
                                        <div class="f_total_listpay">
                                            Biaya Pengiriman <span id="total-shipment">Rp 0</span>
                                        </div>
                                        <div class="f_total_listpay">
                                            Asuransi Pengiriman <span id="total-insurance">Rp 0</span>
                                        </div>
                                        <?php if ($voucherId > 0) {
                                            $voucher = $wpdb->get_row("SELECT * FROM ldr_vouchers WHERE id = $voucherId");
                                        ?>

                                            <div class="f_total_listpay">
                                                Voucher (<?= $voucher->code ?>)
                                                <span class="v_voucheramount">
                                                    Rp 0
                                                </span>
                                            </div>

                                        <?php
                                        }
                                        ?>
                                        <div class="f_total_listpay">
                                            Biaya Penanganan <span class="v_feepayment">free</span>
                                        </div>
                                        <div class="f_total_listbank">
                                            Metode Pembayaran <b><span class="v_namebank"></span></b>
                                        </div>
                                        <div class="f_aform f_aform_brand">
                                            <label class="cont_check">Saya setuju dengan <a target="_blank" href="<?php echo home_url(); ?>/syarat-ketentuan/">Syarat &amp; Ketentuan</a>
                                                <input type="checkbox" name="agreement" class="check_catVoucher" value="yes">
                                                <span class="checkmark"></span>
                                            </label>
                                        </div>
                                        <input type="button" class="sub_cartCheckout mob_sub_paymentNow" value="Bayar Sekarang" disabled="disabled">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </form>

            <?php get_template_part('content', 'popewallet'); ?>
        <?php
        } else {
        ?>
            <div class="row row_finishCheckout">
                <div class="col-md-12 col_finishCheckout">
                    <a href="<?php echo home_url(); ?>">
                        <div class="bx_backShop">
                            <span class="glyphicon glyphicon-menu-left"></span> Kembali ke Home
                        </div>
                    </a>
                    <div class="bx_finishCheckout">
                        <div class="mg_registerIcon">
                            <img class="lazy" data-src="<?php bloginfo('template_directory'); ?>/library/images/ico_cartempty.svg">
                        </div>
                        <div class="ht_register">Duh kok kosong? tas belanjamu bisa masuk angin nih</div>

                        <div class="ht_sucs_register">
                            Yuk isi dengan produk terbaru atau produk yang sudah kamu mimpikan dari kemarin.
                        </div>
                        <div class="bx_def_checkout">
                            <a href="<?php echo home_url(); ?>/shop/">
                                <button class="btn_def_checkout">Belanja Lagi</button>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        <?php
        }
        ?>

        <?php // ============= cannot back =============
        ?>
        <script type="text/javascript">
            (function(global) {
                if (typeof(global) === "undefined") {
                    throw new Error("window is undefined");
                }
                var _hash = "!";
                var noBackPlease = function() {
                    global.location.href += "#";
                    // making sure we have the fruit available for juice (^__^)
                    global.setTimeout(function() {
                        global.location.href += "!";
                    }, 50);
                };
                global.onhashchange = function() {
                    if (global.location.hash !== _hash) {
                        global.location.hash = _hash;
                    }
                };
                global.onload = function() {
                    noBackPlease();
                    // disables backspace on page except on input fields and textarea..
                    document.body.onkeydown = function(e) {
                        var elm = e.target.nodeName.toLowerCase();
                        if (e.which === 8 && (elm !== 'input' && elm !== 'textarea')) {
                            e.preventDefault();
                        }
                        // stopping event bubbling up the DOM tree..
                        e.stopPropagation();
                    };
                }
            })(window);
        </script>

    <?php endwhile; ?>
<?php else : ?>
    <?php get_template_part('content', '404pages'); ?>
<?php endif; ?>
<?php get_footer(); ?>