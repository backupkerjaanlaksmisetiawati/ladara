<?php
/*
Template Name: Payment Confirm Page
*/

use GuzzleHttp\Client;
use Ladara\Helpers\builder\OrderBuilder;
use Ladara\Helpers\builder\PaymentBuilder;
use Ladara\Helpers\builder\ShippingBuilder;
use Ladara\Helpers\builder\UsersBuilder;
use Ladara\Helpers\email\EmailHelper;
use Ladara\Models\Notifications;
use Ladara\Models\OrderModel;

?>

<?php get_header(); ?>
<?php if (have_posts()) :
    while (have_posts()) :
        the_post();
?>
        <style type="text/css">
            .header,
            .footer {
                display: none !important;
            }
        </style>

        <?php
        global $wpdb;
        date_default_timezone_set("Asia/Jakarta");
        $nowdate = date('Y-m-d H:i:s');
        $orderId = '';
        $orderType = '';
        $httpClient = new Client();

        $inputData = json_decode(file_get_contents('php://input'), true);
        ddlog(json_encode($inputData), 'the_dragon');

        if (isset($inputData) && $inputData != '' && !empty($inputData)) {
            $xenditId = $inputData['id'];
            $status = $inputData['status'] ?? '';
            $paymentMethod = $inputData['payment_method'];
            $paidAt = $inputData['paid_at'] ?? '';

            $isEwallet = isset($inputData['ewallet_type']);
            $isRetail = isset($inputData['retail_outlet_name']);

            if ($isEwallet) {
                $xenditId = $inputData['external_id'];

                if ($inputData['ewallet_type'] === 'DANA') {
                    $status = ($status !== '' ? $status : $inputData['payment_status']);
                    $paidAt = $inputData['transaction_date'];
                } else {
                    $paidAt = date("Y-m-d\TH:i:s.000\Z");
                }
            } else if ($isRetail) {
                $xenditId = $inputData['fixed_payment_code_id'];
                $paidAt = $inputData['transaction_timestamp'];
            }

            if ($xenditId !== '') {
                $queryPaymentLog = $wpdb->get_row("SELECT * FROM ldr_payment_log WHERE xendit_id = '$xenditId'");

                if ($queryPaymentLog !== null) { // if exist
                    $wpdb->update('ldr_payment_log', [
                        'status' => $status,
                        'paid_at' => $paidAt
                    ], [
                        'xendit_id' => $xenditId
                    ]);
                } else {
                    if ($_SERVER['SERVER_NAME'] == 'staging.ladara.id') {
                        $url = 'https://testing.ladara.id/payment-confirmation';
                        $ch = curl_init($url);
                        $payload = json_encode($inputData);
                        curl_setopt($ch, CURLOPT_POSTFIELDS, $payload);
                        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type:application/json'));
                        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                        $result = curl_exec($ch);
                        curl_close($ch);
                    }

                    die();
                }

                // =============== IF PAYMENT FOR PRODUCT =====================
                if ($queryPaymentLog->type === 'product') {
                    if (strtolower($status) === 'paid' || strtolower($status) === 'settled' || strtolower($status) === 'completed' || strtolower($status) === 'success_completed') {
                        $orderMerchants = $wpdb->get_results("SELECT * FROM ldr_order_merchants WHERE deleted_at IS NULL AND order_id = " . $queryPaymentLog->order_id);

                        foreach ($orderMerchants as $orderMerchant) {
                            $wpdb->update('ldr_order_merchants', [
                                'status' => 1
                            ], [
                                'id' => $orderMerchant->id
                            ]);

                            $wpdb->insert('ldr_order_merchant_logs', [
                                'order_merchant_id' => $orderMerchant->id,
                                'status' => 1,
                                'log' => 'Pembayaran Order ' . $orderMerchant->id . ' telah diterima. Menunggu konfirmasi Penjual.',
                                'created_at' => $nowdate,
                                'updated_at' => $nowdate
                            ]);

                            $request = $httpClient->request('POST', MERCHANT_URL . '/api/shipper/create-shipment-order', [
                                'headers' => [
                                    'Accept' => 'application/json'
                                ],
                                'json' => [
                                    'order_merchant_id' => $orderMerchant->id
                                ]
                            ]);
                        }

                        $queryOrder = $wpdb->get_row("SELECT * FROM ldr_orders o LEFT JOIN ldr_users u ON u.ID = o.user_id WHERE o.id = " . $queryPaymentLog->order_id);
                        $queryOrderMerchants = $wpdb->get_results("SELECT * FROM ldr_order_merchants WHERE order_id = " . $queryPaymentLog->order_id);

                        foreach ($queryOrderMerchants as $orderMerchant) {
                            // Send Notification to Merchant
                            Notifications::addNotification([
                                'merchantId' => $orderMerchant->merchant_id,
                                'title' => 'Pesanan Baru Diterima.',
                                'descriptions' => 'Anda telah menerima pesanan baru, silahkan lakukan konfirmasi untuk menghindari terkena denda karena pesanan di batalkan secara otomatis.',
                                'type' => 'pesanan',
                                'orderId' => $queryPaymentLog->order_id
                            ]);

                            if ($orderMerchant->rejection_id != null) {
                                $wpdb->update('ldr_order_merchant_rejections', [
                                    'status' => 1
                                ], [
                                    'id' => $orderMerchant->rejection_id
                                ]);
                            }
                        }

                        // Send Notification to User
                        Notifications::addNotification([
                            'userId' => $queryOrder->user_id,
                            'title' => 'Pembayaran Pesanan #' . $queryPaymentLog->order_id . ' telah diterima.',
                            'descriptions' => 'Pembayaran Anda sudah di terima, penjual akan segera menyiapkan pesanan Anda.',
                            'type' => 'pesanan',
                            'orderId' => $queryPaymentLog->order_id
                        ]);

                        // Send email
                        $request = $httpClient->request('POST', MERCHANT_URL . '/api/orders/send-email-payment-success', [
                            'headers' => [
                                'Accept' => 'application/json'
                            ],
                            'json' => [
                                'order_id' => $queryPaymentLog->order_id
                            ]
                        ]);

                        if ($queryOrder->voucher_id != null) {
                            $queryVoucher = $wpdb->get_row("SELECT * FROM ldr_vouchers WHERE id = " . $queryOrder->voucher_id);

                            $wpdb->update('ldr_voucher_usages', [
                                'status' => 1
                            ], [
                                'voucher_id' => $queryOrder->voucher_id,
                                'user_id' => $queryOrder->user_id
                            ]);

                            $wpdb->insert('ldr_voucher_logs', [
                                'voucher_id' => $queryOrder->voucher_id,
                                'user_id' => $queryOrder->user_id,
                                'log' => ($queryOrder->user_email ?? '') . ' telah menggunakan kode voucher ' . ($queryVoucher->code ?? '') . ' pada Pesanan #' . $queryOrder->id
                            ]);
                        }
                    } elseif (strtolower($status) == 'pending') { // jika status = pending / on-hold
                        $orderMerchants = $wpdb->get_results("SELECT * FROM ldr_order_merchants WHERE deleted_at IS NULL AND order_id = " . $queryPaymentLog->order_id);

                        foreach ($orderMerchants as $orderMerchant) {
                            $wpdb->update('ldr_order_merchants', [
                                'status' => 0
                            ], [
                                'id' => $orderMerchant->id
                            ]);

                            $wpdb->insert('ldr_order_merchant_logs', [
                                'order_merchant_id' => $orderMerchant->id,
                                'status' => 0,
                                'log' => 'Pembayaran Order ' . $orderMerchant->id . ' sedang dicek dan diproses Ladara.',
                                'created_at' => $nowdate,
                                'updated_at' => $nowdate
                            ]);
                        }
                    } else {
                        $orderMerchants = $wpdb->get_results("SELECT * FROM ldr_order_merchants WHERE deleted_at IS NULL AND order_id = " . $queryPaymentLog->order_id);

                        foreach ($orderMerchants as $orderMerchant) {
                            $wpdb->update('ldr_order_merchants', [
                                'status' => -1
                            ], [
                                'id' => $orderMerchant->id
                            ]);

                            $wpdb->insert('ldr_order_merchant_logs', [
                                'order_merchant_id' => $orderMerchant->id,
                                'status' => 0,
                                'log' => 'Pembayaran Pesanan #' . $orderMerchant->id . ' dibatalkan oleh sistem.',
                                'created_at' => $nowdate,
                                'updated_at' => $nowdate
                            ]);
                        }

                        // Send email
                        $httpClient = new Client();

                        $request = $httpClient->request('POST', MERCHANT_URL . '/api/orders/send-email-order-cancelled', [
                            'headers' => [
                                'Accept' => 'application/json'
                            ],
                            'json' => [
                                'order_id' => $queryPaymentLog->order_id
                            ]
                        ]);
                    }
                } elseif(strtolower($queryPaymentLog->type) === 'emas') { // If payment for emas
                    // query BELI EMAS sukses payment order

                    $transaksi = $wpdb->get_row(
                        "SELECT * FROM ldr_transaksi_emas WHERE xendit_id='$xenditId'",
                        OBJECT
                    );

                    $gld_payment_datas = [
                        "status" => $status,
                        "xendit_id" => $xenditId
                    ];
                    gld_payment($gld_payment_datas);
                } elseif(strtolower($queryPaymentLog->type) === 'ppob') { // If payment for PPOB
                    $transaksi = $wpdb->get_row(
                        "SELECT * FROM ldr_topup_transactions WHERE id=".$queryPaymentLog->order_id,
                        OBJECT
                    );

                    // call api payment notify ppob
                    $params = [
                        "topup_phoneno"         => $transaksi->customer_no,
                        "topup_customer_name"   => $transaksi->customer_name,
                        "topup_amount"          => $transaksi->amount,
                        "bni_reference_id"      => $transaksi->bni_reference_id,
                        "topup_fee_admin"       => $transaksi->fee_admin,
                        "topup_fee_partner"     => $transaksi->fee_partner,
                    ];
                }
            }
        }
        ?>

        <div id="content">
            <div id="inner-content" class="wrap clearfix">
                <div id="main" class="eightcol first clearfix" role="main">
                    <article id="post-not-found" class="hentry clearfix">
                        <div class="row row_finishCheckout">
                            <div class="col-md-12 col_finishCheckout">
                                <a href="<?php echo home_url(); ?>">
                                    <div class="bx_backShop">
                                        <span class="glyphicon glyphicon-menu-left"></span> Kembali ke Home
                                    </div>
                                </a>
                                <div class="bx_finishCheckout">
                                    <div class="mg_registerIcon">
                                        <img class="lazy" data-src="<?php bloginfo('template_directory'); ?>/library/images/ico_404.jpg">
                                    </div>
                                    <div class="ht_register">Ups, halaman yang kamu cari belum bisa ditemukan :(</div>
                                    <div class="ht_sucs_register">
                                        Coba lagi dengan menekan tombol dibawah ini ya!
                                    </div>
                                    <div class="bx_def_checkout">
                                        <a href="<?php echo home_url(); ?>/">
                                            <button class="btn_def_checkout">Kembali ke Beranda</button>
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </article>
                </div>
            </div>
        </div>
    <?php endwhile; ?>
<?php else : ?>
    <?php get_template_part('content', '404pages'); ?>
<?php endif; ?>
<?php get_footer(); ?>