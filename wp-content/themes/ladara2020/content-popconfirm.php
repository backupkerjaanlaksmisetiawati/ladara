<div class="row fix_alertCustom">
  <div class="col-md-4"></div>
  <div class="col-md-4 col_alertCustom animated zoomIn">
    <div class="mg_alertCustom"><span class="glyphicon glyphicon-exclamation-sign"></span></div>
    <div id="ht_alertCustom" class="ht_alertCustom">Anda yakin menghapus ?</div>

    <div class="btn_alertCustom">
      <a id="close_alertCustom" class="a_close_alertCustom">Cancel</a>
      <a id="confirm_alertCustom" class="a_confirm_alertCustom">Setuju</a>
    </div>
  </div>
  <div class="col-md-4"></div>
</div>