<?php
/*
Template Name: Add Ulasan
*/
?>

<?php get_header(); ?>
<?php if (have_posts()) :
    while (have_posts()) :
        the_post();

        global $wpdb;

        $currentUser = wp_get_current_user();
        $userId = $currentUser->ID;
        $orderMerchantId = htmlspecialchars($_GET['order_merchant_id'] ?? 0);
        $productId = htmlspecialchars($_GET['product_id'] ?? 0);

        $product = getProductById($productId);
        $merchant = getMerchantByOrderMerchant($orderMerchantId);

        if (isset($userId) and $userId != 0): ?>
            <style>
                .box-ulasan{
                    border: 1px solid #DDDDDD;
                    border-radius: 20px;
                    padding: 20px 20px 50px 20px;
                }
                .box-ulasan .box-form{
                    border: 1px solid #EBE9E2;
                    border-radius: 10px;
                    padding: 0;
                }
                .box-form .header{
                    background: #F3F3F3 0% 0% no-repeat padding-box;
                    border-radius: 10px 10px 0px 0px;
                    height: 50px;
                    padding: 15px;
                }
                .mt-20{
                    margin-top: 20px;
                }
                .nopd{
                    padding: 0;
                }
                .ulasan-title{
                    font-size: 16px;
                    font-weight: bold;
                    margin-top: 0;
                }
                .ulasan-start{
                    color: #FFC107;
                    font-size: 25px;
                    cursor: pointer;
                }
                .ulasan-file-input{

                }
                .btn-batal, .btn-batal:hover{
                    background: #FFFFFF 0% 0% no-repeat padding-box;
                    border: 2px solid #0080FF;
                    border-radius: 147px;
                    padding: 5px 20px;
                    min-width: 130px;
                    color: #0080FF;
                    float: right;
                    text-align: center;
                }
                .btn-kirim{
                    background: #0080FF 0% 0% no-repeat padding-box;
                    border-radius: 147px;
                    color: #ffffff;
                    padding: 7px 20px;
                    border: 1px solid;
                    float: right;
                }
                .upload-btn-wrapper {
                    position: relative;
                    overflow: hidden;
                    display: inline-block;
                    cursor: pointer;
                }
                .upload-btn-wrapper .btn{
                    width: 100px;
                    height: 100px;
                }

                .upload-btn-wrapper input[type=file] {
                    font-size: 100px;
                    position: absolute;
                    left: 0;
                    top: 0;
                    opacity: 0;
                }

            </style>
            <div class="row"></div>
            <div id="myorderpage" class="row row_profile">
                <div class="col-md-3 col_profile des_display">
                    <?php get_template_part('content', 'menu-profile'); ?>
                </div>
                <div class="col-md-9 col_profile">
                    <div class="row row_cont_tab_profile">
                        <h1 class="ht_profile ht_myvoucher">Tambah Ulasan</h1>
                        <div class="col-md-12 box-ulasan">
                            <div class="row">
                                <a href="/my-order">< Kembali</a>
                            </div>
                            <div class="row mt-20">
                                <div class="col-md-12 box-form">
                                    <div class="row">
                                        <div class="col-md-12 header">
                                            <div class="col-md-4 nopd" style="font-size: 12px;text-align: left;">
                                                Penjual <a href="<?php echo 'https://'.$merchant->url?>" style="color: #FE5461;"><?php echo $merchant->name?></a>
                                            </div>
                                            <div class="col-md-8 nopd" style="font-size: 12px;text-align: right;">
                                                Pesanan diterima: <?php echo date('d-m-Y H:i:s', strtotime($merchant->updated_at))?>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row mt-20">
                                        <form id="form-ulasan" method="post">
                                            <input type="hidden" name="order_merchant_id" value="<?php echo $orderMerchantId;?>">
                                            <input type="hidden" name="product_id" value="<?php echo $productId;?>">
                                            <input type="hidden" id="rating" name="rating" value="1">
                                            <div class="col-md-2">
                                                <img style="max-width: 100%;border: 1px solid #EBE9E2;"  src="<?php echo IMAGE_URL.'/'.$product->path; ?>" alt="">
                                            </div>
                                            <div class="col-md-10"  style="margin-bottom: 30px;">
                                                <div style="border-bottom: 1px solid #EBE9E2;">
                                                    <h2 class="ulasan-title"><?php echo $product->title;?></h2>
                                                </div>
                                                <div>
                                                    <span style="font-size: 12px;">Bagaimana kualitas produk ini?</span>
                                                </div>
                                                <div>
                                                    <span class="glyphicon glyphicon-star ulasan-start aj-ulasan-start" data-rating="1" aria-hidden="true"></span>
                                                    <span class="glyphicon glyphicon-star-empty ulasan-start aj-ulasan-start" data-rating="2" aria-hidden="true"></span>
                                                    <span class="glyphicon glyphicon-star-empty ulasan-start aj-ulasan-start" data-rating="3" aria-hidden="true"></span>
                                                    <span class="glyphicon glyphicon-star-empty ulasan-start aj-ulasan-start" data-rating="4" aria-hidden="true"></span>
                                                    <span class="glyphicon glyphicon-star-empty ulasan-start aj-ulasan-start" data-rating="5" aria-hidden="true"></span>
                                                </div>
                                                <div class="mt-20">
                                                    <span style="font-size: 12px;">Berikan ulasan tentang produk ini</span>
                                                </div>
                                                <div class="mt-20">
                                                    <textarea name="review" placeholder="Tuliskan deskripsi Anda mengenai produk ini ..." style="background: #F6F6F6 0% 0% no-repeat padding-box;border: 1px solid #F2F2F2;width: 100%;resize: none;" rows="5"></textarea>
                                                </div>
                                                <div class="mt-20 ">
                                                    <div class="upload-btn-wrapper">
                                                        <button class="btn icon">
                                                            <span class="glyphicon glyphicon-camera" style="font-size: 25px;display: block;"></span>
                                                            <span style="font-size: 12px;">Unggah Foto</span>
                                                        </button>
                                                        <div class="btn div-preview" style="display: none;padding: 0;">
                                                            <img style="max-width: 100%;" src="" class="img-preview"  alt="">
                                                        </div>
                                                        <input name="images[]" type="file" accept="image/*"  class="ulasan-file-input ulasan-images">
                                                    </div>
                                                    <div class="upload-btn-wrapper">
                                                        <button class="btn icon">
                                                            <span class="glyphicon glyphicon-plus" style="font-size: 25px;display: block;"></span>
                                                        </button>
                                                        <div class="btn div-preview" style="display: none;padding: 0;">
                                                            <img style="max-width: 100%;" src="" class="img-preview"  alt="">
                                                        </div>
                                                        <input name="images[]" type="file" accept="image/*"  class="ulasan-file-input ulasan-images">

                                                    </div>
                                                    <div class="upload-btn-wrapper">
                                                        <button class="btn icon">
                                                            <span class="glyphicon glyphicon-plus" style="font-size: 25px;display: block;"></span>
                                                        </button>
                                                        <div class="btn div-preview" style="display: none;padding: 0;">
                                                            <img style="max-width: 100%;" src="" class="img-preview"  alt="">
                                                        </div>
                                                        <input name="images[]" type="file" accept="image/*"  class="ulasan-file-input ulasan-images">
                                                    </div>
                                                    <span style="display: block;font-size: 12px;">Ukuran gambar: maksimal 300kb, Format gambar: .JPG .JPEG .PNG</span>
                                                </div>
                                                <div class="mt-20"  style="border-bottom: 1px solid #EBE9E2;">
                                                </div>
                                                <div class="alert alert-danger hide ulasan-error"></div>
                                                <div class="alert alert-success hide ulasan-success"></div>
                                                <div class="mt-20">
                                                    <button type="submit" class="btn-kirim">Kirim Ulasan</button>
                                                    <a href="/my-order" class="btn-batal">Batal</a>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
       <?php else: ?>
            <script>
                // 'Getting' data-attributes using getAttribute
                var plant = document.getElementById('body');
                console.log(plant);
                var hurl = plant.getAttribute('data-hurl');
                location.replace(hurl + '/login/');
            </script>
        <?php endif; ?>

    <?php endwhile; ?>
<?php else : ?>
    <?php get_template_part('content', '404pages'); ?>
<?php endif; ?>
<?php get_footer(); ?>