<?php
/*
Template Name: Download SO Old
*/
?>
<?php get_header(); 
 if (have_posts()) : while (have_posts()) : the_post(); ?>

<style type="text/css">
  header, footer{
    display: none !important;
  }
.box_filterReport{
    margin-bottom: 30px;
}
.sel_filterReport{
    width: 100%;
    height: 30px;
    line-height: 35px;
    font-size: 12px;
    color: #000;
    font-weight: 500;
    border-radius: 3px;
    border: 1px solid rgba(0,0,0,.2);
    padding-left: 10px;
    padding-right: 10px;
}
.txt_filterReport{
    width: 100%;
    height: 30px;
    line-height: 35px;
    font-size: 12px;
    color: #000;
    font-weight: 500;
    border-radius: 3px;
    border: 1px solid rgba(0,0,0,.2);
    padding-left: 10px;
    padding-right: 10px;
}
.sub_filterReport{
    background: #2a96a5;
    font-size: 12px;
    font-weight: 600;
    text-transform: uppercase;
    letter-spacing: .3px;
    color: #fff;
    width: auto;
    line-height: 30px;
    border: 0;
    border-radius: 3px;
    padding-left: 15px;
    padding-right: 15px;
    margin-top: 20px;
}
</style>

<?php 
date_default_timezone_set("Asia/Jakarta");
$nowdate = date('Y-m-d');

$current_user = wp_get_current_user();
$u_id = $current_user->ID;
$u_roles = $current_user->roles;
$admin = 0;

if(isset($u_roles) AND !empty($u_roles)){
  foreach ($u_roles as $key => $value) {
      if(strtolower($value) == 'administrator'){
          $admin = 1;
      }
  }
}

if((isset($u_id) OR $u_id != 0) AND $admin == 1){

if(isset($_GET['so'])){
  $so_status = $_GET['so'];
  $tx_status = $_GET['so'];
}else{
  $so_status = 'wc-all';
  $tx_status = 'wc-all';
}

$order_statuses = array($so_status); 
if(strtolower($so_status) == 'wc-all'){
  $order_statuses = array('wc-pending','wc-on-hold','wc-processing','wc-preparing','wc-ondelivery','wc-completed','wc-cancelled');
  // $order_statuses = array('wc-processing','wc-preparing','wc-ondelivery','wc-completed');
  $tx_status = 'All Status';
}

if(isset($_GET['from_date'])){
  $from_date = $_GET['from_date'];
}else{
  $from_date = $nowdate;
}
if(isset($_GET['to_date'])){
  $to_date = $_GET['to_date'];
}else{
  $to_date = $nowdate;
}

// if(isset($_GET['us_id']) AND $_GET['us_id'] != ''){
//   $us_id = $_GET['us_id'];
//   $query_user = "AND user_id = '$us_id'";
//   $user_data = get_userdata( $us_id );
//   $user_name = $user_data->data->display_name;
// }else{
//   $us_id = '';
//   $query_user = " ";
//   $user_name = 'Semua';
// }

$data_status = array();
$data_status['wc-all'] = 'All';
$data_status['wc-pending'] = 'Pending';
$data_status['wc-on-hold'] = 'On Hold';
$data_status['wc-processing'] = 'Processing';
$data_status['wc-preparing'] = 'Preparing';
$data_status['wc-ondelivery'] = 'On Delivery';
$data_status['wc-completed'] = 'Completed';
$data_status['wc-cancelled'] = 'Cancelled';
?>

  <!-- Begin Page Content -->
  <div class="container-fluid">

    <!-- Page Heading -->
    <h1 class="h3 mb-2 text-gray-800">Report Order</h1>

    <!-- DataTales Example -->
    <div class="card shadow mb-4">
      <div class="card-header py-3">
        <h6 class="m-0 font-weight-bold text-primary">Order Status : <?php echo $tx_status; ?> | user : <?php echo ucfirst($user_name); ?></h6>
      </div>
      <div class="card-body">

        <form class="form_filterReport" onsubmit="form_filterReport_old(event);" method="post" id="form_filterReport">
            <div class="row box_filterReport">
              <div class="col-md-2 col_filterReport">
                  <div class="label_filterReport">Dari Tanggal :</div>
                  <input type="text" name="from_date" class="txt_filterReport all_datepicker" value="<?php echo $from_date; ?>" readonly>
              </div>
              <div class="col-md-2 col_filterReport">
                  <div class="label_filterReport">Sampai Tanggal :</div>
                  <input type="text" name="to_date" class="txt_filterReport all_datepicker" value="<?php echo $to_date; ?>" readonly>
              </div>
              <div class="col-md-2 col_filterReport">
                  <input type="submit" class="sub_filterReport" value="Submit">
              </div>
            </div>
        </form>

        <div class="table-responsive">
          <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
            <thead>
              <tr>
                <th>Order ID</th>
                <th>Customer (user ID)</th>
                <th>Order Date</th>
                <th>Total</th>
                <th>Status</th>
                <th>Kurir</th>
              </tr>
            </thead>
            
            <tbody>

            <?php 
            

            $total_order = 0;
            $a = 0;
            $from_date = $from_date.' 00:00:00';
            $to_date = $to_date.' 23:59:59';
            $all_order_status = $order_statuses;
            $all_order_status = join("','",$order_statuses); 
            global $wpdb;
            $query = "SELECT *
                      FROM ldr_orders2
                      WHERE order_status IN('$all_order_status')
                      AND created_date BETWEEN '$from_date' AND '$to_date'
                      ORDER BY id DESC ";
            $res_query = $wpdb->get_results($query, OBJECT);
            $res_count = count($res_query);
            $total_count = count($res_query);

            if ($res_count > 0){
                foreach ($res_query as $key => $value) {
                    
                    $order_id = $value->order_id;
                    $a++;
                    $order = wc_get_order($order_id);
                    $user_id = $order->user_id;
                    $user_data = get_userdata( $user_id );
                    $user_name = $user_data->data->display_name;
                    $order_status  = $order->get_status();
                    $total_payment = $order->get_total();

                    $total_order = (int)$total_order+(int)$total_payment;

                    $get_Data = $order->get_data();  
                    $billing = $get_Data['billing'];
                    $first_name = $billing['first_name'];
                    $address_1 = $billing['address_1'];
                    $address_2 = $billing['address_2'];
                    $city = $billing['city'];
                    $state = $billing['state'];
                    $postcode = $billing['postcode'];
                    $country = $billing['country'];
                    $email = $billing['email'];
                    $phone = $billing['phone'];

                    $order_date = get_the_date('Y-m-d | h:i:s', $order_id);
                    $yearnow = date('Y',strtotime($order_date));
                    $monthnow = date('m',strtotime($order_date));
                    $daynow = date('d',strtotime($order_date));

                    $text_date = get_the_date('dmY', $order_id);
                    $inv_id = 'INV/'.$text_date.'/'.$order_id;

                    $courier_name = '';
                    $courier_resi = '';

                    $courier_name = $value->courier_name;
                    $courier_resi = $value->courier_resi;

            ?>
                        <tr>
                          <td><?php echo $inv_id; ?></td>
                          <td><?php echo $user_name.' ( '.$user_id.' )'; ?></td>
                          <td><?php echo number_format($total_payment); ?></td>
                          <?php if(strtolower($order_status) == 'completed'){ ?>
                            <td style="color: green;"><?php echo $order_status; ?></td>
                          <?php }else{ ?>
                            <td ><?php echo strtolower($order_status); ?></td>
                          <?php } ?>
                          <td><?php echo $order_date; ?></td>
                          <td>
                              <?php echo $courier_name; ?>
                          </td>
                        </tr>            
            <?php 
                }
            }
            ?>

            </tbody>

            <tfoot>
              <tr>
                <th>Order ID</th>
                <th colspan="2">Total Order: <?php echo $total_count; ?> nota</th>
                <th>Rp <?php echo number_format($total_order); ?></th>
                <th></th>
                <th></th>
                <th></th>
              </tr>
            </tfoot>

          </table>
        </div>

      </div>
    </div>

  </div>
  <!-- /.container-fluid -->

<?php }else{ ?>
    <script>
        // 'Getting' data-attributes using getAttribute
        var plant = document.getElementById('body');
        console.log(plant);
        var hurl = plant.getAttribute('data-hurl'); 
        location.replace(hurl+'/login/'); 
    </script>
<?php } ?>


<?php endwhile; ?>
<?php else : ?>
    <?php get_template_part( 'content', '404pages' ); ?>  
<?php endif; ?>
<?php get_footer(); ?>