<!doctype html>

<!--[if lt IE 7]><html <?php language_attributes(); ?> class="no-js lt-ie9 lt-ie8 lt-ie7"><![endif]-->
<!--[if (IE 7)&!(IEMobile)]><html <?php language_attributes(); ?> class="no-js lt-ie9 lt-ie8"><![endif]-->
<!--[if (IE 8)&!(IEMobile)]><html <?php language_attributes(); ?> class="no-js lt-ie9"><![endif]-->
<!--[if gt IE 8]><!--> <html <?php language_attributes(); ?> class="no-js"><!--<![endif]-->

<head>
	<meta charset="utf-8">

	<?php // Google Chrome Frame for IE ?>
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">

	<title><?php wp_title(''); ?></title>

	<?php // mobile meta (hooray!) ?>
	<meta name="HandheldFriendly" content="True">
	<meta name="MobileOptimized" content="320">
	<meta name="viewport" content="width=device-width, initial-scale=1.0"/>

	<?php // icons & favicons (for more: http://www.jonathantneal.com/blog/understand-the-favicon/) ?>
	<link rel="apple-touch-icon" href="<?php echo get_template_directory_uri(); ?>/library/images/apple-icon-touch.png">
	<link rel="icon" href="<?php echo get_template_directory_uri(); ?>/favicon.png">
	<!--[if IE]>
		<link rel="shortcut icon" href="<?php echo get_template_directory_uri(); ?>/favicon.ico">
	<![endif]-->
	<?php // or, set /favicon.ico for IE10 win ?>
	<meta name="msapplication-TileColor" content="#f01d4f">
	<meta name="msapplication-TileImage" content="<?php echo get_template_directory_uri(); ?>/library/images/win8-tile-icon.png">

	<link rel="pingback" href="<?php bloginfo('pingback_url'); ?>">
	
	<?php // Put you url link font here....  ?>
  <link href="https://fonts.googleapis.com/css?family=Montserrat:300,400,400i,500,600,700&display=swap" rel="stylesheet">
  <script src="https://kit.fontawesome.com/45b63cdda3.js" crossorigin="anonymous"></script>

	<link href="<?php echo get_template_directory_uri(); ?>/sbadmin2/vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
	<?php // Insert your css & jss only at function.php ?>

  	<link href="<?php echo get_template_directory_uri(); ?>/sbadmin2/css/sb-admin-2.min.css" rel="stylesheet">

  	<?php if ( is_page( array('report-order') ) ) { ?>
      <link href="<?php echo get_template_directory_uri(); ?>/library/css/jquery.datetimepicker.min.css" rel="stylesheet">
    <?php } ?>
  		<!-- Custom styles for this page -->
  		<link href="<?php echo get_template_directory_uri(); ?>/sbadmin2/vendor/datatables/dataTables.bootstrap4.min.css" rel="stylesheet">


	<?php // wordpress head functions ?>
	<?php wp_head(); ?>
	<?php // end of wordpress head ?>

	<?php // drop Google Analytics Here ?>


	<?php // end analytics ?>

</head>

<body id="page-top" <?php // body_class(); ?> itemscope itemtype="http://schema.org/WebPage" data-hurl="<?php echo home_url(); ?>" >
<div id="body" class="hide" data-hurl="<?php echo home_url(); ?>"></div>
<?php 
$current_user = wp_get_current_user();
$u_id = $current_user->ID;
$u_displayname = $current_user->display_name;
$mile_reward = 0;
$u_roles = $current_user->roles;
$admin = 0;

if(isset($u_roles) AND !empty($u_roles)){
	foreach ($u_roles as $key => $value) {
	    if(strtolower($value) == 'administrator'){
	        $admin = 1;
	    }
            if(strtolower($value) == 'shop_manager'){
          $admin = 1;
      }
	}
}

if($admin == 0){
?>
    <script>
        // 'Getting' data-attributes using getAttribute
        var plant = document.getElementById('body');
        console.log(plant);
        var hurl = plant.getAttribute('data-hurl'); 
        location.replace(hurl+'/login/'); 
    </script>
<?php
}
?>

	<div id="container">

  <!-- Page Wrapper -->
  <div id="wrapper">

    <!-- Sidebar -->
    <?php get_template_part( 'content', 'admin-sidebar' ); ?>
    <!-- End of Sidebar -->

    <!-- Content Wrapper -->
    <div id="content-wrapper" class="d-flex flex-column">

      <!-- Main Content -->
      <div id="content">

        <!-- Topbar -->
        <nav id="adm_topbar" class="navbar navbar-expand navbar-light bg-white topbar mb-4 static-top shadow">

          <!-- Sidebar Toggle (Topbar) -->
          <button id="sidebarToggleTop" class="btn btn-link d-md-none rounded-circle mr-3">
            <i class="fa fa-bars"></i>
          </button>

          <!-- Topbar Navbar -->
          <ul class="navbar-nav ml-auto">

            <!-- Nav Item - Search Dropdown (Visible Only XS) -->
            <li class="nav-item dropdown no-arrow d-sm-none">
              <a class="nav-link dropdown-toggle" href="#" id="searchDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                <i class="fas fa-search fa-fw"></i>
              </a>
              <!-- Dropdown - Messages -->
              <div class="dropdown-menu dropdown-menu-right p-3 shadow animated--grow-in" aria-labelledby="searchDropdown">
                <form class="form-inline mr-auto w-100 navbar-search">
                  <div class="input-group">
                    <input type="text" class="form-control bg-light border-0 small" placeholder="Search for..." aria-label="Search" aria-describedby="basic-addon2">
                    <div class="input-group-append">
                      <button class="btn btn-primary" type="button">
                        <i class="fas fa-search fa-sm"></i>
                      </button>
                    </div>
                  </div>
                </form>
              </div>
            </li>




            <div class="topbar-divider d-none d-sm-block"></div>

            <?php 
              $user_avatar = get_field('user_avatar', 'user_'.$u_id);
              if(isset($user_avatar) AND $user_avatar != ''){
                $linkpoto = wp_get_attachment_image_src($user_avatar,'thumbnail');
                $url_avatar = $linkpoto[0];
              }else{
                $url_avatar = get_template_directory_uri().'/library/images/icon_profile.png';
              }
            ?>

            <!-- Nav Item - User Information -->
            <li class="nav-item dropdown no-arrow">
              <a class="nav-link dropdown-toggle" href="#" id="userDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                <span class="mr-2 d-none d-lg-inline text-gray-600 small">Hi, <?php echo $u_displayname; ?></span>
                <img class="img-profile rounded-circle" src="<?php echo $url_avatar; ?>">
              </a>
              <!-- Dropdown - User Information -->
              <div class="dropdown-menu dropdown-menu-right shadow animated--grow-in" aria-labelledby="userDropdown">
                <a class="dropdown-item" href="<?php echo home_url(); ?>/logout/" data-toggle="modal" data-target="#logoutModal">
                  <i class="fas fa-sign-out-alt fa-sm fa-fw mr-2 text-gray-400"></i>
                  Logout
                </a>
              </div>
            </li>

          </ul>

        </nav>
        <!-- End of Topbar -->
	
<?php // next to body and footer.php ?>