<?php
/*
Template Name: Logout Page
*/

// logout the wordpress user
setcookie('tnahcremaradal', '', time() - 3600, "/", $_SERVER['SERVER_NAME']);
wp_logout();

// ============ sign out google account ===========
include('content-function-login.php'); 
//Reset OAuth access token
$google_client->revokeToken();
//Destroy entire session data.
session_destroy();
// ============ sign out google account ===========

// ============ sign out facebook account ===========
// $token = $facebook->getAccessToken();
// $url = 'https://www.facebook.com/logout.php?next=' . YOUR_SITE_URL .
//   '&access_token='.$token;
// session_destroy();
// header('Location: '.$url);
// ============ sign out facebook account ===========

//redirect page to homepage
$home_url = home_url();
setcookie('tnahcremaradal', '', time() - 3600, "/", $_SERVER['SERVER_NAME']);
echo '<META HTTP-EQUIV=REFRESH CONTENT="1; '.$home_url.'">';
exit;

?>