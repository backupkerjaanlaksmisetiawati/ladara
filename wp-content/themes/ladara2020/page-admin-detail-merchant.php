<?php
/*
Template Name: Detail Merchant Dashboard Page
*/

use Shipper\Location;

?>

<?php get_header('admin'); ?>

<?php if (have_posts()) :
    while (have_posts()) :
        the_post();
        date_default_timezone_set("Asia/Jakarta");
        $nowdate = date('Y-m-d H:i:s');

        $current_user = wp_get_current_user();
        $u_id = $current_user->ID;
        $u_roles = $current_user->roles;
        $admin = 0;
        $merchantId = $_GET['id'];

        if (isset($u_roles) and !empty($u_roles)) {
            foreach ($u_roles as $key => $value) {
                if (strtolower($value) == 'administrator') {
                    $admin = 1;
                }
                if (strtolower($value) == 'shop_manager') {
                    $admin = 1;
                }
            }
        }

        if ((isset($u_id) and $u_id != 0) and $admin == 1) {
            global $wpdb;

            $merchant = $wpdb->get_row("SELECT * FROM ldr_merchants LEFT JOIN ldr_users ON ldr_merchants.user_id = ldr_users.id WHERE ldr_merchants.id = $merchantId");
            $phone = get_field('telepon', 'user_' . $merchant->user_id);
?>

            <!-- Begin Page Content -->
            <div class="container-fluid">
                <!-- Page Heading -->
                <h1 class="h3 mb-2 text-gray-800">Data Toko</h1>
                <div class="alert alert-danger chk_err"></div>
                <!-- DataTales Example -->
                <div class="card shadow mb-4">
                    <div class="card-header py-3">
                        <h6 class="m-0 font-weight-bold text-primary ht_admdash">Detail Toko</h6>
                    </div>
                    <div class="card-body">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Nama Toko</label>
                                    <input id="name" type="text" class="form-control" value="<?php echo $merchant->name; ?>">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Provinsi</label>
                                    <select id="ch_province" name="provinsi" class="sel_aform ch_province" onchange="get_kotaSel2(event)" required="required">
                                        <?php
                                        $getProvinces = Location::getProvinces();

                                        foreach ($getProvinces->data->rows as $province) {
                                        ?>
                                            <option value="<?php echo $province->id; ?>" <?= ($merchant->province_id == $province->id ? 'selected' : '') ?>><?php echo $province->name; ?></option>
                                        <?php
                                        }
                                        ?>
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group merchant-register-form">
                                    <label>URL Toko</label>
                                    <input id="url" type="text" name="url" class="form-control" value="<?php echo $merchant->url; ?>">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Kota</label>
                                    <select id="ch_city" name="kota" class="sel_aform ch_regencies" onchange="get_kecamatanSel2(event)" required="required">
                                        <?php
                                        $getCities = Location::getCities($merchant->province_id);

                                        foreach ($getCities->data->rows as $city) {
                                        ?>
                                            <option value="<?php echo $city->id; ?>" <?= ($merchant->city_id == $city->id ? 'selected' : '') ?>><?php echo $city->name; ?></option>
                                        <?php
                                        }
                                        ?>
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Nama Penjual</label>
                                    <input type="text" class="form-control" value="<?php echo $merchant->display_name; ?>" readonly>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Kecamatan</label>
                                    <select id="ch_suburb" name="kecamatan" class="sel_aform ch_districts" onchange="get_kelurahanSel2(event)" required="required">
                                        <?php
                                        $getSuburbs = Location::getSuburbs($merchant->city_id);

                                        foreach ($getSuburbs->data->rows as $suburb) {
                                        ?>
                                            <option value="<?php echo $suburb->id; ?>" <?= ($merchant->suburb_id == $city->id ? 'selected' : '') ?>><?php echo $suburb->name; ?></option>
                                        <?php
                                        }
                                        ?>
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>No. Handphone Penjual</label>
                                    <input type="text" class="form-control" value="<?php echo $phone; ?>" readonly>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Kelurahan</label>
                                    <select id="ch_subdistrict" name="kelurahan" class="sel_aform ch_subdistrict" onchange="setKodePos(event)" required="required">
                                        <?php
                                        $getAreas = Location::getAreas($merchant->suburb_id);

                                        foreach ($getAreas->data->rows as $area) {
                                        ?>
                                            <option value="<?php echo $area->id; ?>" <?= ($merchant->area_id == $area->id ? 'selected' : '') ?> data-postcode="<?= $area->postcode ?>"><?php echo $area->name; ?></option>
                                        <?php
                                        }
                                        ?>
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Status Toko</label>
                                    <?php
                                    if ($merchant->status == '-1') {
                                    ?>
                                        <input type="text" class="form-control" value="SEDANG SUSPEND" readonly>
                                    <?php
                                    } else if ($merchant->status == '-2') {
                                    ?>
                                        <input type="text" class="form-control" value="SEDANG SUSPEND" readonly>
                                    <?php
                                    } else {
                                    ?>
                                        <select id="status" name="status" class="form-control" required="required">
                                            <option value="0" <?= ($merchant->status == '0' ? 'selected' : '') ?>>Tidak Aktif</option>
                                            <option value="1" <?= ($merchant->status == '1' ? 'selected' : '') ?>>Aktif</option>
                                        </select>
                                    <?php
                                    }
                                    ?>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Kode Pos</label>
                                    <input id="postcode" type="text" class="form-control" value="<?php echo $merchant->postal_code; ?>" readonly>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Persentase Komisi Ladara (%)</label>
                                    <input id="ladara_commission" type="number" class="form-control" value="<?php echo floatval($merchant->ladara_commission) * 100; ?>" min="0" max="100">
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label>Latitude</label>
                                    <input id="latitude" type="text" class="form-control" value="<?php echo $merchant->latitude; ?>">
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label>Longitude</label>
                                    <input id="longitude" type="text" class="form-control" value="<?php echo $merchant->longitude; ?>">
                                </div>
                            </div>
                        </div>
                        <div class="row" style="margin-top: 20px;">
                            <div class="col-md-6">
                                <a href="<?php echo home_url(); ?>/dashboard/daftar-merchant">
                                    <button class="btn btn-info">
                                        <b>Kembali</b>
                                    </button>
                                </a>
                            </div>
                            <div class="col-md-6" align="right">
                                <button class="btn btn-success btn_ca_active_merchant" title="Ubah Data Merchant Ini." data-id="<?php echo $merchant->id; ?>">
                                    <b>Submit</b>
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- /.container-fluid -->
        <?php
        } else { ?>
            <script>
                // 'Getting' data-attributes using getAttribute
                var plant = document.getElementById('body');
                console.log(plant);
                var hurl = plant.getAttribute('data-hurl');
                location.replace(hurl + '/login/');
            </script>
        <?php } ?>

    <?php endwhile; ?>
<?php else : ?>
    <?php get_template_part('content', '404pages'); ?>
<?php endif; ?>
<?php get_footer('admin'); ?>