<?php
/*
Template Name: Test profile migration
*/
?>

<?php
    
    $resp['updated'] = 0;
    $resp['inserted'] = 0;

    global $wpdb;
    $wpdb_query = "SELECT * FROM ldr_users";
    $res_query = $wpdb->get_results($wpdb_query, OBJECT);
    
    foreach($res_query as $profile){

        $user_id = $profile->ID;
        $insert['user_id'] = $user_id;
        $insert['email'] = $profile->user_email;
        $insert['created_date'] = $profile->user_registered;

        //name
        $name = get_field('nama_lengkap', 'user_'.$user_id);
        if(isset($name) && !empty($name))
            $insert['name'] = $name;
        else
            $insert['name'] = '';

        //phone
        $phone = get_field('telepon', 'user_'.$user_id);
        if(isset($phone) && !empty($phone))
            $insert['phone'] = $phone;
        else
            $insert['phone'] = '';

        //gender
        $gender = get_field('jenis_kelamin', 'user_'.$user_id);
        if(isset($gender) && !empty($gender))
            $insert['gender'] = $gender;
        else
            $insert['gender'] = '';

        //birthdate
        $birthdate = get_field('tanggal_lahir', 'user_'.$user_id);
        if(isset($birthdate) && !empty($birthdate))
            $insert['birthdate'] = $birthdate;
        else 
            $insert['birthdate'] = '';
            
        //check if data exists
        $check_db_query = "SELECT * FROM ldr_user_profile WHERE user_id = '$user_id' LIMIT 1";
        $check_db = $wpdb->get_results($check_db_query, OBJECT);
        $data_count = count($check_db);

        if($data_count > 0){
            //update if exists
            $resp['updated'] = $resp['updated'] + 1;
            $wpdb_insert = $wpdb->update('ldr_user_profile', $insert, array('user_id' => $user_id));
        } else {
            //insert if new
            $resp['inserted'] = $resp['inserted'] + 1;
            $wpdb_insert = $wpdb->insert('ldr_user_profile', $insert);
        }

    }
    // print_r($resp);
?>