<?php
/*
Template Name: Add Address
*/

use Shipper\Location;
?>

<?php get_header(); ?>
<?php if (have_posts()) :
    while (have_posts()) :
        the_post();

        global $wpdb;
        date_default_timezone_set("Asia/Jakarta");
        $current_user = wp_get_current_user();
        $userId = $current_user->ID;
        $user_login = $current_user->user_login;

        $addressId = 0;
        $addressName = '';
        $consigneeName = '';
        $province = '';
        $phone = '';
        $address = '';
        $provinceId = 0;
        $cityId = 0;
        $suburbId = 0;
        $areaId = 0;
        $postalCode = '';
        $longitude = 0;
        $latitude = 0;
        $mainAddress = 0;

        if (isset($_REQUEST['address']) && $_REQUEST['address'] != '') {
            $addressId = trim($_REQUEST['address']);
            $sql = $wpdb->prepare("SELECT * FROM ldr_user_addresses WHERE user_id = %d AND id = %d AND deleted_at IS NULL", [$userId, $addressId]);
            $query = $wpdb->get_row($sql);

            $addressId = $query->id;
            $addressName = $query->name;
            $consigneeName = $query->consignee;
            $phone = $query->phone;
            $address = $query->address;
            $provinceId = $query->province_id;
            $cityId = $query->city_id;
            $suburbId = $query->suburb_id;
            $areaId = $query->area_id;
            $postalCode = $query->postal_code;
            $longitude = $query->longitude;
            $latitude = $query->latitude;
            $mainAddress = $query->main_address;
        }

        if (isset($_REQUEST['rfr']) and $_REQUEST['rfr'] != '') {
            $referer = htmlspecialchars(base64_decode($_REQUEST['rfr']));
        } else {
            $referer = home_url('/profile/address/');
        }

        if (isset($userId) and $userId != 0) {
?>
            <style type="text/css">
                #wp-submit {
                    background: #0080FF;
                }
            </style>

            <div class="row"></div>

            <div id="profilpage" class="row row_profile">
                <div class="col-md-3 col_profile des_display">
                    <?php get_template_part('content', 'menu-profile'); ?>
                </div>
                <div id="profil_address" class="col-md-9 col_profile">
                    <div class="row row_cont_tab_profile">
                        <div class="alert alert-danger chk_err"></div>

                        <?php if (isset($_REQUEST['address']) && $_REQUEST['address'] != '') { ?>
                            <h1 class="ht_profile">Ubah Alamat</h1>
                        <?php } else { ?>
                            <h1 class="ht_profile">Tambah Alamat</h1>
                        <?php } ?>

                        <div class="col-md-12 col_daf_alamat">
                            <div class="bx_f_addAddress auto">
                                <form id="form_addAddress" class="form_addAddress" action="" onsubmit="submit_addAddress(event);" method="get">
                                    <input type="hidden" name="u_id" value="<?php echo $userId; ?>">
                                    <input type="hidden" name="ad_id" value="<?php echo $addressId; ?>">
                                    <input type="hidden" name="referer" value="<?php echo $referer; ?>">

                                    <div class="f_aform">
                                        <label>Nama Alamat <span>*</span></label>
                                        <input type="text" name="nama_alamat" class="txt_aform" required="required" value="<?php echo $addressName; ?>">
                                        <div id="p1" class="err_aform"></div>
                                    </div>

                                    <div class="f_aform">
                                        <label>Nama Penerima <span>*</span></label>
                                        <input type="text" name="nama_lengkap" class="txt_aform" required="required" value="<?php echo $consigneeName; ?>">
                                        <div id="p2" class="err_aform"></div>
                                    </div>

                                    <div class="f_aform">
                                        <label>Nomor Handphone <span>*</span></label>
                                        <input type="text" name="nomor_hp" class="txt_aform onlyphone" required="required" value="<?php echo $phone; ?>">
                                        <div id="p3" class="err_aform"></div>
                                    </div>

                                    <div class="f_aform">
                                        <label>Alamat Lengkap <span>*</span></label>
                                        <textarea class="area_aform" name="alamat" placeholder="" required="required"><?php echo $address; ?></textarea>
                                        <div id="" class="err_aform"></div>
                                    </div>

                                    <div class="f_aform">
                                        <label>Provinsi <span>*</span></label>
                                        <select id="ch_province" name="provinsi" class="sel_aform ch_province" onchange="get_kotaSel2(event)" required="required">
                                            <option value="">Pilih Provinsi</option>
                                            <?php
                                            $getProvinces = Location::getProvinces();

                                            foreach ($getProvinces->data->rows as $province) {
                                            ?>
                                                <option value="<?php echo $province->id; ?>" <?php echo ($provinceId == $province->id ? 'selected' : ''); ?>><?php echo $province->name; ?></option>
                                            <?php
                                            }
                                            ?>
                                        </select>
                                        <div id="" class="err_aform"></div>
                                    </div>

                                    <div class="f_aform">
                                        <label>Kota <span>*</span></label>
                                        <select id="ch_city" name="kota" class="sel_aform ch_regencies kota-profile" onchange="get_kecamatanSel2(event)" required="required">
                                            <option value="">Pilih Kota</option>
                                            <?php
                                            if ($addressId !== 0) {
                                                $getCities = Location::getCities($provinceId);

                                                foreach ($getCities->data->rows as $city) {
                                            ?>
                                                    <option value="<?php echo $city->id; ?>" <?php echo ($cityId == $city->id ? 'selected' : ''); ?>><?php echo $city->name; ?></option>
                                            <?php
                                                }
                                            }
                                            ?>
                                        </select>
                                        <div id="" class="err_aform"></div>
                                    </div>

                                    <div class="f_aform">
                                        <label>Kecamatan <span>*</span></label>
                                        <select id="ch_suburb" name="kecamatan" class="sel_aform ch_districts kecamatan-profile" onchange="get_kodeposSel2(event)" required="required">
                                            <option value="">Pilih Kecamatan</option>
                                            <?php
                                            if ($addressId !== 0) {
                                                $getSuburbs = Location::getSuburbs($cityId);

                                                foreach ($getSuburbs->data->rows as $suburb) {
                                            ?>
                                                    <option value="<?php echo $suburb->id; ?>" <?php echo ($suburbId == $suburb->id ? 'selected' : ''); ?>><?php echo $suburb->name; ?></option>
                                            <?php
                                                }
                                            }
                                            ?>
                                        </select>
                                        <div id="" class="err_aform"></div>
                                    </div>

                                    <div class="f_aform">
                                        <label>Kode Pos <span>*</span></label>
                                        <input type="hidden" name="kodepos" value="<?php echo $postalCode; ?>">
                                        <select id="ch_area" name="kelurahan" class="sel_aform ch_kodepos" onchange="setAreaId(event)" required="required">
                                            <option value="">Pilih Kode Pos</option>
                                            <?php
                                            if ($addressId !== 0) {
                                                $getAreas = Location::getAreas($suburbId);

                                                foreach ($getAreas->data->rows as $area) {
                                            ?>
                                                    <option value="<?php echo $area->id; ?>" data-id="<?php echo $area->postcode; ?>" <?php echo ($areaId == $area->id ? 'selected' : ''); ?>><?php echo $area->postcode; ?> (<?php echo $area->name; ?>)</option>
                                            <?php
                                                }
                                            }
                                            ?>
                                        </select>
                                        <div id="" class="err_aform"></div>
                                    </div>

                                    <div class="f_aform">
                                        <label>Pin Alamat</label>
                                        <input style="width: 50%;height: 40px;" type="text" name="address" id="pac-input" class="txt_aform" placeholder="cari alamat">
                                        <div id="map" style="width: 100%; height: 250px;"></div>
                                        <div id="" class="err_aform"></div>
                                    </div>

                                    <div class="f_aform">
                                        <label class="cont_check">Alamat Utama
                                            <?php if ($mainAddress == 1) { ?>
                                                <input type="checkbox" name="alamat_utama" value="1" checked="checked">
                                            <?php } else { ?>
                                                <input type="checkbox" name="alamat_utama" value="1">
                                            <?php } ?>
                                            <span class="checkmark"></span>
                                        </label>
                                    </div>

                                    <input type="hidden" name="longitude" value="<?php echo $longitude; ?>">
                                    <input type="hidden" name="latitude" value="<?php echo $latitude; ?>">

                                    <div class="label_success"></div>

                                    <div class="f_aform right">

                                        <?php if (isset($_REQUEST['address']) && $_REQUEST['address'] != '') { ?>
                                            <div class="err_info alert-success"><b>Sukses,</b> Alamat ini berhasil diubah.</div>
                                        <?php } else { ?>
                                            <div class="err_info alert-success"><b>Sukses,</b> Alamat ini telah di tambahkan.</div>
                                        <?php } ?>

                                        <a class="a_sub_aform dekstop">
                                            <input type="button" class="btn_aform btn_aform_back" value="Kembali">
                                        </a>
                                        <input type="submit" id="wp-submit" class="sub_aform" value="Simpan Profil">
                                        <a class="a_sub_aform mob_display" href="<?php echo home_url(); ?>/profile/address/">
                                            <input type="button" class="btn_aform" value="Kembali">
                                        </a>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAhLLkF-IXL7ZMmbgCWO0DAqD7thqz778w&callback=initMap&libraries=places&v=weekly" defer></script>
            <script>
                "use strict";

                let longitude = document.querySelector('[name="longitude"]')
                let latitude = document.querySelector('[name="latitude"]')

                let getLoc = function getLocation() {
                    if (longitude.value === '0' || latitude.value === '0') {
                        if (navigator.geolocation) {
                            navigator.geolocation.getCurrentPosition(function(position) {
                                longitude.value = position.coords.longitude
                                latitude.value = position.coords.latitude
                            })
                        }
                    }
                }()

                let markers = []
                let map = null

                function placeMarker(position, map) {
                    let marker = new google.maps.Marker({
                        position: position,
                        map: map
                    })

                    map.panTo(position)
                    markers.push(marker)

                    longitude.value = position.lng()
                    latitude.value = position.lat()
                }

                function initMap() {
                    map = new google.maps.Map(document.getElementById("map"), {
                        zoom: 18,
                        center: {
                            lng: 113.921327,
                            lat: -0.789275
                        },
                        mapTypeId: 'roadmap',
                        mapTypeControl: false,
                        streetViewControl: false
                    })

                    let marker = new google.maps.Marker({
                        position: {
                            lng: parseFloat(longitude.value),
                            lat: parseFloat(latitude.value)
                        },
                        map,
                        title: "Pin Lokasi"
                    })

                    markers.push(marker)
                    setSearchAlamat()

                    map.addListener('click', function(e) {
                        for (let marker of markers) {
                            marker.setMap(null)
                        }

                        placeMarker(e.latLng, map)
                    })
                }

                function setSearchAlamat() {
                    // Create the search box and link it to the UI element.
                    const input = document.getElementById("pac-input");
                    const searchBox = new google.maps.places.SearchBox(input);
                    map.controls[google.maps.ControlPosition.TOP_LEFT].push(input);
                    // Bias the SearchBox results towards current map's viewport.
                    map.addListener("bounds_changed", () => {
                        searchBox.setBounds(map.getBounds());
                    });
                    // Listen for the event fired when the user selects a prediction and retrieve
                    // more details for that place.
                    searchBox.addListener("places_changed", () => {
                        const places = searchBox.getPlaces();

                        if (places.length == 0) {
                            return;
                        }
                        // Clear out the old markers.
                        markers.forEach((marker) => {
                            marker.setMap(null);
                        });
                        markers = [];
                        // For each place, get the icon, name and location.
                        const bounds = new google.maps.LatLngBounds();
                        places.forEach((place) => {
                            if (!place.geometry) {
                                console.log("Returned place contains no geometry");
                                return;
                            }

                            // Create a marker for each place.
                            markers.push(
                                new google.maps.Marker({
                                    map,
                                    position: place.geometry.location,
                                })
                            );

                            if (place.geometry.viewport) {
                                // Only geocodes have viewport.
                                bounds.union(place.geometry.viewport);
                            } else {
                                bounds.extend(place.geometry.location);
                            }
                        });
                        map.fitBounds(bounds);
                    });
                }
            </script>
        <?php } else { ?>
            <script>
                // 'Getting' data-attributes using getAttribute
                var plant = document.getElementById('body');
                console.log(plant);
                var hurl = plant.getAttribute('data-hurl');
                location.replace(hurl + '/login/');
            </script>
        <?php } ?>
    <?php endwhile; ?>
<?php else : ?>
    <?php get_template_part('content', '404pages'); ?>
<?php endif; ?>
<?php get_footer(); ?>