<?php
/*
Template Name: My History Page
*/
?>

<?php get_header();
if (have_posts()) :
    while (have_posts()) :
        the_post();
        date_default_timezone_set("Asia/Jakarta");
        $nowdate = date('Y-m-d');

        global $wpdb;
        $currentUser = wp_get_current_user();
        $userId = $currentUser->ID;

        if (isset($userId) and $userId != 0) {
            if (isset($_GET['from_date'])) {
                $fromDate = $_GET['from_date'];
            } else {
                $fromDate = date("Y-m-d", strtotime($nowdate . "-30 days"));
            }

            if (isset($_GET['to_date'])) {
                $toDate = $_GET['to_date'];
            } else {
                $toDate = $nowdate;
            }

            $after_odate = $fromDate . ' 00:00:00';
            $before_odate = $toDate . ' 23:59:59';

            $data_status = array();
            $data_status['wc-pending'] = 'Menunggu Pembayaran';
            $data_status['wc-on-hold'] = 'Menunggu Konfirmasi';
            $data_status['wc-processing'] = 'Pembayaran diterima';
            $data_status['wc-preparing'] = 'Pesanan Diproses';
            $data_status['wc-ondelivery'] = 'Pesanan Dikirim';
            $data_status['wc-completed'] = 'Pesanan Selesai';
            $data_status['wc-cancelled'] = 'Pesanan Dibatalkan'; ?>

            <style type="text/css">
                #wp-submit {
                    background: #0080FF;
                }
            </style>

            <div class="row"></div>
            <div id="myhistorypage" class="row row_profile">
                <div class="col-md-3 col_profile des_display">
                    <?php get_template_part('content', 'menu-profile'); ?>
                </div>
                <div class="col-md-9 col_profile">
                    <div class="row row_cont_tab_profile">
                        <h1 class="ht_profile ht_myvoucher">Riwayat Pesanan</h1>
                        <div class="row row_myorder_tabs" style="margin-bottom: 0px;">
                            <div class="col-md-12 col_cont_myorder">
                                <form class="form_filterMyOrder" onsubmit="submit_filterMyOrder(event);" method="post" id="form_filterMyOrder">
                                    <div class="row row_filter_myorder">
                                        <div class="col-md-3 col_fl_myorder">
                                            <div class="f_search_myorder">
                                                <label>Filter dari tanggal</label>
                                                <input type="text" name="from_date" class="txt_date_myorder all_datepicker" value="<?php echo $fromDate; ?>" placeholder="<?php echo $nowdate; ?>">
                                                <span class="sp_datepick"><i class="far fa-calendar-alt"></i></span>
                                            </div>
                                        </div>
                                        <div class="col-md-3 col_fl_myorder">

                                            <div class="f_search_myorder">
                                                <label>sampai tanggal</label>
                                                <input type="text" name="to_date" class="txt_date_myorder all_datepicker" value="<?php echo $toDate; ?>" placeholder="<?php echo $nowdate; ?>">
                                                <span class="sp_datepick"><i class="far fa-calendar-alt"></i></span>
                                            </div>
                                        </div>
                                        <div class="col-md-3 col_fl_myorder">
                                            <input type="submit" id="wp-submit" class="sub_filter_myorder" value="cari">
                                        </div>
                                    </div>
                                </form>
                            </div>
                            <div class="col-md-12 col_cont_myorder">
                                <ul class="order_tabs">
                                    <li style="width: 25%;" class="act" data-id="tabs1"><a>Semua</a></li>
                                    <li style="width: 25%;" data-id="tabs2"><a>Pesanan Selesai</a></li>
                                    <li style="width: 25%;" data-id="tabs3"><a>Pesanan Batal</a></li>
                                    <li style="width: 25%;" data-id="tabs4"><a>Pesanan Ditolak</a></li>
                                </ul>
                            </div>
                        </div>

                        <?php
                        $queryOrderMerchants = $wpdb->get_results("SELECT * FROM ldr_orders o LEFT JOIN ldr_order_merchants om ON om.order_id = o.id WHERE om.deleted_at IS NULL AND om.status IN (-2, -1, 4) AND om.created_at BETWEEN '$after_odate' AND '$before_odate' AND o.user_id = $userId ORDER BY om.id DESC");
                        ?>

                        <div id="tabs1" class="row row_box_myorder animated fadeIn act">

                            <?php
                            $orderMerchantCompleted = [];
                            $orderMerchantCancelled = [];
                            $orderMerchantRejected = [];
                            $countOrderMerchants = []; // So I don't need to calculate again

                            if (count($queryOrderMerchants)) {
                                foreach ($queryOrderMerchants as $orderMerchant) {
                                    $ordersWithSameId = array_filter($queryOrderMerchants, function ($fn) use ($orderMerchant) {
                                        return $fn->order_id === $orderMerchant->order_id;
                                    });

                                    $countOrderMerchants[$orderMerchant->order_id] = count($ordersWithSameId);
                                    $orderDate = date("d M Y", strtotime($orderMerchant->created_at));
                                    $adminFee = $orderMerchant->xendit_fee / count($ordersWithSameId);
                                    $totalFee = $orderMerchant->shipping_price + $orderMerchant->insurance_price + $adminFee;
                                    $voucherName = '';
                                    $discountVoucher = 0;

                                    if ($orderMerchant->voucher_id !== '' && $orderMerchant->voucher_id !== null) {
                                        $voucherName = $wpdb->get_var("SELECT code FROM ldr_vouchers WHERE id = {$orderMerchant->voucher_id}");
                                        $discountVoucher = $orderMerchant->discount_voucher_price / count($ordersWithSameId);
                                    }

                                    $orderMerchant->voucherName = $voucherName;
                                    $orderMerchant->discountVoucher = $discountVoucher;

                                    $statusText = 'Pesanan Selesai';

                                    if ($orderMerchant->status === '-1') {
                                        $statusText = 'Pesanan Dibatalkan';
                                        $orderMerchantCancelled[] = $orderMerchant;
                                    } else if ($orderMerchant->status === '-2') {
                                        $statusText = 'Pesanan Ditolak';
                                        $orderMerchantRejected[] = $orderMerchant;
                                    } else if ($orderMerchant->status === '4') {
                                        $orderMerchantCompleted[] = $orderMerchant;
                                    }
                            ?>

                                    <div class="col-md-12 box_list_myorder">
                                        <div class="ht_cont_myorder"><?php echo $orderDate; ?> (<span><?php echo $orderMerchant->no_invoice; ?></span>)</div>
                                        <div class="row row_fill2_myorder">
                                            <div class="col-md-12 col_fill_myorder">

                                                <?php
                                                $queryOrderItems = $wpdb->get_results("SELECT * FROM ldr_order_items WHERE deleted_at IS NULL AND order_merchant_id = " . $orderMerchant->id);
                                                $totalPrice = 0;

                                                foreach ($queryOrderItems as $key => $orderItem) {
                                                    $totalPrice += $orderItem->qty * $orderItem->price;
                                                }

                                                foreach ($queryOrderItems as $key => $orderItem) {
                                                    $product = $wpdb->get_row("SELECT * FROM ldr_products WHERE id = " . $orderItem->product_id);
                                                    $productWeight = intval($product->weight ?? 0) * $orderItem->qty;
                                                    $mainImage = $wpdb->get_row("SELECT * FROM ldr_product_images WHERE name = 'imageUtama' AND deleted_at IS NULL AND products_id = " . $orderItem->product_id);

                                                    if ($mainImage !== null) {
                                                        $urlphoto = home_url('/') . $mainImage->path;
                                                    } else {
                                                        $urlphoto = get_template_directory_uri() . '/library/images/sorry.png';
                                                    }

                                                    $urlphoto = str_replace(home_url(), IMAGE_URL, $urlphoto);
                                                ?>
                                                    <div class="row row_orderlist_myorder">
                                                        <div class="col-md-6 col_in_myorder ">
                                                            <div class="box_mg_myorder">
                                                                <img src="<?php echo $urlphoto; ?>" alt="<?php echo $product->title ?? 'Deleted Product'; ?>">
                                                            </div>
                                                            <div class="box_cont_pro_myorder">
                                                                <a href="<?= ($product === null ? '#' : '/product/' . $product->permalink . '-' . $product->id) ?>" title="Lihat produk lengkap...">
                                                                    <div class="ht_pro_myorder">
                                                                        <?php echo $product->title ?? 'Deleted Product'; ?>
                                                                    </div>
                                                                </a>
                                                                <div class="sm_myorder">
                                                                    <span class="price">Rp <?php echo number_format($orderItem->price); ?></span>
                                                                    <br />
                                                                    <span class="qty"><?php echo $orderItem->qty; ?> barang (<?php echo $productWeight; ?> gram)</span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-3 col_in_myorder in_border">
                                                            <?php if ($key === 0) { ?>
                                                                <div class="ft_myorder order_space">
                                                                    <label>Status</label>
                                                                    <div class="tt_myorder"><b><?php echo $statusText; ?></b></div>
                                                                </div>
                                                                <div class="ft_myorder">
                                                                    <label>Total Harga Produk</label>
                                                                    <div class="tt_myorder"><b class="price">Rp <?php echo number_format($orderItem->qty * $orderItem->price); ?></b></div>
                                                                </div>
                                                            <?php } else { ?>
                                                                <div class="ft_myorder ano_product">
                                                                    <label>Total Harga Produk</label>
                                                                    <div class="tt_myorder"><b class="price">Rp <?php echo number_format($orderItem->qty * $orderItem->price); ?></b></div>
                                                                </div>
                                                            <?php } ?>
                                                        </div>

                                                        <?php if ($key === 0) { ?>
                                                            <div class="col-md-3 col_in_myorder in_last">
                                                                <div class="ft_myorder_total">
                                                                    <label>Total Belanja</label>
                                                                    <br />
                                                                    <label class="price">Rp <?php echo number_format($totalPrice + $totalFee - $orderMerchant->discountVoucher); ?></label>
                                                                </div>
                                                                <div class="ft_myorder">
                                                                    <label>Biaya Pengiriman & Admin</label>
                                                                    <div class="tt_myorder">
                                                                        <b>Rp <?php echo number_format($totalFee); ?></b>
                                                                    </div>
                                                                </div>
                                                                <?php
                                                                if ($orderMerchant->discountVoucher !== 0) :
                                                                ?>
                                                                    <div class="ft_myorder">
                                                                        <label>Potongan Voucher <?= $orderMerchant->voucherName ?></label>
                                                                        <div class="tt_myorder color_delivered">
                                                                            <b>Rp <?php echo number_format($orderMerchant->discountVoucher); ?></b>
                                                                        </div>
                                                                    </div>
                                                                <?php
                                                                endif;
                                                                ?>
                                                                <a href="/product/<?php echo $product->permalink . '-' . $product->id; ?>">
                                                                    <input type="button" class="btn_buyagain" value="Beli Lagi">
                                                                </a>
                                                            </div>
                                                        <?php } else { ?>
                                                            <div class="col-md-3 col_in_myorder in_last">
                                                                <a href="/product/<?php echo $product->permalink . '-' . $product->id; ?>">
                                                                    <input type="button" class="btn_buyagain add_mar" value="Beli Lagi">
                                                                </a>
                                                            </div>
                                                        <?php } ?>
                                                    </div>
                                                <?php
                                                } ?>
                                            </div>
                                        </div>
                                    </div>
                                <?php
                                }
                            } else {
                                ?>
                                <div class="row row_masterInformation">
                                    <div class="col-md-12 col_emptyInformation">
                                        <div class="mg_emptyInformation">
                                            <img src="<?php bloginfo('template_directory'); ?>/library/images/ico_noorder.jpg">
                                        </div>
                                        <h2 class="tx_emptyInformation">Tidak ada transaksi saat ini.</h2>
                                    </div>
                                </div>
                            <?php
                            }
                            ?>
                        </div>

                        <div id="tabs2" class="row row_box_myorder animated fadeIn">

                            <?php
                            if (count($orderMerchantCompleted)) {
                                foreach ($orderMerchantCompleted as $orderMerchant) {
                                    $orderDate = date("d M Y", strtotime($orderMerchant->created_at));
                                    $adminFee = $orderMerchant->xendit_fee / $countOrderMerchants[$orderMerchant->order_id];
                                    $totalFee = $orderMerchant->shipping_price + $orderMerchant->insurance_price + $adminFee;
                            ?>

                                    <div class="col-md-12 box_list_myorder">
                                        <div class="ht_cont_myorder"><?php echo $orderDate; ?> (<span><?php echo $orderMerchant->no_invoice; ?></span>)</div>
                                        <div class="row row_fill2_myorder">
                                            <div class="col-md-12 col_fill_myorder">

                                                <?php
                                                $queryOrderItems = $wpdb->get_results("SELECT * FROM ldr_order_items WHERE deleted_at IS NULL AND order_merchant_id = " . $orderMerchant->id);
                                                $totalPrice = 0;

                                                foreach ($queryOrderItems as $key => $orderItem) {
                                                    $totalPrice += $orderItem->qty * $orderItem->price;
                                                }

                                                foreach ($queryOrderItems as $key => $orderItem) {
                                                    $product = $wpdb->get_row("SELECT * FROM ldr_products WHERE id = " . $orderItem->product_id);
                                                    $productWeight = intval($product->weight ?? 0) * $orderItem->qty;
                                                    $mainImage = $wpdb->get_row("SELECT * FROM ldr_product_images WHERE name = 'imageUtama' AND deleted_at IS NULL AND products_id = " . $orderItem->product_id);

                                                    if ($mainImage !== null) {
                                                        $urlphoto = home_url('/') . $mainImage->path;
                                                    } else {
                                                        $urlphoto = get_template_directory_uri() . '/library/images/sorry.png';
                                                    }
                                                    $urlphoto = str_replace(home_url(), IMAGE_URL, $urlphoto);
                                                ?>
                                                    <div class="row row_orderlist_myorder">
                                                        <div class="col-md-6 col_in_myorder ">
                                                            <div class="box_mg_myorder">
                                                                <img src="<?php echo $urlphoto; ?>" alt="<?php echo $product->title ?? 'Deleted Product'; ?>">
                                                            </div>
                                                            <div class="box_cont_pro_myorder">
                                                                <a href="<?= ($product === null ? '#' : '/product/' . $product->permalink . '-' . $product->id) ?>" title="Lihat produk lengkap...">
                                                                    <div class="ht_pro_myorder">
                                                                        <?php echo $product->title ?? 'Deleted Product'; ?>
                                                                    </div>
                                                                </a>
                                                                <div class="sm_myorder">
                                                                    <span class="price">Rp <?php echo number_format($orderItem->price); ?></span>
                                                                    <br />
                                                                    <span class="qty"><?php echo $orderItem->qty; ?> barang (<?php echo $productWeight; ?> gram)</span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-3 col_in_myorder in_border">
                                                            <?php if ($key === 0) { ?>
                                                                <div class="ft_myorder order_space">
                                                                    <label>Status</label>
                                                                    <div class="tt_myorder">
                                                                        <b>Pesanan Selesai</b>
                                                                    </div>
                                                                </div>
                                                                <div class="ft_myorder">
                                                                    <label>Total Harga Produk</label>
                                                                    <div class="tt_myorder">
                                                                        <b class="price">Rp <?php echo number_format($orderItem->qty * $orderItem->price); ?></b>
                                                                    </div>
                                                                </div>
                                                            <?php } else { ?>
                                                                <div class="ft_myorder ano_product">
                                                                    <label>Total Harga Produk</label>
                                                                    <div class="tt_myorder">
                                                                        <b class="price">Rp <?php echo number_format($orderItem->qty * $orderItem->price); ?></b>
                                                                    </div>
                                                                </div>
                                                            <?php } ?>
                                                        </div>

                                                        <?php if ($key === 0) { ?>
                                                            <div class="col-md-3 col_in_myorder in_last">
                                                                <div class="ft_myorder_total">
                                                                    <label>Total Belanja</label>
                                                                    <br />
                                                                    <label class="price">Rp <?php echo number_format($totalPrice + $totalFee - $orderMerchant->discountVoucher); ?></label>
                                                                </div>
                                                                <div class="ft_myorder ">
                                                                    <label>Biaya Pengiriman & Admin</label>
                                                                    <div class="tt_myorder">
                                                                        <b>Rp <?php echo number_format($totalFee); ?></b>
                                                                    </div>
                                                                </div>
                                                                <?php
                                                                if ($orderMerchant->discountVoucher !== 0) :
                                                                ?>
                                                                    <div class="ft_myorder">
                                                                        <label>Potongan Voucher <?= $orderMerchant->voucherName ?></label>
                                                                        <div class="tt_myorder color_delivered">
                                                                            <b>Rp <?php echo number_format($orderMerchant->discountVoucher); ?></b>
                                                                        </div>
                                                                    </div>
                                                                <?php
                                                                endif;
                                                                ?>
                                                                <a href="/product/<?php echo $product->permalink . '-' . $product->id; ?>">
                                                                    <input type="button" class="btn_buyagain" value="Beli Lagi">
                                                                </a>
                                                            </div>
                                                        <?php } else { ?>
                                                            <div class="col-md-3 col_in_myorder in_last">
                                                                <a href="/product/<?php echo $product->permalink . '-' . $product->id; ?>">
                                                                    <input type="button" class="btn_buyagain add_mar" value="Beli Lagi">
                                                                </a>
                                                            </div>
                                                        <?php } ?>
                                                    </div>
                                                <?php
                                                } ?>
                                            </div>
                                        </div>
                                    </div>
                                <?php
                                }
                            } else {
                                ?>
                                <div class="row row_masterInformation">
                                    <div class="col-md-12 col_emptyInformation">
                                        <div class="mg_emptyInformation">
                                            <img src="<?php bloginfo('template_directory'); ?>/library/images/ico_noorder.jpg">
                                        </div>
                                        <h2 class="tx_emptyInformation">Tidak ada transaksi saat ini.</h2>
                                    </div>
                                </div>
                            <?php
                            }
                            ?>
                        </div>

                        <div id="tabs3" class="row row_box_myorder animated fadeIn">

                            <?php
                            if (count($orderMerchantCancelled)) {
                                foreach ($orderMerchantCancelled as $orderMerchant) {
                                    $orderDate = date("d M Y", strtotime($orderMerchant->created_at));
                                    $adminFee = $orderMerchant->xendit_fee / $countOrderMerchants[$orderMerchant->order_id];
                                    $totalFee = $orderMerchant->shipping_price + $orderMerchant->insurance_price + $adminFee;
                            ?>

                                    <div class="col-md-12 box_list_myorder">
                                        <div class="ht_cont_myorder"><?php echo $orderDate; ?> (<span><?php echo $orderMerchant->no_invoice; ?></span>)</div>
                                        <div class="row row_fill2_myorder">
                                            <div class="col-md-12 col_fill_myorder">

                                                <?php
                                                $queryOrderItems = $wpdb->get_results("SELECT * FROM ldr_order_items WHERE deleted_at IS NULL AND order_merchant_id = " . $orderMerchant->id);
                                                $totalPrice = 0;

                                                foreach ($queryOrderItems as $key => $orderItem) {
                                                    $totalPrice += $orderItem->qty * $orderItem->price;
                                                }

                                                foreach ($queryOrderItems as $key => $orderItem) {
                                                    $product = $wpdb->get_row("SELECT * FROM ldr_products WHERE id = " . $orderItem->product_id);
                                                    $productWeight = intval($product->weight ?? 0) * $orderItem->qty;
                                                    $mainImage = $wpdb->get_row("SELECT * FROM ldr_product_images WHERE name = 'imageUtama' AND deleted_at IS NULL AND products_id = " . $orderItem->product_id);

                                                    if ($mainImage !== null) {
                                                        $urlphoto = home_url('/') . $mainImage->path;
                                                    } else {
                                                        $urlphoto = get_template_directory_uri() . '/library/images/sorry.png';
                                                    }
                                                    $urlphoto = str_replace(home_url(), IMAGE_URL, $urlphoto);
                                                ?>
                                                    <div class="row row_orderlist_myorder">
                                                        <div class="col-md-6 col_in_myorder ">
                                                            <div class="box_mg_myorder">
                                                                <img src="<?php echo $urlphoto; ?>" alt="<?php echo $product->title ?? 'Deleted Product'; ?>">
                                                            </div>
                                                            <div class="box_cont_pro_myorder">
                                                                <a href="<?= ($product === null ? '#' : '/product/' . $product->permalink . '-' . $product->id) ?>" title="Lihat produk lengkap...">
                                                                    <div class="ht_pro_myorder">
                                                                        <?php echo $product->title ?? 'Deleted Product'; ?>
                                                                    </div>
                                                                </a>
                                                                <div class="sm_myorder">
                                                                    <span class="price">Rp <?php echo number_format($orderItem->price); ?></span>
                                                                    <br />
                                                                    <span class="qty"><?php echo $orderItem->qty; ?> barang (<?php echo $productWeight; ?> gram)</span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-3 col_in_myorder in_border">
                                                            <?php if ($key === 0) { ?>
                                                                <div class="ft_myorder order_space">
                                                                    <label>Status</label>
                                                                    <div class="tt_myorder">
                                                                        <b>Pesanan Dibatalkan</b>
                                                                    </div>
                                                                </div>
                                                                <div class="ft_myorder">
                                                                    <label>Total Harga Produk</label>
                                                                    <div class="tt_myorder">
                                                                        <b class="price">Rp <?php echo number_format($orderItem->qty * $orderItem->price); ?></b>
                                                                    </div>
                                                                </div>
                                                            <?php } else { ?>
                                                                <div class="ft_myorder ano_product">
                                                                    <label>Total Harga Produk</label>
                                                                    <div class="tt_myorder">
                                                                        <b class="price">Rp <?php echo number_format($orderItem->qty * $orderItem->price); ?></b>
                                                                    </div>
                                                                </div>
                                                            <?php } ?>
                                                        </div>

                                                        <?php if ($key === 0) { ?>
                                                            <div class="col-md-3 col_in_myorder in_last">
                                                                <div class="ft_myorder_total">
                                                                    <label>Total Belanja</label>
                                                                    <br />
                                                                    <label class="price">Rp <?php echo number_format($totalPrice + $totalFee - $orderMerchant->discountVoucher); ?></label>
                                                                </div>
                                                                <div class="ft_myorder ">
                                                                    <label>Biaya Pengiriman & Admin</label>
                                                                    <div class="tt_myorder">
                                                                        <b>Rp <?php echo number_format($totalFee); ?></b>
                                                                    </div>
                                                                </div>
                                                                <?php
                                                                if ($orderMerchant->discountVoucher !== 0) :
                                                                ?>
                                                                    <div class="ft_myorder">
                                                                        <label>Potongan Voucher <?= $orderMerchant->voucherName ?></label>
                                                                        <div class="tt_myorder color_delivered">
                                                                            <b>Rp <?php echo number_format($orderMerchant->discountVoucher); ?></b>
                                                                        </div>
                                                                    </div>
                                                                <?php
                                                                endif;
                                                                ?>
                                                                <a href="/product/<?php echo $product->permalink . '-' . $product->id; ?>">
                                                                    <input type="button" class="btn_buyagain" value="Beli Lagi">
                                                                </a>
                                                            </div>
                                                        <?php } else { ?>
                                                            <div class="col-md-3 col_in_myorder in_last">
                                                                <a href="/product/<?php echo $product->permalink . '-' . $product->id; ?>">
                                                                    <input type="button" class="btn_buyagain add_mar" value="Beli Lagi">
                                                                </a>
                                                            </div>
                                                        <?php } ?>
                                                    </div>
                                                <?php
                                                } ?>
                                            </div>
                                        </div>
                                    </div>
                                <?php
                                }
                            } else {
                                ?>
                                <div class="row row_masterInformation">
                                    <div class="col-md-12 col_emptyInformation">
                                        <div class="mg_emptyInformation">
                                            <img src="<?php bloginfo('template_directory'); ?>/library/images/ico_noorder.jpg">
                                        </div>
                                        <h2 class="tx_emptyInformation">Tidak ada transaksi saat ini.</h2>
                                    </div>
                                </div>
                            <?php
                            }
                            ?>
                        </div>

                        <div id="tabs4" class="row row_box_myorder animated fadeIn">

                            <?php
                            if (count($orderMerchantRejected)) {
                                foreach ($orderMerchantRejected as $orderMerchant) {
                                    $orderDate = date("d M Y", strtotime($orderMerchant->created_at));
                                    $adminFee = $orderMerchant->xendit_fee / $countOrderMerchants[$orderMerchant->order_id];
                                    $totalFee = $orderMerchant->shipping_price + $orderMerchant->insurance_price + $adminFee;
                            ?>

                                    <div class="col-md-12 box_list_myorder">
                                        <div class="ht_cont_myorder"><?php echo $orderDate; ?> (<span><?php echo $orderMerchant->no_invoice; ?></span>)</div>
                                        <div class="row row_fill2_myorder">
                                            <div class="col-md-12 col_fill_myorder">

                                                <?php
                                                $queryOrderItems = $wpdb->get_results("SELECT * FROM ldr_order_items WHERE deleted_at IS NULL AND order_merchant_id = " . $orderMerchant->id);
                                                $totalPrice = 0;

                                                foreach ($queryOrderItems as $key => $orderItem) {
                                                    $totalPrice += $orderItem->qty * $orderItem->price;
                                                }

                                                foreach ($queryOrderItems as $key => $orderItem) {
                                                    $product = $wpdb->get_row("SELECT * FROM ldr_products WHERE id = " . $orderItem->product_id);
                                                    $productWeight = intval($product->weight ?? 0) * $orderItem->qty;
                                                    $mainImage = $wpdb->get_row("SELECT * FROM ldr_product_images WHERE name = 'imageUtama' AND deleted_at IS NULL AND products_id = " . $orderItem->product_id);

                                                    if ($mainImage !== null) {
                                                        $urlphoto = home_url('/') . $mainImage->path;
                                                    } else {
                                                        $urlphoto = get_template_directory_uri() . '/library/images/sorry.png';
                                                    }
                                                    $urlphoto = str_replace(home_url(), IMAGE_URL, $urlphoto);
                                                ?>
                                                    <div class="row row_orderlist_myorder">
                                                        <div class="col-md-6 col_in_myorder ">
                                                            <div class="box_mg_myorder">
                                                                <img src="<?php echo $urlphoto; ?>" alt="<?php echo $product->title ?? 'Deleted Product'; ?>">
                                                            </div>
                                                            <div class="box_cont_pro_myorder">
                                                                <a href="<?= ($product === null ? '#' : '/product/' . $product->permalink . '-' . $product->id) ?>" title="Lihat produk lengkap...">
                                                                    <div class="ht_pro_myorder">
                                                                        <?php echo $product->title ?? 'Deleted Product'; ?>
                                                                    </div>
                                                                </a>
                                                                <div class="sm_myorder">
                                                                    <span class="price">Rp <?php echo number_format($orderItem->price); ?></span>
                                                                    <br />
                                                                    <span class="qty"><?php echo $orderItem->qty; ?> barang (<?php echo $productWeight; ?> gram)</span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-3 col_in_myorder in_border">
                                                            <?php if ($key === 0) { ?>
                                                                <div class="ft_myorder order_space">
                                                                    <label>Status</label>
                                                                    <div class="tt_myorder">
                                                                        <b>Pesanan Ditolak</b>
                                                                    </div>
                                                                </div>
                                                                <div class="ft_myorder">
                                                                    <label>Total Harga Produk</label>
                                                                    <div class="tt_myorder">
                                                                        <b class="price">Rp <?php echo number_format($orderItem->qty * $orderItem->price); ?></b>
                                                                    </div>
                                                                </div>
                                                            <?php } else { ?>
                                                                <div class="ft_myorder ano_product">
                                                                    <label>Total Harga Produk</label>
                                                                    <div class="tt_myorder">
                                                                        <b class="price">Rp <?php echo number_format($orderItem->qty * $orderItem->price); ?></b>
                                                                    </div>
                                                                </div>
                                                            <?php } ?>
                                                        </div>

                                                        <?php if ($key === 0) { ?>
                                                            <div class="col-md-3 col_in_myorder in_last">
                                                                <div class="ft_myorder_total">
                                                                    <label>Total Belanja</label>
                                                                    <br />
                                                                    <label class="price">Rp <?php echo number_format($totalPrice + $totalFee - $orderMerchant->discountVoucher); ?></label>
                                                                </div>
                                                                <div class="ft_myorder ">
                                                                    <label>Biaya Pengiriman & Admin</label>
                                                                    <div class="tt_myorder">
                                                                        <b>Rp <?php echo number_format($totalFee); ?></b>
                                                                    </div>
                                                                </div>
                                                                <?php
                                                                if ($orderMerchant->discountVoucher !== 0) :
                                                                ?>
                                                                    <div class="ft_myorder">
                                                                        <label>Potongan Voucher <?= $orderMerchant->voucherName ?></label>
                                                                        <div class="tt_myorder color_delivered">
                                                                            <b>Rp <?php echo number_format($orderMerchant->discountVoucher); ?></b>
                                                                        </div>
                                                                    </div>
                                                                <?php
                                                                endif;
                                                                ?>
                                                                <a href="/product/<?php echo $product->permalink . '-' . $product->id; ?>">
                                                                    <input type="button" class="btn_buyagain" value="Beli Lagi">
                                                                </a>
                                                            </div>
                                                        <?php } else { ?>
                                                            <div class="col-md-3 col_in_myorder in_last">
                                                                <a href="/product/<?php echo $product->permalink . '-' . $product->id; ?>">
                                                                    <input type="button" class="btn_buyagain add_mar" value="Beli Lagi">
                                                                </a>
                                                            </div>
                                                        <?php } ?>
                                                    </div>
                                                <?php
                                                } ?>
                                            </div>
                                        </div>
                                    </div>
                                <?php
                                }
                            } else {
                                ?>
                                <div class="row row_masterInformation">
                                    <div class="col-md-12 col_emptyInformation">
                                        <div class="mg_emptyInformation">
                                            <img src="<?php bloginfo('template_directory'); ?>/library/images/ico_noorder.jpg">
                                        </div>
                                        <h2 class="tx_emptyInformation">Tidak ada transaksi saat ini.</h2>
                                    </div>
                                </div>
                            <?php
                            }
                            ?>
                        </div>
                    </div>
                </div>
            </div>
        <?php
        } else { ?>
            <script>
                // 'Getting' data-attributes using getAttribute
                var plant = document.getElementById('body');
                console.log(plant);
                var hurl = plant.getAttribute('data-hurl');
                location.replace(hurl + '/login/');
            </script>
        <?php } ?>
    <?php endwhile; ?>
<?php else : ?>
    <?php get_template_part('content', '404pages'); ?>
<?php endif; ?>
<?php get_footer(); ?>