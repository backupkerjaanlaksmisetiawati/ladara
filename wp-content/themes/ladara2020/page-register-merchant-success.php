<?php
/*
Template Name: Register Merchant Success
*/
?>

<?php get_header(); ?>
<?php if (have_posts()) :
    while (have_posts()) :
        the_post();

        $current_user = wp_get_current_user();
        $u_id = $current_user->ID;
        $dataMerchant = getMerchant($u_id);

        if ($dataMerchant !== null) {
            $dbtimestamp = strtotime($dataMerchant->created_at);
            if (time() - $dbtimestamp > 10 * 60) {
                echo "<script>window.location.replace('".constant("MERCHANT_URL")."')</script>";
            }
        } else {
            echo "<script>window.location.replace('" . home_url() . "')</script>";
        }
?>
        <div class="row row_finishCheckout">
            <div class="col-md-12 col_finishCheckout">
                <div class="bx_finishCheckout">
                    <div class="row">
                        <div class="col-md-12">
                            <h4><b>Toko "<?php echo $dataMerchant->name; ?>" Berhasil Terdaftar</b> </h4>
                            <span>Mohon tunggu proses aktivasi selama 1x24 jam.</span>
                        </div>
                    </div>
                    <div class="mg_registerIcon" style="margin-top: 30px;">
                        <img style="width: 30%; height: auto;" src="<?php bloginfo('template_directory'); ?>/library/images/konfirmasi_toko.png">
                    </div>
<!--                    <div class="ht_sucs_register" style="font-weight: 600;">-->
<!--                        Mohon tunggu proses aktivasi selama 1x24 jam.-->
<!--                    </div>-->
                    <div class="row">
                        <div class="col-md-12" style="margin-top: 20px;">
                            <a href="<?php echo constant('MERCHANT_URL'); ?>">
                                <input type="button" class="sub_login" value="Ke Halaman Toko" style="margin-bottom: 15px; width: 30rem;" />
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <?php // ============= cannot back =============
        ?>
        <script type="text/javascript">
            (function(global) {

                if (typeof(global) === "undefined") {
                    throw new Error("window is undefined");
                }

                var _hash = "!";
                var noBackPlease = function() {
                    global.location.href += "#";

                    // making sure we have the fruit available for juice (^__^)
                    global.setTimeout(function() {
                        global.location.href += "!";
                    }, 50);
                };

                global.onhashchange = function() {
                    if (global.location.hash !== _hash) {
                        global.location.hash = _hash;
                    }
                };

                global.onload = function() {
                    noBackPlease();

                    // disables backspace on page except on input fields and textarea..
                    document.body.onkeydown = function(e) {
                        var elm = e.target.nodeName.toLowerCase();
                        if (e.which === 8 && (elm !== 'input' && elm !== 'textarea')) {
                            e.preventDefault();
                        }
                        // stopping event bubbling up the DOM tree..
                        e.stopPropagation();
                    };
                }

            })(window);
        </script>

    <?php endwhile; ?>
<?php else : ?>
    <?php get_template_part('content', '404pages'); ?>
<?php endif; ?>
<?php get_footer(); ?>