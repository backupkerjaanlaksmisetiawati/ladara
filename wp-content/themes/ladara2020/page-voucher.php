<?php
/*
Template Name: Voucher Page
*/
?>

<?php get_header(); ?>
<?php if (have_posts()) : while (have_posts()) : the_post(); ?>

<?php 
date_default_timezone_set("Asia/Jakarta");
$nowdate = date('Y-m-d');
$now_time = strtotime($nowdate);

$current_user = wp_get_current_user();
$u_id = $current_user->ID;


$all_catVoucher =  get_terms( 'custom_cat', array(
						'orderby' => 'name',
						'order' => 'ASC',
					    'hide_empty' => 0,
					) );

?>
<div class="row row_back_rewardpage"></div>

<div class="row row_reward_page">
	<div class="col-md-3 col_reward_page">
		
		<div class="box_side_voucher">
			<h2 class="ht_side_voucher">Cari voucher yang sesuai keinginanmu</h2>

			<a href="<?php echo home_url(); ?>/my-voucher/" title="Lihat semua voucher saya.">
				<div class="a_myvoucher">
					<img class="mg_myvoucher" src="<?php bloginfo('template_directory'); ?>/library/images/voucher.png"> Voucher Saya
				</div>
			</a>

			<form id="form_filterVoucher" class="form_filterVoucher" action="" method="get">
				<div class="f_side_voucher">
					<label class="hsub_voucher">Kategori Voucher</label>

					<?php foreach ($all_catVoucher as $key => $value) { ?>
						<div class="f_aform f_aform_voucher">
							<label class="cont_check"><?php echo $value->name; ?>
							<input type="checkbox" name="cat_voucher[]" class="check_catVoucher" value="<?php echo $value->term_id; ?>" checked="checked">
							<span class="checkmark"></span>
							</label>
						</div>		
					<?php } ?>
						
				</div>
			</form>

		</div>

	</div>
	<div class="col-md-9">
		
		<div class="row">
				<?php 
					$args = array(
					    'post_type' => 'couponlist',
					    'posts_per_page' => -1,
					    'post_status' => 'publish',
					    'orderby' => 'date',
					    'order' => 'DESC'
					);
					$the_query = new WP_Query($args);
					while ($the_query->have_posts()) : $the_query->the_post();
					$voucher_id = get_the_ID();
					$voucher_name = get_the_title($voucher_id);
					$short_name = get_the_title($voucher_id);
					if(strlen($short_name) > 45) $short_name = substr($short_name, 0, 45).'...';
					$voucher_link = get_the_permalink($product_id);
					$mile_coupon = get_field('mile_coupon',$voucher_id);
					$thumb = wp_get_attachment_image_src( get_post_thumbnail_id($voucher_id), 'medium' );
					if($thumb){
						$urlphoto = $thumb['0'];
					}else{
						$urlphoto = get_template_directory_uri().'/library/images/sorry.png';
					}
					$alt = get_post_meta(get_post_thumbnail_id($voucher_id), '_wp_attachment_image_alt', true);
					if(count($alt));

					$allcat = '';
					$category = get_the_terms( $id_coupon, 'custom_cat' );
					foreach ($category as $term => $value) {
						$voc_cat_id = $value->term_id;
						$allcat = $allcat.' '.$voc_cat_id;
					}

					$start_coupon = get_field('start_coupon',$voucher_id);
					$start_textdate = date("d-m-Y", strtotime($start_coupon));
					$str_startdate = strtotime( date("Y-m-d", strtotime($start_coupon)) );
					
					$end_coupon = get_field('end_coupon',$voucher_id);
					$str_enddate = strtotime( date("Y-m-d", strtotime($end_coupon)) );

					if($now_time <= $str_startdate){
					?>
						<div class="col-md-4 col_list_voc <?php echo $allcat; ?>">
							<a href="<?php echo $voucher_link; ?>" title="Ambil voucher ini.">
								<div class="box_list_voc">
									<div class="mg_list_voc">
										<img src="<?php echo $urlphoto; ?>" alt="<?php echo $alt; ?>">
									</div>
									<h4 class="ht_list_voc"><?php echo $voucher_name; ?></h4>
									<div class="mg_coinreward">
										<?php if(isset($mile_coupon) AND $mile_coupon == 0){ ?>
											<img src="<?php bloginfo('template_directory'); ?>/library/images/mile_reward.png"> <span><b>GRATIS</b></span>
										<?php }else{ ?>
											<img src="<?php bloginfo('template_directory'); ?>/library/images/mile_reward.png"> <span><?php echo $mile_coupon; ?> mile points</span>
										<?php } ?>
									</div>
									<span class="span_ambilvoucher">Segera : <?php echo $start_textdate; ?></span>
								</div>
							</a>
						</div>
					<?php
					}else if($now_time > $str_startdate AND $now_time < $str_enddate){
					?>
						<div class="col-md-4 col_list_voc <?php echo $allcat; ?>">
							<a href="<?php echo $voucher_link; ?>" title="Ambil voucher ini.">
								<div class="box_list_voc">
									<div class="mg_list_voc">
										<img src="<?php echo $urlphoto; ?>" alt="<?php echo $alt; ?>">
									</div>
									<h4 class="ht_list_voc"><?php echo $voucher_name; ?></h4>
									<div class="mg_coinreward">
										<?php if(isset($mile_coupon) AND $mile_coupon == 0){ ?>
											<img src="<?php bloginfo('template_directory'); ?>/library/images/mile_reward.png"> <span><b>GRATIS</b></span>
										<?php }else{ ?>
											<img src="<?php bloginfo('template_directory'); ?>/library/images/mile_reward.png"> <span><?php echo $mile_coupon; ?> mile points</span>
										<?php } ?>
									</div>
									<span class="span_ambilvoucher">Ambil</span>
								</div>
							</a>
						</div>
					<?php
					}

				?>

			<?php 
				endwhile;
				wp_reset_query();
			?>
		</div>

		<div class="no_voucher">
			Maaf, voucher tidak tersedia / tidak ditemukan.<br/>
			<span>Silahkan cari kategori voucher lainnya.</span>
		</div>


	</div>
</div>



<?php // ============= cannot back ============= ?>
<script type="text/javascript">
    (function (global) { 

        if(typeof (global) === "undefined") {
            throw new Error("window is undefined");
        }

        var _hash = "!";
        var noBackPlease = function () {
            global.location.href += "#";

            // making sure we have the fruit available for juice (^__^)
            global.setTimeout(function () {
                global.location.href += "!";
            }, 50);
        };

        global.onhashchange = function () {
            if (global.location.hash !== _hash) {
                global.location.hash = _hash;
            }
        };

        global.onload = function () {            
            noBackPlease();

            // disables backspace on page except on input fields and textarea..
            document.body.onkeydown = function (e) {
                var elm = e.target.nodeName.toLowerCase();
                if (e.which === 8 && (elm !== 'input' && elm  !== 'textarea')) {
                    e.preventDefault();
                }
                // stopping event bubbling up the DOM tree..
                e.stopPropagation();
            };          
        }

    })(window);
</script>

<?php endwhile; ?>
<?php else : ?>
		<?php get_template_part( 'content', '404pages' ); ?>	
<?php endif; ?>
<?php get_footer(); ?>