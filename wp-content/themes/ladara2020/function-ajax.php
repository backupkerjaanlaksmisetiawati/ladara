<?php

/**
 * Check voucher from popup form voucher
 *
 * @return  void
 * @author Amy <laksmise@gmail.com>
 */
function ajax_searchVoucher()
{
    $cartIds = $_SESSION['cartIds'];

    $carts = array_map(function ($fn) {
        return [
            'id' => intval($fn)
        ];
    }, explode(',', $cartIds));

    $voucherCode = $_POST["voucher_code"];

    $searchVoucher = sendRequest('POST', MERCHANT_URL . '/api/vouchers/search-by-code', [
        'voucher_code' => $voucherCode,
        'carts' => $carts
    ]);

    if (http_response_code() !== 200) {
        try {
            $message = $searchVoucher->message;
        } catch (Exception $e) {
            $message = 'Terjadi kesalahan sistem.';
        }

        echo json_encode([
            "code" => http_response_code(),
            "status" => "failed",
            "message" => $message
        ]);

        die();
    }

    echo json_encode($searchVoucher->data->id);
    die();
}

add_action("wp_ajax_ajax_searchVoucher", "ajax_searchVoucher");
add_action("wp_ajax_nopriv_ajax_searchVoucher", "ajax_searchVoucher");

/**
 * Check voucher from popup form voucher
 *
 * @return  void
 * @author Amy <laksmise@gmail.com>
 */
function ajax_checkVoucher()
{
    $identifier = $_SESSION['identifier'];
    $voucherId = $_POST["voucher_id"];
    $currentUser = wp_get_current_user();
    $userId = $currentUser->ID;
    $cartData = get_productCart($userId);
    $requestBodyCart = [];

    if ($cartData !== '' || $identifier == '') {
        $allDataCart = [];

        foreach ($cartData as $value) {
            $allDataCart[$value->seller_id][] = $value;
        }

        foreach ($allDataCart as $carts) {
            $tempCart = [];

            foreach ($carts as $cart) {
                $tempCart[] = [
                    'id' => intval($cart->id),
                    'qty' => intval($cart->qty)
                ];
            }

            $requestBodyCart[]['carts'] = $tempCart;
        }
    } else {
        echo json_encode(0);
        die();
    }

    $applyVoucher = sendRequest('POST', MERCHANT_URL . '/api/vouchers/apply-voucher', [
        'voucher_id' => intval($voucherId),
        'data' => $requestBodyCart
    ]);

    if (http_response_code() !== 200) {
        try {
            $message = $applyVoucher->errors->voucher_id[0];
        } catch (Exception $e) {
            $message = 'Terjadi kesalahan sistem.';
        }

        echo json_encode([
            "code" => http_response_code(),
            "status" => "failed",
            "message" => $message
        ]);

        die();
    }

    $_SESSION["voucherId"][$identifier] = $applyVoucher->data->id;

    echo json_encode([
        "code" => 200,
        "status" => "success",
        "message" => "Sukses.",
        "data" => [
            'code' => $applyVoucher->data->code,
            'type' => $applyVoucher->data->discount_type,
            'amount' => $applyVoucher->data->discount_amount,
            'max' => $applyVoucher->data->discount_max,
            'free_shipping' => $applyVoucher->data->free_shipping
        ]
    ]);

    die();
}

add_action("wp_ajax_ajax_checkVoucher", "ajax_checkVoucher");
add_action("wp_ajax_nopriv_ajax_checkVoucher", "ajax_checkVoucher");

/**
 * Check voucher from popup voucher detail
 *
 * @return  void
 * @author Amy <laksmise@gmail.com>
 */
function ajax_voucherDetail()
{
    global $wpdb;
    $table = $wpdb->prefix . "vouchers";

    $date = date("Y-m-d H:i:s");

    $current_user = wp_get_current_user();

    $voucher_code = $_POST["voucher_code"];

    $voucher = $wpdb->get_row("SELECT * FROM $table WHERE code='$voucher_code' AND deleted_at IS NULL");

    $html = "<div class=\"col-md-5\">";

    $html .= "<div class=\"closePopup\">";
    $html .= "<div>";
    $html .= "<i class=\"fas fa-arrow-left\" onclick=\"popupVoucherDetail('closepopup', '')\"></i>";
    $html .= "</div>";
    $html .= "<div>";
    if (!empty($voucher)) {
        $html .= "<div>";
        $html .= "<img src=\"" . home_url('/') . $voucher->banner . "\" />";
        $html .= "</div>";
        $html .= "<h4 class=\"voucher-detail-name\">";
        $html .= $voucher->name;
        $html .= "</h4>";
        $html .= "<div class=\"voucher-detail-date\">";
        $html .= "<div>Berlaku hingga</div>";
        $html .= "<div>";
        $html .= date("d", strtotime($voucher->period_start)) . " " . month_indonesia(date("n", strtotime($voucher->period_start))) . " " . date("Y", strtotime($voucher->period_start));
        $html .= "</div>";
        $html .= "</div>";
        $html .= "<div class=\"voucher-detail-date\">";
        if (empty($voucher->min_spend)) {
            $html .= "<div>Tanpa minimum transaksi</div>";
        } else {
            $html .= "<div>Minimum transaksi</div>";
            $html .= "<div>Rp " . number_format($voucher->min_spend) . "</div>";
        }
        $html .= "</div>";
    } else {
        $html .= "<div>";
        $html .= "</div>";
        $html .= "<h4 class=\"voucher-detail-name\">";
        $html .= "Voucher";
        $html .= "</h4>";
        $html .= "<div class=\"voucher-detail-date\">";
        $html .= "<div>Berlaku hingga</div>";
        $html .= "<div>";
        $html .= "01 Januari 1970";
        $html .= "</div>";
        $html .= "</div>";
        $html .= "<div class=\"voucher-detail-date\">";
        $html .= "<div>Tanpa minimum transaksi</div>";
        $html .= "</div>";
    }
    $html .= "<div class=\"voucher-detail-date\" style=\"display:block;padding:13px 20px 0px 20px;\">";
    $html .= "<hr />";
    $html .= "</div>";
    $html .= "</div>";
    $html .= "</div>";

    $html .= "<div class=\"box-content\">";
    $html .= "<h4>";
    $html .= "<strong>Syarat &amp; Ketentuan</strong>";
    $html .= "</h4>";
    $html .= "<div>";
    if (!empty($voucher)) {
        $html .= nl2br($voucher->description);
    }
    $html .= "</div>";
    $html .= "</div>";

    $html .= "</div>";

    $return = array(
        "code" => 200,
        "html" => $html
    );

    echo json_encode($return);
    die();
}
add_action("wp_ajax_ajax_voucherDetail", "ajax_voucherDetail");
add_action("wp_ajax_nopriv_ajax_voucherDetail", "ajax_voucherDetail");

/**
 * Undocumented function
 *
 * @param [type] $post
 * @return void
 */
function check_regex($post) {
	if( !preg_match('/^[a-z0-9 .\-]+$/i', $post) ) {
		return false;
	} else {
		return true;
	}
}
add_action( 'check_regex', 'check_regex', 0 );

/**
 * Undocumented function
 *
 * @param [type] $post
 * @return void
 */
function check_slug_regex($post) {
	if( !preg_match('/^[a-z0-9-]+$/i', $post) ) {
		return false;
	} else {
		return true;
	}
}
add_action( 'check_slug_regex', 'check_slug_regex', 0 );

function ajax_add_kategori_topup() {
    global $wpdb;

    $user_id = get_current_user_id();

    require_once( ABSPATH . 'wp-admin/includes/image.php' );
    require_once( ABSPATH . 'wp-admin/includes/file.php' );
    require_once( ABSPATH . 'wp-admin/includes/media.php' );

    /* validate input */
        $error = array();

        if(empty($_POST['name'])) {
            $error['name'] = 'Nama kategori wajib diisi.';
        }
        if( !empty($_POST['name']) && check_regex($_POST['name']) === false ) {
            $error['name'] = 'Nama kategori hanya boleh diisi dengan alphanumeric.';
        }

        if(empty($_POST['slug'])) {
            $error['slug'] = 'Slug kategori wajib diisi.';
        }
        if( !empty($_POST['slug']) && check_slug_regex($_POST['slug']) === false ) {
            $error['slug'] = 'Slug kategori hanya boleh diisi dengan alphanumeric dan dash (-).';
        }

        $maxsize = 100000;

        $filename_foto = $_FILES['image']['name'];
        if($filename_foto == null) {
            $error['image'] = 'Thumbnail wajib diisi.';
        }
        if($filename_foto != null) {
            $ext_foto = pathinfo($filename_foto, PATHINFO_EXTENSION);
            if (!in_array($ext_foto, array('jpg', 'png'))) {
                $error['image'] = 'Format file hanya boleh PNG.';
            }

            if(($_FILES['image']['size'] >= $maxsize) || ($_FILES["image"]["size"] == 0)) {
                $error['image'] = 'Maksimal size file adalah 100kB.';
            }
        }

        if( !empty($error) ) {
            $return = array(
                'status'  => 400,
                'data'    => array(
                    'message' => $error
                )
            );
            echo json_encode($return);
            die();
        }
    /* validate input */

    $icon_attachment_id = media_handle_upload('image', $user_id);
    $icon_url = wp_get_attachment_url($icon_attachment_id);

    $name = $_POST["name"];
    $slug = $_POST["slug"];
    $featured = $_POST["featured"];
    $status = $_POST["status"];
    
    $check_slug = $wpdb->get_results("SELECT slug FROM ldr_topup_categories WHERE slug='".$slug."'", OBJECT);
    if(!empty($check_slug)) {
        $count = count($check_slug);
        $count = $count + 1;
        $slug = $slug."-".$count;
    }

    $data = array(
        'name' => $name,
        'slug' => $slug,
        'featured' => $featured,
        'status' => $status,
        'icon' => $icon_url,
        'created_by' => $user_id,
        'updated_by' => $user_id,
        'deleted_at' => '0000-00-00'

    );
    $wpdb->insert( 'ldr_topup_categories', $data );

    $return = array(
        'status'  => 200,
        'data'    => array(
            'message_type' => "success",
            'message'   => "Sukses.",
        )
    );

    echo json_encode($return);
    die();
}
add_action("wp_ajax_ajax_add_kategori_topup","ajax_add_kategori_topup");
add_action("wp_ajax_nopriv_ajax_add_kategori_topup", "ajax_add_kategori_topup");

function ajax_edit_kategori_topup() {
    global $wpdb;

    $user_id = get_current_user_id();
    $id = $_POST["id"];    

    $data = $wpdb->get_row(
        "SELECT * FROM ldr_topup_categories WHERE id=$id",
        OBJECT
    );

    require_once( ABSPATH . 'wp-admin/includes/image.php' );
    require_once( ABSPATH . 'wp-admin/includes/file.php' );
    require_once( ABSPATH . 'wp-admin/includes/media.php' );

    /* validate input */
        $error = array();

        if(empty($_POST['name'])) {
            $error['name'] = 'Nama kategori wajib diisi.';
        }
        if( !empty($_POST['name']) && check_regex($_POST['name']) === false ) {
            $error['name'] = 'Nama kategori hanya boleh diisi dengan alphanumeric.';
        }

        if(empty($_POST['slug'])) {
            $error['slug'] = 'Slug kategori wajib diisi.';
        }
        if( !empty($_POST['slug']) && check_slug_regex($_POST['slug']) === false ) {
            $error['slug'] = 'Slug kategori hanya boleh diisi dengan alphanumeric dan dash (-).';
        }

        $maxsize = 100000;

        if(empty($data->icon)) {
            $filename_foto = $_FILES['image']['name'];
            if($filename_foto == null) {
                $error['image'] = 'Thumbnail wajib diisi.';
            }
            if($filename_foto != null) {
                $ext_foto = pathinfo($filename_foto, PATHINFO_EXTENSION);
                if (!in_array($ext_foto, array('png'))) {
                    $error['image'] = 'Format file hanya boleh PNG.';
                }

                if(($_FILES['image']['size'] >= $maxsize) || ($_FILES["image"]["size"] == 0)) {
                    $error['image'] = 'Maksimal size file adalah 100kB.';
                }
            }
        }

        if( !empty($error) ) {
            $return = array(
                'status'  => 400,
                'data'    => array(
                    'message' => $error
                )
            );
            echo json_encode($return);
            die();
        }
    /* validate input */

    $icon_attachment_id = media_handle_upload('image', $user_id);
    $icon_url = wp_get_attachment_url($icon_attachment_id);

    $name = $_POST["name"];
    // $slug = $_POST["slug"];
    $featured = $_POST["featured"];
    $status = $_POST["status"];
    
    // $check_slug = $wpdb->get_results("SELECT slug FROM ldr_topup_categories WHERE slug='".$slug."' AND id !=".$id."", OBJECT);
    // if(!empty($check_slug)) {
    //     $count = count($check_slug);
    //     $count = $count + 1;
    //     $slug = $slug."-".$count;
    // }

    $wpdb->query("UPDATE ldr_topup_categories SET 
            name = '$name',
            featured = '$featured',
            status = '$status',
            icon = '$icon_url',
            updated_by = $user_id
        WHERE id = ".$id);

    $return = array(
        'status'  => 200,
        'data'    => array(
            'message_type' => "success",
            'message'   => "Sukses.",
        )
    );

    echo json_encode($return);
    die();
}
add_action("wp_ajax_ajax_edit_kategori_topup","ajax_edit_kategori_topup");
add_action("wp_ajax_nopriv_ajax_edit_kategori_topup", "ajax_edit_kategori_topup");

function ajax_delete_kategori_topup() {
    global $wpdb;

    $user_id = get_current_user_id();
    $id = $_POST["id"];
    $date = date("Y-m-d H:i:s");

    $wpdb->query("UPDATE ldr_topup_categories SET 
            status = 2,
            updated_by = $user_id,
            deleted_at = '".$date."'
        WHERE id = ".$id);

    $return = array(
        'status'  => 200,
        'data'    => array(
            'message_type' => "success",
            'message'   => "Sukses.",
        )
    );

    echo json_encode($return);
    die();
}
add_action("wp_ajax_ajax_delete_kategori_topup","ajax_delete_kategori_topup");
add_action("wp_ajax_nopriv_ajax_delete_kategori_topup", "ajax_delete_kategori_topup");
