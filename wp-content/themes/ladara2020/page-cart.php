<?php
/*
Template Name: Cart Page
*/
?>

<?php get_header(); ?>

<?php
if (have_posts()) :
    while (have_posts()) :
        the_post();
?>
        <style type="text/css">
            .col_menuFooter,
            .col_contFooter {
                display: none !important;
            }

            @media only screen and (max-width:769px) {

                .row_fix_mobmenu,
                #inner-footer {
                    display: none !important;
                }
            }
        </style>

        <?php
        global $wpdb;
        $currentUser = wp_get_current_user();
        $userId = $currentUser->ID;
        $totalAllItem = 0;
        $totalQty = 0;
        $countCart = 0;
        $soldout = false;

        if ($userId !== 0) {
            $queryCart = $wpdb->get_results("SELECT * FROM ldr_cart WHERE user_id = $userId");
            $countCart = count($queryCart);
        } else {
            $cartData = $_COOKIE['user_cart'];

            if ($cartData !== null) {
                $queryCart = json_decode(stripslashes(base64_decode($cartData)));
                $countCart = count($queryCart);
            }
        }

        if ($countCart === 0) {
        ?>
            <div class="row row_finishCheckout">
                <div class="col-md-12 col_finishCheckout">
                    <a href="<?php echo home_url(); ?>">
                        <div class="bx_backShop">
                            <span class="glyphicon glyphicon-menu-left"></span> Kembali ke Home
                        </div>
                    </a>
                    <div class="bx_finishCheckout">
                        <div class="mg_registerIcon">
                            <img class="lazy" data-src="<?php bloginfo('template_directory'); ?>/library/images/ico_cartempty.svg">
                        </div>
                        <div class="ht_register">Kosong?</div>

                        <div class="ht_sucs_register">
                            Yuk segera isi keranjangnya dengan barang-barang impian kamu!
                        </div>
                        <div class="bx_def_checkout">
                            <a href="<?php echo home_url(); ?>/shop/">
                                <button class="btn_def_checkout">Belanja Sekarang</button>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        <?php
        } else {
            $allCarts = array();
            $cartIds = []; // Cart to submitted
            $merchants = [];
            $checkAll = true;
            $cartIsValid = false;

            foreach ($queryCart as $key => $value) {
                $allCarts[$value->seller_id][] = array(
                    'id' => $value->id,
                    'product_id' => $value->product_id,
                    'stock_id' => $value->stock_id,
                    'qty' => $value->qty,
                    'seller_id' => $value->seller_id
                );

                $cartIds[] = $value->id;
                $merchants[$value->seller_id] = $wpdb->get_row("SELECT * FROM ldr_merchants WHERE id = " . $value->seller_id);

                if ($merchants[$value->seller_id]->status !== '1') {
                    $checkAll = false;
                } else {
                    $cartIsValid = true;
                }
            }

            ksort($allCarts);
        ?>

            <form action="<?php echo home_url(); ?>/checkout/" name="form_updateCart" class="form_updateCart" id="form_updateCart" method="post" style="width: 100%;">
                <input type="hidden" name="user_id" value="<?php echo $userId; ?>">
                <?php
                if ($userId !== 0) {
                ?>
                    <input type="hidden" name="cartIds" id="cartIds" value='<?php echo json_encode($cartIds); ?>'>
                <?php
                }
                ?>

                <div class="row row_masterCart">
                    <div class="col-md-12 col_masterCart">
                        <a href="<?php echo home_url(); ?>/shop/">
                            <div class="bx_backShop">
                                <span class="glyphicon glyphicon-menu-left"></span> Lanjutkan Belanja
                            </div>
                        </a>
                        <div class="bx_cart_page">
                            <h1 class="ht_masterCart">Keranjang Belanja</h1>
                            <div class="row box_cartItem_head">
                                <div class="col-md-1 col_top_cartItem">
                                    <input type="checkbox" class="check_cart" data-check="all" onclick="setCheckCart(this)" <?= ($checkAll ? 'checked' : '') ?>>
                                </div>
                                <div class="col-md-4 col_top_cartItem" style="padding-left: 5px;">
                                    Pilih Semua
                                </div>
                                <div class="col-md-2 col_top_cartItem" style="padding-left: 10px;">
                                    Harga Unit
                                </div>
                                <div class="col-md-2 col_top_cartItem" style="padding-left: 10px;">
                                    Jumlah
                                </div>
                                <div class="col-md-3 col_top_cartItem" style="padding-left: 10px;">
                                    Subtotal
                                </div>
                            </div>
                            <?php
                            foreach ($allCarts as $index => $cart) {
                                $merchantId = $index;
                                $queryMerchant = $merchants[$merchantId];
                            ?>
                                <div class="box_row_cartItem">
                                    <div class="row">
                                        <div class="col-md-1">
                                            <?php
                                            if ($queryMerchant->status === '1') :
                                            ?>
                                                <input type="checkbox" class="check_cart" data-check="seller" data-seller-id="<?php echo $merchantId; ?>" onclick="setCheckCart(this)" checked>
                                            <?php
                                            endif;
                                            ?>
                                        </div>
                                        <div class="col-md-8">
                                            <span class="ht_cartSeller">
                                                <a href="<?= $queryMerchant->url ?>" title="<?= $queryMerchant->name ?>" style="text-decoration: none;">
                                                    <b><?php echo $queryMerchant->name ?? ''; ?></b>
                                                </a>
                                            </span>
                                            <?php
                                            if ($queryMerchant->status !== '1') :
                                            ?>
                                                <span style="font-weight: bold; color: red;">&emsp;(TOKO SEDANG TIDAK AKTIF)</span>
                                            <?php
                                            endif;
                                            ?>
                                        </div>
                                        <div class="col-md-3">
                                            <a class="a_remove_cartseller btn_remove_cartseller" data-sel="<?= $queryMerchant->id ?>" title="Hapus semua produk di toko ini.">Hapus Toko</a>
                                        </div>
                                    </div>
                                    <?php
                                    $currentDate = date('Y-m-d H:i:s');
                                    $getCurrentDiscount = $wpdb->get_col("SELECT id FROM ldr_discount WHERE deleted_at IS NULL AND start_date <= '$currentDate' AND end_date >= '$currentDate'");

                                    foreach ($cart as $key => $value) {
                                        $keyId = $value['id'];
                                        $productId = $value['product_id'];
                                        $product = $wpdb->get_row("SELECT * FROM ldr_products pr LEFT JOIN ldr_posts po ON po.ID = pr.brand_id WHERE pr.id = $productId");

                                        if ($product !== null) {
                                            $cartQty = intval($value['qty']);
                                            $priceTotal = intval($product->price) * $cartQty;
                                            $salePrice = 0;
                                            $salePriceTotal = 0;
                                            $discountPercentage = 0;

                                            if (count($getCurrentDiscount)) {
                                                $productDiscount = $wpdb->get_row("SELECT * FROM ldr_discount_product WHERE deleted_at IS NULL AND product_id = $productId AND discount_id IN (" . implode(',', $getCurrentDiscount) . ")");
                                                $salePrice = $productDiscount->discount_price ?? 0;
                                                $discountPercentage = round(($product->price - $salePrice) / $product->price * 100);

                                                if (intval($salePrice) !== 0) {
                                                    $salePriceTotal = intval($salePrice) * $cartQty;
                                                }
                                            }

                                            if ($queryMerchant->status === '1') { // If merchant is active
                                                $totalQty += $cartQty;

                                                if ($salePriceTotal !== 0) {
                                                    $totalAllItem += $salePriceTotal;
                                                } else {
                                                    $totalAllItem += $priceTotal;
                                                }
                                            }

                                            $productBrand = $product->post_title; // To be continue

                                            $stockId = $value['stock_id'];
                                            $stock = $wpdb->get_row("SELECT * FROM ldr_stock WHERE id = $stockId");

                                            $ukuran_value = $stock->ukuran_value;
                                            $nomor_value = $stock->nomor_value;
                                            $warna_value = $stock->warna_value;
                                            $productQuantity = $stock->product_quantity;

                                            if ($productQuantity < 1) {
                                                $cartQty = 0;
                                                $soldout = true;
                                            }

                                            $image = $wpdb->get_row("SELECT * FROM ldr_product_images WHERE products_id = $productId AND name = 'imageUtama' AND deleted_at IS NULL");
                                            $urlphoto = IMAGE_URL . '/'.$image->path ?? get_template_directory_uri() . '/library/images/sorry.png';
                                    ?>
                                            <div class="row box_cartItem pro_<?php echo $product->id; ?> <?= $queryMerchant->status !== '1' ? 'disabled' : '' ?>">
                                                <div class="col-md-1">
                                                    <?php
                                                    if ($queryMerchant->status === '1') :
                                                    ?>
                                                        <input type="checkbox" class="check_cart" data-check="product" data-seller-id="<?php echo $merchantId; ?>" data-id="<?php echo $keyId; ?>" data-price="<?php echo $product->price; ?>" onclick="setCheckCart(this)" checked>
                                                    <?php
                                                    endif;
                                                    ?>
                                                </div>
                                                <div class="col-md-4 col_top_cartItem">
                                                    <div class="row">
                                                        <div class="col-md-4">
                                                            <div class="mg_cartItem">
                                                                <img src="<?php echo $urlphoto; ?>" alt="<?php echo $product->title; ?>">
                                                            </div>
                                                        </div>
                                                        <div class="col-md-8 col_mid_cartItem">
                                                            <a href="/product/<?php echo $product->permalink . '-' . $product->id; ?>" title="Lihat <?php echo $product->title; ?>">
                                                                <h2 class="ht_cartItem"><?php echo $product->title; ?></h2>
                                                            </a>
                                                            <div class="brand_cartItem"><?php echo $productBrand; ?></div>
                                                            <div class="opsi_cartItem">
                                                                <?php
                                                                if (isset($warna_value) and $warna_value != '') {
                                                                    echo $warna_value;
                                                                }

                                                                if (isset($ukuran_value) and $ukuran_value != '') {
                                                                    echo " | " . $ukuran_value;
                                                                }

                                                                if (isset($nomor_value) and $nomor_value != '') {
                                                                    echo " | " . $nomor_value;
                                                                }
                                                                ?>
                                                            </div>
                                                            <div class="sku_cartItem">SKU : <?php echo $product->sku; ?></div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-2 hidden-xs">
                                                    <div class="ht_price_totalpcs">
                                                        <?php if (intval($salePrice) !== 0) { ?>
                                                            <div class="tx_price">Rp <span class="price_per_unit" data-id="<?= $key ?>"><?php echo number_format($salePrice); ?></span></div>
                                                            <?php if (intval($salePrice) !== intval($product->price)) : ?>
                                                                <div class="tx_price_sale" style="text-decoration: none; margin-top: -15px;">
                                                                    <span style="text-decoration: line-through;">Rp <span class="price_per_unit_old" data-id="<?= $key ?>"><?php echo number_format($product->price); ?></span></span>
                                                                    <span class="discount"><?php echo $discountPercentage; ?> %</span>
                                                                </div>
                                                            <?php endif; ?>
                                                        <?php } else { ?>
                                                            Rp <span class="price_per_unit" data-id="<?= $key ?>"><?php echo number_format($product->price); ?></span>
                                                        <?php } ?>
                                                    </div>
                                                </div>
                                                <div class="col-md-2 col_mid_cartItem mob_cartQty">
                                                    <?php
                                                    if ($productQuantity <= 0) {
                                                    ?>
                                                        <div class="soldout_cart">Habis</div>
                                                    <?php
                                                    } else {
                                                    ?>
                                                        <div class="bx_qty_cartItem box_qtyitem">
                                                            <?php
                                                            if ($cartQty > 1) {
                                                            ?>
                                                                <div class="span_qtyItem qty_minus btn_update_itemCart" data-action="minus" data-id="<?php echo $keyId; ?>" data-max="<?php echo $productQuantity; ?>" title="Kurang">-</div>
                                                            <?php
                                                            } else {
                                                            ?>
                                                                <div class="span_qtyItem qty_minus btn_update_itemCart max" data-action="minus" data-id="<?php echo $keyId; ?>" data-max="<?php echo $productQuantity; ?>" title="Sudah Minimum">-</div>
                                                            <?php
                                                            }
                                                            ?>
                                                            <input type="text" name="qty_item_<?php echo $keyId; ?>" class="tx_cart_qtyItem" value="<?php echo $cartQty; ?>" readonly="readonly" required="required">
                                                            <?php
                                                            if ($cartQty >= $productQuantity) {
                                                            ?>
                                                                <div class="span_qtyItem qty_add btn_update_itemCart max" data-action="plus" data-id="<?php echo $keyId; ?>" data-max="<?php echo $productQuantity; ?>" title="Sudah Maksimal">+</div>
                                                            <?php
                                                            } else { // if can plus qty
                                                            ?>
                                                                <div class="span_qtyItem qty_add btn_update_itemCart" data-action="plus" data-id="<?php echo $keyId; ?>" data-max="<?php echo $productQuantity; ?>" title="Tambah">+</div>
                                                            <?php
                                                            }
                                                            ?>
                                                        </div>
                                                    <?php
                                                    }
                                                    ?>
                                                    <div class="mg_loadingbar">
                                                        <img class="mg_tree" src="<?php bloginfo('template_directory'); ?>/library/images/loading_ico.gif">
                                                    </div>
                                                    <div class="keterangan_cart" data-id="<?php echo $keyId; ?>" style="margin: 50px 0 0 35px; color: red; font-weight: 500; font-size: 11px;"></div>
                                                </div>
                                                <div class="col-md-3 col_mid_cartItem des_display">
                                                    <div class="col-md-7" style="padding-left: 5px;">
                                                        <div class="ht_price_totalpcs">
                                                            <?php if (intval($salePrice) !== 0) { ?>
                                                                <div class="tx_price">Rp <span class="total_cart_price" data-id="<?= $key ?>"><?= number_format($salePriceTotal) ?></span></div>
                                                                <?php if (intval($salePriceTotal) > 0) : ?>
                                                                    <div class="tx_price_sale" style="text-decoration: none; margin-top: -15px;">
                                                                        <span style="text-decoration: line-through;">Rp <span class="total_cart_price_old" data-id="<?= $key ?>"><?= number_format($priceTotal) ?></span></span>
                                                                        <span class="discount"><?php echo $discountPercentage; ?> %</span>
                                                                    </div>
                                                                <?php endif; ?>
                                                            <?php } else { ?>
                                                                Rp <span class="total_cart_price" data-id="<?= $key ?>"><?php echo number_format($priceTotal); ?></span>
                                                            <?php } ?>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-5 col_right_cartItemlist mob_cartLove">
                                                        <?php
                                                        $queryWishlist = $wpdb->get_row("SELECT * FROM ldr_wishlist WHERE user_id = $userId AND product_id = $productId");

                                                        if ($queryWishlist === null) {
                                                        ?>
                                                            <div class="wish_itemCart addWishlist" title="Tambah ke wishlist" data-id="<?php echo $productId; ?>">
                                                                <span class=""><i class="fas fa-heart love"></i></span>
                                                            </div>
                                                        <?php
                                                        } else {
                                                        ?>
                                                            <div class="wish_itemCart act removewishlist" title="Hapus dari wishlist" data-id="<?php echo $productId; ?>">
                                                                <span class=""><i class="fas fa-heart love"></i></span>
                                                            </div>
                                                        <?php
                                                        }
                                                        ?>
                                                        <div class="del_itemCart btn_del_itemCart" title="Hapus produk ini" data-id="<?php echo $keyId; ?>">
                                                            <span><i class="fas fa-trash"></i></span>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                    <?php
                                        }
                                    }
                                    ?>
                                </div>
                            <?php
                            }

                            if ($soldout) {
                            ?>
                                <div class="err_infocart">
                                    <div class="alert alert-danger ">
                                        <b>Maaf,</b> ada produk yang sudah kosong, silahkan hapus produk tersebut untuk bisa melanjutkan.
                                    </div>
                                </div>
                            <?php
                            } else {
                            ?>
                                <div class="err_infocart hide">
                                    <div class="alert alert-danger ">

                                    </div>
                                </div>
                            <?php
                            }
                            ?>

                            <div class="row row_cartBottom">
                                <div class="col-md-6 col_cartBottom">
                                    <div class="info_cartBottom">Keamanan Berbelanja</div>
                                    <div class="mg_cartBottom">
                                        <img src="<?php bloginfo('template_directory'); ?>/library/images/icon_visa.png">
                                        <img src="<?php bloginfo('template_directory'); ?>/library/images/icon_mastercard.png">
                                        <img src="<?php bloginfo('template_directory'); ?>/library/images/icon_jcb.png">
                                    </div>
                                </div>
                                <div class="col-md-4 col_cartBottom right des_display">
                                    <div class="info_cartBottom">Total Harga (<span class="total_cart_qty"><?php echo $totalQty; ?></span> Barang)</div>
                                    <div class="price_cartBottom">Rp <?php echo number_format($totalAllItem); ?></div>

                                </div>
                                <div class="col-md-2 col_cartBottom row_fix_submitCart">
                                    <div class="mob_left_cartCheckout mob_display">
                                        <div class="info_cartBottom">Total Harga (<span class="total_cart_qty"><?php echo $totalQty; ?></span> Barang)</div>
                                        <div class="price_cartBottom">Rp <?php echo number_format($totalAllItem); ?></div>
                                    </div>
                                    <?php
                                    if ($soldout || !$cartIsValid) {
                                    ?>
                                        <input type="button" class="sub_cartCheckout" value="Checkout" disabled="disabled">
                                    <?php
                                    } else {
                                    ?>
                                        <input type="button" class="sub_cartCheckout" value="Checkout" onclick="submitUpdateCart(event, <?php echo ($userId !== 0); ?>);">
                                    <?php
                                    }
                                    ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
<?php
        }

        get_template_part('content', 'popconfirm');

        if ($userId === 0) {
            get_template_part('content', 'poplogin');
        }
    endwhile;
else :
    get_template_part('content', '404pages');
endif;
get_footer();
?>