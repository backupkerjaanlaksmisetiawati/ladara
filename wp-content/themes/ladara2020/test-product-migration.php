<?php
/*
Template Name: Test product migration
*/
?>

<?php
    $resp['updated'] = 0;
    $resp['inserted'] = 0;

    global $wpdb;
    $wpdb_query = "SELECT * FROM ldr_posts WHERE post_type = 'product' AND post_status = 'publish' ORDER BY id DESC";
    $res_query = $wpdb->get_results($wpdb_query, OBJECT);

    foreach ($res_query as $product_post){
        $product = wc_get_product($product_post->ID);
        $decode = json_decode($product);
        $product_id = $decode->id;
        
        $insert['product_id'] = $product_id;
        
        //name
        if(isset($decode->name) && !empty($decode->name))
        $insert['product_name'] = $decode->name;

        //brand
        $brand_data = get_field('product_brand',$product_id);
        if (isset($brand_data) AND is_object($brand_data)) {
            $brand_name = $brand_data->post_title;
            $insert['product_brand'] = $brand_name;
        }else{
            $brand_name = '';
            $insert['product_brand'] = '';
        }

        //category
        $categories = [];
        foreach($decode->category_ids as $cat_id){
            $term = get_term_by('id', $cat_id, 'product_cat');
            array_push($categories, $term->name);
        }
        $insert['product_category'] = implode(', ', $categories);

        //price
        if(isset($decode->price) && !empty($decode->price))
        $insert['product_price'] = $decode->price;

        //sale price
        if(isset($decode->sale_price) && !empty($decode->sale_price))
            $insert['product_saleprice'] = $decode->sale_price;

        //description
        $desc = get_field('product_description',$product_id);
        if(isset($desc) && $desc != ''){
            $insert['product_description'] = $desc;
        }else{
            $insert['product_description'] = '';
        }
        
        //bahan
        $bahan = get_field('product_bahan',$product_id);
        if(isset($bahan) && $bahan != ''){
            $insert['product_bahan'] = $bahan;
        }else{
            $insert['product_bahan'] = '';  
        }
        
        //ukuran
        $ukuran = get_field('product_ukuran',$product_id);
        if(isset($ukuran) && $ukuran != ''){
            $insert['product_ukuran'] = $ukuran;
        }else{
            $insert['product_ukuran'] = '';
        }

        //sku
        if(isset($decode->sku) && !empty($decode->sku))
        $insert['product_sku'] = $decode->sku;

        //main image
        $image = wp_get_attachment_image_src((int)$decode->image_id);
        $insert['main_images'] = $image[0];

        //other image
        $other_images = [];
        foreach ($decode->gallery_image_ids as $other_img){
            $other_img = wp_get_attachment_image_src($other_img);
            array_push($other_images, $other_img[0]);
        }
        $insert['other_images'] = implode(', ', $other_images);
        
        //created date
        $insert['created_date'] = $decode->date_created->date;


        $product_rate = get_field('product_rate',$product_id);
        if(isset($product_rate) && $product_rate != ''){
            $insert['product_rating'] = $product_rate;
        }else{
            $insert['product_rating'] = 0;
        }

        $total_product_rate = get_field('total_product_rate',$product_id);
        if(isset($total_product_rate) && $total_product_rate != ''){
            $insert['product_totalrate'] = $total_product_rate;
        }else{
            $insert['product_totalrate'] = 0;
        }

        $seller_data = get_field('seller_name',$product_id);
        if (isset($seller_data) AND is_object($seller_data)) {
            $seller_name = $seller_data->post_title;
            $insert['seller_name'] = $seller_name;
        }else{
            $seller_name = '';
            $insert['seller_name'] = '';
        }

        $komisi_produk = get_field('komisi_produk',$product_id);
        if(isset($komisi_produk) && $komisi_produk != ''){
            $insert['commission'] = $komisi_produk;
        }else{
            $insert['commission'] = 0;
        }

        //check if data exists
        $check_db_query = "SELECT * FROM ldr_product WHERE product_id = '$product_id' LIMIT 1";
        $check_db = $wpdb->get_results($check_db_query, OBJECT);
        $data_count = count($check_db);
        
        if($data_count > 0){
            //update if exists
            $resp['updated'] = $resp['updated'] + 1;
            $wpdb_insert = $wpdb->update('ldr_product', $insert, array('product_id' => $product_id));
        } else {
            //insert if new
            $resp['inserted'] = $resp['inserted'] + 1;
            $wpdb_insert = $wpdb->insert('ldr_product', $insert);
        }
    }

?>