<?php
/*
Template Name: Profile Address
*/

use Shipper\Location;
?>

<?php get_header(); ?>
<?php if (have_posts()) :
    while (have_posts()) :
        the_post();

        global $wpdb;
        date_default_timezone_set("Asia/Jakarta");
        $current_user = wp_get_current_user();
        $userId = $current_user->ID;
        $referer = home_url() . '/profile/address/';
        $_SESSION['rfr'] = $referer;

        if (isset($userId) and $userId != 0) {
            $query = $wpdb->get_results("SELECT * FROM ldr_user_addresses WHERE user_id = '$userId' AND deleted_at IS NULL ORDER BY created_at DESC");
            $count = count($query);
?>
            <div class="row"></div>

            <input type="hidden" name="u_id" value="<?php echo $userId; ?>">
            <div id="profil_address" class="row row_profile">
                <div class="col-md-3 col_profile des_display">
                    <?php get_template_part('content', 'menu-profile'); ?>
                </div>
                <div class="col-md-9 col_profile">
                    <div class="row row_cont_tab_profile">
                        <div class="bx_tab_profile">
                            <h1 class="ht_profile">Daftar Alamat</h1>
                            <?php
                            if ($count < 10) {
                            ?>
                                <a class="a_tambah_alamat" href="<?php echo home_url(); ?>/profile/address/add/" title="Tambah alamat disini">
                                    <button class="btn_tambah_alamat">
                                        <span>
                                            <i class="fas fa-plus"></i>
                                        </span>
                                        Tambah Alamat
                                    </button>
                                </a>
                            <?php
                            }
                            ?>
                        </div>
                        <?php
                        if ($count > 0) {
                            foreach ($query as $key => $value) {
                                $getLocation = Location::searchLocation($value->postal_code);

                                $tempLocation = array_values(array_filter($getLocation->data->rows, function ($fn) use ($value) {
                                    return $fn->area_id == $value->area_id;
                                }));

                                $addressId = $value->id;
                                $name = htmlspecialchars($value->name);
                                $consignee = htmlspecialchars($value->consignee);
                                $phone = htmlspecialchars($value->phone);
                                $address = htmlspecialchars($value->address);

                                $province = $tempLocation[0]->province_name;
                                $city = $tempLocation[0]->city_name;
                                $suburb = $tempLocation[0]->suburb_name;
                                $postalCode = $value->postal_code;
                                $longitude = $value->longitude;
                                $latitude = $value->latitude;
                                $mainAddress = $value->main_address;
                        ?>
                                <div class="col-md-12 col_daf_alamat">
                                    <h3 class="ht_daf_alamat"><?php echo $name; ?>
                                        <?php if ($mainAddress == 1) { ?>
                                            <span class="a_btn_addr utama">Utama</span>
                                        <?php } ?>
                                    </h3>

                                    <div class="bx_addr_list">
                                        <div class="f_list_addr">
                                            <label>Nama</label>
                                            <span><?php echo $consignee; ?></span>
                                        </div>
                                        <div class="f_list_addr">
                                            <label>Telepon</label>
                                            <span><?php echo $phone; ?></span>
                                        </div>
                                        <div class="f_list_addr">
                                            <label>Alamat</label>
                                            <span>
                                                <?php echo $address . ', ' . $suburb; ?> <br />
                                                <?php echo $city . ', ' . $province . ', ' . $postalCode; ?>
                                            </span>
                                        </div>
                                        <div class="btn_addr_list">
                                            <a href="<?php echo home_url(); ?>/profile/address/add/?address=<?php echo $addressId; ?>" title="Ubah alamat ini.">
                                                <span class="btn_addr_ubah">Ubah</span>
                                            </a>
                                            <a class="btn_addr_del" title="Hapus alamat ini." data-ad_id=<?php echo $addressId; ?>>
                                                <span class="btn_addr_ubah">Hapus</span>
                                            </a>
                                        </div>
                                    </div>
                                </div>
                        <?php
                            }
                        }
                        ?>
                    </div>
                </div>
            </div>
            <?php get_template_part('content', 'popconfirm'); ?>
        <?php
        } else { ?>
            <script>
                // 'Getting' data-attributes using getAttribute
                var plant = document.getElementById('body');
                console.log(plant);
                var hurl = plant.getAttribute('data-hurl');
                location.replace(hurl + '/login/');
            </script>
<?php
        }
    endwhile;
else :
    get_template_part('content', '404pages');
endif;
get_footer();
?>