<?php
/*
Template Name: Warehouse Detail Page
*/
?>

<?php get_header('admin'); ?>

<?php if (have_posts()) : while (have_posts()) : the_post(); ?>

<?php 
date_default_timezone_set("Asia/Jakarta");
$nowdate = date('Y-m-d H:i:s');

$current_user = wp_get_current_user();
$u_id = $current_user->ID;
$u_roles = $current_user->roles;
$admin = 0;

if(isset($u_roles) AND !empty($u_roles)){
  foreach ($u_roles as $key => $value) {
      if(strtolower($value) == 'administrator'){
          $admin = 1;
      }
      if(strtolower($value) == 'shop_manager'){
          $admin = 1;
      }
  }
}

if((isset($u_id) AND $u_id != 0) AND $admin == 1){

if(isset($_GET['so'])){
  $order_id = $_GET['so'];
}else{
  $order_id = '';
}

$data_status = array();
$data_status['wc-pending'] = 'Menunggu Pembayaran';
$data_status['wc-on-hold'] = 'Menunggu Konfirmasi';
$data_status['wc-processing'] = 'Pembayaran diterima';
$data_status['wc-preparing'] = 'Pesanan Diproses';
$data_status['wc-ondelivery'] = 'Pesanan Dikirim';
$data_status['wc-completed'] = 'Pesanan Selesai';
$data_status['wc-cancelled'] = 'Pesanan Dibatalkan';
?>

  <!-- Begin Page Content -->
  <div class="container-fluid">

    <!-- Page Heading -->
    <h1 class="h3 mb-2 text-gray-800">Preparing Order ID <?php echo $order_id; ?></h1>

    <?php 
    $admin_fee = 0;
    $voucher_discount = 0;
    $voucher_code = '';
    $total_allkomisi = 0;
    $order = wc_get_order($order_id);

    if(isset($order) AND !empty($order)){ // jika ada

        $courier_resi = '';
        global $wpdb;
        $query = "SELECT *
                  FROM ldr_orders
                  WHERE order_id = '$order_id' 
                  ";
        $res_query = $wpdb->get_results($query, OBJECT);
        $res_count = count($res_query);
        if ($res_count > 0){
            foreach ($res_query as $key => $value){
                $o_status = $value->order_status;
                $courier_name = $value->courier_name;
                $courier_resi = $value->courier_resi;
                $shipping_total = $value->shipping_price;
                $voucher_id = $value->voucher_id;

                $voucher_code = get_the_title($voucher_id);

                $voucher_discount = $value->voucher_discount;
                $address_id = $value->address_id;
                $notes = $value->notes;
                // $mile_poin = $value->mile_poin;

                if(isset($value->xendit_fee) AND $value->xendit_fee != ''){
                  $admin_fee = $value->xendit_fee;
                }

            }
        }

        $user_id = $order->user_id;
        $user_data = get_userdata( $user_id );
        $user_name = $user_data->data->display_name;
        $order_status  = $order->get_status();
        $info_status = $data_status[$o_status];
        $total_payment = $order->get_total();

        $total_user_payment = (int)$total_payment+(int)$admin_fee;

        $get_Data = $order->get_data();  
        $billing = $get_Data['billing'];
        $first_name = $billing['first_name'];
        $address_1 = $billing['address_1'];
        $address_2 = $billing['address_2'];
        $city = $billing['city'];
        $state = $billing['state'];
        $postcode = $billing['postcode'];
        $country_code = $billing['country'];
        $email = $billing['email'];
        $phone = $billing['phone'];

        $countries_obj   = new WC_Countries();
        $allcountries = $countries_obj->countries;
        $country = $allcountries[$country_code];

        $allstates  = $countries_obj->get_states($country_code);
        $state_name = $allstates[$state];

        $order_date = get_the_date('Y-m-d | h:i:s', $order_id);
        $yearnow = date('Y',strtotime($order_date));
        $monthnow = date('m',strtotime($order_date));
        $daynow = date('d',strtotime($order_date));

        $text_date = date("dmY",strtotime($order_date));
        $inv_id = 'INV/'.$text_date.'/'.$order_id;
    ?>

        <!-- Collapsable Card Example -->
        <div class="card shadow mb-4">
          <!-- Card Header - Accordion -->
          <a href="#collapseCardExample" class="d-block card-header py-3" data-toggle="collapse" role="button" aria-expanded="true" aria-controls="collapseCardExample">
            <h6 class="m-0 font-weight-bold text-primary">Status : <?php echo $info_status; ?></h6>
          </a>

          <div class="col_head_detailWA">

            <div class="alert alert-warning alert-dismissible fade show box_info_detailWA" role="alert">
              <b>Informasi</b> : <br/>
              - Pastikan produk tersedia dan siap di kirim.<br/>
              - Masukkan Nomor Resi / AWB, jika sudah di kirim.
              <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>

            <div class="info_detailWA">Invoice : <b><?php echo $inv_id; ?></b></div>
            <div class="info_detailWA">Order Date : <?php echo $order_date; ?></div>

            <input type="hidden" name="order_id" value="<?php echo $order_id; ?>">

            <?php if(strtolower($order_status) == 'preparing'){ ?>
                <input type="button" class="btn_detailWA btn_confirmdelivery" data-id="<?php echo $order_id; ?>" value="Kirim Sekarang">
            <?php }else if(strtolower($order_status) == 'ondelivery'){ ?>
                <input type="button" class="btn_detailWA btn_confirmdelivery delivery" data-id="<?php echo $order_id; ?>" value="On Delivery">
            <?php } ?>

          </div>

          <!-- Card Content - Collapse -->
          <div class="collapse show" id="collapseCardExample">
            <div class="card-body">
                
              <div class="card mb-4 py-3 border-left-primary">
                <div class="card-body row">

                  <div class="col-md-8">
                    <div class="ht_detailWA">Alamat Penerima :</div>
                    <div class="cont_detailWA">
                        <br/>
                        Nama Penerima : <?php echo $first_name; ?> <br/>
                        No.Telepon : <?php echo $phone; ?> <br/>
                        Alamat : <br/>
                        <?php echo $address_1; ?> <br/>
                        <?php echo $address_2.', '.$city.', '.$state_name; ?> <br/>
                        <?php echo $country.'. '.$postcode; ?>
                    </div>

                    <?php if(isset($notes) AND $notes != ''){ ?>
                        <div class="cont_detailWA space">Note : <?php echo $notes; ?></div>
                    <?php } ?>

                  </div>
                  <div class="col-md-4">
                    <div class="ht_detailWA">Kurir Pengiriman :</div>
                    <div class="cont_detailWA">
                        <?php echo $courier_name ?>
                        <br/>
                        Rp <?php echo number_format($shipping_total); ?>
                    </div>

                    <?php /*
                    <a target="_blank" href="<?php echo home_url(); ?>/export-print-alamat?so=<?php echo $order_id; ?>" class="btn btn-secondary btn-icon-split a_print_alamat">
                      <span class="icon text-white-50">
                        <i class="fas fa-print"></i>
                      </span>
                      <span class="text">Print Alamat</span>
                    </a>
                    */ ?>
                    
                    <a target="_blank" href="<?php echo home_url(); ?>/export-surat-jalan?so=<?php echo $order_id; ?>" class="btn btn-info btn-icon-split a_print_alamat">
                      <span class="icon text-white-50">
                        <i class="fas fa-print"></i>
                      </span>
                      <span class="text">Print Purchase Order</span>
                    </a>

                    <a target="_blank" href="<?php echo home_url(); ?>/export-invoice-admin?so=<?php echo $order_id; ?>" class="btn btn-secondary btn-icon-split a_print_alamat">
                      <span class="icon text-white-50">
                        <i class="fas fa-print"></i>
                      </span>
                      <span class="text">Print Invoice</span>
                    </a>


                    <?php // if(strtolower($order_status) == 'ondelivery'){ ?>
                      <div class="ht_detailWA space">Nomor Resi / AWB :</div>
                      <input type="text" name="nomor_resi" class="txt_nomor_resi" placeholder="nomor resi..." value="<?php echo $courier_resi; ?>">
                      <div class="err_resi">Nomor resi tidak boleh kosong.</div>
                      <input type="button" class="btn_updateResi" data-id="<?php echo $order_id; ?>" value="update">
                    <?php // } ?>

                  </div>
          
                </div>
              </div>

              <div class="card mb-4 py-3 border-left-primary">
                <div class="card-body">
                    <div class="ht_detailWA">Daftar Produk :</div>

                    <?php 
                    $jumlah_product = 0;
                    $total_berat = 0;
                    $total_harga = 0;
                    $save_stock = array();
                    global $wpdb;
                    $query = "SELECT *
                              FROM ldr_orders_item
                              WHERE order_id = '$order_id' 
                              ";
                    $res_query = $wpdb->get_results($query, OBJECT);
                    $res_count = count($res_query);
                    if ($res_count > 0){
                        foreach ($res_query as $key => $value){
                            $product_id = $value->product_id;
                            $product = wc_get_product( $product_id );

                            $product_name = get_the_title($product_id);
                            $db_stock_id = $value->stock_id;
                            $product_qty = $value->qty;
                            $product_price = $value->price;
                            $product_komisi = $value->komisi;
                            $total_komisi = (int)$product_komisi*(int)$product_qty;
                            $total_allkomisi = (int)$total_allkomisi+(int)$total_komisi;

                            $price_total = (int)$product_price*(int)$product_qty;
                            $total_harga = (int)$total_harga+(int)$price_total;
                            
                            $product_variasi = $value->variasi;
                            $product_weight = $product->get_weight();
                            $cal_weight = (int)$product_weight*(int)$product_qty;
                            $total_berat = (int)$total_berat+(int)$cal_weight;
                            
                            $image_id = get_post_thumbnail_id($product_id);
                            $thumb = wp_get_attachment_image_src( $image_id, 'small' );
                            if($thumb){
                                $urlphoto = $thumb['0'];
                            }else{
                                $urlphoto = get_template_directory_uri().'/library/images/sorry.png';
                            }
                            $alt = 'Milenial Mall';

                            $jumlah_product = (int)$jumlah_product+(int)$product_qty;


                    ?>

                            <div class="row box_cartItem pro_<?php echo $product_id; ?>">

                              <div class="col-md-2 col_top_cartItem">
                                <div class="mg_cartItem">
                                  <img src="<?php echo $urlphoto; ?>" alt="<?php echo $alt; ?>">
                                </div>
                              </div>
                              <div class="col-md-10 col_top_cartItem">
                                <div class="row">
                                  <div class="col-md-6 col_mid_cartItem">
                                    <a href="<?php echo get_permalink($product_id); ?>" title="Lihat detail <?php echo $product_name; ?>">
                                      <h2 class="ht_cartItem"><?php echo $product_name; ?></h2>
                                    </a>
                                    <?php if(isset($product_variasi) AND $product_variasi != ''){ ?>
                                      <div class="opsi_cartItem">Variasi : <?php echo $product_variasi; ?></div>
                                    <?php } ?>  

                                      <div class="opsi_cartItem">Berat : <?php echo $product_weight; ?> gram</div>

                                      <div class="opsi_cartItem">Harga : Rp <?php echo number_format($product_price); ?></div>

                                  </div>
                                  <div class="col-md-2 col_mid_cartItem">
                                    
                                    <div class="info_totalpcs">Quantity</div>
                                    <div class="ht_pricepcs">
                                      <?php echo $product_qty; ?> pcs
                                    </div>

                                  </div>
                                  <div class="col-md-2 col_mid_cartItem">
                                    
                                    <div class="info_totalpcs">Total Harga</div>
                                    <div class="ht_pricepcs">
                                      Rp <?php echo number_format($price_total); ?>
                                    </div>

                                  </div>

                                  <?php // if($total_komisi > 0){ ?>
                                    <div class="col-md-2 col_mid_cartItem">
                                      
                                      <div class="info_totalpcs">Total Komisi</div>
                                      <div class="ht_pricepcs" style="color: green;">
                                        Rp <?php echo number_format($total_komisi); ?>
                                      </div>

                                    </div>
                                  <?php // } ?>
                      
                                </div>
                              </div>
                            </div>

                    <?php 
                        }
                    }
                    ?>

                    <div class="row">
                      <div class="col-md-6">
                        <div class="info_high_detailWA">Total Produk : <b><?php echo $jumlah_product; ?> produk</b></div>
                        <div class="info_high_detailWA">Total Berat : <b><?php echo number_format($total_berat); ?> gram</b></div>
                      </div>
                      <div class="col-md-6">

                        <div class="info_high_detailWA">
                            <?php echo $courier_name ?> : 
                            Rp <?php echo number_format($shipping_total); ?>
                        </div>

                        <?php if(isset($voucher_discount) AND $voucher_discount != 0){ ?>
                          <div class="info_high_detailWA discount" style="color: green;">Discount <span style="font-size: 10px;"><?php echo $voucher_code; ?></span> : <b>- Rp <?php echo number_format($voucher_discount); ?></b>
                          </div>
                        <?php } ?>

                        <div class="info_high_detailWA">
                          Admin Fee : Rp <?php echo number_format($admin_fee); ?>
                        </div>

                        <div class="info_high_detailWA discount">
                            Total Pembayaran User : <b style="font-size: 13px;"> Rp <?php echo number_format($total_user_payment); ?></b>
                        </div>
                       
                        <div class="col_line_boradmin">

                            <div class="info_high_detailWA">
                              Total Komisi : <span style="color: green;">Rp <?php echo number_format($total_allkomisi); ?></span>
                            </div>

                            <?php $total_payladara = (int)$total_user_payment-(int)$total_allkomisi-(int)$admin_fee-(int)$voucher_discount-(int)$shipping_total; ?>
                            <div class="info_high_detailWA discount">
                                Total Pembayaran Ladara : <b style="font-size: 16px;color: red;"> Rp <?php echo number_format($total_payladara); ?></b>
                            </div>

                            <div class="inf_alert_payladara">*Ladara Indonesia akan melakukan pembayaran kepada merchant / seller / penjual berdasarkan jumlah pada <b>Total Pembayaran Ladara</b></div>

                        </div>

                      </div>
                    </div>
                    
                </div>
              </div>

            </div>
          </div>
        </div>

    <?php 
    }else{ // jika tidak ada data order id
    ?>

        <!-- Begin Page Content -->
        <div class="container-fluid">

          <!-- 404 Error Text -->
          <div class="text-center">
            <div class="error mx-auto" data-text="404">404</div>
            <p class="lead text-gray-800 mb-5">Order ID not Found</p>
            <p class="text-gray-500 mb-0">Sepertinya anda lelah, silahkan pilih order ID yang lain.</p>
            <a href="<?php echo home_url(); ?>/dashboard/warehouse/">&larr; Back to Dashboard</a>
          </div>

        </div>
        <!-- /.container-fluid -->

    <?php
    }
    ?>

  </div>
  <!-- /.container-fluid -->

<?php }else{ ?>
    <script>
        // 'Getting' data-attributes using getAttribute
        var plant = document.getElementById('body');
        console.log(plant);
        var hurl = plant.getAttribute('data-hurl'); 
        location.replace(hurl+'/login/'); 
    </script>
<?php } ?>

<?php endwhile; ?>
<?php else : ?>
    <?php get_template_part( 'content', '404pages' ); ?>  
<?php endif; ?>
<?php get_footer('admin'); ?>