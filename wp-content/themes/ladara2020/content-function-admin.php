<?php 

function ajax_adm_addVariation(){

	date_default_timezone_set("Asia/Jakarta");
	$nowdate = date('Y-m-d H:i:s');

	$current_user = wp_get_current_user();
	$u_id = $current_user->ID;
	$u_name = $current_user->display_name;

	$product_id = $_REQUEST['product_id'];
	$adm_warna = $_REQUEST['adm_warna'];
	$adm_ukuran = $_REQUEST['adm_ukuran'];
	$adm_nomor = $_REQUEST['adm_nomor'];
	$adm_stock = $_REQUEST['adm_stock'];
	$valid = 0;

    global $wpdb;
    $wpdb_query = "SELECT * FROM ldr_stock WHERE product_id = '$product_id' AND ukuran_value = '$adm_ukuran' AND nomor_value = '$adm_nomor' AND warna_value = '$adm_warna' ";
    $res_query = $wpdb->get_results($wpdb_query, OBJECT);
    $count_res = count($res_query);
    if($count_res == 0){
        if($adm_stock > 0){
        	// ============== Insert STOCK PRODUCT ====================
	        	$query = "INSERT INTO ldr_stock(product_id,ukuran_value,nomor_value,warna_value,product_quantity) 
		                VALUES('$product_id','$adm_ukuran','$adm_nomor','$adm_warna','$adm_stock')";
		    	$result = $wpdb->query($query);
	    	// ============== Insert STOCK PRODUCT ====================
	    	$valid = 1;

			$wpdb_query = "SELECT * FROM ldr_stock WHERE product_id = '$product_id' AND ukuran_value = '$adm_ukuran' AND nomor_value = '$adm_nomor' AND warna_value = '$adm_warna' ";
		    $res_query = $wpdb->get_results($wpdb_query, OBJECT);
		    $count_res = count($res_query);
		    if($count_res > 0){
		    	foreach ($res_query as $key => $value){
		            	$id_stock = $value->id;
						// ============== Insert STOCK LOG ====================
			     			$status = 'new';
			     			$from_stock = 0;
			            	$query = "INSERT INTO ldr_stock_log(created_date,product_id,stock_id,from_stock,to_stock,status,admin_name) 
					                VALUES('$nowdate','$product_id','$id_stock','$from_stock','$adm_stock','$status','$u_name')";
					    	$result = $wpdb->query($query);
						// ============== Insert STOCK LOG ====================
		        }
		    }
		} 
    }
	// $valid = 'masuk pak eko';
	echo json_encode($valid);
	die();
}   
add_action('wp_ajax_ajax_adm_addVariation','ajax_adm_addVariation');
add_action('wp_ajax_nopriv_ajax_adm_addVariation', 'ajax_adm_addVariation');


function ajax_adm_updateVariation(){

	date_default_timezone_set("Asia/Jakarta");
	$nowdate = date('Y-m-d H:i:s');

	$current_user = wp_get_current_user();
	$u_id = $current_user->ID;
	$u_name = $current_user->display_name;

	$stock_id = $_REQUEST['stock_id'];
	$product_qty = $_REQUEST['product_qty'];

	$valid = 0;

    global $wpdb;
    $wpdb_query = "SELECT * FROM ldr_stock WHERE id = '$stock_id' ";
    $res_query = $wpdb->get_results($wpdb_query, OBJECT);
    $count_res = count($res_query);
    if($count_res > 0){
        foreach ($res_query as $key => $value){
			$id_stock = $value->id;
			$product_id = $value->product_id;
			$product_quantity = $value->product_quantity;

            if($product_quantity >= 0){

            	// =============== UPDATE STOCK ==================
	                global $wpdb;
					$wpdb_query_update = " UPDATE ldr_stock
					              SET product_quantity=$product_qty
					              WHERE id = '$id_stock'
					            ";
					$res_query_update = $wpdb->query($wpdb_query_update);
				// =============== UPDATE STOCK ==================

				// ============== Insert STOCK LOG ====================
		 			$status = 'update';
		 			$from_stock = $product_quantity;
		 			$to_stock = $product_qty;
		        	$query = "INSERT INTO ldr_stock_log(created_date,product_id,stock_id,from_stock,to_stock,status,admin_name) 
			                VALUES('$nowdate','$product_id','$id_stock','$from_stock','$to_stock','$status','$u_name')";
			    	$result = $wpdb->query($query);
				// ============== Insert STOCK LOG ====================	

			    $valid = 1;

			}
		} 
    }
	// $valid = 'masuk pak eko';
	echo json_encode($valid);
	die();
}   
add_action('wp_ajax_ajax_adm_updateVariation','ajax_adm_updateVariation');
add_action('wp_ajax_nopriv_ajax_adm_updateVariation', 'ajax_adm_updateVariation');


function ajax_adm_deleteVariation(){

	date_default_timezone_set("Asia/Jakarta");
	$nowdate = date('Y-m-d H:i:s');

	$current_user = wp_get_current_user();
	$u_id = $current_user->ID;
	$u_name = $current_user->display_name;

	$stock_id = $_REQUEST['stock_id'];
	$product_qty = $_REQUEST['product_qty'];

	$valid = 0;

    global $wpdb;
    $wpdb_query = "SELECT * FROM ldr_stock WHERE id = '$stock_id' ";
    $res_query = $wpdb->get_results($wpdb_query, OBJECT);
    $count_res = count($res_query);
    if($count_res > 0){
        foreach ($res_query as $key => $value){
			$id_stock = $value->id;
			$product_id = $value->product_id;
			$product_quantity = $value->product_quantity;

			// ==================== DELETE GOOD BYE ======================
				global $wpdb;
				$query = "DELETE FROM ldr_stock WHERE id = '$id_stock' ";
				$result = $wpdb->query($query);
			// ==================== DELETE GOOD BYE ======================

			// ============== Insert STOCK LOG ====================
	 			$status = 'delete';
	 			$from_stock = $product_quantity;
	 			$to_stock = $product_qty;
	        	$query = "INSERT INTO ldr_stock_log(created_date,product_id,stock_id,from_stock,to_stock,status,admin_name) 
		                VALUES('$nowdate','$product_id','$id_stock','$from_stock','$to_stock','$status','$u_name')";
		    	$result = $wpdb->query($query);
			// ============== Insert STOCK LOG ====================	

		    $valid = 1;

			
		} 
    }
	// $valid = 'masuk pak eko';
	echo json_encode($valid);
	die();
}   
add_action('wp_ajax_ajax_adm_deleteVariation','ajax_adm_deleteVariation');
add_action('wp_ajax_nopriv_ajax_adm_deleteVariation', 'ajax_adm_deleteVariation');


function adm_kategori_topup_list($params) {
	global $wpdb;

	$limit = adm_gld_limit();
    $page = $params["pg"];

	/** where */
		$where = "";

		if(isset($params["action"])) {
			$where .= " WHERE";

			if(!empty($params["name"])) {
				$where .= " name LIKE '%" . $params["name"] . "%'";
			}

			if(!empty($params["name"]) && $params["status"] !== "all") {
				$where .= " AND";
			}

			if($params["status"] !== "all") {
				$where .= " status=" . $params["status"] . "";
			}
		} else {
			$where .= " WHERE status!=2";
		}
	/** where */

	/** sorting */
		$order_by = "";

		if(!isset($params["order"]) && !isset($params["sort"])) {
			$order_by .= " ORDER BY id DESC";
		}

		if(isset($params["order"]) && isset($params["sort"])) {
			if($params["order"] === "name") {
				$order_by .= " ORDER BY name";
			}
			if($params["order"] === "slug") {
				$order_by .= " ORDER BY slug";
			}
			if($params["order"] === "status") {
				$order_by .= " ORDER BY status";
			}
			if($params["order"] === "update_date") {
				$order_by .= " ORDER BY updated_at";
			}

			$sort = " " . $params["sort"];
			$order_by .= $sort;
		}
	/** sorting */

	/** data */
		$start = ($page > 1) ? ($page * $limit) - $limit : 0;
		$query = "SELECT * FROM ldr_topup_categories";
		$query .= $where;
		$query .= $order_by;
		$query .= " LIMIT " . $start . ", " . $limit;
		// echo $query;exit;
		$list = $wpdb->get_results($query, OBJECT);
	/** data */

	/** total */
		$total_query = "SELECT COUNT(*) as total FROM ldr_topup_categories";
		$total_query .= $where;
		$total = $wpdb->get_row($total_query, OBJECT);
		$total = (!empty($total)) ? $total->total : 0;
	/** total */

	$rtn = [
		"total" => $total,
		"datas" => $list
	];

	return $rtn;
}
add_action( "adm_kategori_topup_list", "adm_kategori_topup_list", 0 );

?>