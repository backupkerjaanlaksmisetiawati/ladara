<div id="modal-ewallet" class="modal fade" role="dialog">
    <div class="modal-dialog" style="width: 53rem;">
        <div class="modal-content" style="padding: 3rem; height: 42rem;">
            <div class="modal-header" style="border-bottom: 0px;">
                <button type="button" class="close" data-dismiss="modal" style="margin-top: -25px; font-size: 25px;">&times;</button>
                <h4 class="modal-title" align="center">
                    <img id="selected_ewallet_img">
                    <div style="margin: 3rem 0rem;">
                        <span id="selected_ewallet_txt" style="border-bottom: 1px solid #dddddd; display: inline-block; width: 60%;">&nbsp;</span>
                    </div>
                </h4>
            </div>
            <div class="modal-body" style="margin-top: -2rem;">
                <div class="f_ad_input">
                    <input type="text" id="nohp" class="ad_input" placeholder="Masukkan No. Handphone" required="required" maxlength="14" />
                    <div id="err_handphone" class="f_err">*Masukkan No. Handphone</div>
                </div>
                <div class="login_fields">
                    <input type="button" id="submit_nohp" data-dismiss="modal" class="sub_login" value="Selanjutnya" />
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    document.querySelector('#nohp').addEventListener('keyup', function(e) {
        let number = document.querySelector('#nohp').value.replace(/[^0-9]/g, '')
        document.querySelector('#nohp').value = number
    })

    document.querySelector('#submit_nohp').addEventListener('click', function() {
        let nohp = document.querySelector('#nohp').value
        let ewallet = document.querySelector('#selected_ewallet').innerText.trim()

        document.querySelector('[name="nohp"]').value = nohp
        document.querySelectorAll('#selected_ewallet div')[1].textContent = `${ewallet} (${nohp})`
    }, false);
</script>