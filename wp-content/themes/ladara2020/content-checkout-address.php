<?php

use Shipper\Location;

date_default_timezone_set("Asia/Jakarta");
$currentUser = wp_get_current_user();
$userId = $currentUser->ID;

if (isset($_SESSION['noaddress'])) {
    $no_address = $_SESSION['noaddress'];
} else {
    $no_address = '';
}

$queryAddress = $wpdb->get_results("SELECT * FROM ldr_user_addresses WHERE user_id = '$userId' AND deleted_at IS NULL ORDER BY created_at DESC");
$countAddress = count($queryAddress);
?>
<style type="text/css">
    #wp-submit {
        background: #0080FF;
    }
</style>
<div class="row row_globalPopUp">
    <div class="col-md-2 col_hide_popup"></div>
    <div class="col-md-8 col_changeAddress chaddr_auto animated zoomIn">
        <h2 class="ht_checkout_addr">Daftar Alamat</h2>

        <?php if ($countAddress < 10) { ?>
            <div id="add_btc_addaddress" class="btn_tambah_alamat checkout" title="Tambah alamat disini"><span><i class="fas fa-plus"></i></span> Tambah Alamat</div>
        <?php
        }
        ?>

        <div class="row row_checkout_addr bx_list_chk_addr">

            <?php
            global $wpdb;

            if ($countAddress > 0) {
                foreach ($queryAddress as $key => $value) {
                    $addressId = $value->id;
                    $name = $value->name;
                    $consignee = $value->consignee;
                    $phone = $value->phone;
                    $address = $value->address;
                    $postalCode = $value->postal_code;
                    $longitude = $value->longitude;
                    $latitude = $value->latitude;
                    $mainAddress = $value->main_address;

                    $getLocation = Location::searchLocation($value->postal_code);
                    $location = array_values(array_filter($getLocation->data->rows, function ($fn) use ($value) {
                        return $fn->area_id == $value->area_id;
                    }));

                    $provinceName = $location[0]->province_name;
                    $cityName = $location[0]->city_name;
                    $suburbName = $location[0]->suburb_name; ?>
                    <div class="col-md-12 col_daf_alamat col_mob_alamat">
                        <h3 class="ht_daf_alamat"><?php echo $name; ?>
                            <?php if ($mainAddress == 1) { ?>
                                <span class="a_btn_addr utama">Utama</span>
                            <?php } ?>
                        </h3>

                        <div class="bx_addr_list">
                            <div class="f_list_addr">
                                <label>Nama</label>
                                <span><?php echo $consignee; ?></span>
                            </div>
                            <div class="f_list_addr">
                                <label>Telepon</label>
                                <span><?php echo $phone ?></span>
                            </div>
                            <div class="f_list_addr">
                                <label>Alamat</label>
                                <span>
                                    <?php echo $address . ', ' . $suburbName; ?> <br />
                                    <?php echo $cityName . ', ' . $provinceName . ', ' . $postalCode; ?>
                                </span>
                            </div>

                            <div class="btn_addr_list">
                                <a href="<?php echo home_url(); ?>/profile/address/add/?address=<?= $addressId; ?>&rfr=<?= base64_encode(home_url('/checkout/')) ?>" title="Ubah alamat ini."><span class="a_ubah_address">Ubah</span></a>

                                <?php
                                if ($_SESSION['checkoutAddressId'] !== $addressId) {
                                ?>
                                    <a class="a_pilih_address" title="Pilih alamat ini." data-adid=<?php echo $addressId; ?>>Pilih</a>
                                <?php
                                }

                                if ((int) $mainAddress !== 1) {
                                ?>
                                    <a class="btn_addr_del" title="Hapus alamat ini." data-ad_id=<?php echo $addressId; ?>>
                                        <span class="btn_addr_ubah">Hapus</span>
                                    </a>
                                <?php
                                }
                                ?>
                            </div>
                        </div>
                    </div>
            <?php
                }
            }
            ?>

            <a class="a_hide_popup" title="Tutup tampilan">
                <div class="bx_backShop">
                    <span class="glyphicon glyphicon-menu-left"></span> Kembali
                </div>
            </a>
        </div>
    </div>
    <div class="col-md-2 col_hide_popup"></div>
</div>

<?php $ad_id = ''; // default for new address
?>
<div class="row row_fix_add_address">
    <div class="col-md-3 col_hide_popup"></div>
    <div class="col-md-6 col_add_address animated zoomIn">
        <div class="bx_f_addAddress">
            <div class="ht_add_address ht_add_chkaddress">Tambah Alamat</div>
            <form id="form_addAddress" class="form_addAddress bx_chk_addAddress" action="" onsubmit="submit_addAddress(event);" method="get">
                <input type="hidden" name="u_id" value="<?php echo $userId; ?>">
                <input type="hidden" name="ad_id" value="<?php echo $ad_id; ?>">
                <input type="hidden" name="referer" value="<?php echo $referer; ?>">

                <div class="f_aform_add">
                    <label class="cont_check">Atur Sebagai Alamat Utama
                        <input type="checkbox" name="alamat_utama" value="1">
                        <span class="checkmark"></span>
                    </label>
                </div>
                <div class="f_aform_add">
                    <input type="text" name="nama_alamat" class="txt_aform" required="required" value="" placeholder="Nama Alamat (rumah, apartemen, kantor)">
                    <div id="p1" class="err_aform"></div>
                </div>
                <div class="f_aform_add">
                    <input type="text" name="nama_lengkap" class="txt_aform" required="required" value="" placeholder="Nama Penerima">
                    <div id="p2" class="err_aform"></div>
                </div>
                <div class="f_aform_add">
                    <input type="text" name="nomor_hp" class="txt_aform onlyphone" required="required" value="" placeholder="Nomor Telepon">
                    <div id="p3" class="err_aform"></div>
                </div>
                <div class="f_aform_add">
                    <textarea class="area_aform" name="alamat" required="required" placeholder="Alamat Lengkap"></textarea>
                    <div id="p4" class="err_aform"></div>
                </div>
                <div class="f_aform_add2">
                    <select id="ch_province" name="provinsi" class="sel_aform ch_province" onchange="get_kotaSel2(event)" required="required">
                        <option value="">Pilih Provinsi</option>
                        <?php
                        $getProvinces = Location::getProvinces();

                        foreach ($getProvinces->data->rows as $province) {
                        ?>
                            <option value="<?php echo $province->id; ?>"><?php echo $province->name; ?></option>
                        <?php
                        }
                        ?>
                    </select>
                    <div id="p5" class="err_aform"></div>
                </div>
                <div class="f_aform_add2">
                    <select id="ch_city" name="kota" class="sel_aform ch_regencies" onchange="get_kecamatanSel2(event)" required="required">
                        <option value="">Pilih Kota</option>
                    </select>
                    <div id="p6" class="err_aform"></div>
                </div>
                <div class="f_aform_add2">
                    <select id="ch_suburb" name="kecamatan" class="sel_aform ch_districts" onchange="get_kodeposSel2(event)" required="required">
                        <option value="">Pilih Kecamatan</option>
                    </select>
                    <div id="p7" class="err_aform"></div>
                </div>
                <div class="f_aform_add2">
                    <input type="hidden" name="kodepos">
                    <select id="ch_area" name="kelurahan" class="sel_aform ch_kodepos" onchange="setAreaId(event)" required="required">
                        <option value="">Pilih Kode pos</option>
                    </select>
                    <div id="p8" class="err_aform"></div>
                </div>

                <div class="f_aform">
                    <label>Pin Alamat</label>
                    <div id="map" style="width: 100%; height: 250px;"></div>
                    <div id="" class="err_aform"></div>
                </div>

                <div class="f_aform">
                    <label class="cont_check">Alamat Utama
                        <input type="checkbox" name="alamat_utama" value="1">
                        <span class="checkmark"></span>
                    </label>
                </div>

                <input type="hidden" name="longitude" value="0">
                <input type="hidden" name="latitude" value="0">
                <div class="label_success"></div>

                <div class="f_aform_btns">
                    <div class="err_info alert-success"><b>Selamat!</b> Alamat ini telah di tambahkan.</div>
                    <div class="err_info alert-danger"><b>Gagal!</b> Ada yang kurang atau telah mencapai batas maksimal 10 alamat.</div>
                    <input id="des_backchk" type="button" class="btn_aform back_add_addr des_display" value="Kembali">
                    <input type="submit" class="sub_aform" id="" value="Simpan">
                    <input id="mob_backchk" type="button" class="btn_aform back_add_addr mob_display" value="Kembali">
                </div>
            </form>
        </div>
    </div>
    <div class="col-md-3 col_hide_popup"></div>
</div>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAhLLkF-IXL7ZMmbgCWO0DAqD7thqz778w&callback=initMap&libraries=&v=weekly" defer></script>
<script>
    "use strict";

    let longitude = document.querySelector('[name="longitude"]')
    let latitude = document.querySelector('[name="latitude"]')

    let getLoc = function getLocation() {
        if (longitude.value === '0' || latitude.value === '0') {
            if (navigator.geolocation) {
                navigator.geolocation.getCurrentPosition(function(position) {
                    longitude.value = position.coords.longitude
                    latitude.value = position.coords.latitude
                })
            }
        }
    }()

    let markers = []

    function placeMarker(position, map) {
        let marker = new google.maps.Marker({
            position: position,
            map: map
        })

        map.panTo(position)
        markers.push(marker)

        longitude.value = position.lng()
        latitude.value = position.lat()
    }

    function initMap() {
        const map = new google.maps.Map(document.getElementById("map"), {
            zoom: 18,
            center: {
                lng: parseFloat(longitude.value),
                lat: parseFloat(latitude.value)
            }
        })

        let marker = new google.maps.Marker({
            position: {
                lng: parseFloat(longitude.value),
                lat: parseFloat(latitude.value)
            },
            map,
            title: "Pin Lokasi"
        })

        markers.push(marker)

        map.addListener('click', function(e) {
            for (let marker of markers) {
                marker.setMap(null)
            }

            placeMarker(e.latLng, map)
        })
    }
</script>