<?php
/*
Template Name: Page Invoice
*/
if (!defined('ABSPATH')) {
    exit;
}
?>
<?php get_header(); ?>
<?php if (have_posts()) : while (have_posts()) : the_post(); ?>

        <style>
            .txt-right {
                text-align: right;
            }

            .row-invoice .head-back {
                margin-top: 50px;
            }

            .row-invoice .back {
                margin-left: 10px;
                font-weight: bold;
            }

            .row-invoice .logo {
                text-align: center;
            }

            .row-invoice .logo img {
                width: 150px;
            }

            .row-invoice .invoice-container {
                background-color: #ffffff;
                border-radius: 20px;
                padding: 50px 25px;
                margin-bottom: 100px;
            }

            .row-invoice .invoice-no {
                color: #0080FF;
            }

            .invoice-download,
            .invoice-download:hover,
            .invoice-download:focus,
            .invoice-download:active {
                background-color: #0080FF;
                border-radius: 20px;
                border: 1px solid #0080FF;
                outline: unset;
            }

            .invoice-box-addr {
                background: rgba(0, 128, 255, 0.2);
                border-radius: 20px;
                width: 100%;
                float: left;
                padding: 30px 15px;
            }

            .invoice-border-right {
                border-right: 2px solid #aeb2b6;
            }

            .no-padding {
                padding: 0 !important;
            }

            .mt-3 {
                margin-top: 3em;
            }

            .invoice-table {
                width: 100%;
                border: 1px solid #DDDDDD;
                border-radius: 20px;
                border-collapse: separate !important;
                padding: 20px;
            }

            .invoice-table thead tr th {
                padding-bottom: 20px;
                border-bottom: 1px solid #dddddd;
            }

            .invoice-table tbody tr td {
                padding: 10px 0;
            }

            .invoice-table tbody tr:last-child td {
                border-top: 1px solid #dddddd;
            }

            .f-bold {
                font-weight: bold;
            }

            .table_calinvoice td {
                padding: 3px;
            }

            .table_calinvoice {
                width: 40%;
                float: right
            }

            @media only screen and (max-width:769px) {
                .row-invoice .invoice-container {
                    padding: 20px 0px;
                    border-radius: 10px;
                    margin-bottom: 30px;
                }

                .mob_noright {
                    margin: auto;
                    text-align: center;
                    margin-top: 15px;
                    margin-bottom: 15px;
                }

                .invoice-border-right {
                    border: 0;
                }

                .invoice-box-addr {
                    padding: 15px 5px;
                }

                .row-invoice {
                    font-size: 12px;
                }

                .table_calinvoice {
                    width: 100%;
                }

                .invoice-table {
                    font-size: 10px;
                }


            }
        </style>

        <?php
        $orderId = $_GET['id'];
        $getOrderMerchant = sendRequest('GET', MERCHANT_URL . '/api/orders/get-order/' . $orderId);

        if ($getOrderMerchant->status === 200) {
            $orderMerchant = $getOrderMerchant->data;
        ?>

            <div class="row row_profile row-invoice">
                <div class="head-back">
                    <a href="<?php echo site_url() . '/my-order'; ?>">
                        <i class="fa fa-angle-left"></i> <span class="back">Kembali </span>
                    </a>
                </div>
                <div class="logo">
                    <img src="<?php bloginfo('template_directory'); ?>/library/images/main_logo.png">
                </div>

                <div class="invoice-container">
                    <div class="row">
                        <div class="col-md-6">
                            <p><strong>Nomor Invoice</strong> : <span class="invoice-no"><?= $orderMerchant->no_invoice ?></span></p>
                            <p><strong>Tanggal</strong> : <?= $orderMerchant->created_at ?></p>
                            <p><strong>Metode Pembayaran</strong> : <?= $orderMerchant->payment_method; ?> <?= ($orderMerchant->payment_code !== '' ? "({$orderMerchant->payment_code})" : '') ?></p>
                            <p><strong>Jasa Pengiriman</strong> : <?= $orderMerchant->courier_name ?? '' ?> ( <?= $orderMerchant->courier_rate_name ?? '' ?> ) <?= ($orderMerchant->shipment_awb_number !== null ? "- {$orderMerchant->shipment_awb_number}" : '') ?></p>
                        </div>
                        <div class="col-md-6 txt-right mob_noright">
                            <button class="btn btn-primary invoice-download" data-id="<?= $orderId ?>">Unduh Invoice</button>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="invoice-box-addr">
                                <div class="col-md-6 invoice-border-right">
                                    <p><strong>Nama Penerima</strong> : <?= $orderMerchant->order_address->consignee ?? ''; ?></p>
                                    <p><strong>Nomor Telephone</strong> : <?= $orderMerchant->order_address->phone ?? ''; ?></p>
                                    <p>
                                        <strong>Tujuan Pengiriman</strong> :
                                    </p>
                                    <p>
                                        <?= $orderMerchant->order_address->address ?? ''; ?>
                                        <br>
                                        <?= $orderMerchant->order_address->suburb_name ?? ''; ?>, <?= $orderMerchant->order_address->city_name ?? ''; ?>, <?= $orderMerchant->order_address->province_name ?? ''; ?>, <?= $orderMerchant->order_address->postal_code ?? ''; ?>
                                    </p>
                                </div>
                                <div class="col-md-6">
                                    <p><strong>Nama Toko</strong> :<br />
                                        <?= $orderMerchant->merchant_name ?? ''; ?>
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row mt-3">
                        <div class="col-md-12">
                            <table class="invoice-table">
                                <thead>
                                    <tr>
                                        <th style="width: 50%;">Nama Produk</th>
                                        <th style="width: 10%;">Jumlah</th>
                                        <th style="width: 20%;">Harga Produk</th>
                                        <th style="width: 20%;">Sub Total</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                    $subtotalProductPrice = 0;

                                    foreach ($orderMerchant->order_items as $orderItem) {
                                        $productPrice = $orderItem->qty * $orderItem->price;
                                        $subtotalProductPrice += $productPrice;
                                    ?>

                                        <tr>
                                            <td>
                                                <?= $orderItem->product_name ?> <br />
                                                <?= $orderItem->ukuran ?>
                                            </td>
                                            <td><?= $orderItem->qty; ?></td>
                                            <td>Rp <?= number_format($orderItem->price); ?></td>
                                            <td>Rp <?= number_format($productPrice); ?></td>
                                        </tr>

                                    <?php
                                    }
                                    ?>
                                    <tr>
                                        <td colspan="2">
                                            <span style="text-align: right;display: block;font-weight: bold;">
                                                Subtotal Harga Produk
                                            </span>
                                        </td>
                                        <td></td>
                                        <td>
                                            Rp <?= number_format($subtotalProductPrice); ?>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div class="row mt-3">
                        <div class="col-md-12">
                            <table class="table_calinvoice">
                                <tr>
                                    <td>Subtotal untuk Produk</td>
                                    <td align="right">Rp <?= number_format($subtotalProductPrice) ?></td>
                                </tr>
                                <tr>
                                    <td>Biaya Pengiriman</td>
                                    <td align="right">Rp <?= number_format($orderMerchant->shipping_price); ?></td>
                                </tr>
                                <tr>
                                    <td>Biaya Asuransi Pengiriman</td>
                                    <td align="right">Rp <?= number_format($orderMerchant->insurance_price); ?></td>
                                </tr>
                                <tr>
                                    <td>Biaya Layanan</td>
                                    <td align="right">Rp <?= number_format($orderMerchant->admin_fee); ?></td>
                                </tr>
                                <?php
                                if ($orderMerchant->voucher_code !== null) :
                                ?>
                                    <tr>
                                        <td>Potongan Voucher <?= $orderMerchant->voucher_code ?></td>
                                        <td align="right">(Rp <?= number_format($orderMerchant->discount_voucher_price); ?>)</td>
                                    </tr>
                                <?php
                                endif;
                                ?>
                                <tr>
                                    <td style="border-top: 1px solid #dddddd;">
                                        <span class="f-bold">
                                            Total Pembayaran
                                        </span>
                                    </td>
                                    <td style="border-top: 1px solid #dddddd; text-align: right;">
                                        <span class="f-bold" style="color: #FF1E00;">
                                            Rp <?= number_format(($subtotalProductPrice + $orderMerchant->shipping_price + $orderMerchant->insurance_price + $orderMerchant->admin_fee - $orderMerchant->discount_voucher_price)); ?>
                                        </span>
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        <?php
        } else { ?>
            <div class="row row_finishCheckout">
                <div class="col-md-12 col_finishCheckout">
                    <a href="<?php echo home_url(); ?>">
                        <div class="bx_backShop">
                            <span class="glyphicon glyphicon-menu-left"></span> Kembali ke Home
                        </div>
                    </a>
                    <div class="bx_finishCheckout">
                        <div class="mg_registerIcon">
                            <img class="lazy" data-src="<?php bloginfo('template_directory'); ?>/library/images/ico_404.svg">
                        </div>
                        <div class="ht_register">Ups, halaman yang kamu cari belum bisa ditemukan :(</div>

                        <div class="ht_sucs_register">
                            Coba lagi dengan menekan tombol dibawah ini ya!
                        </div>
                        <div class="bx_def_checkout">
                            <a href="<?php echo home_url(); ?>/">
                                <button class="btn_def_checkout">Kembali ke Beranda</button>
                            </a>
                        </div>
                    </div>
                </div>
            </div>

        <?php } ?>

    <?php endwhile; ?>
<?php else : ?>
    <?php get_template_part('content', '404pages'); ?>
<?php endif; ?>
<?php get_footer(); ?>