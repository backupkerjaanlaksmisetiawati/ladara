<?php
/*
Template Name: admin kategori
*/
?>

<?php get_header('admin'); ?>

<?php if (have_posts()) : while (have_posts()) : the_post(); ?>

<style type="text/css">
.td_adm_kategori{
    margin: auto;
    text-align: center;
}
.mg_adm_kategori{
    width: 60px;
    height: 60px;
    margin: auto;
    text-align: center;
    padding: 5px;
    border: 1px solid rgba(0,0,0,.1);
}
.mg_adm_kategori img{
    width: auto;
    height: 100%;  
    margin: auto;
    text-align: center;
}
</style>

    <?php
    date_default_timezone_set("Asia/Jakarta");
    $nowdate = date('Y-m-d H:i:s');

    $current_user = wp_get_current_user();
    $u_id = $current_user->ID;
    $u_roles = $current_user->roles;
    $admin = 0;

    if(isset($u_roles) AND !empty($u_roles)){
        foreach ($u_roles as $key => $value) {
            if(strtolower($value) == 'administrator'){
                $admin = 1;
            }
            if(strtolower($value) == 'shop_manager'){
                $admin = 1;
            }
        }
    }

    if((isset($u_id) AND $u_id != 0) AND $admin == 1){

        ?>
        <style>
            .btn_ca_confirm:hover{
                color: #ffffff;
            }
        </style>
        <!-- Begin Page Content -->
        <div class="container-fluid">

            <!-- Page Heading -->
            <div class="row" style="margin-bottom: 2em;">
                <div class="col-md-6">
                    <h1 class="h3 mb-2 text-gray-800">List kategori</h1>
                </div>
                <div class="col-md-6">
                    <a href="admin-kategori/add-kategori" style="float: right;">
                        <input type="button" class="btn_gotoXendit" value="Tambah kategori" style="background: #0080FF;">
                    </a>
                </div>
            </div>

            <!-- DataTales Example -->
            <div class="card shadow mb-4">
                <div class="card-body">
                    <div class="table-responsive">
                        <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0" data-order="[[ 0, &quot;desc&quot; ]]">
                            <thead>
                            <tr>
                                <th>Gambar</th>
                                <th>Nama</th>
                                <th>Slug</th>
                                <th>Atur sebagai Utama</th>
                                <th>Action</th>
                            </tr>
                            </thead>
                            <tbody>
                                <?php
                                global $wpdb;
                                $query = "SELECT *
                                          FROM ldr_categories cate
                                          WHERE cate.deleted_at is null";
                                $res_query = $wpdb->get_results($query, OBJECT);

                                if (count($res_query) > 0 ){
                                    foreach ($res_query as $row){

                                        $url = IMAGE_URL;
                                        $url = str_replace(home_url(), '', $url);
                                        $url_path = $row->image;
                                        $url_fix = $url.'/'.$url_path;
                                        ?>
                                        <tr>
                                            <td class="td_adm_kategori">
                                                <div class="mg_adm_kategori">
                                                    <img class="" style="" src="<?php echo $url_fix; ?>" alt="">
                                                </div>
                                            </td>
                                            <td><?php echo $row->name; ?></td>
                                            <td><?php echo $row->slug; ?></td>
                                            <td><?php echo $row->featured == 1 ? 'Ya' : 'Tidak'; ?></td>
                                            <td>
                                                <a  class="btn_ca_confirm" href="/dashboard/admin-kategori/edit-kategori?id=<?php echo $row->id?>" style="padding: 5px 24px;font-size:12px;" title="Edit kategori">
                                                    Edit
                                                </a>

                                                <a class="btn_ca_cancel btn-hapus-cate" data-id="<?php echo $row->id?>" href="<?php echo MERCHANT_URL.'/api/categories'?>" style="padding: 2px 15px;font-size:12px;" title="Hapus kategori">
                                                    Hapus
                                                </a>
                                            </td>
                                        </tr>

                                <?php
                                    }
                                }
                                ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        <!-- /.container-fluid -->

    <?php }else{ ?>
        <script>
            // 'Getting' data-attributes using getAttribute
            var plant = document.getElementById('body');
            console.log(plant);
            var hurl = plant.getAttribute('data-hurl');
            location.replace(hurl+'/login/');
        </script>
    <?php } ?>

<?php endwhile; ?>
<?php else : ?>
    <?php get_template_part( 'content', '404pages' ); ?>
<?php endif; ?>
<?php get_footer('admin'); ?>