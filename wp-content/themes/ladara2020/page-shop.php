<?php
/*
Template Name: Shop Page
*/
?>

<?php get_header(); ?>
<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
<style type="text/css">
	.header{
		height: 135px;
	}
	.header_list_tops{
		display: none!important;
	}
</style>
<?php 
date_default_timezone_set("Asia/Jakarta");
$nowdate = date('Y-m-d');
$now_time = strtotime($nowdate);

$current_user = wp_get_current_user();
$u_id = $current_user->ID;

if(isset($_GET['keyword'])){
    $keyword = htmlentities($_GET['keyword'], ENT_QUOTES, 'UTF-8');
}else{
	$keyword = '';
}

$offset = 0;
$limit_item = 30;
if(isset($_GET['pg'])){
	$page = $_GET['pg'];
	$page_now = (int)$page-1;
	$offset = (int)$page_now*(int)$limit_item;
}else{
	$page = 1;
	$offset = (int)$page*0;
}

if(isset($_GET['category'])){
	$category = $_GET['category'];
	$tx_category = str_replace('-', ' ', $category);
	$_SESSION['category'] = $category;
}else{
	$category = '';
	$tx_category = 'Unggulan';
}

// if(isset($_SESSION['category'])){
// 	$category = $_SESSION['category'];
// 	$tx_category = str_replace('-', ' ', $category);
// }

if(isset($_GET['by'])){
	$by = $_GET['by'];
	$_SESSION['by'] = $by;
}else{
	$by = 'terkait';
}

if(isset($_SESSION['by'])){
	$by = $_SESSION['by'];
}

global $wp;
$cur_page = home_url( $wp->request );

?>
<div class="row"></div>

<input type="hidden" name="keyword_s" value="<?php echo $keyword; ?>">
<input type="hidden" name="offset" value="<?php echo $offset; ?>">
<input type="hidden" name="limit" value="<?php echo $limit_item; ?>">
<input type="hidden" name="pagination" value="<?php echo $page; ?>">
<input type="hidden" name="category" value="<?php echo $category; ?>">
<input type="hidden" name="page_url" value="<?php echo $cur_page; ?>">

<div id="shop_product" class="row row_masterPage" data-url="<?php echo home_url(); ?>">
	<div class="col-md-12 col_shop_category">
		
		<?php /* if(isset($category) AND $category != ''){ ?>
			<div class="bx_shop_category">
				<a href="<?php echo home_url(); ?>/shop/"><span class="cate">Shop</span></a>
				<span class="arrow"><img src="<?php bloginfo('template_directory'); ?>/library/images/next_shop.svg"></span>
				<span class="cate"><?php echo ucfirst($tx_category); ?></span>
			</div>
		<?php } */ ?>

	</div>
	<div class="col-md-3 col_shop_filter">
		
		<?php get_template_part( 'content', 'filter-shop' ); ?>

	</div>
	<div class="col-md-9 col_product_shoplist">

		<div class="box_head_shoplist">
	
			<?php if(isset($keyword) AND $keyword != ''){ ?>
				<div class="ht_head_shoplist">
					<span class="glyphicon glyphicon-eye-open"></span> Hasil pencarian untuk <b>'<?php echo ucfirst($keyword); ?>'</b>
				</div>
			<?php }else{

					if($category != ''){
					?>
						<h2 class="ht_head_shoplist">
							<?php
                            $exCate = explode(' ', $tx_category);
                            array_pop($exCate);
                            echo ucfirst(implode(' ', $exCate));
                            ?>
						</h2>
					<?php
					}else{
					?>
						<h2 class="ht_head_shoplist">
							Produk Terbaru
						</h2>
					<?php
					}
			 ?>
				
			<?php } ?>	


			<?php 
				$sortby = array();
				$sortby['terkait'] = 'Paling Sesuai';
				$sortby['asc'] = 'Produk A-Z';
				$sortby['desc'] = 'Produk Z-A';
				$sortby['termurah'] = 'Termurah';
			?>
			<div class="ht_head_sortby pull_right">
				<div class="label">Urutkan Berdasarkan </div>
				<select name="filter_sortby" class="sel_sortby">
					<?php 
						foreach ($sortby as $key => $value) { 
							if($key == $by){
							?>
								<option value="<?php echo $key; ?>" selected="selected"><?php echo $value; ?></option>
							<?php
							}else{
							?>
								<option value="<?php echo $key; ?>"><?php echo $value; ?></option>
							<?php
							}
				 		} 
				 	?>
				</select>
			</div>

			<div class="bx_mob_sortby mob_display">
				<div class="bx_mob_insortby">
					<div class="ht_mob_sortby">Urutkan Berdasarkan:</div>
					<ul class="ul_mob_sortby">
						<?php 
							foreach ($sortby as $key => $value) {  
								if($key == $by){
								?>
									<li class="act" data-val="<?php echo $key; ?>"><a><?php echo $value; ?></a></li>
								<?php
								}else{
								?>
									<li data-val="<?php echo $key; ?>"><a><?php echo $value; ?></a></li>
								<?php
								}
							} 
						?>
					</ul>

					<input type="button" class="sub_subfilter close_mob_sortby" value="Tutup">
				</div>
			</div>

		</div>

		<div id="view_resProduct" class="row row_resProduct">

		</div>

		<div class="row">
			<div class="col-md-12 col_pagination_shop view_pagi_shops">
				<?php // view pagi shops my ajax ?>
			</div>
		</div>
		
	</div>
</div>



<?php endwhile; ?>
<?php else : ?>
		<?php get_template_part( 'content', '404pages' ); ?>	
<?php endif; ?>
<?php get_footer(); ?>