<?php
/*
Template Name: Payment Thankyou Page
*/

use Xendit\EWallets;
use Xendit\Invoice;
use Xendit\Retail;

?>

<?php get_header(); ?>
<?php if (have_posts()) :
    while (have_posts()) :
        the_post();

        global $wpdb;
        $current_user = wp_get_current_user();
        $u_id = $current_user->ID;

        if (isset($_GET['order'])) {
            $order_id = $_GET['order'];
        } else {
            $order_id = isset($_SESSION['order_id']) ? $_SESSION['order_id'] : 0;
        }

        if ($order_id !== '') {
            $order_id = decryptString($order_id);
        }

        $status = '';
        $valid = '';

        if (intval($order_id) !== 0) {
            $order = $wpdb->get_row("SELECT * FROM ldr_orders WHERE id = $order_id AND deleted_at IS NULL");
            $paymentLog = $wpdb->get_row("SELECT * FROM ldr_payment_log WHERE order_id = $order_id");
            $xenditId = $order->xendit_id;
            $paymentMethod = $paymentLog->payment_method;
            $selectedBank = $paymentLog->bank_code;

            if ($paymentMethod === 'cc' || $paymentMethod === 'va') {
                $getInvoice = Invoice::retrieve($xenditId);

                if (isset($getInvoice) and !empty($getInvoice)) {
                    $status = $getInvoice['status'];
                }
            } else if ($paymentMethod === 'ewallet') {
                $getInvoice = EWallets::getPaymentStatus($xenditId, $selectedBank);

                if (isset($getInvoice) and !empty($getInvoice)) {
                    $status = $getInvoice['status'];
                }
            } else if ($paymentMethod === 'retail') {
                $getInvoice = Retail::retrieve($xenditId);

                if (isset($getInvoice) and !empty($getInvoice)) {
                    if ($getInvoice['status'] == 'ACTIVE') {
                        $status = 'PENDING';
                    } else {
                        if ($paymentLog->paid_at !== null) {
                            $status = 'COMPLETED';
                        }
                    }
                }
            }

            if (strtolower($status) == 'paid' || strtolower($status) == 'settled' || strtolower($status) === 'completed') {
?>
                <div class="row row_finishCheckout">
                    <div class="col-md-12 col_finishCheckout">
                        <a href="<?php echo home_url(); ?>">
                            <div class="bx_backShop">
                                <span class="glyphicon glyphicon-menu-left"></span> Kembali ke Home
                            </div>
                        </a>
                        <div class="bx_finishCheckout">
                            <div class="mg_registerIcon">
                                <img class="lazy" data-src="<?php bloginfo('template_directory'); ?>/library/images/ico_successpay.jpg">
                            </div>
                            <div class="ht_register">Pembayaran Anda Telah Berhasil !</div>

                            <div class="ht_sucs_register">
                                <b>Terima kasih telah melakukan pembayaran.</b> Resi pembayaran akan dikirim ke email anda.
                            </div>
                            <div class="bx_def_checkout">
                                <a href="<?php echo home_url(); ?>/shop/">
                                    <button class="btn_def_checkout">Belanja Lagi</button>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
                <?php
            } elseif (strtolower($status) == 'pending') {
                if ($paymentMethod === 'va') {
                    $availableBanks = array_values(array_filter($getInvoice['available_banks'], function ($fn) {
                        return $fn['collection_type'] === 'CALLBACK_VIRTUAL_ACCOUNT';
                    }));

                    if (isset($availableBanks) and !empty($availableBanks)) {
                        foreach ($availableBanks as $key => $value) {
                            $bank_account_number = $value['bank_account_number'];
                            $selectedBank = $value['bank_code'];

                            if ($selectedBank === 'MANDIRI') {
                                $urlInstructions = "https://invoice.xendit.co/static/locales/id/mandiri_instructions.json";
                            } elseif ($selectedBank === 'BCA') {
                                $urlInstructions = "https://invoice.xendit.co/static/locales/id/bca_instructions.json";
                            } elseif ($selectedBank === 'PERMATA') {
                                $urlInstructions = "https://invoice.xendit.co/static/locales/id/permata_instructions.json";
                            } elseif ($selectedBank === 'BRI') {
                                $urlInstructions = "https://invoice.xendit.co/static/locales/id/bri_instructions.json";
                            } else {
                                $urlInstructions = "https://invoice.xendit.co/static/locales/id/bni_instructions.json";
                            }

                            $ch = curl_init();
                            curl_setopt($ch, CURLOPT_URL, $urlInstructions);
                            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
                            $result = curl_exec($ch);
                            curl_close($ch);

                            $instructions = json_decode($result, true);
                            $resultInstrunctions = [];

                            foreach ($instructions as $key => $instruction) {
                                $method = strtoupper($key);

                                if (strpos($method, "ATM") !== false || strpos($method, "SMS") !== false) {
                                    $method = substr_replace($method, " ", 3, 0);
                                } elseif (strpos($method, "MOBILEBANKING") !== false) {
                                    $method = substr_replace($method, " ", 6, 0);
                                    $method = substr_replace($method, " ", 14, 0);
                                } elseif (strpos($method, "IBANKPERSONAL") !== false) {
                                    $method = substr_replace($method, " ", 5, 0);
                                    $method = substr_replace($method, " ", 14, 0);
                                } elseif (strpos($method, "TELLER") !== false) {
                                    $method = substr_replace($method, " ", 6, 0);
                                } elseif (strpos($method, "OTHER") !== false) {
                                    $method = 'BANK LAIN';
                                } elseif (strpos($method, "PERMATAMOBILE") !== false) {
                                    $method = substr_replace($method, " ", 7, 0);
                                    $method = substr_replace($method, " ", 14, 0);
                                    $method = trim($method);
                                } elseif (strpos($method, "INTERNETBANKING") !== false) {
                                    $method = substr_replace($method, " ", 8, 0);
                                    $method = substr_replace($method, " ", 16, 0);
                                }

                                $resultInstrunctions[$method] = [];

                                foreach ($instruction as $step) {
                                    $fixedStep = str_replace('{{companyCode}}', '88908', $step);
                                    $fixedStep = str_replace('{{companyName}}', '88908 XENDIT', $fixedStep);
                                    $fixedStep = str_replace('{{- vaNumber}}', $bank_account_number, $fixedStep);
                                    $resultInstrunctions[$method][] = $fixedStep;
                                }
                            } ?>

                            <div class="row row_finishCheckout">
                                <div class="col-md-12 col_finishCheckout">
                                    <a href="<?php echo home_url(); ?>">
                                        <div class="bx_backShop">
                                            <span class="glyphicon glyphicon-menu-left"></span> Kembali ke Home
                                        </div>
                                    </a>
                                    <div class="bx_finishCheckout">
                                        <div class="mg_registerIcon">
                                            <img class="lazy" data-src="<?php bloginfo('template_directory'); ?>/library/images/ico_onhold.jpg">
                                        </div>
                                        <div class="ht_register">Kami menunggu pembayaran Anda</div>

                                        <div class="ht_sucs_register">
                                            Silahkan lakukan pembayaran dengan memilih salah satu cara pembayaran berikut :
                                        </div>

                                        <div class="box_cont_listva">

                                            <div class="ht_va_account">Kode Virtual Account : <span><?php echo $bank_account_number; ?></span></div>

                                            <?php
                                            $cpt = 0;
                                            foreach ($resultInstrunctions as $key_name => $value_list) {
                                                $cpt++; ?>

                                                <div class="fd_listva">
                                                    <div class="ht_listva open_listva" data-id="listva_<?php echo $cpt; ?>" title="Lihat detail informasi ini."><?php echo $key_name; ?> <span class="glyphicon glyphicon-menu-down"></span></div>
                                                    <ol id="listva_<?php echo $cpt; ?>" class="ul_listva animated">
                                                        <?php foreach ($value_list as $key => $value) { ?>
                                                            <li><?php echo $value['step']; ?></li>
                                                        <?php } ?>

                                                    </ol>
                                                </div>

                                            <?php
                                            } ?>

                                        </div>

                                        <div class="bx_def_checkout">
                                            <input type="text" value="<?php echo $bank_account_number; ?>" id="copy_va" style="display: none;">
                                            <button type="button" onclick="CopyToClipboard('copy_va')" class="btn_def_checkout" style="outline: none;">Salin Kode VA</button>
                                        </div>

                                        <div class="bx_def_checkout" style="margin-top: 20px;">
                                            <a href="<?php echo home_url(); ?>/shop/">
                                                <button class="btn_def_checkout">Belanja Lagi</button>
                                            </a>
                                        </div>

                                    </div>
                                </div>
                            </div>

                    <?php
                        }
                    }
                } else if ($paymentMethod === 'retail') {
                    $urlInstructions = "https://invoice.xendit.co/static/locales/id/indomart_instructions.json";

                    if ($selectedBank === 'ALFAMART') {
                        $urlInstructions = "https://invoice.xendit.co/static/locales/id/alfamart_instructions.json";
                    }

                    $ch = curl_init();
                    curl_setopt($ch, CURLOPT_URL, $urlInstructions);
                    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
                    $result = curl_exec($ch);
                    curl_close($ch);

                    $instructions = json_decode($result, true);
                    $resultInstrunctions = [];

                    foreach ($instructions as $key => $instruction) {
                        $method = 'Gerai ' . ucwords(strtolower($selectedBank));

                        foreach ($instruction as $step) {
                            $fixedStep = str_replace('{{merchantName}}', 'XENDIT', $step);
                            $resultInstrunctions[$method][] = $fixedStep;
                        }
                    }
                    ?>

                    <div class="row row_finishCheckout">
                        <div class="col-md-12 col_finishCheckout">
                            <a href="<?php echo home_url(); ?>">
                                <div class="bx_backShop">
                                    <span class="glyphicon glyphicon-menu-left"></span> Kembali ke Home
                                </div>
                            </a>
                            <div class="bx_finishCheckout">
                                <div class="mg_registerIcon">
                                    <img class="lazy" data-src="<?php bloginfo('template_directory'); ?>/library/images/ico_onhold.jpg">
                                </div>
                                <div class="ht_register">Kami menunggu pembayaran Anda</div>

                                <div class="ht_sucs_register">
                                    Silahkan lakukan pembayaran dengan memilih salah satu cara pembayaran berikut :
                                </div>

                                <div class="box_cont_listva">

                                    <div class="ht_va_account">Kode Pembayaran : <span><?php echo $getInvoice['payment_code']; ?></span></div>

                                    <?php
                                    foreach ($resultInstrunctions as $key_name => $value_list) {
                                    ?>

                                        <div class="fd_listva">
                                            <div class="ht_listva open_listva" data-id="listva_0" title="Lihat detail informasi ini."><?php echo $key_name; ?> <span class="glyphicon glyphicon-menu-down"></span></div>
                                            <ol id="listva_0" class="ul_listva animated">
                                                <?php foreach ($value_list as $key => $value) { ?>
                                                    <li><?php echo $value['step']; ?></li>
                                                <?php } ?>
                                            </ol>
                                        </div>

                                    <?php
                                    } ?>

                                </div>
                                <div class="bx_def_checkout" style="margin-top: 20px;">
                                    <a href="<?php echo home_url(); ?>/shop/">
                                        <button class="btn_def_checkout">Belanja Lagi</button>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>

                <?php
                } else {
                ?>
                    <div class="row row_finishCheckout">
                        <div class="col-md-12 col_finishCheckout">
                            <a href="<?php echo home_url(); ?>">
                                <div class="bx_backShop">
                                    <span class="glyphicon glyphicon-menu-left"></span> Kembali ke Home
                                </div>
                            </a>
                            <div class="bx_finishCheckout">
                                <div class="mg_registerIcon">
                                    <img class="lazy" data-src="<?php bloginfo('template_directory'); ?>/library/images/ico_onhold.jpg">
                                </div>
                                <div class="ht_register">Pembayaran Anda Menunggu Konfirmasi</div>

                                <div class="ht_sucs_register">
                                    Tenang, kami sedang memkonfirmasi pembayaran anda. Pembayaran Anda akan segera diproses.
                                </div>
                                <div class="bx_def_checkout">
                                    <a href="<?php echo home_url(); ?>/shop/">
                                        <button class="btn_def_checkout">Belanja Lagi</button>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                <?php
                }
            } else {
                ?>
                <div class="row row_finishCheckout">
                    <div class="col-md-12 col_finishCheckout">
                        <a href="<?php echo home_url(); ?>">
                            <div class="bx_backShop">
                                <span class="glyphicon glyphicon-menu-left"></span> Kembali ke Home
                            </div>
                        </a>
                        <div class="bx_finishCheckout">
                            <div class="mg_registerIcon">
                                <img class="lazy" data-src="<?php bloginfo('template_directory'); ?>/library/images/ico_cancelled.jpg">
                            </div>
                            <div class="ht_register">Pembayaran Anda Gagal</div>

                            <div class="ht_sucs_register">
                                Maaf transaksi pembayaran Anda gagal dikonfirmasi. Silahkan melakukan pembayaran sesuai petunjuk.
                            </div>
                            <div class="bx_def_checkout">
                                <a href="<?php echo home_url(); ?>/shop/">
                                    <button class="btn_def_checkout">Belanja Lagi</button>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            <?php
            }
        } else {
            ?>
            <div class="row row_finishCheckout">
                <div class="col-md-12 col_finishCheckout">
                    <a href="<?php echo home_url(); ?>">
                        <div class="bx_backShop">
                            <span class="glyphicon glyphicon-menu-left"></span> Kembali ke Home
                        </div>
                    </a>
                    <div class="bx_finishCheckout">
                        <div class="mg_registerIcon">
                            <img class="lazy" data-src="<?php bloginfo('template_directory'); ?>/library/images/ico_cartempty.jpg">
                        </div>
                        <div class="ht_register">Duh kok kosong? Tas belanjamu bisa masuk angin nih</div>

                        <div class="ht_sucs_register">
                            Yuk isi dengan produk terbaru atau produk yang sudah kamu mimpikan dari kemarin.
                        </div>
                        <div class="bx_def_checkout">
                            <a href="<?php echo home_url(); ?>/shop/">
                                <button class="btn_def_checkout">Belanja Lagi</button>
                            </a>
                        </div>
                    </div>
                </div>
            </div>

        <?php
        }
        ?>

        <?php // ============= cannot back =============
        ?>
        <script type="text/javascript">
            (function(global) {

                if (typeof(global) === "undefined") {
                    throw new Error("window is undefined");
                }

                var _hash = "!";
                var noBackPlease = function() {
                    global.location.href += "#";

                    // making sure we have the fruit available for juice (^__^)
                    global.setTimeout(function() {
                        global.location.href += "!";
                    }, 50);
                };

                global.onhashchange = function() {
                    if (global.location.hash !== _hash) {
                        global.location.hash = _hash;
                    }
                };

                global.onload = function() {
                    noBackPlease();

                    // disables backspace on page except on input fields and textarea..
                    document.body.onkeydown = function(e) {
                        var elm = e.target.nodeName.toLowerCase();
                        if (e.which === 8 && (elm !== 'input' && elm !== 'textarea')) {
                            e.preventDefault();
                        }
                        // stopping event bubbling up the DOM tree..
                        e.stopPropagation();
                    };
                }

            })(window);

            function CopyToClipboard(containerid) {
                if (document.selection) {
                    var range = document.body.createTextRange();
                    range.moveToElementText(document.getElementById(containerid));
                    range.select().createTextRange();
                    document.execCommand("Copy");
                } else if (window.getSelection) {
                    var range = document.createRange();
                    document.getElementById(containerid).style.display = "block";
                    range.selectNode(document.getElementById(containerid));
                    window.getSelection().addRange(range);
                    document.execCommand("Copy");
                    document.getElementById(containerid).style.display = "none";
                }
                
                alert('Kode VA berhasil disalin.')
            }
        </script>

    <?php endwhile; ?>
<?php else : ?>
    <?php get_template_part('content', '404pages'); ?>
<?php endif; ?>
<?php get_footer(); ?>