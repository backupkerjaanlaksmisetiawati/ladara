<?php get_header(); ?>
<?php if (have_posts()) : while (have_posts()) : the_post(); ?>

<?php 
date_default_timezone_set("Asia/Jakarta");
$nowdate = date('Y-m-d');
$now_time = strtotime($nowdate);

$current_user = wp_get_current_user();
$u_id = $current_user->ID;
$mile_reward = 0;
$mile_reward = get_field('mile_reward', 'user_'.$u_id);

$id_coupon = $post->ID;
$title_coupon = get_the_title($id_coupon);

$id_bigPhoto = get_field('big_coupon',$id_coupon);
$code_coupon = get_field('code_coupon',$id_coupon);
$mile_coupon = get_field('mile_coupon',$id_coupon);
$start_coupon = get_field('start_coupon',$id_coupon);
$end_coupon = get_field('end_coupon',$id_coupon);

$expired_date = date('d M Y',strtotime($end_coupon));

$total_coupon = get_field('total_coupon',$id_coupon);
$mini_info_coupon = get_field('mini_info_coupon',$id_coupon);

$allcategory = array();
$category = get_the_terms( $id_coupon, 'custom_cat' );

$thumb = wp_get_attachment_image_src( $id_bigPhoto, 'full' );
if($thumb){
	$url_photo = $thumb['0'];
}else{
	$url_photo= get_template_directory_uri().'/library/images/normal-back.jpg';
}
$alt = get_post_meta($id_bigPhoto, '_wp_attachment_image_alt', true);
if(count($alt));

$start_coupon = get_field('start_coupon',$id_coupon);
$start_textdate = date("d-m-Y", strtotime($start_coupon));
$str_startdate = strtotime( date("Y-m-d", strtotime($start_coupon)) );

$end_coupon = get_field('end_coupon',$id_coupon);
$str_enddate = strtotime( date("Y-m-d", strtotime($end_coupon)) );


if(isset($u_id) AND $u_id != 0 AND $u_id != ''){ // check only for login user

$voc_valid = 0;
global $wpdb;
$wpdb_query = "SELECT * FROM mlm_voucher WHERE user_id = '$u_id' AND voucher_id = '$id_coupon'";
$res_query = $wpdb->get_results($wpdb_query, OBJECT);
$count_res = count($res_query);

if($count_res > 0){ // if not exist with same user_id
	$voc_valid = 1;
}

?>
<input type="hidden" name="u_id" value="<?php echo $u_id; ?>">
<input type="hidden" name="vou_id" value="<?php echo $id_coupon; ?>">

<article id="post-<?php the_ID(); ?>" <?php post_class('clearfix'); ?> role="article" itemscope itemtype="http://schema.org/BlogPosting">

<?php // code here for single page ?>
	
<div class="row row_couponpage">
	<div class="col-md-9 col_couponpage">

		<div class="ht_proCategory"> 
				<a href="<?php echo home_url(); ?>/mile-reward/" class="a_proCategory"><i class="fab fa-fort-awesome"></i></a>
				<?php 
					foreach ($category as $term => $value) {
						$product_cat_id = $value->term_id;
						$product_cat_slug = $value->slug;
						$product_cat_name = $value->name;
						array_push($allcategory, $product_cat_id);

				?>
						<span><i class="fas fa-chevron-right"></i></span>
						<a href="#" class="a_proCategory"><?php echo $product_cat_name; ?></a>
				<?php
					}
				 ?>
				<span><i class="fas fa-chevron-right"></i></span>
				<a class="a_proCategory"><?php echo get_the_title($post->ID); ?></a>
		</div>	

		<div class="mg_couponpage">
			<img src="<?php echo $url_photo; ?>" alt="<?php echo $alt; ?>">
		</div>

		<h1 class="ht_couponpage"><?php echo $title_coupon; ?></h1>

		<div class="cont_coupon"><?php the_content(); ?></div>


	</div>
	<div class="col-md-3 col_couponpage">
		
		<div class="box_r_couponpage">
			<h2 class="ht_detailcoupon">Detail Voucher</h2>
			<div class="cont_detailcoupon"><?php echo $mini_info_coupon; ?></div>

			<!-- <div class="box_valcoupon">Code : <span><?php // echo $code_coupon; ?></span></div> -->
			<?php if($now_time <= $str_startdate){ ?>
				<input id="" type="button" class="btn_buycoupon" value="Segera : <?php echo $start_textdate; ?>" disabled="disabled">
			<?php }else{ ?>
				<?php if($voc_valid == 1){ ?>
					<input id="" type="button" class="btn_buycoupon" value="Sudah di Ambil" disabled="disabled">
				<?php }else{ ?>
					<?php if($mile_reward > $mile_coupon){ ?>
						<input id="ambil_voucher" type="button" class="btn_buycoupon" value="Ambil Voucher ( <?php echo $mile_coupon; ?> mile )">
					<?php }else if($mile_coupon == 0){ ?>
						<input id="ambil_voucher_gratis" type="button" class="btn_buycoupon" value="Ambil Voucher GRATIS">
					<?php }else{ ?>
						<input id="" type="button" class="btn_buycoupon" value="Mile Anda Tidak Cukup" disabled="disabled">
					<?php } ?>
				<?php } ?>
			<?php } ?>


			<div class="info_mymiles">Reward Anda : <?php echo number_format($mile_reward); ?> mile</div>
			<div class="box_valcoupon">Berlaku sampai <b><?php echo $expired_date; ?></b></div>

		</div>

	</div>


	<div class="col-md-12 col_couponpage2">
		
		<h3 class="pt_relatedcoupon">Voucher Lainnya</h3>

		<div class="row">
				<?php 
					$args = array(
					    'post_type' => 'couponlist',
					    'posts_per_page' => 4,
					    'post_status' => 'publish',
					    'orderby' => 'rand',
					    'order' => 'DESC',
					    'post__not_in' => array($post->ID)
					);
					$the_query = new WP_Query($args);
					while ($the_query->have_posts()) : $the_query->the_post();
					$voucher_id = get_the_ID();
					$voucher_name = get_the_title($voucher_id);
					$short_name = get_the_title($voucher_id);
					if(strlen($short_name) > 45) $short_name = substr($short_name, 0, 45).'...';
					$voucher_link = get_the_permalink($voucher_id);
					$mile_coupon = get_field('mile_coupon',$voucher_id);
					$thumb = wp_get_attachment_image_src( get_post_thumbnail_id($voucher_id), 'medium' );
					if($thumb){
						$urlphoto = $thumb['0'];
					}else{
						$urlphoto = get_template_directory_uri().'/library/images/sorry.png';
					}
					$alt = get_post_meta(get_post_thumbnail_id($voucher_id), '_wp_attachment_image_alt', true);
					if(count($alt));

				?>
						<div class="col-md-3 col_list_voc">
							<a href="<?php echo $voucher_link; ?>" title="Lihat voucher ini.">
								<div class="box_list_voc">
									<div class="mg_list_voc">
										<img src="<?php echo $urlphoto; ?>" alt="<?php echo $alt; ?>">
									</div>
									<h4 class="ht_list_voc"><?php echo $voucher_name; ?></h4>
									<div class="mg_coinreward">
										<?php if(isset($mile_coupon) AND $mile_coupon == 0){ ?>
											<img src="<?php bloginfo('template_directory'); ?>/library/images/mile_reward.png"> <span><b>GRATIS</b></span>
										<?php }else{ ?>
											<img src="<?php bloginfo('template_directory'); ?>/library/images/mile_reward.png"> <span><?php echo $mile_coupon; ?> mile points</span>
										<?php } ?>
									</div>
									<span class="span_ambilvoucher">Ambil</span>
								</div>
							</a>
						</div>
			<?php 
				endwhile;
				wp_reset_query();
			?>
		</div>


		<div class="bx_backCoupon">
			<a href="<?php echo home_url(); ?>/mile-reward/">Kembali ke Halaman Mile Rewards</a>
		</div>

	</div>

</div>

<?php }else{ ?>
	<div id="redirect_now" data-hurl="<?php echo home_url(); ?>/login/"></div>
	<script>
	    // 'Getting' data-attributes using getAttribute
	    var plant = document.getElementById('redirect_now');
	    console.log(plant);
	    var hurl = plant.getAttribute('data-hurl'); 
	    location.replace(hurl); 
	</script>
<?php } ?>

</article>
<?php endwhile; ?>
<?php else : ?>
<article id="post-not-found" class="hentry clearfix">
		<header class="article-header">
			<h1><?php _e( 'Oops, Post Not Found!', 'bonestheme' ); ?></h1>
		</header>
		<section class="entry-content">
			<p><?php _e( 'Uh Oh. Something is missing. Try double checking things.', 'bonestheme' ); ?></p>
		</section>
		<footer class="article-footer">
				<p><?php _e( 'This is the error message in the single.php template.', 'bonestheme' ); ?></p>
		</footer>
</article>
<?php endif; ?>
<?php get_footer(); ?>