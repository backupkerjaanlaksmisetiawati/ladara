<?php get_header(); ?>
<?php if (have_posts()) :
    while (have_posts()) :
        the_post();
?>
        <article id="post-<?php the_ID(); ?>" <?php post_class('clearfix'); ?> role="article" itemscope itemtype="http://schema.org/BlogPosting">

            <style type="text/css">
                .header {
                    height: 135px;
                }

                .header_list_tops {
                    display: none !important;
                }
            </style>
            <?php
            date_default_timezone_set("Asia/Jakarta");
            $nowdate = date('Y-m-d');
            $now_time = strtotime($nowdate);

            $current_user = wp_get_current_user();
            $u_id = $current_user->ID;

            if (isset($_GET['keyword'])) {
                $keyword = $_GET['keyword'];
            } else {
                $keyword = '';
            }

            $promo_name = get_the_title($post->ID);

            $linkpoto = wp_get_attachment_image_src(get_post_thumbnail_id($post->ID), 'full');
            if ($linkpoto) {
                $urlphoto = $linkpoto['0'];
            } else {
                $urlphoto = get_template_directory_uri() . '/library/images/default_banner.jpg';
            }

            ?>
            <div id="" class="row row_masterPage" data-url="<?php echo home_url(); ?>">
                <div class="col-md-12 col_shop_category">

                    <div class="bx_shop_category">
                        <a href="<?php echo home_url(); ?>"><span class="cate">Home</span></a>
                        <?php /*
            <span class="arrow"><img src="<?php bloginfo('template_directory'); ?>/library/images/next_shop.svg"></span>
            <a href="#segera"><span class="cate">Promo</span></a>
            */ ?>
                        <span class="arrow"><img src="<?php bloginfo('template_directory'); ?>/library/images/next_shop.svg"></span>
                        <span class="cate"><?php echo $promo_name; ?></span>
                    </div>

                </div>
                <div class="col-md-12 col_product_shoplist">

                    <div class="mg_head_Banner">
                        <img src="<?php echo $urlphoto; ?>" alt="Promo <?php echo $promo_name; ?>">
                    </div>

                    <div class="line_contbanner"></div>

                    <h1 class="ht_banner_cont"><?php echo $promo_name; ?></h1>

                    <div class="content_banner">
                        <?php the_content(); ?>
                    </div>

                </div>
                <?php
                $linkBelanjaSekarang = get_post_meta($post->ID, 'link-belanja-sekarang', true);

                if ($linkBelanjaSekarang !== null && $linkBelanjaSekarang !== '') {
                ?>
                    <div class="col-md-12" align="right" style="margin-top: 15px;">
                        <a href="<?php echo $linkBelanjaSekarang; ?>">
                            <input type="button" class="sub_buyProduct sub_addtoCart" value="Belanja Sekarang">
                        </a>
                    </div>
                <?php
                }
                ?>
            </div>
        </article>
    <?php endwhile; ?>
<?php else : ?>
    <article id="post-not-found" class="hentry clearfix">
        <header class="article-header">
            <h1><?php _e('Oops, Post Not Found!', 'bonestheme'); ?></h1>
        </header>
        <section class="entry-content">
            <p><?php _e('Uh Oh. Something is missing. Try double checking things.', 'bonestheme'); ?></p>
        </section>
        <footer class="article-footer">
            <p><?php _e('This is the error message in the single.php template.', 'bonestheme'); ?></p>
        </footer>
    </article>
<?php endif; ?>
<?php get_footer(); ?>