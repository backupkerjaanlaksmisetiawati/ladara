<?php get_header(); ?>
<?php if (have_posts()) : 
while (have_posts()) : 
the_post(); 
?>
<article id="post-<?php the_ID(); ?>" <?php post_class('clearfix'); ?> role="article" itemscope itemtype="http://schema.org/BlogPosting">

<style type="text/css">
    .header {
        height: 135px;
    }

    .header_list_tops {
        display: none !important;
    }
    .row_masterPage{
        padding-bottom: 10px;
    }
    .row_resProduct2{
        padding-left: 0;
        padding-right: 0;
    }
</style>

    <?php
    date_default_timezone_set("Asia/Jakarta");
    $nowdate = date('Y-m-d');
    $now_time = strtotime($nowdate);

    $current_user = wp_get_current_user();
    $u_id = $current_user->ID;

    if (isset($_GET['keyword'])) {
        $keyword = $_GET['keyword'];
    } else {
        $keyword = '';
    }

    $promo_name = get_the_title($post->ID);

    $offset = 0;
    $limit_item = 12;
    if (isset($_GET['pg'])) {
        $page = $_GET['pg'];
        $page_now = (int)$page - 1;
        $offset = (int)$page_now * (int)$limit_item;
    } else {
        $page = 1;
        $offset = (int)$page * 0;
    }

    global $wp;
    $cur_page = home_url($wp->request);

    $linkpoto = wp_get_attachment_image_src(get_post_thumbnail_id($post->ID), 'full');
    if ($linkpoto) {
        $urlphoto = $linkpoto['0'];
    } else {
        $urlphoto = get_template_directory_uri() . '/library/images/default_banner.jpg';
    }

    ?>

    <input type="hidden" name="promo_id" value="<?php echo $post->ID; ?>">
    <input type="hidden" name="keyword_s" value="<?php echo $keyword; ?>">
    <input type="hidden" name="offset" value="<?php echo $offset; ?>">
    <input type="hidden" name="limit" value="<?php echo $limit_item; ?>">
    <input type="hidden" name="pagination" value="<?php echo $page; ?>">
    <input type="hidden" name="page_url" value="<?php echo $cur_page; ?>">

    <div id="shop_promo" class="row row_masterPage" data-url="<?php echo home_url(); ?>">
        <div class="col-md-12 col_shop_category">

        </div>
        <div class="col-md-12 col_product_shoplist">
            <h1 style="display: none;">Produk <?php echo get_the_title($post->ID); ?></h1>
            
            <div class="mg_head_promoBanner">
                <img src="<?php echo $urlphoto; ?>" alt="Promo <?php echo $promo_name; ?>">
            </div>

            <div id="view_resProduct" class="row row_resProduct2">

            </div>

            <div class="row">
                <div class="col-md-12 col_pagination_shop view_pagi_shops">
                    <?php // view pagi shops my ajax
                    ?>
                </div>
            </div>

        </div>


        <div class="bx_cont_featured">
            <h1 class="ht_cont_featured">Product <?php echo get_the_title($post->ID); ?></h1>
            <div class="box_content_featured"> 
                    <?php the_content(); ?>
            </div>
        </div>
    </div>

</article>
<?php endwhile; ?>
<?php else : ?>
    <article id="post-not-found" class="hentry clearfix">
        <header class="article-header">
            <h1><?php _e('Oops, Post Not Found!', 'bonestheme'); ?></h1>
        </header>
        <section class="entry-content">
            <p><?php _e('Uh Oh. Something is missing. Try double checking things.', 'bonestheme'); ?></p>
        </section>
        <footer class="article-footer">
            <p><?php _e('This is the error message in the single.php template.', 'bonestheme'); ?></p>
        </footer>
    </article>
<?php endif; ?>
<?php get_footer(); ?>