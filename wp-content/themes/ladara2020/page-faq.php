<?php
/*
Template Name: FAQ Page
*/
?>
<?php get_header(); ?>
<?php if (have_posts()) : while (have_posts()) : the_post(); ?>

<?php 
$post_id = $post->ID;

$parent_data = array();
$child_data = array();
$body_data = array();
$args = array(
            'post_type' => 'faqcategory',
            'posts_per_page' => -1,
            'post_status' => 'publish',
            'orderby' => 'title',
            'order' => 'ASC',
            // 'meta_query' => array(
            //     array(
            //     'key' => 'kategori_faq',
            //     'value' => '',
            //     ),
            // ),
    );
$the_query = new WP_Query($args);
while ($the_query->have_posts()) : $the_query->the_post();

$cat_id = get_the_ID();
$cat_name = get_the_title($cat_id);
$parent_id = get_field('kategori_faq',$cat_id);
$cat_link = get_the_permalink($cat_id);
if(isset($parent_id) AND $parent_id != ''){
    $child_data[$cat_id] = array('cat_id'=>$cat_id, 'cat_name'=>$cat_name,'parent_id'=>$parent_id,'cat_link'=>$cat_link);
}else{
    $parent_id = '';
    $parent_data[$cat_id] = array('cat_id'=>$cat_id, 'cat_name'=>$cat_name,'parent_id'=>$parent_id,'cat_link'=>$cat_link);
}

endwhile;
wp_reset_query();


if(isset($child_data) AND !empty($child_data)){
    foreach ($child_data as $key => $value) {
        $c_id = $value['cat_id'];
        $c_par_id = $value['parent_id'];
        $parent_data[$c_par_id]['child'][] = $child_data[$c_id];
    }
}

$parent_id = $post_id;
$args = array(
    'post_type' => 'faq',
    'posts_per_page' => -1,
    'post_status' => 'publish',
    'orderby' => 'date',
    'order' => 'DESC',
    'meta_query' => array(
        array(
        'key' => 'faq_parent_category',
        'value' => $parent_id,
        ),
    ),
);
$the_query = new WP_Query($args);
while ($the_query->have_posts()) : $the_query->the_post();

    $faq_id = get_the_ID();
    $faq_name = get_the_title($faq_id);
    $faq_link = get_the_permalink($faq_id);
    $body_data[] = array(
                    'faq_id' => $faq_id,
                    'faq_name' => $faq_name,
                    'faq_link' => $faq_link
                );

endwhile;
wp_reset_query();

$cat_parent_name = '';
$cat_this_name = '';
?>


    <article id="post-<?php the_ID(); ?>" <?php post_class('clearfix'); ?> role="article" itemscope itemtype="http://schema.org/BlogPosting">

    <div class="row row_qna_page">
        <div class="col-md-12 col_banner_qna">

            <div class="bx_banner_qna">
                <div class="mg_banner_qna">
                    <img src="<?php bloginfo('template_directory'); ?>/library/images/test/ex-banner.jpg">
                </div>               
            </div>


            <div class="row row_body_qna">
                <div class="col-md-4 col-lg-4 col_tab_qna_l">
                    <h3 class="ht_sub_qna">Kategori</h3>

                    <div class="bx_qna_cat">
                        <ul class="ul_qna_cat">
                            <?php 
                                if(isset($parent_data) AND !empty($parent_data)){
                                    foreach ($parent_data as $key => $value) {
                                        $cat_id = $value['cat_id'];
                                        $cat_name = $value['cat_name'];
                                        $parent_id = $value['parent_id'];
                                        $cat_link = $value['cat_link'];
                                        $child = $value['child'];

                                        if(isset($child) AND !empty($child)){
                                            $cat_parent_name = $cat_name;
                                            ?>
                                            <li class="act"><a href="<?php echo $cat_link; ?>">Pendaftaran Toko</a>
                                                <div class="child_qna_cat">    
                                                    <?php
                                                    foreach ($child as $key2 => $value2) {
                                                        $ch_cat_id= $value2['cat_name'];
                                                        $ch_cat_name = $value2['cat_name'];
                                                        $ch_cat_link = $value2['cat_link'];

                                                        if($ch_cat_id == $post_id){
                                                            $cat_this_name = $ch_cat_name;
                                                        ?>
                                                            <div class="a_child_qna_cat"><a href="<?php echo $ch_cat_link; ?>" class="act"><?php echo $ch_cat_name; ?></a></div> 
                                                        <?php
                                                        }else{
                                                        ?>  
                                                            <div class="a_child_qna_cat"><a href="<?php echo $ch_cat_link; ?>" class=""><?php echo $ch_cat_name; ?></a></div> 
                                                        <?php
                                                        }
                                                    }
                                                    ?>
                                                </div>
                                            </li>
                                            <?php
                                        }else{
                                        ?>
                                            <li><a href="<?php echo $cat_link; ?>"><?php echo $cat_name; ?></a></li>
                                        <?php
                                        }

                                    }
                                }
                            ?>
                        </ul>
                    </div>

                </div>
                <div class="col-md-8 col-lg-8 col_tab_qna_r">
                    
                    <div class="bx_res_qna_mst">
                        <h1 class="ht_tab_qna">FAQ <?php echo $cat_parent_name; ?></h1>

                        <div id="label_pointer" class="ico_tab_pointer"><?php echo $cat_this_name; ?></div>

                        <div class="bx_qna_mst">

                            <?php 
                                if(isset($body_data) AND !empty($body_data)){
                                    foreach ($body_data as $key => $value) {
                                    
                                        $faq_id = $value['faq_id'];
                                        $faq_name = $value['faq_name'];
                                        $faq_link = $value['faq_link'];
                                        ?>
                                            <a href="<?php echo $faq_link; ?>" title="Lihat lebih <?php echo $faq_name; ?>">
                                                <div class="pt_qna_mst"><?php echo $faq_name; ?></div>
                                            </a>   
                                        <?php  
                                    }        
                                }
                            ?>

                        </div>

                    </div>

                </div>
            </div>



        </div>
    </div>

    </article>

<?php endwhile; ?>
<?php else : ?>
<?php get_template_part('content', '404pages'); ?>
<?php endif; ?>
<?php get_footer(); ?>