<?php
/*
Template Name: Register Merchant
*/
?>

<?php get_header(); ?>
<?php if (have_posts()) :
    while (have_posts()) :
        the_post();
?>
        <style type="text/css">
            .header {
                height: 135px;
            }

            .header_list_tops {
                display: none !important;
            }
        </style>
        <?php
        date_default_timezone_set("Asia/Jakarta");
        $nowdate = date('Y-m-d');
        $now_time = strtotime($nowdate);

        $current_user = wp_get_current_user();
        $u_id = $current_user->ID;

        $dataMerchant = getMerchant($u_id);

        if (intval($u_id) !== 0) {
            if ($dataMerchant !== null) {
                $dbtimestamp = strtotime($dataMerchant->created_at);
                if (time() - $dbtimestamp > 10 * 60) {
                    echo "<script>window.location.replace('" . constant("MERCHANT_URL") . "')</script>";
                }
            } ?>
            <div class="row"></div>
            <div class="row row_masterPage" data-url="<?php echo home_url(); ?>">
                <div class="alert alert-danger chk_err"></div>
                <div class="col-md-12" style="text-align: center; font-size: 18px; margin: 30px 0px; font-weight: 500;">
                    Halo <b><?php echo $current_user->display_name; ?></b>, ayo lengkapi detail tokomu!
                </div>
                <div class="col-md-6">
                    <img style="width: 85%; height: auto;" src="<?php bloginfo('template_directory'); ?>/library/images/merchant_registration.png">
                </div>
                <div class="col-md-6 merchant-register-form">
                    <div class="col-md-12" style="margin-bottom: 10px;">
                        <input type="text" name="name" class="txt_aform" required="required" placeholder="Nama Toko">
                        <div style="font-weight: 500; margin-top: 5px; display: flex; justify-content: space-between;">
                            <span>Nama yang dipilih tidak bisa diubah lagi nantinya</span>
                            <span id="name-length">0/24</span>
                        </div>
                    </div>
                    <div class="col-md-12" style="margin-bottom: 10px;">
                        <input type="text" name="url" class="txt_aform" required="required" placeholder="Link Toko">
                        <div style="font-weight: 500; margin-top: 5px; display: flex; justify-content: space-between;">
                            <span>Contoh : <?php echo $_SERVER['SERVER_NAME']; ?>/seller/namatokomu</span>
                            <span id="url-length">0/24</span>
                        </div>
                    </div>
                    <div class="col-md-12" style="margin-bottom: 20px;">
                        <input type="text" name="address" class="txt_aform" required="required" placeholder="Alamat Toko">
                    </div>
                    <div class="col-md-12" style="margin-bottom: 20px;">
                        <select name="city" class="merchant_city_search" required="required" autocomplete="off"></select>
                    </div>
                    <div class="col-md-12" style="margin-bottom: 20px;">
                        <select name="postal_code" class="merchant_postcode_search" required="required" autocomplete="off"></select>
                    </div>
                    <input type="hidden" name="province_id" class="txt_aform" required="required">
                    <input type="hidden" name="city_id" class="txt_aform" required="required">
                    <input type="hidden" name="area_id" class="txt_aform" required="required">
                    <input type="hidden" name="latitude" class="txt_aform set-coordinate" required="required">
                    <input type="hidden" name="longitude" class="txt_aform set-coordinate" required="required">
                    <div class="col-md-12">
                        <input style="width: 50%;height: 40px;" type="text" name="address" id="pac-input" class="txt_aform" placeholder="cari alamat">
                    </div>
                    <div class="col-md-12" style="margin-bottom: 10px;">
                        <div style="font-weight: 500; margin-top: 5px; display: flex; justify-content: space-between;">
                            <span>Tambah lokasi pickup</span>
                        </div>
                        <div id="map" style="width: 100%; height: 250px; border: 1px solid black; border-radius: 5px;"></div>
                    </div>
                    <div class="col-md-12 f_aform f_aform_brand">
                        <label class="cont_check">Saya setuju dengan <a target="_blank" href="<?= home_url('syarat-ketentuan-daftar-toko') ?>">Syarat &amp; Ketentuan</a> serta <a target="_blank" href="<?= home_url('/kebijakan-privasi-daftar-toko') ?>">Kebijakan Privasi</a> Ladara.
                            <input type="checkbox" name="agreement" class="check_catVoucher" value="yes">
                            <span class="checkmark"></span>
                        </label>
                    </div>
                    <input type="button" class="sub_login sub_register_merchant" value="Buka Toko Sekarang" style="margin-bottom: 15px;" />
                </div>
            </div>
            <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAhLLkF-IXL7ZMmbgCWO0DAqD7thqz778w&callback=initMap&libraries=places&v=weekly" defer></script>
            <script>
                "use strict";

                let longitude = document.querySelector('[name="longitude"]')
                let latitude = document.querySelector('[name="latitude"]')

                let markers = []
                let map = null

                function placeMarker(position, map) {
                    let marker = new google.maps.Marker({
                        position: position,
                        map: map
                    })

                    map.panTo(position)
                    markers.push(marker)

                    longitude.value = position.lng()
                    latitude.value = position.lat()
                }

                function initMap() {
                    map = new google.maps.Map(document.getElementById("map"), {
                        zoom: 4,
                        center: {
                            lng: 113.921327,
                            lat: -0.789275
                        },
                        mapTypeId: 'roadmap',
                        mapTypeControl: false,
                        streetViewControl: false
                    })

                    setSearchAlamat()
                    map.addListener('click', function(e) {
                        for (let marker of markers) {
                            marker.setMap(null)
                        }

                        placeMarker(e.latLng, map)
                    })
                }

                function setSearchAlamat() {
                    // Create the search box and link it to the UI element.
                    const input = document.getElementById("pac-input");
                    const searchBox = new google.maps.places.SearchBox(input);
                    map.controls[google.maps.ControlPosition.TOP_LEFT].push(input);
                    // Bias the SearchBox results towards current map's viewport.
                    map.addListener("bounds_changed", () => {
                        searchBox.setBounds(map.getBounds());
                    });
                    // Listen for the event fired when the user selects a prediction and retrieve
                    // more details for that place.
                    searchBox.addListener("places_changed", () => {
                        const places = searchBox.getPlaces();

                        if (places.length == 0) {
                            return;
                        }
                        // Clear out the old markers.
                        markers.forEach((marker) => {
                            marker.setMap(null);
                        });
                        markers = [];
                        // For each place, get the icon, name and location.
                        const bounds = new google.maps.LatLngBounds();
                        places.forEach((place) => {
                            if (!place.geometry) {
                                console.log("Returned place contains no geometry");
                                return;
                            }

                            // Create a marker for each place.
                            markers.push(
                                new google.maps.Marker({
                                    map,
                                    position: place.geometry.location,
                                })
                            );

                            if (place.geometry.viewport) {
                                // Only geocodes have viewport.
                                bounds.union(place.geometry.viewport);
                            } else {
                                bounds.extend(place.geometry.location);
                            }
                        });
                        map.fitBounds(bounds);
                    });
                }

                const setCoordinates = document.querySelectorAll('.set-coordinate');

                setCoordinates.forEach(el => el.addEventListener('change', event => {
                    for (let marker of markers) {
                        marker.setMap(null)
                    }

                    let latLng = new google.maps.LatLng(parseFloat(latitude.value), parseFloat(longitude.value));
                    map.setCenter(latLng)
                    map.setZoom(18)

                    let marker = new google.maps.Marker({
                        position: {
                            lng: parseFloat(longitude.value),
                            lat: parseFloat(latitude.value)
                        },
                        map,
                        title: "Pin Lokasi"
                    })

                    markers.push(marker)
                }))
            </script>
        <?php
        } else {
        ?>
            <script>
                // 'Getting' data-attributes using getAttribute
                var plant = document.getElementById('body');
                console.log(plant);
                var hurl = plant.getAttribute('data-hurl');
                location.replace(hurl + '/login/');
            </script>
<?php
        }
    endwhile;
else :
    get_template_part('content', '404pages');
endif;
get_footer(); ?>