<?php
  $value_gram = str_replace(",", ".", $_POST["value_gram"]);
  $value_currency = str_replace(".", "", $_POST["value_currency"]);

  $bank = gld_bank(array("type"=>"xen"));
  $bank = json_decode($bank);
?>

<div id="popUpPaymentGld" class="row bx_leBeli bx_leBeliPopup">
  <div class="bx_gldLoading">
    <img src="<?php bloginfo("template_directory"); ?>/library/images/emas/icon-loading.svg" />
  </div>

  <div class="col-md-5">
    <p class="btn_gldFont closePopup">
      <i class="fas fa-times" onclick="metodePembayaranGld('closepopup')"></i> 
      <span>Pilih Metode Pembayaran</span>
    </p>
    <div class="col_gldContent">
      <p>
        <strong>Transfer Virtual Account</strong>
      </p>
      <ul>
        <?php foreach($bank->data as $b) { ?>
          <li value="<?php echo $b->code; ?>" data-typepayment="va" data-va="<?php echo $b->code; ?>">
            <div>
              <img src="<?php bloginfo("template_directory"); ?>/library/images/emas/icon_va_<?php echo strtolower($b->code); ?>2.png"/ >
              <?php echo $b->name; ?>
            </div>
          </li>
        <?php } ?>
      </ul>
      
      <p>
        <strong>Kartu Kredit</strong>
      </p>
      <ul <?php echo($value_currency < 10000)?"class=\"disabled\"":""; ?>>
        <li <?php echo($value_currency < 10000)?"class=\"disabled\"":""; ?> value="<?php echo $b->code; ?>" data-typepayment="cc">
          <div>
            <img src="<?php bloginfo("template_directory"); ?>/library/images/emas/icon_cc2.png"/ >
            Kartu Kredit
          </div>
          <?php if($value_currency < 10000) { ?>
            <p class="error_beliemas_cc">
              Kamu tidak bisa melakukan pembayaran dengan Kartu Kredit jika membeli emas dibawah Rp 10.000
            </p>
          <?php } ?>
        </li>
      </ul>

      <p>
        <strong>Pembayaran Instan</strong>
      </p>
      <ul>
        <li value="<?php echo $b->code; ?>" data-typepayment="ewallet" data-ewallet="DANA">
          <div>
            <img src="<?php bloginfo("template_directory"); ?>/library/images/emas/icon_ewallet_dana2.png"/ >
            DANA
          </div>
        </li>
        <li value="<?php echo $b->code; ?>" data-typepayment="ewallet" data-ewallet="LINKAJA">
          <div>
            <img src="<?php bloginfo("template_directory"); ?>/library/images/emas/icon_ewallet_linkaja2.png"/ >
            LinkAja
          </div>
        </li>
        <?php /*
        <li value="<?php echo $b->code; ?>" data-typepayment="ewallet" data-ewallet="OVO">
          <div>
            <img src="<?php bloginfo("template_directory"); ?>/library/images/emas/icon_ewallet_ovo2.png"/ >
            OVO
          </div>
        </li>
        */ ?>
      </ul>
      
      <?php /*      
      <p>
        <strong>Tunai di Gerai Retail</strong>
      </p>
      <ul>
        <li value="<?php echo $b->code; ?>" data-typepayment="retail" data-retail="ALFAMART">
          <div>
            <img src="<?php bloginfo("template_directory"); ?>/library/images/emas/icon_retail_alfamart2.png"/ >
            Alfamart
          </div>
        </li>
        <li value="<?php echo $b->code; ?>" data-typepayment="retail" data-retail="INDOMARET">
          <div>
            <img src="<?php bloginfo("template_directory"); ?>/library/images/emas/icon_retail_indomaret2.png"/ >
            Indomaret
          </div>
        </li>
      </ul>
      */ ?>
    </div>
  </div>
</div>