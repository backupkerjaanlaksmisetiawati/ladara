<div id="popUpBuyGld" class="row bx_leBeli bx_leBeliPopup">
  <div class="bx_gldLoading">
    <img src="<?php bloginfo("template_directory"); ?>/library/images/emas/icon-loading.svg" />
  </div>

  <div class="col-md-5">
    <div class="col_gldContent">
      <p>
        <img src="<?php bloginfo("template_directory"); ?>/library/images/emas/img-business-and-finance.svg" />
      </p>
      <h4>Apa kamu yakin akan menabung emas di Ladara Emas sebesar <?php echo $current_gold_value; ?> gram?</h4>
      <p>Jika kamu yakin, uang kamu akan langsung dikonversi menjadi emas. Kamu dapat melakukan penarikan sewaktu-waktu.</p>
      <div class="gldDesktop">
        <p class="button">
          <a class="btn_gldFont LEBtn_blue_white" onclick="ajax_buy_emas('desktop')">Ya, saya yakin</a>
        </p>
      </div>
      <div class="gldMobile">
        <p class="button">
          <a class="btn_gldFont LEBtn_blue_white" onclick="ajax_buy_emas('mobile')">Ya, saya yakin</a>
        </p>
      </div>
      <p class="button button2">
        <a class="btn_gldFont LEBtn_white_blue" onclick="verifikasiGldBuy('hidepopup')">Biar saya pikirkan lagi</a>
      </p>
    </div>
  </div>
</div>