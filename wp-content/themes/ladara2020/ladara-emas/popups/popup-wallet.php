<div id="popUpNoHpEwalletGld" class="row bx_leBeli bx_leBeliPopup">
  <div class="bx_gldLoading">
    <img src="<?php bloginfo("template_directory"); ?>/library/images/emas/icon-loading.svg" />
  </div>
  <div class="col-md-5">
    <p class="btn_gldFont closePopup">
      <i class="fas fa-times" onclick="noHpEwalletGld('closepopup', '')"></i> 
      <span></span>
    </p>
    <div class="col_gldContent">
      <p>
        <strong>Masukan no hp yang terdaftar di <span class="namaNoHpEwalletGld"></span></strong> 
        <img class="iconNoHpEwalletGld" src="<?php bloginfo("template_directory"); ?>/library/images/emas/icon_ewallet_dana.png" style="width:20px;margin-top:5px;" />
      </p>
      <div class="gld_ewalletPhone">
        <p>
          <span>+62</span>
          <input type="number" id="popup_ewallet_phone" min="1" />
        </p>
      </div>
      <p class="error_beliemas_ewallet" style="display:none;"></p>
      <p class="button">
        <a class="btn_gldFont LEBtn_blue_white" onclick="noHpEwalletGld('hidepopup', '')">Submit</a>
      </p>
    </div>
  </div>
</div>