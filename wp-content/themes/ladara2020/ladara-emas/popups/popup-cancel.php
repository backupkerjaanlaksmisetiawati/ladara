<?php
  $data_beli_emas = $_SESSION['data_beli_emas'];
?>

<?php if(!isset($data_beli_emas->no_va)) { ?>
  <div class="row bx_leBeli bx_leBeliPopup">
    <div class="bx_gldLoading">
      <img src="<?php bloginfo("template_directory"); ?>/library/images/emas/icon-loading.svg" />
    </div>

    <div class="col-md-5">
      <div class="col_gldContent">
        <p>
          <img src="<?php bloginfo("template_directory"); ?>/library/images/emas/img-business-and-finance.svg" />
        </p>
        <h4>Kamu belum menyelesaikan proses pembelian emas. Apakah kamu yakin untuk keluar?</h4>
        <p>Jika kamu yakin, pembelian kamu akan dibatalkan dan kamu dapat melakukan pembelian kembali sewaktu-waktu.</p>
        <div class="gldDesktop">
          <p class="button">
            <a class="btn_gldFont LEBtn_blue_white" onclick="ajax_gld_cancel_buy('<?php echo $data_beli_emas->no_invoice; ?>')">Ya, saya yakin</a>
          </p>
        </div>
        <div class="gldMobile">
          <p class="button">
            <a class="btn_gldFont LEBtn_blue_white" onclick="ajax_gld_cancel_buy('<?php echo $data_beli_emas->no_invoice; ?>')">Ya, saya yakin</a>
          </p>
        </div>
        <p class="button button2">
          <a class="btn_gldFont LEBtn_white_blue" onclick="gldAlertBack('hidepopup')">Biar saya pikirkan lagi</a>
        </p>
      </div>
    </div>
  </div>
<?php } ?>