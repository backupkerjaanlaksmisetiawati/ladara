<?php
  $user_id = get_current_user_id();

  $profile = gld_profile_emas(array("user_id"=>$user_id));
  $profile = json_decode($profile);

  $data_beli_emas = $_SESSION['data_beli_emas'];
?>

<div class="col_gldContent col_finishedBeliEmas">
  <div class="box">
    <h3 class="title">Halo, <strong><?php echo $profile->data->name; ?></strong>!</h3>
    <h3 class="title">Yuk segera selesaikan pembayaran kamu!</h3>
  </div>
  <div class="border">&nbsp;</div>
  <div class="box_invoiceEmas">
    <p>
      Kamu telah melakukan pembelian emas pada <strong><?php echo date("j", strtotime($data_beli_emas->created_at)); ?> <?php echo month_indonesia(date("n", strtotime($data_beli_emas->created_at))); ?> <?php echo date("Y", strtotime($data_beli_emas->created_at)); ?> (<?php echo date("H:i", strtotime($data_beli_emas->created_at)); ?> WIB)</strong> dengan informasi sebagai berikut:
    </p>

    <div class="tgl_invoice">
      <div>
        <p>Nomor Invoice</p>
        <p>: <strong><?php echo $data_beli_emas->no_invoice; ?></strong></p>
      </div>
      <div>
        <p>Waktu</p>
        <p>: <?php echo date("j", strtotime($data_beli_emas->created_at)); ?> <?php echo month_indonesia(date("n", strtotime($data_beli_emas->created_at))); ?> <?php echo date("Y", strtotime($data_beli_emas->created_at)); ?> (<?php echo date("H:i", strtotime($data_beli_emas->created_at)); ?> WIB)</p>
      </div>
      <div>
        <p>Metode Pembayaran</p>
        <p>: <strong><?php echo $data_beli_emas->metode_pembayaran; ?></strong></p>
      </div>
      <div>
        <p>Batas Waktu Pembayaran</p>
        <p>: <?php echo date("j", strtotime($data_beli_emas->due_date)); ?> <?php echo month_indonesia(date("n", strtotime($data_beli_emas->due_date))); ?> <?php echo date("Y", strtotime($data_beli_emas->due_date)); ?> (<?php echo date("H:i", strtotime($data_beli_emas->due_date)); ?> WIB)</p>
      </div>
    </div>

    <div class="detail_invoice">
      <div>
        <p>Nilai Beli Emas</p>
        <p><strong>Rp <?php echo number_format(round($data_beli_emas->total_price), 0, ".", "."); ?></strong></p>
      </div>
      <div class="red">
        <p>Jumlah Emas</p>
        <p><strong><?php echo str_replace(".", ",", round($data_beli_emas->total_emas, 4, PHP_ROUND_HALF_UP)); ?> gram</strong></p>
      </div>
      <div>
        <p>Biaya Pemesanan Emas</p>
        <p>Rp <?php echo number_format(round($data_beli_emas->booking_fee), 0, ".", "."); ?></p>
      </div>
      <div>
        <p>PPN</p>
        <p>Rp <?php echo number_format(round($data_beli_emas->tax), 0, ".", "."); ?></p>
      </div>
      <div>
        <p>Biaya Admin</p>
        <p>Rp <?php echo number_format(round($data_beli_emas->partner_fee), 0, ".", "."); ?></p>
      </div>
      <div>
        <p>Biaya Bank</p>
        <p>Rp <?php echo number_format(round($data_beli_emas->xendit_fee), 0, ".", "."); ?></p>
      </div>
      <div class="total">
        <p><strong>Total</strong></p>
        <p><strong>Rp <?php echo number_format(round($data_beli_emas->total_payment), 0, ".", "."); ?></strong></p>
      </div>
    </div>
    
    <?php if(isset($data_beli_emas->no_va)) { ?>
      <div class="detail_va">
        <p>Kode pembayaran virtual account kamu adalah:</p>
        <h4>
          <input id="noVirtualAccount" readonly="readonly" value="<?php echo $data_beli_emas->no_va; ?>" />
        </h4>
      </div>

      <div class="detail_va">
        <h4>
          <button class="btn_gldFont LEBtn_rainbow" onclick="copyVirtualAccount()">
            Salin Virtual Account
          </button>
        </h4>
      </div>

      <?php if(!empty($data_beli_emas->va_instructions)) { ?>
        <?php $va_instructions = (array)$data_beli_emas->va_instructions; ?>

        <div class="tutorial_paymentEmas">
          <h4 class="title">Langkah-langkah pembayaran <?php echo $data_beli_emas->metode_pembayaran; ?>:</h4>

          <?php $bankname = str_replace(" Virtual Account", "", $data_beli_emas->metode_pembayaran); ?>

          <?php foreach($va_instructions as $key => $instruction) { ?>
            <?php
              $title_string = $key;
              $title = preg_replace('/(?<!\ )[A-Z]/', ' $0', $title_string);
              $title = ucwords($title);

              if(strtolower($title) === "ovo") {
                $title = "OVO";
              }

              if (strpos(strtolower($title), "atm") !== false) {
                $title = str_replace("Atm", "ATM", $title);
              }

              if (strpos(strtolower($title), "sms") !== false) {
                $title = str_replace("Sms", "SMS", $title);
              }

              if (strpos(strtolower($title), "ibank") !== false) {
                $title = str_replace("Ibank", "ibank", $title);
              }

              if (strpos(strtolower($title), "mbanking") !== false) {
                $title = str_replace("Mbanking", "mbanking", $title);
              }

              if(strtolower($bankname) !== "mandiri" && strtolower($bankname) !== "permata") {
                if (strpos($title, ucfirst(strtolower($bankname))) !== false) {
                  $title = str_replace(ucfirst(strtolower($bankname)), strtoupper($bankname), $title);
                }
              }
            ?>
          
            <div class="step box_step_<?php echo strtolower($key); ?> <?php echo (array_key_first($va_instructions) == $key) ?'active':''; ?>">
              <h4 onclick="showGldTutorialPayment('<?php echo strtolower($key); ?>')">
                <span>Via <?php echo $title; ?></span>
                <img src="<?php bloginfo("template_directory"); ?>/library/images/emas/icon-arrow.svg" />
              </h4>
              <div class="step_<?php echo strtolower($key); ?> <?php echo (array_key_first($va_instructions) == $key) ?'active':''; ?>">
                <ol>
                  <?php foreach($instruction as $step) { ?>
                    <li>
                      <?php echo str_replace("{{- vaNumber}}", $data_beli_emas->no_va, str_replace("{{companyCode}}", $data_beli_emas->xen_company_code, str_replace("{{companyName}}", $data_beli_emas->xen_merchant_name, $step->step))); ?>
                    </li>
                  <?php } ?>
                </ol>
              </div>
            </div>
          <?php } ?>
        </div>
      <?php } ?>
    <?php } ?>

    <?php if(!empty($data_beli_emas->xendit_url) && $data_beli_emas->type_payment !== "va") { ?>
      <div class="detail_va">
        <p>Silahkan klik tombol di bawah ini untuk melakukan pembayaran</p>
        <h4>
          <a class="btn_gldFont LEBtn_rainbow" href="<?php echo $data_beli_emas->xendit_url; ?>">
            Bayar Sekarang
          </a>
        </h4>
      </div>
    <?php } ?>
    
    <?php if(isset($data_beli_emas->xendit_payment_code)) { ?>
      <div class="detail_va">
        <p>Kode pembayaran <?php echo ucfirst(strtolower($data_beli_emas->metode_pembayaran)); ?> kamu adalah:</p>
        <h4>
          <input id="noVirtualAccount" readonly="readonly" value="<?php echo $data_beli_emas->xendit_payment_code; ?>" />
        </h4>
      </div>

      <div class="detail_va">
        <h4>
          <button class="btn_gldFont LEBtn_rainbow" onclick="copyVirtualAccount()">
            Salin Kode Pembayaran
          </button>
        </h4>
      </div>
    <?php } ?>
  </div>
</div>