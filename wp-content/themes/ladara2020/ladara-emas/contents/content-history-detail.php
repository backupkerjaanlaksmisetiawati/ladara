<?php
  $user_id = get_current_user_id();

  // $invoice_order = (isset($_GET["faktur"])) ? str_replace("-", "/", $_GET["faktur"]) : str_replace("-", "/", $_GET["invoice"]);
  $invoice_order = (isset($_GET["faktur"])) ? $_GET["faktur"] : $_GET["invoice"];

  $history = gld_detail_history(
    array(
      "invoice_no"  => $invoice_order
    )
  );
  $history = json_decode($history);
  $history = $history->data;
  
  if(strtolower($history->status) === "pending") {
    $status_color = "FFA600";
  } elseif(strtolower($history->status) === "canceled" || strtolower($history->status) === "rejected") {
    $status_color = "FF1E00";
  } else {
    $status_color = "5CC450";
  }

  $va_instructions = "";
  $bankname = "";
  if(!empty($history->xendit_bank)) {
    $bankname = $history->xendit_bank;

    $va_instructions = gld_va_instructions(array("bank"=>$history->xendit_bank));
    $va_instructions = json_decode($va_instructions);
  }

  $user = $wpdb->get_row(
    "SELECT name, id, email, account_name, account_number, bank_code, bank_name, branch 
      FROM ldr_user_emas WHERE id=" . $history->user_id . "",
    OBJECT
  );
?>

<div class="col_gldContent col_finishedBeliEmas">
  <div class="box">
    <h3 class="title">Halo, <strong><?php echo $user->name; ?></strong>!</h3>
  </div>
  <div class="border">&nbsp;</div>
  <div class="box_invoiceEmas">
    <p>
      <?php if(strtolower($history->type) === "buy") { ?>
        Kamu telah melakukan pembelian emas pada <strong><?php echo date("j", strtotime($history->created_date)); ?> <?php echo month_indonesia(date("n", strtotime($history->created_date))); ?> <?php echo date("Y", strtotime($history->created_date)); ?> (<?php echo date("H:i", strtotime($history->created_date)); ?> WIB)</strong> dengan informasi sebagai berikut:
      <?php } else { ?>
        Kamu telah melakukan penjualan emas pada <strong><?php echo date("j", strtotime($history->created_date)); ?> <?php echo month_indonesia(date("n", strtotime($history->created_date))); ?> <?php echo date("Y", strtotime($history->created_date)); ?> (<?php echo date("H:i", strtotime($history->created_date)); ?> WIB)</strong> dengan informasi sebagai berikut:
      <?php } ?>
    </p>

    <div class="tgl_invoice">
      <div>
        <p>Nomor Invoice</p>
        <p>: <strong><?php echo $history->invoice_order; ?></strong></p>
      </div>
      <div>
        <p>Waktu</p>
        <p>: <?php echo date("j", strtotime($history->created_date)); ?> <?php echo month_indonesia(date("n", strtotime($history->created_date))); ?> <?php echo date("Y", strtotime($history->created_date)); ?> (<?php echo date("H:i", strtotime($history->created_date)); ?> WIB)</p>
      </div>
      <?php if(strtolower($history->type) === "buy") { ?>
        <div>
          <p>Metode Pembayaran</p>
          <p>: <strong><?php echo $history->payment_method; ?></strong></p>
        </div>
        <?php if(!empty($history->xendit_va)) { ?>
          <div>
            <p>Virtual Account</p>
            <p>: <strong><?php echo $history->xendit_va; ?></strong></p>
          </div>
        <?php } ?>
        <?php if(!empty($history->xendit_payment_code)) { ?>
          <div>
            <p>Kode Pembayaran</p>
            <p>: <strong><?php echo $history->xendit_payment_code; ?></strong></p>
          </div>
        <?php } ?>
        <div>
          <p>Batas Waktu Pembayaran</p>
          <p>: <?php echo date("j", strtotime($history->due_date)); ?> <?php echo month_indonesia(date("n", strtotime($history->due_date))); ?> <?php echo date("Y", strtotime($history->due_date)); ?> (<?php echo date("H:i", strtotime($history->due_date)); ?> WIB)</p>
        </div>
      <?php } ?>
      <?php if(strtolower($history->type) === "sell") { ?>
        <div>
          <p>No Rekening</p>
          <p>: <strong><?php echo $history->bank_account; ?></strong></p>
        </div>
        <div>
          <p>Pemilik Rekening</p>
          <p>: <strong><?php echo $history->bank_account_name; ?></strong></p>
        </div>
        <div>
          <p>Nama Bank</p>
          <p>: <strong><?php echo $history->bank_name; ?></strong></p>
        </div>
      <?php } ?>
      <div>
        <?php if(strtolower($history->type) === "buy") { ?>
          <p>Rate Beli Emas</p>
          <p>: Rp <?php echo number_format(round($history->buying_rate), 0, ".", "."); ?>/gr</p>
        <?php } else { ?>
          <p>Rate Jual Emas</p>
          <p>: Rp <?php echo number_format(round($history->selling_rate), 0, ".", "."); ?>/gr</p>
        <?php } ?>
      </div>
    </div>

    <div class="detail_invoice">
      <div>
        <p>Nilai Beli Emas</p>
        <p><strong>Rp <?php echo number_format(round($history->total_price), 0, ".", "."); ?></strong></p>
      </div>
      <div class="red">
        <p>Jumlah Emas</p>
        <p>
          <strong style="color:#0080ff;">
            <?php echo str_replace(".", ",", round($history->total_gold, 4, PHP_ROUND_HALF_UP)); ?> gram
          </strong>
        </p>
      </div>
      <?php if(strtolower($history->type) === "buy") { ?>
        <div>
          <p>Biaya Pemesanan Emas</p>
          <p>Rp <?php echo number_format(round($history->booking_fee), 0, ".", "."); ?></p>
        </div>
        <div>
          <p>PPN</p>
          <p>Rp <?php echo number_format(round($history->tax), 0, ".", "."); ?></p>
        </div>
      <?php } ?>
      <div>
        <p>Biaya Admin</p>
        <p>Rp <?php echo number_format(round($history->partner_fee), 0, ".", "."); ?></p>
      </div>
      <?php if(strtolower($history->type) === "buy") { ?>
        <div>
          <p>Biaya Bank</p>
          <p>Rp <?php echo number_format(round($history->xendit_fee), 0, ".", "."); ?></p>
        </div>
      <?php } ?>
      <div class="total">
        <p><strong>Total</strong></p>
        <p>
          <strong style="color:#0080ff;">
            Rp <?php echo number_format(round($history->total_payment), 0, ".", "."); ?>
          </strong>
        </p>
      </div>
      <div class="total">
        <p><strong>Status</strong></p>
        <p>
          <strong style="color:#<?php echo $status_color; ?>">
            <?php if(strtolower($history->type) === "buy" && strtolower($history->status) == "pending") { ?>
              Pembelian Menunggu Pembayaran
            <?php } else if(strtolower($history->type) === "buy" && strtolower($history->status) == "canceled") { ?>
              Pembelian Telah Dibatalkan 
              <?php if(!empty($history->status_reason)) { ?>
                (<?php echo $history->status_reason; ?>)
              <?php } ?>
            <?php } else if(strtolower($history->type) === "buy" && strtolower($history->status) == "success") { ?>
              Pembelian Berhasil
            <?php } else if(strtolower($history->type) === "buy" && strtolower($history->status) == "failed") { ?>
              Pembelian Telah Gagal
            <?php } else if(strtolower($history->type) === "sell" && strtolower($history->status) == "approved") { ?>
              Penjualan Telah Disetujui
            <?php } else if(strtolower($history->type) === "sell" && strtolower($history->status) == "pending") { ?>
              Penjualan Sedang Diproses
            <?php } else if(strtolower($history->type) === "sell" && strtolower($history->status) == "rejected") { ?>
              Penjualan Telah Ditolak 
              <?php if(!empty($history->status_reason)) { ?>
                (<?php echo $history->status_reason; ?>)
              <?php } ?>
            <?php } ?>
          </strong>
        </p>
      </div>
    </div>
    
    <?php if(strtolower($history->type) === "buy" && strtolower($history->status) == "pending") { ?>
      <?php if(!empty($history->xendit_bank)) { ?>
        <div class="detail_va">
          <p>Kode pembayaran virtual account kamu adalah:</p>
          <h4>
            <input id="noVirtualAccount" readonly="readonly" value="<?php echo $history->xendit_va; ?>" />
          </h4>
        </div>

        <div class="detail_va">
          <h4>
            <button class="btn_gldFont LEBtn_rainbow" onclick="copyVirtualAccount()">
              Salin Virtual Account
            </button>
          </h4>
        </div>

        <?php if(!empty($va_instructions) && $va_instructions->code === 200) { ?>
          <?php $vaInstructions = (array)$va_instructions->data; ?>

          <div class="tutorial_paymentEmas">
            <h4 class="title">Langkah-langkah pembayaran <?php echo $history->payment_method; ?>:</h4>

            <?php foreach($vaInstructions as $key => $instruction) { ?>
              <?php
                $title_string = $key;
                $title = preg_replace('/(?<!\ )[A-Z]/', ' $0', $title_string);
                $title = ucwords($title);

                if(strtolower($title) === "ovo") {
                  $title = "OVO";
                }

                if (strpos(strtolower($title), "atm") !== false) {
                  $title = str_replace("Atm", "ATM", $title);
                }

                if (strpos(strtolower($title), "sms") !== false) {
                  $title = str_replace("Sms", "SMS", $title);
                }

                if (strpos(strtolower($title), "ibank") !== false) {
                  $title = str_replace("Ibank", "ibank", $title);
                }

                if (strpos(strtolower($title), "mbanking") !== false) {
                  $title = str_replace("Mbanking", "mbanking", $title);
                }

                if(strtolower($bankname) !== "mandiri" && strtolower($bankname) !== "permata") {
                  if (strpos($title, ucfirst(strtolower($bankname))) !== false) {
                    $title = str_replace(ucfirst(strtolower($bankname)), strtoupper($bankname), $title);
                  }
                }
              ?>
            
              <div class="step box_step_<?php echo strtolower($key); ?> <?php echo (array_key_first($vaInstructions) == $key) ?'active':''; ?>">
                <h4 onclick="showGldTutorialPayment('<?php echo strtolower($key); ?>')">
                  <span>Via <?php echo $title; ?></span>
                  <img src="<?php bloginfo("template_directory"); ?>/library/images/emas/icon-arrow.svg" />
                </h4>
                <div class="step_<?php echo strtolower($key); ?> <?php echo (array_key_first($vaInstructions) == $key) ?'active':''; ?>">
                  <ol>
                    <?php foreach($instruction as $step) { ?>
                      <li>
                        <?php echo str_replace("{{- vaNumber}}", $history->xendit_va, str_replace("{{companyCode}}", $GLOBALS["gld_global"]["xendit"]["company_code"], str_replace("{{companyName}}", $GLOBALS["gld_global"]["xendit"]["merchant_name"], $step->step))); ?>
                      </li>
                    <?php } ?>
                  </ol>
                </div>
              </div>
            <?php } ?>
          </div>
        <?php } ?>
      <?php } ?>

      <?php if(empty($history->xendit_bank)) { ?>
        <div class="detail_va">
          <p>Silahkan klik tombol di bawah ini untuk melakukan pembayaran</p>
          <h4>
            <a class="btn_gldFont LEBtn_rainbow" href="<?php echo $history->xendit_url; ?>">
              Bayar Sekarang
            </a>
          </h4>
        </div>
      <?php } ?>

      <?php if(!empty($history->xendit_payment_code)) { ?>
        <div class="detail_va">
          <p>Kode pembayaran <?php echo ucfirst(strtolower($history->payment_method)); ?> kamu adalah:</p>
          <h4>
            <input id="noVirtualAccount" readonly="readonly" value="<?php echo $history->xendit_payment_code; ?>" />
          </h4>
        </div>

        <div class="detail_va">
          <h4>
            <button class="btn_gldFont LEBtn_rainbow" onclick="copyVirtualAccount()">
              Salin Kode Pembayaran
            </button>
          </h4>
        </div>
      <?php } ?>
    <?php } ?>
  </div>
</div>