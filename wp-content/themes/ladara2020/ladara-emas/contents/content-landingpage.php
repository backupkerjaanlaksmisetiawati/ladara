<?php
  $banner = gld_banner();
  $banner = json_decode($banner);
  $banner = $banner->data;
  $advantages = gld_landing_page();
  $advantages = json_decode($advantages);
  $advantages = $advantages->data;
?>

<div id="LadaraEmas" class="row_gldDashboardPage">
  <div class="gldDesktop">
    <div class="row">
      <div class="col-md-12">
        <div class="bx_gldBreadcrumb">
          <a href="<?php echo home_url(); ?>">
            <span>&lsaquo;</span>
            <span>Kembali ke Home</span>
          </a>
        </div>
        <div class="col_gldContent">
          <div class="box_gldBanner">
            <img src="<?php echo $banner->desktop; ?>" class="full-img" />
          </div>
          
          <div class="bx_LEPlus">
            <h3 class="ht_indexLEPlus">Apa sih kelebihannya Ladara Emas?</h3>

            <div class="row row_LEPlus box_gldAdvs">
              <?php foreach($advantages as $adv) { ?>
                <div class="cols col-sm-4 col-md-4">
                  <div>
                    <img src="<?php echo $adv->icon; ?>" />
                  </div>
                  <p>
                    <?php echo $adv->title; ?>
                  </p>
                  <p>
                    <?php echo $adv->description; ?>
                  </p>
                </div>
              <?php } ?>
            </div>

            <div class="row p-relative" style="display:flex;justify-content:center;">
              <div class="col-sm-4 col-md-4">
                <div class="row_btnGldIndex">
                  <a class="btn_gldFont LEBtn_blue_white" href="<?php echo home_url(); ?>/emas/aktivasi">Mulai Sekarang</a>
                </div>
                <div class="row_btnGldIndex scd">
                  <a class="btn_gldFont LEBtn_white" href="<?php echo home_url(); ?>/emas/faq-ladara-emas">Pelajari Lebih Lanjut</a>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  
  <div class="gldMobile box_mgldDashboard">
    <div class="col_gldContent">
      <div class="box_gldBanner">
        <img src="<?php echo $banner->mobile; ?>" class="full-img lazy" />
      </div>
      
      <div class="bx_LEPlus">
        <h3 class="ht_indexLEPlus">Apa sih kelebihannya Ladara Emas?</h3>

        <div class="box_mgldAdvs">
          <?php foreach($advantages as $adv) { ?>
            <div class="clearfix">
              <div>
                <img src="<?php echo $adv->icon; ?>" class="lazy" />
              </div>
              <div>
                <p>
                  <?php echo $adv->title; ?>
                </p>
                <p>
                  <?php echo $adv->description; ?>
                </p>
              </div>
            </div>
          <?php } ?>
        </div>
        
        <div class="row_btnGldIndex">
          <a class="btn_gldFont LEBtn_blue_white" href="<?php echo home_url(); ?>/emas/aktivasi">Mulai Sekarang</a>
        </div>
        <div class="row_btnGldIndex scd">
          <a class="btn_gldFont LEBtn_white" href="<?php echo home_url(); ?>/emas/faq-ladara-emas">Pelajari Lebih Lanjut</a>
        </div>
      </div>
    </div>
  </div>
</div>