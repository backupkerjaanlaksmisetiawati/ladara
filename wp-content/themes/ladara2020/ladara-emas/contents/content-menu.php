<?php
  global $post;

  $user_id = get_current_user_id();

  $profile = gld_profile_emas(array("user_id"=>$user_id));
  $profile = json_decode($profile);

  $user_avatar = get_field('user_avatar', 'user_' . $user_id);
  if(isset($user_avatar) AND $user_avatar != '') {
    $linkpoto = wp_get_attachment_image_src($user_avatar,'thumbnail');
    $url_avatar = $linkpoto[0];
  } else {
    $url_avatar = get_template_directory_uri().'/library/images/icon_profile.png';
  }
?>

<div id="menuGld" class="col_gldContent col_gldProfile" style="display:none;">
  <a class="box_gldProfile" href="<?php echo home_url(); ?>/profile">
    <img src="<?php echo $url_avatar; ?>">
    <span><?php echo $profile->data->name; ?></span>
  </a>

  <div class="box_gldProfileDate">
    <span>
      <?php echo day_indonesia(date('w')) . ', ' . date('d') . ' ' . month_indonesia(date('n')) . ' ' . date('Y'); ?>
    </span>
    <div class="clock">
      <span id="gldOHours">00</span><span id="gldOPoint">:</span><span id="gldOMin">00</span><span id="gldOPoint">:</span><span id="gldOSec">00</span>
    </div>
  </div>

  <div class="bx_leMenu">
    <a href="<?php echo home_url(); ?>/emas" <?php echo ($post->post_name === "riwayat-transaksi") ? 'class="inactive"' : ""; ?>>
      <?php if($post->post_name === "emas") { ?>
        <img src="<?php bloginfo('template_directory'); ?>/library/images/emas/icon-shopping-bag.svg">
      <?php } else { ?>
        <img src="<?php bloginfo('template_directory'); ?>/library/images/emas/icon-shopping-bag-inactive.svg">
      <?php } ?>
      <span <?php echo ($post->post_name === "emas") ? 'style="color:#FE5461"' : ""; ?>>Jual/Beli Emas</span>
    </a>

    <a href="<?php echo home_url(); ?>/emas/riwayat-transaksi" <?php echo ($post->post_name === "emas") ? 'class="inactive"' : ""; ?>>
      <?php if($post->post_name === "riwayat-transaksi") { ?>
        <img src="<?php bloginfo('template_directory'); ?>/library/images/emas/icon-history.svg">
      <?php } else { ?>
        <img src="<?php bloginfo('template_directory'); ?>/library/images/emas/icon-history-inactive.svg">
      <?php } ?>
      <span <?php echo ($post->post_name === "riwayat-transaksi") ? 'style="color:#FE5461"' : ""; ?>>Riwayat Transaksi</span>
    </a>

    <div>
      <p class="box_lePowered">
        Powered by:
        <img src="<?php bloginfo('template_directory'); ?>/library/images/emas/logo-treasury.png" />
      </p>
    </div>
  </div>
</div>