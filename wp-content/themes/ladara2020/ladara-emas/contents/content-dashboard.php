<?php
  $user_id = get_current_user_id();
  $gldPage = home_url() . "/emas";

  $profile = gld_profile_emas(array("user_id"=>$user_id));
  $profile = json_decode($profile);

  $harga_emas = gld_rate(array("user_id"=>$user_id));
  $harga_emas = json_decode($harga_emas);
?>

<div id="LadaraEmas" class="row_gldDashboardPage">
  <div class="gldDesktop">
    <div class="col-md-12">
      <div class="bx_gldBreadcrumb bx_breadcrumbAktivasiLE">
        <div>
          <a href="<?php echo home_url(); ?>">
            <span>&lsaquo;</span>
            <span>Kembali ke Home</span>
          </a>
        </div>
      </div>

      <div class="row">
        <div class="col-sm-3 col-md-3">
          <?php get_template_part( "ladara-emas/contents/content", "menu" ); ?>
        </div>

        <div class="col-sm-9 col-md-9">
          <div id="gldBalance" class="col_gldContent" style="display:none;">
            <div class="box_gldDetail">
              <div class="box_gldBalance">
                <img src="<?php bloginfo("template_directory"); ?>/library/images/emas/icon-gold.svg" />
                <div>
                  <p class="wrap_tooltip">
                    Nilai Emas Kamu
                    <span class="tooltip_gldBalance">
                      <label>
                        Perhitungan nilai emas kamu berasal dari jumlah emas dalam gram dikali harga jual emas/gr saat ini.
                      </label>
                    </span>
                  </p>
                  <h4 id="wrapgldBalanceRp">Rp <span id="gldBalanceRp"><?php echo number_format(round($profile->data->gold_balance_in_currency, 0), 0, ".", "."); ?></span></h4>
                  <?php $gold_balance = str_replace(".", ",", round($profile->data->gold_balance, 4, PHP_ROUND_HALF_UP)); ?>
                  <p>Jumlah Emas: <span id="gldBalanceGr"><?php echo $gold_balance; ?></span> <span>gram</span></p>
                </div>
              </div>

              <div class="box_gldRate">
                <div>
                  <p>Harga Beli Emas</p>
                  <h4 id="wrapgldRateBuy">
                    Rp <span id="gldRateBuy"><?php echo number_format(round($harga_emas->data->buy_price), 0, ".", "."); ?></span>/gr
                  </h4>
                </div>
                <div>
                  <p>Harga Jual Emas</p>
                  <h4 id="wrapgldRateSell">
                    Rp <span id="gldRateSell"><?php echo number_format(round($harga_emas->data->sell_price), 0, ".", "."); ?></span>/gr
                  </h4>
                </div>
              </div>
            </div>

            <div class="box_leBgGold"></div>
          </div>

          <div class="col_gldContent">
            <div id="gldRateTabs" style="display:none;">
              <ul>
                <li class="beliemas"><a href="#beliemas">Beli Emas</a></li>
                <li class="jualemas"><a href="#jualemas">Jual Emas</a></li>
              </ul>

              <div id="beliemas">
                <?php if(isset($_SESSION["alert_error_buy_emas"])) { ?>
                  <p style="color:#FE5461;">
                    <?php echo $_SESSION["alert_error_buy_emas"]; ?>
                  </p>
                <?php } ?>
                <form id="formBeliemas" class="row" action="<?php echo home_url(); ?>/emas/beli" method="post">
                  <label class="col-md-4 col_leNilaiRp">
                    <p>Nilai dalam Rupiah</p>
                <input type="text" autocomplete="off" name="value_currency" id="valueRpBeli" class="input valueRpBeli gldCal" placeholder="0"<?php if($profile->data->activated_user == 0) { ?> disabled<?php } ?> />
                  </label>
                  <label class="col-md-4 col_leNilaiGr">
                    <p>Jumlah Emas dalam gr</p>
                    <input type="text" autocomplete="off" name="value_gram" id="valueGrBeli" class="input valueGrBeli gldCal" placeholder="0"<?php if($profile->data->activated_user == 0) { ?> disabled<?php } ?> />
                  </label>
                  <div class="col-md-4 col_leBtnBeliEmas">
                    <img src="<?php bloginfo("template_directory"); ?>/library/images/emas/icon-lephone.svg" />
                    <?php if($profile->data->activated_user == 1) { ?>
                      <p>Pastikan yang kamu<br />masukkan <span>sudah benar</span> ya!</p>
                      <p>
                        <button type="submit" class="beliemassekarang btn_gldFont LEBtn_rainbow" disabled>Lanjut</button>
                      </p>
                      <div class="d-none">
                        <input type="hidden" name="transaction_type" value="buy" />
                        <input type="text" name="calculate_type" class="calculate_type" value="currency" />
                        <input type="text" name="harga_beli_emas" class="harga_beli_emas" value="0" />
                      </div>
                      <p class="col_leTerm">Dengan mengeklik tombol di atas. Kamu sudah menyetujui <a href="<?php echo home_url(); ?>/emas/syarat-ketentuan" target="_blank">syarat dan ketentuan</a> yang berlaku.</p>
                    <?php } else { ?>
                      <p>
                        Akun kamu<br />
                        <span>belum teraktivasi<br />dengan baik</span> :(<br />
                        Segera hubungi tim kami ya!<br />
                        Agar kamu dapat mulai<br />menabung emas.
                      </p>
                    <?php } ?>
                  </div>
                </form>
                <p class="p_leJualNotif">Minimal pembelian emas sebesar <strong>Rp 5.000</strong> dan maksimal sebesar <strong>Rp 100.000.000</strong>.</p>
                <span class="loadingGldBeli" style="color:#FE5461">&nbsp;</span>
              </div>
              
              <div id="jualemas">
                <?php if(isset($_SESSION["alert_error_sell_emas"])) { ?>
                  <p style="color:#FE5461;">
                    <?php echo $_SESSION["alert_error_sell_emas"]; ?>
                  </p>
                <?php } ?>
                <form id="formJualemas" class="row" action="<?php echo home_url(); ?>/emas/jual" method="post">
                  <label class="col-md-4 col_leNilaiRp">
                    <p>Nilai dalam Rupiah</p>
                    <input type="text" autocomplete="off" name="value_currency" id="valueRpJual" class="input valueRpJual gldCal" placeholder="0"<?php if($profile->data->activated_user == 0) { ?> disabled<?php } ?> />
                  </label>
                  <label class="col-md-4 col_leNilaiGr">
                    <p>Jumlah Emas dalam gr</p>
                    <input type="text" autocomplete="off" name="value_gram" id="valueGrJual" class="input valueGrJual gldCal" placeholder="0"<?php if($profile->data->activated_user == 0) { ?> disabled<?php } ?> />
                  </label>
                  <div class="col-md-4 col_leBtnBeliEmas">
                    <img src="<?php bloginfo("template_directory"); ?>/library/images/emas/icon-lephone.svg" />
                    <?php if($profile->data->verified_user == 1 && $profile->data->activated_user == 1) { ?>
                      <p>Pastikan yang kamu<br />masukkan <span>sudah benar</span> ya!</p>
                      <p>
                        <button type="submit" class="jualemassekarang btn_gldFont LEBtn_rainbow" disabled>Lanjut</button>
                      </p>
                      <p class="col_leTerm">Dengan mengeklik tombol di atas. Kamu sudah menyetujui <a href="<?php echo home_url(); ?>/emas/syarat-ketentuan" target="_blank">syarat dan ketentuan</a> yang berlaku.</p>
                    <?php } elseif($profile->data->verified_user == 0 && $profile->data->activated_user == 0) { ?>
                      <p>
                        Akun kamu<br />
                        <span>belum teraktivasi<br />dengan baik</span> :(<br />
                        Segera hubungi tim kami ya!
                      </p>
                    <?php } else { ?>
                      <p>Akun kamu<br /><span>belum ter-verifikasi</span> :(<br />Tim kami akan<br />segera memverifikasi akunmu.</p>
                    <?php } ?>
                    <div class="d-none">
                      <input type="hidden" name="transaction_type" value="sell" />
                      <input type="text" name="calculate_type" class="calculate_type" value="currency" />
                      <input type="text" name="harga_jual_emas" class="harga_jual_emas" value="0" />
                    </div>
                  </div>
                </form>
                <p class="p_leJualNotif">Hasil penjualan akan dikirim ke <strong>Rekening Bank Terdaftar</strong>.</p>
                <span class="loadingGldJual" style="color:#FE5461">&nbsp;</span>
              </div>

            </div>
          </div>
        </div>
      </div>
    </div>
  </div>

  <div class="gldMobile box_mgldDashboard">
    <div class="col_mgldDashboard">
      <div class="col_mgldDashboardTitle">
        <div>
          <h3>Jual/Beli Emas</h3>
          <a href="<?php echo home_url(); ?>/emas/riwayat-transaksi">
            Riwayat Transaksi
          </a>
        </div>
      </div>

      <div id="mgldBalance" class="col_gldContent" style="display:block;">
        <div class="box_gldDetail">
          <div class="box_gldBalance">
            <img src="<?php bloginfo("template_directory"); ?>/library/images/emas/icon-gold.svg" />
            <div>
              <p class="wrap_tooltip">
                Nilai Emas Kamu
                <span class="tooltip_gldBalance">
                  <label>
                    Perhitungan nilai emas kamu berasal dari jumlah emas dalam gram dikali harga jual emas/gr saat ini.
                  </label>
                </span>
              </p>
              <h4 id="wrapmgldBalanceRp">Rp <span id="mgldBalanceRp"><?php echo number_format(round($profile->data->gold_balance_in_currency, 0), 0, ".", "."); ?></span></h4>
            </div>
          </div>
        </div>

        <div class="box_gldDetail">
          <div class="box_mgldRate">
            <p>Harga Beli Emas</p>
            <h4 id="wrapmgldRateBuy">
              Rp <span id="mgldRateBuy"><?php echo number_format(round($harga_emas->data->buy_price), 0, ".", "."); ?></span>/gr
            </h4>
          </div>
          <div class="box_mgldRate">
            <p>Harga Jual Emas</p>
            <h4 id="wrapmgldRateSell">
              Rp <span id="mgldRateSell"><?php echo number_format(round($harga_emas->data->sell_price), 0, ".", "."); ?></span>/gr
            </h4>
          </div>
        </div>

        <div class="box_leBgGold"></div>
      </div>

      <p class="box_mgldGr" style="display:block;">
        <?php $gold_balance = str_replace(".", ",", round($profile->data->gold_balance, 4, PHP_ROUND_HALF_UP)); ?>
        Jumlah Emas: <span id="mgldBalanceGr"><?php echo $gold_balance; ?></span> <span>gram</span>
      </p>

      <div id="mgldRateTabs" style="display:none;">
        <ul>
          <li class="beliemas"><a href="#mbeliemas">Beli Emas</a></li>
          <li class="jualemas"><a href="#mjualemas">Jual Emas</a></li>
        </ul>

        <div id="mbeliemas">
          <?php if(isset($_SESSION["alert_error_buy_emas"])) { ?>
            <p style="color:#FE5461;">
              <?php echo $_SESSION["alert_error_buy_emas"]; ?>
            </p>
          <?php } ?>
          <form id="mformBeliemas" action="<?php echo home_url(); ?>/emas/beli" method="post">
            <div class="row">
              <label class="col-sm-6 col_leNilaiRp">
                <p>Nilai dalam Rupiah</p>
                <input type="text" autocomplete="off" name="value_currency" id="mvalueRpBeli" class="input valueRpBeli gldCal" placeholder="0"<?php if($profile->data->activated_user == 0) { ?> disabled<?php } ?> />
              </label>
              <label class="col-sm-6 col_leNilaiGr">
                <p>Jumlah Emas dalam gr</p>
                <input type="text" autocomplete="off" name="value_gram" id="mvalueGrBeli" class="input valueGrBeli gldCal" placeholder="0"<?php if($profile->data->activated_user == 0) { ?> disabled<?php } ?> />
              </label>
            </div>
            <p class="p_leJualNotif">Minimal pembelian emas sebesar <strong>Rp 5.000</strong> dan maksimal sebesar <strong>Rp 100.000.000</strong>.</p>
            <span class="loadingGldBeli" style="color:#FE5461">&nbsp;</span>
            <div class="col_mgldDetailBeli" style="display:none;">
              <h4>Detail Transaksi</h4>
              <div>
                <p>
                  <span id="mgldTotalGr">0</span> gr emas
                </p>
                <p>
                  Rp <span id="mgldSubTotalRp">0</span>
                </p>
              </div>
              <div>
                <p>
                  Total
                </p>
                <p>
                  Rp <span id="mgldTotalRp">0</span>
                </p>
              </div>
            </div>
            <div class="col_leBtnBeliEmas">
              <?php if($profile->data->activated_user == 1) { ?>
                <p>
                  <button type="submit" class="mbeliemassekarang btn_gldFont LEBtn_rainbow" disabled>Lanjut</button>
                </p>
                <p class="col_leTerm">
                  Dengan mengeklik tombol di atas. Kamu sudah menyetujui 
                  <a href="<?php echo home_url(); ?>/emas/syarat-ketentuan" target="_blank">syarat dan ketentuan</a> yang berlaku.
                </p>
                <div class="d-none">
                  <input type="hidden" name="transaction_type" value="buy" />
                  <input type="text" name="calculate_type" class="calculate_type" value="currency" />
                  <input type="text" name="harga_beli_emas" class="harga_beli_emas" value="0" />
                </div>
              <?php } else { ?>
                <p>
                  Akun kamu 
                  <span>belum teraktivasi dengan baik</span> :(<br />
                  Segera hubungi tim kami ya!<br />
                  Agar kamu dapat mulai menabung emas.
                </p>
              <?php } ?>
            </div>
          </form>
        </div>
        
        <div id="mjualemas">
          <?php if(isset($_SESSION["alert_error_sell_emas"])) { ?>
            <p style="color:#FE5461;">
              <?php echo $_SESSION["alert_error_sell_emas"]; ?>
            </p>
          <?php } ?>
          <form id="formJualemas" action="<?php echo home_url(); ?>/emas/jual" method="post">
            <div class="row">
              <label class="col-sm-6 col_leNilaiRp">
                <p>Nilai dalam Rupiah</p>
                <input type="text" autocomplete="off" name="value_currency" id="mvalueRpJual" class="input valueRpJual gldCal" placeholder="0"<?php if($profile->data->activated_user == 0) { ?> disabled<?php } ?> />
              </label>
              <label class="col-sm-6 col_leNilaiGr">
                <p>Jumlah Emas dalam gr</p>
                <input type="text" autocomplete="off" name="value_gram" id="mvalueGrJual" class="input valueGrJual gldCal" placeholder="0"<?php if($profile->data->activated_user == 0) { ?> disabled<?php } ?> />
              </label>
            </div>
            <p class="p_leJualNotif">Hasil penjualan akan dikirim ke <strong>Rekening Bank Terdaftar</strong>.</p>
            <span class="loadingGldJual" style="color:#FE5461">&nbsp;</span>
            
            <div class="col_leBtnBeliEmas">
              <?php if($profile->data->verified_user == 1 && $profile->data->activated_user == 1) { ?>
                <p>
                  <button type="submit" class="mjualemassekarang btn_gldFont LEBtn_rainbow" disabled>Lanjut</button>
                </p>
                <p class="col_leTerm">
                  Dengan mengeklik tombol di atas. Kamu sudah menyetujui 
                  <a href="<?php echo home_url(); ?>/emas/syarat-ketentuan" target="_blank">syarat dan ketentuan</a> yang berlaku.
                </p>
              <?php } elseif($profile->data->verified_user == 0 && $profile->data->activated_user == 0) { ?>
                <p>
                  Akun kamu 
                  <span>belum teraktivasi dengan baik</span> :(<br />
                  Segera hubungi tim kami ya!
                </p>
              <?php } else { ?>
                <p>Akun kamu <span>belum ter-verifikasi</span> :(<br />Tim kami akan segera memverifikasi akunmu.</p>
              <?php } ?>
              <div class="d-none">
                <input type="hidden" name="transaction_type" value="sell" />
                <input type="text" name="calculate_type" class="calculate_type" value="currency" />
                <input type="text" name="harga_jual_emas" class="harga_jual_emas" value="0" />
              </div>
            </div>
          </form>
        </div>
      </div>

    </div>
  </div>
</div>
<?php /*js tabs ada di "wp-content\themes\ladara2020\ladara-emas\index-emas.php"*/ ?>