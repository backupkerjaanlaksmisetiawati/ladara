<?php
  $value_gram = str_replace(",", ".", $_POST["value_gram"]);
  $value_currency = str_replace(".", "", $_POST["value_currency"]);

  $bank = gld_bank(array("type"=>"xen"));
  $bank = json_decode($bank);
?>

<div class="metodePembayaranGld" style="display:none">
  <h4>Metode Pembayaran</h4>
  <div class="bx_metodePembayaranEmas">
    <div class="metodePembayaranGldVa" style="display:none">
      <label class="lbl_radio">
        <input type="radio" class="radio_pembayaran_emas" name="type_payment" value="va" />
        <span class="checkmark"></span>
        Transfer Melalui Virtual Account
        
        <div class="content">
          <select class="dropDownVaEmas" name="va" style="display:none">
            <?php foreach($bank->data as $b) { ?>
              <option value="<?php echo $b->code; ?>"><?php echo $b->name; ?></option>
            <?php } ?>
          </select>

          <div class="selectedVaEmas selectedPembayaranEmas"></div>

          <p class="ketentuan">
            Setelah kamu bayar, pesanan kamu akan langsung diproses
            <br />
            Pastikan kamu memilih penyedia jasa Ladara Indonesia
            <br />
            Kamu diberikan waktu maksimal 1 jam untuk melakukan pembayaran, terhitung setelah kamu melakukan proses checkout
          </p>
        </div>
      </label>
    </div>

    <div class="metodePembayaranGldCc" style="display:none">
      <label class="lbl_radio">
        <input type="radio" class="radio_pembayaran_emas" name="type_payment" value="cc" <?php echo($value_currency < 10000)?"disabled":""; ?> />
        <span class="checkmark"></span>
        Kartu Kredit
        
        <div class="content">
          <div class="ketentuan">
            <p>Selesaikan pembayaran dalam waktu 1 jam untuk menghindari pembatalan transaksi secara otomatis.</p>
            <div class="ccList">
              <img src="<?php bloginfo("template_directory"); ?>/library/images/icon_visa.png">
              <img src="<?php bloginfo("template_directory"); ?>/library/images/icon_mastercard.png">
              <img src="<?php bloginfo("template_directory"); ?>/library/images/icon_jcb.png">
            </div>
          </div>
        </div>

        <?php if($value_currency < 10000) { ?>
          <p class="error_beliemas_cc">
            Kamu tidak bisa melakukan pembayaran dengan Kartu Kredit jika membeli emas dibawah Rp 10.000
          </p>
        <?php } ?>
      </label>
    </div>
    
    <div class="metodePembayaranGldEwallet" style="display:none">
      <label class="lbl_radio">
        <input type="radio" class="radio_pembayaran_emas" name="type_payment" value="ewallet" />
        <span class="checkmark"></span>
        Pembayaran Instan
        
        <div class="content">
          <select class="dropDownEwalletEmas" name="ewallet" style="display:none;">
            <option value="DANA" data-image="<?php bloginfo("template_directory"); ?>/library/images/dana_wallet.png">DANA</option>
            <option value="LINKAJA" data-image="<?php bloginfo("template_directory"); ?>/library/images/linkaja_wallet.png">LINKAJA</option>
            <option value="OVO" data-image="<?php bloginfo("template_directory"); ?>/library/images/ovo_wallet.png">OVO</option>
          </select>

          <div class="gld_ewalletPhone" style="display:none;">
            <span>
              <strong>Masukan No HP</strong>
            </span>
            <p>
              <span>+62</span>
              <input type="number" class="ewallet_phone" name="ewallet_phone" min="1" />
            </p>
          </div>

          <div class="selectedEwalletEmas selectedPembayaranEmas"></div>

          <p class="ketentuan">
            Setelah kamu bayar, pesanan kamu akan langsung diproses
            <br />
            Pastikan kamu memilih penyedia jasa Ladara Indonesia
            <br />
            Kamu diberikan waktu maksimal 1 jam untuk melakukan pembayaran, terhitung setelah kamu melakukan proses checkout
          </p>
        </div>
      </label>
    </div>
    
    <div class="metodePembayaranGldRetail" style="display:none">
      <label class="lbl_radio">
        <input type="radio" class="radio_pembayaran_emas" name="type_payment" value="retail" />
        <span class="checkmark"></span>
        Tunai di Gerai Retail
        
        <div class="content">
          <select class="dropDownRetailEmas" name="retail" style="display:none;">
            <option value="ALFAMART">Alfamart</option>
            <option value="INDOMARET">Indomaret</option>
          </select>

          <div class="selectedRetailEmas selectedPembayaranEmas"></div>

          <p class="ketentuan">
            Setelah kamu bayar, pesanan kamu akan langsung diproses
            <br />
            Pastikan kamu memilih penyedia jasa Ladara Indonesia
            <br />
            Kamu diberikan waktu maksimal 1 jam untuk melakukan pembayaran, terhitung setelah kamu melakukan proses checkout
          </p>
        </div>
      </label>
    </div>
  </div>
</div>