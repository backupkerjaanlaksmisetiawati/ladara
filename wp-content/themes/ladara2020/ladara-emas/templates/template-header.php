<!doctype html>

<!--[if lt IE 7]><html <?php language_attributes(); ?> class="no-js lt-ie9 lt-ie8 lt-ie7"><![endif]-->
<!--[if (IE 7)&!(IEMobile)]><html <?php language_attributes(); ?> class="no-js lt-ie9 lt-ie8"><![endif]-->
<!--[if (IE 8)&!(IEMobile)]><html <?php language_attributes(); ?> class="no-js lt-ie9"><![endif]-->
<!--[if gt IE 8]><!--> <html <?php language_attributes(); ?> class="no-js"><!--<![endif]-->

<head>
	<meta charset="utf-8">

	<?php // Google Chrome Frame for IE ?>
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">

	<title><?php wp_title(''); ?></title>

	<?php // mobile meta (hooray!) ?>
	<meta name="HandheldFriendly" content="True">
	<meta name="MobileOptimized" content="320">
	<meta name="viewport" content="width=device-width, initial-scale=1.0"/>

	<?php // icons & favicons (for more: http://www.jonathantneal.com/blog/understand-the-favicon/) ?>
	<link rel="apple-touch-icon" href="<?php echo get_template_directory_uri(); ?>/library/images/apple-icon-touch.png">
	<link rel="icon" href="<?php echo get_template_directory_uri(); ?>/favicon.png">
	<!--[if IE]>
		<link rel="shortcut icon" href="<?php echo get_template_directory_uri(); ?>/favicon.ico">
	<![endif]-->
	<?php // or, set /favicon.ico for IE10 win ?>
	<meta name="msapplication-TileColor" content="#f01d4f">
	<meta name="msapplication-TileImage" content="<?php echo get_template_directory_uri(); ?>/library/images/win8-tile-icon.png">

	<link rel="pingback" href="<?php bloginfo('pingback_url'); ?>">
	
	<?php // Put you url link font here....  ?>
	<link href="https://fonts.googleapis.com/css?family=Montserrat:300,400,400i,500,600,700&display=swap" rel="stylesheet">
	<script src="https://kit.fontawesome.com/45b63cdda3.js" crossorigin="anonymous"></script>
	<?php // Insert your css & jss only at function.php ?>

	<?php // wordpress head functions ?>
	<?php wp_head(); ?>
	<?php // end of wordpress head ?>

	<?php // drop Google Analytics Here ?>


	<?php // end analytics ?>

	<?php /*
	<script type="text/javascript"> 
	history.pushState(null, null, location.href);
	window.onpopstate = function () {
		history.go(0);
	};
	window.history.pushState(null, "", window.location.href);
	window.onpopstate = function () {
		window.history.pushState(null, "", window.location.href);
	};
	</script>
	*/ ?>

</head>

<?php
	if (isset($_GET['mobile']) and $_GET['mobile'] != '') {
		$yes_mobile = 1;
	} else {
		$yes_mobile = 0;
	}
?>

<?php if ($yes_mobile == 1) { ?>
	<style>
		.wa__btn_popup,
		.wa__popup_chat_box {
			display: none !important;
		}
	</style>
<?php } ?>

<style>
	.wa__btn_popup {
		bottom: 30px !important;
	}
</style>

<body id="body" <?php body_class(); ?> onload="afterGldPageLoad()" itemscope itemtype="http://schema.org/WebPage" data-hurl="<?php echo home_url(); ?>/emas" >

<?php 
	$user_id = get_current_user_id();
	$current_page = $wp_query->posts[0];
	$check_user_emas = gld_check_user_emas(array("user_id"=>$user_id));
	$check_user_emas = json_decode($check_user_emas);
?>

<?php if($check_user_emas->code === 200 && ($current_page->post_name === "emas" || $current_page->post_name === "riwayat-transaksi")) { ?>
	<div id="gldLoading">
		<?php /*<img src="<?php bloginfo("template_directory"); ?>/library/images/emas/icon-loading.svg" />*/ ?>
	</div>
<?php } ?>

<div id="container" class="ladaraEmas">
	<?php if ($yes_mobile == 0) { ?>
		<header class="header" role="banner" <?php /*if($current_page->post_name === "processing") { ?>style="display:none"<?php }*/ ?>>
			<div class="gldDesktop">
				<div class="row bx_head_top">
					<div class="col-sm-6 col-md-6 col-lg-6 col_head_top">
						<ul class="ul_head_top ul_left" style="display:none;">
							<?php /*<li><a href="javascript:;">Unduh Aplikasi LaDaRa</a></li>
							<li><a href="javascript:;">Promo Terbaru</a></li>*/ ?>
							<li><a>&nbsp;</a></li>
						</ul>
					</div>
					<div class="col-sm-6 col-md-6 col-lg-6 col_head_top">
						<ul class="ul_head_top ul_right" style="height:21px;">
							<?php /*<li>
								<a href="javascript:;">Pusat Bantuan <span class="glyphicon glyphicon-menu-down"></span></a>
							</li>*/ ?>
							<li><a>&nbsp;</a></li>
						</ul>
					</div>
				</div>

				<div class="row bx_head_middle gldHeader">
					<div class="box_gldHeader">
						<a href="<?php echo home_url(); ?>" title="Home - Ladara Indonesia">
							<div class="img_gldLogo animated jello">
								<img src="<?php bloginfo('template_directory'); ?>/library/images/main_logo.png">
							</div>
						</a>
						<div id="open_topmenu" class="ht_ladaraEmas">Ladara Emas</div>
					</div>
				</div>
			</div>

			<div class="gldMobile">
				<div class="bx_head_middle gldHeader">
					<div class="box_mGldHeader">
						<h3>
							<?php if($current_page->post_name === "emas") { ?>
								<?php if($check_user_emas->code !== 200) { ?> 
									<a href="<?php echo home_url(); ?>">
										Ladara Emas
									</a>
								<?php } else { ?>
									<a href="<?php echo home_url(); ?>">
										Dashboard Emas
									</a>
								<?php } ?>
							<?php } else if($current_page->post_name === "beli") { ?>
								<a href="<?php echo home_url(); ?>/emas">
									Checkout Beli Emas
								</a>
							<?php } else if($current_page->post_name === "jual") { ?>
								<a href="<?php echo home_url(); ?>/emas">
									Jual Emas
								</a>
							<?php } else if($current_page->post_name === "invoice") { ?>
								<?php if(isset($_SESSION["buying_type_payment"])) { ?>
									<?php if($_SESSION["buying_type_payment"] === "va") { ?>
									<a href="<?php echo home_url(); ?>/emas">
									<?php } else { ?>
									<a onclick="gldAlertBack('showpopup')">
									<?php } ?>
									<?php unset($_SESSION["buying_type_payment"]); ?>
								<?php } else { ?>
								<a href="<?php echo home_url(); ?>/emas">
								<?php } ?>
									Invoice Beli Emas
								</a>
							<?php } else if($current_page->post_name === "faktur") { ?>
								<a href="<?php echo home_url(); ?>/emas">
									Faktur Jual Emas
								</a>
							<?php } else if($current_page->post_name === "riwayat-transaksi") { ?>
								<a href="<?php echo home_url(); ?>/emas">
									Riwayat Transaksi
								</a>
							<?php } else if($current_page->post_name === "transaksi-detail") { ?>
								<a href="<?php echo home_url(); ?>/emas">
									Detail Transaksi
								</a>
							<?php } else if($current_page->post_name === "aktivasi") { ?>
								<a href="<?php echo home_url(); ?>/emas">
									Aktivasi Akun
								</a>
							<?php } else if($current_page->post_name === "selesai") { ?>
								<a href="<?php echo home_url(); ?>/emas">
									Aktivasi Akun Selesai
								</a>
							<?php } else if($current_page->post_name === "processing") { ?>
								<a>
									Ladara Emas
								</a>
							<?php } else { ?>
								<a href="<?php echo home_url(); ?>/emas">
									Ladara Emas
								</a>
							<?php } ?>
						</h3>

						<div id="btnGldMenu" class="dots">
							<span></span>
							<span></span>
							<span></span>
						</div>
					</div>
				</div>

				<div class="gldMenu">
					<a href="<?php echo home_url(); ?>">
						<div>
							<img src="<?php bloginfo('template_directory'); ?>/library/images/emas/icon-mhome.svg">
						</div>
						<span>Beranda</span>
					</a>
					<?php /*<a href="<?php echo home_url(); ?>">
						<div>
							<img src="<?php bloginfo('template_directory'); ?>/library/images/emas/icon-mlist.svg">
						</div>
						<span>Kategori</span>
					</a>*/ ?>
					<a href="<?php echo home_url(); ?>/cart">
						<div>
							<img src="<?php bloginfo('template_directory'); ?>/library/images/emas/icon-mcart.svg">
						</div>
						<span>Keranjang</span>
					</a>
					<a href="<?php echo home_url(); ?>/notifikasi">
						<div>
							<img src="<?php bloginfo('template_directory'); ?>/library/images/emas/icon-mbell.svg">
						</div>
						<span>Notifikasi</span>
					</a>
					<a href="<?php echo home_url(); ?>/profile">
						<div>
							<img src="<?php bloginfo('template_directory'); ?>/library/images/emas/icon-maccount.svg">
						</div>
						<span>Akun</span>
					</a>
				</div>
			</div>
		</header>
	<?php } ?>

<?php // next to body and footer.php ?>