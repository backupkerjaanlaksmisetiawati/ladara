  <?php 
    if (isset($_GET['mobile']) AND $_GET['mobile'] != '') {
      $yes_mobile = 1;
    }else{
      $yes_mobile = 0;
    }   

    $home_id = 2;
    $cs_phone = get_field('cs_phone',$home_id);
    $cs_email = get_field('cs_email',$home_id);
  ?>

  <?php if ($yes_mobile == 0):?>
    <footer class="footer" role="contentinfo">
      <div id="inner-footer" class="wrap clearfix">
        <div class="row row_footer_menu">
          <div class="col-xs-6 col-md-3 col_ft_mn">
            <h2 class="ht_sub_ft_mn">Customer Care</h2>
            <ul class="ul_ft_mn">
              <?php if(isset($cs_phone) AND $cs_phone != ''){ ?>
                <li>
                  <a href="tel:<?php echo $cs_phone; ?>">
                    <span class="glyphicon glyphicon-earphone"></span> <?php echo $cs_phone; ?>
                  </a>
                </li>
              <?php } ?>
              <?php if(isset($cs_email) AND $cs_email != ''){ ?>
                <li>
                  <a href="mailto:<?php echo $cs_email; ?>">
                    <span class="glyphicon glyphicon-envelope"></span> <?php echo $cs_email; ?>
                  </a>
                </li>
              <?php } ?>
            </ul>
          </div>
          <div class="col-xs-6 col-md-3 col_ft_mn">
            <h2 class="ht_sub_ft_mn">Info</h2>
            <ul class="ul_ft_mn">
              <?php
                $menu_id = 98;
                $menu_list='';
                $menu_items = wp_get_nav_menu_items($menu_id);
                foreach ( $menu_items as $key => $menu_item ) {
                  $title = $menu_item->title;
                  $url = $menu_item->url;
                  $menu_list .= '<li><a href="'.$url.'" title="view detail '.$title.'">'.$title.'</a></li>';
                }
                echo $menu_list;
              ?>
            </ul>
          </div>
          <div class="col-xs-6 col-md-3 col_ft_mn">
            <h2 class="ht_sub_ft_mn">Bantuan</h2>
            <ul class="ul_ft_mn">
              <?php
                $menu_id = 99;
                $menu_list='';
                $menu_items = wp_get_nav_menu_items($menu_id);
                foreach ( $menu_items as $key => $menu_item ) {
                  $title = $menu_item->title;
                  $url = $menu_item->url;
                  $menu_list .= '<li><a href="'.$url.'" title="view detail '.$title.'">'.$title.'</a></li>';
                }
                echo $menu_list;
              ?>
            </ul>
          </div>
          <div class="col-xs-6 col-md-3 col_ft_mn">
            <h2 class="ht_sub_ft_mn">Ikuti Kami</h2>
            <ul class="ul_ft_mn_social">
              <?php
                $get_fb = get_field('footer_facebook',2);
                $get_twitter = get_field('footer_twitter',2);
                $get_instagram = get_field('footer_instagram',2);
                $get_whatsapp = get_field('footer_whatsapp',2);
              ?>
              <li class="ss_fb"><a href="<?php echo $get_fb; ?>" title="Follow us"><i class="fab fa-facebook-f"></i></a></li>
              <li class="ss_twitter"><a href="<?php echo $get_twitter; ?>" title="Follow us"><i class="fab fa-twitter"></i></a></li>
              <li class="ss_instagram"><a href="<?php echo $get_instagram; ?>" title="Follow us"><i class="fab fa-instagram"></i></a></li>
              <li class="ss_wa"><a href="<?php echo $get_whatsapp; ?>" title="Follow us"><i class="fab fa-whatsapp"></i></a></li>
            </ul>
          </div>
        </div>
        <div class="row row_footer_menu_in no_bor des_display">
          <div class="col-md-4 col_ft_mn">
            <h2 class="ht_sub_ft_mn">KEAMANAN BERBELANJA</h2>
            <div class="mg_ft_mn">
              <img src="<?php bloginfo('template_directory'); ?>/library/images/icon_visa.png">
              <img src="<?php bloginfo('template_directory'); ?>/library/images/icon_mastercard.png">
              <img src="<?php bloginfo('template_directory'); ?>/library/images/icon_jcb.png">
            </div>
          </div>
          <div class="col-md-4 col_ft_mn">
            <h2 class="ht_sub_ft_mn">METODE PEMBAYARAN</h2>
            <div class="mg_ft_mn">
              <?php /*
              <img src="<?php bloginfo('template_directory'); ?>/library/images/icon_bca.png">
              */ ?>
              <img src="<?php bloginfo('template_directory'); ?>/library/images/icon_bni.png">
              <img src="<?php bloginfo('template_directory'); ?>/library/images/icon_mandiri.png">
              <img src="<?php bloginfo('template_directory'); ?>/library/images/icon_permata.png">
              <img src="<?php bloginfo('template_directory'); ?>/library/images/icon_bri.png">
            </div>
          </div>
          <div class="col-md-4 col_ft_mn">
            <h2 class="ht_sub_ft_mn">JASA PENGIRIMAN</h2>
            <div class="mg_ft_mn">
              <img src="<?php bloginfo('template_directory'); ?>/library/images/icon_jne.png">
            </div>
          </div>
        </div>
        <?php if($post->ID == 2){ // 2 = homepage?>
          <div class="row row_footer_menu_in des_display">
            <div class="col-md-12 col_ft_mn">
              <?php /*
              <h3 class="ft_mn">Toko Online Terlengkap & Terpercaya</h3>
              <h1 class="ht_mn">LADARA, Toko Online Terlengkap & Terpercaya Dengan Beragam Produk Pilihan Serta Promo Menarik</h1>
              */ ?>
              <div class="tx_mn">
                <?php
                  $home_id = 2;
                  echo apply_filters('the_content', get_post_field('post_content', $home_id));
                ?>
              </div>
            </div>
          </div>
        <?php } ?>
        <div class="row row_footer_copyright">
          <div class="col-md-12 col_ft_mn">
            <div class="tx_copyright">
              &copy;<?php echo date("Y"); ?> Ladara Indonesia
            </div>
          </div>
        </div>
      </div>
    </footer>
  <?php endif;?>
</div> <?php // end of div container ?>

<?php wp_footer(); ?>

<script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/jquery.lazy/1.7.9/jquery.lazy.min.js"></script>
<script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/jquery.lazy/1.7.9/jquery.lazy.plugins.min.js"></script>

<?php 
  $user_id = get_current_user_id();
  $current_page = $wp_query->posts[0];
  $check_user_emas = gld_check_user_emas(array("user_id"=>$user_id));
  $check_user_emas = json_decode($check_user_emas);
?>

<?php if($current_page->post_name === "emas") { ?>
  <?php if($check_user_emas->code === 200) { ?>
    <script type="text/javascript">
      $(document).ready(function () {
        if ($("#gldRateTabs").length > 0) { 
          $("#gldRateTabs").tabs({ active: <?php echo (isset($_SESSION["alert_error_sell_emas"])) ? "1" : "0"; ?> });
        }
      });
    </script>
    <script type="text/javascript">
      $(document).ready(function () {
        if ($("#mgldRateTabs").length > 0) { 
          $("#mgldRateTabs").tabs({ active: <?php echo (isset($_SESSION["alert_error_sell_emas"])) ? "1" : "0"; ?> });
        }
      });
    </script>
  <?php } ?>
  <?php
    if(isset($_SESSION["alert_error_sell_emas"])) {
      unset($_SESSION["alert_error_sell_emas"]);
    }
    if(isset($_SESSION["alert_error_buy_emas"])) {
      unset($_SESSION["alert_error_buy_emas"]);
    }
  ?>
<?php } ?>

</body>
</html>