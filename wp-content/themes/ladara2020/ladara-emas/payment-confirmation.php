<?php
/*Template Name: Ladara Emas - Beli - Payment Confirmation*/

use Ladara\Helpers\builder\OrderBuilder;
use Ladara\Helpers\builder\PaymentBuilder;
use Ladara\Helpers\builder\ShippingBuilder;
use Ladara\Helpers\builder\UsersBuilder;
use Ladara\Helpers\email\EmailHelper;
use Ladara\Models\Notifications;
use Ladara\Models\OrderModel;
?>

<?php get_template_part( "ladara-emas/templates/template", "header" ); ?>

<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
<style type="text/css">
  .header, .footer{
    display: none !important;
  }
</style>
<?php
global $wpdb;
$inputData = json_decode(file_get_contents('php://input'), true);
$wpdb->query("INSERT INTO ldr_payment_emas_test (payment) VALUES ('" . json_encode($inputData) . "')");
ddbug($inputData);
// echo "<pre>";
// print_r($inputData);
// echo "</pre>";
?>

<?php /*
<?php
if (isset($inputData) and $inputData != '' and !empty($inputData)) {
    $xendit_id = $inputData['id'];
    $external_id = $inputData['external_id'];
    $payment_method = $inputData['payment_method'];
    $status = $inputData['status'];
    $paid_amount = $inputData['paid_amount'];
    $bank_code = $inputData['bank_code'];
    $paid_at = $inputData['paid_at'];


    if ($xendit_id != '') {
        global $wpdb;
        $wpdb_query = "SELECT * FROM ldr_payment_log WHERE xendit_id = '$xendit_id' ";
        $res_query = $wpdb->get_results($wpdb_query, OBJECT);
        $count_res = count($res_query);

        if ($count_res > 0) { // if exist
            foreach ($res_query as $key => $value) {
                $order_id = $value->order_id;
                $order_type = $value->type;
            }

            global $wpdb;
            $wpdb_query_update = " UPDATE ldr_payment_log
                          SET payment_method='$payment_method', status='$status', bank_code='$bank_code', paid_at='$paid_at'
                          WHERE xendit_id = '$xendit_id' ";
            $res_query_update = $wpdb->query($wpdb_query_update);
        } else {
            if ($_SERVER['SERVER_NAME'] == 'staging.ladara.id') {
                $url = 'https://testing.ladara.id/payment-confirmation';
                $ch = curl_init($url);
                $payload = json_encode($inputData);
                curl_setopt($ch, CURLOPT_POSTFIELDS, $payload);
                curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type:application/json'));
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                $result = curl_exec($ch);
                curl_close($ch);
            }
        }

        // =============== IF PAYMENT FOR PRODUCT =====================
        if (isset($order_type) and strtolower($order_type) == 'product') {
            // query save & update product order

            if (strtolower($status) == 'paid' or strtolower($status) == 'settled') { // jika status = paid / lunas
                $valid = 'accept';

                // if payment sukses update status
                $order = new WC_Order($order_id);

                if (isset($order) and $order != '' and !empty($order)) {
                    $order->update_status('processing');

                    global $wpdb;
                    $wpdb_query_update = " UPDATE ldr_orders
                                  SET order_status = 'wc-processing'
                                  WHERE order_id = '$order_id'
                                ";
                    $res_query_update = $wpdb->query($wpdb_query_update);

                    $qOrderDetail = "SELECT * FROM ldr_orders o
                                LEFT JOIN ldr_users u ON u.ID = o.user_id
                                  WHERE o.order_id = '$order_id'";
                    $orderDetail = $wpdb->get_row($qOrderDetail, OBJECT);

                    //Kirim email
                    $userBuilder = new UsersBuilder($orderDetail->display_name, $orderDetail->user_email);

                    $paymentBuilder = (new PaymentBuilder())
                        ->setBank($payment_method);

                    $items = [];
                    foreach ($order->get_items() as $item) {
                        $data = $item->get_data();
                        $items[] = [
                            'name' => $item->get_name(),
                            'qty' => $item->get_quantity(),
                            'price' => $data['total']
                        ];
                    }
                    $shipping = $orderDetail->courier_name;
                    preg_match("/\((.*?)\)/", $shipping, $service);
                    $courierName = preg_replace("/\((.*?)\)/", '', $shipping);
                    $shippingBuilder = new ShippingBuilder($courierName ?? '', $service[1], $orderDetail->shipping_price);


                    $orderBuilder = (new OrderBuilder())
                        ->setDateCreated($orderDetail->created_date)
                        ->setTotalPrice($paid_amount)
                        ->setItems($items)
                        ->setStatus('Sudah Diverifikasi')
                        ->setInvoiceNo(OrderModel::invoice($order->get_date_created(), $order->get_id()))
                        ->setPaymentDate($paid_at);

                    $email = (new EmailHelper($userBuilder))
                        ->pembayaranBerhasil($orderBuilder, $paymentBuilder, $shippingBuilder);
                    //END kirim email

                    // ==== insert to order log
                    date_default_timezone_set("Asia/Jakarta");
                    $nowdate = date('Y-m-d H:i:s');
                    $order_status = 'processing';
                    $log_text = 'Pembayaran diterima Order ID #'.$order_id.', Status menjadi processing';
                    
                    $query_log = "INSERT INTO ldr_order_log(order_id,log,created_date,order_status) VALUES('$order_id','$log_text','$nowdate','$order_status')";
                    $result_log = $wpdb->query($query_log);


                    $user_id = $order->get_user_id(); // or $order->get_customer_id();
                    // =========== insert notification ==============
                    $orderId_notif = $order_id;
                    $type_notif = 'pesanan';
                    $title_notif = 'Pembayaran '.$orderId_notif.' di konfirmasi.';
                    $desc_notif = 'Pembayaran Anda sudah di konfirmasi, kami akan segera menyiapkan pesanan Anda.';
                    $data = [
                        'userId' => $user_id, //wajib
                        'title' => $title_notif, //wajib
                        'descriptions' => $desc_notif, //wajib
                        'type' => $type_notif, //pesanan, info, pembayaran, emas // wajib
                        'orderId' => $orderId_notif, // optional
                        'data' => [] //array optional bisa diisi dengan data lainnya
                    ];
                    $addNotif = Notifications::addNotification($data);
                    // =========== insert notification ==============
                }
            } elseif (strtolower($status) == 'pending') { // jika status = pending / on-hold
                $valid = 'pending';

                // if payment sukses update status
                $order = new WC_Order($order_id);
                if (isset($order) and $order != '' and !empty($order)) {
                    $order->update_status('on-hold');

                    global $wpdb;
                    $wpdb_query_update = " UPDATE ldr_orders
                                  SET order_status = 'wc-on-hold'
                                  WHERE order_id = '$order_id'
                                ";
                    $res_query_update = $wpdb->query($wpdb_query_update);

                    // ==== insert to order log
                    date_default_timezone_set("Asia/Jakarta");
                    $nowdate = date('Y-m-d H:i:s');
                    $order_status = 'on-hold';
                    $log_text = 'Pembayaran sedang di cek Order ID #'.$order_id.', Status menjadi on-hold';
                    
                    $query_log = "INSERT INTO ldr_order_log(order_id,log,created_date,order_status) VALUES('$order_id','$log_text','$nowdate','$order_status')";
                    $result_log = $wpdb->query($query_log);
                }
            } else {
                $valid = 'cancel';

                // if payment sukses update status
                $order = new WC_Order($order_id);
                if (isset($order) and $order != '' and !empty($order)) {
                    $order->update_status('cancelled');

                    global $wpdb;
                    $wpdb_query_update = " UPDATE ldr_orders
                                  SET order_status = 'wc-cancelled'
                                  WHERE order_id = '$order_id'
                                ";
                    $res_query_update = $wpdb->query($wpdb_query_update);

                    $qOrderDetail = "SELECT * FROM ldr_orders o
                                LEFT JOIN ldr_users u ON u.ID = o.user_id
                                  WHERE o.order_id = '$order_id'";
                    $orderDetail = $wpdb->get_row($qOrderDetail, OBJECT);

                    //Kirim email
                    $userBuilder = new UsersBuilder($orderDetail->display_name, $orderDetail->user_email);

                    $paymentBuilder = (new PaymentBuilder())
                        ->setBank($payment_method);

                    $items = [];
                    foreach ($order->get_items() as $item) {
                        $data = $item->get_data();
                        $items[] = [
                            'name' => $item->get_name(),
                            'qty' => $item->get_quantity(),
                            'price' => $data['total']
                        ];
                    }
                    $shipping = $orderDetail->courier_name;
                    preg_match("/\((.*?)\)/", $shipping, $service);
                    $courierName = preg_replace("/\((.*?)\)/", '', $shipping);
                    $shippingBuilder = new ShippingBuilder($courierName ?? '', $service[1], $orderDetail->shipping_price);
                    
                    $orderBuilder = (new OrderBuilder())
                        ->setDateCreated($orderDetail->created_date)
                        ->setTotalPrice($paid_amount)
                        ->setItems($items)
                        ->setStatus('Pembayaran Dibatalkan')
                        ->setInvoiceNo(OrderModel::invoice($order->get_date_created(), $order->get_id()))
                        ->setPaymentDate($paid_at);

                    $email = (new EmailHelper($userBuilder))
                        ->batalTransaksi($orderBuilder, $paymentBuilder, $shippingBuilder);
                    //END kirim email

                    // ==== insert to order log
                    date_default_timezone_set("Asia/Jakarta");
                    $nowdate = date('Y-m-d H:i:s');
                    $order_status = 'cancelled';
                    $log_text = 'Pembayaran di tolak, Order ID #'.$order_id.', Status menjadi cancelled';
                    
                    $query_log = "INSERT INTO ldr_order_log(order_id,log,created_date,order_status) VALUES('$order_id','$log_text','$nowdate','$order_status')";
                    $result_log = $wpdb->query($query_log);
                }
            }

            // =============== IF PAYMENT FOR LADARA EMAS =====================
        } elseif (isset($order_type) and strtolower($order_type) == 'emas') {
            // query BELI EMAS sukses payment order

            $transaksi = $wpdb->get_row(
                "SELECT * FROM ldr_transaksi_emas WHERE xendit_id='$xendit_id'",
                OBJECT
            );
                        
            $gld_payment_datas = [
                "status" => $status,
                "xendit_id" => $xendit_id
            ];
            gld_payment($gld_payment_datas);
        }
    }
}
?>

<div id="content">

    <div id="inner-content" class="wrap clearfix">

        <div id="main" class="eightcol first clearfix" role="main">

            <article id="post-not-found" class="hentry clearfix">
                            <div class="row row_finishCheckout">
                                <div class="col-md-12 col_finishCheckout">
                                    <a href="<?php echo home_url(); ?>">
                                        <div class="bx_backShop">
                                            <span class="glyphicon glyphicon-menu-left"></span> Kembali ke Home
                                        </div>
                                    </a>
                                    <div class="bx_finishCheckout">
                                        <div class="mg_registerIcon">
                                            <img class="lazy" data-src="<?php bloginfo('template_directory'); ?>/library/images/ico_404.jpg">
                                        </div>
                                        <div class="ht_register">Ups, halaman yang kamu cari belum bisa ditemukan :(</div>

                                        <div class="ht_sucs_register">
                                            Coba lagi dengan menekan tombol dibawah ini ya!
                                        </div>
                                        <div class="bx_def_checkout">
                                            <a href="<?php echo home_url(); ?>/">
                                                <button class="btn_def_checkout">Kembali ke Beranda</button>
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </div>
            </article>

        </div>

    </div>

</div>
*/ ?>

<?php endwhile; ?>
<?php else : ?>
        <?php get_template_part('content', '404pages'); ?>    
<?php endif; ?>

<?php get_template_part( "ladara-emas/templates/template", "footer" ); ?>