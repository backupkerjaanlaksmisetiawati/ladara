<?php /* Template Name: Ladara Emas - Beli - Processing */ ?>

<?php if (have_posts()) : while (have_posts()) : the_post(); ?>

<?php get_template_part( "ladara-emas/templates/template", "header" ); ?>

<?php
  $gldPage = home_url() . "/emas";
  $check_user_emas = gld_check_user_emas(array("user_id"=>get_current_user_id()));
  $check_user_emas = json_decode($check_user_emas);
?>

<?php if($check_user_emas->code !== 200) { ?>
  <style>
    header, body {
      display: none;
    }
  </style>
  <script type="application/javascript">
    setTimeout(function(){
      location.replace("<?php echo $gldPage; ?>"); 
    }, "fast");
  </script>
<?php } ?>

<div id="LadaraEmas" class="row_homePage">
  <div class="gldDesktop">
    <div class="row">
      <div class="col-md-12">
        <div id="wrap_aktivasiLE">
          <div class="bx_gldBreadcrumb bx_breadcrumbAktivasiLE">
            <div>&nbsp;</div>
          </div>
          <div class="col_gldContent">
            <div class="row row_LEPlus row_gldFinishedActivation">
              <div class="cols upload col-md-12">
                <div class="imgProccessingTransaksi"></div>
                <h3>Tunggu sebentar, ya...</h3>
                <p>Pembelian emasmu sedang diproses.</p>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="gldMobile box_mgldDashboard">
    <div class="col-md-12">          
        <div class="col_gldContent">
          <div class="row row_LEPlus row_gldFinishedActivation">
            <div class="cols upload col-md-12">
              <div class="imgProccessingTransaksi"></div>
              <h3>Tunggu sebentar, ya...</h3>
              <p>Pembelian emasmu sedang diproses.</p>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

<?php /** user must not access this page directly */ if($_POST["value_gram"] && $_POST["value_currency"] && isset($_POST["type_payment"])) { ?>
  <?php
    $va = "BNI";
    if(isset($_POST['va']) && isset($_POST["type_payment"]) && $_POST["type_payment"] === 'va') {
      if($_POST['va'] === "BNI") {
        $va = "BNI";
      } elseif($_POST['va'] === "MANDIRI") {
        $va = "BMRI";
      } elseif($_POST['va'] === "BCA") {
        $va = "BCA";
      } elseif($_POST['va'] === "BRI") {
        $va = "BRIN";
      } elseif($_POST['va'] === "PERMATA") {
        $va = "BBBA";
      };
    }
    if(isset($_POST['mva']) && isset($_POST["mtype_payment"]) && $_POST["mtype_payment"] === 'va') {
      if($_POST['mva'] === "BNI") {
        $va = "BNI";
      } elseif($_POST['mva'] === "MANDIRI") {
        $va = "BMRI";
      } elseif($_POST['mva'] === "BCA") {
        $va = "BCA";
      } elseif($_POST['mva'] === "BRI") {
        $va = "BRIN";
      } elseif($_POST['mva'] === "PERMATA") {
        $va = "BBBA";
      };
    }

    $data_beli_emas = array(
      "amount"        => 0,
      "amount_type"   => $_POST["calculate_type"],
      "bank_trs"      => $va,
      "type_payment"  => "",
      "latitude"      => "",
      "longitude"     => ""
    );

    if(isset($_POST["type_payment"])) {
      $data_beli_emas["type_payment"] = $_POST["type_payment"];
      $_SESSION["buying_type_payment"] = $_POST["type_payment"];
    }

    if($_POST["calculate_type"] == "currency") {
      $data_beli_emas["amount"] = $_POST["value_currency"];
    }

    if($_POST["calculate_type"] == "gold") {
      $data_beli_emas["amount"] = $_POST["value_gram"];
    }

    if(isset($_POST['va']) && $_POST["type_payment"] === 'va') {
      $data_beli_emas["bank"] = $_POST["va"];
    }

    if(isset($_POST['ewallet']) && $_POST["type_payment"] === 'ewallet' && isset($_POST['ewallet_phone'])) {
      $data_beli_emas["ewallet_type"] = $_POST["ewallet"];
      $data_beli_emas["ewallet_phone"] = $_POST["ewallet_phone"];
    }

    if(isset($_POST['retail']) && $_POST["type_payment"] === 'retail') {
      $data_beli_emas["retail_type"] = $_POST["retail"];
    }

    $buy_emas = gld_buy($data_beli_emas);
    $buy_emas = json_decode($buy_emas);
  ?>
  
  <?php if($buy_emas->code !== 200) { ?>
    <?php $_SESSION['alert_error_buy_emas'] = $buy_emas->message; ?>
    <script type="application/javascript">
      setTimeout(function(){
        location.replace("<?php echo $gldPage; ?>"); 
      }, "fast");
    </script>
  <?php } else { ?>
    <?php $_SESSION['data_beli_emas'] = $buy_emas->data; ?>
    <script type="application/javascript">
      setTimeout(function(){
        location.replace("<?php echo $gldPage . "/beli/invoice"; ?>"); 
      }, "fast");
    </script>
  <?php } ?>

<?php } else { ?>

  <script type="application/javascript">
    setTimeout(function(){
      location.replace("<?php echo $le_dashboard; ?>"); 
    }, "fast");
  </script>

<?php } ?>

<style>
  .row_gldDashboardPage {
    min-height: auto;
  }
</style>

<?php endwhile; ?>
<?php else : ?>
	<?php get_template_part( "content", "404pages" ); ?>
<?php endif; ?>

<?php get_template_part( "ladara-emas/templates/template", "footer" ); ?>