<?php /* Template Name: Ladara Emas - Beli - Invoice */ ?>

<?php get_template_part( "ladara-emas/templates/template", "header" ); ?>

<?php if (have_posts()) : while (have_posts()) : the_post(); ?>

<?php
  $user_id = get_current_user_id();

  $check_user_emas = gld_check_user_emas(array("user_id"=>$user_id));
  $check_user_emas = json_decode($check_user_emas);

  $gldPage = home_url() . "/emas";
?>

<?php if($check_user_emas->code !== 200) { ?>
  <style>
    header, body {
      display: none;
    }
  </style>
  <script type="application/javascript">
    setTimeout(function(){
      location.replace("<?php echo $gldPage; ?>"); 
    }, "fast");
  </script>
<?php } ?>

<?php
  $profile = gld_profile_emas(array("user_id"=>$user_id));
  $profile = json_decode($profile);

  $data_beli_emas = $_SESSION['data_beli_emas'];
?>

<div id="LadaraEmas" class="row_gldDashboardPage">
  <div class="gldDesktop">
    <div class="row">
      <div class="col-md-12">
        <div class="bx_gldBreadcrumb bx_breadcrumbAktivasiLE">
          <div>
            <?php if(isset($data_beli_emas->no_va)) { ?>
            <a href="<?php echo home_url(); ?>/emas">
            <?php } else { ?>
            <a onclick="gldAlertBack('showpopup')">
            <?php } ?>
              <span>&lsaquo;</span>
              <span>Kembali ke Jual/Beli Emas</span>
            </a>
          </div>
        </div>

        <div class="gldcheckoutbeliemas">
          <div class="row bx_leBeli row_LEPlus row_gldFinishedActivation">
            <div class="col-md-12">
              <?php get_template_part( "ladara-emas/contents/content", "invoice" ); ?>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  
  <div class="gldMobile box_mgldDashboard">
    <div class="gldcheckoutbeliemas">
      <div class="bx_leBeli">
        <?php get_template_part( "ladara-emas/contents/content", "invoice" ); ?>
      </div>
    </div>
  </div>
</div>

<?php get_template_part( "ladara-emas/popups/popup", "cancel" ); ?>

<?php endwhile; ?>
<?php else : ?>
		<?php get_template_part( "content", "404pages" ); ?>	
<?php endif; ?>

<?php get_template_part( "ladara-emas/templates/template", "footer" ); ?>