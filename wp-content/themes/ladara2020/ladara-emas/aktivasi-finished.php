<?php /* Template Name: Ladara Emas - Aktivasi Selesai */ ?>

<?php get_template_part( "ladara-emas/templates/template", "header" ); ?>

<?php if (have_posts()) : while (have_posts()) : the_post(); ?>

<div id="LadaraEmas" class="row_gldDashboardPage">
  <div class="gldDesktop">
    <div class="row">
      <div class="col-md-12">
        <div id="wrap_aktivasiLE">
          <div class="bx_gldBreadcrumb">
            <div>
              <a href="<?php echo home_url(); ?>/emas">
                <span>&lsaquo;</span>
                <span>Kembali</span>
              </a>
            </div>
          </div>
          
          <div class="col_gldContent">
            <div class="row row_LEPlus row_gldFinishedActivation">
              <div class="cols upload col-md-12">
                <div>
                  <img src="<?php bloginfo('template_directory'); ?>/library/images/emas/img-finished-activation.svg" />
                </div>
                <h3>Selesai! Data kamu berhasil diunggah</h3>
                <p>Saat ini data kamu telah masuk dan sedang dalam tahap verifikasi<br />dan pengecekan oleh tim Ladara. Mohon bersabar ya!</p>
                <br />
                <a href="<?php echo home_url(); ?>/emas" class="btn_gldFont LEBtn_rainbow" style="padding:10px 70px;">Menuju Dashboard</a>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  
  <div class="gldMobile box_mgldDashboard">
    <div id="wrap_aktivasiLE">
      <div class="col_gldContent">
        <div class="row row_LEPlus row_gldFinishedActivation">
          <div class="cols upload col-md-12">
            <div>
              <img src="<?php bloginfo('template_directory'); ?>/library/images/emas/img-finished-activation.svg" />
            </div>
            <h3>Selesai! Data kamu berhasil diunggah</h3>
            <p>Saat ini data kamu telah masuk dan sedang dalam tahap verifikasi<br />dan pengecekan oleh tim Ladara. Mohon bersabar ya!</p>
            <br />
            <a href="<?php echo home_url(); ?>/emas" class="btn_gldFont LEBtn_rainbow" style="padding:10px 70px;">Menuju Dashboard</a>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

<?php endwhile; ?>
<?php else : ?>
		<?php get_template_part( 'content', '404pages' ); ?>	
<?php endif; ?>

<?php get_template_part( "ladara-emas/templates/template", "footer" ); ?>