<?php /* Template Name: Ladara Emas - Beli - Response */ ?>

<?php get_template_part( "ladara-emas/templates/template", "header" ); ?>

<?php if (have_posts()) : while (have_posts()) : the_post(); ?>

<?php
  $gldPage = home_url() . "/emas";
  if (isset($_GET['mobile']) and $_GET['mobile'] != '') {
    $yes_mobile = 1;
  } else {
    $yes_mobile = 0;
  }
?>

<?php if(!isset($_GET["invoice"])) { ?>
  <style>
    header, body {
      display: none;
    }
  </style>
  <script type="application/javascript">
    setTimeout(function(){
      location.replace("<?php echo $gldPage; ?>"); 
    }, "fast");
  </script>
<?php } ?>

<?php
  global $wpdb;
  
  $inputData = json_decode(file_get_contents('php://input'), true);

  $current_page = $wp_query->posts[0];

  $transaksi_emas = $wpdb->get_row(
    "SELECT * FROM ldr_transaksi_emas WHERE invoice_order='" . $_GET["invoice"] . "'",
    OBJECT
  );

  if (strtolower($current_page->post_name) == 'sukses-bayar') {
    $gld_payment_datas = [
      "status"              => "PAID",
      "xendit_external_id"  => $transaksi_emas->xendit_external_id
    ];
    gld_payment_response($gld_payment_datas);
  } elseif (strtolower($current_page->post_name) == 'gagal-bayar') {
    $gld_payment_datas = [
      "status"              => "CANCELED",
      "xendit_external_id"  => $transaksi_emas->xendit_external_id
    ];
    gld_payment_response($gld_payment_datas);
  }
?>

<div id="LadaraEmas" class="row_gldDashboardPage">
  <div class="gldDesktop">
    <div class="row">
      <div class="col-md-12">
        <div id="wrap_aktivasiLE">
          <div class="bx_gldBreadcrumb bx_breadcrumbAktivasiLE">
            <div>
              <a href="<?php echo $gldPage; ?>">
                <span>&lsaquo;</span>
                <span>Kembali ke Jual/Beli Emas</span>
              </a>
            </div>
          </div>
          
          <?php if($current_page->post_name === 'sukses-bayar') { ?>
            <div class="col_gldContent">
              <div class="row row_LEPlus row_gldFinishedActivation">
                <div class="cols upload col-md-12">
                  <div>
                    <img src="<?php bloginfo('template_directory'); ?>/library/images/emas/img-finished-checkout.svg" />
                  </div>
                  <h3>Pembayaran Emas Kamu Telah Berhasil!</h3>
                  <p>Terima kasih sudah melakukan pembayaran. Kami akan menverifikasi pembayaran mu terlebih dahulu.</p>
                  <br />
                  <?php if ($yes_mobile == 0) { ?>
                    <a href="<?php echo $gldPage; ?>" class="btn_gldFont LEBtn_rainbow" style="padding:10px 70px;">
                      Menuju Dashboard
                    </a>
                  <?php } else { ?>
                    <a class="btn_gldFont LEBtn_rainbow" style="padding:10px 70px;">
                      Menuju Dashboard
                    </a>
                  <?php } ?>
                </div>
              </div>
            </div>
          <?php } elseif($current_page->post_name === 'gagal-bayar') { ?>
            <div class="col_gldContent">
              <div class="row row_LEPlus row_gldFinishedActivation">
                <div class="cols upload col-md-12">
                  <div>
                    <img src="<?php bloginfo('template_directory'); ?>/library/images/ico_cancelled.svg" />
                  </div>
                  <h3>Pembayaran Emas Kamu Gagal</h3>
                  <p>Maaf transaksi pembelian emas kamu gagal dikonfirmasi. Silahkan ulangi lagi.</p>
                  <br />
                  <?php if ($yes_mobile == 0) { ?>
                    <a href="<?php echo $gldPage; ?>" class="btn_gldFont LEBtn_rainbow" style="padding:10px 70px;">
                      Menuju Dashboard
                    </a>
                  <?php } else { ?>
                    <a class="btn_gldFont LEBtn_rainbow" style="padding:10px 70px;">
                      Menuju Dashboard
                    </a>
                  <?php } ?>
                </div>
              </div>
            </div>
          <?php } ?>
        </div>
      </div>
    </div>
  </div>
  
  <div class="gldMobile box_mgldDashboard">
    <div id="wrap_aktivasiLE">          
      <?php if($current_page->post_name === 'sukses-bayar') { ?>
        <div class="col_gldContent">
          <div class="row row_LEPlus row_gldFinishedActivation">
            <div class="cols upload col-md-12">
              <div>
                <img src="<?php bloginfo('template_directory'); ?>/library/images/emas/img-finished-checkout.svg" />
              </div>
              <h3>Pembayaran Emas Kamu Telah Berhasil!</h3>
              <p>Terima kasih sudah melakukan pembayaran. Kami akan menverifikasi pembayaran mu terlebih dahulu.</p>
              <br />
              <?php if ($yes_mobile == 0) { ?>
                <a href="<?php echo $gldPage; ?>" class="btn_gldFont LEBtn_rainbow" style="padding:10px 70px;">
                  Menuju Dashboard
                </a>
              <?php } else { ?>
                <button id="buttonCallBackEmas" class="btn_gldFont LEBtn_rainbow" style="padding:10px 70px;">
                  Menuju Dashboard
                </button>
              <?php } ?>
            </div>
          </div>
        </div>
      <?php } elseif($current_page->post_name === 'gagal-bayar') { ?>
        <div class="col_gldContent">
          <div class="row row_LEPlus row_gldFinishedActivation">
            <div class="cols upload col-md-12">
              <div>
                <img src="<?php bloginfo('template_directory'); ?>/library/images/ico_cancelled.svg" />
              </div>
              <h3>Pembayaran Emas Kamu Gagal</h3>
              <p>Maaf transaksi pembelian emas kamu gagal dikonfirmasi. Silahkan ulangi lagi.</p>
              <br />
              <?php if ($yes_mobile == 0) { ?>
                <a href="<?php echo $gldPage; ?>" class="btn_gldFont LEBtn_rainbow" style="padding:10px 70px;">
                  Menuju Dashboard
                </a>
              <?php } else { ?>
                <button id="buttonCallBackEmas" class="btn_gldFont LEBtn_rainbow" style="padding:10px 70px;">
                  Menuju Dashboard
                </button>
              <?php } ?>
            </div>
          </div>
        </div>
      <?php } ?>
    </div>
  </div>
</div>

<?php endwhile; ?>
<?php else : ?>
		<?php get_template_part( 'content', '404pages' ); ?>	
<?php endif; ?>

<?php get_template_part( "ladara-emas/templates/template", "footer" ); ?>