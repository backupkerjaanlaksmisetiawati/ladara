<?php /* Template Name: Ladara Emas - Jual - Processing */ ?>

<?php if (have_posts()) : while (have_posts()) : the_post(); ?>

<?php get_template_part( "ladara-emas/templates/template", "header" ); ?>

<?php
  $gldPage = home_url() . "/emas";
  $user_id = get_current_user_id();
  $check_user_emas = gld_check_user_emas(array("user_id"=>$user_id));
  $check_user_emas = json_decode($check_user_emas);
?>

<?php if($check_user_emas->code !== 200) { ?>
  <style>
    header, body {
      display: none;
    }
  </style>
  <script type="application/javascript">
    setTimeout(function(){
      location.replace("<?php echo $le_dashboard; ?>"); 
    }, "fast");
  </script>
<?php } ?>

<div id="LadaraEmas" class="row_homePage">
  <div class="gldDesktop">
    <div class="row">
      <div class="col-md-12">
        <div id="wrap_aktivasiLE">
          <div class="bx_gldBreadcrumb bx_breadcrumbAktivasiLE">
            <div>&nbsp;</div>
          </div>
          
          <div class="col_gldContent">
            <div class="row row_LEPlus row_gldFinishedActivation">
              <div class="cols upload col-md-12">
                <div class="imgProccessingTransaksi"></div>
                <h3>Tunggu sebentar, ya...</h3>
                <p>Penjualan emasmu sedang diproses.</p>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="gldMobile box_mgldDashboard">
    <div class="col-md-12">          
        <div class="col_gldContent">
          <div class="row row_LEPlus row_gldFinishedActivation">
            <div class="cols upload col-md-12">
              <div class="imgProccessingTransaksi"></div>
              <h3>Tunggu sebentar, ya...</h3>
              <p>Penjualan emasmu sedang diproses.</p>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

<?php /** user must not access this page directly */ if(!isset($_POST["value_gram"]) && !isset($_POST["value_currency"])) { ?>
  <script type="application/javascript">
    setTimeout(function(){
      location.replace("<?php echo $gldPage; ?>"); 
    }, "fast");
  </script>
<?php } ?>

<?php
  $data_sell_emas = array(
    "user_id"     => $user_id,
    "amount"      => 0,
    "amount_type" => $_POST["calculate_type"],
  );

  if($_POST["calculate_type"] == "currency") {
    $data_sell_emas["amount"] = $_POST["value_currency"];
  }

  if($_POST["calculate_type"] == "gold") {
    $data_sell_emas["amount"] = $_POST["value_gram"];
  }

  $sell_emas = gld_sell($data_sell_emas);
  $sell_emas = json_decode($sell_emas);
?>

<?php if($sell_emas->code === 200) { ?>
  <?php $_SESSION['data_jual_emas'] = $sell_emas->data; ?>
  <script type="application/javascript">
    setTimeout(function(){
      location.replace("<?php echo $gldPage . "/jual/faktur"; ?>"); 
    }, "fast");
  </script>
<?php } else { ?>
  <?php
    $_SESSION['alert_error_sell_emas'] = $sell_emas->message;
    $_SESSION['alert_error_sell_emas_activity'] = time() + 600;
  ?>
  <script type="application/javascript">
    setTimeout(function(){
      location.replace("<?php echo $gldPage; ?>"); 
    }, "fast");
  </script>
<?php } ?>

<style>
  .row_gldDashboardPage {
    min-height: auto;
  }
</style>

<?php endwhile; ?>
<?php else : ?>
	<?php get_template_part( "content", "404pages" ); ?>
<?php endif; ?>

<?php get_template_part( "ladara-emas/templates/template", "footer" ); ?>