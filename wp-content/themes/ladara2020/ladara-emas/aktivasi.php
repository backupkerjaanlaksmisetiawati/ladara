<?php /* Template Name: Ladara Emas - Aktivasi */ ?>

<?php get_template_part( "ladara-emas/templates/template", "header" ); ?>

<?php if (have_posts()) : while (have_posts()) : the_post(); ?>

<?php $user_id = get_current_user_id(); ?>

<?php if($user_id == 0) { ?>
  <?php $login = home_url() . '/login'; ?>
  <style>
    header, body {
      display: none;
    }
  </style>
  <script type="application/javascript">
    setTimeout(function(){
      location.replace('<?php echo $login; ?>'); 
    }, 'fast');
  </script>
<?php } ?>

<?php
  global $wpdb;
  
  $gldPage = home_url() . '/emas';
  $check_user_emas = gld_check_user_emas(array("user_id"=>$user_id));
  $check_user_emas = json_decode($check_user_emas);
?>

<?php if($check_user_emas->code === 200) { ?>
  <style>
    header, body {
      display: none;
    }
  </style>
  <script type="application/javascript">
    setTimeout(function(){
      location.replace('<?php echo $gldPage; ?>'); 
    }, 'fast');
  </script>
<?php } ?>

<?php
  $get_user = gld_user_profile(array("user_id"=>$user_id));
  $get_user = json_decode($get_user);

  $bank_list = gld_bank(array("type"=>"trs"));
  $bank_list = json_decode($bank_list);

  $security_question = gld_security_question();
  $security_question = json_decode($security_question);
?>

<?php if(!isset($_POST["owner"]) && !isset($_POST["norek"]) && !isset($_POST["bank"])) { ?>
  <div id="LadaraEmas">
    <div class="gldDesktop">
      <form class="row row_gldDashboardPage" id="registerEmas" action="<?php echo home_url(); ?>/emas/aktivasi" method="post" enctype="multipart/form-data">
        <div class="col-md-12">
          <?php if($get_user->code !== 200) { ?>
            <div class="wrap_gldAktivasi" style="height:640px;">
              <div class="bx_gldLoading">
                <img src="<?php bloginfo('template_directory'); ?>/library/images/emas/icon-loading.svg" />
              </div>

              <div id="aktivasiLE_foto_ktp">
                <div class="bx_gldBreadcrumb bx_breadcrumbGldAktivasi">
                  <div>
                    <a href="<?php echo home_url(); ?>/emas">
                      <span>&lsaquo;</span>
                      <span>Kembali</span>
                    </a>
                  </div>
                </div>
                
                <div class="col_gldContent">
                  <div class="row row_LEPlus row_gldFinishedActivation">
                    <div class="cols upload col-md-12">
                      <div>
                        <img src="<?php bloginfo('template_directory'); ?>/library/images/emas/img-complete-profile.svg" />
                      </div>
                      <h3>Ups! Data diri kamu belum Lengkap</h3>
                      <p>Sebelum melanjutkan transaksi di Ladara Emas, kamu harus melengkapi data diri.<br />Yuk lengkapi profil kamu sekarang</p>
                      <br />
                      <a href="<?php echo home_url(); ?>/profile" class="btn_gldFont LEBtn_blue_white">Lengkapi Profil Sekarang</a>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          <?php } else { ?>
            <div class="wrap_gldAktivasi" style="<?php echo ($security_question->code !== 200 || isset($_SESSION["message_error_register_ladara_emas"])) ? "height:680px;" : ""; ?>">
              <div class="bx_gldLoading">
                <img src="<?php bloginfo('template_directory'); ?>/library/images/emas/icon-loading.svg" />
              </div>

              <div id="aktivasiLE_foto_ktp">
                <div class="bx_gldBreadcrumb bx_breadcrumbGldAktivasi">
                  <div>
                    <a href="<?php echo home_url(); ?>/emas">
                      <span>&lsaquo;</span>
                      <span>Kembali</span>
                    </a>
                  </div>
                  <div class="clearfix col_stepActivation">
                    <a class="clearfix">
                      <span>2</span>
                      <span>Verifikasi Informasi</span>
                    </a>
                    <span class="clearfix">
                      <hr />
                    </span>
                    <a class="clearfix active">
                      <span>1</span>
                      <span>Verifikasi KTP</span>
                    </a>
                  </div>
                </div>
                
                <div class="col_gldContent">
                  <div class="bx_LEPlus">
                    <h3 class="ht_aktivasiLE">Verifikasi identitas kamu dulu yuk!</h3>

                    <?php if($security_question->code !== 200) { ?>
                      <p style="color:#FF1E00; font-weight:bold; margin:30px auto; width:90%;">
                        Gagal mendapatkan data. Silahkan hubungi Admin Ladara Emas
                      </p>
                    <?php } ?>

                    <?php if(isset($_SESSION["message_error_register_ladara_emas"])) { ?>
                      <p style="color:#FF1E00; font-weight:bold; margin:30px auto; width:90%;">
                        <?php echo $_SESSION["message_error_register_ladara_emas"]; ?>
                      </p>
                    <?php } ?>

                    <div class="row row_LEPlus">
                      <div id="upload_ktp_LE" class="cols upload col-sm-4 col-md-4">
                        <label class="step">1</label>
                        <div class="wrap_foto icon-upload-ktp"></div>
                        <p>Foto KTP kamu dengan jelas</p>
                        <p>Kami harus memastikan bahwa identitas yang kamu miliki adalah identitas yang asli</p>
                        <span class="error"></span>
                        <div class="bx_leFileKtp">
                          <?php /*<input id="upload_ktp_LE_file" name="le_ktp" type="file" style="visibility:hidden" onchange="preview_image(event, 'preview_ktp')" /> */ ?>
                          <input id="upload_ktp_LE_file" name="le_ktp" type="file" />
                          <button type="button" <?php echo ($security_question->code !== 200) ? "disabled" : ""; ?> class="btn_gldFont LEBtn_blue_white" onclick="gld_upload_ktp()">Unggah Foto</button>
                          <p id="preview_ktp"><span></span><label class="ubah_KTPLE" onclick="gld_upload_ktp()">ubah</label></p>
                        </div>
                        <label class="step_bottom step_upload_ktp_LE clearfix">
                          <span>1</span>
                          <span>Unggah KTP</span>
                        </label>
                      </div>
                      
                      <div id="upload_foto_LE" class="cols upload col-sm-4 col-md-4">
                        <label class="step">2</label>
                        <div class="wrap_foto icon-upload-selfie"></div>
                        <p>Foto Selfie dengan KTP kamu</p>
                        <p>Kami perlu memastikan 100% bahwa kamu adalah kamu yang asli</p>
                        <span class="error"></span>
                        <div class="bx_leFileKtp">
                          <?php /*<input id="upload_foto_LE_file" name="le_foto" type="file" style="visibility:hidden" onchange="preview_image(event, 'preview_selfie')" /> */ ?>
                          <input id="upload_foto_LE_file" name="le_foto" type="file" />
                          <button type="button" disabled class="btn_gldFont LEBtn_blue_white" onclick="upload_selfie_emas()">Unggah Foto</button>
                          <p id="preview_selfie"><span></span><label class="ubah_selfieLE" onclick="upload_selfie_emas()">ubah</label></p>
                        </div>
                        <label class="step_bottom step_upload_foto_LE clearfix">
                          <span>2</span>
                          <span>Unggah Selfie + KTP</span>
                        </label>
                      </div>
                      
                      <div id="lanjut_regis_LE" class="cols upload col-sm-4 col-md-4">
                        <label class="step">3</label>
                        <div class="wrap_foto icon-undraw"></div>
                        <p>Lanjutkan pendaftaran!</p>
                        <p>Tenang! Data kamu selalu aman karena dijaga dengan ketat oleh tim Ladara dan Treasury!</p>
                        <br />
                        <button type="button" disabled class="btn_gldFont LEBtn_blue_white" onclick="gldAktivasiBank('desktop')">Lanjutkan</button>
                        <label class="step_bottom clearfix">
                          <span>3</span>
                          <span>Telah Diunggah</span>
                        </label>
                      </div>
                      
                      <div class="col-md-12 border">
                        <hr />
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              
              <div id="aktivasiLE_bank">
                <div class="bx_gldBreadcrumb bx_breadcrumbGldAktivasi">
                  <div>
                    <a href="<?php echo home_url(); ?>/emas/aktivasi">
                      <span>&lsaquo;</span>
                      <span>Kembali</span>
                    </a>
                  </div>
                  <div class="clearfix col_stepActivation">
                    <a class="clearfix active">
                      <span>2</span>
                      <span>Verifikasi Informasi</span>
                    </a>
                    <span class="clearfix">
                      <hr />
                    </span>
                    <a class="clearfix active">
                      <span>1</span>
                      <span>Verifikasi KTP</span>
                    </a>
                  </div>
                </div>
                
                <div class="col_gldContent">
                  <div class="bx_LEPlus">
                    <h3 class="ht_aktivasiLE">Masukkan informasi tentang Bank kamu!</h3>
                    <br />

                    <div class="row">
                      <div class="col-sm-6 col-md-6">
                        <div class="bx_gldBank">
                          <p class="bx_le_select">
                            <select class="jsselect_le" name="bank">
                              <option value="0">Pilih Bank</option>
                              <?php if(!empty($bank_list->data)) { ?>
                                <?php foreach($bank_list->data as $bank) { ?>
                                  <option value="<?php echo $bank->code; ?>">
                                    <?php echo $bank->name; ?>
                                  </option>
                                <?php } ?>
                              <?php } ?>
                            </select>
                            <span class="error_le error_bank"></span>
                          </p>
                          <p>
                            <input type="text" name="norek" autocomplete="off" placeholder="Masukkan Nomor Rekening" />
                            <span class="error_le error_norek"></span>
                          </p>
                          <p>
                            <input type="text" name="owner" autocomplete="off" placeholder="Masukkan Nama Pemilik Rekening" />
                            <span class="error_le error_owner"></span>
                          </p>
                          <p>
                            <input type="text" name="branch" autocomplete="off" placeholder="Masukkan Branch Bank (optional)" />
                            <span class="error_le error_branch"></span>
                          </p>
                          <p>
                            <button type="button" class="btn_gldFont LEBtn_blue_white" onclick="ajax_gld_register('desktop')">Mulai Menabung Emas</button>
                          </p>
                          <p class="le_term">
                            Dengan mendaftar, kamu setuju dengan<br />
                            <a href="<?php echo home_url(); ?>/emas/syarat-ketentuan" target="_blank">Syarat dan Ketentuan</a> serta <a href="<?php echo home_url(); ?>/emas/kebijakan-privasi" target="_blank">Kebijakan Privasi</a>.
                          </p>
                        </div>
                      </div>
                      <div class="col-sm-6 col-md-6">
                        <img src="<?php bloginfo('template_directory'); ?>/library/images/emas/img-saving.svg" />
                      </div>
                    </div>
                    
                    <br />
                  </div>
                </div>
              </div>
            </div>
          <?php } ?>
        </div>
      </form>
    </div>

    <div class="gldMobile box_mgldDashboard">
      <form class="row_gldDashboardPage" id="mregisterEmas" action="<?php echo home_url(); ?>/emas/aktivasi" method="post" enctype="multipart/form-data">
        <?php if($get_user->code !== 200) { ?>
          <div class="wrap_gldAktivasi" style="height:auto;">
            <div id="maktivasigld_foto_ktp">
              <div class="col_gldContent">
                <div class="row row_LEPlus row_gldFinishedActivation">
                  <div class="cols upload col-md-12">
                    <div>
                      <img src="<?php bloginfo('template_directory'); ?>/library/images/emas/img-complete-profile.svg" />
                    </div>
                    <h3>Ups! Data diri kamu belum Lengkap</h3>
                    <p>Sebelum melanjutkan transaksi di Ladara Emas, kamu harus melengkapi data diri.<br />Yuk lengkapi profil kamu sekarang</p>
                    <br />
                    <a href="<?php echo home_url(); ?>/profile" class="btn_gldFont LEBtn_blue_white">Lengkapi Profil Sekarang</a>
                  </div>
                </div>
              </div>
            </div>
          </div>
        <?php } else { ?>
          <div class="wrap_mgldAktivasi" style="<?php echo ($security_question->code !== 200 || isset($_SESSION["message_error_register_ladara_emas"])) ? "height:1290px;" : ""; ?>">
            <div class="bx_gldLoading">
              <img src="<?php bloginfo('template_directory'); ?>/library/images/emas/icon-loading.svg" />
            </div>

            <div id="maktivasigld_foto_ktp">
              <div class="bx_gldBreadcrumb bx_breadcrumbGldAktivasi">
                <div class="clearfix col_stepActivation">
                  <a class="clearfix">
                    <span>2</span>
                    <span>Verifikasi Informasi</span>
                  </a>
                  <span class="clearfix">
                    <hr />
                  </span>
                  <a class="clearfix active">
                    <span>1</span>
                    <span>Verifikasi KTP</span>
                  </a>
                </div>
              </div>
              
              <div class="col_gldContent">
                <div class="bx_LEPlus">
                  <h3 class="ht_aktivasiLE">Verifikasi identitas kamu dulu yuk!</h3>

                  <?php if($security_question->code !== 200) { ?>
                    <p style="color:#FF1E00; font-weight:bold; margin:30px auto; width:90%;">
                      Gagal mendapatkan data. Silahkan hubungi Admin Ladara Emas
                    </p>
                  <?php } ?>
                  <?php if(isset($_SESSION["message_error_register_ladara_emas"])) { ?>
                    <p style="color:#FF1E00; font-weight:bold; margin:30px auto; width:90%;">
                      <?php echo $_SESSION["message_error_register_ladara_emas"]; ?>
                    </p>
                  <?php } ?>

                  <div class="row row_LEPlus">
                    <div id="mgld_upload_ktp" class="cols upload col-sm-12 col-md-4">
                      <label class="step">1</label>
                      <div class="wrap_foto icon-upload-ktp"></div>
                      <p>Foto KTP kamu dengan jelas</p>
                      <p>Kami harus memastikan bahwa identitas yang kamu miliki adalah identitas yang asli</p>
                      <span class="error"></span>
                      <div class="bx_leFileKtp">
                        <input id="mgld_upload_ktp_file" name="le_ktp" type="file" />
                        <button type="button" <?php echo ($security_question->code !== 200) ? "disabled" : ""; ?> class="btn_gldFont LEBtn_blue_white" onclick="mgld_upload_ktp()">Unggah Foto</button>
                        <p id="mgld_preview_ktp"><span></span><label class="ubah_KTPLE" onclick="mgld_upload_ktp()">ubah</label></p>
                      </div>
                      <p>&nbsp;</p><p>&nbsp;</p>
                    </div>
                    
                    <div id="mgld_upload_foto" class="cols upload col-sm-12 col-md-4">
                      <label class="step">2</label>
                      <div class="wrap_foto icon-upload-selfie"></div>
                      <p>Foto Selfie dengan KTP kamu</p>
                      <p>Kami perlu memastikan 100% bahwa kamu adalah kamu yang asli</p>
                      <span class="error"></span>
                      <div class="bx_leFileKtp">
                        <input id="mgld_upload_foto_file" name="le_foto" type="file" />
                        <button type="button" disabled class="btn_gldFont LEBtn_blue_white" onclick="mgld_upload_selfie()">Unggah Foto</button>
                        <p id="mgld_preview_selfie"><span></span><label class="ubah_selfieLE" onclick="mgld_upload_selfie()">ubah</label></p>
                      </div>
                      <p>&nbsp;</p><p>&nbsp;</p>
                    </div>
                    
                    <div id="mgld_lanjut_regis" class="cols upload col-sm-12 col-md-4">
                      <label class="step">3</label>
                      <div class="wrap_foto icon-undraw"></div>
                      <p>Lanjutkan pendaftaran!</p>
                      <p>Tenang! Data kamu selalu aman karena dijaga dengan ketat oleh tim Ladara dan Treasury!</p>
                      <br />
                      <button type="button" disabled class="btn_gldFont LEBtn_blue_white" onclick="gldAktivasiBank('mobile')">Lanjutkan</button>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            
            <div id="maktivasigld_bank">
              <div class="bx_gldBreadcrumb bx_breadcrumbGldAktivasi">
                <div class="clearfix col_stepActivation">
                  <a class="clearfix active">
                    <span>2</span>
                    <span>Verifikasi Informasi</span>
                  </a>
                  <span class="clearfix">
                    <hr />
                  </span>
                  <a class="clearfix active">
                    <span>1</span>
                    <span>Verifikasi KTP</span>
                  </a>
                </div>
              </div>
              
              <div class="col_gldContent">
                <div class="bx_LEPlus">
                  <h3 class="ht_aktivasiLE">Masukkan informasi tentang Bank kamu!</h3>
                  <br />
                  
                  <div>
                    <img src="<?php bloginfo('template_directory'); ?>/library/images/emas/img-saving.svg" class="full-img" />
                  </div>

                  <div class="bx_gldBank">
                    <p class="bx_le_select">
                      <select class="jsselect_le" name="bank">
                        <option value="0">Pilih Bank</option>
                        <?php if(!empty($bank_list->data)) { ?>
                          <?php foreach($bank_list->data as $bank) { ?>
                            <option value="<?php echo $bank->code; ?>">
                              <?php echo $bank->name; ?>
                            </option>
                          <?php } ?>
                        <?php } ?>
                      </select>
                      <span class="error_le error_bank"></span>
                    </p>
                    <p>
                      <input type="text" name="norek" autocomplete="off" placeholder="Masukkan Nomor Rekening" />
                      <span class="error_le error_norek"></span>
                    </p>
                    <p>
                      <input type="text" name="owner" autocomplete="off" placeholder="Masukkan Nama Pemilik Rekening" />
                      <span class="error_le error_owner"></span>
                    </p>
                    <p>
                      <input type="text" name="branch" autocomplete="off" placeholder="Masukkan Branch Bank (optional)" />
                      <span class="error_le error_branch"></span>
                    </p>
                    <p>
                      <button type="button" class="btn_gldFont LEBtn_blue_white" onclick="ajax_gld_register('mobile')">Mulai Menabung Emas</button>
                    </p>
                    <p class="le_term">
                      Dengan mendaftar, kamu setuju dengan<br />
                      <a href="<?php echo home_url(); ?>/emas/syarat-ketentuan" target="_blank">Syarat dan Ketentuan</a> serta <a href="<?php echo home_url(); ?>/emas/kebijakan-privasi" target="_blank">Kebijakan Privasi</a>.
                    </p>
                  </div>
                  
                </div>
              </div>
            </div>
          </div>
        <?php } ?>
      </form>
    </div>
  </div>
<?php } else { ?>
  <div id="LadaraEmas" class="row_homePage">
    <div class="gldDesktop">
      <div class="row">
        <div class="col-md-12">
          <div id="wrap_aktivasiLE">
            <div class="bx_gldBreadcrumb bx_breadcrumbAktivasiLE">
              <div>&nbsp;</div>
            </div>
            
            <div class="col_gldContent">
              <div class="row row_LEPlus row_gldFinishedActivation">
                <div class="cols upload col-md-12">
                  <div class="imgProccessingTransaksi"></div>
                  <h3>Tunggu sebentar, ya...</h3>
                  <p>Aktivasi akun Ladara Emas kamu sedang diproses.</p>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="gldMobile box_mgldDashboard">
      <div class="col-md-12">          
          <div class="col_gldContent">
            <div class="row row_LEPlus row_gldFinishedActivation">
              <div class="cols upload col-md-12">
                <div class="imgProccessingTransaksi"></div>
                <h3>Tunggu sebentar, ya...</h3>
                <p>Aktivasi akun Ladara Emas kamu sedang diproses.</p>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
<?php } ?>

<?php /** proses registrasi */ if(isset($_POST["owner"]) && isset($_POST["norek"]) && isset($_POST["bank"])) { ?>
  <?php
    $foto = $_FILES['le_foto'];
    $ktp = $_FILES['le_ktp'];

    $filename_foto = $foto['tmp_name'];
    $handle_foto = fopen($filename_foto, "r");
    $data_foto = file_get_contents($filename_foto);

    $filename_ktp = $ktp['tmp_name'];
    $handle_ktp = fopen($filename_ktp, "r");
    $data_ktp = file_get_contents($filename_ktp);

    $data_user = array(
      'user_id'                   => $get_user->data->id,
      'name'                      => $get_user->data->name,
      'email'                     => $get_user->data->email,
      'web_password'              => $get_user->data->pass,
      'ts_password'               => $get_user->data->pass,
      'referral_code'             => "",
      'security_question'         => 1,
      'security_question_answer'  => "Mickey Mouse",
      'gender'                    => $get_user->data->gender,
      'phone'                     => $get_user->data->phone,
      'birthday'                  => $get_user->data->birthdate,
      'account_name'              => $_POST["owner"],
      'account_number'            => $_POST["norek"],
      'bank_code'                 => $_POST["bank"],
      'branch'                    => $_POST["branch"],
      'selfie_scan'               => base64_encode($data_foto),
      'id_card_scan'              => base64_encode($data_ktp)
    );

    if(date("Y-m-d H:i:s") < "2021-12-31 23:59:59") {
      $data_user["referral_code"] = "LDRTRS01";
    }

    if($security_question->code === 200) {
      $data_user["security_question"] = $security_question->data[0]->id;
    }

    $register = gld_register($data_user);
    $register = json_decode($register);
  ?>

  <?php if($register->code === 200) { ?>
    <script type="application/javascript">
      setTimeout(function(){
        location.replace('<?php echo $gldPage . '/aktivasi/selesai'; ?>'); 
      }, 'fast');
    </script>
  <?php } else { ?>
    <?php
      $_SESSION["message_error_register_ladara_emas"] = $register->message;
      // $_SESSION["message_error_register_ladara_emas_activity"] = time() + 300;
    ?>
    <script type="application/javascript">
      setTimeout(function(){
        location.replace('<?php echo $gldPage . '/aktivasi'; ?>'); 
      }, 'fast');
    </script>
  <?php } ?>

<?php } ?>

<?php if(isset($_SESSION["message_error_register_ladara_emas"])) { ?>
  <?php unset($_SESSION["message_error_register_ladara_emas"]); ?>
<?php } ?>
  
<?php endwhile; ?>
<?php else : ?>
		<?php get_template_part( 'content', '404pages' ); ?>	
<?php endif; ?>

<?php get_template_part( "ladara-emas/templates/template", "footer" ); ?>