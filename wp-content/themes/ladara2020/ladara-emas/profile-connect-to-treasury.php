<?php /* Template Name: Connect to Treasury Page */ ?>

<?php get_header(); ?>

<?php if (have_posts()) : while (have_posts()) : the_post(); ?>

<?php 
	date_default_timezone_set("Asia/Jakarta");
	$user_id = get_current_user_id();

	$check_user_emas = gld_check_user_emas(array("user_id"=>$user_id));
	$check_user_emas = json_decode($check_user_emas);

	if($check_user_emas->code === 200) {
		$profile_emas = gld_profile_emas(array("user_id"=>$user_id));
		$profile_emas = json_decode($profile_emas);
	}

  $bank_list = gld_bank(array("type"=>"trs"));
	$bank_list = json_decode($bank_list);
	
	$content_post = get_post();
?>

<?php if(!isset($user_id) || $user_id == 0){ ?>
	<script>
		// 'Getting' data-attributes using getAttribute
		var plant = document.getElementById('body');
		console.log(plant);
		var hurl = plant.getAttribute('data-hurl'); 
		location.replace(hurl+'/login/'); 
	</script>
<?php } ?>

<style type="text/css">
	#wp-submit {
		background: #0080FF;
	}
</style>

<div class="row"></div>

<div id="link_to_treasury" class="row row_profile">
	<div class="col-md-3 col_profile des_display">
		<?php get_template_part( 'content', 'menu-profile' ); ?>
	</div>

	<div class="col-md-9 col_profile">
		<div class="row row_cont_tab_profile">
			<h1 class="ht_profile">
				<?php echo $content_post->post_title; ?>
			</h1>

			<?php if(!empty($content_post->post_content)) { ?>
				<div style="margin-bottom:15px;">
					<div>
						<?php echo $content_post->post_content; ?>
					</div>
				</div>
			<?php } ?>

			<div class="bx_f_editProfile">
				<form id="form_linktreasury" class="form_linktreasury" method="post">

					<?php /*
					<div class="f_aform">
						<h5>
							<strong>
								Masukkan informasi profile Treasury kamu
							</strong>
						</h5>
					</div>
					*/ ?>

					<div class="f_aform">
						<label><b>Email Treasury </b><span>*</span></label>
						<input type="text" name="email" autocomplete="off" id="email_treasury" class="txt_aform" required="required" <?php if($check_user_emas->code === 200 && !isset($_SESSION["message_error_register_ladara_emas"])) { ?>readonly="readonly"<?php } ?> <?php if($check_user_emas->code === 200) { ?>value="<?php echo $profile_emas->data->email; ?>"<?php } ?>>
						<div class="err_aform error_email"></div>
					</div>

					<div class="f_aform">
						<label><b>Password Treasury </b><span>*</span></label>
						<input type="password" name="password" autocomplete="off" id="password_treasury" class="txt_aform" required="required" <?php if($check_user_emas->code === 200 && !isset($_SESSION["message_error_register_ladara_emas"])) { ?>readonly="readonly"<?php } ?> <?php if($check_user_emas->code === 200) { ?>value="*****"<?php } ?>>
						<div class="err_aform error_password"></div>
					</div>

					<div>
						<input type="hidden" name="is_update" autocomplete="off" id="is_update_treasury" required="required" value="<?php if(!isset($_SESSION["message_error_register_ladara_emas"])) { ?>false<?php } else { ?>true<?php } ?>">
					</div>
					
					<?php if($check_user_emas->code === 200 && !isset($_SESSION["message_error_register_ladara_emas"])) { ?>
						<div class="f_aform">
							<div class="err_info alert-success" style="display:block">
								<p style="font-size:11px;margin:0px;">
									Kamu sudah menghubungkan akun Treasury mu di Ladara Emas.
									<br />
									Silahkan klik tombol dibawah untuk menabung emas.
								</p>
							</div>
						</div>

						<div class="f_aform">
							<p style="display:flex;">
								<a class="sub_aform" style="color:#ffffff;" href="<?php echo home_url(); ?>/emas">
									Mulai menabung emas
								</a>
							</p>
						</div>
					<?php } else { ?>
						<div class="f_aform">
							<div class="err_info alert-success"></div>
							<div class="err_info alert-danger error_link_to_treasury">
								<?php
									if(isset($_SESSION["message_error_register_ladara_emas"])) {
										echo $_SESSION["message_error_register_ladara_emas"];
									}
								?>
							</div>
							<input type="button" id="wp-submit" onclick="ajax_linking_treasury(event)" class="sub_aform" value="Simpan">
						</div>
					<?php } ?>
				</form>
			</div>

		</div>
	</div>
</div>

<?php endwhile; ?>
<?php else : ?>
	<?php get_template_part( 'content', '404pages' ); ?>	
<?php endif; ?>
<?php get_footer(); ?>