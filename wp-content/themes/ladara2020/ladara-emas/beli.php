<?php /* Template Name: Ladara Emas - Beli */ ?>

<?php get_template_part( "ladara-emas/templates/template", "header" ); ?>

<?php if (have_posts()) : while (have_posts()) : the_post(); ?>

<?php
  $user_id = get_current_user_id();
  $check_user_emas = gld_check_user_emas(array("user_id"=>$user_id));
  $check_user_emas = json_decode($check_user_emas);
  $gldPage = home_url() . "/emas";
?>

<?php if($check_user_emas->code !== 200) { ?>
  <style>
    header, body {
      display: none;
    }
  </style>
  <script type="application/javascript">
    setTimeout(function(){
      location.replace("<?php echo $gldPage; ?>"); 
    }, "fast");
  </script>
<?php } ?>

<?php /** user must not access this page directly */ if(!isset($_POST["value_currency"]) && !isset($_POST["value_gram"])) { ?>
  <style>
    header, body {
      display: none;
    }
  </style>
  <script type="application/javascript">
    setTimeout(function(){
      location.replace("<?php echo $gldPage; ?>"); 
    }, "fast");
  </script>
<?php } ?>

<style>
  html {
    overflow-x: hidden !important;
  }
</style>

<?php
  $value_gram = str_replace(",", ".", $_POST["value_gram"]);
  $value_currency = str_replace(".", "", $_POST["value_currency"]);

  $calculate_datas = array(
    "user_id"           => $user_id,
    "transaction_type"  => "buy",
    "amount_type"       => $_POST["calculate_type"],
  );

  if($_POST["calculate_type"] == "currency") {
    $calculate_datas["amount"] = $value_currency;
  }

  if($_POST["calculate_type"] == "gold") {
    $calculate_datas["amount"] = $value_gram;
  }

  $calculate_emas = gld_calculate($calculate_datas);
  $calculate_emas = json_decode($calculate_emas);

  $current_buying_rate = round($calculate_emas->data->buy_price);
  $current_gold_value = str_replace(".", ",", round($calculate_emas->data->unit, 4, PHP_ROUND_HALF_DOWN));
  $current_currency_value = number_format(round($calculate_emas->data->currency), 0, ".", ".");
?>

<div id="LadaraEmas" class="row_gldDashboardPage">
  <div class="gldDesktop">
    <div class="row">
      <div class="col-md-12">
        <div class="bx_gldBreadcrumb bx_breadcrumbAktivasiLE">
          <div>
            <a href="<?php echo $gldPage; ?>">
              <span>&lsaquo;</span>
              <span>Kembali ke Jual/Beli Emas</span>
            </a>
          </div>
        </div>

        <form id="lecheckoutbeliemas" action="<?php echo home_url(); ?>/emas/beli/processing" method="post">
          <div class="row bx_leBeli">
            <div class="col-md-6">
              <div class="col_gldContent">
                <?php get_template_part( "ladara-emas/contents/content", "payment" ); ?>

                <h4>Ringkasan Pembayaran</h4>

                <div class="bx_leDetailBeli first">
                  <p><?php echo $current_gold_value; ?> gr emas</p>
                  <p>Rp <?php echo $current_currency_value; ?></p>
                </div>

                <div class="bx_leDetailBeli total">
                  <p>
                    Total<?php if($_POST["harga_beli_emas"] != $current_buying_rate) { ?><span style="color:#FE5461;">*</span><?php } ?>
                  </p>
                  <p>Rp <?php echo $current_currency_value; ?></p>
                </div>

                <?php if($_POST["harga_beli_emas"] != $current_buying_rate) { ?>
                  <p class="term change">
                    *Harga emas telah berubah dari <?php echo number_format($_POST["harga_beli_emas"], 0, ".", "."); ?>/gr
                    menjadi <?php echo number_format($current_buying_rate, 0, ".", "."); ?>/gr
                  </p>
                <?php } ?>

                <p class="button">
                  <a class="btn_gldFont LEBtn_blue_white btnMetodePembayaranGld" onclick="metodePembayaranGld('showpopup')">
                    Pilih Metode Pembayaran
                  </a>
                </p>

                <p class="button" style="margin-top: 0px;">
                  <button class="btn_gldFont LEBtn_rainbow btnVerifikasiGldBuy" type="button" disabled onclick="verifikasiGldBuy('showpopup')">
                    Beli Emas
                  </button>
                </p>

                <p class="term">
                  Dengan mengeklik tombol di atas. Kamu sudah menyetujui
                  <a href="<?php echo home_url(); ?>/emas/syarat-ketentuan" target="_blank">syarat dan ketentuan</a> yang berlaku.
                </p>

                <div class="d-none">
                  <input type="hidden" name="value_gram" class="d-none" value="<?php echo $value_gram; ?>" />
                  <input type="hidden" name="value_currency" class="d-none" value="<?php echo $value_currency; ?>" />
                  <input type="hidden" name="calculate_type" class="d-none" value="<?php echo $_POST["calculate_type"]; ?>" />
                </div>
              </div>
            </div>
          </div>
        </form>
      </div>
    </div>
  </div>

  <div class="gldMobile box_mgldDashboard">
    <form id="mlecheckoutbeliemas" action="<?php echo home_url(); ?>/emas/beli/processing" method="post">
      <div class="row bx_leBeli">
        <div class="col-md-6">
          <div class="col_gldContent">
            <?php get_template_part( "ladara-emas/contents/content", "payment" ); ?>

            <h4>Ringkasan Pembayaran</h4>

            <div class="bx_leDetailBeli first">
              <p><?php echo $current_gold_value; ?> gr emas</p>
              <p>Rp <?php echo $current_currency_value; ?></p>
            </div>

            <div class="bx_leDetailBeli total">
              <p>
                Total<?php if($_POST["harga_beli_emas"] != $current_buying_rate) { ?><span style="color:#FE5461;">*</span><?php } ?>
              </p>
              <p>Rp <?php echo $current_currency_value; ?></p>
            </div>

            <?php if($_POST["harga_beli_emas"] != $current_buying_rate) { ?>
              <p class="term change">
                *Harga emas telah berubah dari <?php echo number_format($_POST["harga_beli_emas"], 0, ".", "."); ?>/gr
                menjadi <?php echo number_format($current_buying_rate, 0, ".", "."); ?>/gr
              </p>
            <?php } ?>

            <p class="button">
              <a class="btn_gldFont LEBtn_blue_white btnMetodePembayaranGld" onclick="metodePembayaranGld('showpopup')">
                Pilih Metode Pembayaran
              </a>
            </p>
            <p class="button" style="margin-top: 0px;">
              <button class="btn_gldFont LEBtn_rainbow btnVerifikasiGldBuy" type="button" disabled onclick="verifikasiGldBuy('showpopup')">
                Beli Emas
              </button>
            </p>

            <p class="term">
              Dengan mengeklik tombol di atas. Kamu sudah menyetujui
              <a href="<?php echo home_url(); ?>/emas/syarat-ketentuan" target="_blank">syarat dan ketentuan</a> yang berlaku.
            </p>

            <div class="d-none">
              <input type="hidden" name="value_gram" class="d-none" value="<?php echo $value_gram; ?>" />
              <input type="hidden" name="value_currency" class="d-none" value="<?php echo $value_currency; ?>" />
              <input type="hidden" name="calculate_type" class="d-none" value="<?php echo $_POST["calculate_type"]; ?>" />
            </div>
          </div>
        </div>
      </div>
    </form>
  </div>
</div>

<?php get_template_part( "ladara-emas/popups/popup", "buy" ); ?>

<?php get_template_part( "ladara-emas/popups/popup", "payment" ); ?>

<?php get_template_part( "ladara-emas/popups/popup", "wallet" ); ?>

<style>
  .row_gldDashboardPage {
    min-height: auto;
  }
</style>

<?php endwhile; ?>
<?php else : ?>
		<?php get_template_part( "content", "404pages" ); ?>	
<?php endif; ?>

<?php get_template_part( "ladara-emas/templates/template", "footer" ); ?>