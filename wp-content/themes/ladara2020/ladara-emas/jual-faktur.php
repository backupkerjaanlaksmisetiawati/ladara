<?php /* Template Name: Ladara Emas - Jual - Faktur */ ?>

<?php get_template_part( "ladara-emas/templates/template", "header" ); ?>

<?php if (have_posts()) : while (have_posts()) : the_post(); ?>

<?php
  $user_id = get_current_user_id();

  $check_user_emas = gld_check_user_emas(array("user_id"=>$user_id));
  $check_user_emas = json_decode($check_user_emas);

  $gldPage = home_url() . "/emas";
?>

<?php if($check_user_emas->code === 200) { ?>
  <?php
    $profile = gld_profile_emas(array("user_id"=>$user_id));
    $profile = json_decode($profile);

    $data_jual_emas = $_SESSION['data_jual_emas'];
  ?>

  <div id="LadaraEmas" class="row_gldDashboardPage">
    <div class="gldDesktop">
      <div class="row">
        <div class="col-md-12">
          <div class="bx_gldBreadcrumb bx_breadcrumbAktivasiLE">
            <div>
              <a href="<?php echo home_url(); ?>/emas">
                <span>&lsaquo;</span>
                <span>Kembali ke Jual/Beli Emas</span>
              </a>
            </div>
          </div>

          <div id="lecheckoutbeliemas">
            <div class="row bx_leBeli row_LEPlus row_gldFinishedActivation">
              <div class="col-md-12">
                <div class="col_gldContent col_finishedBeliEmas">
                  <div class="box">
                    <h3 class="title">Halo, <strong><?php echo $profile->data->name; ?></strong>!</h3>
                  </div>
                  <div class="border">&nbsp;</div>
                  <div class="box_invoiceEmas">
                    <p>
                      Kamu telah melakukan penjualan emas pada <strong><?php echo date("j", strtotime($data_jual_emas->created_at)); ?> <?php echo month_indonesia(date("n", strtotime($data_jual_emas->created_at))); ?> <?php echo date("Y", strtotime($data_jual_emas->created_at)); ?> (<?php echo date("H:i", strtotime($data_jual_emas->created_at)); ?> WIB)</strong> dengan informasi sebagai berikut:
                    </p>

                    <div class="tgl_invoice">
                      <div>
                        <p>Nomor Invoice</p>
                        <p>: <strong><?php echo $data_jual_emas->no_invoice; ?></strong></p>
                      </div>
                      <div>
                        <p>Waktu</p>
                        <p>: <?php echo date("j", strtotime($data_jual_emas->created_at)); ?> <?php echo month_indonesia(date("n", strtotime($data_jual_emas->created_at))); ?> <?php echo date("Y", strtotime($data_jual_emas->created_at)); ?> (<?php echo date("H:i", strtotime($data_jual_emas->created_at)); ?> WIB)</p>
                      </div>
                    </div>

                    <div class="detail_invoice">
                      <div>
                        <p>Nilai Jual Emas</p>
                        <p><strong>Rp <?php echo number_format(round($data_jual_emas->total_currency), 0, ".", "."); ?></strong></p>
                      </div>
                      <div class="red">
                        <p>Jumlah Emas</p>
                        <p><strong><?php echo str_replace(".", ",", round($data_jual_emas->total_emas, 4, PHP_ROUND_HALF_UP)); ?> gram</strong></p>
                      </div>
                      <div>
                        <p>Biaya Admin</p>
                        <p>Rp <?php echo number_format(round($data_jual_emas->partner_fee), 0, ".", "."); ?></p>
                      </div>
                      <div class="total">
                        <p><strong>Total</strong></p>
                        <p><strong>Rp <?php echo number_format(round($data_jual_emas->total_payment), 0, ".", "."); ?></strong></p>
                      </div>
                    </div>

                    <div class="detail_va">
                      <p>Penjualan emas kamu sedang diproses dan<br/>dana kamu akan ditransfer ke:</p>
                      <h4><?php echo $data_jual_emas->bank_name; ?> - <?php echo $data_jual_emas->account_number; ?> a.n <?php echo $data_jual_emas->account_name; ?></h4>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>

    <div class="gldMobile box_mgldDashboard">
      <div class="gldcheckoutbeliemas">
        <div class="bx_leBeli">
          <div class="col_gldContent col_finishedBeliEmas">
            <div class="box">
              <h3 class="title">Halo, <strong><?php echo $profile->data->name; ?></strong>!</h3>
            </div>
            <div class="border">&nbsp;</div>
            <div class="box_invoiceEmas">
              <p>
                Kamu telah melakukan penjualan emas pada <strong><?php echo date("j", strtotime($data_jual_emas->created_at)); ?> <?php echo month_indonesia(date("n", strtotime($data_jual_emas->created_at))); ?> <?php echo date("Y", strtotime($data_jual_emas->created_at)); ?> (<?php echo date("H:i", strtotime($data_jual_emas->created_at)); ?> WIB)</strong> dengan informasi sebagai berikut:
              </p>

              <div class="tgl_invoice">
                <div>
                  <p>Nomor Invoice</p>
                  <p>: <strong><?php echo $data_jual_emas->no_invoice; ?></strong></p>
                </div>
                <div>
                  <p>Waktu</p>
                  <p>: <?php echo date("j", strtotime($data_jual_emas->created_at)); ?> <?php echo month_indonesia(date("n", strtotime($data_jual_emas->created_at))); ?> <?php echo date("Y", strtotime($data_jual_emas->created_at)); ?> (<?php echo date("H:i", strtotime($data_jual_emas->created_at)); ?> WIB)</p>
                </div>
              </div>

              <div class="detail_invoice">
                <div>
                  <p>Nilai Jual Emas</p>
                  <p><strong>Rp <?php echo number_format(round($data_jual_emas->total_currency), 0, ".", "."); ?></strong></p>
                </div>
                <div class="red">
                  <p>Jumlah Emas</p>
                  <p><strong><?php echo str_replace(".", ",", round($data_jual_emas->total_emas, 4, PHP_ROUND_HALF_UP)); ?> gram</strong></p>
                </div>
                <div>
                  <p>Biaya Admin</p>
                  <p>Rp <?php echo number_format(round($data_jual_emas->partner_fee), 0, ".", "."); ?></p>
                </div>
                <div class="total">
                  <p><strong>Total</strong></p>
                  <p><strong>Rp <?php echo number_format(round($data_jual_emas->total_payment), 0, ".", "."); ?></strong></p>
                </div>
              </div>

              <div class="detail_va">
                <p>Penjualan emas kamu sedang diproses dan dana kamu akan ditransfer ke:</p>
                <h4><?php echo $data_jual_emas->bank_name; ?> - <?php echo $data_jual_emas->account_number; ?> a.n <?php echo $data_jual_emas->account_name; ?></h4>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>

<?php } else { ?>
  <style>
    header, body {
      display: none;
    }
  </style>
  <script type="application/javascript">
    setTimeout(function(){
      location.replace('<?php echo $gldPage; ?>'); 
    }, 'fast');
  </script>
<?php } ?>

<?php endwhile; ?>
<?php else : ?>
		<?php get_template_part( 'content', '404pages' ); ?>	
<?php endif; ?>

<?php get_template_part( "ladara-emas/templates/template", "footer" ); ?>