<?php /* Template Name: Ladara Emas */ ?>

<?php get_template_part( "ladara-emas/templates/template", "header" ); ?>

<?php if (have_posts()) : while (have_posts()) : the_post(); ?>

<?php
  $user_id = get_current_user_id();
  $check_user_emas = gld_check_user_emas(array("user_id"=>$user_id));
  $check_user_emas = json_decode($check_user_emas);
?>

<?php if($check_user_emas->code !== 200) { ?>

	<?php get_template_part( "ladara-emas/contents/content", "landingpage" ); ?>

<?php } else { ?>

	<?php
		$login = gld_login(array("user_id"=>$user_id));
		$login = json_decode($login);
		$check_incorrect = ( strstr( $login->message, "incorrect" ) ? "failed" : "success" ); //nggak bisa pake boolean :/
	?>

	<?php if($login->code !== 200) { ?>

		<?php if($login->code === 401 && $check_incorrect === "failed") { ?>

			<?php if($profile->data->trs_user === 1) { ?>
				<?php
					gld_user_exist_handling(array("user_id"=>$user_id));
					$_SESSION["message_error_register_ladara_emas"] = "Credential akun Treasury kamu salah, silahkan update.";
				?>
				<style>
					header, body {
						display: none;
					}
				</style>
				<script type="application/javascript">
					setTimeout(function(){
						location.replace("<?php echo home_url(); ?>/profile/link-to-treasury"); 
					}, "fast");
				</script>
			<?php } else { ?>
				<?php
					// do something kalau user existing di ladara emas, tidak exist di treasury
					// lebih baik, user exist di ladara emas di delete supaya g ada user "sampah" dan suruh user buat aktivasi ulang / register ulang ke treasury
					// setelah itu di redirect ke halaman aktivasi ladara emas
					gld_user_exist_handling(array("user_id"=>$user_id));
					$_SESSION["message_error_register_ladara_emas"] = "Akun Ladara Emas kamu sudah tidak aktif. Silahkan melakukan aktivasi ulang terlebih dahulu.";
				?>
				<style>
					header, body {
						display: none;
					}
				</style>
				<script type="application/javascript">
					setTimeout(function(){
						location.replace("<?php echo home_url(); ?>/emas/aktivasi"); 
					}, "fast");
				</script>
			<?php } ?>

		<?php } else { ?>

			<div id="LadaraEmas">
				<div class="gldDesktop">
					<div class="row row_gldDashboardPage" id="registerEmas">
						<div class="col-md-12">
							<div class="wrap_gldAktivasi" style="height:640px;">
								<div class="bx_gldLoading">
									<img src="<?php bloginfo('template_directory'); ?>/library/images/emas/icon-loading.svg" />
								</div>

								<div id="aktivasiLE_foto_ktp">
									<div class="bx_gldBreadcrumb bx_breadcrumbGldAktivasi">
										<div>
											<a href="<?php echo home_url(); ?>/emas">
												<span>&lsaquo;</span>
												<span>Kembali</span>
											</a>
										</div>
									</div>
									
									<div class="col_gldContent">
										<div class="row row_LEPlus row_gldFinishedActivation">
											<div class="cols upload col-md-12">
												<div>
													<img src="<?php bloginfo('template_directory'); ?>/library/images/emas/img-server-down.svg" />
												</div>
												<h3>Ups!</h3>
												<p><?php echo $login->message; ?></p>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>

				<div class="gldMobile box_mgldDashboard">
					<div class="row_gldDashboardPage" id="mregisterEmas">
						<div class="wrap_gldAktivasi" style="height:690px;">
							<div id="maktivasigld_foto_ktp">
								<div class="col_gldContent">
									<div class="row row_LEPlus row_gldFinishedActivation">
										<div class="cols upload col-md-12">
											<div>
												<img src="<?php bloginfo('template_directory'); ?>/library/images/emas/img-server-down.svg" />
											</div>
											<h3>Ups!</h3>
											<p><?php echo $login->message; ?></p>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>

		<?php } ?>

	<?php } else { ?>
		<?php get_template_part( "ladara-emas/contents/content", "dashboard" ); ?>
	<?php } ?>

<?php } ?>

<?php endwhile; ?>

<?php else : ?>
	<?php get_template_part( 'content', '404pages' ); ?>	
<?php endif; ?>

<?php get_template_part( "ladara-emas/templates/template", "footer" ); ?>