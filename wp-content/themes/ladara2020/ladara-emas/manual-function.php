<?php /* Template Name: Ladara Emas - Manual Function */ ?>

<?php
global $wpdb;

// in case cron didnt work, or something urgent ;;
// for the sake of this project, please put cron in crontab not wordpress cron event T_T

if(isset($_GET["x"])) {

  if($_GET["x"] === "cek_jual") {
    cronJualEmas();
  }

  if($_GET["x"] === "cek_beli") {
    cronBeliEmas();
  }

  if($_GET["x"] === "verifikasi") {
    cronVerifikasiAkunLadaraEmas();
  }

  if($_GET["x"] === "refresh_balance") {
    cronRefreshBalanceEmas();
  }

  if($_GET["x"] === "retrieve_payment") {
    cronRetrievePaymentEmas();
  }

  if($_GET["x"] === "check_payment_status" && isset($_GET["start_date"]) && $_GET["end_date"]) {
    $start_date = $_GET["start_date"]." 00-00-00";
    $end_date = $_GET["end_date"]." 23-59-59";

    gld_check_payment_status(
      array(
        "start_date"  => $start_date,
        "end_date"    => $end_date,
        "show_debug"  => true
      )
    );
  }

} else {

  $html = '<div style="width: 100%; height: 100%; display: flex; justify-content: center; align-items: center; background-color: black; position: relative">';
    $html .= '<p style="font-size: 3rem; color: red; text-align: center; font-family: monospace;">Hey! Go home!<br />You are drunk!</p>';
    $html .= '<p style="font-size: 1rem; color: rgba(255,255,255,0.1); text-align: center; font-family: monospace; position: absolute; bottom: 0px; left: 1em;">Psst... you forgot your param!</p>';
  $html .= '</div>';
  $html .= '<style>body{ margin:0px auto; }</style>';

  echo $html;

}

?>