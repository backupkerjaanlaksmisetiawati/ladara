<?php /* Template Name: Ladara Emas - History */ ?>

<?php get_template_part( "ladara-emas/templates/template", "header" ); ?>

<?php if (have_posts()) : while (have_posts()) : the_post(); ?>

<?php
  $user_id = get_current_user_id();

  $check_user_emas = gld_check_user_emas(array("user_id"=>$user_id));
  $check_user_emas = json_decode($check_user_emas);

  $gldPage = home_url() . "/emas";
?>

<?php if($check_user_emas->code !== 200) { ?>
	<style>
		header, body {
			display: none;
		}
	</style>
	<script type="application/javascript">
		setTimeout(function(){
			location.replace("<?php echo $gldPage; ?>"); 
		}, "fast");
	</script>
<?php } ?>

<?php
  global $wpdb;

  $per_page = 5;

  $profile = gld_profile_emas(array("user_id"=>$user_id));
  $profile = json_decode($profile);
  $user_id = $profile->data->id;

  $history_emas = gld_history(array("type" => "all", "user_id" => $user_id, "limit" => $per_page));
  $history_emas = json_decode($history_emas);
  $total_history_emas = $history_emas->data->total;
  $pages_history_emas = ceil($total_history_emas/$per_page);

  $history_emas_beli = gld_history(array("type" => "buy", "user_id" => $user_id, "limit" => $per_page));
  $history_emas_beli = json_decode($history_emas_beli);
  $total_history_emas_beli = $history_emas_beli->data->total;
  $pages_history_emas_beli = ceil($total_history_emas_beli/$per_page);

  $history_emas_jual = gld_history(array("type" => "sell", "user_id" => $user_id, "limit" => $per_page));
  $history_emas_jual = json_decode($history_emas_jual);
  $total_history_emas_jual = $history_emas_jual->data->total;
  $pages_history_emas_jual = ceil($total_history_emas_jual/$per_page);
?>

<div id="LadaraEmas">
  <div class="gldDesktop row_gldDashboardPage">
    <div class="row">
      <div class="col-md-12">
        <div class="bx_gldBreadcrumb bx_breadcrumbAktivasiLE">
          <div>
            <a href="<?php echo home_url(); ?>">
              <span>&lsaquo;</span>
              <span>Kembali ke Home</span>
            </a>
          </div>
        </div>

        <div class="row">
          <div class="col-sm-3 col-md-3">
            <?php get_template_part( "ladara-emas/contents/content", "menu" ); ?>
          </div>

          <div class="col-sm-9 col-md-9">          
            <div class="col_gldContent">
              <div id="gldHistoryTabs" style="display:none;">
                <ul>
                  <li><a href="#history_semua">Semua</a></li>
                  <li><a href="#history_beliemas">Pembelian</a></li>
                  <li><a href="#history_jualemas">Penjualan</a></li>
                </ul>

                <div id="history_semua" class="bx_goldHistory p-relative">
                  <div class="bx_historyEmas" style="min-height:1000px;">
                    <?php /** semua transaksi emas */ ?>
                    <?php if(!empty($history_emas->data->history)) { ?>
                      <?php foreach($history_emas->data->history as $x => $h) { ?>
                        <div class="col_historyEmas">
                          <div class="col_heStatus">
                            <img src="<?php bloginfo("template_directory"); ?>/library/images/emas/icon-gold-history.svg" alt="icon" />
                          
                            <div>
                              <p>Emas</p>
                              <?php /*<p>Transaksi Pembelian #10010514</p>*/ ?>
                              <p>Status Transaksi</p>
                              <?php if($h->type === 'buy' && strtolower($h->status) === "success") { ?>
                                <p class="success">Pembelian Berhasil</p>
                              <?php } ?>
                              <?php if($h->type === 'buy' && strtolower($h->status) === "pending") { ?>
                                <p class="pending">Pembelian Menunggu Pembayaran</p>
                              <?php } ?>
                              <?php if($h->type === 'buy' && strtolower($h->status) === "canceled") { ?>
                                <p class="failed">Pembelian Telah Dibatalkan</p>
                              <?php } ?>
                              <?php if($h->type === 'buy' && strtolower($h->status) === "failed") { ?>
                                <p class="failed">Pembelian Telah Gagal</p>
                              <?php } ?>
                              <?php if($h->type === 'sell' && strtolower($h->status) === "pending") { ?>
                                <p class="pending">Penjualan Sedang Diproses</p>
                              <?php } ?>
                              <?php if($h->type === 'sell' && strtolower($h->status) === "approved") { ?>
                                <p class="success">Penjualan Telah Disetujui</p>
                              <?php } ?>
                              <?php if($h->type === 'sell' && strtolower($h->status) === "rejected") { ?>
                                <p class="failed">Penjualan Telah Ditolak</p>
                              <?php } ?>

                            </div>
                          </div>

                          <div class="col_heDetail">
                            <p class="tgl">
                              Tanggal Transaksi:
                              <strong>
                                <?php echo date("j", strtotime($h->created_date)); ?> <?php echo month_indonesia(date("n", strtotime($h->created_date))); ?> <?php echo date("Y", strtotime($h->created_date)); ?> <?php echo date("H:i", strtotime($h->created_date)); ?> WIB
                              </strong>
                            </p>

                            <?php /*
                            <p class="id_transaksi">
                              ID Transaksi: <strong>#10010514</strong>
                            </p>
                            */ ?>

                            <div class="box_hePembayaran">
                              <div>
                                <?php if($h->type === 'buy') { ?>
                                  <p>Nominal pembelian</p>
                                <?php } ?>
                                <?php if($h->type === 'sell') { ?>
                                  <p>Nominal penjualan</p>
                                <?php } ?>
                                <p class="amount">
                                  Rp <?php echo number_format($h->total_payment, 0, ".", "."); ?>
                                </p>
                              </div>

                              <div>
                                <?php if($h->type === 'buy') { ?>
                                  <p>Metode pembayaran</p>
                                  <p><?php echo $h->payment_method; ?></p>
                                <?php } ?>
                                <?php if($h->type === 'sell') { ?>
                                  <p>Dana Ditransfer ke</p>
                                  <p>
                                    <?php echo $h->bank_name; ?>
                                    <br />
                                    <?php echo $h->bank_account; ?>
                                    <br />
                                    <span>a.n <?php echo $h->bank_account_name; ?></span>
                                  </p>
                                <?php } ?>
                              </div>

                              <div>
                                <p>Invoice</p>
                                <p>
                                  (<?php echo $h->invoice_order; ?>)
                                </p>
                              </div>
                            </div>

                            <?php if(strtolower($h->status) === "success") { ?>
                              <div class="jmlh_emas">
                                <?php if($h->type === 'buy') { ?>
                                  Emas kamu telah masuk sebanyak
                                  <strong><?php echo str_replace(".", ",", $h->total_gold); ?></strong>gr
                                  ke Akun kamu
                                <?php } ?>
                                <?php if($h->type === 'sell') { ?>
                                  Emas kamu telah terpotong sebanyak 
                                  <strong><?php echo str_replace(".", ",", $h->total_gold); ?></strong>gr 
                                  dari Akun kamu
                                <?php } ?>
                              </div>
                            <?php } ?>

                            <div class="col_heDetailTransaksi">
                              <div class="title transaksi_semua_<?php echo $x; ?>">
                                <?php if($h->type === 'buy') { ?>
                                  <a href="<?php echo home_url(); ?>/emas/transaksi-detail?invoice=<?php echo $h->invoice_order; ?>">
                                   Lihat Invoice
                                  </a>
                                <?php } ?>
                                <?php if($h->type === 'sell') { ?>
                                  <a href="<?php echo home_url(); ?>/emas/transaksi-detail?faktur=<?php echo $h->invoice_order; ?>">
                                    Lihat Invoice
                                  </a>
                                <?php } ?>
                                <span onclick="show_detailHistoryEmas('transaksi_semua_<?php echo $x; ?>')">
                                  Detail Transaksi
                                </span>
                              </div>

                              <div class="jmlh transaksi_semua_<?php echo $x; ?>">
                                <div>
                                  <p>Jumlah Emas</p>
                                  <p><?php echo str_replace(".", ",", $h->total_gold); ?> gram</p>
                                </div>
                                <div>
                                  <?php if($h->type === 'buy') { ?>
                                    <p>Harga Emas Saat Transaksi</p>
                                    <p>Rp <?php echo number_format($h->buying_rate, 0, ".", "."); ?>/gr</p>
                                  <?php } ?>
                                  <?php if($h->type === 'sell') { ?>
                                    <p>Harga Emas Saat Transaksi</p>
                                    <p>Rp <?php echo number_format($h->selling_rate, 0, ".", "."); ?>/gr</p>
                                  <?php } ?>
                                </div>
                              </div>
                            </div>
                          </div>                      
                        </div>
                      <?php } ?>
                    <?php } else { ?>
                      <div class="col_gldContent">
                        <div class="row row_LEPlus row_gldFinishedActivation">
                          <div class="cols upload col-md-12">
                            <div>
                              <img src="<?php bloginfo('template_directory'); ?>/library/images/emas/img-empty-history.svg" />
                            </div>
                            <p>Tidak ada transaksi saat ini</p>
                          </div>
                        </div>
                      </div>
                    <?php } ?>

                    <?php if(!empty($history_emas->data->history)) { ?>
                      <?php if($total_history_emas > $per_page) { ?>
                        <div id="gld_history_all" class="col_historyEmas page_emas"
                          data-type="all"
                          rel="2"
                          data-pages="<?php echo $pages_history_emas; ?>"
                          data-perpage="<?php echo $per_page; ?>"
                        >Transaksi Sebelumnya</div>
                      <?php } ?>
                    <?php } ?>
                    <?php /** semua transaksi emas */ ?>
                  </div>
                </div>

                <div id="history_beliemas" class="bx_goldHistory p-relative">
                  <div class="bx_historyEmas" style="min-height:1000px;">
                    <?php /** beli emas */ ?>
                    <?php if(!empty($history_emas_beli->data->history)) { ?>
                      <?php foreach($history_emas_beli->data->history as $x => $h) { ?>
                        <div class="col_historyEmas">
                          <div class="col_heStatus">
                            <img src="<?php bloginfo("template_directory"); ?>/library/images/emas/icon-gold-history.svg" alt="icon" />
                          
                            <div>
                              <p>Emas</p>
                              <?php /*<p>Transaksi Pembelian #10010514</p>*/ ?>
                              <p>Status Transaksi</p>
                              <?php if(strtolower($h->status) === "success") { ?>
                                <p class="success">Pembelian Telah Berhasil</p>
                              <?php } ?>
                              <?php if(strtolower($h->status) === "pending") { ?>
                                <p class="pending">Pembelian Menunggu Pembayaran</p>
                              <?php } ?>
                              <?php if(strtolower($h->status) === "canceled") { ?>
                                <p class="failed">Pembelian Telah Dibatalkan</p>
                              <?php } ?>
                              <?php if(strtolower($h->status) === "failed") { ?>
                                <p class="failed">Pembelian Telah Gagal</p>
                              <?php } ?>
                            </div>
                          </div>

                          <div class="col_heDetail">
                            <p class="tgl">
                              Tanggal Transaksi:
                              <strong>
                                <?php echo date("j", strtotime($h->created_date)); ?> <?php echo month_indonesia(date("n", strtotime($h->created_date))); ?> <?php echo date("Y", strtotime($h->created_date)); ?> <?php echo date("H:i", strtotime($h->created_date)); ?> WIB
                              </strong>
                            </p>

                            <?php /*
                            <p class="id_transaksi">
                              ID Transaksi: <strong>#10010514</strong>
                            </p>
                            */ ?>

                            <div class="box_hePembayaran">
                              <div>
                                <p>Nominal pembelian</p>
                                <p class="amount">
                                  Rp <?php echo number_format($h->total_payment, 0, ".", "."); ?>
                                </p>
                              </div>
                              <div>
                                <p>Metode pembayaran</p>
                                <p><?php echo $h->payment_method; ?></p>
                              </div>
                              <div>
                                <p>Invoice</p>
                                <p>
                                  (<?php echo $h->invoice_order; ?>)
                                </p>
                              </div>
                            </div>

                            <?php if(strtolower($h->status) === "success") { ?>
                              <div class="jmlh_emas">
                                Emas kamu telah masuk sebanyak
                                <strong><?php echo str_replace(".", ",", $h->total_gold); ?></strong>gr
                                ke Akun kamu
                              </div>
                            <?php } ?>

                            <div class="col_heDetailTransaksi">
                              <div class="title transaksi_beli_<?php echo $x; ?>">
                                <a href="<?php echo home_url(); ?>/emas/transaksi-detail?invoice=<?php echo $h->invoice_order; ?>">
                                  Lihat Invoice
                                </a>
                                <span onclick="show_detailHistoryEmas('transaksi_beli_<?php echo $x; ?>')">
                                  Detail Transaksi
                                </span>
                              </div>

                              <div class="jmlh transaksi_beli_<?php echo $x; ?>">
                                <div>
                                  <p>Jumlah Emas</p>
                                  <p>
                                    <?php echo str_replace(".", ",", $h->total_gold); ?> gram
                                  </p>
                                </div>
                                <div>
                                  <p>Harga Emas Saat Transaksi</p>
                                  <p>Rp <?php echo number_format($h->buying_rate, 0, ".", "."); ?>/gr</p>
                                </div>
                              </div>
                            </div>
                          </div>                      
                        </div>
                      <?php } ?>
                    <?php } else { ?>
                      <div class="col_gldContent">
                        <div class="row row_LEPlus row_gldFinishedActivation">
                          <div class="cols upload col-md-12">
                            <div>
                              <img src="<?php bloginfo('template_directory'); ?>/library/images/emas/img-empty-history.svg" />
                            </div>
                            <p>Tidak ada transaksi saat ini</p>
                          </div>
                        </div>
                      </div>
                    <?php } ?>

                    <?php if(!empty($history_emas_beli->data->history)) { ?>
                      <?php if($total_history_emas_beli > $per_page) { ?>
                        <div id="gld_history_buy" class="col_historyEmas page_emas"
                          data-type="buy"
                          rel="2"
                          data-pages="<?php echo $pages_history_emas_beli; ?>"
                          data-perpage="<?php echo $per_page; ?>"
                        >Transaksi Sebelumnya</div>
                      <?php } ?>
                    <?php } ?>
                    <?php /** beli emas */ ?>
                  </div>
                </div>

                <div id="history_jualemas" class="bx_goldHistory p-relative">
                  <div class="bx_historyEmas" style="min-height:1000px;">
                    <?php /** jual emas */ ?>
                    <?php if(!empty($history_emas_jual->data->history)) { ?>
                      <?php foreach($history_emas_jual->data->history as $x => $h) { ?>
                        <div class="col_historyEmas">
                          <div class="col_heStatus">
                            <img src="<?php bloginfo("template_directory"); ?>/library/images/emas/icon-gold-history.svg" alt="icon" />
                          
                            <div>
                              <p>Emas</p>
                              <?php /*<p>Transaksi Penjualan #10010514</p>*/ ?>
                              <p>Status Transaksi</p>
                              <?php if(strtolower($h->status) === "approved") { ?>
                                <p class="success">Penjualan Telah Disetujui</p>
                              <?php } ?>
                              <?php if(strtolower($h->status) === "pending") { ?>
                                <p class="pending">Penjualan Sedang Diproses</p>
                              <?php } ?>
                              <?php if(strtolower($h->status) === "rejected") { ?>
                                <p class="failed">Penjualan Telah Ditolak</p>
                              <?php } ?>
                            </div>
                          </div>

                          <div class="col_heDetail">
                            <p class="tgl">
                              Tanggal Transaksi:
                              <strong>
                                <?php echo date("j", strtotime($h->created_date)); ?> <?php echo month_indonesia(date("n", strtotime($h->created_date))); ?> <?php echo date("Y", strtotime($h->created_date)); ?> <?php echo date("H:i", strtotime($h->created_date)); ?> WIB
                              </strong>
                            </p>

                            <?php /*
                            <p class="id_transaksi">
                              ID Transaksi: <strong>#10010514</strong>
                            </p>
                            */ ?>                        

                            <div class="box_hePembayaran">
                              <div>
                                <p>Nominal penjualan</p>
                                <p class="amount">
                                  Rp <?php echo number_format($h->total_payment, 0, ".", "."); ?>
                                </p>
                              </div>
                              <div>
                                <p>Dana Ditransfer ke</p>
                                <p>
                                  <?php echo $h->bank_name; ?>
                                  <br />
                                  <?php echo $h->bank_account; ?>
                                  <br />
                                  <span>a.n <?php echo $h->bank_account_name; ?></span>
                                </p>
                              </div>
                              <div>
                                <p>Invoice</p>
                                <p>
                                  (<?php echo $h->invoice_order; ?>)
                                </p>
                              </div>
                            </div>

                            <?php if(strtolower($h->status) === "success") { ?>
                              <div class="jmlh_emas">
                                Emas kamu telah terpotong sebanyak 
                                <strong><?php echo str_replace(".", ",", $h->total_gold); ?></strong>gr 
                                dari Akun kamu
                              </div>
                            <?php } ?>

                            <div class="col_heDetailTransaksi">
                              <div class="title transaksi_jual_<?php echo $x; ?>">
                                <a href="<?php echo home_url(); ?>/emas/transaksi-detail?faktur=<?php echo $h->invoice_order; ?>">
                                  Lihat Invoice
                                </a>
                                <span onclick="show_detailHistoryEmas('transaksi_jual_<?php echo $x; ?>')">
                                  Detail Transaksi
                                </span>
                              </div>

                              <div class="jmlh transaksi_jual_<?php echo $x; ?>">
                                <div>
                                  <p>Jumlah Emas</p>
                                  <p><?php echo str_replace(".", ",", $h->total_gold); ?> gram</p>
                                </div>
                                <div>
                                  <p>Harga Emas Saat Transaksi</p>
                                  <p>Rp <?php echo number_format($h->selling_rate, 0, ".", "."); ?>/gr</p>
                                </div>
                              </div>
                            </div>
                          </div>                      
                        </div>
                      <?php } ?>
                    <?php } else { ?>
                      <div class="col_gldContent">
                        <div class="row row_LEPlus row_gldFinishedActivation">
                          <div class="cols upload col-md-12">
                            <div>
                              <img src="<?php bloginfo('template_directory'); ?>/library/images/emas/img-empty-history.svg" />
                            </div>
                            <p>Tidak ada transaksi saat ini</p>
                          </div>
                        </div>
                      </div>
                    <?php } ?>

                    <?php if(!empty($history_emas_jual->data->history)) { ?>
                      <?php if($total_history_emas_jual > $per_page) { ?>
                        <div id="gld_history_sell" class="col_historyEmas page_emas"
                          data-type="sell"
                          rel="2"
                          data-pages="<?php echo $total_history_emas_jual; ?>"
                          data-perpage="<?php echo $per_page; ?>"
                        >Transaksi Sebelumnya</div>
                      <?php } ?>
                    <?php } ?>
                    <?php /** jual emas */ ?>
                  </div>
                </div>

              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>

  <div class="gldMobile" style="min-height: 1590px;">       
    <div class="col_gldContent no-margin">
      <div id="mgldHistoryTabs" style="display:none;">
        <ul>
          <li><a href="#mhistory_semua">Semua</a></li>
          <li><a href="#mhistory_beliemas">Pembelian</a></li>
          <li><a href="#mhistory_jualemas">Penjualan</a></li>
        </ul>

        <div id="mhistory_semua" class="bx_goldHistory p-relative">
          <div class="bx_historyEmas" style="min-height:1000px;">
            <?php /** semua transaksi emas mobile */ ?>
            <?php if(!empty($history_emas->data->history)) { ?>
              <?php foreach($history_emas->data->history as $x => $h) { ?>
                <div class="col_historyEmas">
                  <div class="col_heStatus">
                    <div>
                      <img src="<?php bloginfo("template_directory"); ?>/library/images/emas/icon-gold-history.svg" alt="icon" />
                      <p>Emas</p>
                    </div>

                    <p class="tgl">
                      Tanggal Transaksi
                      <br />
                      <strong>
                        <?php echo date("j", strtotime($h->created_date)); ?> <?php echo month_indonesia(date("n", strtotime($h->created_date))); ?> <?php echo date("Y", strtotime($h->created_date)); ?> <?php echo date("H:i", strtotime($h->created_date)); ?> WIB
                      </strong>
                    </p>
                  </div>

                  <div class="col_heDetail">
                    <div class="box_hePembayaran">
                      <div>
                        <?php if($h->type === 'buy') { ?>
                          <p>Nominal pembelian</p>
                        <?php } ?>
                        <?php if($h->type === 'sell') { ?>
                          <p>Nominal penjualan</p>
                        <?php } ?>
                        <p class="amount">
                          Rp <?php echo number_format($h->total_payment, 0, ".", "."); ?>
                        </p>
                      </div>

                      <div>
                        <?php if($h->type === 'buy') { ?>
                          <p>Metode pembayaran</p>
                          <p><?php echo $h->payment_method; ?></p>
                        <?php } ?>
                        <?php if($h->type === 'sell') { ?>
                          <p>Dana Ditransfer ke</p>
                          <p>
                            <?php echo $h->bank_name; ?>
                            <br />
                            <?php echo $h->bank_account; ?>
                            <br />
                            <span>a.n <?php echo $h->bank_account_name; ?></span>
                          </p>
                        <?php } ?>
                      </div>

                      <div>
                        <p>Invoice</p>
                        <p>
                          (<?php echo $h->invoice_order; ?>)
                        </p>
                      </div>

                      <div>
                        <p>Status Transaksi</p>
                        <?php if($h->type === 'buy' && strtolower($h->status) === "success") { ?>
                          <p class="success">Pembelian Berhasil</p>
                        <?php } ?>
                        <?php if($h->type === 'buy' && strtolower($h->status) === "pending") { ?>
                          <p class="pending">Pembelian Menunggu Pembayaran</p>
                        <?php } ?>
                        <?php if($h->type === 'buy' && strtolower($h->status) === "canceled") { ?>
                          <p class="failed">Pembelian Telah Dibatalkan</p>
                        <?php } ?>
                        <?php if($h->type === 'buy' && strtolower($h->status) === "failed") { ?>
                          <p class="failed">Pembelian Telah Gagal</p>
                        <?php } ?>
                        <?php if($h->type === 'sell' && strtolower($h->status) === "pending") { ?>
                          <p class="pending">Penjualan Sedang Diproses</p>
                        <?php } ?>
                        <?php if($h->type === 'sell' && strtolower($h->status) === "approved") { ?>
                          <p class="success">Penjualan Telah Disetujui</p>
                        <?php } ?>
                        <?php if($h->type === 'sell' && strtolower($h->status) === "rejected") { ?>
                          <p class="failed">Penjualan Telah Ditolak</p>
                        <?php } ?>
                      </div>
                    </div>

                    <?php if(strtolower($h->status) === "success") { ?>
                      <div class="jmlh_emas">
                        <?php if($h->type === 'buy') { ?>
                          Emas kamu telah masuk sebanyak
                          <strong><?php echo str_replace(".", ",", $h->total_gold); ?></strong>gr
                          ke Akun kamu
                        <?php } ?>
                        <?php if($h->type === 'sell') { ?>
                          Emas kamu telah terpotong sebanyak 
                          <strong><?php echo str_replace(".", ",", $h->total_gold); ?></strong>gr 
                          dari Akun kamu
                        <?php } ?>
                      </div>
                    <?php } ?>

                    <div class="col_heDetailTransaksi">
                      <div class="title mtransaksi_semua_<?php echo $x; ?>">
                        <div>
                          <p>
                            <?php if($h->type === 'buy') { ?>
                              <a href="<?php echo home_url(); ?>/emas/transaksi-detail?invoice=<?php echo $h->invoice_order; ?>">
                                Lihat Invoice
                              </a>
                            <?php } ?>
                            <?php if($h->type === 'sell') { ?>
                              <a href="<?php echo home_url(); ?>/emas/transaksi-detail?faktur=<?php echo $h->invoice_order; ?>">
                                Lihat Invoice
                              </a>
                            <?php } ?>
                          </p>
                        </div>
                        <span onclick="show_detailHistoryEmas('mtransaksi_semua_<?php echo $x; ?>')">
                          Detail Transaksi
                        </span>
                      </div>

                      <div class="jmlh mtransaksi_semua_<?php echo $x; ?>">
                        <div>
                          <p>Jumlah Emas</p>
                          <p><?php echo str_replace(".", ",", $h->total_gold); ?> gram</p>
                        </div>
                        <div>
                          <?php if($h->type === 'buy') { ?>
                            <p>Harga Emas Saat Transaksi</p>
                            <p>Rp <?php echo number_format($h->buying_rate, 0, ".", "."); ?>/gr</p>
                          <?php } ?>
                          <?php if($h->type === 'sell') { ?>
                            <p>Harga Emas Saat Transaksi</p>
                            <p>Rp <?php echo number_format($h->selling_rate, 0, ".", "."); ?>/gr</p>
                          <?php } ?>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              <?php } ?>
            <?php } else { ?>
              <div class="col_gldContent">
                <div class="row row_LEPlus row_gldFinishedActivation">
                  <div class="cols upload col-md-12">
                    <div>
                      <img src="<?php bloginfo('template_directory'); ?>/library/images/emas/img-empty-history.svg" />
                    </div>
                    <p>Tidak ada transaksi saat ini</p>
                  </div>
                </div>
              </div>
            <?php } ?>

            <?php if(!empty($history_emas->data->history)) { ?>
              <?php if($total_history_emas > $per_page) { ?>
                <div id="gld_history_mall" class="col_historyEmas page_emas"
                  data-type="mall"
                  rel="2"
                  data-pages="<?php echo $pages_history_emas; ?>"
                  data-perpage="<?php echo $per_page; ?>"
                >Transaksi Sebelumnya</div>
              <?php } ?>
            <?php } ?>
            <?php /** semua transaksi emas mobile */ ?>
          </div>
        </div>

        <div id="mhistory_beliemas" class="bx_goldHistory p-relative">
          <div class="bx_historyEmas" style="min-height:1000px;">
            <?php /** beli emas mobile */ ?>
            <?php if(!empty($history_emas_beli->data->history)) { ?>
              <?php foreach($history_emas_beli->data->history as $x => $h) { ?>
                <div class="col_historyEmas">
                  <div class="col_heStatus">
                    <div>
                      <img src="<?php bloginfo("template_directory"); ?>/library/images/emas/icon-gold-history.svg" alt="icon" />
                      <p>Emas</p>
                    </div>

                    <p class="tgl">
                      Tanggal Transaksi
                      <br />
                      <strong>
                        <?php echo date("j", strtotime($h->created_date)); ?> <?php echo month_indonesia(date("n", strtotime($h->created_date))); ?> <?php echo date("Y", strtotime($h->created_date)); ?> <?php echo date("H:i", strtotime($h->created_date)); ?> WIB
                      </strong>
                    </p>
                  </div>

                  <div class="col_heDetail">
                    <div class="box_hePembayaran">
                      <div>
                        <p>Nominal pembelian</p>
                        <p class="amount">
                          Rp <?php echo number_format($h->total_payment, 0, ".", "."); ?>
                        </p>
                      </div>

                      <div>
                        <p>Metode pembayaran</p>
                        <p><?php echo $h->payment_method; ?></p>
                      </div>

                      <div>
                        <p>Invoice</p>
                        <p>
                          (<?php echo $h->invoice_order; ?>)
                        </p>
                      </div>

                      <div>
                        <p>Status Transaksi</p>
                        <?php if($h->type === 'buy' && strtolower($h->status) === "success") { ?>
                          <p class="success">Pembelian Berhasil</p>
                        <?php } ?>
                        <?php if($h->type === 'buy' && strtolower($h->status) === "pending") { ?>
                          <p class="pending">Pembelian Menunggu Pembayaran</p>
                        <?php } ?>
                        <?php if($h->type === 'buy' && strtolower($h->status) === "canceled") { ?>
                          <p class="failed">Pembelian Telah Dibatalkan</p>
                        <?php } ?>
                        <?php if($h->type === 'buy' && strtolower($h->status) === "failed") { ?>
                          <p class="failed">Pembelian Telah Gagal</p>
                        <?php } ?>
                      </div>
                    </div>

                    <?php if(strtolower($h->status) === "success") { ?>
                      <div class="jmlh_emas">
                        Emas kamu telah masuk sebanyak
                        <strong><?php echo str_replace(".", ",", $h->total_gold); ?></strong>gr
                        ke Akun kamu
                      </div>
                    <?php } ?>

                    <div class="col_heDetailTransaksi">
                      <div class="title mtransaksi_semua_<?php echo $x; ?>">
                        <div>
                          <p>
                            <a href="<?php echo home_url(); ?>/emas/transaksi-detail?invoice=<?php echo $h->invoice_order; ?>">
                              Lihat Invoice
                            </a>
                          </p>
                        </div>
                        <span onclick="show_detailHistoryEmas('mtransaksi_semua_<?php echo $x; ?>')">
                          Detail Transaksi
                        </span>
                      </div>

                      <div class="jmlh mtransaksi_semua_<?php echo $x; ?>">
                        <div>
                          <p>Jumlah Emas</p>
                          <p><?php echo str_replace(".", ",", $h->total_gold); ?> gram</p>
                        </div>
                        <div>
                          <p>Harga Emas Saat Transaksi</p>
                          <p>Rp <?php echo number_format($h->buying_rate, 0, ".", "."); ?>/gr</p>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              <?php } ?>
            <?php } else { ?>
              <div class="col_gldContent">
                <div class="row row_LEPlus row_gldFinishedActivation">
                  <div class="cols upload col-md-12">
                    <div>
                      <img src="<?php bloginfo('template_directory'); ?>/library/images/emas/img-empty-history.svg" />
                    </div>
                    <p>Tidak ada transaksi saat ini</p>
                  </div>
                </div>
              </div>
            <?php } ?>

            <?php if(!empty($history_emas_beli->data->history)) { ?>
              <?php if($total_history_emas_beli > $per_page) { ?>
                <div id="gld_history_mbuy" class="col_historyEmas page_emas"
                  data-type="mbuy"
                  rel="2"
                  data-pages="<?php echo $pages_history_emas_beli; ?>"
                  data-perpage="<?php echo $per_page; ?>"
                >Transaksi Sebelumnya</div>
              <?php } ?>
            <?php } ?>
            <?php /** beli emas mobile */ ?>
          </div>
        </div>

        <div id="mhistory_jualemas" class="bx_goldHistory p-relative">
          <div class="bx_historyEmas" style="min-height:1000px;">
            <?php /** jual emas mobile */ ?>
            <?php if(!empty($history_emas_jual->data->history)) { ?>
              <?php foreach($history_emas_jual->data->history as $x => $h) { ?>
                <div class="col_historyEmas">
                  <div class="col_heStatus">
                    <div>
                      <img src="<?php bloginfo("template_directory"); ?>/library/images/emas/icon-gold-history.svg" alt="icon" />
                      <p>Emas</p>
                    </div>

                    <p class="tgl">
                      Tanggal Transaksi
                      <br />
                      <strong>
                        <?php echo date("j", strtotime($h->created_date)); ?> <?php echo month_indonesia(date("n", strtotime($h->created_date))); ?> <?php echo date("Y", strtotime($h->created_date)); ?> <?php echo date("H:i", strtotime($h->created_date)); ?> WIB
                      </strong>
                    </p>
                  </div>

                  <div class="col_heDetail">
                    <div class="box_hePembayaran">
                      <div>
                        <p>Nominal penjualan</p>
                        <p class="amount">
                          Rp <?php echo number_format($h->total_payment, 0, ".", "."); ?>
                        </p>
                      </div>

                      <div>
                        <p>Dana Ditransfer ke</p>
                        <p>
                          <?php echo $h->bank_name; ?>
                          <br />
                          <?php echo $h->bank_account; ?>
                          <br />
                          <span>a.n <?php echo $h->bank_account_name; ?></span>
                        </p>
                      </div>

                      <div>
                        <p>Invoice</p>
                        <p>
                          (<?php echo $h->invoice_order; ?>)
                        </p>
                      </div>

                      <div>
                        <p>Status Transaksi</p>
                        <?php if($h->type === 'sell' && strtolower($h->status) === "pending") { ?>
                          <p class="pending">Penjualan Sedang Diproses</p>
                        <?php } ?>
                        <?php if($h->type === 'sell' && strtolower($h->status) === "approved") { ?>
                          <p class="success">Penjualan Telah Disetujui</p>
                        <?php } ?>
                        <?php if($h->type === 'sell' && strtolower($h->status) === "rejected") { ?>
                          <p class="failed">Penjualan Telah Ditolak</p>
                        <?php } ?>
                      </div>
                    </div>

                    <?php if(strtolower($h->status) === "success") { ?>
                      <div class="jmlh_emas">
                        Emas kamu telah terpotong sebanyak 
                        <strong><?php echo str_replace(".", ",", $h->total_gold); ?></strong>gr 
                        dari Akun kamu
                      </div>
                    <?php } ?>

                    <div class="col_heDetailTransaksi">
                      <div class="title mtransaksi_semua_<?php echo $x; ?>">
                        <div>
                          <p>
                            <a href="<?php echo home_url(); ?>/emas/transaksi-detail?faktur=<?php echo $h->invoice_order; ?>">
                              Lihat Invoice
                            </a>
                          </p>
                        </div>
                        <span onclick="show_detailHistoryEmas('mtransaksi_semua_<?php echo $x; ?>')">
                          Detail Transaksi
                        </span>
                      </div>

                      <div class="jmlh mtransaksi_semua_<?php echo $x; ?>">
                        <div>
                          <p>Jumlah Emas</p>
                          <p><?php echo str_replace(".", ",", $h->total_gold); ?> gram</p>
                        </div>
                        <div>
                          <p>Harga Emas Saat Transaksi</p>
                          <p>Rp <?php echo number_format($h->selling_rate, 0, ".", "."); ?>/gr</p>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              <?php } ?>
            <?php } else { ?>
              <div class="col_gldContent">
                <div class="row row_LEPlus row_gldFinishedActivation">
                  <div class="cols upload col-md-12">
                    <div>
                      <img src="<?php bloginfo('template_directory'); ?>/library/images/emas/img-empty-history.svg" />
                    </div>
                    <p>Tidak ada transaksi saat ini</p>
                  </div>
                </div>
              </div>
            <?php } ?>

            <?php if(!empty($history_emas_jual->data->history)) { ?>
              <?php if($total_history_emas_jual > $per_page) { ?>
                <div id="gld_history_msell" class="col_historyEmas page_emas"
                  data-type="msell"
                  rel="2"
                  data-pages="<?php echo $total_history_emas_jual; ?>"
                  data-perpage="<?php echo $per_page; ?>"
                >Transaksi Sebelumnya</div>
              <?php } ?>
            <?php } ?>
            <?php /** jual emas mobile */ ?>
          </div>
        </div>

      </div>
    </div>
  </div>
</div>

<?php endwhile; ?>
<?php else : ?>
		<?php get_template_part( "content", "404pages" ); ?>	
<?php endif; ?>

<?php get_template_part( "ladara-emas/templates/template", "footer" ); ?>