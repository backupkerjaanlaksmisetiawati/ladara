<?php /* Template Name: Ladara Emas - Default*/ ?>

<?php get_template_part( "ladara-emas/templates/template", "header" ); ?>

<?php if (have_posts()) : while (have_posts()) : the_post(); ?>

<div id="LadaraEmas">
  <article id="post-<?php the_ID(); ?>" <?php post_class( 'clearfix' ); ?> role="article" itemscope itemtype="http://schema.org/BlogPosting">
    <div class="gldDesktop">
      <div class="row row_globalPage page_gldContent page_gld<?php echo ucfirst(str_replace("-", "", $post->post_name)); ?>">
        <div class="col-md-12">
        
          <div class="bx_gldBreadcrumb bx_breadcrumbAktivasiLE">
            <div>
              <a href="<?php echo home_url(); ?>/emas">
                <span>&lsaquo;</span>
                <span>Kembali ke Jual/Beli Emas</span>
              </a>
            </div>
          </div>

          <div class="bx_globalPage">

            <div class="row row_title_globalPage">
              <div class="col-md-8 col_globalPage">
                <h1 class="ht_globalPage"><?php echo get_the_title($post->ID); ?></h1>
              </div>
              <div class="col-md-4 ">
                <div class="mg_globalPage animated fadeIn">
                  <img src="<?php bloginfo('template_directory'); ?>/library/images/emas/logo-ladara-emas.png">
                </div>
              </div>
            </div>

            <div class="content_globalPage">
              <?php the_content(); ?>
            </div>

          </div>
        </div>
      </div>
    </div>

    <div class="gldMobile box_mgldDashboard">
      <div class="row row_globalPage page_gldContent page_gld<?php echo ucfirst(str_replace("-", "", $post->post_name)); ?>">
        <div class="bx_globalPage">

          <div class="row_title_globalPage" style="display:block">
            <div class="">
              <div class="animated fadeIn">
                <img src="<?php bloginfo('template_directory'); ?>/library/images/emas/logo-ladara-emas-large.png" style="max-width:100%;" />
              </div>
            </div>
            <div class="col_globalPage">
              <h1 class="ht_globalPage"><?php echo get_the_title($post->ID); ?></h1>
            </div>
          </div>

          <div class="content_globalPage">
            <?php the_content(); ?>
          </div>

        </div>
      </div>
    </div>
  </article>
</div>

<?php endwhile; ?>
<?php else : ?>
		<?php get_template_part( 'content', '404pages' ); ?>	
<?php endif; ?>

<?php get_template_part( "ladara-emas/templates/template", "footer" ); ?>