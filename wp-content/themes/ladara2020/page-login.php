<?php
/*
Template Name: Login Page
*/
?>
<?php get_header(); ?>
<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
<?php 
require_once 'vendor-fb/autoload.php';

$fb = new Facebook\Facebook([
  'app_id' => getenv('FACEBOOK_APP_ID'),
  'app_secret' => getenv('FACEBOOK_APP_SECRET'),
  'default_graph_version' => getenv('FACEBOOK_DEFAULT_GRAPH_VERSION')
]);

$helper = $fb->getRedirectLoginHelper();

if (isset($helper)) {

	$permissions = ['email']; // Optional permissions
	$callbackUrl = getenv('FACEBOOK_CALLBACK_URL');
	$loginUrl_fb = $helper->getLoginUrl($callbackUrl, $permissions);

	if(isset($_SESSION['fblog']) AND $_SESSION['fblog'] == $loginUrl_fb){
		session_destroy();
	}else{
		$_SESSION['fblog'] = $loginUrl_fb;
	}
}
// ================ custom google auth ================
include('content-function-login.php'); 
$login_button = '';

if(!isset($_SESSION['access_token'])){
	$url_googleLogin = $google_client->createAuthUrl();
}else{
	$url_googleLogin = '#';

	// ================ custom logout social media ================
	//Reset OAuth access token
	$google_client->revokeToken();
	//Destroy entire session data.
	session_destroy();
	// ================ custom logout social media ================

}

$current_user = wp_get_current_user();
$u_id = $current_user->ID;

if(isset($_SESSION['rfr']) AND $_SESSION['rfr'] != ''){
	$referer = $_SESSION['rfr'];
}else{

	$rfr = wp_get_referer();
	if(isset($rfr) AND $rfr != ''){
		$referer = wp_get_referer();
		$_SESSION['rfr'] = $referer;
	}else{
		$referer = home_url();
	}

}
?>

<style type="text/css">
	.bx_head_bottom{
		display: none !important;
	}
#wp-submit{
	background: #0080FF;
}
</style>
<div class="row"></div>
<div class="row row_login">
	<div class="col-md-12 col_login">
		<div class="box_login">

				<?php 
					if(isset($u_id) AND $u_id != '' AND $u_id != '0'){

						unset($_SESSION['rfr']); // unset refferer
				?>
					<div class="ok_login">
						<span class="glyphicon glyphicon-ok ok_icon"></span>
						<div class="ht_okLogin">Selamat datang di Ladara</div>
						<span>Mohon menunggu...</span>
						<img src="<?php bloginfo('template_directory'); ?>/library/images/loading.gif">
					</div>

					<div id="redirect_ok" data-hurl="<?php echo home_url(); ?>"></div>
					<script>
						var plant = document.getElementById('redirect_ok');
						var hurl = plant.getAttribute('data-hurl'); 
					    setTimeout(function(){
				          location.replace(hurl); 
				        }, 2000);
					</script>
				<?php
					}else{
				?>
						<h1 class="ht_login">Selamat datang di Ladara</h1>

					    <form id="formLogin" method="post" autocomplete="nope" action="<?php bloginfo('url'); ?>/login/" class="wp-user-form formLogin" onsubmit="submit_LoginNow(event);">
					        <input autocomplete="nope" name="hidden" type="text" style="display:none;">

							<?php 
								if(isset($_GET['login']) AND $_GET['login'] == 'failed'){
							?>
								<div class="alert alert-danger">
								  <strong>Maaf,</strong> email atau password salah. Silahkan coba kembali.
								</div>
							<?php
								}
							?>
						        <div class="f_ad_input">
						       
						        	<input type="email" ng-model="username" name="log" id="user_login" class="ad_input" placeholder="Masukkan Email" value="" required="required"  maxlength="50" />
						     
						        	<div id="err_email" class="f_err">*Masukkan Email</div>
						        </div>

						        <div class="f_ad_input">
						        
						        	<input type="password" name="pwd" id="user_pass" class="ad_input" placeholder="Masukkan kata sandi" value="" required="required" autocomplete="nope" minlength="5"/>
						        	<span id="open_password" class="glyphicon glyphicon-eye-close open_password" title="Lihat/sembunyikan password."></span>
				
						        	<div id="err_pwd" class="f_err">*Masukkan Kata Sandi</div>
						        </div>
						        
						        <div class="f_errLogin h_errLogin"></div>
						    
						        <div class="login_fields">
						            <?php // do_action('login_form'); ?>
						            <input type="submit" name="user-submit" id="wp-submit" class="sub_login" value="Masuk"/>
						            <input type="hidden" name="redirect_to" value="<?php echo esc_attr($referer); ?>" />
						            <input type="hidden" name="user-cookie" value="1" />
						            <?php wp_nonce_field( 'login_submit' ); ?>
						        </div>
					    </form>

					    <div class="info_login">atau masuk dengan</div>

					    <a href="<?php echo $loginUrl_fb; ?>" title="Login dengan akun facebook.">
						    <div id="sub_facebook" class="btn_logSocial login_facebook">
						    	<img src="<?php bloginfo('template_directory'); ?>/library/images/icon_fb.svg"> Facebook
						    </div>
					    </a>

					    <a href="<?php echo $url_googleLogin; ?>" title="Login dengan akun google.">
						    <div id="sub_google" class="btn_logSocial login_google">
						    	<img src="<?php bloginfo('template_directory'); ?>/library/images/icon_google.svg"> Google
						    </div>
					    </a>

					    <div class="a_bergabung">
						    Belum punya akun Ladara? <a href="<?php echo home_url(); ?>/daftar/" title="Daftar Sekarang!">Daftar</a>
					    </div>
					    <div class="a_bergabung">
					    	<a class="" href="<?php echo home_url(); ?>/lupa-password/">Lupa kata sandi?</a>
						</div>
				<?php
					}
				 ?>
		</div>
	</div>
</div>

<?php // ============= cannot back ============= ?>
<script type="text/javascript">
    (function (global) { 

        if(typeof (global) === "undefined") {
            throw new Error("window is undefined");
        }

        var _hash = "!";
        var noBackPlease = function () {
            global.location.href += "#";

            // making sure we have the fruit available for juice (^__^)
            global.setTimeout(function () {
                global.location.href += "!";
            }, 50);
        };

        global.onhashchange = function () {
            if (global.location.hash !== _hash) {
                global.location.hash = _hash;
            }
        };

        global.onload = function () {            
            noBackPlease();

            // disables backspace on page except on input fields and textarea..
            document.body.onkeydown = function (e) {
                var elm = e.target.nodeName.toLowerCase();
                if (e.which === 8 && (elm !== 'input' && elm  !== 'textarea')) {
                    e.preventDefault();
                }
                // stopping event bubbling up the DOM tree..
                e.stopPropagation();
            };          
        }

    })(window);
</script>

<?php endwhile; ?>
<?php else : ?>
		<?php get_template_part( 'content', '404pages' ); ?>	
<?php endif; ?>
<?php get_footer(); ?>