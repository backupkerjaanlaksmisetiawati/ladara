<?php
/*
Template Name: Tentang Kami Page
*/
?>
<?php get_header(); 
 if (have_posts()) : while (have_posts()) : the_post(); ?>

<?php 
$current_user = wp_get_current_user();
$u_id = $current_user->ID;
?>

<article id="post-<?php the_ID(); ?>" <?php post_class( 'clearfix' ); ?> role="article" itemscope itemtype="http://schema.org/BlogPosting">
	
<div class="row row_globalPage">
	<div class="col-md-12 col_globalPage">
		
		<a href="<?php echo home_url(); ?>">
            <div class="bx_backShop">
                <span class="glyphicon glyphicon-menu-left"></span> Kembali ke Home
            </div>
        </a>

        <div class="bx_globalPage">

        	<div class="row row_title_globalPage">
        		<div class="col-md-8 col_globalPage">
        			<h1 class="ht_globalPage"><?php echo get_the_title($post->ID); ?></h1>
        		</div>
        		<div class="col-md-4 ">
        			<div class="mg_globalPage animated fadeIn">
						<img src="<?php bloginfo('template_directory'); ?>/library/images/main_logo.png">
					</div>
        		</div>
        	</div>

            

        	<div class="content_globalPage">
				<?php // the_content(); ?>

                <?php 
                    $data_abtus = get_post_meta($post->ID,'tentangladara',true);
                    if(isset($data_abtus) AND !empty($data_abtus)){
                        $op = 0;
                        foreach ($data_abtus as $key => $value) {
                            $op++;
                            $title = $value['judul-tentang'];
                            $content = $value['isi-tentang'];
                            ?>
                                <div class="bx_abtus">
                                    <div id="ht_abtus_<?php echo $op; ?>" class="ht_abtus open_abtus act" title="Lihat penjelasan detail tentang <?php echo $title; ?>"><?php echo $title; ?> <span class="glyphicon glyphicon-menu-up"></span></div>
                                    <div id="ct_abtus_<?php echo $op; ?>" class="cont_abtus act">
                                        <?php echo $content; ?>
                                    </div>
                                </div>
                            <?php   
                        }
                    }
                ?>

			</div>
			
        </div>
	</div>
</div>

</article>

<?php endwhile; ?>
<?php else : ?>
		<?php get_template_part( 'content', '404pages' ); ?>	
<?php endif; ?>
<?php get_footer(); ?>