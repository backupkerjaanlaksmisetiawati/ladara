<?php
global $wpdb;
$prefix = $wpdb->prefix;

$date = date("Y-m-d H:i:s");
$cartIds = $_SESSION['cartIds'];
$identifier = $_SESSION['identifier'];
$voucherId = isset($_SESSION['voucherId'][$identifier]) ? $_SESSION['voucherId'][$identifier] : 0;

$carts = array_map(function ($fn) {
    return [
        'id' => intval($fn)
    ];
}, explode(',', $cartIds));

$vouchers = sendRequest('POST', MERCHANT_URL . '/api/vouchers/get-order-vouchers', [
    'carts' => $carts
]);
?>

<div id="popupVoucher" class="row popup-ems">
    <div class="col-md-5">
        <p class="closePopup">
            <span>Pilih Voucher</span>
            <i class="fas fa-times" onclick="popupVoucher('closepopup')"></i>
        </p>
        <div class="box-content">
            <p style="font-size:13px;">Masukan code voucher yang kamu miliki, atau pilih voucher yang tersedia.</p>
            <p class="alert-voucher"></p>

            <div class="form-voucher">
                <input type="hidden" id="voucher-id">
                <input type="text" id="voucherCode" placeholder="Masukan kode voucher" class="txt_aform" />
                <button type="button" class="btn-blue-white disabled" id="btnVoucherCode" disabled onclick="searchVoucher()">Terapkan</button>
            </div>

            <div class="list-voucher">
                <?php if (!empty($vouchers->data)) { ?>
                    <?php foreach ($vouchers->data as $k => $v) { ?>
                        <div class="vouched-cd cd-<?php echo $v->code; ?> <?= (intval($v->id) === intval($voucherId) ? 'selected' : '') ?>" onclick="selectVoucherDetail('<?= $v->code ?>', <?= $v->id ?>)">
                            <h3 class="title"><?php echo $v->name; ?></h3>
                            <p>
                                <span class="subTitle">Berlaku hingga <?php echo date("d M Y", strtotime($v->period_end)); ?>.</span>
                                <?php if ($v->merchant_id === 0) : ?>
                                    <a class="detail voucher_detail_<?php echo $v->code; ?>" onclick="popupVoucherDetail('showpopup', '<?php echo $v->code; ?>')">Lihat Detail</a>
                                <?php endif; ?>
                            </p>
                            <i class="fas fa-check"></i>
                        </div>
                    <?php } ?>
                <?php } ?>
            </div>
        </div>
    </div>
</div>