<?php
/*
Template Name: Login Check FB Page
*/
?>
<?php 
// ================ custom google auth ================
require_once 'vendor-fb/autoload.php';
session_start();

$app_id = getenv('FACEBOOK_APP_ID');
$app_secret = getenv('FACEBOOK_APP_SECRET');
$default_graph_version = getenv('FACEBOOK_DEFAULT_GRAPH_VERSION');

$fb = new Facebook\Facebook([
  'app_id' => $app_id,
  'app_secret' => $app_secret,
  'default_graph_version' => $default_graph_version,
  ]);

$helper = $fb->getRedirectLoginHelper();

try {
    if (isset($_SESSION['facebook_access_token'])) {
      $accessToken = $_SESSION['facebook_access_token'];
    } else {
      $accessToken = $helper->getAccessToken();
    }
} catch(Facebook\Exceptions\facebookResponseException $e) {
  // When Graph returns an error
  echo 'Graph returned an error: ' . $e->getMessage();
    exit;
} catch(Facebook\Exceptions\FacebookSDKException $e) {
  // When validation fails or other local issues
  echo 'Facebook SDK returned an error: ' . $e->getMessage();
    exit;
}

if (isset($accessToken)) {
    if (isset($_SESSION['facebook_access_token'])) {
      $fb->setDefaultAccessToken($_SESSION['facebook_access_token']);
    } else {
      // getting short-lived access token
      $_SESSION['facebook_access_token'] = (string) $accessToken;
        // OAuth 2.0 client handler
      $oAuth2Client = $fb->getOAuth2Client();
      // Exchanges a short-lived access token for a long-lived one
      $longLivedAccessToken = $oAuth2Client->getLongLivedAccessToken($_SESSION['facebook_access_token']);
      $_SESSION['facebook_access_token'] = (string) $longLivedAccessToken;
      // setting default access token to be used in script
      $fb->setDefaultAccessToken($_SESSION['facebook_access_token']);
    }
    // redirect the user to the profile page if it has "code" GET variable
    // getting basic info about user
    try {
      $profile_request = $fb->get('/me?fields=name,first_name,last_name,email');
      $requestPicture = $fb->get('/me/picture?redirect=false&height=200'); //getting user picture
      $picture = $requestPicture->getGraphUser();
      $profile = $profile_request->getGraphUser();
      $fbid = $profile->getProperty('id');           // To Get Facebook ID
      $fbfullname = $profile->getProperty('name');   // To Get Facebook full name
      $fbemail = $profile->getProperty('email');    //  To Get Facebook email
      $fbpic = "<img src='".$picture['url']."' class='img-rounded'/>";
      # save the user nformation in session variable

      $user_data = array();
      $user_data['fb_id'] = $fbid;
      $user_data['fb_name'] = $fbfullname;
      $user_data['fb_email'] = $fbemail;
      // $user_data['fb_pic'] = $fbpic;

      $_SESSION['fb_data'] = $user_data;

    } catch(Facebook\Exceptions\FacebookResponseException $e) {
      // When Graph returns an error
      // echo 'Graph returned an error: ' . $e->getMessage();
      session_destroy();
      // redirecting user back to app login page
      // header("Location: ./");
      // exit;
    } catch(Facebook\Exceptions\FacebookSDKException $e) {
      // When validation fails or other local issues
      // echo 'Facebook SDK returned an error: ' . $e->getMessage();
      // exit;
    }

} 

// ================ custom facebook auth ================
if(isset($_SESSION['rfr']) AND $_SESSION['rfr'] != ''){
  $referer = $_SESSION['rfr'];
}else{

  $rfr = wp_get_referer();
  if(isset($rfr) AND $rfr != ''){
    $referer = wp_get_referer();
    $_SESSION['rfr'] = $referer;
  }else{
    $referer = home_url();
  }
}

// ================= IF LOGIN FACEBOOK SUCCESS ================
$valid_login = 0;
if(isset($_SESSION['fb_data']) AND !empty($_SESSION['fb_data'])){

      $fb_data = $_SESSION['fb_data'];
      $data_user = array();
      $data_user['first_name'] = $fb_data['fb_name'];
      $data_user['last_name'] = '';
      $data_user['user_email'] = $fb_data['fb_email'];
      // $data_user['user_image'] = $fb_data['fb_pic'];
      $data_user['user_gender'] = '';
      $data_user['user_phone'] = '';
      $data_user['acc_type'] = 1; // 1 for facebook

      if(isset($fb_data['fb_email']) AND $fb_data['fb_email'] != ''){
         $valid_login = login_socialmedia($data_user,'fb');
      }

      if($valid_login == 0){ // if failed login fb
          session_destroy();
        ?>
          <div id="redirect_ok" data-hurl="<?php echo home_url(); ?>/login/?login=failed"></div>
          <script>
            var plant = document.getElementById('redirect_ok');
            var hurl = plant.getAttribute('data-hurl'); 
              setTimeout(function(){
                location.replace(hurl); 
              }, 2000);
          </script>
        <?php
      }else{ // if succcess
        ?>
          <div id="redirect_ok" data-hurl="<?php echo $referer; ?>"></div>
          <script>
            var plant = document.getElementById('redirect_ok');
            var hurl = plant.getAttribute('data-hurl'); 
              setTimeout(function(){
                location.replace(hurl); 
              }, 2000);
          </script>
        <?php
      }
      // === login now ===
}else{
      session_destroy();
?>
      <div id="redirect_ok" data-hurl="<?php echo home_url(); ?>/login/?login=failed"></div>
      <script>
        var plant = document.getElementById('redirect_ok');
        var hurl = plant.getAttribute('data-hurl'); 
          setTimeout(function(){
            location.replace(hurl); 
          }, 2000);
      </script>
<?php
}
?>