<?php
/*
Template Name: Notifikasi Page
*/
use Ladara\Models\Notifications;

get_header(); 
if (have_posts()) : while (have_posts()) : the_post(); 

date_default_timezone_set("Asia/Jakarta");
$nowdate = date('Y-m-d H:i:s');
$d_now = date('d');

$current_user = wp_get_current_user();
$u_id = $current_user->ID;

//Get notif
$res_notif = Notifications::getNotifications($u_id);

// $order_id = '1234';
// $type_notif = 'pembayaran';
// $title_notif = 'Transaksi Pembelian';
// $desc_notif = 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.';

// $data = [
//       'userId' => $u_id, //wajib
//       'title' => $title_notif, //wajib
//       'descriptions' => $desc_notif, //wajib
//       'type' => $type_notif, //pesanan, info, pembayaran, emas // wajib
//       'orderId' => $order_id, // optional
//       'data' => [] //array optional bisa diisi dengan data lainnya
// ];
// $addNotif = Notifications::addNotification($data);

// echo "<pre>";
// print_r($res_notif);
// echo "</pre>";

if(isset($u_id) AND $u_id != 0){
 ?>

<article id="post-<?php the_ID(); ?>" <?php post_class( 'clearfix' ); ?> role="article" itemscope itemtype="http://schema.org/BlogPosting">
	
<?php 
if (isset($res_notif['data']) AND !empty($res_notif['data']) ){
?>

<div class="row row_globalPage">
    <div class="col-md-12">
        
        <a href="<?php echo home_url(); ?>">
            <div class="bx_backShop">
                <span class="glyphicon glyphicon-menu-left"></span> Kembali ke Home
            </div>
        </a>


        <h1 class="ht_globalPage_notif"><?php echo get_the_title($post->ID); ?></h1>

        <div class="col_globalPage_notif">

            <div class="row ">
                <div class="col-md-12 col_page_notif">

                    <?php
                        foreach ($res_notif['data'] as $key => $value) {
                            $notif_id = $value['id'];
                            $order_id_notif = $value['order_id'];
                            $type_notif = $value['type'];
                            if(strtolower($type_notif) == 'info'){
                                $link_notif = home_url();
                            }else if(strtolower($type_notif) == 'emas'){
                                $link_notif = home_url().'/emas/';
                            }else{
                                $link_notif = home_url().'/my-order/';
                            }
                            $notif_status = $value['status'];
                            if(isset($notif_status) AND $notif_status == 1){
                                $class_act = '';
                                $icon_act = '';
                            }else{
                                $class_act = 'new';
                                $icon_act = '<span class="ntf_new">baru</span>';
                            }
                            $url_image_notif = $value['url_image'];
                            $title_notif = $value['title'];
                            $desc_notif = $value['descriptions'];
                            $date_created_notif = $value['date_created'];
                            $day_notif = date("d", strtotime($date_created_notif));
                            $time_notif = date("h:i", strtotime($date_created_notif));
                            $date_notif = date("d M", strtotime($date_created_notif));
                            if($day_notif == $d_now){
                                $date_notif = 'hari ini';
                            }

                            // $detail = Notifications::detail($notif_id);

                            ?>        			
                                <div class="row row_topnotif big <?php echo $class_act; ?> open_thisNotif" id="notif_<?php echo $notif_id; ?>" data-id="<?php echo $notif_id; ?>">
                                    <a href="<?php echo $link_notif; ?>" title="Lihat detail">
                                    <div class="col-md-12 col_topnotif big">
                                        <div class="bx_date_topnotif big"><img src="<?php echo $url_image_notif; ?>">info • <?php echo $date_notif; ?> <?php echo $icon_act; ?> <span><?php echo $time_notif; ?></span></div>
                                        <div class="hts_topnotif big"><?php echo $title_notif; ?></div>
                                        <div class="cont_topnotif big">
                                            <?php echo $desc_notif; ?>
                                        </div>
                                    </div>
                                    </a>
                                </div>

                            <?php 
                        }
                        ?>

                </div>
            </div>

            <div class="content_globalPage">
                <?php the_content(); ?>
            </div>
            
        </div>
    </div>
</div>

<?php    
}else{
?>

    <div class="row row_finishCheckout">
        <div class="col-md-12 col_finishCheckout">
            <a href="<?php echo home_url(); ?>">
                <div class="bx_backShop">
                    <span class="glyphicon glyphicon-menu-left"></span> Kembali ke Home
                </div>
            </a>
            <div class="bx_finishCheckout">
                <div class="mg_registerIcon">
                    <img class="lazy" data-src="<?php bloginfo('template_directory'); ?>/library/images/notif_empty.svg">
                </div>
                <div class="ht_register">Tidak ada notifikasi</div>

                <div class="ht_sucs_register">
                    Maaf, Anda belum memiliki notifikasi apapun. Segera belanja untuk kebutuhan Anda.
                </div>
                <div class="bx_def_checkout">
                    <a href="<?php echo home_url(); ?>/shop/">
                        <button class="btn_def_checkout">Belanja Sekarang</button>
                    </a>
                </div>
            </div>
        </div>
    </div>

<?php
}
?>                      

</article>
<?php }else{ ?>
    <script>
        // 'Getting' data-attributes using getAttribute
        var plant = document.getElementById('body');
        console.log(plant);
        var hurl = plant.getAttribute('data-hurl'); 
        location.replace(hurl+'/login/'); 
    </script>
<?php } ?>

<?php endwhile; ?>
<?php else : ?>
		<?php get_template_part( 'content', '404pages' ); ?>	
<?php endif; ?>
<?php get_footer(); ?>