<?php

if (!defined('ABSPATH')) {
    exit;
}

class WC_Xendit_EWallet extends WC_Payment_Gateway {
    const DEFAULT_EXTERNAL_ID_VALUE = 'woocommerce-xendit';
    const DEFAULT_EWALLET_TYPE = 'OVO';
    const DEFAULT_MINIMUM_AMOUNT = 10000;
    const DEFAULT_MAXIMUM_AMOUNT = 10000000;

    public function __construct() {
        $this->id = 'xendit_ovo';
        $this->has_fields = true;

        // Load the form fields.
        $this->init_form_fields();

        // Load the settings.
        $this->init_settings();

        $this->enabled = $this->get_option( 'enabled' );
        $this->supported_currencies = array(
            'IDR'
        );

        $this->method_type = 'eWallet';
        $this->method_code = 'OVO';
        $this->title = !empty($this->get_option('channel_name')) ? $this->get_option('channel_name') : $this->method_code;
        $this->default_description = 'Bayar pesanan dengan akun OVO anda melalui <strong>Xendit</strong>';
        $this->description = !empty($this->get_option('payment_description')) ? nl2br($this->get_option('payment_description')) : $this->default_description;

        $this->method_title = __( 'Xendit OVO', 'woocommerce-gateway-xendit' );
        $this->method_description = sprintf( __( 'Collect payment from OVO account on checkout page and get the report realtime on your Xendit Dashboard. <a href="%1$s" target="_blank">Sign In</a> or <a href="%2$s" target="_blank">sign up</a> on Xendit and integrate with <a href="%3$s" target="_blank">your Xendit keys</a>.', 'woocommerce-gateway-xendit' ), 'https://dashboard.xendit.co/auth/login', 'https://dashboard.xendit.co/auth/register', 'https://dashboard.xendit.co/settings/developers#api-keys' );

        $main_settings = get_option( 'woocommerce_xendit_gateway_settings' );
        $this->developmentmode = $main_settings['developmentmode'];
        $this->secret_key = $this->developmentmode == 'yes' ? $main_settings['secret_key_dev'] : $main_settings['secret_key'];
        $this->publishable_key = $this->developmentmode == 'yes' ? $main_settings['api_key_dev'] : $main_settings['api_key'];
        $this->external_id_format = !empty($main_settings['external_id_format']) ? $main_settings['external_id_format'] : self::DEFAULT_EXTERNAL_ID_VALUE;

        $this->xendit_status = $this->developmentmode == 'yes' ? "[Development]" : "[Production]";
        $this->xendit_callback_url = home_url() . '/?xendit_mode=xendit_ewallet_callback';

        $options['secret_api_key'] = $this->secret_key;
        $options['public_api_key'] = $this->publishable_key;
        $this->xenditClass = new WC_Xendit_PG_API($options);

        add_action( 'wp_enqueue_scripts', array( $this, 'payment_scripts' ) );
        add_action('woocommerce_update_options_payment_gateways_' . $this->id, array($this, 'process_admin_options'));

        add_filter('woocommerce_available_payment_gateways', array(&$this, 'check_gateway_status'));
    }

    public function payment_scripts() {
        WC_Xendit_PG_Logger::log( "WC_Xendit_EWallet::payment_scripts called" );
        if ( ! is_cart() && ! is_checkout() && ! isset( $_GET['pay_for_order'] ) && ! is_add_payment_method_page() ) {
            return;
        }

        if ( 'no' === $this->enabled ) {
            return;
        }

        if ( empty( $this->secret_key ) ) {
            return;
        }

        wp_enqueue_script( 'woocommerce_xendit_ewallet', plugins_url( 'assets/js/xendit-ovo.js', WC_XENDIT_PG_MAIN_FILE ), array( 'jquery' ), WC_XENDIT_PG_VERSION, true );
    }

    public function payment_fields() {
        if ( $this->description ) {
            $test_description = '';
            if ( $this->developmentmode == 'yes' ) {
                $test_description = ' <strong>TEST MODE</strong> - Bank account numbers below are for testing. Real payment will not be detected';
            }

            echo '<p>' . $this->description . '</p>
                <p style="color: red; font-size:80%; margin-top:10px;">' . $test_description . '</p>';
        }

        echo '<fieldset id="wc-' . esc_attr( $this->id ) . '-cc-form" class="wc-credit-card-form wc-payment-form" style="background:transparent;">';

        do_action( 'woocommerce_credit_card_form_start', $this->id );

        // I recommend to use inique IDs, because other gateways could already use #ccNo, #expdate, #cvc
        echo '<div class="form-row form-row-wide">
                <input id="xendit_ovo_phone" type="text" autocomplete="off" placeholder="Phone Number">
            </div>
            <div class="clear"></div>';

        do_action( 'woocommerce_credit_card_form_end', $this->id );

        echo '<div class="clear"></div></fieldset>';

    }

    public function validate_fields(){
        if( empty( $_POST[ 'xendit_ovo_phone' ]) ) {
            wc_add_notice(  '<strong>OVO phone number</strong> is required!', 'error' );
            return false;
        }
        return true;
    }

    public function process_payment( $order_id ) {
        global $woocommerce;
        $log_msg = "WC_Xendit_EWallet::process_payment($order_id) [".$this->external_id_format . '-' . $order_id."]\n\n";

        $order = wc_get_order( $order_id );
        $total_amount = $order->get_total();

        if ($total_amount < WC_Xendit_EWallet::DEFAULT_MINIMUM_AMOUNT && $this->developmentmode != 'yes') {
            $this->cancel_order($order, 'Cancelled because the amount is below the minimum amount');
            $log_msg .= "Cancelled because amount is below minimum amount. Amount = $total_amount\n\n";
            WC_Xendit_PG_Logger::log( $log_msg, WC_LogDNA_Level::ERROR, true );

            throw new Exception( sprintf( __(
                'The minimum amount for using this payment is %1$s. Please put more item to reach the minimum amount. <br />' .
                '<a href="%2$s">Your Cart</a>',
                'woocommerce-gateway-xendit'
            ), wc_price( WC_Xendit_EWallet::DEFAULT_MINIMUM_AMOUNT ),  wc_get_cart_url()) );
        }

        if ($total_amount > WC_Xendit_EWallet::DEFAULT_MAXIMUM_AMOUNT) {
            $this->cancel_order($order, 'Cancelled because amount is above maximum amount');
            $log_msg .= "Cancelled because the amount is above the maximum amount. Amount = $total_amount\n\n";
            WC_Xendit_PG_Logger::log( $log_msg, WC_LogDNA_Level::ERROR, true );

            throw new Exception( sprintf( __(
                'The maximum amount for using this payment is %1$s. Please remove one or more item(s) from your cart. <br />' .
                '<a href="%2$s">Your Cart</a>',
                'woocommerce-gateway-xendit'
            ), wc_price( WC_Xendit_EWallet::DEFAULT_MAXIMUM_AMOUNT ), wc_get_cart_url()) );
        }

        // we need it to get any order details
        try {
            $phone = wc_clean( $_POST['xendit_ovo_phone'] );
            $log_msg .= "Start generate items and customer data\n\n";
            $additional_data = WC_Xendit_PG_Helper::generate_items_and_customer( $order );
            $log_msg .= "Finish generate items and customer data\n\n";
            /*
              * Array with parameters for API interaction
             */
            $external_id = $this->external_id_format . '-' . $order_id;
            $args = array(
                'external_id' => $external_id,
                'amount' => $total_amount,
                'phone' => $phone,
                'ewallet_type' => WC_Xendit_EWallet::DEFAULT_EWALLET_TYPE,
                'platform_callback_url' => $this->xendit_callback_url,
                'items' => $additional_data['items'],
                'customer' => $additional_data['customer']
            );
            $header = array(
                'x-plugin-method' => strtoupper( $this->method_code ),
                'x-plugin-store-name' => get_option('blogname')
            );

            $response = $this->xenditClass->createEwalletPayment($args, $header);

            if ( isset($response['error_code']) ) {
                $log_msg .= "Ewallet request error. Response: " . json_encode($response, JSON_PRETTY_PRINT) . "\n\n";
                update_post_meta($order_id, 'Xendit_error', esc_attr($response['error_code']));

                wc_add_notice( $this->get_localized_error_message( $response['error_code'] ), 'error' );
                WC_Xendit_PG_Logger::log( $log_msg, WC_LogDNA_Level::ERROR, true );
                return;
            }

            if ( isset($response['is_paid']) && $response['is_paid'] ) {
                if( $response['is_paid'] ){
                    $log_msg .= "Ewallet is paid\n\n";
                    $order->set_status( apply_filters( 'woocommerce_payment_complete_order_status', $order->needs_processing() ? 'processing' : 'completed', $order_id, $order ) );
                    $order->save();
    
                    do_action( 'woocommerce_payment_complete', $order_id );
                }
                else { //cancel order
                    $log_msg .= "Ewallet is not paid\n\n";
                    $this->cancel_order($order, 'Cancelled due to payment issues.');
                }
            }

            $log_msg .= "Process finished\n\n";
            WC_Xendit_PG_Logger::log( $log_msg, WC_LogDNA_Level::INFO, true );

            // Redirect to the thank you page
            return array(
                'result' => 'success',
                'redirect' => $this->get_return_url( $order )
            );
        } catch ( Exception $e ) {
            $log_msg .= "Exception caught. Error message: " . $e->getMessage() . "\n\n";
            WC_Xendit_PG_Logger::log( $log_msg, WC_LogDNA_Level::ERROR, true );
            wc_add_notice(  'Unexpected error.', 'error' );
            return;
        }
    }

    public function validate_payment( $response ) {
        global $wpdb, $woocommerce;

        $external_id = $response->external_id;
        $log_msg = "WC_Xendit_EWallet::validate_payment()[$external_id]\n\n";
        $merchant_names = array($this->merchant_name, 'Xendit');

        $xendit_status = $this->xendit_status;

        if ($external_id) {
            $exploded_ext_id = explode("-", $external_id);
            $order_id = end($exploded_ext_id);

            if (!is_numeric($order_id)) {
                $exploded_ext_id = explode("_", $external_id);
                $order_id = end($exploded_ext_id);
            }

            $order = new WC_Order($order_id);

            if ($this->developmentmode != 'yes') {
                $payment_gateway = wc_get_payment_gateway_by_order($order_id);
                if (false === get_post_status($order_id) || strpos($payment_gateway->id, 'xendit')) {
                    $log_msg .= "{$xendit_status} Xendit is live and required valid order id!\n\n";
                    WC_Xendit_PG_Logger::log($log_msg, WC_LogDNA_Level::ERROR, true);

                    header('HTTP/1.1 400 Invalid Data Received');
                    exit;
                }
            }

            //check if order in WC is still pending after payment
            if ($response->ewallet_transaction_id) {
                $ewallet_status = $this->xenditClass->getEwalletStatus($response->ewallet_type, $external_id);
                if ('COMPLETED' == $ewallet_status) {
                    $log_msg .= "{$xendit_status} Xendit is {$ewallet_status}, Proccess Order!\n\n";

                    $notes = json_encode(
                        array(
                            'transaction_id' => $response->ewallet_transaction_id,
                            'status' => $ewallet_status,
                            'payment_method' => $response->ewallet_type,
                            'paid_amount' => $response->amount,
                        )
                    );

                    $note = "Xendit Payment Response:" .
                        "{$notes}";

                    $order->add_order_note('Xendit payment successful');
                    $order->add_order_note($note);

                    // Do mark payment as complete
                    $order->payment_complete();

                    // Reduce stock levels
                    $order->reduce_order_stock();

                    // Empty cart in action
                    $woocommerce->cart->empty_cart();

                    $log_msg .= "{$xendit_status} Payment for Order #{$order->id} now mark as complete with Xendit!\n\n";
                    WC_Xendit_PG_Logger::log($log_msg, WC_LogDNA_Level::INFO, true);

                    //die(json_encode($response, JSON_PRETTY_PRINT)."\n");
                    die('SUCCESS');
                } else {
                    $log_msg .= "{$xendit_status} Xendit is {$ewallet_status}, Proccess Order Declined!\n\n";
                    WC_Xendit_PG_Logger::log($log_msg, WC_LogDNA_Level::ERROR, true);

                    $order->update_status('failed');

                    $notes = json_encode(
                        array(
                            'transaction_id' => $response->ewallet_transaction_id,
                            'status' => $ewallet_status,
                            'payment_method' => $response->ewallet_type,
                            'paid_amount' => $response->amount,
                        )
                    );

                    $note = "Xendit Payment Response:" .
                        "{$notes}";

                    $order->add_order_note('Xendit payment failed');
                    $order->add_order_note($note);

                    header('HTTP/1.1 400 Invalid Data Received');
                    exit;
                }
            }
            else {
                $log_msg .= "{$xendit_status} Order is not pending or ewallet data is not valid!\n\n";
                WC_Xendit_PG_Logger::log($log_msg, WC_LogDNA_Level::ERROR, true);
                //do nothing - payment & order status have been completed successfully
                header('HTTP/1.1 400 Invalid Data Received');
                exit;
            }
        } else {
            $log_msg .= "{$xendit_status} Xendit external id check not passed, break!\n\n";
            WC_Xendit_PG_Logger::log($log_msg, WC_LogDNA_Level::ERROR, true);

            header('HTTP/1.1 400 Invalid Data Received');
            exit;
        }
    }

    public function is_valid_for_use() {
        return in_array( get_woocommerce_currency(), apply_filters(
                'woocommerce_' . $this->id . '_supported_currencies',
                $this->supported_currencies
        ) );
    }

    public function check_gateway_status( $gateways ) {
        global $wpdb, $woocommerce;

        if (is_null($woocommerce->cart)) {
            return $gateways;
        }

        if ( $this->secret_key == "" ) {
            unset($gateways[$this->id]);
            WC_Xendit_PG_Logger::log( "Gateway unset because API key is not set", WC_LogDNA_Level::INFO, true );

            return $gateways;
        }

        $amount = WC_Xendit_PG_Helper::get_float_amount($woocommerce->cart->get_cart_total());
        if ($amount > WC_Xendit_EWallet::DEFAULT_MAXIMUM_AMOUNT) {
            unset($gateways[$this->id]);

            WC_Xendit_PG_Logger::log(
                "Gateway unset because amount: $amount is above maximum amount: " . WC_Xendit_EWallet::DEFAULT_MAXIMUM_AMOUNT,
                WC_LogDNA_Level::INFO,
                true
            );

            return $gateways;
        }

        if (!$this->is_valid_for_use()) {
            unset($gateways[$this->id]);

            return $gateways;
        }

        return $gateways;
    }

    public function init_form_fields() {
        $this->form_fields = require( WC_XENDIT_PG_PLUGIN_PATH . '/libs/forms/wc-xendit-ewallet-ovo-settings.php' );
    }

    public function admin_options() {
        ?>
        <script>
        jQuery(document).ready(function ($) {
            var paymentDescription = $(
                    "#woocommerce_<?=$this->id; ?>_payment_description"
                ).val();
            if (paymentDescription.length > 250) {
                return swal({
                    text: 'Text is too long, please reduce the message and ensure that the length of the character is less than 250.',
                    buttons: {
                        cancel: 'Cancel'
                    }
                }).then(function(value) {
                    e.preventDefault();
                });
            }

            $(".channel-name-format").text(
                "<?=$this->title?>");

            $("#woocommerce_<?=$this->id; ?>_channel_name").change(
                function() {
                    $(".channel-name-format").text($(this).val());
                });
        });
        </script>
        <table class="form-table">
            <?php $this->generate_settings_html(); ?>
        </table>
        <?php
    }

    public function get_xendit_method_title() {
        return $this->method_type . ' - ' . $this->method_code;
    }

    public function get_localized_error_message( $error_code ) {
        switch ( $error_code ) {
            case 'USER_DID_NOT_AUTHORIZE_THE_PAYMENT':
                return 'Please complete the payment request within 60 seconds.';
            case 'USER_DECLINED_THE_TRANSACTION':
                return 'You rejected the payment request, please try again when needed.';
            case 'PHONE_NUMBER_NOT_REGISTERED':
                return 'Your number is not registered in OVO, please register first or contact OVO Customer Service.';
            case 'EXTERNAL_ERROR':
                return 'There is a technical issue happens on OVO, please contact the merchant to solve this issue.';
            case 'SENDING_TRANSACTION_ERROR':
                return 'Your transaction is not sent to OVO, please try again.';
            case 'EWALLET_APP_UNREACHABLE':
                return 'Do you have OVO app on your phone? Please check your OVO app on your phone and try again.';
            case 'REQUEST_FORBIDDEN_ERROR':
                return 'Your merchant disable OVO payment from his side, please contact your merchant to re-enable it
                    before trying it again.';
            case 'DEVELOPMENT_MODE_PAYMENT_ACKNOWLEDGED':
                return 'Development mode detected. Please refer to
                    <a href=\'https://docs.xendit.co/en/testing-payments.html\'>this docs</a> for successful payment
                    simulation';
            default:
                return "Failed to pay with eWallet. Error code: $error_code";
        }
    }

    public function get_icon() {
        $style = version_compare( WC()->version, '2.6', '>=' ) ? 'style="margin-left: 0.3em; margin-top: 0.3em; max-width: 45px;"' : '';
        $file_name = strtolower( $this->method_code ) . '.png';
        $icon = '<img src="' . plugins_url('assets/images/' . $file_name , WC_XENDIT_PG_MAIN_FILE) . '" alt="Xendit" ' . $style . ' />';

        return apply_filters( 'woocommerce_gateway_icon', $icon, $this->id );
    }

    private function cancel_order($order, $note) {
        $order->update_status('wc-cancelled');
        $order->add_order_note($note);
    }
}