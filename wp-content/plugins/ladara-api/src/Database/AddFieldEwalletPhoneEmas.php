<?php

namespace Ladara\Database;
if ( ! defined( 'ABSPATH' ) ) {
    exit;
}

/**
 * Class AddFieldEwalletPhoneEmas
 * @package Ladara\Database
 * @author Amy <laksmise@gmail.com>
 */
class AddFieldEwalletPhoneEmas
{
    /**
     * init function
     */
    public function init()
    {
        global $wpdb;
        $initialVersion = get_option( 'add_field_ewallet_phone_table_ladara_emas', 1 ); //initial version
        $currentVersion = 4; //versi sekarang kalau mau update silahkan nilainya ditambah
        $charset_collate = $wpdb->get_charset_collate();
        $table_name = $wpdb->prefix . 'transaksi_emas'; // nama table

        if ( $initialVersion < $currentVersion) {
          $check = $wpdb->query("SHOW COLUMNS FROM $table_name LIKE 'ewallet_phone'");
          if($check == false) {
            $sql = "ALTER TABLE $table_name ADD COLUMN ewallet_phone VARCHAR(255) NULL DEFAULT NULL AFTER payment_method;";
            update_option( 'add_field_ewallet_phone_table_ladara_emas', $currentVersion );
            require_once( ABSPATH . 'wp-admin/includes/upgrade.php' );
            $wpdb->query($sql);
          }
        }
    }
}