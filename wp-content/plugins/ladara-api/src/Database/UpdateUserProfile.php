<?php


namespace Ladara\Database;
if ( ! defined( 'ABSPATH' ) ) {
    exit;
}

class UpdateUserProfile
{
    /**
     * init function
     */
    public function init()
    {
        global $wpdb;
        $initialVersion = get_option( 'tbluserprofile', 1 ); //initial version
        $currentVersion = 2; //versi sekarang minimal 2 kalau mau update silahkan nilainya ditambah
        $charset_collate = $wpdb->get_charset_collate();
        $table_name = $wpdb->prefix . 'user_profile'; // nama table

        if ( $initialVersion < $currentVersion) {
            $sql = "alter table $table_name ADD COLUMN access_token_sosmed longtext null;";
            update_option( 'tbluserprofile', $currentVersion );
            require_once( ABSPATH . 'wp-admin/includes/upgrade.php' );
            $wpdb->query($sql);
        }
    }
}