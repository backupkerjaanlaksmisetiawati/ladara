<?php

namespace Ladara\Database;
if ( ! defined( 'ABSPATH' ) ) {
    exit;
}

/**
 * Class InsertPagesLadaraEmas
 * @package Ladara\Database
 * @author Amy <laksmise@gmail.com>
 */
class InsertPagesLadaraEmas
{
  /**
   * init function
   */
  public function init()
  {
    global $wpdb;
    $initialVersion = get_option( 'insert_page_ladara_emas', 1 ); //initial version
    $currentVersion = 7; //versi sekarang kalau mau update silahkan nilainya ditambah
    $charset_collate = $wpdb->get_charset_collate();
    $prefix = $wpdb->prefix;

    if ( $initialVersion < $currentVersion) {

      $author = $wpdb->get_results("SELECT * FROM ".$prefix."users WHERE user_email = 'laksmise+lkh2706@gmail.com'");
      $author = $author[0];
      
      // $tanggal_post = "2020-03-11 10:40:40";
      $tanggal_post = date("Y-m-d H:i:s");
      $str_tanggal_post = strtotime($tanggal_post);
      $edit_lock = $str_tanggal_post.":".$author->ID;

      $post = $wpdb->get_results("SELECT * FROM ".$prefix."posts WHERE post_name='emas' AND post_status='publish' AND post_type='page'");

      if(!empty($post)) {
        $post = $post[0];
        $postID = $post->ID;

        // delete semua sub page 'emas' karena mau buat baru yang disesuaikan dengan dengan local gw
        // daripada update satu2 mending delete semua dan buat baru
        $sub_posts = $wpdb->get_results("SELECT * FROM ".$prefix."posts WHERE post_parent=$postID AND post_status='publish' AND post_type='page' AND post_name!='syarat-ketentuan' AND post_name!='kebijakan-privasi' AND post_name!='informasi-emas'");
        if(!empty($sub_posts)) {
          foreach($sub_posts as $subpost) {
            $subpostID = $subpost->ID;    
            $sub_subposts = $wpdb->get_results("SELECT * FROM ".$prefix."posts WHERE post_parent=$subpostID AND post_status='publish' AND post_type='page'");
            if(!empty($sub_subposts)) {
              foreach($sub_subposts as $subsubposts) {
                $wpdb->query("DELETE FROM ".$prefix."posts WHERE ID=".$subsubposts->ID);
                $wpdb->query("DELETE FROM ".$prefix."postmeta WHERE post_id=".$subsubposts->ID);
              }
            }    
            $wpdb->query("DELETE FROM ".$prefix."posts WHERE ID=".$subpost->ID);
            $wpdb->query("DELETE FROM ".$prefix."postmeta WHERE post_id=".$subpost->ID);
          }
        }

        // list sub page emas, tinggal tambah sendiri
        $post_title = array(
            "Beli Emas",
            "Jual Emas",
            "Aktivasi Akun Ladara Emas",
            // "Dashboard Ladara Emas",
            "Riwayat Transaksi Ladara Emas",
            // "Syarat & Ketentuan Ladara Emas"
            // "Kebijakan Privasi Ladara Emas",
            // "Informasi Ladara Emas",
            "Detail Transaksi Ladara Emas"
          );
        $post_name = array(
            "beli",
            "jual",
            "aktivasi",
            // "shop",
            "riwayat-transaksi",
            // "syarat-ketentuan"
            // "kebijakan-privasi",
            // "informasi-emas",
            "transaksi-detail"
          );

        // insert sub page 'emas', sub-sub page, dan meta page
        foreach($post_title as $key => $title) {
          //insert post
          $wpdb->query("INSERT INTO ".$prefix."posts (
              post_author,
              post_date,
              post_date_gmt,
              post_content,
              post_title,
              post_excerpt,
              post_status,
              comment_status,
              ping_status,
              post_password,
              post_name,
              to_ping,
              pinged,
              post_modified,
              post_modified_gmt,
              post_content_filtered,
              post_parent,
              guid,
              menu_order,
              post_type,
              post_mime_type,
              comment_count
            ) VALUES (
              ".$author->ID.",
              '".$tanggal_post."',
              '".$tanggal_post."',
              '',
              '".$title."',
              '',
              'publish',
              'closed',
              'closed',
              '',
              '".$post_name[$key]."',
              '',
              '',
              '".$tanggal_post."',
              '".$tanggal_post."',
              '',
              ".$postID.",
              'http://localhost/ladara/?page_id=',
              0,
              'page',
              '',
              0
            );");

          $sp = $wpdb->get_results("SELECT * FROM ".$prefix."posts WHERE post_title='".$title."' AND post_name='".$post_name[$key]."' AND post_status='publish' AND post_type='page'");
          $latestID = $sp[0]->ID;

          //update guid
          $wpdb->query("UPDATE ".$prefix."posts SET
              guid = 'http://localhost/ladara/?page_id=".$latestID."'
            WHERE ID = ".$latestID);

          //insert postmeta
          //page, page default hanya berisi text content dari "page"
          $template_file = "ladara-emas/default.php";
          if($post_name[$key] === "beli") {
            $template_file = "ladara-emas/beli.php";
          } elseif($post_name[$key] === "jual") {
            $template_file = "ladara-emas/jual.php";
          } elseif($post_name[$key] === "aktivasi") {
            $template_file = "ladara-emas/aktivasi.php";
          // } elseif($post_name[$key] === "shop") {
          //   $template_file = "ladara-emas/dashboard.php";
          } elseif($post_name[$key] === "riwayat-transaksi") {
            $template_file = "ladara-emas/history.php";
          } elseif($post_name[$key] === "transaksi-detail") {
            $template_file = "ladara-emas/transaksi-detail.php";
          }
          
          $wpdb->query("INSERT INTO ".$prefix."postmeta (post_id,meta_key,meta_value) VALUES (".$latestID.",'_edit_last','".$author->ID."');");
          $wpdb->query("INSERT INTO ".$prefix."postmeta (post_id,meta_key,meta_value) VALUES (".$latestID.",'_edit_lock','".$edit_lock."');");
          $wpdb->query("INSERT INTO ".$prefix."postmeta (post_id,meta_key,meta_value) VALUES (".$latestID.",'_wp_page_template','".$template_file."');");

          // insert sub sub page
          // masih manual satu-satu T_T
          if($post_name[$key] === "beli") {
            
            $wpdb->query("INSERT INTO ".$prefix."posts (
                post_author,
                post_date,
                post_date_gmt,
                post_content,
                post_title,
                post_excerpt,
                post_status,
                comment_status,
                ping_status,
                post_password,
                post_name,
                to_ping,
                pinged,
                post_modified,
                post_modified_gmt,
                post_content_filtered,
                post_parent,
                guid,
                menu_order,
                post_type,
                post_mime_type,
                comment_count
              ) VALUES (
                ".$author->ID.",
                '".$tanggal_post."',
                '".$tanggal_post."',
                '',
                'Gagal Pembayaran Emas',
                '',
                'publish',
                'closed',
                'closed',
                '',
                'gagal-bayar',
                '',
                '',
                '".$tanggal_post."',
                '".$tanggal_post."',
                '',
                ".$latestID.",
                'http://localhost/ladara/?page_id=',
                0,
                'page',
                '',
                0
              );");

            $sp1 = $wpdb->get_results("SELECT * FROM ".$prefix."posts WHERE post_title='Gagal Pembayaran Emas' AND post_name='gagal-bayar' AND post_parent=".$latestID." AND post_status='publish' AND post_type='page'");
            $latestID1 = $sp1[0]->ID;

            //update guid
            $wpdb->query("UPDATE ".$prefix."posts SET
                guid = 'http://localhost/ladara/?page_id=".$latestID1."'
              WHERE ID = ".$latestID1);

            // postmeta
            $wpdb->query("INSERT INTO ".$prefix."postmeta (post_id,meta_key,meta_value) VALUES (".$latestID1.",'_edit_last','".$author->ID."');");
            $wpdb->query("INSERT INTO ".$prefix."postmeta (post_id,meta_key,meta_value) VALUES (".$latestID1.",'_edit_lock','".$edit_lock."');");
            $wpdb->query("INSERT INTO ".$prefix."postmeta (post_id,meta_key,meta_value) VALUES (".$latestID1.",'_wp_page_template','ladara-emas/beli-response.php');");
              
            
            $wpdb->query("INSERT INTO ".$prefix."posts (
                post_author,
                post_date,
                post_date_gmt,
                post_content,
                post_title,
                post_excerpt,
                post_status,
                comment_status,
                ping_status,
                post_password,
                post_name,
                to_ping,
                pinged,
                post_modified,
                post_modified_gmt,
                post_content_filtered,
                post_parent,
                guid,
                menu_order,
                post_type,
                post_mime_type,
                comment_count
              ) VALUES (
                ".$author->ID.",
                '".$tanggal_post."',
                '".$tanggal_post."',
                '',
                'Pembayaran Emas Sukses',
                '',
                'publish',
                'closed',
                'closed',
                '',
                'sukses-bayar',
                '',
                '',
                '".$tanggal_post."',
                '".$tanggal_post."',
                '',
                ".$latestID.",
                'http://localhost/ladara/?page_id=',
                0,
                'page',
                '',
                0
              );");

            $sp2 = $wpdb->get_results("SELECT * FROM ".$prefix."posts WHERE post_title='Pembayaran Emas Sukses' AND post_name='sukses-bayar' AND post_parent=".$latestID." AND post_status='publish' AND post_type='page'");
            $latestID2 = $sp2[0]->ID;

            //update guid
            $wpdb->query("UPDATE ".$prefix."posts SET
                guid = 'http://localhost/ladara/?page_id=".$latestID2."'
              WHERE ID = ".$latestID2);

            // postmeta
            $wpdb->query("INSERT INTO ".$prefix."postmeta (post_id,meta_key,meta_value) VALUES (".$latestID2.",'_edit_last','".$author->ID."');");
            $wpdb->query("INSERT INTO ".$prefix."postmeta (post_id,meta_key,meta_value) VALUES (".$latestID2.",'_edit_lock','".$edit_lock."');");
            $wpdb->query("INSERT INTO ".$prefix."postmeta (post_id,meta_key,meta_value) VALUES (".$latestID2.",'_wp_page_template','ladara-emas/beli-response.php');");
              
            
            $wpdb->query("INSERT INTO ".$prefix."posts (
                post_author,
                post_date,
                post_date_gmt,
                post_content,
                post_title,
                post_excerpt,
                post_status,
                comment_status,
                ping_status,
                post_password,
                post_name,
                to_ping,
                pinged,
                post_modified,
                post_modified_gmt,
                post_content_filtered,
                post_parent,
                guid,
                menu_order,
                post_type,
                post_mime_type,
                comment_count
              ) VALUES (
                ".$author->ID.",
                '".$tanggal_post."',
                '".$tanggal_post."',
                '',
                'Proses Beli Emas',
                '',
                'publish',
                'closed',
                'closed',
                '',
                'processing',
                '',
                '',
                '".$tanggal_post."',
                '".$tanggal_post."',
                '',
                ".$latestID.",
                'http://localhost/ladara/?page_id=',
                0,
                'page',
                '',
                0
              );");

            $sp3 = $wpdb->get_results("SELECT * FROM ".$prefix."posts WHERE post_title='Proses Beli Emas' AND post_name='processing' AND post_parent=".$latestID." AND post_status='publish' AND post_type='page'");
            $latestID3 = $sp3[0]->ID;

            //update guid
            $wpdb->query("UPDATE ".$prefix."posts SET
                guid = 'http://localhost/ladara/?page_id=".$latestID3."'
              WHERE ID = ".$latestID3);    

            // postmeta
            $wpdb->query("INSERT INTO ".$prefix."postmeta (post_id,meta_key,meta_value) VALUES (".$latestID3.",'_edit_last','".$author->ID."');");
            $wpdb->query("INSERT INTO ".$prefix."postmeta (post_id,meta_key,meta_value) VALUES (".$latestID3.",'_edit_lock','".$edit_lock."');");
            $wpdb->query("INSERT INTO ".$prefix."postmeta (post_id,meta_key,meta_value) VALUES (".$latestID3.",'_wp_page_template','ladara-emas/beli-process.php');");
              
            
            $wpdb->query("INSERT INTO ".$prefix."posts (
                post_author,
                post_date,
                post_date_gmt,
                post_content,
                post_title,
                post_excerpt,
                post_status,
                comment_status,
                ping_status,
                post_password,
                post_name,
                to_ping,
                pinged,
                post_modified,
                post_modified_gmt,
                post_content_filtered,
                post_parent,
                guid,
                menu_order,
                post_type,
                post_mime_type,
                comment_count
              ) VALUES (
                ".$author->ID.",
                '".$tanggal_post."',
                '".$tanggal_post."',
                '',
                'Invoice Beli Emas',
                '',
                'publish',
                'closed',
                'closed',
                '',
                'invoice',
                '',
                '',
                '".$tanggal_post."',
                '".$tanggal_post."',
                '',
                ".$latestID.",
                'http://localhost/ladara/?page_id=',
                0,
                'page',
                '',
                0
              );");

            $sp4 = $wpdb->get_results("SELECT * FROM ".$prefix."posts WHERE post_title='Invoice Beli Emas' AND post_name='invoice' AND post_parent=".$latestID." AND post_status='publish' AND post_type='page'");
            $latestID4 = $sp4[0]->ID;

            //update guid
            $wpdb->query("UPDATE ".$prefix."posts SET
                guid = 'http://localhost/ladara/?page_id=".$latestID4."'
              WHERE ID = ".$latestID4);    

            // postmeta
            $wpdb->query("INSERT INTO ".$prefix."postmeta (post_id,meta_key,meta_value) VALUES (".$latestID4.",'_edit_last','".$author->ID."');");
            $wpdb->query("INSERT INTO ".$prefix."postmeta (post_id,meta_key,meta_value) VALUES (".$latestID4.",'_edit_lock','".$edit_lock."');");
            $wpdb->query("INSERT INTO ".$prefix."postmeta (post_id,meta_key,meta_value) VALUES (".$latestID4.",'_wp_page_template','ladara-emas/beli-invoice.php');");
            
          } elseif($post_name[$key] === "jual") {
            
            $wpdb->query("INSERT INTO ".$prefix."posts (
                post_author,
                post_date,
                post_date_gmt,
                post_content,
                post_title,
                post_excerpt,
                post_status,
                comment_status,
                ping_status,
                post_password,
                post_name,
                to_ping,
                pinged,
                post_modified,
                post_modified_gmt,
                post_content_filtered,
                post_parent,
                guid,
                menu_order,
                post_type,
                post_mime_type,
                comment_count
              ) VALUES (
                ".$author->ID.",
                '".$tanggal_post."',
                '".$tanggal_post."',
                '',
                'Checkout Jual Berhasil',
                '',
                'publish',
                'closed',
                'closed',
                '',
                'faktur',
                '',
                '',
                '".$tanggal_post."',
                '".$tanggal_post."',
                '',
                ".$latestID.",
                'http://localhost/ladara/?page_id=',
                0,
                'page',
                '',
                0
              );");

            $sp5 = $wpdb->get_results("SELECT * FROM ".$prefix."posts WHERE post_title='Checkout Jual Berhasil' AND post_name='faktur' AND post_parent=".$latestID." AND post_status='publish' AND post_type='page'");
            $latestID5 = $sp5[0]->ID;

            //update guid
            $wpdb->query("UPDATE ".$prefix."posts SET
                guid = 'http://localhost/ladara/?page_id=".$latestID5."'
              WHERE ID = ".$latestID5);

            // postmeta
            $wpdb->query("INSERT INTO ".$prefix."postmeta (post_id,meta_key,meta_value) VALUES (".$latestID5.",'_edit_last','".$author->ID."');");
            $wpdb->query("INSERT INTO ".$prefix."postmeta (post_id,meta_key,meta_value) VALUES (".$latestID5.",'_edit_lock','".$edit_lock."');");
            $wpdb->query("INSERT INTO ".$prefix."postmeta (post_id,meta_key,meta_value) VALUES (".$latestID5.",'_wp_page_template','ladara-emas/jual-faktur.php');");
              
            
            $wpdb->query("INSERT INTO ".$prefix."posts (
                post_author,
                post_date,
                post_date_gmt,
                post_content,
                post_title,
                post_excerpt,
                post_status,
                comment_status,
                ping_status,
                post_password,
                post_name,
                to_ping,
                pinged,
                post_modified,
                post_modified_gmt,
                post_content_filtered,
                post_parent,
                guid,
                menu_order,
                post_type,
                post_mime_type,
                comment_count
              ) VALUES (
                ".$author->ID.",
                '".$tanggal_post."',
                '".$tanggal_post."',
                '',
                'Proses Jual',
                '',
                'publish',
                'closed',
                'closed',
                '',
                'processing',
                '',
                '',
                '".$tanggal_post."',
                '".$tanggal_post."',
                '',
                ".$latestID.",
                'http://localhost/ladara/?page_id=',
                0,
                'page',
                '',
                0
              );");

            $sp6 = $wpdb->get_results("SELECT * FROM ".$prefix."posts WHERE post_title='Proses Jual' AND post_name='processing' AND post_parent=".$latestID." AND post_status='publish' AND post_type='page'");
            $latestID6 = $sp6[0]->ID;

            //update guid
            $wpdb->query("UPDATE ".$prefix."posts SET
                guid = 'http://localhost/ladara/?page_id=".$latestID6."'
              WHERE ID = ".$latestID6);

            // postmeta
            $wpdb->query("INSERT INTO ".$prefix."postmeta (post_id,meta_key,meta_value) VALUES (".$latestID6.",'_edit_last','".$author->ID."');");
            $wpdb->query("INSERT INTO ".$prefix."postmeta (post_id,meta_key,meta_value) VALUES (".$latestID6.",'_edit_lock','".$edit_lock."');");
            $wpdb->query("INSERT INTO ".$prefix."postmeta (post_id,meta_key,meta_value) VALUES (".$latestID6.",'_wp_page_template','ladara-emas/jual-process.php');");

          } elseif($post_name[$key] === "aktivasi") {
            
            $wpdb->query("INSERT INTO ".$prefix."posts (
                post_author,
                post_date,
                post_date_gmt,
                post_content,
                post_title,
                post_excerpt,
                post_status,
                comment_status,
                ping_status,
                post_password,
                post_name,
                to_ping,
                pinged,
                post_modified,
                post_modified_gmt,
                post_content_filtered,
                post_parent,
                guid,
                menu_order,
                post_type,
                post_mime_type,
                comment_count
              ) VALUES (
                ".$author->ID.",
                '".$tanggal_post."',
                '".$tanggal_post."',
                '',
                'Aktivasi Selesai',
                '',
                'publish',
                'closed',
                'closed',
                '',
                'selesai',
                '',
                '',
                '".$tanggal_post."',
                '".$tanggal_post."',
                '',
                ".$latestID.",
                'http://localhost/ladara/?page_id=',
                0,
                'page',
                '',
                0
              );");


            $sp7 = $wpdb->get_results("SELECT * FROM ".$prefix."posts WHERE post_title='Aktivasi Selesai' AND post_name='selesai' AND post_parent=".$latestID." AND post_status='publish' AND post_type='page'");
            $latestID7 = $sp7[0]->ID;

            //update guid
            $wpdb->query("UPDATE ".$prefix."posts SET
                guid = 'http://localhost/ladara/?page_id=".$latestID7."'
              WHERE ID = ".$latestID7);

            // postmeta
            $wpdb->query("INSERT INTO ".$prefix."postmeta (post_id,meta_key,meta_value) VALUES (".$latestID7.",'_edit_last','".$author->ID."');");
            $wpdb->query("INSERT INTO ".$prefix."postmeta (post_id,meta_key,meta_value) VALUES (".$latestID7.",'_edit_lock','".$edit_lock."');");
            $wpdb->query("INSERT INTO ".$prefix."postmeta (post_id,meta_key,meta_value) VALUES (".$latestID7.",'_wp_page_template','ladara-emas/aktivasi-finished.php');");
            
          }
        }
      }

      update_option( 'insert_page_ladara_emas', $currentVersion );
      require_once( ABSPATH . 'wp-admin/includes/upgrade.php' );
    }
  }
}