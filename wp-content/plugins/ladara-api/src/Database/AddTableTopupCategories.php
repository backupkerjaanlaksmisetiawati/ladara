<?php

namespace Ladara\Database;
if ( ! defined( 'ABSPATH' ) ) {
    exit;
}

/**
 * Class AddTableTopupCategories
 * @package Ladara\Database
 * @author Amy <laksmise@gmail.com>
 */
class AddTableTopupCategories
{
    /**
     * init function
     */
    public function init()
    {
        global $wpdb;
        $initialVersion = get_option( 'add_table_topup_categories', 1 ); //initial version
        $currentVersion = 6; //versi sekarang kalau mau update silahkan nilainya ditambah
        $charset_collate = $wpdb->get_charset_collate();
        $table_name = 'ldr_topup_categories'; // nama table

        if ( $initialVersion < $currentVersion) {            
            $sql = "CREATE TABLE IF NOT EXISTS $table_name (
              `id` bigint(20) NOT NULL AUTO_INCREMENT,
              `name` varchar(255) DEFAULT NULL,
              `slug` varchar(255) DEFAULT NULL,
              `icon` varchar(255) DEFAULT NULL,
              `featured` smallint(6) DEFAULT 0,
              `status` smallint(6) DEFAULT 0,
              `created_by` bigint(20) DEFAULT NULL,
              `updated_by` bigint(20) DEFAULT NULL,
              `created_at` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
              `updated_at` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
              `deleted_at` TIMESTAMP NULL DEFAULT CURRENT_TIMESTAMP,
              PRIMARY KEY (`id`)
            ) $charset_collate ;";
            update_option( 'add_table_topup_categories', $currentVersion );
            require_once( ABSPATH . 'wp-admin/includes/upgrade.php' );
            dbDelta( $sql );
        }
    }
}