<?php

namespace Ladara\Database;
if ( ! defined( 'ABSPATH' ) ) {
    exit;
}

/**
 * Class AddFieldTokenEmas
 * @package Ladara\Database
 * @author Amy <laksmise@gmail.com>
 */
class AddFieldTokenEmas
{
    /**
     * init function
     */
    public function init()
    {
        global $wpdb;
        $initialVersion = get_option( 'add_field_token_table_ladara_emas', 1 ); //initial version
        $currentVersion = 3; //versi sekarang kalau mau update silahkan nilainya ditambah
        $charset_collate = $wpdb->get_charset_collate();
        $table_name = $wpdb->prefix . 'user_emas'; // nama table

        if ( $initialVersion < $currentVersion) {
          $check = $wpdb->query("SHOW COLUMNS FROM $table_name LIKE 'trs_token'");
          if($check == false) {
            $sql = "ALTER TABLE $table_name ADD COLUMN trs_token LONGTEXT NULL DEFAULT NULL AFTER trs_user;";
            update_option( 'add_field_token_table_ladara_emas', $currentVersion );
            require_once( ABSPATH . 'wp-admin/includes/upgrade.php' );
            $wpdb->query($sql);
          }
        }
    }
}