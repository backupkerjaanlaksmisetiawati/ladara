<?php

namespace Ladara\Database;
if ( ! defined( 'ABSPATH' ) ) {
    exit;
}

/**
 * Class AddFieldVoucherTarget
 * @package Ladara\Database
 * @author Amy <laksmise@gmail.com>
 */
class AddFieldVoucherTarget
{
    /**
     * init function
     */
    public function init()
    {
        global $wpdb;
        $initialVersion = get_option( 'add_field_target_table_voucher', 1 ); //initial version
        $currentVersion = 2; //versi sekarang kalau mau update silahkan nilainya ditambah
        $charset_collate = $wpdb->get_charset_collate();
        $table_name = $wpdb->prefix . 'vouchers'; // nama table

        if ( $initialVersion < $currentVersion) {
          $check = $wpdb->query("SHOW COLUMNS FROM $table_name LIKE 'target'");
          if($check == false) {
            $sql = "ALTER TABLE $table_name ADD COLUMN target ENUM('public','limited') NULL DEFAULT 'public' AFTER limit_per_user;";
            update_option( 'add_field_target_table_voucher', $currentVersion );
            require_once( ABSPATH . 'wp-admin/includes/upgrade.php' );
            $wpdb->query($sql);
          }
        }
    }
}