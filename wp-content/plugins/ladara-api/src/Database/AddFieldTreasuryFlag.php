<?php

namespace Ladara\Database;
if ( ! defined( 'ABSPATH' ) ) {
    exit;
}

/**
 * Class AddFieldTreasuryFlag
 * @package Ladara\Database
 * @author Amy <laksmise@gmail.com>
 */
class AddFieldTreasuryFlag
{
    /**
     * init function
     */
    public function init()
    {
        global $wpdb;
        $initialVersion = get_option( 'add_field_treasury_flag_table_ladara_emas', 1 ); //initial version
        $currentVersion = 5; //versi sekarang kalau mau update silahkan nilainya ditambah
        $charset_collate = $wpdb->get_charset_collate();
        $table_name = $wpdb->prefix . 'user_emas'; // nama table

        if ( $initialVersion < $currentVersion) {
          $check = $wpdb->query("SHOW COLUMNS FROM ldr_user_emas LIKE 'trs_user'");
          if($check == false) {
            $sql = "ALTER TABLE $table_name ADD COLUMN trs_user INT(11) NOT NULL DEFAULT '0' AFTER gold_balance_in_currency;";
            update_option( 'add_field_treasury_flag_table_ladara_emas', $currentVersion );
            require_once( ABSPATH . 'wp-admin/includes/upgrade.php' );
            $wpdb->query($sql);
          }
        }
    }
}