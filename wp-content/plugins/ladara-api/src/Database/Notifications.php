<?php

namespace Ladara\Database;
if ( ! defined( 'ABSPATH' ) ) {
    exit;
}

class Notifications
{
    /**
     * init function
     */
    public function init()
    {
        global $wpdb;
        $initialVersion = get_option( 'db_notification', 1 ); //initial version
        $currentVersion = 3; //versi sekarang kalau mau update silahkan nilainya ditambah
        $charset_collate = $wpdb->get_charset_collate();
        $table_name = $wpdb->prefix . 'notifications'; // nama table

        if ( $initialVersion < $currentVersion) {
            $sql = "CREATE TABLE $table_name(
                    id int auto_increment,
                    user_id bigint not null,
                    order_id bigint null,
                    type varchar(255) null,
                    url_image text null,
                    title text not null,
                    descriptions text not null,
                    status int default 0 null,
                    data json null,
                    date_created TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
                    constraint {$table_name}_pk primary key (id)
            ) $charset_collate";
            update_option( 'db_notification', $currentVersion );
            require_once( ABSPATH . 'wp-admin/includes/upgrade.php' );
            dbDelta( $sql );
        }
    }
}