<?php

namespace Ladara\Database;
if ( ! defined( 'ABSPATH' ) ) {
    exit;
}

/**
 * Class InsertUserEmasForTesting
 * @package Ladara\Database
 * @author Amy <laksmise@gmail.com>
 */
class InsertUserEmasForTesting
{
  /**
   * init function
   */
  public function init()
  {
    global $wpdb;
    $initialVersion = get_option( 'insert_db_user_emas', 1 ); //initial version
    $currentVersion = 6; //versi sekarang kalau mau update silahkan nilainya ditambah
    $charset_collate = $wpdb->get_charset_collate();
    $table_name = $wpdb->prefix . 'user_emas'; // nama table

    if ( $initialVersion < $currentVersion) {
      $password = 'bebekgoreng';
      $web_password = '$P$B4aBKJjR3gkSubIxwvuCRSOkinDw8U0';
      $ts_password = '$P$BmsATbB3TeMOyZiAPmfdAtdg2znJBw1';
      $email = 'laksmise+lkh2706@gmail.com';

      $query_get_users = "SELECT * FROM " . $wpdb->prefix . "users WHERE user_email = '$email'";
      $check_users = $wpdb->get_results($query_get_users);

      if(empty($check_users)) {
        // insert user
        $sql_users = "INSERT INTO " . $wpdb->prefix . "users (
            user_login,
            user_pass,
            user_nicename,
            user_email,
            user_url,
            user_registered,
            user_activation_key,
            user_status,
            display_name
          ) VALUES (
            'laksmi',
            '$web_password',
            'laksmi-setiawati',
            '$email',
            '',
            '2020-04-17 08:10:12',
            '',
            0,
            'Laksmi Setiawati'
          );";
        $wpdb->query($sql_users);

        $get_users = $wpdb->get_results($query_get_users);
        
        $user_id = $get_users[0]->ID;
        
        // usermeta T_T
        $usermeta = array(
          "nickname" => "laksmi",
          "first_name" => "Laksmi Setiawati",
          "last_name" => "Setiawati",
          "description" => "",
          "rich_editing" => "true",
          "syntax_highlighting" => "true",
          "comment_shortcuts" => "false",
          "admin_color" => "fresh",
          "use_ssl" => "0",
          "show_admin_bar_front" => "true",
          "locale" => "",
          "ldr_capabilities" => 'a:1:{s:13:"administrator";b:1;}',
          "ldr_user_level" => "10",
          "user_avatar" => "426",
          "_user_avatar" => "field_5e7a0ea8fff07",
          "nama_lengkap" => "Laksmi Setiawati",
          "_nama_lengkap" => "",
          "tanggal_lahir" => "1991-1-1",
          "_tanggal_lahir" => "",
          "telepon" => "081343688225",
          "_telepon" => "",
          "jenis_kelamin" => "wanita",
          "_jenis_kelamin" => "",
          "dismissed_wp_pointers" => "",
          "last_update" => "1587113114",
          "session_tokens" => 'a:1:{s:64:"2b156203a03a48b2308c124c71f6368f8d8bf3ee5e73bd02b6be5347b3b65a8e";a:4:{s:10:"expiration";i:1587283886;s:2:"ip";s:14:"162.158.106.16";s:2:"ua";s:115:"Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/81.0.4044.113 Safari/537.36";s:5:"login";i:1587111086;}}',
          "wc_last_active" => "1587056400",
          "odp_cfs_shield_notice" => "Y",
          "ldr_dashboard_quick_press_last_post_id" => "421",
          "community-events-location" => 'a:1:{s:2:"ip";s:10:"36.70.13.0";}'
        );

        foreach($usermeta as $key => $value) {
          $wpdb->query("INSERT INTO " . $wpdb->prefix . "usermeta (user_id,meta_key,meta_value) VALUES (".$user_id.",'".$key."','".$value."');");
        }

        $sql = "INSERT INTO $table_name (
            user_id,
            name,
            email,
            web_password,
            ts_password,
            birthday,
            referral_code,
            phone,
            security_question,
            security_question_answer,
            selfie_scan,
            id_card_scan,
            account_name,
            account_number,
            bank_code,
            branch,
            verified_user,
            gold_balance,
            gold_balance_in_currency
          ) VALUES (
            $user_id,
            'Laksmi Setiawati',
            '$email',
            '$web_password',
            '$ts_password',
            '1991-01-01',
            '',
            '081394525227',
            'KQxz9YXazA14VEO',
            'Mickey Mouse',
            '',
            '',
            'Ami Setiawati',
            '123456789',
            'BI',
            'Jakarta',
            0,
            0,
            0
          );";
        $wpdb->query($sql);

        require_once( ABSPATH . 'wp-admin/includes/upgrade.php' );
      }
      update_option( 'insert_db_user_emas', $currentVersion );
    }
  }
}