<?php

namespace Ladara\Database;
if ( ! defined( 'ABSPATH' ) ) {
    exit;
}

/**
 * Class DropFieldOldUserEmas
 * @package Ladara\Database
 * @author Amy <laksmise@gmail.com>
 */
class DropFieldOldUserEmas
{
    /**
     * init function
     */
    public function init()
    {
        global $wpdb;
        $initialVersion = get_option( 'drop_field_old_user_table_ladara_emas', 1 ); //initial version
        $currentVersion = 4; //versi sekarang kalau mau update silahkan nilainya ditambah
        $charset_collate = $wpdb->get_charset_collate();
        $table_name = $wpdb->prefix . 'user_emas'; // nama table

        if ( $initialVersion < $currentVersion) {
          $check = $wpdb->query("SHOW COLUMNS FROM ldr_user_emas LIKE 'old_user'");
          if($check == true) {
            $sql = "ALTER TABLE $table_name DROP COLUMN old_user;";
            update_option( 'drop_field_old_user_table_ladara_emas', $currentVersion );
            require_once( ABSPATH . 'wp-admin/includes/upgrade.php' );
            $wpdb->query($sql);
          }
        }
    }
}