<?php

namespace Ladara\Database;
if ( ! defined( 'ABSPATH' ) ) {
    exit;
}

/**
 * Class InsertPagesLadaraEmas
 * @package Ladara\Database
 * @author Amy <laksmise@gmail.com>
 */
class InsertDashboardEmas
{
  /**
   * init function
   */
  public function init()
  {
    global $wpdb;
    $initialVersion = get_option( 'insert_dashboard_emas', 1 ); //initial version
    $currentVersion = 4; //versi sekarang kalau mau update silahkan nilainya ditambah
    $charset_collate = $wpdb->get_charset_collate();
    $prefix = $wpdb->prefix;

    if ( $initialVersion < $currentVersion) {
      $author = $wpdb->get_row("SELECT * FROM ".$prefix."users WHERE user_email = 'laksmise+lkh2706@gmail.com'");
      
      $tanggal_post = date("Y-m-d H:i:s");
      $str_tanggal_post = strtotime($tanggal_post);
      $edit_lock = $str_tanggal_post.":".$author->ID;

      $post = $wpdb->get_row("SELECT * FROM ".$prefix."posts WHERE post_name='dashboard' AND post_status='publish' AND post_type='page'");

      if(!empty($post)) {
        $postID = $post->ID;

        //insert post
        $wpdb->query("INSERT INTO ".$prefix."posts (
            post_author,
            post_date,
            post_date_gmt,
            post_content,
            post_title,
            post_excerpt,
            post_status,
            comment_status,
            ping_status,
            post_password,
            post_name,
            to_ping,
            pinged,
            post_modified,
            post_modified_gmt,
            post_content_filtered,
            post_parent,
            guid,
            menu_order,
            post_type,
            post_mime_type,
            comment_count
          ) VALUES (
            ".$author->ID.",
            '".$tanggal_post."',
            '".$tanggal_post."',
            '',
            'Dashboard Ladara Emas',
            '',
            'publish',
            'closed',
            'closed',
            '',
            'ladara-emas',
            '',
            '',
            '".$tanggal_post."',
            '".$tanggal_post."',
            '',
            ".$postID.",
            'http://localhost/ladara/?page_id=',
            0,
            'page',
            '',
            0
          );");

        $sp = $wpdb->get_row("SELECT * FROM ".$prefix."posts WHERE post_title='Dashboard Ladara Emas' AND post_name='ladara_emas' AND post_status='publish' AND post_type='page'");
        $latestID = $sp->ID;

        //update guid
        $wpdb->query("UPDATE ".$prefix."posts SET
            guid = 'http://localhost/ladara/?page_id=".$latestID."'
          WHERE ID = ".$latestID);

        //insert postmeta        
        $wpdb->query("INSERT INTO ".$prefix."postmeta (post_id,meta_key,meta_value) VALUES (".$latestID.",'_edit_last','".$author->ID."');");
        $wpdb->query("INSERT INTO ".$prefix."postmeta (post_id,meta_key,meta_value) VALUES (".$latestID.",'_edit_lock','".$edit_lock."');");
        $wpdb->query("INSERT INTO ".$prefix."postmeta (post_id,meta_key,meta_value) VALUES (".$latestID.",'_wp_page_template','page-admin-emas.php');");
      }

      update_option( 'insert_dashboard_emas', $currentVersion );
      require_once( ABSPATH . 'wp-admin/includes/upgrade.php' );
    }
  }
}