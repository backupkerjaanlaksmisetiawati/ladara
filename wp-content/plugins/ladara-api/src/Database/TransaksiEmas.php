<?php

namespace Ladara\Database;
if ( ! defined( 'ABSPATH' ) ) {
    exit;
}

/**
 * Class TransaksiEmas
 * @package Ladara\Database
 * @author Amy <laksmise@gmail.com>
 */
class TransaksiEmas
{
    /**
     * init function
     */
    public function init()
    {
        global $wpdb;
        $initialVersion = get_option( 'db_transaksi_emas', 1 ); //initial version
        $currentVersion = 2; //versi sekarang kalau mau update silahkan nilainya ditambah
        $charset_collate = $wpdb->get_charset_collate();
        $table_name = $wpdb->prefix . 'transaksi_emas'; // nama table

        if ( $initialVersion < $currentVersion) {            
            $sql = "CREATE TABLE $table_name(
                    id int(11) NOT NULL,
                    user_id int(11) NOT NULL,
                    type enum('sell','buy') NOT NULL,
                    invoice_order varchar(255) DEFAULT NULL,
                    total_gold double DEFAULT NULL,
                    total_price int(11) DEFAULT NULL,
                    test_price double DEFAULT NULL,
                    bank_account varchar(255) DEFAULT NULL,
                    bank_account_name varchar(255) DEFAULT NULL,
                    bank_name varchar(255) DEFAULT NULL,
                    buying_rate int(11) DEFAULT NULL,
                    selling_rate int(11) DEFAULT NULL,
                    booking_fee int(11) DEFAULT NULL,
                    tax int(11) DEFAULT NULL,
                    partner_fee int(11) DEFAULT NULL,
                    bank_fee int(11) DEFAULT NULL COMMENT 'bank_fee dari calculate dengan pembayaran via treasury',
                    trs_invoice varchar(255) DEFAULT NULL,
                    trs_va varchar(255) DEFAULT NULL,
                    trs_payment_channel varchar(25) DEFAULT NULL,
                    payment_method varchar(255) DEFAULT NULL,
                    xendit_fee int(11) DEFAULT NULL,
                    xendit_va varchar(255) DEFAULT NULL,
                    xendit_id varchar(255) DEFAULT NULL,
                    xendit_url varchar(255) DEFAULT NULL,
                    status varchar(50) NOT NULL,
                    due_date datetime DEFAULT NULL,
                    created_date datetime NOT NULL,
                    updated_date timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
                    constraint {$table_name}_pk primary key (id)
            ) $charset_collate ;";
            update_option( 'db_transaksi_emas', $currentVersion );
            require_once( ABSPATH . 'wp-admin/includes/upgrade.php' );
            dbDelta( $sql );
        }
    }
}