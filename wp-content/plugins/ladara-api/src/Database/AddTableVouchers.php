<?php

namespace Ladara\Database;
if ( ! defined( 'ABSPATH' ) ) {
    exit;
}

/**
 * Class AddTableVouchers
 * @package Ladara\Database
 * @author Amy <laksmise@gmail.com>
 */
class AddTableVouchers
{
    /**
     * init function
     */
    public function init()
    {
        global $wpdb;
        $initialVersion = get_option( 'add_table_voucher', 1 ); //initial version
        $currentVersion = 3; //versi sekarang kalau mau update silahkan nilainya ditambah
        $charset_collate = $wpdb->get_charset_collate();
        $table_name = $wpdb->prefix . 'vouchers'; // nama table

        if ( $initialVersion < $currentVersion) {            
            $sql = "CREATE TABLE IF NOT EXISTS $table_name (
              `id` bigint(20) NOT NULL AUTO_INCREMENT,
              `merchant_id` bigint(20) NOT NULL DEFAULT '0',
              `user_id` bigint(20) NOT NULL DEFAULT '0',
              `user_type` enum('merchant','admin') DEFAULT 'admin',
              `name` varchar(50) DEFAULT NULL,
              `description` text,
              `banner_image` text,
              `code` varchar(20) DEFAULT NULL,
              `discount_type` enum('percent','fix') DEFAULT NULL,
              `discount_amount` bigint(20) DEFAULT NULL,
              `min_spend` bigint(20) DEFAULT NULL,
              `max_spend` bigint(20) DEFAULT NULL,
              `limit` int(11) DEFAULT NULL,
              `limit_per_user` int(11) DEFAULT NULL,
              `period_start` datetime DEFAULT NULL,
              `period_end` datetime DEFAULT NULL,
              `device` enum('web','app','all') DEFAULT 'all',
              `product_ids` text,
              `product_categories` text,
              `flag` int(11) DEFAULT '1',
              `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
              `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
              PRIMARY KEY (`id`)
            ) $charset_collate ;";
            update_option( 'add_table_voucher', $currentVersion );
            require_once( ABSPATH . 'wp-admin/includes/upgrade.php' );
            dbDelta( $sql );
        }
    }
}