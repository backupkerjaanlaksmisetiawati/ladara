<?php

namespace Ladara\Database;
if ( ! defined( 'ABSPATH' ) ) {
    exit;
}

/**
 * Class InsertProfileLinkTreasury
 * @package Ladara\Database
 * @author Amy <laksmise@gmail.com>
 */
class InsertProfileLinkTreasury
{
  /**
   * init function
   */
  public function init()
  {
    global $wpdb;
    $initialVersion = get_option( 'insert_profile_link_treasury', 1 ); //initial version
    $currentVersion = 4; //versi sekarang kalau mau update silahkan nilainya ditambah
    $charset_collate = $wpdb->get_charset_collate();
    $prefix = $wpdb->prefix;

    if ( $initialVersion < $currentVersion) {
      $author = $wpdb->get_row("SELECT * FROM ".$prefix."users WHERE user_email='laksmise+lkh2706@gmail.com'");
      
      $tanggal_post = date("Y-m-d H:i:s");
      $str_tanggal_post = strtotime($tanggal_post);
      $edit_lock = $str_tanggal_post.":".$author->ID;

      $post = $wpdb->get_row("SELECT * FROM ".$prefix."posts WHERE post_name='profile' AND post_status='publish' AND post_type='page'");

      $link_to_treasury = $wpdb->get_row("SELECT * FROM ".$prefix."posts WHERE post_name='link-to-treasury' AND post_status='publish' AND post_type='page'");

      if(!empty($post) && empty($link_to_treasury)) {
        $postID = $post->ID;

        //insert post
        $wpdb->query("INSERT INTO ".$prefix."posts (
            post_author,
            post_date,
            post_date_gmt,
            post_content,
            post_title,
            post_excerpt,
            post_status,
            comment_status,
            ping_status,
            post_password,
            post_name,
            to_ping,
            pinged,
            post_modified,
            post_modified_gmt,
            post_content_filtered,
            post_parent,
            guid,
            menu_order,
            post_type,
            post_mime_type,
            comment_count
          ) VALUES (
            ".$author->ID.",
            '".$tanggal_post."',
            '".$tanggal_post."',
            '',
            'Linking Akun Treasury ke Ladara Emas Account',
            '',
            'publish',
            'closed',
            'closed',
            '',
            'link-to-treasury',
            '',
            '',
            '".$tanggal_post."',
            '".$tanggal_post."',
            '',
            ".$postID.",
            'http://localhost/ladara/?page_id=',
            0,
            'page',
            '',
            0
          );");

        $sp = $wpdb->get_row("SELECT * FROM ".$prefix."posts WHERE post_title='Linking Akun Treasury ke Ladara Emas Account' AND post_name='link-to-treasury' AND post_status='publish' AND post_type='page'");
        $latestID = $sp->ID;

        //update guid
        $wpdb->query("UPDATE ".$prefix."posts SET
            guid = 'http://localhost/ladara/?page_id=".$latestID."'
          WHERE ID = ".$latestID);

        //insert postmeta        
        $wpdb->query("INSERT INTO ".$prefix."postmeta (post_id,meta_key,meta_value) VALUES (".$latestID.",'_edit_last','".$author->ID."');");
        $wpdb->query("INSERT INTO ".$prefix."postmeta (post_id,meta_key,meta_value) VALUES (".$latestID.",'_edit_lock','".$edit_lock."');");
        $wpdb->query("INSERT INTO ".$prefix."postmeta (post_id,meta_key,meta_value) VALUES (".$latestID.",'_wp_page_template','ladara-emas/link-to-treasury.php');");
      }

      update_option( 'insert_profile_link_treasury', $currentVersion );
      require_once( ABSPATH . 'wp-admin/includes/upgrade.php' );
    }
  }
}