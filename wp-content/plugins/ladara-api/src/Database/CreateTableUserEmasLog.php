<?php

namespace Ladara\Database;
if ( ! defined( 'ABSPATH' ) ) {
    exit;
}

/**
 * Class CreateTableUserEmasLog
 * @package Ladara\Database
 * @author Amy <laksmise@gmail.com>
 */
class CreateTableUserEmasLog
{
  /**
   * init function
   */
  public function init()
  {
    global $wpdb;
    $initialVersion = get_option( 'create_table_user_emas_log', 1 ); //initial version
    $currentVersion = 4; //versi sekarang kalau mau update silahkan nilainya ditambah
    $charset_collate = $wpdb->get_charset_collate();
    $table_name = $wpdb->prefix . 'user_emas_log'; // nama table

    if ( $initialVersion < $currentVersion) {
      $sql = "CREATE TABLE $table_name (
          id INT NOT NULL AUTO_INCREMENT,
          user_emas_id INT NULL,
          log VARCHAR(255) NULL DEFAULT NULL,
          created_date TIMESTAMP NULL DEFAULT NULL,
          PRIMARY KEY (id)
        ) $charset_collate;";
      update_option( 'create_table_user_emas_log', $currentVersion );
      require_once( ABSPATH . 'wp-admin/includes/upgrade.php' );
      $wpdb->query($sql);
    }
  }
}