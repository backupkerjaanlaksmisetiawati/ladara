<?php

namespace Ladara\Database;
if ( ! defined( 'ABSPATH' ) ) {
    exit;
}

/**
 * Class AddTableVoucherProductCategory
 * @package Ladara\Database
 * @author Amy <laksmise@gmail.com>
 */
class AddTableVoucherProductCategory
{
    /**
     * init function
     */
    public function init()
    {
        global $wpdb;
        $initialVersion = get_option( 'add_table_voucher_product_category', 1 ); //initial version
        $currentVersion = 3; //versi sekarang kalau mau update silahkan nilainya ditambah
        $charset_collate = $wpdb->get_charset_collate();
        $table_name = $wpdb->prefix . 'voucher_product_category'; // nama table

        if ( $initialVersion < $currentVersion) {            
            $sql = "CREATE TABLE IF NOT EXISTS $table_name (
              `id` bigint(20) NOT NULL AUTO_INCREMENT,
              `voucher_id` bigint(20) NOT NULL DEFAULT '0',
              `product_category_id` bigint(20) NOT NULL DEFAULT '0',
              `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
              PRIMARY KEY (`id`)
            ) $charset_collate ;";
            update_option( 'add_table_voucher_product_category', $currentVersion );
            require_once( ABSPATH . 'wp-admin/includes/upgrade.php' );
            dbDelta( $sql );
        }
    }
}