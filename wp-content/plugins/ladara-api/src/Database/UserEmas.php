<?php

namespace Ladara\Database;
if ( ! defined( 'ABSPATH' ) ) {
    exit;
}

/**
 * Class UserEmas
 * @package Ladara\Database
 * @author Amy <laksmise@gmail.com>
 */
class UserEmas
{
    /**
     * init function
     */
    public function init()
    {
        global $wpdb;
        $initialVersion = get_option( 'db_user_emas', 1 ); //initial version
        $currentVersion = 5; //versi sekarang kalau mau update silahkan nilainya ditambah
        $charset_collate = $wpdb->get_charset_collate();
        $table_name = $wpdb->prefix . 'user_emas'; // nama table

        if ( $initialVersion < $currentVersion) {            
            $sql = "CREATE TABLE $table_name(
                    id INT NOT NULL auto_increment,
                    user_id BIGINT NOT NULL DEFAULT '0',
                    name VARCHAR(255) DEFAULT NULL,
                    email VARCHAR(255) DEFAULT NULL,
                    web_password TEXT,
                    ts_password TEXT,
                    gender INT NOT NULL DEFAULT '0',
                    birthday DATE DEFAULT NULL,
                    referral_code VARCHAR(255) DEFAULT NULL,
                    phone VARCHAR(255) DEFAULT NULL,
                    security_question VARCHAR(255) DEFAULT NULL,
                    security_question_answer VARCHAR(255) DEFAULT NULL,
                    selfie_scan TEXT,
                    id_card_scan TEXT,
                    account_name VARCHAR(255) DEFAULT NULL,
                    account_number VARCHAR(255) DEFAULT NULL,
                    bank_code VARCHAR(255) DEFAULT NULL,
                    branch VARCHAR(255) DEFAULT NULL,
                    verified_user INT NOT NULL DEFAULT '0',
                    gold_balance FLOAT NOT NULL DEFAULT '0',
                    gold_balance_in_currency INT NOT NULL DEFAULT '0',
                    created_date TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
                    updated_date TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
                    constraint {$table_name}_pk primary key (id)
            ) $charset_collate ;";
            update_option( 'db_user_emas', $currentVersion );
            require_once( ABSPATH . 'wp-admin/includes/upgrade.php' );
            dbDelta( $sql );
        }
    }
}