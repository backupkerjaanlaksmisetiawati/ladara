<?php

namespace Ladara\Database;
if ( ! defined( 'ABSPATH' ) ) {
    exit;
}

/**
 * Class DropEmasLog
 * @package Ladara\Database
 * @author Amy <laksmise@gmail.com>
 */
class DropEmasLog
{
    /**
     * init function
     */
    public function init()
    {
        global $wpdb;
        $initialVersion = get_option( 'drop_db_emas_log', 1 ); //initial version
        $currentVersion = 2; //versi sekarang kalau mau update silahkan nilainya ditambah
        $charset_collate = $wpdb->get_charset_collate();
        $table_name = $wpdb->prefix . 'emas_log'; // nama table

        if ( $initialVersion < $currentVersion) {
            //drop table lama yang distaging, mau disesuikan dengan local gw yang udah fix
            $sql = "DROP TABLE IF EXISTS $table_name;";
            update_option( 'drop_db_emas_log', $currentVersion );
            require_once( ABSPATH . 'wp-admin/includes/upgrade.php' );
            $wpdb->query($sql);
        }
    }
}