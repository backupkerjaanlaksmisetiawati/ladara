<?php

namespace Ladara\Database;
if ( ! defined( 'ABSPATH' ) ) {
    exit;
}

/**
 * Class AddFieldLimitUsageVouchers
 * @package Ladara\Database
 * @author Amy <laksmise@gmail.com>
 */
class AddFieldLimitUsageVouchers
{
    /**
     * init function
     */
    public function init()
    {
        global $wpdb;
        $initialVersion = get_option( 'add_field_limit_usage_table_voucher', 1 ); //initial version
        $currentVersion = 5; //versi sekarang kalau mau update silahkan nilainya ditambah
        $charset_collate = $wpdb->get_charset_collate();
        $table_name = $wpdb->prefix . 'vouchers'; // nama table

        if ( $initialVersion < $currentVersion) {
          $check = $wpdb->query("SHOW COLUMNS FROM $table_name LIKE 'limit_usage'");
          if($check == false) {
            $sql = "ALTER TABLE $table_name ADD COLUMN `limit_usage` INT(11) NULL DEFAULT '0' AFTER `limit`";
            update_option( 'add_field_limit_usage_table_voucher', $currentVersion );
            require_once( ABSPATH . 'wp-admin/includes/upgrade.php' );
            $wpdb->query($sql);
          }
        }
    }
}