<?php

namespace Ladara\Database;
if ( ! defined( 'ABSPATH' ) ) {
    exit;
}

class NotificationToken
{
    /**
     * init function
     */
    public function init()
    {
        global $wpdb;
        $initialVersion = get_option( 'db_notification_token', 1 ); //initial version
        $currentVersion = 2; //versi sekarang kalau mau update silahkan nilainya ditambah
        $charset_collate = $wpdb->get_charset_collate();
        $table_name = $wpdb->prefix . 'notification_token'; // nama table

        if ( $initialVersion < $currentVersion) {
            $sql = "CREATE TABLE $table_name(
                    id int auto_increment,
                    user_id bigint not null,
                    token varchar(255) null,
                    date_created TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
                    constraint {$table_name}_pk primary key (id)
            ) $charset_collate";
            update_option( 'db_notification_token', $currentVersion );

            require_once( ABSPATH . 'wp-admin/includes/upgrade.php' );
            dbDelta( $sql );
        }
    }
}