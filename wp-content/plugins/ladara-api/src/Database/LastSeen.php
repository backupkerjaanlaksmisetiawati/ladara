<?php


namespace Ladara\Database;
if ( ! defined( 'ABSPATH' ) ) {
    exit;
}

/**
 * Class LastSeen
 * @package Ladara\Database
 * @author rikihandoyo
 */
class LastSeen
{
    /**
     * init function
     */
    public function init()
    {
        global $wpdb;
        $initialVersion = get_option( 'last_seen', 1 ); //initial version
        $currentVersion = 3; //versi sekarang kalau mau update silahkan nilainya ditambah
        $charset_collate = $wpdb->get_charset_collate();
        $table_name = $wpdb->prefix . 'last_seen'; // nama table

        if ( $initialVersion < $currentVersion) {
            $sql = "CREATE TABLE $table_name (
                    id int auto_increment,
                    user_id int null,
                    product_id int null,
                    date_created TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
                    constraint {$table_name}_pk primary key (id)
            ) $charset_collate";
            update_option( 'last_seen', $currentVersion );

            require_once( ABSPATH . 'wp-admin/includes/upgrade.php' );
            dbDelta( $sql );
        }
    }
}