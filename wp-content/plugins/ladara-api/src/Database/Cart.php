<?php


namespace Ladara\Database;
if ( ! defined( 'ABSPATH' ) ) {
    exit;
}

/**
 * Class Cart
 * @package Ladara\Database
 * @author rikihandoyo
 */
class Cart
{
    /**
     * init function
     */
    public function init()
    {
        global $wpdb;
        $initialVersion = get_option( 'tblCart', 1 ); //initial version
        $currentVersion = 2; //versi sekarang minimal 2 kalau mau update silahkan nilainya ditambah
        $charset_collate = $wpdb->get_charset_collate();
        $table_name = $wpdb->prefix . 'cart'; // nama table

        if ( $initialVersion < $currentVersion) {
            $sql = "CREATE TABLE $table_name (
                    id           int auto_increment primary key,
                    user_id      int      null,
                    product_id   int      null,
                    stock_id     int      null,
                    qty          int      null,
                    created_date datetime null
            ) $charset_collate";
            update_option( 'tblCart', $currentVersion );

            require_once( ABSPATH . 'wp-admin/includes/upgrade.php' );
            dbDelta( $sql );
        }
    }
}