<?php

namespace Ladara\Database;
if ( ! defined( 'ABSPATH' ) ) {
    exit;
}

/**
 * Class AddFieldStatusReasonEmas
 * @package Ladara\Database
 * @author Amy <laksmise@gmail.com>
 */
class AddFieldStatusReasonEmas
{
    /**
     * init function
     */
    public function init()
    {
        global $wpdb;
        $initialVersion = get_option( 'add_field_status_reason_table_ladara_emas', 1 ); //initial version
        $currentVersion = 3; //versi sekarang kalau mau update silahkan nilainya ditambah
        $charset_collate = $wpdb->get_charset_collate();
        $table_name = $wpdb->prefix . 'transaksi_emas'; // nama table

        if ( $initialVersion < $currentVersion) {
          $check = $wpdb->query("SHOW COLUMNS FROM $table_name LIKE 'status_reason'");
          if($check == false) {
            $sql = "ALTER TABLE $table_name ADD COLUMN status_reason VARCHAR(255) NULL DEFAULT NULL AFTER status;";
            update_option( 'add_field_status_reason_table_ladara_emas', $currentVersion );
            require_once( ABSPATH . 'wp-admin/includes/upgrade.php' );
            $wpdb->query($sql);
          }
        }
    }
}