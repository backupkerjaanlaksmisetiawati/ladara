<?php

namespace Ladara\Database;
if ( ! defined( 'ABSPATH' ) ) {
    exit;
}

/**
 * Class DropUserEmas
 * @package Ladara\Database
 * @author Amy <laksmise@gmail.com>
 */
class AlterIDCardScan
{
    /**
     * init function
     */
    public function init()
    {
        global $wpdb;
        $initialVersion = get_option( 'alter_id_card_scan_table_ladara_emas', 1 ); //initial version
        $currentVersion = 2; //versi sekarang kalau mau update silahkan nilainya ditambah
        $charset_collate = $wpdb->get_charset_collate();
        $table_name = $wpdb->prefix . 'user_emas'; // nama table

        if ( $initialVersion < $currentVersion) {
            $sql = "ALTER TABLE $table_name CHANGE COLUMN id_card_scan id_card_scan TEXT NULL DEFAULT NULL COLLATE 'utf8mb4_unicode_ci' AFTER selfie_scan;";
            update_option( 'alter_id_card_scan_table_ladara_emas', $currentVersion );
            require_once( ABSPATH . 'wp-admin/includes/upgrade.php' );
            $wpdb->query($sql);
        }
    }
}