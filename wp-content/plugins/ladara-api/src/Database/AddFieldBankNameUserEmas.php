<?php

namespace Ladara\Database;
if ( ! defined( 'ABSPATH' ) ) {
    exit;
}

/**
 * Class InsertUserEmasForTesting
 * @package Ladara\Database
 * @author Amy <laksmise@gmail.com>
 */
class AddFieldBankNameUserEmas
{
  /**
   * init function
   */
  public function init()
  {
    global $wpdb;
    $initialVersion = get_option( 'add_field_bank_name_db_user_emas', 1 ); //initial version
    $currentVersion = 3; //versi sekarang kalau mau update silahkan nilainya ditambah
    $charset_collate = $wpdb->get_charset_collate();
    $table_name = $wpdb->prefix . 'user_emas'; // nama table

    if ( $initialVersion < $currentVersion) {
      $sql = "ALTER TABLE " . $wpdb->prefix . "user_emas ADD COLUMN bank_name VARCHAR(255) NULL DEFAULT NULL AFTER bank_code;";
      $wpdb->query($sql);

      update_option( 'add_field_bank_name_db_user_emas', $currentVersion );
      require_once( ABSPATH . 'wp-admin/includes/upgrade.php' );
    }
  }
}