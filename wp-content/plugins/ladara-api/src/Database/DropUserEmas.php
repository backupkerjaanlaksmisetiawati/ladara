<?php

namespace Ladara\Database;
if ( ! defined( 'ABSPATH' ) ) {
    exit;
}

/**
 * Class DropUserEmas
 * @package Ladara\Database
 * @author Amy <laksmise@gmail.com>
 */
class DropUserEmas
{
    /**
     * init function
     */
    public function init()
    {
        global $wpdb;
        $initialVersion = get_option( 'drop_db_user_emas', 1 ); //initial version
        $currentVersion = 3; //versi sekarang kalau mau update silahkan nilainya ditambah
        $charset_collate = $wpdb->get_charset_collate();
        $table_name = $wpdb->prefix . 'users_emas'; // nama table

        if ( $initialVersion < $currentVersion) {
            //drop table lama yang distaging, mau disesuikan dengan local gw yang udah fix
            $sql = "DROP TABLE IF EXISTS $table_name;";
            update_option( 'drop_db_user_emas', $currentVersion );
            require_once( ABSPATH . 'wp-admin/includes/upgrade.php' );
            $wpdb->query($sql);
        }
    }
}