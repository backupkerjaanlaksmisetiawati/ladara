<?php

namespace Ladara\Database;
if ( ! defined( 'ABSPATH' ) ) {
    exit;
}

/**
 * Class TransaksiEmasLog
 * @package Ladara\Database
 * @author Amy <laksmise@gmail.com>
 */
class TransaksiEmasLog
{
    /**
     * init function
     */
    public function init()
    {
      global $wpdb;
      $initialVersion = get_option( 'db_transaksi_emas_log', 1 ); //initial version
      $currentVersion = 2; //versi sekarang kalau mau update silahkan nilainya ditambah
      $charset_collate = $wpdb->get_charset_collate();
      $table_name = $wpdb->prefix . 'transaksi_emas_log'; // nama table

      if ( $initialVersion < $currentVersion) {            
        $sql = "CREATE TABLE $table_name(
                id int(11) NOT NULL,
                transaksi_emas_id int(11) NOT NULL,
                log text NOT NULL,
                status varchar(50) NOT NULL,
                created_date timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
                constraint {$table_name}_pk primary key (id)
        ) $charset_collate ;";
        update_option( 'db_transaksi_emas_log', $currentVersion );
        require_once( ABSPATH . 'wp-admin/includes/upgrade.php' );
        dbDelta( $sql );
      }
    }
}