<?php

namespace Ladara\Controllers;
use Ladara\Helpers\BaseController;
use Ladara\Helpers\ControllerInterface;
use Ladara\Helpers\JneHelpers;
use Ladara\Helpers\Jwt as Token;
use WP_REST_Request;

if ( ! defined( 'ABSPATH' ) ) {
    exit;
}

class Shipping extends BaseController implements ControllerInterface
{

    /**
     * List service jne yang dipakai.
     * karena tidak semua service tersedia ini karens kerjasama bisnisnya seperti itu.
     * @var array
     */
    private $serviceJne = ['REG','CTC'];

    /**
     * @inheritDoc
     */
    public function register_routes()
    {
        register_rest_route( $this->nameSpace, 'shipping/get-price', [
            'methods' => 'GET',
            'callback' => [$this,'getPrice'],
        ]);

        register_rest_route( $this->nameSpace, 'shipping/tracking', [
            'methods' => 'POST',
            'callback' => [$this,'tracking'],
        ]);
    }

    /**
     * api get ongkir
     * @Get("/wp-json/ladara-api/v1/shipping/get-price")
     * @param WP_REST_Request $request
     * @return \WP_REST_Response
     */
    public function getPrice(WP_REST_Request $request)
    {
        $kodePos = $request->get_param('kode_pos');
//        $city = $request->get_param('city_name');
        $weight = $request->get_param('weight');
        $courier = $request->get_param('courier') ?? 'JNE';
        if (!$kodePos || !$weight){
            return $this->sendError([],'Error Parameter');
        }
        $ongkirJne = $this->get_ongkir_jne($kodePos, $weight);
        $resp = [];
        foreach ($ongkirJne as $ongkir){
            if (in_array($ongkir['service_display'],$this->serviceJne)){
                $service_display['service_display'] = 'JNE ('.$ongkir['service_display'].')';
                $resp['JNE'][] = array_merge($ongkir, $service_display);
            }
        }
        if (count($ongkirJne) == 0){
            return  $this->sendError([],'Price Not Found');
        }
        return $this->sendSuccess($resp,'Success Get ongkir');
    }

    /**
     * APi get history Pengiriman
     * @param WP_REST_Request $request
     * @return array|mixed|object|\WP_REST_Response
     */
    public function tracking(WP_REST_Request $request)
    {
        $noResi = $request->get_param('resi');

        $token = Token::validate_token();
        if($token['status'] != 200){
            return $token;
        }

        $tracking = (new JneHelpers())->tracking($noResi);
        unset($tracking->cnote);

        return $this->sendSuccess($tracking,'Success');
    }

    /**
     * bikin fungsi get ongkir sendiri biar gak kegangu2
     * @param $kode_pos
     * @param $weight
     * @return array|mixed
     */
    function get_ongkir_jne($kode_pos, $weight){

        $destination_code = '';
        global $wpdb;
        $wpdb_query = "SELECT * FROM jne_destination WHERE zip_code = '$kode_pos'";
        $res_query_jne = $wpdb->get_results($wpdb_query, OBJECT);
        $count_res = count($res_query_jne);
        if($count_res > 0){
            foreach ($res_query_jne as $key => $value){
                $id_kota = $value->id;
                $destination_code = $value->CITY_CODE;

            }
        }

        $username = getenv('JNE_USERNAME');
        $apikey = getenv('JNE_API_KEY');
        $url_jne = getenv('JNE_PRICE_ENDPOINT');

        $from_jne = 'CGK10000'; // jakarta
        // $to_jne = 'BDO10000';
        $to_jne = $destination_code;
        $weight_jne = $weight ?? 1;

        $head_key = array(
            "content-type: application/x-www-form-urlencoded"
        );

        $postfield = 'username='.$username.'&api_key='.$apikey.'&from='.$from_jne.'&thru='.$to_jne.'&weight='.$weight_jne;

        $curl = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_URL => $url_jne,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "POST",
            CURLOPT_POSTFIELDS => $postfield,
            CURLOPT_HTTPHEADER => $head_key,
        ));
        $response = curl_exec($curl);
        $err = curl_error($curl);
        curl_close($curl);

        $response = json_decode($response,true);

        if( isset($response['price']) AND !empty($response['price']) ){
            $response_data = $response['price'];
        }else{
            $response_data = array();
        }

        return $response_data;
    }



}