<?php


namespace Ladara\Controllers;

if (! defined('ABSPATH')) {
    exit;
}

use Ladara\Helpers\BaseController;
use Ladara\Helpers\builder\OrderBuilder;
use Ladara\Helpers\builder\PaymentBuilder;
use Ladara\Helpers\builder\ShippingBuilder;
use Ladara\Helpers\builder\UsersBuilder;
use Ladara\Helpers\ControllerInterface;
use Ladara\Helpers\email\EmailHelper;
use Ladara\Helpers\Jwt as Token;
use Ladara\Models\Notifications;
use WP_REST_Request;
use Xendit\Exceptions\ApiException;
use Xendit\Invoice;
use Xendit\VirtualAccounts;

/**
 * Class Xendit
 * @package Ladara\Controllers
 * @author Yusuf The Dragon
 */
class Xendit extends BaseController implements ControllerInterface
{
    /**
     * @inheritDoc
     */
    public function register_routes()
    {
        register_rest_route($this->nameSpace, 'xendit/get-list-bank', [
            'methods' => 'GET',
            'callback' => [$this, 'getListBank'],
        ]);

        // register_rest_route($this->nameSpace, 'xendit/create-invoice', [
        //     'methods' => 'POST',
        //     'callback' => [$this, 'createInvoice'],
        // ]);

        register_rest_route($this->nameSpace, 'xendit/cancel-invoice', [
            'methods' => 'POST',
            'callback' => [$this, 'cancelInvoice'],
        ]);

        register_rest_route($this->nameSpace, 'xendit/calculate-amount-invoice', [
            'methods' => 'POST',
            'callback' => [$this, 'calculateAmountInvoice'],
        ]);

        register_rest_route($this->nameSpace, 'xendit/get-invoice', [
            'methods' => 'GET',
            'callback' => [$this, 'getInvoice'],
        ]);
    }

    /**
     * Get list of available Bank for payment.
     * @Get ("/ladara-api/v1/xendit/get-list-bank")
     * @return array|mixed|object|\WP_REST_Response
     */
    public function getListBank()
    {
        $result = VirtualAccounts::getVABanks();
        
        $result = array_filter($result, function ($fn) {
            return $fn['code'] !== 'BCA';
        });

        return $this->sendSuccess($result, 'Success get banks.');
    }

    /**
     * Create invoice based on selected bank.
     * @Post ("/ladara-api/v1/xendit/create-invoice")
     * @param WP_REST_Request $request
     * @return array|mixed|object|\WP_REST_Response
     */
    public function createInvoice(WP_REST_Request $request)
    {
        global $wpdb;

        $token = Token::validate_token();

        if ($token['status'] !== 200) {
            return $token;
        }

        $selectedBank = $request->get_param('selected_bank');
        $orderId = $request->get_param('order_id');
        $amount = intval($request->get_param('amount'));
        $userId = $token['data']['user_id'] ?? 0;
        $displayName = $token['data']['display_name'] ?? 'Yusuf The Dragon';

        $user = $wpdb->get_row("SELECT * FROM ldr_users WHERE id = $userId");
        $userEmail = $user->user_email;

        $urlInstructions = "https://invoice.xendit.co/static/locales/id/bni_instructions.json";

        if ($selectedBank === 'MANDIRI') {
            $urlInstructions = "https://invoice.xendit.co/static/locales/id/mandiri_instructions.json";
        } elseif ($selectedBank === 'BCA') {
            $urlInstructions = "https://invoice.xendit.co/static/locales/id/bca_instructions.json";
        } elseif ($selectedBank === 'PERMATA') {
            $urlInstructions = "https://invoice.xendit.co/static/locales/id/permata_instructions.json";
        } elseif ($selectedBank === 'BRI') {
            $urlInstructions = "https://invoice.xendit.co/static/locales/id/bri_instructions.json";
        }

        $wpdb_query = "SELECT * FROM ldr_orders WHERE order_id = $orderId AND user_id = $userId AND order_status = 'wc-pending' LIMIT 1";
        $res_query = $wpdb->get_row($wpdb_query, OBJECT);
        $post = [];

        if ($res_query === null) {
            return $this->sendError('Order not found.', 'Invalid Order', 404);
        }

        if ($res_query->xendit_id === null) {
            $shippingTotal = $res_query->shipping_price;
            $totalPrice = $res_query->total_price;

            $feeQuery = "SELECT * FROM ldr_xendit_fee";
            $feeResult = $wpdb->get_results($feeQuery, OBJECT);

            $dataVA = array_values(array_filter($feeResult, function ($item) {
                return $item->category === 'va';
            }));

            $dataCC = array_values(array_filter($feeResult, function ($item) {
                return $item->category === 'cc';
            }));

            if (empty($dataVA)) {
                return $this->sendError('Data Payment Not Found.', 'Failed to Calculate Invoice.', 404);
            } else {
                $dataVA = $dataVA[0];
            }

            if (empty($dataCC)) {
                return $this->sendError('Data Payment Not Found.', 'Failed to Calculate Invoice.', 404);
            } else {
                $dataCC = $dataCC[0];
            }

            if ($selectedBank === 'CREDIT_CARD') {
                $fixedFeeCC = floatval($dataCC->fee_price);
                $ppn1CC = round(floatval($dataCC->fee_ppn) * $fixedFeeCC);
                $variableFeeCC = round(floatval($dataCC->fee_cc) * $totalPrice);
                $ppn2CC = round(floatval($dataCC->fee_ppn) * $variableFeeCC);
                $amountCC = round($totalPrice + $fixedFeeCC + $ppn1CC + $variableFeeCC + $ppn2CC);
                $amountVA = 0;
                $xenditFee = $amountCC - $totalPrice;
            } else {
                $fixedFeeVA = floatval($dataVA->fee_price);
                $ppn1VA = floatval($dataVA->fee_ppn) * $fixedFeeVA;
                $amountVA = $totalPrice + $ppn1VA + $fixedFeeVA;
                $amountCC = 0;
                $xenditFee = $amountVA - $totalPrice;
            }

            if (intval($amountVA) !== intval($amount) && intval($amountCC) !== intval($amount)) {
                return $this->sendError('Amount is invalid.', 'Invalid Amount', 422);
            }

            $post = Invoice::create([
                'external_id' => 'invoice-'.$orderId,
                'amount' => $amount,
                'description' => 'Invoice #'.$orderId,
                'payer_email' => $userEmail,
                'invoice_duration' => 7200,
                'payment_methods' => [$selectedBank],
                'should_send_email' => false
            ]);

            $wpdb->update('ldr_orders', [
                'xendit_id' => $post['id'],
                'xendit_url' => $post['invoice_url'],
                'xendit_fee' => $xenditFee
            ], ['order_id' => $orderId]);

            $paymentQuery = "SELECT * FROM ldr_payment_log WHERE order_id = '$orderId'";
            $paymentResult = $wpdb->get_row($paymentQuery, OBJECT);

            if ($paymentResult === null) {
                if ($selectedBank === 'CREDIT_CARD') {
                    $paymentMethod = 'CREDIT_CARD';
                    $bankCode = '';
                } else {
                    $paymentMethod = 'BANK_TRANSFER';
                    $bankCode = $selectedBank;
                }

                $wpdb->insert('ldr_payment_log', [
                    'order_id' => $orderId,
                    'xendit_id' => $post['id'],
                    'external_id' => $post['external_id'],
                    'payment_method' => $paymentMethod,
                    'status' => $post['status'],
                    'paid_amount' => $post['amount'],
                    'bank_code' => $bankCode,
                    'paid_at' => '',
                    'adjusted_received_amount' => '',
                    'currency' => $post['currency'],
                    'type' => 'product'
                ]);
            }
        } else {
            if ($selectedBank === 'CREDIT_CARD') {
                $post['invoice_url'] = $res_query->xendit_url;
            } else {
                try {
                    $xenditId = $res_query->xendit_id ?? '123';
                    $invoice = Invoice::retrieve($xenditId);
                } catch (ApiException $e) {
                    $wpdb->update('ldr_orders', [
                        'xendit_id' => null,
                        'xendit_url' => null,
                        'xendit_fee' => null
                    ], ['order_id' => $orderId]);

                    return $this->sendError('Please Try Again.', 'Create Invoice Failed.', 500);
                }

                $post['available_banks'][0]['bank_account_number'] = $invoice['available_banks'][0]['bank_account_number'];
            }
        }

        if ($selectedBank === 'CREDIT_CARD') {
            $result = [
                'invoice_url' => $post['invoice_url']
            ];
        } else {
            $accountNumber = $post['available_banks'][0]['bank_account_number'];

            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $urlInstructions);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            $result = curl_exec($ch);
            curl_close($ch);

            $instructions = json_decode($result, true);
            $resultInstructions = [];

            foreach ($instructions as $key => $instruction) {
                $method = strtoupper($key);
    
                if (strpos($method, "ATM") !== false || strpos($method, "SMS") !== false) {
                    $method = substr_replace($method, " ", 3, 0);
                } elseif (strpos($method, "MOBILEBANKING") !== false) {
                    $method = substr_replace($method, " ", 6, 0);
                    $method = substr_replace($method, " ", 14, 0);
                } elseif (strpos($method, "IBANKPERSONAL") !== false) {
                    $method = substr_replace($method, " ", 5, 0);
                    $method = substr_replace($method, " ", 14, 0);
                } elseif (strpos($method, "TELLER") !== false) {
                    $method = substr_replace($method, " ", 6, 0);
                } elseif (strpos($method, "OTHER") !== false) {
                    $method = 'BANK LAIN';
                } elseif (strpos($method, "PERMATAMOBILE") !== false) {
                    $method = substr_replace($method, " ", 7, 0);
                    $method = substr_replace($method, " ", 14, 0);
                    $method = trim($method);
                } elseif (strpos($method, "INTERNETBANKING") !== false) {
                    $method = substr_replace($method, " ", 8, 0);
                    $method = substr_replace($method, " ", 16, 0);
                }
    
                $resultInstructions[$method] = [];
    
                foreach ($instruction as $step) {
                    $fixedStep = str_replace('{{companyCode}}', '88908', $step);
                    $fixedStep = str_replace('{{companyName}}', '88908 XENDIT', $fixedStep);
                    $fixedStep = str_replace('{{- vaNumber}}', $accountNumber, $fixedStep);
                    $resultInstructions[$method][] = $fixedStep;
                }
            }
        
            $result = [
                'bank_account_number' => $accountNumber,
                'instructions' => $resultInstructions
            ];

            if ($res_query->xendit_id === null) {
                $explodeCourier = explode('(', $res_query->courier_name);
                $courier = trim($explodeCourier[0]);
                $service = trim(str_replace(')', '', $explodeCourier[1]));
                $textDate = date("dmY", strtotime($res_query->created_date));
                $invoiceNumber = 'INV/'.$textDate.'/'.$orderId;
                $ordersItems = $wpdb->get_results("SELECT ldr_orders_item.product_id, ldr_orders_item.qty, ldr_orders_item.price, ldr_posts.post_title FROM ldr_orders_item, ldr_posts WHERE ldr_orders_item.product_id = ldr_posts.id AND ldr_orders_item.order_id = '$orderId' ", OBJECT);
                $ordersItemsArr = array_map(function ($fn) {
                    return [
                        'name' => $fn->post_title,
                        'qty' => $fn->qty,
                        'price' => intval($fn->qty * $fn->price)
                    ];
                }, $ordersItems);

                $user = new UsersBuilder($displayName, $userEmail);
                $shippingBuilder = new ShippingBuilder($courier, $service, intval($res_query->shipping_price));
                $orderBuilder = new OrderBuilder();
                $orderBuilder->setDateCreated($res_query->created_date);
                $orderBuilder->setTotalPrice($res_query->total_price);
                $orderBuilder->setInvoiceNo($invoiceNumber);
                $orderBuilder->setStatus('Menunggu Pembayaran');
                $orderBuilder->setItems($ordersItemsArr);

                $paymentBuilder = new PaymentBuilder();
                $paymentBuilder->setPaymentExpired(date('Y-m-d H:i:s', strtotime($res_query->created_date) + 60 * 60 * 2));
                $paymentBuilder->setBank($selectedBank);
                $paymentBuilder->setVa(123);
                $paymentBuilder->setPaymentInstruction($resultInstructions);

                $email = new EmailHelper($user);
                $email->menungguPembayaran($orderBuilder, $paymentBuilder, $shippingBuilder);

                $data = [
                    'userId' => $userId,
                    'title' => 'Pesanan '.$orderId.' sudah di buat.',
                    'descriptions' => 'Terima kasih sudah melakukan pemesanan, silahkan lakukan pembayaran untuk menghindari pesanan di batalkan secara otomatis.',
                    'type' => 'pesanan',
                    'orderId' => $orderId,
                    'data' => []
                ];
                
                Notifications::addNotification($data);
            }
        }

        return $this->sendSuccess($result, 'Success create invoice.');
    }

    /**
     * Cancel invoice that has been created.
     * @Post ("/ladara-api/v1/xendit/cancel-invoice")
     * @param WP_REST_Request $request
     * @return array|mixed|object|\WP_REST_Response
     */
    public function cancelInvoice(WP_REST_Request $request)
    {
        global $wpdb;

        $token = Token::validate_token();

        if ($token['status'] !== 200) {
            return $token;
        }

        date_default_timezone_set("Asia/Jakarta");
        $currentDate = date('Y-m-d H:i:s');
        $userId = $token['data']['user_id'] ?? 0;
        $orderId = $request->get_param('order_id');
        $wpdb_query = "SELECT * FROM ldr_orders WHERE order_id = $orderId AND user_id = $userId AND order_status = 'wc-pending' LIMIT 1";
        $query = $wpdb->get_row($wpdb_query, OBJECT);

        try {
            Invoice::expireInvoice($query->xendit_id);
        } catch (ApiException $e) {
            // Continue
        }

        $log_text = 'Pembayaran sedang di cek Order ID #'.$query->order_id.', Status menjadi cancelled';

        $wpdb->query("UPDATE ldr_orders SET order_status = 'wc-cancelled' WHERE id = $query->id");
        $wpdb->query("INSERT INTO ldr_order_log(order_id, log, created_date, order_status) VALUES('$query->order_id', '$log_text', '$currentDate', 'cancelled')");
        $wpdb->query("UPDATE ldr_payment_log SET status = 'EXPIRED' WHERE order_id = $query->order_id");

        return $this->sendSuccess('Cancel Invoice Success.', 'Successfully cancel invoice.');
    }

    /**
     * Calculate total amount invoice.
     * @Post ("/ladara-api/v1/xendit/calculate-amount-invoice")
     * @param WP_REST_Request $request
     * @return array|mixed|object|\WP_REST_Response
     */
    public function calculateAmountInvoice(WP_REST_Request $request)
    {
        global $wpdb;

        $token = Token::validate_token();

        if ($token['status'] !== 200) {
            return $token;
        }

        $amount = round($request->get_param('amount'));
        $shippingTotal = round($request->get_param('shipping_total'));
        $orderId = $request->get_param('order_id');

        $feeQuery = "SELECT * FROM ldr_xendit_fee";
        $feeResult = $wpdb->get_results($feeQuery, OBJECT);

        $dataVA = array_values(array_filter($feeResult, function ($item) {
            return $item->category === 'va';
        }));

        $dataCC = array_values(array_filter($feeResult, function ($item) {
            return $item->category === 'cc';
        }));

        if (empty($dataVA)) {
            return $this->sendError('Data Payment Not Found.', 'Failed to Calculate Invoice.', 404);
        } else {
            $dataVA = $dataVA[0];
        }

        if (empty($dataCC)) {
            return $this->sendError('Data Payment Not Found.', 'Failed to Calculate Invoice.', 404);
        } else {
            $dataCC = $dataCC[0];
        }

        // Virtual Account
        $fixedFeeVA = floatval($dataVA->fee_price);
        $ppn1VA = floatval($dataVA->fee_ppn) * $fixedFeeVA;
        $amountVA = $amount + $ppn1VA + $fixedFeeVA;

        // Credit Card
        $fixedFeeCC = floatval($dataCC->fee_price);
        $ppn1CC = round(floatval($dataCC->fee_ppn) * $fixedFeeCC);
        $variableFeeCC = round(floatval($dataCC->fee_cc) * $amount);
        $ppn2CC = round(floatval($dataCC->fee_ppn) * $variableFeeCC);
        $amountCC = round($amount + $fixedFeeCC + $ppn1CC + $variableFeeCC + $ppn2CC);

        $order = $wpdb->get_row("SELECT * FROM ldr_orders WHERE order_id = $orderId");

        $result = [
            'va' => [
                'amount' => $amount,
                'admin' => $fixedFeeVA,
                'ppn' => $ppn1VA,
                'total' => $amountVA,
                'courier_name' => $order->courier_name,
                'shipping_price' => $order->shipping_price
            ],
            'cc' => [
                'amount' => $amount,
                'admin' => $fixedFeeCC + $variableFeeCC,
                'ppn' => $ppn1CC + $ppn2CC,
                'total' => $amountCC,
                'courier_name' => $order->courier_name,
                'shipping_price' => $order->shipping_price
            ]
        ];

        return $this->sendSuccess($result, 'Successfully calculate amount invoice.', 200);
    }

    /**
     * Get invoice data.
     * @Get ("/ladara-api/v1/xendit/get-invoice")
     * @param WP_REST_Request $request
     * @return array|mixed|object|\WP_REST_Response
     */
    public function getInvoice(WP_REST_Request $request)
    {
        $invoice_id = $request->get_param('invoice_id');

        $invoice = Invoice::retrieve($invoice_id);

        if ($invoice['status'] === 'PAID' || $invoice['status'] === 'SETTLED') {
            return $this->sendSuccess(true, 'Successfully get invoice.', 200);
        } else {
            return $this->sendSuccess(false, 'Successfully get invoice.', 200);
        }
    }
}
