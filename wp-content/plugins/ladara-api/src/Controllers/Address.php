<?php
namespace Ladara\Controllers;

if ( ! defined( 'ABSPATH' ) ) {
    exit;
}
use Ladara\Helpers\BaseController;
use Ladara\Helpers\ControllerInterface;
use Ladara\Helpers\Jwt as Token;
use WP_REST_Request;

/**
 * Class Address
 * @package Ladara\Controllers
 */
class Address extends BaseController implements ControllerInterface
{

    /**
     * @inheritDoc
     */
    public function register_routes()
    {
        register_rest_route( $this->nameSpace, 'address', [
            'methods' => 'GET',
            'callback' => [$this,'get_address'],
        ]);

        register_rest_route( $this->nameSpace, 'address', [
            'methods' => 'POST',
            'callback' => [$this,'add_address'],
        ]);

        register_rest_route( $this->nameSpace, 'address', [
            'methods' => 'PUT',
            'callback' => [$this,'edit_address'],
        ]);

        register_rest_route( $this->nameSpace, 'address/(?P<id>\d+)', [
            'methods' => 'DELETE',
            'callback' => [$this,'delete_address'],
        ]);

        register_rest_route( $this->nameSpace, 'address/provinsi', [
            'methods' => 'GET',
            'callback' => [$this,'get_provinsi'],
        ]);

        register_rest_route( $this->nameSpace, 'address/kota', [
            'methods' => 'GET',
            'callback' => [$this,'get_kota'],
        ]);

        register_rest_route( $this->nameSpace, 'address/kecamatan', [
            'methods' => 'GET',
            'callback' => [$this,'get_kecamatan'],
        ]);

        register_rest_route( $this->nameSpace, 'address/kodepos', [
            'methods' => 'GET',
            'callback' => [$this,'get_kodepos'],
        ]);
    }

    /**
     * get data provinsi
     * @Get("ladara-api/v1/address/provinsi")
     * @param WP_REST_Request $request
     * @return array|mixed|object|\WP_REST_Response
     */
    public function get_provinsi(WP_REST_Request $request){

        global $wpdb;

        $resp = [];
                
        $wpdb_query = "SELECT DISTINCT province_name as name FROM jne_destination ORDER BY province_name ASC";
        $res_query = $wpdb->get_results($wpdb_query, OBJECT);
        $count_res = count($res_query);
        if($count_res > 0){
            $resp = $res_query;
        }
        
        return $this->sendSuccess($resp, 'Success get provinsi');
    }

    /**
     * get data kota
     * @Get("ladara-api/v1/address/kota")
     * @param WP_REST_Request $request
     * @return array|mixed|object|\WP_REST_Response
     */
    public function get_kota(WP_REST_Request $request){

        global $wpdb;

        $provinsi_name = $request->get_param('provinsi_name');

        $resp = [];
        $wpdb_query = "SELECT DISTINCT city_name as name
                        FROM jne_destination
                        WHERE PROVINCE_NAME = '$provinsi_name'
                        AND CITY_NAME != '$provinsi_name'
                        ORDER BY CITY_NAME ASC";

        $res_query = $wpdb->get_results($wpdb_query, OBJECT);
        $count_res = count($res_query);
        if($count_res > 0){
            $resp = $res_query;
        }
        
        return $this->sendSuccess($resp, 'Success get kota');
    }

    /**
     * get data kecamatan
     * @Delete("ladara-api/v1/address/regions")
     * @param WP_REST_Request $request
     * @return array|mixed|object|\WP_REST_Response
     */
    public function get_kecamatan(WP_REST_Request $request){

        $kota_name = $request->get_param('kota_name');

        global $wpdb;

        $resp = [];
                
        $wpdb_query = "SELECT DISTINCT district_name as name
                    FROM jne_destination
                    WHERE CITY_NAME = '$kota_name'
                    AND DISTRICT_NAME != '$kota_name'
                    ORDER BY DISTRICT_NAME ASC";

        $res_query = $wpdb->get_results($wpdb_query, OBJECT);
        $count_res = count($res_query);
        if($count_res > 0){
            $resp = $res_query;
        }
        
        return $this->sendSuccess($resp, 'Success get kecamatan');
    }

    /**
     * get data kodepos
     * @Get("ladara-api/v1/address/regions")
     * @param WP_REST_Request $request
     * @return array|mixed|object|\WP_REST_Response
     */
    public function get_kodepos(WP_REST_Request $request){

        $kecamatan = $request->get_param('kecamatan');

        global $wpdb;

        $resp = [];
                
        $wpdb_query = "SELECT DISTINCT ZIP_CODE as kode_pos
                        FROM jne_destination
                        WHERE DISTRICT_NAME = '$kecamatan'
                        ORDER BY ZIP_CODE ASC";

        $res_query = $wpdb->get_results($wpdb_query, OBJECT);
        $count_res = count($res_query);
        if($count_res > 0){
            $resp = $res_query;
        }
        
        return $this->sendSuccess($resp, 'Success get kodepos');
    }

    /**
     * delete address user
     * @Delete("ladara-api/v1/address")
     * @param WP_REST_Request $request
     * @return array|mixed|object|\WP_REST_Response
     */
    public function delete_address(WP_REST_Request $request)
    {
        $addr_id = $request->get_param('id');

        global $wpdb;

        $token = Token::validate_token();
        if($token['status'] != 200){
            return $token;
        }
    
        $user_id = $token['data']['user_id'];
        $wpdb_query = "SELECT * FROM ldr_user_address WHERE id = '$addr_id'";
        $res_query = $wpdb->get_results($wpdb_query, OBJECT);
        $count_res = count($res_query);
        
        if($count_res <= 0){
            return $this->sendError([], 'Address ID is not found', 401);
        }

        // $delete_query = "DELETE FROM ldr_user_address WHERE id=$addr_id";
        $res_delete = $wpdb->delete('ldr_user_address', array('ID' => $addr_id));
        if($res_update === false){
            return $this->sendError([],'Failed to update address', 403);
        }
        return $this->sendSuccess([], 'Address deleted');
    }

    /**
     * get addresses user
     * @Get("ladara-api/v1/address")
     * @param WP_REST_Request $request
     * @return array|mixed|object|\WP_REST_Response
     */
    public function get_address(WP_REST_Request $request)
    {
        global $wpdb;

        $token = Token::validate_token();
        if($token['status'] != 200){
            return $token;
        }
    
        $user_id = $token['data']['user_id'];
        $wpdb_query = "SELECT * FROM ldr_user_address WHERE user_id=$user_id";
        $res_query = $wpdb->get_results($wpdb_query, OBJECT);
        return $this->sendSuccess($res_query, 'User addresses');
    }

    /**
     * fungsi add address user
     * @Post("ladara-api/v1/add_address")
     * @param WP_REST_Request $request
     * @return array|mixed|object|\WP_REST_Response
     */
    public function add_address(WP_REST_Request $request){
        $addr_nama = $request->get_param('nama_alamat');
        $addr_penerima = $request->get_param('nama_penerima');
        $addr_telepon = $request->get_param('telepon');
        $addr_alamat = $request->get_param('alamat');
        $addr_provinsi = $request->get_param('provinsi');
        $addr_kota = $request->get_param('kota');
        $addr_kecamatan = $request->get_param('kecamatan');
        $addr_kode_pos = $request->get_param('kode_pos');
        $addr_longitude = $request->get_param('longitude');
        $addr_latitude = $request->get_param('latitude');
        $addr_alamat_utama = $request->get_param('alamat_utama');

        global $wpdb;

        $token = Token::validate_token();
        if($token['status'] != 200){
            return $token;
        }

        $user_id = $token['data']['user_id'];
        if($addr_alamat_utama == 1){
            //Ubah semua address user jadi non-main address jika user set main address baru
            $res_update = $wpdb->update('ldr_user_address', array('alamat_utama' => 0), array( 'user_id'=>$user_id ));
        }
        //Add address
        $res_insert = $wpdb->insert('ldr_user_address', array(
            'user_id' => $user_id,
            'nama_alamat' => $addr_nama,
            'nama_penerima' => $addr_penerima,
            'telepon' => $addr_telepon,
            'provinsi' => $addr_provinsi,
            'kota' => $addr_kota,
            'kecamatan' => $addr_kecamatan,
            'alamat' => $addr_alamat,
            'kode_pos' => $addr_kode_pos,
            'longitude' => $addr_longitude,
            'latitude' => $addr_latitude,
            'alamat_utama' => $addr_alamat_utama,
            'created_date' => current_time( 'mysql', true)
            )
        );
        if($res_insert == false){
            return $this->sendError([], 'Failed to create address', 403);
        }
        $insert_id = $wpdb->insert_id;
        $wpdb_query = "SELECT * FROM ldr_user_address WHERE id='$insert_id'";
        $res_query = $wpdb->get_results($wpdb_query, OBJECT);
        $address = $res_query[0];
        $res = [
            'nama_alamat' => $address->nama_alamat,
            'nama_penerima' => $address->nama_penerima,
            'telepon' => $address->telepon,
            'provinsi' => $address->provinsi,
            'alamat' => $address->alamat,
            'kota' => $address->kota,
            'kecamatan' => $address->kecamatan,
            'kode_pos' => $address->kode_pos,
            'longitude' => $address->longitude,
            'latitude' => $address->latitude,
            'alamat_utama' => $address->alamat_utama,
        ];
        return $this->sendSuccess($res, 'Successfully create address');
    }

    /**
     * fungsi edit address user
     * @Post("ladara-api/v1/edit_address")
     * @param WP_REST_Request $request
     * @return array|mixed|object|\WP_REST_Response
     */
    public function edit_address(WP_REST_Request $request){
        $addr_id = $request->get_param('address_id');
        $addr_nama = $request->get_param('nama_alamat');
        $addr_penerima = $request->get_param('nama_penerima');
        $addr_telepon = $request->get_param('telepon');
        $addr_alamat = $request->get_param('alamat');
        $addr_provinsi = $request->get_param('provinsi');
        $addr_kota = $request->get_param('kota');
        $addr_kecamatan = $request->get_param('kecamatan');
        $addr_kode_pos = $request->get_param('kode_pos');
        $addr_longitude = $request->get_param('longitude');
        $addr_latitude = $request->get_param('latitude');
        $addr_alamat_utama = $request->get_param('alamat_utama');

        global $wpdb;

        $token = Token::validate_token();
        if($token['status'] != 200){
            return $token;
        }

        $user_id = $token['data']['user_id'];

        $wpdb_query = "SELECT * FROM ldr_user_address WHERE id = '$addr_id'";
        $res_query = $wpdb->get_results($wpdb_query, OBJECT);
        $count_res = count($res_query);

        if($count_res <= 0){
            return $this->sendError([], 'Address ID is not found', 401);
        }
        
        if($addr_alamat_utama == 1){
            //Ubah semua address user jadi non-main address jika user set main address baru
            $res_update = $wpdb->update('ldr_user_address', array('alamat_utama' => 0), array( 'user_id'=>$user_id ));
        }
        //Update address
        $res_update = $wpdb->update('ldr_user_address', array(
            'nama_alamat' => $addr_nama,
            'nama_penerima' => $addr_penerima,
            'telepon' => $addr_telepon,
            'provinsi' => $addr_provinsi,
            'kota' => $addr_kota,
            'kecamatan' => $addr_kecamatan,
            'alamat' => $addr_alamat,
            'kode_pos' => $addr_kode_pos,
            'longitude' => $addr_longitude,
            'latitude' => $addr_latitude,
            'alamat_utama' => $addr_alamat_utama
            ), array('id'=>$addr_id)
        );
        if($res_update === false){
            return $this->sendError([],'Failed to update address', 403);
        }
        $wpdb_query = "SELECT * FROM ldr_user_address WHERE id=$addr_id";
        $res_query = $wpdb->get_results($wpdb_query, OBJECT);
        $address = $res_query[0];
        $res = [
            'nama_alamat' => $address->nama_alamat,
            'nama_penerima' => $address->nama_penerima,
            'telepon' => $address->telepon,
            'provinsi' => $address->provinsi,
            'kota' => $address->kota,
            'kecamatan' => $address->kecamatan,
            'alamat' => $address->alamat,
            'kode_pos' => $address->kode_pos,
            'longitude' => $address->longitude,
            'latitude' => $address->latitude,
            'alamat_utama' => $address->alamat_utama,
        ];
        return $this->sendSuccess($res, 'Successfully update address');
    }
}