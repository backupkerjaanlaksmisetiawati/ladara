<?php

namespace Ladara\Controllers;
if ( ! defined( 'ABSPATH' ) ) {
    exit;
}
use Ladara\Helpers\BaseController;
use Ladara\Helpers\ControllerInterface;
use Ladara\Models\Products as ProductModel;

/**
 * Class Categories
 * @package Ladara\Controllers
 * @author rikihandoyo
 */
class Categories extends BaseController implements ControllerInterface
{

    /**
     * @inheritDoc
     */
    public function register_routes()
    {
        register_rest_route( $this->nameSpace, 'categories', [
            'methods' => 'GET',
            'callback' => [$this,'getCategories'],
        ]);
    }

    /**
     * get all categories
     * @Get("/ladara-api/v1/categories")
     * @return \WP_REST_Response
     */
    public function getCategories()
    {
       $categories = ProductModel::getCategories();
       $resp = $this->buildTree($categories);

       return $this->sendSuccess($resp, 'Success get categories');
    }

    /**
     * function untuk build tree category
     * @param $categories
     * @param int $parent
     * @return array
     */
    public function buildTree($categories, $parent = 0)
    {
        $data = [];
        foreach ($categories as $category){
            $thumbnail_id = get_term_meta($category->term_id,'thumbnail_id', true );
            //Default image
            $category->image = get_site_url().'/wp-content/uploads/woocommerce-placeholder-125x125.png';
            if ($thumbnail_id){
                $category->image = wp_get_attachment_url($thumbnail_id);
            }
            if ($category->parent == $parent){
                $category->child = [];
                $child = $this->buildTree($categories,$category->term_id);
                if ($child){
                    $category->child = $child;
                }
                $data[] = $category;
            }
        }
        return $data;
    }

}