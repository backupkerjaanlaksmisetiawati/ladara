<?php
namespace Ladara\Controllers;

if ( ! defined( 'ABSPATH' ) ) {
    exit;
}

use Ladara\Helpers\BaseController;
use Ladara\Helpers\builder\UsersBuilder;
use Ladara\Helpers\ControllerInterface;
use Ladara\Helpers\email\Email;
use Ladara\Helpers\email\EmailHelper;
use Ladara\Helpers\Jwt as Token;
use WP_Error;
use WP_REST_Request;
use Ladara\Models\Notifications;

/**
 * Class Auth
 * @package Ladara\Controllers
 * @author rikihandoyo
 */
class Auth extends BaseController implements ControllerInterface
{
    /**
     * @inheritDoc
     */
    public function register_routes()
    {
        register_rest_route( $this->nameSpace, 'profile', [
            'methods' => 'GET',
            'callback' => [$this,'profile'],
        ]);

        register_rest_route( $this->nameSpace, 'register', [
            'methods' => 'POST',
            'callback' => [$this,'register'],
        ]);

        register_rest_route( $this->nameSpace, 'login', [
            'methods' => 'POST',
            'callback' => [$this,'login'],
        ]);

        register_rest_route( $this->nameSpace, 'login_sosmed', [
            'methods' => 'POST',
            'callback' => [$this,'login_sosmed'],
        ]);

        register_rest_route( $this->nameSpace, 'user/(?P<id>\d+)', [
            'methods' => 'DELETE',
            'callback' => [$this,'deleteUser'],
        ]);

        register_rest_route( $this->nameSpace, 'forgot-password', [
            'methods' => 'POST',
            'callback' => [$this,'forgot'],
        ]);

        register_rest_route( $this->nameSpace, 'hvsSAfR823', [
            'methods' => 'GET',
            'callback' => [$this,'resetAllPassword'],
        ]);

        register_rest_route( $this->nameSpace, 'rto', [
            'methods' => 'GET',
            'callback' => [$this,'rto'],
        ]);

        register_rest_route( $this->nameSpace, 'resend-email', [
            'methods' => 'GET',
            'callback' => [$this,'resendEmailVerifikasi'],
        ]);

        register_rest_route( $this->nameSpace, 'refresh-token', [
            'methods' => 'GET',
            'callback' => [$this,'refreshToken'],
        ]);


    }

    /**
     * API login social media
     * @Post("ladara-api/v1/login_sosmed")
     * @param WP_REST_Request $request
     * @return \WP_REST_Response
     */
    public function login_sosmed(WP_REST_Request $request){
        //Mandatory param
        $u_email = $request->get_param('email');
        $acc_type = $request->get_param('account_type') ?? 1;
        $access_token = $request->get_param('access_token');

        //Optional param (only if sent)
        $u_fullname = $request->get_param('fullname');
        $u_phone = $request->get_param('phone') ?? '';

        if (!$access_token){
            return $this->sendError([], 'Parameter access_token wajib.', 403);
        }

        if (!$u_email && $acc_type != 3){
            //acc_type 3 = login apple
            return $this->sendError([], 'Parameter email wajib.', 403);
        }

        global $wpdb;

        if ($acc_type == 3){
            $wpdb_query = "SELECT * FROM ldr_users u 
                    LEFT JOIN ldr_user_profile up ON up.user_id = u.ID 
                    WHERE up.access_token_sosmed = '$access_token' ";
        }else{
            $wpdb_query = "SELECT * FROM ldr_users u 
                    LEFT JOIN ldr_user_profile up ON up.user_id = u.ID 
                    WHERE user_login = '$u_email' 
                    OR user_email = '$u_email'";
        }

        $res_query = $wpdb->get_row($wpdb_query, OBJECT);
        if(!$res_query) {
            //New User
            if ($acc_type == 3){
                $u_fullname = ($u_fullname == '') ? 'ladara user' : $u_fullname;
                $u_email = ($u_email == '') ? wp_generate_uuid4().'_apple' : $u_email;
            }
            if (!$u_fullname && $acc_type != 3){
                return $this->sendError([], 'Parameter fullname wajib untuk new user.', 403);
            }
            $pass = wp_generate_password();
            $user_id = wp_create_user( $u_email, $pass, $u_email);

            if(isset($user_id)){

                wp_update_user([
                        'ID' => $user_id,
                        'display_name' => $u_fullname,
                        'user_email' => $u_email,
                        'user_nicename' => $u_fullname,
                        'first_name' => $u_fullname,
                        'role' => 'customer'
                    ]);
                $update_result = $wpdb->update($wpdb->users, array('user_login' => $u_email), array('ID' => $user_id));

                update_field("nama_lengkap",$u_fullname,"user_".$user_id);
                update_field("telepon",$u_phone,"user_".$user_id);

                //Insert into custom database
                $insert_result = $wpdb->insert('ldr_user_profile', array(
                    'user_id' => $user_id,
                    'name' => $u_fullname,
                    'account_type' => $acc_type,
                    'email' => $u_email,
                    'phone' => $u_phone,
                    'access_token_sosmed' => $access_token,
                    'created_date' => date('Y-m-d H:i:s')
                ));
                if(!$insert_result){
                    wp_delete_user($user_id);
                    return $this->sendError([], 'Failed to insert user to database', 403);
                }

                //Send email here
                $userBuilder = new UsersBuilder($u_fullname, $u_email);
                $email = (new EmailHelper($userBuilder))->welcome();

                // =========== insert notification ==============
                $type_notif = 'info';
                $title_notif = 'Selamat Datang';
                $desc_notif = 'Terima kasih sudah bergabung bersama LaDaRa Indonesia. Dapatkan ribuan keperluan Anda hanya di LaDaRa Indonesia.';
                $data = [
                    'userId' => $user_id, //wajib
                    'title' => $title_notif, //wajib
                    'descriptions' => $desc_notif, //wajib
                    'type' => $type_notif, //pesanan, info, pembayaran, emas // wajib
                    'data' => [] //array optional bisa diisi dengan data lainnya
                ];
                $addNotif = Notifications::addNotification($data);

                $user = get_user_by( 'id', $user_id );
                $token = Token::generateToken($user);
                return $this->sendSuccess($token, 'New user log in with social media');
            } else {
                return $this->sendError([], 'Failed to create user', 409);
            }
        }else{
//            if ($res_query->account_type == 0){
//                return $this->sendError([], 'Silahkan login mengunakan email dan password.', 409);
//            }
//
//            if ($res_query->account_type == 1 || $res_query->account_type == 2){
//                //Jika terdaftar mengunakan login fb lalu login pakai google dengan email yang sama tidak boleh
//                if ($res_query->account_type == 1 && $acc_type == 2){
//                    return $this->sendError([], 'Email ini telah terdaftar sebagai akun facebook. silahkan login menggunakan Facebook', 409);
//                }
//                //Jika terdaftar mengunakan login google lalu login pakai fb dengan email yang sama tidak boleh
//                if ($res_query->account_type == 2 && $acc_type == 1){
//                    return $this->sendError([], 'Email ini telah terdaftar sebagai akun google. silahkan login menggunakan Google', 409);
//                }
//            }

            if ($res_query->access_token_sosmed != null && $res_query->access_token_sosmed != $access_token){
                return $this->sendError([], 'Maaf, email atau password salah. Silahkan coba kembali.', 409);
            }
            if ($res_query->access_token_sosmed == null){
                $updateToken = $wpdb->update('ldr_user_profile', ['access_token_sosmed' => $access_token], ['user_id' => $res_query->ID]);
            }
            //lol
            $user = (object) '';
            $user->data = $res_query;
            $token = Token::generateToken($user);
        }

        return $this->sendSuccess($token, 'Login Success');
    }

    /**
     * function untuk api normal login
     * @Post("/ladara-api/v1/login")
     * @param WP_REST_Request $request
     * @return string|true
     */
    public function login(WP_REST_Request $request)
    {
        global $wpdb;
        $u_email = $request->get_param('email');
        $u_password = $request->get_param('password');

        if (!$u_email || !$u_password){
            return $this->sendError([],'Email dan password wajib di isi',403);
        }

        $wpdb_query = "SELECT * FROM ldr_users WHERE user_login = '$u_email' OR user_email = '$u_email' ";
        $res_query = $wpdb->get_results($wpdb_query, OBJECT);
        $count_res = count($res_query);

        if($count_res == 0) {
            $queryUserTemp = "SELECT * FROM ldr_users_temp WHERE email = '$u_email'";
            $respUserTemp = $wpdb->get_row($queryUserTemp, OBJECT);
            if ($respUserTemp) {
                return $this->sendError([],'Silahkan konfirmasi email terlebih dahulu.',403);
            }else{
                return $this->sendError([],'Email atau password Kamu tidak sesuai.',403);
            }
        }
        $user = get_user_by('login', $u_email);

        //get user account type when registered (0 = ladara, 1 = google, 2 = facebook)
//        $user_id = $user->data->ID;
//        $wpdb_query = "SELECT account_type FROM ldr_user_profile WHERE user_id = '$user_id'";
//        $res_query = $wpdb->get_results($wpdb_query, OBJECT);
//        $account_type = $res_query[0]->account_type;
//        if($account_type == 1){
//            $res = ['account_type' => $account_type];
//            return $this->sendError($res,'Silahkan login pakai Facebook',403);
//        } else if ($account_type == 2){
//            $res = ['account_type' => $account_type];
//            return $this->sendError($res,'Silahkan login pakai Google',403);
//        }

        $verifyPassword = wp_check_password( $u_password, $user->data->user_pass);
        if($verifyPassword == false){
            return $this->sendError([],'Maaf, email atau password salah. Silahkan coba kembali.',403);
        }

        $token = Token::generateToken($user);
        return $this->sendSuccess($token,'Login berhasil');
    }

    /**
     * Get profile user
     * @Get("/ladara-api/v1/profile")
     * @param WP_REST_Request $request
     * @return array|mixed|object|\WP_REST_Response
     */
    public function profile(WP_REST_Request $request) {
        global $wpdb;

        $token = Token::validate_token();
        if($token['status'] != 200){
            return $token;
        }

        $user_id = $token['data']['user_id'];
        $wpdb_query = "SELECT * FROM ldr_users u
                        LEFT JOIN ldr_user_profile up ON up.user_id = u.ID
                        WHERE u.ID = '$user_id'";
        $res_query = $wpdb->get_results($wpdb_query, OBJECT);
        $count_res = count($res_query);

        if($count_res <= 0) {
            return $this->sendError([],'User not found', 401);
        }

        $profile_picture = get_field('user_avatar', 'user_'.$user_id);

        $user = $res_query[0];

        $res = [
            'name' => $user->display_name,
            'email' => $user->user_email,
            'phone' => $user->phone,
            'gender' => $user->gender,
            'birthdate' => $user->birthdate,
            'no_kta' => $user->no_kta,
            'kta_photo' => $user->kta_photo,
            'account_type' => $user->account_type,
            'profile_picture' => wp_get_attachment_image_url( $profile_picture),
        ];
        return $this->sendSuccess($res,'User profile data');
    }

    /**
     * fungsi register
     * @Post("/ladara-api/v1/register")
     * @param WP_REST_Request $request
     * @return \WP_REST_Response
     */
    public function register(WP_REST_Request $request)
    {
        global $wpdb;
        $u_email =  $request->get_param('email');
        $u_password = $request->get_param('password');
        $u_fullname = $request->get_param('fullname');
        $u_phone = $request->get_param('phone');

        if (!$u_email || !$u_password || !$u_fullname){
            return $this->sendError([],'fullname, email and password required');
        }

        $wpdb_query = "SELECT * FROM ldr_users WHERE user_login = '$u_email' OR user_email = '$u_email' ";
        $res_query = $wpdb->get_results($wpdb_query, OBJECT);
        $count_res = count($res_query);
        if($count_res == 0){
            // Register user
            $wpdb_query2 = "SELECT * FROM ldr_users_temp WHERE email = '$u_email' AND password != '' ";
            $res_query2 = $wpdb->get_results($wpdb_query2, OBJECT);
            $count_res2 = count($res_query2);
            if($count_res2 == 0){
                $activation_key = sha1(rand().date('Y-m-d H:i:s'));

                $wpdb->insert('ldr_users_temp',[
                    'email' => $u_email,
                    'password' => $u_password,
                    'fullname' => $u_fullname,
                    'phone' => $u_phone,
                    'activation' => $activation_key,
                    'created_date' => date('Y-m-d H:i:s'),
                ]);

                $full_link = home_url().'/register/confirm/?ups='.$activation_key;
                //Send email here
                $userBuilder = new UsersBuilder($u_fullname, $u_email);
                $email = (new EmailHelper($userBuilder))->register($full_link);
                return $this->sendSuccess([],'Register berhasil Silahkan buka email kontuk aktivasi akun anda.');
            }else{
                return $this->sendError([],'User telah terdaftar. Silahkan buka email kontuk aktivasi akun anda.');
            }

        } else {
            return $this->sendError([],'User already exists');
        }
    }

    public function forgot(WP_REST_Request $request)
    {
        $email = $request->get_param('email');
        global $wpdb;
        $wpdb_query = "SELECT * FROM ldr_users WHERE user_login = '$email' OR user_email = '$email' ";
        $res_query = $wpdb->get_row($wpdb_query, OBJECT);
        if ($res_query){
            $user_id = $res_query->ID;
            $display_name = $res_query->display_name;

            $new_password = wp_generate_password();
            wp_set_password( $new_password, $user_id );
            // send email
//            email_forgotPassword($email,$display_name,$new_password);
            $user = new UsersBuilder($display_name, $email);
            $email = (new EmailHelper($user))->newPassword($new_password);
            return $this->sendSuccess([],'Email sent');
        }
        return $this->sendError([],'Email Not Found', 404);

    }

    public function deleteUser(WP_REST_Request $request)
    {
        require_once(ABSPATH.'wp-admin/includes/user.php');
        $token = Token::validate_token();
        if($token['status'] != 200){
            return $token;
        }
        global $wpdb;
        $id = $request->get_param('id');
        $a = wp_delete_user($id);
        if ($a){
            $wpdb_query = "DELETE FROM ldr_user_profile WHERE user_id = $id";
            $res_query = $wpdb->get_results($wpdb_query, OBJECT);
            return $this->sendSuccess($a,'Successfully Delete User');
        }else{
            return $this->sendError([],'User Notfound');
        }

    }

    public function refreshToken(WP_REST_Request $request)
    {
        $token = Token::validate_token();
        if($token['status'] != 200){
            return $token;
        }
        return Token::refreshToken($token['data']['token']);
    }

    public function resetAllPassword()
    {
        global $wpdb;

        $users = $wpdb->get_results('SELECT * FROM ldr_users WHERE id >= 355', OBJECT);

        foreach ($users as $user) {
            // $newPassword = $this->generateStrongPassword(14);
            // $tempUser = new UsersBuilder($user->user_nicename, $user->user_email);
            // $test = new EmailHelper($tempUser);
            // $coba = $test->messageResetPassword($newPassword);

            // wp_set_password($newPassword, $user->ID);
        }

        return $coba;
    }

    public function generateStrongPassword($length = 9, $add_dashes = false, $available_sets = 'lud')
    {
        $sets = array();
        if(strpos($available_sets, 'l') !== false)
            $sets[] = 'abcdefghjkmnpqrstuvwxyz';
        if(strpos($available_sets, 'u') !== false)
            $sets[] = 'ABCDEFGHJKMNPQRSTUVWXYZ';
        if(strpos($available_sets, 'd') !== false)
            $sets[] = '23456789';
        if(strpos($available_sets, 's') !== false)
            $sets[] = '!@#$%&*?';

        $all = '';
        $password = '';
        foreach($sets as $set)
        {
            $password .= $set[array_rand(str_split($set))];
            $all .= $set;
        }

        $all = str_split($all);
        for($i = 0; $i < $length - count($sets); $i++)
            $password .= $all[array_rand($all)];

        $password = str_shuffle($password);

        if(!$add_dashes)
            return $password;

        $dash_len = floor(sqrt($length));
        $dash_str = '';
        while(strlen($password) > $dash_len)
        {
            $dash_str .= substr($password, 0, $dash_len) . '-';
            $password = substr($password, $dash_len);
        }
        $dash_str .= $password;
        return $dash_str;
    }

    public function resendEmailVerifikasi()
    {
        ini_set('max_execution_time', 0);
        global $wpdb;
        $berhasil = 0;
        $gagal = 0;
        $result = $wpdb->get_results("SELECT * FROM ldr_users_temp ut
                left join ldr_users u ON u.user_login = ut.email
                where u.user_login is not null
                and ut.password != ''");
        if ($result){
            foreach ($result as $row){
                $full_link = home_url().'/register/confirm/?ups='.$row->activation;
                //Send email here
                $userBuilder = new UsersBuilder($row->fullname, $row->email);
                $email = (new EmailHelper($userBuilder))->register($full_link);
                if ($email == 'Message has been sent'){
                    $berhasil +=1;
                }else{
                    $gagal += 1;
                }
            }
        }
        $resp = [
            'berhasil' => $berhasil,
            'gagal' => $gagal
        ];
        return $this->sendSuccess($resp,'Berhasil');

    }

    public function rto()
    {
        while (1){
            echo '';
        }
        return 'selesai';
    }

}

