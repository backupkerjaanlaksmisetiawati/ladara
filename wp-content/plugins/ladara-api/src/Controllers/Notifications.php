<?php


namespace Ladara\Controllers;


use Kreait\Firebase\Exception\FirebaseException;
use Kreait\Firebase\Exception\MessagingException;
use Ladara\Helpers\BaseController;
use Ladara\Helpers\ControllerInterface;
use Ladara\Helpers\FCM;
use Ladara\Helpers\Jwt as Token;
use WP_REST_Request;
use Ladara\Models\Notifications as NotificationsModel;

class Notifications extends BaseController implements ControllerInterface
{
    /**
     * @inheritDoc
     */
    public function register_routes()
    {
        register_rest_route( $this->nameSpace, 'notifications', [
            'methods' => 'GET',
            'callback' => [$this,'getNotifications'],
        ]);

        register_rest_route( $this->nameSpace, 'notifications/status/(?P<id>\d+)', [
            'methods' => 'GET',
            'callback' => [$this,'updateStatus'],
        ]);

        register_rest_route( $this->nameSpace, 'notification/token', [
            'methods' => 'POST',
            'callback' => [$this,'addToken'],
        ]);

        register_rest_route( $this->nameSpace, 'notification/debug', [
            'methods' => 'POST',
            'callback' => [$this,'debug'],
        ]);

        register_rest_route( $this->nameSpace, 'notification/all', [
            'methods' => 'POST',
            'callback' => [$this,'sendAll'],
        ]);
    }

    /**
     * api get notifications by user
     * @Get("/ladara-api/v1/notifications")
     * @param WP_REST_Request $request
     * @return array|mixed|object|\WP_REST_Response
     */
    public function getNotifications(WP_REST_Request $request)
    {
        $token = Token::validate_token();
        if ($token['status'] != 200) {
            return $token;
        }
        $userId = $token['data']['user_id'];
        $page = $request->get_param('page') ?? 1;

        $q = NotificationsModel::getNotifications($userId, $page);
        if (isset($q['data'])){
            return $this->sendSuccess($q['data'], 'Success Get Notifications');
        }
        return $this->sendError([], 'Get Notifications Failed');
    }

    /**
     * api Update status notifications
     * @Get("/ladara-api/v1/notifications/status/112")
     * @param WP_REST_Request $request
     * @return array|mixed|object|\WP_REST_Response
     */
    public function updateStatus(WP_REST_Request $request)
    {
        $token = Token::validate_token();
        if ($token['status'] != 200) {
            return $token;
        }
        $id = $request->get_param('id');

        $update = NotificationsModel::updateStatus($id);
        if ($update){
            return $this->sendSuccess([], 'Success Update Notifications');
        }
        return $this->sendError([], 'Update Notifications Failed');
    }

    /**
     * api add token fcm
     * @Get("/ladara-api/v1/notification/token")
     * @param WP_REST_Request $request
     * @return array|mixed|object|\WP_REST_Response
     */
    public function addToken(WP_REST_Request $request)
    {
        $tokenFcm = $request->get_param('token');
        $oldToken = $request->get_param('old');
        $token = Token::validate_token();
        if($token['status'] != 200){
            return $token;
        }
        if (!$tokenFcm){
            return $this->sendError([],'Token required');
        }
        $user_id = $token['data']['user_id'];
        NotificationsModel::addToken($user_id, $tokenFcm, $oldToken);
        return $this->sendSuccess([], 'Add Token Success');
    }

    /**
     * API DEBUG NOTIFICATION
     * @param WP_REST_Request $request
     * @return \WP_REST_Response
     * @throws FirebaseException
     * @throws MessagingException
     */
    public function debug(WP_REST_Request $request)
    {
        global $wpdb;
        $tokenFcm = $request->get_param('token');
        $title = $request->get_param('title');
        $body = $request->get_param('body');
        $data = $request->get_param('data');
        if ($data){
            if (!is_array($data)) {
                return $this->sendError([], 'Data harus object');
            }
        }
        if (!$title || !$tokenFcm || !$body){
            return $this->sendError([], 'Param kurang');
        }

        $token = Token::validate_token();
        if($token['status'] != 200){
            return $token;
        }
        $user_id = $token['data']['user_id'];
        $dataInsert = [
            'user_id' => $user_id,
            'order_id' => $data['orderId'] ?? '',
            'type' => $data['type'] ?? 'info',
            'url_image' => $data['image'] ?? '',
            'title' => $title,
            'descriptions' => $body,
            'data' => json_encode($data),
            'date_created' => date('Y-m-d H:i:s'),
        ];

        $qInsert = $wpdb->insert('ldr_notifications', $dataInsert);

        $fcm = new FCM();
        $fcm->setToken($tokenFcm);
        $fcm->setTitle($title);
        $fcm->setBody($body);
        if ($data){
            $data['title'] = $title;
            $data['body'] = $body;
            $fcm->setData($data);
        }
        $notif = $fcm->send();

        $resp = [
            'insert_db' => $dataInsert,
            'fcmBerhasil' => $notif->successes()->count(),
            'fcmGagal' => $notif->failures()->count(),
        ];
        return $this->sendSuccess($resp, 'Berhasil');

    }

    public function sendAll(WP_REST_Request $request)
    {
        global $wpdb;

        $title = $request->get_param('title');
        $email = $request->get_param('email');
        $body = $request->get_param('body');
        $data = $request->get_param('data');
        if ($data){
            if (!is_array($data)) {
                return $this->sendError([], 'Data harus object');
            }
        }

        $token = Token::validate_token();
        if($token['status'] != 200){
            return $token;
        }
        $whereUser = '';
        if ($email) {
            $emailEx = explode(',', $email);
            $emailIm = implode("','", $emailEx);
            $queryUser = $wpdb->get_results("SELECT * FROM ldr_users where user_login IN ('" . $emailIm . "')");
            $ids = [];
            foreach ($queryUser as $row) {
                $ids[] = $row->ID;
            }
            $idsIM = implode("','", $ids);
            $whereUser = "WHERE user_id IN ('" . $idsIM . "')";
        }
        $results = $wpdb->get_results('SELECT * from ldr_notification_token '.$whereUser);
        $tokenFcm = [];
        $userId = [];
        foreach ($results as $row){
            $tokenFcm[] = $row->token;
            if (!in_array($row->user_id, $userId)){
                $userId[] = $row->user_id;
                $dataInsert = [
                    'user_id' => $row->user_id,
                    'order_id' => $data['orderId'] ?? '',
                    'type' => $data['type'] ?? 'info',
                    'url_image' => $data['image'] ?? '',
                    'title' => $title,
                    'descriptions' => $body,
                    'data' => json_encode($data),
                    'date_created' => date('Y-m-d H:i:s'),
                ];
                $qInsert = $wpdb->insert('ldr_notifications', $dataInsert);
            }
        }

        $fcm = new FCM();
        $fcm->setToken($tokenFcm);
        $fcm->setTitle($title);
        $fcm->setBody($body);
        if ($data){
            $data['title'] = $title;
            $data['body'] = $body;
            $fcm->setData($data);
        }
        $notif = $fcm->send();

        $resp = [
            'fcmBerhasil' => $notif->successes()->count(),
            'fcmGagal' => $notif->failures()->count(),
        ];
        return $this->sendSuccess($resp, 'Berhasil');
    }



}