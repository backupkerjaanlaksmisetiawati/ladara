<?php


namespace Ladara\Controllers;

use Ladara\Helpers\BaseController;
use Ladara\Helpers\ControllerInterface;
use Ladara\Helpers\Jwt;
use Ladara\Models\CartModel;
use Ladara\Models\Products as ProductModel;
use WP_REST_Request;

if (! defined('ABSPATH')) {
    exit;
}

class Cart extends BaseController implements ControllerInterface
{
    /**
     * @inheritDoc
     */
    public function register_routes()
    {
        //Detail product
        register_rest_route($this->nameSpace, 'cart', [
            'methods' => 'GET',
            'callback' => [$this,'getCart'],
        ]);

        register_rest_route($this->nameSpace, 'cart', [
            'methods' => 'POST',
            'callback' => [$this,'addCart'],
        ]);

        register_rest_route($this->nameSpace, 'cart/(?P<id>\d+)', [
            'methods' => 'DELETE',
            'callback' => [$this,'deleteCart'],
        ]);

        register_rest_route($this->nameSpace, 'cart/(?P<id>\d+)', [
            'methods' => 'PUT',
            'callback' => [$this,'updateCart'],
        ]);
    }

    public function deleteCart(WP_REST_Request $request)
    {
        $id = $request->get_param('id');

        global $wpdb;

        $wpdb_query = "SELECT * FROM ldr_cart WHERE id = '$id'";
        $res_query = $wpdb->get_results($wpdb_query, OBJECT);
        $count_res = count($res_query);
        
        if ($count_res <= 0) {
            return $this->sendError([], 'Cart ID is not found', 401);
        }

        // $delete_query = "DELETE FROM ldr_user_address WHERE id=$addr_id";
        $res_delete = $wpdb->delete('ldr_cart', array('id' => $id));
        if (!$res_delete) {
            return $this->sendError([], 'Failed to delete product from cart', 403);
        }

        return $this->sendSuccess([], 'Product deleted from cart');
    }

    public function updateCart(WP_REST_Request $request)
    {
        $id = $request->get_param('id');
        $qty = $request->get_param('qty');

        global $wpdb;
        $queryCart = "select * from ldr_cart c 
                        left join ldr_stock ls on c.stock_id = ls.id
                        where c.id = '$id'";
        $resCart = $wpdb->get_row($queryCart, OBJECT);
        if ($qty > $resCart->product_quantity){
            return $this->sendError([], "Stok tersedia hanya ".$resCart->product_quantity." pcs", 401);
        }

        $wpdb->update('ldr_cart',['qty'=> $qty], ['id' => $id]);

        if (!$wpdb) {
            return $this->sendError([], 'Gagal update quantity.');
        }

        return $this->sendSuccess([], 'Berhasil update quantity');
    }

    public function getCart(WP_REST_Request $request)
    {
        global $wpdb;

        $token = Jwt::validate_token();

        if ($token['status'] != 200) {
            return $token;
        }

        $userId = $token['data']['user_id'];
        $carts = CartModel::getCart($userId);

        $resp = [];
        foreach ($carts as $cart) {
            $product_id = $cart->product_id;
            $stock_id = $cart->stock_id;
            $seller_id = $cart->seller_id;
            
            $product = wc_get_product($product_id);

            $wpdb_query = "SELECT * FROM ldr_stock WHERE id = '$stock_id' ";
            $res_query = $wpdb->get_row($wpdb_query, OBJECT);
            $stock = $res_query;

            $brandData = get_field('product_brand',$product->get_id());
            $sellerData = get_field('seller_name',$product->get_id());

            $cart->product_name = $product->name;
            $cart->ukuran = $stock->ukuran_value;
            $cart->nomor = $stock->nomor_value;
            $cart->warna = $stock->warna_value;
            $cart->price = $product->get_price();
            $cart->regular_price = $product->get_regular_price();
            $cart->sale_price = $product->get_sale_price();
            $cart->discount = $this->calculateDiscount($cart->regular_price, $cart->sale_price);
            $cart->image = $this->getProductImage($product);
            $cart->wishlist = ProductModel::checkWishList($userId,$product->get_id());
            $cart->max_qty = intval($stock->product_quantity);
            $cart->weight = (int)$product->get_weight();
            $cart->brand = [
                'id' => $brandData->ID,
                'name' => $brandData->post_title
            ];
            $cart->seller = [
                'id' => $sellerData->ID,
                'name' => $sellerData->post_title
            ];;

            array_push($resp, $cart);
        }

        return $this->sendSuccess($resp, 'Success get cart');
    }

    public function addCart(WP_REST_Request $request)
    {
        $product_id = $request->get_param('product_id');
        $seller_id = $request->get_param('seller_id');
        $ukuran = $request->get_param('ukuran');
        $nomor = $request->get_param('nomor');
        $warna = $request->get_param('warna') ?? '';
        $quantity = $request->get_param('quantity');

        global $wpdb;
    
        $token = Jwt::validate_token();
        if ($token['status'] != 200) {
            return $token;
        }
    
        $user_id = $token['data']['user_id'];

        $wpdb_query = "SELECT * FROM ldr_stock WHERE product_id = '$product_id' AND ukuran_value = '$ukuran' AND nomor_value = '$nomor' AND warna_value = '$warna' ";
        $res_query = $wpdb->get_results($wpdb_query, OBJECT);
        $count_res = count($res_query);

        if ($count_res > 0) {
            $stock = $res_query[0];
            $stock_id = $stock->id;
            $product_quantity = $stock->product_quantity;

            $resp = array(
                'product_quantity' => $product_quantity
            );

            if ($quantity > $product_quantity) {
                return $this->sendError($resp, "Stok tersedia hanya ".$product_quantity." pcs", 401);
            }

            $wpdb_query = "SELECT * FROM ldr_cart WHERE user_id = '$user_id'";
            $queryCart = $wpdb->get_results($wpdb_query, OBJECT);
            $insertNew = true;
            foreach ($queryCart as $cart){
                if ($cart->seller_id != $seller_id){
                    return $this->sendError([], 'Maaf, Anda hanya dapat melakukan pembayaran untuk produk-produk dalam 1 toko penjual saja.', 401);
                }
                if ($cart->stock_id == $stock_id){
                    //update qty;
                    $total_qty = (int)$cart->qty + (int)$quantity;

                    if ($total_qty > $product_quantity) {
                        $resp = [
                            'total_in_cart' => $total_qty,
                            'product_quantity' => $product_quantity
                        ];
                        return $this->sendError($resp, "Stok tersedia hanya ".$product_quantity." pcs. Sudah ada ".$total_qty.' di keranjang anda.', 401);
                    }

                    $wpdb_query_update = " UPDATE ldr_cart
                                    SET qty= $total_qty
                                    WHERE id = '$cart->id' AND user_id = '$user_id' AND stock_id = '$cart->stock_id' 
                                    ";
                    $wpdb->query($wpdb_query_update);
                    $insertNew = false;
                }
            }

            if ($insertNew){
                //insert new cart
                $wpdb->insert(
                    'ldr_cart', [
                        'user_id' => $user_id,
                        'product_id' => $product_id,
                        'stock_id' => $stock_id,
                        'qty' => $quantity,
                        'seller_id' => $seller_id,
                        'created_date' => date('Y-m-d H:i:s')
                    ]
                );
            }
            return $this->sendSuccess([], 'Produk berhasil ditambahkan di cart');
        }

        return $this->sendError([], 'Variasi ini tidak ada', 401);
    }

    private function getProductImage($product)
    {
        $resp =[];
        $attachment_ids = $product->get_gallery_image_ids();
        foreach( $attachment_ids as $attachment_id ) {
            $resp[] =[
                'full' =>  wp_get_attachment_image_url( $attachment_id, 'full' ),
                'medium' =>  wp_get_attachment_image_url( $attachment_id, 'medium' ),
                'thumbnail' =>  wp_get_attachment_image_url( $attachment_id),
            ];
        }
        return $resp;
    }

    private function calculateDiscount($regularPrice, $salePrice=0)
    {
        // Rumus : Persentase (%) = (awal-akhir) / awal x 100%
        if ($salePrice != 0){
            $dis = ($regularPrice - $salePrice) / $regularPrice * 100;
            return [
                'text' => round($dis).'%',
                'int' => round($dis)
            ];
        }
        return [
            'text' => '0%',
            'int' => 0
        ];
    }
}
