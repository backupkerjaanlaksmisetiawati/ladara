<?php
namespace Ladara\Controllers;

if ( ! defined( 'ABSPATH' ) ) {
    exit;
}

use Ladara\Helpers\BaseController;
use Ladara\Helpers\ControllerInterface;
use Ladara\Helpers\Jwt as Token;
use WP_REST_Request;

/**
 * Class Profile
 * @package Ladara\Controllers
 * @author hafiz
 */
class Profile extends BaseController implements ControllerInterface
{

    /**
     * @inheritDoc
     */
    public function register_routes()
    {
        register_rest_route( $this->nameSpace, 'profile', [
            'methods' => 'PUT',
            'callback' => [$this,'edit_profile'],
        ]);

        register_rest_route( $this->nameSpace, 'profile/change_password', [
            'methods' => 'POST',
            'callback' => [$this,'change_password'],
        ]);

        register_rest_route( $this->nameSpace, 'profile/upload_photo', [
            'methods' => 'POST',
            'callback' => [$this,'upload_photo'],
        ]);

        register_rest_route( $this->nameSpace, 'profile/get', [
            'methods' => 'GET',
            'callback' => [$this,'getProfileDebug'],
        ]);

        register_rest_route( $this->nameSpace, 'profile/remove', [
            'methods' => 'GET',
            'callback' => [$this,'removeProfileDebug'],
        ]);

    }

    /**
     * api change password
     * @Post("/ladara-api/v1/profile/change_password")
     * @param WP_REST_Request $request
     * @return array|mixed|object|\WP_REST_Response
     */
    public function change_password(WP_REST_Request $request)
    {
        $u_oldPassword = $request->get_param('oldPassword');
        $u_newPassword = $request->get_param('newPassword');
        
        global $wpdb;
    
        $token = Token::validate_token();
        if($token['status'] != 200){
            return $token;
        }
    
        $user_id = $token['data']['user_id'];
        $user = get_user_by('id', $user_id);
        $verifyPassword = wp_check_password( $u_oldPassword, $user->data->user_pass);
        if($verifyPassword == false){
            return $this->sendError([],'Password lama tidak sesuai', 403);
        }
        wp_update_user( array('ID' => $user_id, 'user_pass' => $u_newPassword) );
        return $this->sendSuccess([], 'Password berhasil diganti');
    }

    /**
     * api edit profile
     * @Put("/ladara-api/v1/profile")
     * @param WP_REST_Request $request
     * @return array|mixed|object|\WP_REST_Response
     */
    public function edit_profile(WP_REST_Request $request)
    {
        $email = $request->get_param('email');
        $u_fullname = $request->get_param('fullname');
        $u_phone = $request->get_param('phone');
        $u_birthdate = $request->get_param('birthdate');
        $u_gender = $request->get_param('gender');
        $u_no_kta = $request->get_param('no_kta');
        $u_kta_photo = $request->get_param('kta_photo');
        
        global $wpdb;

        $token = Token::validate_token();
        if($token['status'] != 200){
            return $token;
        }
        $user_id = $token['data']['user_id'];

        if($email != ''){
            $wpdb_query = "
            SELECT * FROM ldr_users
            WHERE ID != '$user_id' AND
                (user_login = '$email'
               OR user_email = '$email')
               ";
            $cekEmail = $wpdb->get_results($wpdb_query, OBJECT);
            if (count($cekEmail) > 0){
                return $this->sendError([],'Email sudah dipakai di akun lain');
            }
        }

        $user_id = $token['data']['user_id'];
        if($u_kta_photo !== ''){
            $dataDecoded = $this->imageDecoder($u_kta_photo);
            if($dataDecoded[status] === 401){
                return $this->sendError([], $dataDecoded[message], $dataDecoded[status]);
            }
            $type = $dataDecoded[type];
            $data = $dataDecoded[data];

            $filename = "kta_photo_{$user_id}.{$type}";

            $dirUpload = wp_upload_dir()['basedir'] . "/kta_images/";
            $urlUpload = wp_upload_dir()['baseurl'] . "/kta_images/";

            if (! file_exists($dirUpload)) {
                mkdir($dirUpload, 0755, true);
            }
            $image_upload = file_put_contents( $dirUpload . $filename, $data );
            $u_kta_photo = $urlUpload . $filename;
        }

        $dataUpdate = [
            'name' => $u_fullname,
            'phone'=>$u_phone,
            'birthdate'=>$u_birthdate,
            'gender'=>$u_gender,
            'no_kta' => $u_no_kta
        ];
        if($email != ''){
            $dataUpdate['email'] = $email;
        }

        if($u_kta_photo !== ''){
            $dataUpdate['kta_photo'] = $u_kta_photo;
        }
        $update_result = $wpdb->update('ldr_user_profile', $dataUpdate, ['user_id' => $user_id]);

        $updateUser = [
            'display_name' => $u_fullname,
        ];
        if($email != ''){
            $updateUser['user_email'] = $email;
            $updateUser['user_login'] = $email;
        }
        $update_resultUser = $wpdb->update('ldr_users', $updateUser, ['ID' => $user_id]);

        update_field("nama_lengkap",$u_fullname,"user_".$user_id);
        update_field("first_name",$u_fullname,"user_".$user_id);
        update_field("tanggal_lahir",$u_birthdate,"user_".$user_id);
        update_field("telepon",$u_phone,"user_".$user_id);
        update_field("jenis_kelamin",$u_gender,"user_".$user_id);

        if($update_result === false){
            return $this->sendError([], 'Gagal update foto profile', 403);
        }

        $wpdb_query = "SELECT * FROM ldr_user_profile WHERE user_id = $user_id";
        $res_query = $wpdb->get_results($wpdb_query, OBJECT);
        $user = $res_query[0];
        $res = [
                'email' => $user->email,
                'fullname' => $user->name,
                'phone' => $user->phone,
                'gender' => $user->gender,
                'birthdate' => $user->birthdate,
                'no_kta' => $user->no_kta,
                'kta_photo' => $user->kta_photo,
        ];
        return $this->sendSuccess($res, 'Profile berhasil di update');
    }

    /**
     * fungsi Image decoder dari base64
     * @param $data
     * @return array
     */
    function imageDecoder($data){
        if (preg_match('/^data:image\/(\w+);base64,/', $data, $type)) {
            $data = substr($data, strpos($data, ',') + 1);
            $type = strtolower($type[1]); // jpg, png, gif
        
            if (!in_array($type, [ 'jpg', 'jpeg', 'png' ])) {
                return array(
                    'status' => 401,
                    'message' => 'invalid image type',
                    'data' => new \stdClass()
                );
            }
        
            $data = base64_decode($data);
        
            if ($data === false) {
                return array(
                    'status' => 401,
                    'message' => 'base64_decode failed',
                    'data' => new \stdClass()
                );
            }
        } else {
            return array(
                'status' => 401,
                'message' => 'did not match data URI with image data',
                'data' => new \stdClass()
            );
        }
        
        return array(
            'data' => $data,
            'type' => $type,
        );
    }

    public function upload_photo(WP_REST_Request $request){
        $token = Token::validate_token();
        if($token['status'] != 200){
            return $token;
        }

        $user_id = $token['data']['user_id'];

        $files = $_FILES['profile_picture'];

        if (!in_array($files['type'], [ 'image/jpg', 'image/jpeg', 'image/png' ])) {
            return $this->sendError([], 'File must be in jpg, jpeg, or png', 401);
        }

        if($files['size'] > 1*MB_IN_BYTES){
            return $this->sendError([], 'File size must be less than 1MB', 401);
        }

        // These files need to be included as dependencies when on the front end.
        require_once( ABSPATH . 'wp-admin/includes/image.php' );
        require_once( ABSPATH . 'wp-admin/includes/file.php' );
        require_once( ABSPATH . 'wp-admin/includes/media.php' );

        $nowdate = date('Y-m-d H:i:s');
        $strnow = strtotime($nowdate);

        $newfilename = $user_id.'-'.$strnow.'.jpg';
        $uploadedfile = [
            'name' => esc_attr($newfilename),
            'type' => $files['type'],
            'tmp_name' => esc_attr($files['tmp_name']),
            'error' => $files['error'],
            'size' => $files['size']
        ];
    
        $valid = 0;
        $upload_overrides = array( 'test_form' => false );
        // $time = "profile/";
        $movefile = wp_handle_upload($uploadedfile, $upload_overrides);
        if ( $movefile && ! isset( $movefile['error'] ) ) {
            $src = media_sideload_image( $movefile['url'], null, null, 'src' );
            $image_id = attachment_url_to_postid( $src );
    
            $update = update_field('user_avatar',$image_id, 'user_'.$user_id);
            return $this->sendSuccess([],'success upload foto');
        }

        return $this->sendError([], 'file cannot be uploaded', 403);
    
    }

    public function getProfileDebug(WP_REST_Request $request)
    {
        $email = $request->get_param('email');
        $token = Token::validate_token();
        if($token['status'] != 200){
            return $token;
        }
        global $wpdb;
        $queryUser = "SELECT * FROM ldr_users u
                        LEFT JOIN ldr_user_profile up ON up.user_id = u.ID
                        WHERE user_login = '$email'
                        OR user_email = '$email'";
        $respUser = $wpdb->get_row($queryUser, OBJECT);

        return $this->sendSuccess($respUser,'berhasil');
    }

    public function removeProfileDebug(WP_REST_Request $request)
    {
        require_once(ABSPATH.'wp-admin/includes/user.php');
        $token = Token::validate_token();
        if($token['status'] != 200){
            return $token;
        }
        global $wpdb;
        $email = $request->get_param('email');
        $user = get_user_by('email', $email);
        if ($user){
            $id = $user->get('ID');
            $a = wp_delete_user($id);
            $wpdb_query = "DELETE FROM ldr_user_profile WHERE user_id = $id";
            $res_query = $wpdb->get_results($wpdb_query, OBJECT);
            return $this->sendSuccess($a,'Successfully Delete User');
        }else{
            return $this->sendError([],'User Notfound');
        }
    }

}