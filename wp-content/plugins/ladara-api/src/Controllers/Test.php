<?php
namespace Ladara\Controllers;

if (! defined('ABSPATH')) {
    exit;
}
use Ladara\Helpers\BaseController;
use Ladara\Helpers\builder\OrderBuilder;
use Ladara\Helpers\builder\PaymentBuilder;
use Ladara\Helpers\builder\ShippingBuilder;
use Ladara\Helpers\builder\UsersBuilder;
use Ladara\Helpers\ControllerInterface;
use Ladara\Helpers\email\EmailHelper;

class Test extends BaseController implements ControllerInterface
{
    public function register_routes()
    {
        register_rest_route($this->nameSpace, 'test', [
            'methods' => 'GET',
            'callback' => [$this,'test'],
        ]);

        // register_rest_route($this->nameSpace, 'asdfgh', [
        //     'methods' => 'GET',
        //     'callback' => [$this, 'asdfgh']
        // ]);
    }

    public function test()
    {
        $user = new UsersBuilder('Riki', 'jawakeror@gmail.com');
        $link = 'http://google.com';

        $items = [
            [
                'name' => 'name',
                'qty' => 2,
                'price' => 20000
            ],
            [
                'name' => 'product 2',
                'qty' => 1,
                'price' => 30000
            ]
        ];

        $order = (new OrderBuilder())
                ->setDateCreated(date('Y-m-d H:i:s'))
                ->setTotalPrice(20000)
                ->setItems($items)
                ->setStatus('Pembayaran Dibatalkan')
                ->setInvoiceNo('INV/07042020/0001')
                ->setPaymentDate(date('Y-m-d H:i:s'));

        $paymentInstructions = [
            'ATM BCA' =>
                [
                    0 =>
                        array(
                            'step' => 'Masukkan kartu ATM dan PIN BCA Anda',
                        ),
                    1 =>
                        array(
                            'step' => 'Di menu utama, pilih "Transaksi Lainnya". Pilih "Transfer". Pilih "Ke BCA Virtual Account"',
                        ),
                    2 =>
                        array(
                            'step' => 'Masukkan nomor Virtual Account 1076617778603',
                        ),
                    3 =>
                        array(
                            'step' => 'Pastikan data Virtual Account Anda benar, kemudian masukkan angka yang perlu Anda bayarkan, kemudian pilih "Benar"',
                        ),
                    4 =>
                        array(
                            'step' => 'Cek dan perhatikan konfirmasi pembayaran dari layar ATM, jika sudah benar pilih "Ya", atau pilih "Tidak" jika data di layar masih salah',
                        ),
                    5 =>
                        array(
                            'step' => 'Transaksi Anda sudah selesai. Pilih "Tidak" untuk tidak melanjutkan transaksi lain',
                        ),
                ],
            'IBANKING' =>
                array(
                    0 =>
                        array(
                            'step' => 'Login ke KlikBCA Individual',
                        ),
                    1 =>
                        array(
                            'step' => 'Pilih "Transfer", kemudian pilih "Transfer ke BCA Virtual Account"',
                        ),
                    2 =>
                        array(
                            'step' => 'Masukkan nomor Virtual Account 1076617778603',
                        ),
                    3 =>
                        array(
                            'step' => 'Pilih "Lanjutkan" untuk melanjutkan pembayaran',
                        ),
                    4 =>
                        array(
                            'step' => 'Masukkan "RESPON KEYBCA APPLI 1" yang muncul pada Token BCA Anda, lalu klik tombol "Kirim"',
                        ),
                    5 =>
                        array(
                            'step' => 'Transaksi Anda sudah selesai',
                        ),
                ),
            'MBANKING' =>
                array(
                    0 =>
                        array(
                            'step' => 'Buka Aplikasi BCA Mobile',
                        ),
                    1 =>
                        array(
                            'step' => 'Pilih "m-BCA", kemudian pilih "m-Transfer"',
                        ),
                    2 =>
                        array(
                            'step' => 'Pilih "BCA Virtual Account"',
                        ),
                    3 =>
                        array(
                            'step' => 'Masukkan nomor Virtual Account 1076617778603, lalu pilih "OK"',
                        ),
                    4 =>
                        array(
                            'step' => 'Klik tombol "Send" yang berada di sudut kanan atas aplikasi untuk melakukan transfer',
                        ),
                    5 =>
                        array(
                            'step' => 'Klik "OK" untuk melanjutkan pembayaran',
                        ),
                    6 =>
                        array(
                            'step' => 'Masukkan PIN Anda untuk meng-otorisasi transaksi',
                        ),
                ),
        ];

        $payment = (new PaymentBuilder())
            ->setPaymentExpired(date('Y-m-d H:i:s'))
            ->setBank('BCA Virtual account')
            ->setPaymentInstruction($paymentInstructions)
            ->setVa(139848484884);

        $courier = new ShippingBuilder('JNE', 'REG', 11000);

        $a = (new EmailHelper($user))
            ->messageForgotPassword($link);
        echo $a;
        exit();
    }

    public function asdfgh()
    {
        global $wpdb;

        $flag = false;
        $sum = 0;
        $sameArray = [];
        $handle = fopen("/home/one_winged_angel/Downloads/b3bf466a-1db9-44ea-b3cb-7c510a8465b5.csv", "r");

        for ($i = 0; $row = fgetcsv($handle); ++$i) {
            if ($flag === false) {
                $flag = true;
            } else {
                if ($row[0] === '') {
                    break;
                }

                if ($row[2] === 'delivered' && ! in_array($row[8], $sameArray)) {
                    $email = trim($row[8]);
                    $sum++;

                    $user = $wpdb->get_row('SELECT * FROM ldr_users WHERE sent = 0 and user_email = "'.$email.'"', OBJECT);

                    $username = $user->display_name ?? '';
                    $useremail = $email;

                    if ($user !== null) {
                        $sameArray[] = $email;

                        $wpdb->update('ldr_users', [
                            'sent' => 1
                        ], [
                            'user_email' => $email
                        ]);

                        ddlog("Send email to " . $email . ".", "ladara_ggwp");
                    }
                }
            }
        }

        fclose($handle);

        var_dump($sameArray);
    }
}
