<?php
namespace Ladara\Controllers;

if ( ! defined( 'ABSPATH' ) ) {
    exit;
}
use Ladara\Helpers\BaseController;
use Ladara\Helpers\ControllerInterface;
use Ladara\Helpers\Jwt;
use Ladara\Helpers\ProductHelpers;
use WP_REST_Request;
use Ladara\Models\Products as ProductModel;

/**
 * Class Products
 * @package Ladara\Controllers
 * @author rikihandoyo
 */
class Products extends BaseController implements ControllerInterface
{

    /**
     * @inheritDoc
     */
    public function register_routes()
    {
        //search products
        register_rest_route( $this->nameSpace, 'products', [
            'methods' => 'GET',
            'callback' => [$this,'getProducts'],
        ]);

         //Detail product
        register_rest_route( $this->nameSpace, 'product/(?P<id>\d+)', [
            'methods' => 'GET',
            'callback' => [$this,'getProductById'],
        ]);

        register_rest_route( $this->nameSpace, 'product/last_seen', [
            'methods' => 'GET',
            'callback' => [$this,'getLastSeen'],
        ]);

        register_rest_route( $this->nameSpace, 'product/migration', [
            'methods' => 'GET',
            'callback' => [$this,'migration'],
        ]);

        register_rest_route( $this->nameSpace, 'product/migration-tag', [
            'methods' => 'GET',
            'callback' => [$this,'migrationTags'],
        ]);

        register_rest_route( $this->nameSpace, 'product/migration-cate', [
            'methods' => 'GET',
            'callback' => [$this,'migrationCategories'],
        ]);

        register_rest_route( $this->nameSpace, 'product/migration/categories', [
            'methods' => 'GET',
            'callback' => [$this,'categories'],
        ]);
        
    }

    /**
     * api get product
     * @Get("/ladara-api/v1/products")
     * @param WP_REST_Request $request
     * @return \WP_REST_Response
     */
    public function getProducts(WP_REST_Request $request)
    {
        $page = $request->get_param('page') ?? 1;
        $search = $request->get_param('search');
        $category = $request->get_param('category');
        $sortPrice = $request->get_param('price');
        $sortSales = $request->get_param('sales');
        $sortDate = $request->get_param('date');
        $limit = $request->get_param('limit') ?? 10;
        $brand = json_decode($request->get_param('brand_id'));
        $priceRange = json_decode($request->get_param('price_range'));
        $sortName = $request->get_param('name');

        $token = Jwt::validate_token();
        $userId = $token['data']['user_id'] ?? 0;
//        add_filter( 'posts_search', 'add_other_table', 500, 2 );


        $args =[
            'status' => 'publish',
            'page' => $page,
            's' => $search,
            'limit' => $limit
        ];
        if ($priceRange){
            if (!is_array($priceRange)){
                return $this->sendError([], 'param price_range harus berisi array price min dan max');
            }
            $args['price_range'] = $priceRange;
        }
        if ($category){
            $categories = ProductModel::getIdChildCategory($category);
            $args['tax_query'] = [[
                    'taxonomy'      => 'product_cat',
                    'field'         => 'term_id', //This is optional, as it defaults to 'term_id'
                    'terms'         => $categories,
                    'operator'      => 'IN' // Possible values are 'IN', 'NOT IN', 'AND'.
                ]];
        }

        if ($brand){
            if (!is_array($brand)){
                return $this->sendError([], 'param brand_id harus berisi array brand id');
            }
            $args['product_brand'] = $brand;
        }


        if ($sortPrice){
            $args['orderby'] = 'meta_value_num';
            $args['meta_key'] = '_price';
            $args['order'] = $sortPrice;
        }
        if ($sortName){
            $args['orderby'] = 'title';
            $args['order'] = $sortName;
        }

        if ($sortSales){
            $args['orderby'] = 'meta_value_num';
            $args['meta_key'] = 'total_sales';
            $args['order'] = $sortSales;
        }

        if ($sortDate){
            $args['orderby'] = 'date';
            $args['order'] = $sortDate;
        }

        //tambah search tag pada produk
//        if ($search){
//            $args['tax_query'] = [
//                'relation' => 'OR',
//                [
//                    'taxonomy'      => 'product_tag',
//                    'field'         => 'name', //This is optional, as it defaults to 'term_id'
//                    'terms'         => $search,
//                    'operator'      => 'IN' // Possible values are 'IN', 'NOT IN', 'AND'.
//                ]
//            ];
//        }
//        global $wpdb;
        $products = wc_get_products($args);
//        return $args;

        //Override limit untuk menghitung jumlah page
//        $args['limit'] = -1;
//        $otalProducts = wc_get_products($args);
        $resp['products'] = (new ProductHelpers())->respProducts($userId, $products);

//        $resp['page']['totalPage'] = ceil(count($otalProducts) / $limit);
        $resp['page']['currentPage'] = (int)$page;
        return $this->sendSuccess($resp,'Success Get Products');
    }

    /**
     * fungsi get product by id
     * @Get("/ladara-api/v1/product/{product_id}")
     * @param WP_REST_Request $request
     * @return \WP_REST_Response
     */
    public function getProductById(WP_REST_Request $request)
    {
        //Kurang variant product dan tambah terakhir dilihat
        $id = $request->get_param('id');

        $token = Jwt::validate_token();
        $userId = $token['data']['user_id'] ?? 0;

        $product =  wc_get_product($id);
        if (!$product){
            return $this->sendError([],'Product Not Found');
        }
        $resp = (new ProductHelpers())->respProduct($userId, $product);

        //add to last seen
        ProductModel::addToLastSeen($userId,$id);

        return $this->sendSuccess($resp,'Success get product');
    }

    /**
     * fungsi get terakhir dilihat
     * @Get("/ladara-api/v1/product/last_seen")
     * @param WP_REST_Request $request
     * @return \WP_REST_Response
     */
    public function getLastSeen(WP_REST_Request $request)
    {
        $limit = $request->get_param('limit') ?? 10;
        $token = Jwt::validate_token();
        $userId = $token['data']['user_id'] ?? 0;
        $productIds = ProductModel::getLastSeen($userId,$limit);
        if (count($productIds) > 0 ){
            $args = [
                'status' => 'publish',
                'include' => $productIds,
            ];
            $products = wc_get_products($args);
            $resp = (new ProductHelpers())->respProducts($userId, $products);
            return $this->sendSuccess($resp,'Success Get Last seen ');
        }
        return $this->sendError([],'Not found');

    }

     function add_other_table( $join, $wp_query ) {
        global $wpdb;
        $join .= " LEFT JOIN ldr_term_taxonomy ON ldr_term_relationships.term_taxonomy_id = ldr_term_taxonomy.term_taxonomy_id
               LEFT JOIN ldr_terms ON ldr_term_taxonomy.term_id = ldr_terms.term_id";
        return $join;
    }

    public function migration(WP_REST_Request $request)
    {
        ini_set('max_execution_time', 0);
        $token = Jwt::validate_token();
        if($token['status'] != 200){
            return $token;
        }
        $args =[
            'status' => 'publish',
            'limit' => -1
        ];
        $products = wc_get_products($args);
        $userId = $token['data']['user_id'] ?? 0;
        $resp['products'] = (new ProductHelpers())->respProductMigration($userId, $products);
        $resp['count'] = count($resp['products']);
        return $this->sendSuccess($resp['products'],'Success Get Products');
    }

    public function migrationTags(WP_REST_Request $request)
    {
        ini_set('max_execution_time', 0);
        $token = Jwt::validate_token();
        if($token['status'] != 200){
            return $token;
        }
        $args =[
            'status' => 'publish',
            'limit' => -1
        ];
        $products = wc_get_products($args);
        $userId = $token['data']['user_id'] ?? 0;
        $resp['products'] = (new ProductHelpers())->respProductMigrationTags($userId, $products);
        $resp['count'] = count($resp['products']);
        return $this->sendSuccess($resp['products'],'Success Get Products');
    }

    public function categories()
    {
//        global $wpdb;
//        $wpdb->query('TRUNCATE TABLE ldr_categories');
//        $categories = ProductModel::getCategories();
//        $data = [];
//        foreach ($categories as $category){
//            $thumbnail_id = get_term_meta($category->term_id,'thumbnail_id', true );
//            //Default image
//            $category->image = 'wp-content/uploads/woocommerce-placeholder-125x125.png';
//            if ($thumbnail_id){
//                $image =  get_attached_file($thumbnail_id);
//                if ($image){
//                    $exPath = explode('wp-content', $image);
//                    if (isset($exPath[1])){
//                        $category->image =  'wp-content'.$exPath[1];
//                    }
//                }
//            }
//
//            if ($category->name != 'Uncategorized'){
//                $data = [
//                    'id' => $category->term_id,
//                    'name' => $category->name,
//                    'image' => $category->image,
//                    'slug' => $category->slug,
//                    'featured' => 0,
//                    'parent' => $category->parent,
//                    'created_at' => date('Y-m-d H:i:s'),
//                ];
//                $wpdb->insert('ldr_categories', $data);
//            }
//        }
        $data=[];
        return $this->sendSuccess($data, 'Success get categories');
    }

    public function migrationCategories()
    {
        ini_set('max_execution_time', 0);
        $token = Jwt::validate_token();
        if($token['status'] != 200){
            return $token;
        }
        $args =[
            'status' => 'publish',
            'limit' => -1
        ];
        $products = wc_get_products($args);
        $userId = $token['data']['user_id'] ?? 0;
        $resp['products'] = (new ProductHelpers())->respProductMigrationUpdateCategories($products);
        $resp['count'] = count($resp['products']);
        return $this->sendSuccess($resp['products'],'Success Get Products');
    }
}