<?php
namespace Ladara\Controllers;

if ( ! defined( 'ABSPATH' ) ) {
    exit;
}

use Ladara\Helpers\BaseController;
use Ladara\Helpers\ControllerInterface;
use Ladara\Helpers\Jwt as Token;
use WP_REST_Request;

/**
 * Class Profile
 * @package Ladara\Controllers
 * @author hafiz
 */
class Wishlist extends BaseController implements ControllerInterface
{

    /**
     * @inheritDoc
     */
    public function register_routes()
    {
        register_rest_route( $this->nameSpace, 'wishlist', [
            'methods' => 'GET',
            'callback' => [$this,'get_wishlist'],
        ]);

        register_rest_route( $this->nameSpace, 'wishlist', [
            'methods' => 'POST',
            'callback' => [$this,'add_wishlist'],
        ]);

        register_rest_route( $this->nameSpace, 'wishlist/(?P<product_id>\d+)', [
            'methods' => 'DELETE',
            'callback' => [$this,'delete_wishlist'],
        ]);

    }

    /**
     * api get wishlist
     * @Get("/ladara-api/v1/wishlist")
     * @param WP_REST_Request $request
     * @return array|mixed|object|\WP_REST_Response
     */
    public function get_wishlist(WP_REST_Request $request)
    {
        global $wpdb;
    
        $token = Token::validate_token();
        if($token['status'] != 200){
            return $token;
        }
    
        $user_id = $token['data']['user_id'];

        $wpdb_query = "SELECT * FROM ldr_wishlist WHERE user_id = $user_id";
        $res_query = $wpdb->get_results($wpdb_query, OBJECT);

        $products = array();
        foreach ($res_query as $wishlist){
            $product_id = $wishlist->product_id;
            
            $product =  wc_get_product($product_id);
            if (!$product){
                continue;
            }

            $wishlist->product_name = $product->name;
            $wishlist->price = $product->get_price();
            $wishlist->regular_price = $product->get_regular_price();
            $wishlist->sale_price = $product->get_sale_price();
            $wishlist->discount = $this->calculateDiscount($wishlist->regular_price, $wishlist->sale_price);
            $wishlist->image = $this->getProductImage($product);
            array_push($products, $wishlist);
        }

        return $this->sendSuccess($products, 'Wishlist products');
    }

    /**
     * api add wishlist
     * @Post("/ladara-api/v1/wishlist")
     * @param WP_REST_Request $request
     * @return array|mixed|object|\WP_REST_Response
     */
    public function add_wishlist(WP_REST_Request $request)
    {
        $product_id = $request->get_param('product_id');

        global $wpdb;
    
        $token = Token::validate_token();
        if($token['status'] != 200){
            return $token;
        }
    
        $user_id = $token['data']['user_id'];

        $wpdb_query = "SELECT * FROM ldr_wishlist WHERE user_id = $user_id AND product_id = $product_id";
        $res_query = $wpdb->get_results($wpdb_query, OBJECT);
        $count_res = count($res_query);

        if($count_res > 0){
            return $this->sendError([],'Product is already in wishlist', 401);
        }

        $insert_result = $wpdb->insert('ldr_wishlist', array('user_id' => $user_id, 'product_id' => $product_id, 'created_date' => date('Y-m-d H:i:s')));

        if($insert_result === false){
            return $this->sendError([],'Insert failed', 403);
        }
        return $this->sendSuccess([], 'Product inserted to wishlist');
    }

    /**
     * api delete wishlist
     * @Post("/ladara-api/v1/wishlist")
     * @param WP_REST_Request $request
     * @return array|mixed|object|\WP_REST_Response
     */
    public function delete_wishlist(WP_REST_Request $request)
    {
        global $wpdb;
        $product_id = $request->get_param('product_id');

        $token = Token::validate_token();
        if($token['status'] != 200){
            return $token;
        }

        $user_id = $token['data']['user_id'];

        $res_delete = $wpdb->delete('ldr_wishlist', [
            'user_id' => $user_id,
            'product_id' => $product_id
        ]);
        if(!$res_delete){
            return $this->sendError([],'Failed to delete product from wishlist', 403);
        }
        return $this->sendSuccess([], 'Product deleted from wishlist');
    }
    
    private function getProductImage($product)
    {
        $resp =[];
        $attachment_ids = $product->get_gallery_image_ids();
        foreach( $attachment_ids as $attachment_id ) {
            $resp[] =[
                'full' =>  wp_get_attachment_image_url( $attachment_id, 'full' ),
                'medium' =>  wp_get_attachment_image_url( $attachment_id, 'medium' ),
                'thumbnail' =>  wp_get_attachment_image_url( $attachment_id),
            ];
        }
        return $resp;
    }
    
    private function calculateDiscount($regularPrice, $salePrice=0)
    {
        // Rumus : Persentase (%) = (awal-akhir) / awal x 100%
        if ($salePrice != 0){
            $dis = ($regularPrice - $salePrice) / $regularPrice * 100;
            return [
                'text' => round($dis).'%',
                'int' => round($dis)
            ];
        }
        return [
            'text' => '0%',
            'int' => 0
        ];
    }
}