<?php
namespace Ladara\Controllers;

if ( ! defined( 'ABSPATH' ) ) {
    exit;
}

use Ladara\Helpers\email\EmailHelper;
use Ladara\Helpers\builder\UsersBuilder;
use Ladara\Helpers\BaseController;
use Ladara\Helpers\ControllerInterface;
use Ladara\Helpers\Jwt as Token;
use Ladara\Models\Notifications;
use Xendit\Invoice;
use Xendit\VirtualAccounts;

use WP_REST_Request;


date_default_timezone_set("Asia/Jakarta");

/**
 * nggak usah panggil function emas untuk semua function yang panggil api treasury, access_token belum disesuaikan
 * curl "manual" ke treasury, nyiksa diri sendiri T_T
 * 
 * Class Products
 * @package Ladara\Controllers
 * @author Amy
 */
class Golds extends BaseController implements ControllerInterface
{

    /**
     * @inheritDoc
     */
    public function register_routes() {
        register_rest_route( $this->nameSpace, 'emas', [
            'methods' => 'GET',
            'callback' => [$this, 'emas_test'],
        ]);

        register_rest_route( $this->nameSpace, 'emas/register', [
            'methods' => 'POST',
            'callback' => [$this, 'emas_register'],
        ]);

        register_rest_route( $this->nameSpace, 'emas/calculate', [
            'methods' => 'POST',
            'callback' => [$this, 'emas_calculate'],
        ]);

        register_rest_route( $this->nameSpace, 'emas/rate', [
            'methods' => 'GET',
            'callback' => [$this, 'emas_rate'],
        ]);

        register_rest_route( $this->nameSpace, 'emas/buy', [
            'methods' => 'POST',
            'callback' => [$this, 'emas_buy'],
        ]);

        //api get status pembelian emas 
        register_rest_route( $this->nameSpace, 'emas/invoice_status', [
            'methods' => 'POST',
            'callback' => [$this, 'emas_invoice_status'],
        ]);

        register_rest_route( $this->nameSpace, 'emas/sell', [
            'methods' => 'POST',
            'callback' => [$this, 'emas_sell'],
        ]);

        register_rest_route( $this->nameSpace, 'emas/profile', [
            'methods' => 'GET',
            'callback' => [$this, 'emas_profile'],
        ]);

        register_rest_route( $this->nameSpace, 'emas/history', [
            'methods' => 'POST',
            'callback' => [$this, 'emas_history'],
        ]);

        register_rest_route( $this->nameSpace, 'emas/detail_history', [
            'methods' => 'POST',
            'callback' => [$this, 'emas_detail_history'],
        ]);

        register_rest_route( $this->nameSpace, 'emas/advantages', [
            'methods' => 'GET',
            'callback' => [$this, 'emas_landing_page'],
        ]);

        register_rest_route( $this->nameSpace, 'emas/banner', [
            'methods' => 'GET',
            'callback' => [$this, 'emas_banner'],
        ]);

        register_rest_route( $this->nameSpace, 'emas/content', [
            'methods' => 'GET',
            'callback' => [$this, 'emas_content'],
        ]);

        register_rest_route( $this->nameSpace, 'emas/bank', [
            'methods' => 'POST',
            'callback' => [$this, 'emas_bank'],
        ]);

        register_rest_route( $this->nameSpace, 'emas/connect_to_treasury', [
            'methods' => 'POST',
            'callback' => [$this, 'emas_connect_to_treasury'],
        ]);

    }

    public function emas_test() {
        $token = Token::validate_token();
        return $this->sendError(
            $token['data'],
            "Hello!",
            $token['status']
        );
    }

    /**
     * aktivasi akun ladara emas
     * @Post("/ladara-api/v1/emas/register")
     * @param WP_REST_Request $request
     * @return \WP_REST_Response
     */
    public function emas_register(WP_REST_Request $request) {
        global $wpdb;

        $token = Token::validate_token();
        if($token['status'] != 200){
            return $token;
        }

        $user_id = $token['data']['user_id'];
        
        ddlog("Start register user id " . $user_id . " to treasury.", "ladara_emas_mobile");

        ddlog("Start get treasury security question for user id " . $user_id . " treasury registration.", "ladara_emas_mobile");

        $security_question = gld_security_question();
        $security_question = json_decode($security_question);
        
        if($security_question->code !== 200) {
            ddlog("End get treasury security question for user id " . $user_id . " treasury registration. Error " . $security_question->code . "(" . $security_question->message . ").", "ladara_emas_mobile");

            return $this->sendError(
                [],
                $security_question->message,
                $security_question->code
            );
        }

        ddlog("End get treasury security question for user id " . $user_id . " treasury registration. Success.", "ladara_emas_mobile");
        
        ddlog("Start user profile with id " . $user_id . " for user treasury registration.", "ladara_emas_mobile");

        // get user profile
        $user_profile = gld_user_profile(array("user_id"=>$user_id));
        $user_profile = json_decode($user_profile);
        
        if($user_profile->code !== 200) {
            ddlog("End user profile with id " . $user_id . " for user treasury registration. Error " . $user_profile->code . "(" . $user_profile->message . ").", "ladara_emas_mobile");

            return $this->sendError(
                [],
                $user_profile->message,
                $user_profile->code
            );
        }

        ddlog("End user profile with id " . $user_id . " for user treasury registration. Success.", "ladara_emas_mobile");

        ddlog("Start check if foto selfie and ktp uploaded or not for user id " . $user_id . " treasury registration.", "ladara_emas_mobile");

        if(!empty($_FILES['foto_selfie']) && !empty($_FILES['scan_ktp'])) {

            // convert image ke base64
            $foto = $_FILES['foto_selfie'];
            $ktp = $_FILES['scan_ktp'];

            ddlog("data foto \n" . json_encode($foto), "ladara_emas_mobile");
            ddlog("data ktp \n" . json_encode($ktp), "ladara_emas_mobile");

            if (!in_array($foto['type'], [ 'image/jpg', 'image/jpeg', 'image/png' ]) || !in_array($ktp['type'], [ 'image/jpg', 'image/jpeg', 'image/png' ])) {
                return $this->sendError(
                    [],
                    "Format file harus jpg, jpeg, or png!",
                    401
                );
            }

            if($foto['size'] > 1*MB_IN_BYTES || $ktp['size'] > 1*MB_IN_BYTES){
                return $this->sendError(
                    [],
                    "Size file tidak boleh lebih dari 1MB!",
                    401
                );
            }
            
            $filename_foto = $foto['tmp_name'];
            $handle_foto = fopen($filename_foto, "r");
            $data_foto = file_get_contents($filename_foto);
            
            $filename_ktp = $ktp['tmp_name'];
            $handle_ktp = fopen($filename_ktp, "r");
            $data_ktp = file_get_contents($filename_ktp);

        } else {
            
            ddlog("End check if foto selfie and ktp uploaded or not for user id " . $user_id . " treasury registration. Files not uploaded.", "ladara_emas_mobile");

            return $this->sendError(
                [],
                "Foto selfie dan scan KTP harus di upload!",
                401
            );
        }
        
        ddlog("End check if foto selfie and ktp uploaded or not for user id " . $user_id . " treasury registration. Files uploaded.", "ladara_emas_mobile");
        
        $email_param = $user_profile->data->email;
        // random email for testing only
        // $email_domain = ltrim(stristr($email_param, "@"), "@");
        // $email_user = stristr($email_param, "@", TRUE);
        // $email_param = $email_user . "+" . date("ymdHis") . "@" . $email_domain;

        $phone = $user_profile->data->phone;
        // random phone for testing only
        // $phone = mt_rand(10000000,99999999);
        // $phone = "0813".$phone;

        $check_email = gld_check_email_format($email_param);
        if($check_email["code"] !== 200) {
            return $this->sendError(
                [],
                $check_email["message"],
                $check_email["code"]
            );
        }

        // param. what else???
        $param = array(
            "user_id"                   => $user_id,
            "name"                      => $user_profile->data->name,
            "email"                     => $email_param,
            "web_password"              => $user_profile->data->pass,
            "ts_password"               => $user_profile->data->pass,
            "gender"                    => $user_profile->data->gender,
            "birthday"                  => $user_profile->data->birthdate,
            "referral_code"             => "",
            "phone"                     => $phone,
            "security_question"         => $security_question->data[0]->id,
            "security_question_answer"  => "Mickey Mouse",
            "account_name"              => $request->get_param("account_name"),
            "account_number"            => $request->get_param("account_number"),
            "bank_code"                 => $request->get_param("bank_code"),
            "branch"                    => "",
            "selfie_scan"               => 'base64_encode($data_foto)',
            "id_card_scan"              => 'base64_encode($data_ktp)'
        );
        if(!empty($request->get_param("branch"))) {
            $param["branch"] = $request->get_param("branch");
        }
        if(date("Y-m-d H:i:s") < "2021-12-31 23:59:59") {
            $param["referral_code"] = "LDRTRS01";
        }

        $param["selfie_scan"] = base64_encode($data_foto);
        $param["id_card_scan"] = base64_encode($data_ktp);
        
        if(strtolower($user_profile->data->name) !== strtolower($param["account_name"])) {
            return $this->sendError(
                [],
                "Nama Pemilik Rekening tidak sesuai dengan nama kamu",
                400
            );
        }

        ddlog("Start login treasury partner to register new user for user " . $param["email"] . ".", "ladara_emas_mobile");

        // login partner
        $body_login = [
            "client_id"     => getenv('TREASURY_CLIENT_ID'),
            "client_secret" => getenv('TREASURY_CLIENT_SECRET'),
            "grant_type"    => getenv('TREASURY_GRANT_CLIENT')
        ];

        $ch = curl_init();
        curl_setopt_array($ch, array(
            CURLOPT_URL => getenv('TREASURY_BASE_URL') . "/login",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "POST",
            CURLOPT_POSTFIELDS => $body_login,
            CURLOPT_HTTPHEADER => array(
                "Accept: application/json",
            ),
        ));

        $login_partner = curl_exec($ch);
        curl_getinfo($ch, CURLINFO_EFFECTIVE_URL);
        curl_getinfo($ch, CURLINFO_HTTP_CODE);
        curl_close($ch);
        $json_login_partner = json_decode($login_partner);

        if(!isset($json_login_partner->meta->code)) {
            ddlog("End login treasury partner to register new user for user " . $param["email"] . ". Failed to connect treasury api.\n" . htmlentities($login_partner), "ladara_emas_mobile");

            return $this->sendError(
                [],
                "Maaf terjadi gangguan login partner. Silahkan coba lagi beberapa saat lagi.",
                500
            );
        }

        if($json_login_partner->meta->code !== 200) {
            ddlog("End login treasury partner to register new user for user " . $param["email"] . ". Error " . $json_login_partner->meta->code . " (" . $json_login_partner->meta->message . ").", "ladara_emas_mobile");

            return $this->sendError(
                [],
                "Maaf gagal login partner. Silahkan coba lagi beberapa saat lagi.",
                500
            );
        }

        ddlog("End login treasury partner to register new user for user " . $param["email"] . ". Success.", "ladara_emas_mobile");
        
        // register body. what else???
        $body = array(
            "name"                      => $param["name"],
            "email"                     => $param["email"],
            "password"                  => $param["ts_password"],
            "password_confirmation"     => $param["ts_password"],
            "birthday"                  => $param["birthday"],
            "referral_code"             => $param["referral_code"],
            "phone"                     => $param["phone"],
            "security_question"         => $param["security_question"],
            "security_question_answer"  => $param["security_question_answer"],
            "selfie_scan"               => '$param["selfie_scan"]',
            "id_card_scan"              => '$param["id_card_scan"]',
            "owner_name"                => $param["account_name"],
            "account_number"            => $param["account_number"],
            "bank_code"                 => $param["bank_code"],
            "branch"                    => $param["branch"]
        );

        if($param["gender"] === "wanita") {
            $body["gender"] = "Female";
        } else {
            $body["gender"] = "Male";
        }

        $body["selfie_scan"] = $param["selfie_scan"];
        $body["id_card_scan"] = $param["id_card_scan"];

        $curl = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_URL => getenv('TREASURY_BASE_URL') . "/register",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "POST",
            CURLOPT_POSTFIELDS => $body,
            CURLOPT_HTTPHEADER => array(
                "Authorization: Bearer " . $json_login_partner->data->access_token
            ),
        ));

        $register = curl_exec($curl);
        curl_close($curl);
        $json_register = json_decode($register);

        if(!isset($json_register->meta->code)) {
            ddlog("End register user " . $param["email"] . " to treasury. Failed to connect treasury api.\n" . htmlentities($register), "ladara_emas_mobile");

            $wpdb->query("INSERT INTO ldr_user_emas_log (
                user_emas_id, 
                log 
                ) VALUES (
                0,  
                '" . $user_profile->data->email . " tried to activate Ladara Emas account. Failed to connect treasury api.' 
                )");

            return $this->sendError(
                [],
                "Maaf terjadi gangguan untuk aktivasi Ladara Emas. Silahkan coba lagi beberapa saat lagi.",
                500
            );
        }

        if($json_register->meta->code !== 200) {
            ddlog("End register treasury untuk user " . $param["email"] . ". Error " . $json_register->meta->code . " (" . $json_register->meta->message . ").", "ladara_emas_mobile");

            $wpdb->query("INSERT INTO ldr_user_emas_log (
                user_emas_id, 
                log 
                ) VALUES (
                0,  
                '" . $user_profile->data->email . " tried to activate Ladara Emas account. Error " . $json_register->meta->code . " (" . $json_register->meta->message . ").' 
                )");
            
            return $this->sendError(
                [],
                "Maaf kamu gagal melakukan aktivasi Ladara Emas, " . $json_register->meta->message . " Silahkan coba lagi beberapa saat lagi.",
                $json_register->meta->code
            );
        }        

        ddlog("End register user " . $param["email"] . " to treasury. Success.", "ladara_emas_mobile");

        ddlog("Start insert db ladara emas user " . $param["email"] . " after register to treasury.", "ladara_emas_mobile");

        $bank_name = "";
        if(!empty($param["bank_code"])) {
            $gld_bank = gld_bank(array("type"=>"trs"));
            $gld_bank = json_decode($gld_bank);
            $bank_name = gld_search_bank_name($param["bank_code"], $gld_bank->data);
        }

        $query_insert_new_user_emas = "INSERT INTO ldr_user_emas (
            user_id, 
            name, 
            email, 
            web_password, 
            ts_password, 
            gender, 
            birthday, 
            referral_code, 
            phone, 
            security_question, 
            security_question_answer, 
            selfie_scan, 
            id_card_scan, 
            account_name, 
            account_number, 
            bank_code,  
            bank_name, 
            branch,
            verified_user, 
            gold_balance, 
            gold_balance_in_currency 
        ) VALUES (
            " . $param["user_id"] . ",  
            '" . $param["name"] . "',  
            '" . $param["email"] . "',  
            '" . $param["web_password"] . "',  
            '" . $param["ts_password"] . "',  
            '" . $param["gender"] . "', 
            '" . $param["birthday"] . "',  
            '" . $param["referral_code"] . "',  
            '" . $param["phone"] . "',  
            '" . $param["security_question"] . "',  
            '" . $param["security_question_answer"] . "',  
            '" . $param["selfie_scan"] . "',  
            '" . $param["id_card_scan"] . "',  
            '" . $param["account_name"] . "',  
            '" . $param["account_number"] . "',  
            '" . $param["bank_code"] . "',   
            '" . $bank_name . "',  
            '" . $param["branch"] . "',
            0,  
            0,  
            0
        )";
        
        $wpdb->query($query_insert_new_user_emas);

        ddlog($query_insert_new_user_emas, "ladara_emas_mobile");

        $new_user_emas = $wpdb->get_row(
            "SELECT id, user_id, name, email
                FROM ldr_user_emas 
                WHERE user_id=" . $param["user_id"] . " 
                AND email='" . $param["email"] . "' 
                ORDER BY id DESC LIMIT 1",
            OBJECT
        );

        $wpdb->query("INSERT INTO ldr_user_emas_log (
            user_emas_id, 
            log 
        ) VALUES (
            " . $new_user_emas->id . ",  
            'Success activate Ladara Emas account.' 
        )");

        ddlog("End insert db ladara emas user " . $param["email"] . " after register to treasury.", "ladara_emas_mobile");

        ddlog("Start send email aktivasi Ladara Emas for user " . $param["email"] . ".", "ladara_emas_mobile");

        $data_email = [
            "customer_name"   => $param["name"],
            "customer_email"  => $param["email"]
        ];

        $customer = new UsersBuilder($param["name"], $param["email"]);
        $email_customer = (new EmailHelper($customer))->aktivasiAkunEmas($data_email);
        
        ddlog("End send email aktivasi Ladara Emas for user " . $param["email"] . ".", "ladara_emas_mobile");

        ddlog("Start send email aktivasi Ladara Emas to admin for user " . $param["email"] . ".", "ladara_emas_mobile");

        $admin = new UsersBuilder($GLOBALS["gld_global"]["email_admin"]["name"], $GLOBALS["gld_global"]["email_admin"]["email"]);
        $email_admin = (new EmailHelper($admin))->adminAktivasiAkunEmas($data_email);

        ddlog("End send email aktivasi Ladara Emas to admin for user " . $param["email"] . ".", "ladara_emas_mobile");

        ddlog("Start add web notification user " . $param["email"] . " after register to treasury.", "ladara_emas_mobile");

        $notif_data = [
            "userId"        => $param["user_id"],
            "title"         => "Selamat datang di Ladara Emas!",
            "descriptions"  => "Kamu telah melakukan aktivasi akun Ladara Emas. Kamu sudah bisa menabung emas di Ladara Emas.",
            "type"          => "emas",
            "orderId"       => 0,
            "data"          => []
        ];

        Notifications::addNotification($notif_data);

        ddlog("End add web notification user " . $param["email"] . " after register to treasury.", "ladara_emas_mobile");

        ddlog("Start Generate Token for user " . $param["email"], "ladara_emas_mobile");

        $user = get_user_by( 'id', $user_id );
        $token = Token::generateToken($user);

        ddlog("End Generate Token for user " . $param["email"], "ladara_emas_mobile");
        
        return $this->sendSuccess(
            $token,
            "Sukses aktivasi akun Ladara Emas.",
            200
        );
    }

    /**
     * API calculate emas
     * @Post("/ladara-api/v1/emas/calculate")
     * @param WP_REST_Request $request
     * @return \WP_REST_Response
     */
    public function emas_calculate(WP_REST_Request $request) {
        global $wpdb;

        $token = Token::validate_token();
        if($token['status'] != 200){
            return $token;
        }

        $user_id = $token['data']['user_id'];

        ddlog("Start calculate emas for user id " . $user_id . ".", "ladara_emas_mobile");

        ddlog("Start check is user id " . $user_id . " already activate their ladara emas account for calculate proccess.", "ladara_emas_mobile");

        if($token['data']['user_emas']['flag'] === false) {
            ddlog("End check is user id " . $user_id . " already activate their ladara emas account for calculate proccess. Haven't activated.", "ladara_emas_mobile");

            return $this->sendError(
                [],
                "Kamu belum mengaktivasikan akun Ladara Emas mu!",
                400
            );
        }

        ddlog("End check is user id " . $user_id . " already activate their ladara emas account for calculate proccess. Activated.", "ladara_emas_mobile");

        $param = array(
            "amount_type"       => $request->get_param('amount_type'),
            "amount"            => $request->get_param('amount'),
            "transaction_type"  => $request->get_param('transaction_type'),
            "payment_type"      => "gross",
        );

        ddlog("Start calculate " . $param["transaction_type"] . " emas for user " . $user_id .".");
    
        // get profile emas
        $profile = gld_profile_emas(array("user_id"=>$user_id));
        $profile = json_decode($profile);

        if($profile->code !== 200) {
            return $this->sendError(
                [],
                $profile->message,
                $profile->code
            );
        }

        $error_data = [
            "buy_price"   => 0,
            "sell_price"  => 0,
            "currency"    => 0,
            "unit"        => 0,
            "total"       => 0
        ];

        // cek balance emas sebelum jual
        if($param["amount_type"] === "sell") {
            ddlog("Start check user " . $profile->data->email . " balance for calculate sell emas proccess.", "ladara_emas_mobile");

            $count_emas = $wpdb->get_row(
                "SELECT SUM(total_gold) as sum_total_gold FROM ldr_transaksi_emas WHERE user_id=" . $profile->data->id,
                OBJECT
            );

            $total_gold_balance = str_replace(".", ",", round($count_emas->sum_total_gold, 1, PHP_ROUND_HALF_UP));
            $gold_balance = str_replace(".", ",", round($profile->data->gold_balance, 1, PHP_ROUND_HALF_UP));

            if($profile->data->gold_balance == 0) {
                ddlog("End check user " . $profile->data->email . " balance for calculate sell emas proccess. User don't have emas.", "ladara_emas_mobile");

                return $this->sendError(
                    $error_data,
                    "Kamu belum memiliki emas!",
                    400
                );
            }

            if($param["amount_type"] === "gold") {
                ddlog("End check user " . $profile->data->email . " balance for calculate sell emas proccess. User's balance didn't enought to do sell emas " . $param["amount"] . " gram, user only have " . $profile->data->gold_balance . " gram.", "ladara_emas_mobile");

                $amount = str_replace(",", ".", $param["amount"]);
                if($profile->data->gold_balance < $amount) {
                    ddlog("End calculate " . $param["transaction_type"] . " emas for user " . $user_id .". Failed. User don't enough balance.");

                    return $this->sendError(
                        $error_data,
                        "Maaf saldo kamu tidak cukup!",
                        400
                    );
                }
            }  

            if($param["amount_type"] === "currency") {
                ddlog("End check user " . $profile->data->email . " balance for calculate sell emas proccess. User's balance didn't enought to do sell emas Rp. " . $param["amount"] . ", user only have Rp. " . $profile->data->gold_balance_in_currency . ".", "ladara_emas_mobile");

                $amount = str_replace(".", "", $param["amount"]);
                if($profile->data->gold_balance_in_currency < $amount) {
                    return $this->sendError(
                        $error_data,
                        "Maaf saldo kamu tidak cukup!",
                        400
                    );
                }
            }

            ddlog("End check user " . $profile->data->email . " balance for calculate sell emas proccess. Valid.", "ladara_emas_mobile");
        }

        // validate beli min 5.000, max 100.000.000
        if($param["amount_type"] === "buy") {
            ddlog("Start validate amount for calculate buy emas user " . $profile->data->email . " balance proccess.", "ladara_emas_mobile");

            if($param["amount_type"] === "currency") {
                $amount = str_replace(".", "", $param["amount"]);
                if($amount < 5000) {
                    ddlog("Start validate amount for calculate buy emas user " . $profile->data->email . " balance proccess. Min 5.000.", "ladara_emas_mobile");

                    return $this->sendError(
                        $error_data,
                        "Minimal pembelian emas sebesar 5.000.",
                        400
                    );
                }
                if($amount > 100000000) {
                    ddlog("Start validate amount for calculate buy emas user " . $profile->data->email . " balance proccess. Max 100.000.000.", "ladara_emas_mobile");

                    return $this->sendError(
                        $error_data,
                        "Maksimal pembelian emas sebesar 100.000.000.",
                        400
                    );
                }
            }

            ddlog("Start validate amount for calculate buy emas user " . $profile->data->email . " balance proccess. Valid.", "ladara_emas_mobile");
        }

        $body = $param;

        $authorization = "Authorization: Bearer " . $token['data']['user_emas']['access_token'];
        $curl_header = array();
        $curl_header[] = $authorization;

        $ch = curl_init();
        curl_setopt_array($ch, array(
            CURLOPT_URL => getenv('TREASURY_BASE_URL') . "/calculate",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "POST",
            CURLOPT_POSTFIELDS => $body,
            CURLOPT_HTTPHEADER => $curl_header,
        ));

        $output_calculate = curl_exec($ch);
        curl_getinfo($ch, CURLINFO_EFFECTIVE_URL);
        curl_getinfo($ch, CURLINFO_HTTP_CODE);
        curl_close($ch);
        $json_calculate = json_decode($output_calculate);

        // gangguan server calculate emas treasury
        if(!isset($json_calculate->meta->code)) {
            ddlog("End calculate " . $param["transaction_type"] . " emas for user " . $user_id .". Failed to connect treasury api.\n" . htmlentities($output_calculate), "ladara_emas_mobile");

            return $this->sendError(
                [],
                "Mohon maaf terjadi gangguan dalam melakukan penghitungan " . $param["transaction_type"] . " emas. Silahkan coba lagi setelah beberapa menit.",
                500
            );
        }

        if($json_calculate->meta->code !== 200) {
            // $check_min = ( strstr( $json_calculate->meta->message, "5.000" ) ? "failed" : "success" );
            // if($json_calculate->meta->code === 400 || $check_min === "failed") {
            //     ddlog("End calculate " . $param["transaction_type"] . " emas for user " . $user_id .". Min 5.000.", "ladara_emas_mobile");

            //     return $this->sendError(
            //         $error_data,
            //         "Minimal pembelian emas sebesar 5.000.",
            //         400
            //     );
            // }

            // $check_max = ( strstr( $json_calculate->meta->message, "100.000.000" ) ? "failed" : "success" );
            // if($json_calculate->meta->code === 400 || $check_max === "failed") {
            //     ddlog("End calculate " . $param["transaction_type"] . " emas for user " . $user_id .". Max 100.000.000.", "ladara_emas_mobile");

            //     return $this->sendError(
            //         $error_data,
            //         "Maksimal pembelian emas sebesar 100.000.000.",
            //         400
            //     );
            // }

            ddlog("End calculate " . $param["transaction_type"] . " emas for user " . $user_id .". Error " . $json_calculate->meta->code . " (" . $json_calculate->meta->message . ").", "ladara_emas_mobile");

            return $this->sendError(
                $error_data,
                $json_calculate->meta->message,
                $json_calculate->meta->code
            );
        }

        ddlog("End calculate emas for user " . $user_id .". Success.");

        $calculate_data = [
            "buy_price" => round($json_calculate->data->buy_price),
            "sell_price" => round($json_calculate->data->sell_price),
            "currency" => round($json_calculate->data->currency),
            "unit" => round($json_calculate->data->unit, 4, PHP_ROUND_HALF_UP),
            "tax" => round($json_calculate->data->tax),
            "booking_fee" => round($json_calculate->data->booking_fee),
            "admin_fee" => round($json_calculate->data->admin_fee),
            "partner_fee" => round($json_calculate->data->partner_fee),
            "total" => round($json_calculate->data->total),
        ];

        if($param["transaction_type"] === "buy") {
            $calculate_data["disable_cc"] = false;
            if($json_calculate->data->currency < 10000) {
                $calculate_data["disable_cc"] = true;
            }
        }

        // ddlog(json_encode($calculate_data), "ladara_emas_mobile");

        return $this->sendSuccess(
            $calculate_data,
            "Sukses.",
            200
        );
    }

    /**
     * API rate emas
     * @Post("/ladara-api/v1/emas/rate")
     * @return \WP_REST_Response
     */
    public function emas_rate() {
        $token = Token::validate_token();
        if($token['status'] != 200){
            return $token;
        }

        if($token['data']['user_emas']['flag'] === false) {
            return $this->sendError(
                [],
                "Kamu belum mengaktivasikan akun Ladara Emas mu.",
                400
            );
        }

        $user_id = $token['data']['user_id'];

        // kadang g muncul mulu rate nya, g tiap hari ganti
        $current_date = date("Y-m-d H:i:s");
        $start_date = date('Y-m-d H:i:s', strtotime('-1 day'));
        $end_date = $current_date;

        ddlog("Start get rate emas from " . $start_date . " to " . $end_date . ".", "ladara_emas_mobile");

        $authorization = "Authorization: Bearer " . $token['data']['user_emas']['access_token'];
        $curl_header = array();
        $curl_header[] = $authorization;

        $body = [
            "start_date"    => $start_date,
            "end_date"      => $end_date,
        ];

        $ch = curl_init();

        curl_setopt_array($ch, array(
            CURLOPT_URL => getenv('TREASURY_BASE_URL') . "/gold-price",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "POST",
            CURLOPT_POSTFIELDS => $body,
            CURLOPT_HTTPHEADER => $curl_header,
        ));

        $output_rate = curl_exec($ch);
        curl_close($ch);
        $json_rate = json_decode($output_rate);

        // gangguan server rate emas treasury
        if(!isset($json_rate->meta->code)) {
            ddlog("End get rate emas from " . $start_date . " to " . $end_date . ". Failed to connect treasury api.\n" . htmlentities($output_rate), "ladara_emas_mobile");

            return $this->sendError(
                [],
                "Mohon maaf terjadi gangguan check rate emas. Silahkan coba lagi setelah beberapa menit.",
                500
            );
        }

        // gagal dapat rate emas
        if($json_rate->meta->code !== 200) {
            ddlog("End get rate emas from " . $start_date . " to " . $end_date . ". Error " . $json_rate->meta->code . " (" . $json_rate->meta->message . ")", "ladara_emas_mobile");

            return $this->sendError(
                [],
                $json_rate->meta->message,
                $json_rate->meta->code
            );
        }

        $rate_data = [
            "buy_price"     => number_format(round($json_rate->data[0]->buy_price), 0, ".", "."),
            "sell_price"    => number_format(round($json_rate->data[0]->sell_price), 0, ".", "."),
            "datetime"      => $json_rate->data[0]->datetime
        ];

        return $this->sendSuccess(
            $json_rate->data[0],
            "Sukses.",
            200
        );
    }

    /**
     * API beli emas
     * @Post("/ladara-api/v1/emas/buy")
     * @param WP_REST_Request $request
     * @return \WP_REST_Response
     */
    public function emas_buy(WP_REST_Request $request) {
        global $wpdb;

        $token = Token::validate_token();
        if($token['status'] != 200){
            return $token;
        }

        $user_id = $token['data']['user_id'];

        ddlog("Start check is user id " . $user_id . " already activate their ladara emas account.", "ladara_emas_mobile");

        if($token['data']['user_emas']['flag'] === false) {
            ddlog("End check is user id " . $user_id . " already activate their ladara emas account. Haven't activated.", "ladara_emas_mobile");

            return $this->sendError(
                [],
                "Kamu belum mengaktivasikan akun Ladara Emas mu!",
                400
            );
        }

        ddlog("End check is user id " .$user_id . " already activate their ladara emas account. Activated.", "ladara_emas_mobile");
        
        $profile = gld_profile_emas(array("user_id"=>$user_id));
        $profile = json_decode($profile);

        if($profile->code !== 200) {
            return $this->sendError(
                [],
                $profile->message,
                $profile->code
            );
        }

        ddlog("Start buy emas for user " . $profile->data->email . ".", "ladara_emas_mobile");
        
        // semua param untuk calculate dan jual
        $param = array(
            "amount_type"   => $request->get_param('amount_type'),
            "amount"        => $request->get_param('amount'),
            "payment_type"  => $request->get_param('payment_type'),
            "va"            => "",
            "latitude"      => "",
            "longitude"     => "",
        );

        if($param["payment_type"] === "va" && empty($request->get_param('va'))) {
            return $this->sendError(
                [],
                "Kamu wajib memilih jenis virtual account!",
                401
            );
        }

        if(!empty($request->get_param('va'))) {
            $param["va"] = $request->get_param('va');
        }

        if(!empty($request->get_param('latitude')) && !empty($request->get_param('longitude'))) {
            $param["latitude"]  = $request->get_param('latitude');
            $param["longitude"] = $request->get_param('longitude');
        };

        ddlog("Start calculate subtotal buy emas for user " . $profile->data->email . ".", "ladara_emas_mobile");

        $body_calculate = array(
            "amount_type"       => $param["amount_type"],
            "amount"            => $param["amount"],
            "transaction_type"  => "buy",
            "payment_type"      => "gross",
        );

        $authorization = "Authorization: Bearer " . $token['data']['user_emas']['access_token'];
        $curl_header = array();
        $curl_header[] = $authorization;

        $ch = curl_init();

        curl_setopt_array($ch, array(
            CURLOPT_URL => getenv('TREASURY_BASE_URL') . "/calculate",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "POST",
            CURLOPT_POSTFIELDS => $body_calculate,
            CURLOPT_HTTPHEADER => $curl_header,
        ));

        $output_calculate = curl_exec($ch);
        curl_close($ch);
        $json_calculate = json_decode($output_calculate);

        // gangguan server calculate emas treasury
        if(!isset($json_calculate->meta->code)) {
            ddlog("End calculate subtotal buy emas for user " . $profile->data->email . ". Failed to connect treasury api \n" . htmlentities($output_calculate) . ".", "ladara_emas_mobile");

            return $this->sendError(
                [],
                "Maaf terjadi gangguan dalam melakukan penghitungan emas. Silahkan coba lagi beberapa saat lagi.",
                500
            );
        }

        // gagal calculate
        if($json_calculate->meta->code !== 200) {
            ddlog("End calculate subtotal buy emas for user " . $profile->data->email . ". Error " . $json_calculate->meta->code . " (" . $json_calculate->meta->message . ").", "ladara_emas_mobile");

            return $this->sendError(
                [],
                $json_calculate->meta->message,
                $json_calculate->meta->code
            );
        }
        
        ddlog("End calculate subtotal buy emas for user " . $profile->data->email . ". Success.", "ladara_emas_mobile");
        
        ddlog("Start proccess buy emas for user " . $profile->data->email . ".", "ladara_emas_mobile");
      
        $data_user = $wpdb->get_row(
            "SELECT ldr_user_emas.id, 
                ldr_user_emas.name,
                ldr_user_emas.email
            FROM ldr_user_emas 
            WHERE user_id=" . $user_id . "",
            OBJECT
        );
        
        $wpdb->query("INSERT INTO ldr_transaksi_emas (
                user_id, 
                type, 
                status,
                created_date
            ) VALUES (
                " . $profile->data->id . ", 
                'buy', 
                'Create Invoice', 
                '" . date('Y-m-d H:i:s') . "'
            )");

        $new_transaksi_emas = $wpdb->get_row(
            "SELECT id, user_id, invoice_order 
            FROM ldr_transaksi_emas 
            WHERE user_id=" . $profile->data->id . " 
                AND type='buy' 
                AND status='Create Invoice' 
            ORDER BY id DESC LIMIT 1",
            OBJECT
        );

        if(!empty($new_transaksi_emas)) {
            $new_transaksi_emas_id = $new_transaksi_emas->id;
        } else {
            $new_transaksi_emas_id = 0;
        }

        $wpdb->query("INSERT INTO ldr_transaksi_emas_log (
            transaksi_emas_id, 
            log, 
            status
        ) VALUES (
            " . $new_transaksi_emas_id . ", 
            'Membuat invoice beli emas.', 
            'Create Invoice'
        )");

        $external_id = "invoice-emas-" . date("Ymd") . "-" . date("His");
        $invoice_id = "LDREMAS".date("YmdHis");
        $invoice_order = "INV/EMAS/" . date("Ymd") . "/" . date("His");

        $trs_bank = "BNI";
        if(!empty($param["va"]) && strtolower($param["payment_type"]) === 'va') {
            if($param["va"] === "MANDIRI") {
                $trs_bank = "BMRI";
            } elseif($param["va"] === "BCA") {
                $trs_bank = "BCA";
            } elseif($param["va"] === "BRI") {
                $trs_bank = "BRIN";
            } elseif($param["va"] === "PERMATA") {
                $trs_bank = "BBBA";
            };
        }

        if(strtolower($param["payment_type"]) === "va") {
            $namabank = $param["va"];
            if(strtolower($param["va"]) === "mandiri" || strtolower($param["bank"]) === "permata") {
                $namabank = ucfirst($param["va"]);
            }
            $metode_pembayaran = $namabank . " Virtual Account";
            $payment_methods_xendit = [$param["va"]];
            $xendit_bank = $param["va"];
        } else {
            $metode_pembayaran = "Kartu Kredit";
            $payment_methods_xendit = ["CREDIT_CARD"];
            $xendit_bank = "";
        }

        $body_buy = array(
            "invoice_number"  => $invoice_id,
            "unit"            => $json_calculate->data->unit,
            "total"           => $json_calculate->data->total,
            "payment_method"  => "partner",
            "payment_channel" => $trs_bank
        );
        if(!empty($param["latitude"]) && !empty($param["longitude"])) {
            $body_buy["latitude"]  = $param["latitude"];
            $body_buy["longitude"] = $param["longitude"];
        };

        ddlog("Start submit beli emas to treasury for invoice " . $invoice_order . ".", "ladara_emas_mobile");

        $curl = curl_init();

        curl_setopt_array($curl, array(
            CURLOPT_URL => getenv('TREASURY_BASE_URL') . "/buy",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "POST",
            CURLOPT_POSTFIELDS => $body_buy,
            CURLOPT_HTTPHEADER => $curl_header,
        ));

        $output_buy = curl_exec($curl);
        curl_close($curl);
        $json_buy = json_decode($output_buy);

        // gangguan server beli emas treasury
        if(!isset($json_buy->meta->code)) {
            ddlog("End submit beli emas to treasury for invoice " . $invoice_order . ". Failed to connect treasury api.\n" . htmlentities($output_buy), "ladara_emas_mobile");

            return $this->sendError(
                [],
                "Mohon maaf terjadi gangguan pembelian emas. Silahkan coba lagi setelah beberapa menit.",
                500
            );
        }

        // gagal beli
        if($json_buy->meta->code !== 200) {
            ddlog("End submit beli emas to treasury for invoice " . $invoice_order . ". Error " . $json_buy->meta->code . " (" . $json_buy->meta->message . ").", "ladara_emas_mobile");

            return $this->sendError(
                [],
                $json_buy->meta->message,
                $json_buy->meta->code
            );
        }

        ddlog("End submit beli emas to treasury for invoice " . $invoice_order . ". Success.", "ladara_emas_mobile");

        ddlog("Start update transaction status for invoice " . $invoice_order . ", insert price datas", "ladara_emas_mobile");

        $wpdb->query("UPDATE ldr_transaksi_emas SET 
            trs_invoice='" . $invoice_id . "', 
            invoice_order='" . $invoice_order . "', 
            total_gold='" . $json_buy->data->unit . "', 
            total_price='" . $json_buy->data->currency . "', 
            payment_method='" . $metode_pembayaran . "', 
            buying_rate='" . $json_buy->data->buy_price . "', 
            selling_rate='" . $json_buy->data->sell_price . "', 
            booking_fee='" . $json_buy->data->booking_fee . "', 
            tax='" . $json_buy->data->tax . "', 
            partner_fee='" . $json_buy->data->parter_fee . "', 
            trs_payment_channel='" . $trs_bank . "',
            trs_due_date='" . $json_buy->data->due_date . "'
        WHERE id=" . $new_transaksi_emas_id);

        ddlog("End update transaction status for invoice " . $invoice_order . ", insert price datas", "ladara_emas_mobile");

        $total_emas_currency = round($json_buy->data->total);
        $emas_currency = round($json_buy->data->currency);

        if(strtolower($param["payment_type"]) === "va") {
            $LadaraFee = $wpdb->get_row("SELECT * FROM ldr_xendit_fee WHERE category = 'va' LIMIT 1", OBJECT);
            $xendit_fee = $LadaraFee->fee_price ?? 4500;
            $xendit_ppn = round(floatval($LadaraFee->fee_ppn ?? 0.1) * $xendit_fee);
            $xendit_fee = $xendit_fee + $xendit_ppn;
        } else {
            $LadaraFee = $wpdb->get_row("SELECT * FROM ldr_xendit_fee WHERE category = 'cc' LIMIT 1", OBJECT);  
            $xendit_fee = $LadaraFee->fee_price ?? 2000;
            $xendit_variable_fee = round(floatval($LadaraFee->fee_cc ?? 0.029) * $emas_currency);
            $xendit_ppn = round(floatval($LadaraFee->fee_ppn ?? 0.1) * $xendit_fee);
            $xendit_ppn_variable = round(floatval($LadaraFee->fee_ppn ?? 0.1) * $xendit_variable_fee);
            $xendit_total_ppn = $xendit_ppn + $xendit_ppn_variable;
            $xendit_fee = $xendit_fee + $xendit_variable_fee + $xendit_total_ppn;
        };

        $total_amount = $total_emas_currency + $xendit_fee;

        // $data_invoice = [
        //     'external_id'           => $external_id,
        //     'amount'                => $total_amount,
        //     'description'           => "Pembelian Emas di Ladara Emas dengan ID #" . $new_transaksi_emas_id,
        //     'payer_email'           => $profile->data->email,
        //     'invoice_duration'      => 3600,
        //     'payment_methods'       => $payment_methods_xendit,
        //     'should_send_email'     => false,
        //     'success_redirect_url'  => home_url().'/emas/beli/sukses-bayar/?invoice=' . $new_transaksi_emas_id,
        //     'failure_redirect_url'  => home_url().'/emas/beli/gagal-bayar/?invoice=' . $new_transaksi_emas_id
        // ];

        $data_invoice = [
            'external_id'           => $external_id,
            'amount'                => $total_amount,
            'description'           => "Pembelian Emas di Ladara Emas dengan invoice " . $invoice_order,
            'payer_email'           => $profile->data->email,
            'invoice_duration'      => 3600,
            'payment_methods'       => $payment_methods_xendit,
            'should_send_email'     => false
        ];

        // if($param["payment_type"] === "cc") {
        //     $data_invoice['success_redirect_url'] = home_url().'/emas/beli/sukses-bayar/?invoice=' . $new_transaksi_emas_id . "&mobile=true";
        //     $data_invoice['failure_redirect_url'] = home_url().'/emas/beli/gagal-bayar/?invoice=' . $new_transaksi_emas_id . "&mobile=true";
        // }

        if($param["payment_type"] === "va" && empty($request->get_param('va'))) {
            // Get User Virtual Account
            $virtualAccountUser = $wpdb->get_row("SELECT * FROM ldr_virtual_accounts WHERE user_id = $user_id AND bank_code = '".$param["va"]."'");
            $virtualAccountName = preg_replace('/\s+/', ' ', preg_replace("/[^a-zA-Z ]/", "", $data_user->name)); // Get only string
            $virtualAccountNumber = '9999'.sprintf('%06u', $user_id);

            if ($virtualAccountUser === null) { // If User doesn't have Virtual Account, create for him
                $VA = VirtualAccounts::create([
                    'external_id' => 'VA_'.$user_id.'_'.$param["va"].'_'.$virtualAccountNumber,
                    'bank_code' => $param["va"],
                    'name' => $virtualAccountName
                ]);

                $insertData = [
                    'user_id' => $user_id,
                    'virtual_account_id' => $VA['id'],
                    'virtual_account_number' => $VA["account_number"],
                    'bank_code' => $param["va"]
                ];

                $wpdb->insert('ldr_virtual_accounts', $insertData);

                $virtualAccountUser = (object) $insertData;
            }

            $data_invoice['callback_virtual_account_id'] = $virtualAccountUser->virtual_account_id;
        }

        ddlog("Start create xendit invoice for invoice " . $invoice_order . ".", "ladara_emas_mobile");

        $create_invoice = Invoice::create($data_invoice);

        if(!isset($create_invoice["id"])) {
            ddlog("End create xendit invoice for invoice " . $invoice_order . ". Failed.", "ladara_emas_mobile");

            return $this->sendError(
                [],
                "Maaf invoice pembelian emasmu gagal dibuat. Silahkan coba lagi setelah beberapa menit.",
                400
            );
        }

        ddlog("End create xendit invoice for invoice " . $invoice_order . ". Success.", "ladara_emas_mobile");

        $xendit_va = "";
        if(strtolower($param["payment_type"]) === "va") {
            $xendit_va = $create_invoice["available_banks"][0]["bank_account_number"];
        }

        ddlog("Start update transaction status for invoice " . $invoice_order . ", insert xendit datas.", "ladara_emas_mobile");

        $wpdb->query("UPDATE ldr_transaksi_emas SET 
            xendit_fee = '" . $xendit_fee . "',
            xendit_va = '" . $xendit_va . "', 
            xendit_id = '" . $create_invoice["id"] . "', 
            xendit_url = '" . $create_invoice["invoice_url"] . "', 
            xendit_bank = '" . $xendit_bank . "', 
            xendit_external_id = '" . $create_invoice["external_id"] . "'
        WHERE id = " . $new_transaksi_emas_id);

        ddlog("End update transaction status for invoice " . $invoice_order . ", insert xendit datas.", "ladara_emas_mobile");

        $data_update_transaksi_emas = [
            "trs_invoice"         => $invoice_id,
            "invoice_order"       => $invoice_order,
            "total_gold"          => $json_buy->data->unit,
            "total_price"         => $json_buy->data->currency,
            "payment_method"      => $metode_pembayaran,
            // "trs_va"              => $json_buy->data->virtual_account,
            "trs_due_date"        => $json_buy->data->due_date,
            "trs_payment_channel" => $trs_bank,
            "buying_rate"         => $json_buy->data->buy_price,
            "selling_rate"        => $json_buy->data->sell_price,
            "booking_fee"         => $json_buy->data->booking_fee,
            "tax"                 => $json_buy->data->tax,
            "partner_fee"         => $json_buy->data->parter_fee,
            // "bank_fee"            => $json_buy->data->bank_fee,
            "xendit_fee"          => $xendit_fee,
            "xendit_va"           => $xendit_va,
            "xendit_id"           => $create_invoice["id"],
            "xendit_external_id"  => $create_invoice["external_id"],
            "xendit_url"          => $create_invoice["invoice_url"],
            "xendit_bank"         => $xendit_bank,
            "total_payment"       => $total_amount,
            "status"              => "Pending",
            "due_date"            => date("Y-m-d H:i:s", strtotime('+1 hours')),
            "created_date"        => date('Y-m-d H:i:s')
        ];

        ddlog("Start update transaction status for invoice " . $data_update_transaksi_emas["invoice_order"] . " from Create Invoice to Pending.", "ladara_emas_mobile");

        $wpdb->query("UPDATE ldr_transaksi_emas SET 
                trs_invoice='" . $data_update_transaksi_emas["trs_invoice"] . "', 
                invoice_order='" . $data_update_transaksi_emas["invoice_order"] . "', 
                total_gold='" . $data_update_transaksi_emas["total_gold"] . "', 
                total_price='" . $data_update_transaksi_emas["total_price"] . "', 
                payment_method='" . $data_update_transaksi_emas["payment_method"] . "', 
                buying_rate='" . $data_update_transaksi_emas["buying_rate"] . "', 
                selling_rate='" . $data_update_transaksi_emas["selling_rate"] . "', 
                booking_fee='" . $data_update_transaksi_emas["booking_fee"] . "', 
                tax='" . $data_update_transaksi_emas["tax"] . "', 
                trs_payment_channel='" . $data_update_transaksi_emas["trs_payment_channel"] . "',
                trs_due_date='" . $data_update_transaksi_emas["trs_due_date"] . "',
                partner_fee='" . $data_update_transaksi_emas["partner_fee"] . "', 
                xendit_fee='" . $data_update_transaksi_emas["xendit_fee"] . "',
                xendit_va='" . $data_update_transaksi_emas["xendit_va"] . "', 
                xendit_id='" . $data_update_transaksi_emas["xendit_id"] . "', 
                xendit_url='" . $data_update_transaksi_emas["xendit_url"] . "', 
                xendit_bank='" . $data_update_transaksi_emas["xendit_bank"] . "', 
                xendit_external_id='" . $data_update_transaksi_emas["xendit_external_id"] . "', 
                total_payment='" . $data_update_transaksi_emas["total_payment"] . "', 
                status='" . $data_update_transaksi_emas["status"] . "', 
                due_date='" . $data_update_transaksi_emas["due_date"] . "', 
                created_date='" . $data_update_transaksi_emas["created_date"] . "'
            WHERE id=" . $new_transaksi_emas_id);

        ddlog("End update transaction status for invoice " . $data_update_transaksi_emas["invoice_order"] . " from Create Invoice to Pending.", "ladara_emas_mobile");

        ddlog("Start insert transaction log for invoice " . $data_update_transaksi_emas["invoice_order"] . ".", "ladara_emas_mobile");

        $wpdb->query("INSERT INTO ldr_transaksi_emas_log (
                transaksi_emas_id, 
                log, 
                status
            ) VALUES (
                " . $new_transaksi_emas_id . ", 
                'Pembelian emas dengan invoice " . $data_update_transaksi_emas["invoice_order"] . " menunggu pembayaran.', 
                'Pending'
            )");
        
        ddlog("End insert transaction log for invoice " . $data_update_transaksi_emas["invoice_order"] . ".", "ladara_emas_mobile");

        ddlog("Start insert payment log for invoice " . $data_update_transaksi_emas["invoice_order"] . ".", "ladara_emas_mobile");

        $data_payment_log = [
            "order_id"                  => $new_transaksi_emas_id,
            "xendit_id"                 => $create_invoice["id"],
            "external_id"               => $create_invoice["external_id"],
            "payment_method"            => "",
            "status"                    => $create_invoice["status"],
            "paid_amount"               => $create_invoice["amount"],
            "bank_code"                 => "",
            "paid_at"                   => "",
            "adjusted_received_amount"  => "",
            "currency"                  => $create_invoice["currency"],
            "type"                      => "EMAS"
        ];

        $wpdb->query("INSERT INTO ldr_payment_log (
                order_id, 
                xendit_id, 
                external_id, 
                payment_method, 
                status, 
                paid_amount, 
                bank_code, 
                paid_at, 
                adjusted_received_amount, 
                currency, 
                type
            ) VALUES (
                " . $data_payment_log["order_id"] . ", 
                '" . $data_payment_log["xendit_id"] . "', 
                '" . $data_payment_log["external_id"] . "', 
                '" . $data_payment_log["payment_method"] . "', 
                '" . $data_payment_log["status"] . "', 
                '" . $data_payment_log["paid_amount"] . "', 
                '" . $data_payment_log["bank_code"] . "', 
                '" . $data_payment_log["paid_at"] . "', 
                '" . $data_payment_log["adjusted_received_amount"] . "', 
                '" . $data_payment_log["currency"] . "', 
                '" . $data_payment_log["type"] . "'
            )");
        
        ddlog("End insert payment log for invoice " . $data_update_transaksi_emas["invoice_order"] . ".", "ladara_emas_mobile");

        $vaInstructions = array();
        if(strtolower($param["payment_type"]) === "va" && !empty($xendit_va)) {
            ddlog("Start get va instruction for invoice " . $data_update_transaksi_emas["invoice_order"] . ".", "ladara_emas_mobile");
            $va_instructions = gld_va_instructions(array("bank"=>$xendit_bank));
            $va_instructions = json_decode($va_instructions);
            $virtualaccountinstructions = (array)$va_instructions->data;

            foreach($virtualaccountinstructions as $key => $instruction) {
                $title_string = $key;
                $title = preg_replace('/(?<!\ )[A-Z]/', ' $0', $title_string);
                $title = ucwords($title);

                if(strtolower($title) === "ovo") {
                    $title = "OVO";
                }

                if (strpos(strtolower($title), "atm") !== false) {
                    $title = str_replace("Atm", "ATM", $title);
                }

                if (strpos(strtolower($title), "sms") !== false) {
                    $title = str_replace("Sms", "SMS", $title);
                }

                if (strpos(strtolower($title), "ibank") !== false) {
                    $title = str_replace("Ibank", "ibank", $title);
                }

                if (strpos(strtolower($title), "mbanking") !== false) {
                    $title = str_replace("Mbanking", "mbanking", $title);
                }

                if(strtolower($xendit_bank) !== "mandiri" && strtolower($xendit_bank) !== "permata") {
                    if (strpos($title, ucfirst(strtolower($xendit_bank))) !== false) {
                        $title = str_replace(ucfirst(strtolower($xendit_bank)), strtoupper($xendit_bank), $title);
                    }
                }
                
                $vaInstructions[$key]["title"] = $title;

                $instruction_step = array();

                foreach($instruction as $k => $inst) {
                    $instruction_step[$k] = str_replace("{{- vaNumber}}", $xendit_va, str_replace("{{companyCode}}", $GLOBALS["gld_global"]["xendit"]["company_code"], str_replace("{{companyName}}", $GLOBALS["gld_global"]["xendit"]["merchant_name"], $inst->step)));
                }

                $vaInstructions[$key]["steps"] = $instruction_step;

            }
            ddlog("End get va instruction for invoice " . $data_update_transaksi_emas["invoice_order"] . ".", "ladara_emas_mobile");
        }

        $data_email = [
            "invoice_order"     => $data_update_transaksi_emas["invoice_order"],
            "customer_name"     => $profile->data->name,
            "customer_email"    => $profile->data->email,
            "created_date"      => $data_update_transaksi_emas["created_date"],
            "due_date"          => $data_update_transaksi_emas["due_date"],
            "metode_pembayaran" => $metode_pembayaran,
            "total_price"       => $data_update_transaksi_emas["total_price"],
            "total_emas"        => $data_update_transaksi_emas["total_gold"],
            "booking_fee"       => $data_update_transaksi_emas["booking_fee"],
            "tax"               => $data_update_transaksi_emas["tax"],
            "partner_fee"       => $data_update_transaksi_emas["partner_fee"],
            "total_payment"     => $data_update_transaksi_emas["total_payment"],
            "xendit_fee"        => $data_update_transaksi_emas["xendit_fee"],
            "xen_company_code"  => $GLOBALS["gld_global"]["xendit"]["company_code"],
            "xen_merchant_name" => $GLOBALS["gld_global"]["xendit"]["merchant_name"],
            "xendit_url"        => $data_update_transaksi_emas["xendit_url"]
        ];

        if(!empty($xendit_va)) {
            $data_email["no_va"] = $xendit_va;
            if(isset($va_instructions) && $va_instructions->code === 200) {
                $data_email["va_instructions"] = $va_instructions->data;
            }
        }

        ddlog("Start send invoice buy emas for user " . $profile->data->email . " with invoice " . $data_update_transaksi_emas["invoice_order"] . ".", "ladara_emas_mobile");

        $customer = new UsersBuilder($profile->data->name, $profile->data->email);
        $email_customer = (new EmailHelper($customer))->invoiceBeliEmas($data_email);

        ddlog("End send invoice buy emas for user " . $profile->data->email . " with invoice " . $data_update_transaksi_emas["invoice_order"] . ".", "ladara_emas_mobile");
        
        ddlog("Start send invoice buy emas admin with invoice " . $data_update_transaksi_emas["invoice_order"] . ".", "ladara_emas_mobile");

        $admin = new UsersBuilder($GLOBALS["gld_global"]["email_admin"]["name"], $GLOBALS["gld_global"]["email_admin"]["email"]);
        $email_admin = (new EmailHelper($admin))->adminInvoiceBeliEmas($data_email);

        ddlog("End send invoice buy emas admin with invoice " . $data_update_transaksi_emas["invoice_order"] . ".", "ladara_emas_mobile");

        $data_return = [
            "no_invoice"        => $data_update_transaksi_emas["invoice_order"],
            "created_at"        => $data_update_transaksi_emas["created_date"],
            "due_date"          => $data_update_transaksi_emas["due_date"],
            "metode_pembayaran" => $metode_pembayaran,
            "total_emas"        => $data_update_transaksi_emas["total_gold"],
            "total_price"       => $data_update_transaksi_emas["total_price"],
            "total_payment"     => $data_update_transaksi_emas["total_payment"],
            "booking_fee"       => $data_update_transaksi_emas["booking_fee"],
            "tax"               => $data_update_transaksi_emas["tax"],
            "partner_fee"       => $data_update_transaksi_emas["partner_fee"],
            "xendit_url"        => $data_update_transaksi_emas["xendit_url"],
            "xendit_fee"        => $data_update_transaksi_emas["xendit_fee"],
            "va_instructions"   => [],
        ];
        if(!empty($xendit_va)) {
            $data_return["no_va"] = $xendit_va;
            if(!empty($vaInstructions)) {
                $data_return["va_instructions"] = $vaInstructions;
            }
        }

        ddlog("End proccess buy emas for user " . $profile->data->email . ".", "ladara_emas_mobile");

        $due_date_response = date("j", strtotime($data_return["due_date"])) . " " . month_indonesia(date("n", strtotime($data_return["due_date"]))) . " " . date("Y", strtotime($data_return["due_date"])) . " (" . date("H:i", strtotime($data_return["due_date"])) . "WIB)";

        ddlog("End buy emas for user " . $profile->data->email . ".", "ladara_emas_mobile");

        $notif_data = [
            "userId"        => $user_id,
            "title"         => "Kamu baru saja membeli emas",
            "descriptions"  => "Kamu baru saja membeli emas sebesar " . $data_update_transaksi_emas["total_gold"] . " gram. Yuk segera selesaikan pembelian emas mu!",
            "type"          => "emas",
            "orderId"       => 0,
            "data"          => []
        ];

        Notifications::addNotification($notif_data);

        return $this->sendSuccess(
            $data_return,
            "Pembelian emas sebesar " . $data_update_transaksi_emas["total_gold"] . "gram kamu sedang diproses. Segera melakukan pembayaran sebelum " . $due_date_response . ".",
            200
        );
    }

    /**
     * API detail history emas
     * @Post("/ladara-api/v1/emas/invoice_status")
     * @param WP_REST_Request $request
     * @return \WP_REST_Response
     */
    public function emas_invoice_status(WP_REST_Request $request) {
        global $wpdb;

        $token = Token::validate_token();
        if($token['status'] != 200){
            return $token;
        }

        $user_id = $token['data']['user_id'];

        ddlog("Start check is user id " . $user_id . " already activate their ladara emas account.", "ladara_emas_mobile");

        if($token['data']['user_emas']['flag'] === false) {
            ddlog("End check is user id " . $user_id . " already activate their ladara emas account. Haven't activated.", "ladara_emas_mobile");

            return $this->sendError(
                [],
                "Kamu belum mengaktivasikan akun Ladara Emas mu!",
                400
            );
        }

        ddlog("End check is user id " .$user_id . " already activate their ladara emas account. Activated.", "ladara_emas_mobile");

        $invoice_order = $request->get_param('invoice_order');

        $profile = gld_profile_emas(array("user_id"=>$user_id));
        $profile = json_decode($profile);

        if($profile->code !== 200) {
            return $this->sendError(
                [],
                $profile->message,
                $profile->code
            );
        }
        
        ddlog("Start get invoice " . $invoice_order . " status for user " . $profile->data->email . ".", "ladara_emas_mobile");

        $history = $wpdb->get_row(
            "SELECT id, user_id, type, invoice_order, total_gold, total_price, 
                bank_account_name, bank_account, bank_name, 
                buying_rate, selling_rate, booking_fee, tax, partner_fee, payment_method, 
                xendit_fee, xendit_va, xendit_bank, xendit_url, total_payment, status, due_date, created_date 
            FROM ldr_transaksi_emas WHERE invoice_order='".$invoice_order."' AND status!='Create Invoice'",
            OBJECT
        );

        if(empty($history)) {
            ddlog("End get invoice " . $invoice_order . " status for user " . $profile->data->email . ". Invoice " . $invoice_order . " can't be found.", "ladara_emas_mobile");

            return $this->sendError(
                [],
                "Invoice " . $invoice_order . " tidak ditemukan!",
                404
            );
        }

        $user = $wpdb->get_row(
                "SELECT name, id, email, account_name, account_number, bank_code, bank_name, branch 
                    FROM ldr_user_emas WHERE id=" . $history->user_id . "",
                OBJECT
            );

        if(empty($user)) {
            ddlog("End get emas history " . $type . " detail invoice " . $invoice_order . " for user " . $profile->data->email . ". User for invoice " . $invoice_order . " can't be found.", "ladara_emas_mobile");

            return $this->sendError(
                [],
                "User untuk invoice " . $invoice_order . " tidak ditemukan!",
                404
            );
        }

        $retrieve_payment = gld_retrieve_payment($history->id, $history->user_id);
        $retrieve_payment = json_decode($retrieve_payment);
        
        if($retrieve_payment->status !== 200) {
            ddlog("End get emas history " . $type . " detail invoice " . $invoice_order . " for user " . $profile->data->email . ". Not found.", "ladara_emas_mobile");
            
            return $this->sendError(
                [],
                "Invoice " . $invoice_order . " tidak ditemukan!",
                404
            );
        }

        $rspn = [
            "status" => $retrieve_payment->data->status,
        ];

        ddlog("End get emas history " . $type . " detail invoice " . $invoice_order . " for user " . $profile->data->email . ". Sukses.", "ladara_emas_mobile");

        return $this->sendSuccess(
            $rspn,
            "Sukses.",
            200
        );
    }

    /**
     * API jual emas
     * @Post("/ladara-api/v1/emas/sell")
     * @param WP_REST_Request $request
     * @return \WP_REST_Response
     */
    public function emas_sell(WP_REST_Request $request) {
        global $wpdb;

        $token = Token::validate_token();
        if($token['status'] != 200){
            return $token;
        }

        $user_id = $token['data']['user_id'];

        ddlog("Start check is user id " . $user_id . " already activate their ladara emas account.", "ladara_emas_mobile");

        if($token['data']['user_emas']['flag'] === false) {
            ddlog("End check is user id " . $user_id . " already activate their ladara emas account. Haven't activated.", "ladara_emas_mobile");

            return $this->sendError(
                [],
                "Kamu belum mengaktivasikan akun Ladara Emas mu!",
                400
            );
        }

        ddlog("End check is user id " .$user_id . " already activate their ladara emas account. Activated.", "ladara_emas_mobile");

        $profile = gld_profile_emas(array("user_id"=>$user_id));
        $profile = json_decode($profile);

        if($profile->code !== 200) {
            return $this->sendError(
                [],
                $profile->message,
                $profile->code
            );
        }

        ddlog("Start sell emas for user " . $profile->data->email . ".", "ladara_emas_mobile");

        // semua param untuk calculate dan jual
        $param = array(
            "amount_type"   => $request->get_param('amount_type'),
            "amount"        => $request->get_param('amount'),
            "latitude"      => "",
            "longitude"     => "",
        );

        if(!empty($request->get_param('latitude')) && !empty($request->get_param('longitude'))) {
            $param["latitude"]  = $request->get_param('latitude');
            $param["longitude"] = $request->get_param('longitude');
        };

        ddlog("Start calculate subtotal sell emas for user " . $profile->data->email . ".", "ladara_emas_mobile");

        $body_calculate = array(
            "amount_type"       => $param["amount_type"],
            "amount"            => $param["amount"],
            "transaction_type"  => "sell",
            "payment_type"      => "gross",
        );

        $authorization = "Authorization: Bearer " . $token['data']['user_emas']['access_token'];
        $curl_header = array();
        $curl_header[] = $authorization;

        $ch = curl_init();

        curl_setopt_array($ch, array(
            CURLOPT_URL => getenv('TREASURY_BASE_URL') . "/calculate",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "POST",
            CURLOPT_POSTFIELDS => $body_calculate,
            CURLOPT_HTTPHEADER => $curl_header,
        ));

        $output_calculate = curl_exec($ch);
        curl_close($ch);
        $json_calculate = json_decode($output_calculate);

        // gangguan server calculate emas treasury
        if(!isset($json_calculate->meta->code)) {
            ddlog("End calculate subtotal sell emas for user " . $profile->data->email . ". Failed to connect treasury api \n" . htmlentities($output_calculate) . ".", "ladara_emas_mobile");

            return $this->sendError(
                [],
                "Maaf terjadi gangguan dalam melakukan penghitungan emas. Silahkan coba lagi beberapa saat lagi.",
                500
            );
        }

        // gagal calculate
        if($json_calculate->meta->code !== 200) {
            ddlog("End calculate subtotal sell emas for user " . $profile->data->email . ". Error " . $json_calculate->meta->code . " (" . $json_calculate->meta->message . ").");

            return $this->sendError(
                [],
                "Maaf gagal melakukan penghitungan emas. Silahkan coba lagi beberapa saat lagi.",
                $json_calculate->meta->code
            );
        }
        
        ddlog("End calculate subtotal sell emas for user " . $profile->data->email . ". Success.", "ladara_emas_mobile");
        
        ddlog("Start proccess sell emas for user " . $profile->data->email . ".", "ladara_emas_mobile");
      
        $data_user = $wpdb->get_row(
            "SELECT ldr_user_emas.id, 
                ldr_user_emas.name,
                ldr_user_emas.email,
                ldr_user_emas.account_name,
                ldr_user_emas.account_number,
                ldr_user_emas.bank_name
            FROM ldr_user_emas 
            WHERE user_id=" . $user_id . "",
            OBJECT
        );
        
        $wpdb->query("INSERT INTO ldr_transaksi_emas (
                user_id, 
                type, 
                status,
                created_date
            ) VALUES (
                " . $profile->data->id . ", 
                'sell', 
                'Create Invoice', 
                '" . date('Y-m-d H:i:s') . "'
            )");

        $new_transaksi_emas = $wpdb->get_row(
            "SELECT id, user_id, invoice_order 
            FROM ldr_transaksi_emas 
            WHERE user_id=" . $profile->data->id . " 
                AND type='sell' 
                AND status='Create Invoice' 
            ORDER BY id DESC LIMIT 1",
            OBJECT
        );

        if(!empty($new_transaksi_emas)) {
            $new_transaksi_emas_id = $new_transaksi_emas->id;
        } else {
            $new_transaksi_emas_id = 0;
        }

        $wpdb->query("INSERT INTO ldr_transaksi_emas_log (
            transaksi_emas_id, 
            log, 
            status
        ) VALUES (
            " . $new_transaksi_emas_id . ", 
            'Membuat invoice jual emas.', 
            'Create Invoice'
        )");

        $invoice_order = "INV/EMAS/" . date("Ymd") . "/" . date("His");

        $body_buy = array(
            "total" => $json_calculate->data->total,
            "unit"  => $json_calculate->data->unit
        );

        ddlog("Start submit jual emas to treasury for invoice " . $invoice_order . ".\n" . json_encode($body_buy), "ladara_emas_mobile");

        $curl = curl_init();

        curl_setopt_array($curl, array(
            CURLOPT_URL => getenv('TREASURY_BASE_URL') . "/sell",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "POST",
            CURLOPT_POSTFIELDS => $body_buy,
            CURLOPT_HTTPHEADER => $curl_header,
        ));

        $output_sell = curl_exec($curl);
        curl_close($curl);
        $json_sell = json_decode($output_sell);

        // gangguan server jual emas treasury
        if(!isset($json_sell->meta->code)) {
            ddlog("End submit jual emas to treasury for invoice " . $invoice_order . ". Failed to connect treasury api.\n" . htmlentities($output_sell), "ladara_emas_mobile");

            return $this->sendError(
                [],
                "Mohon maaf terjadi gangguan penjualan emas. Silahkan coba lagi setelah beberapa menit.",
                500
            );
        }

        // gagal jual
        if($json_sell->meta->code !== 200) {
            ddlog("End submit jual emas to treasury for invoice " . $invoice_order . ". Error " . $json_sell->meta->code . " (" . $json_sell->meta->message . ").", "ladara_emas_mobile");

            return $this->sendError(
                [],
                "Mohon maaf gagal menjual emas. Silahkan coba lagi setelah beberapa menit.",
                $json_sell->meta->code
            );
        }

        ddlog("End submit jual emas to treasury for invoice " . $invoice_order . ". Success.", "ladara_emas_mobile");

        ddlog("Start update status invoice " . $invoice_order . " from Create Invoice to Pending.", "ladara_emas_mobile");

        $data_update_transaksi_emas = [
            "trs_invoice"         => $json_sell->data->invoice_no,
            "invoice_order"       => $invoice_order,
            "total_gold"          => $json_calculate->data->unit,
            "total_price"         => $json_calculate->data->currency,
            "buying_rate"         => $json_calculate->data->buy_price,
            "selling_rate"        => $json_calculate->data->sell_price,
            "booking_fee"         => $json_calculate->data->booking_fee,
            "tax"                 => $json_calculate->data->tax,
            "partner_fee"         => $json_calculate->data->partner_fee,
            "bank_account"        => $profile->data->account_number,
            "bank_account_name"   => $profile->data->account_name,
            "bank_name"           => $profile->data->bank_name,
            "total_payment"       => $json_calculate->data->total,
            "status"              => "Pending",
            "created_date"        => date('Y-m-d H:i:s')
        ];

        $wpdb->query("UPDATE ldr_transaksi_emas SET 
            trs_invoice = '" . $data_update_transaksi_emas["trs_invoice"] . "', 
            invoice_order = '" . $data_update_transaksi_emas["invoice_order"] . "', 
            total_gold = '" . $data_update_transaksi_emas["total_gold"] . "', 
            total_price = '" . $data_update_transaksi_emas["total_price"] . "', 
            buying_rate = '" . $data_update_transaksi_emas["buying_rate"] . "', 
            selling_rate = '" . $data_update_transaksi_emas["selling_rate"] . "', 
            booking_fee = '" . $data_update_transaksi_emas["booking_fee"] . "', 
            tax = '" . $data_update_transaksi_emas["tax"] . "', 
            partner_fee = '" . $data_update_transaksi_emas["partner_fee"] . "',
            bank_account = '" . $data_update_transaksi_emas["bank_account"] . "',
            bank_account_name = '" . $data_update_transaksi_emas["bank_account_name"] . "',
            bank_name = '" . $data_update_transaksi_emas["bank_name"] . "', 
            total_payment = '" . $data_update_transaksi_emas["total_payment"] . "', 
            status = '" . $data_update_transaksi_emas["status"] . "', 
            created_date = '" . $data_update_transaksi_emas["created_date"] . "'
        WHERE id = " . $new_transaksi_emas_id);

        $wpdb->query("INSERT INTO ldr_transaksi_emas_log (
            transaksi_emas_id, 
            log, 
            status
        ) VALUES (
            " . $new_transaksi_emas_id . ", 
            'Penjualan emas dengan invoice " . $data_update_transaksi_emas["invoice_order"] . " sedang diproses.', 
            'Pending'
        )");

        ddlog("End update status invoice " . $invoice_order . " from Create Invoice to Pending. Success.", "ladara_emas_mobile");

        ddlog("Start update user emas balance for user " . $profile->data->email . ".", "ladara_emas_mobile");

        $trs_profile = gld_trs_profile(array("user_id"=>$user_id));
        $trs_profile = json_decode($trs_profile);

        if ($trs_profile->code === 200) {
            $gold_balance = $trs_profile->data->gold_balance;
            $gold_balance_in_currency = $trs_profile->data->gold_balance_in_currency;

            $wpdb->query("UPDATE ldr_user_emas SET 
                gold_balance=" . $gold_balance . ",
                gold_balance_in_currency=" . $gold_balance_in_currency . "
            WHERE id=" . $profile->data->id);

            ddlog("End update user emas balance for user " . $profile->data->email . ". Success.", "ladara_emas_mobile");
        } else {
            ddlog("End update user emas balance for user " . $profile->data->email . ". Error " . $trs_profile->code . " (" . $trs_profile->message . ").", "ladara_emas_mobile");
        }

        $data_email = [
            "invoice_order"     => $data_update_transaksi_emas["invoice_order"],
            "customer_name"     => $profile->data->name,
            "customer_email"    => $profile->data->email,
            "created_date"      => $data_update_transaksi_emas["created_date"],
            "total_price"       => $data_update_transaksi_emas["total_price"],
            "total_emas"        => $data_update_transaksi_emas["total_gold"],
            "partner_fee"       => $data_update_transaksi_emas["partner_fee"],
            "total_payment"     => $data_update_transaksi_emas["total_payment"]
        ];

        ddlog("Start send email to user for invoice " . $invoice_order . ".", "ladara_emas_mobile");

        $customer = new UsersBuilder($profile->data->name, $profile->data->email);
        $email_customer = (new EmailHelper($customer))->invoiceJualEmas($data_email);

        ddlog("End send email to user for invoice " . $invoice_order . ".", "ladara_emas_mobile");

        ddlog("Start send email to admin for invoice " . $invoice_order . ".", "ladara_emas_mobile");
        
        $admin = new UsersBuilder($GLOBALS["gld_global"]["email_admin"]["name"], $GLOBALS["gld_global"]["email_admin"]["email"]);
        $email_admin = (new EmailHelper($admin))->adminInvoiceJualEmas($data_email);

        ddlog("End send email to admin for invoice " . $invoice_order . ".", "ladara_emas_mobile");

        $notif_data = [
            "userId"        => $user_id,
            "title"         => "Kamu baru saja menjual emas",
            "descriptions"  => "Kamu baru saja menjual emas sebesar " . $data_update_transaksi_emas["total_gold"] . " gram. Kami akan memproses penjualan emas mu.",
            "type"          => "emas",
            "orderId"       => 0,
            "data"          => []
        ];

        Notifications::addNotification($notif_data);

        $data_return = array(
            "no_invoice"        => $data_update_transaksi_emas["invoice_order"],
            "created_at"        => $data_update_transaksi_emas["created_date"],
            "selling_rate"      => $data_update_transaksi_emas["selling_rate"],
            "total_emas"        => $data_update_transaksi_emas["total_gold"],
            "total_currency"    => $data_update_transaksi_emas["total_price"],
            "total_payment"     => $data_update_transaksi_emas["total_payment"],
            "tax"               => $data_update_transaksi_emas["tax"],
            "partner_fee"       => $data_update_transaksi_emas["partner_fee"],
            "bank_name"         => $profile->data->bank_name,
            "account_number"    => $profile->data->account_number,
            "account_name"      => $profile->data->account_name
        );

        ddlog("End sell emas for user " . $profile->data->email . ".", "ladara_emas_mobile");

        return $this->sendSuccess(
            $data_return,
            "Penjualan emasmu sedang diproses.",
            200
        );

    }

    /**
     * API profile ladara emas
     * @Post("/ladara-api/v1/emas/profile")
     * @return \WP_REST_Response
     */
    public function emas_profile() {
        global $wpdb;

        $token = Token::validate_token();
        if($token['status'] != 200){
            return $token;
        }

        if($token['data']['user_emas']['flag'] === false) {
            return $this->sendError(
                [],
                "Kamu belum mengaktivasikan akun Ladara Emas mu!",
                400
            );
        }

        $user_id = $token['data']['user_id'];

        $get_user = $wpdb->get_row(
            "SELECT name, 
                account_name, 
                account_number, 
                bank_code, 
                bank_name, 
                branch, 
                verified_user, 
                activated_user, 
                gold_balance, 
                gold_balance_in_currency 
            FROM ldr_user_emas WHERE user_id='".$user_id."'",
            OBJECT
        );

        if(empty($get_user)) {
            return $this->sendError(
                [],
                "Maaf data kamu tidak ditemukan. Silahkan hubungi customer service kami.",
                404
            );
        }

        $user = [
            "account_name"              => $get_user->account_name, 
            "account_number"            => $get_user->account_number, 
            "bank_code"                 => $get_user->bank_code, 
            "bank_name"                 => $get_user->bank_name, 
            "branch"                    => $get_user->branch, 
            "verified_user"             => $get_user->verified_user, 
            "gold_balance"              => str_replace(".", ",", round($get_user->gold_balance, 4, PHP_ROUND_HALF_UP)),
            "gold_balance_in_currency"  => $get_user->gold_balance_in_currency
        ];

        return $this->sendSuccess(
            $user,
            "Sukses.",
            200
        );

    }

    /**
     * API history emas
     * @Post("/ladara-api/v1/emas/history")
     * @param WP_REST_Request $request
     * @return \WP_REST_Response
     */
    public function emas_history(WP_REST_Request $request) {
        global $wpdb;

        $token = Token::validate_token();
        if($token['status'] != 200){
            return $token;
        }

        $user_id = $token['data']['user_id'];

        ddlog("Start check is user id " . $user_id . " already activate their ladara emas account.", "ladara_emas_mobile");

        if($token['data']['user_emas']['flag'] === false) {
            ddlog("End check is user id " . $user_id . " already activate their ladara emas account. Haven't activated.", "ladara_emas_mobile");

            return $this->sendError(
                [],
                "Kamu belum mengaktivasikan akun Ladara Emas mu!",
                400
            );
        }

        ddlog("End check is user id " .$user_id . " already activate their ladara emas account. Activated.", "ladara_emas_mobile");

        $profile = gld_profile_emas(array("user_id"=>$user_id));
        $profile = json_decode($profile);

        if($profile->code !== 200) {
            return $this->sendError(
                [],
                $profile->message,
                $profile->code
            );
        }
        
        ddlog("Start get emas history for user " . $profile->data->email . ".", "ladara_emas_mobile");

        $per_page = 5;

        $page = ($request->get_param('current_page') - 1) * $request->get_param('limit_history');

        $param = [
            "type"      => $request->get_param('history_type'),
            "user_id"   => $profile->data->id,
            "limit"     => $request->get_param('limit_history'),
            "page"      => $page
        ];

        $rspn = gld_history($param);
        $rspn = json_decode($rspn);

        if($rspn->code === 200) {
            $rspn_history = array();

            foreach($rspn->data->history as $kh => $h) {
                $rspn_history[$kh]["id"] = $h->id;
                $rspn_history[$kh]["user_id"] = $h->user_id;
                $rspn_history[$kh]["type"] = $h->type;
                $rspn_history[$kh]["invoice_order"] = $h->invoice_order;
                $rspn_history[$kh]["total_gold"] = $h->total_gold;
                $rspn_history[$kh]["total_price"] = $h->total_price;
                $rspn_history[$kh]["account_name"] = $h->bank_account_name;
                $rspn_history[$kh]["account_number"] = $h->bank_account;
                $rspn_history[$kh]["bank_name"] = $h->bank_name;
                $rspn_history[$kh]["buying_rate"] = $h->buying_rate;
                $rspn_history[$kh]["selling_rate"] = $h->selling_rate;
                $rspn_history[$kh]["booking_fee"] = $h->booking_fee;
                $rspn_history[$kh]["tax"] = $h->tax;
                $rspn_history[$kh]["partner_fee"] = $h->partner_fee;
                $rspn_history[$kh]["payment_method"] = $h->payment_method;
                $rspn_history[$kh]["xendit_fee"] = $h->xendit_fee;
                $rspn_history[$kh]["xendit_va"] = $h->xendit_va;
                $rspn_history[$kh]["xendit_bank"] = $h->xendit_bank;
                $rspn_history[$kh]["xendit_url"] = $h->xendit_url;
                $rspn_history[$kh]["total_payment"] = $h->total_payment;
                $rspn_history[$kh]["status"] = $h->status;
                if($h->type === 'buy' && strtolower($h->status) === "success") {
                    $rspn_history[$kh]["status_detail"] = "Pembelian Berhasil";
                }
                if($h->type === 'buy' && strtolower($h->status) === "pending") {
                    $rspn_history[$kh]["status_detail"] = "Pembelian Menunggu Pembayaran";
                }
                if($h->type === 'buy' && strtolower($h->status) === "canceled") {
                    $rspn_history[$kh]["status_detail"] = "Pembelian Telah Dibatalkan";
                }
                if($h->type === 'buy' && strtolower($h->status) === "failed") {
                    $rspn_history[$kh]["status_detail"] = "Pembelian Telah Gagal";
                }
                if($h->type === 'sell' && strtolower($h->status) === "pending") {
                    $rspn_history[$kh]["status_detail"] = "Penjualan Sedang Diproses";
                }
                if($h->type === 'sell' && strtolower($h->status) === "approved") {
                    $rspn_history[$kh]["status_detail"] = "Penjualan Telah Disetujui";
                }
                if($h->type === 'sell' && strtolower($h->status) === "rejected") {
                    $rspn_history[$kh]["status_detail"] = "Penjualan Telah Ditolak";
                }
                $rspn_history[$kh]["due_date"] = $h->due_date;
                $rspn_history[$kh]["created_date"] = $h->created_date;
                $rspn_history[$kh]["va_instructions"] = array();

                if($h->type === 'buy' && strtolower($h->status) === "pending") {
                    $va_instructions = gld_va_instructions(array("bank"=>$h->xendit_bank));
                    $va_instructions = json_decode($va_instructions);
                    $va_instructions = (array)$va_instructions->data;

                    $vaInstructions = array();
                    foreach($va_instructions as $key => $instruction) {
                        $title_string = $key;
                        $title = preg_replace('/(?<!\ )[A-Z]/', ' $0', $title_string);
                        $title = ucwords($title);

                        if(strtolower($title) === "ovo") {
                            $title = "OVO";
                        }

                        if (strpos(strtolower($title), "atm") !== false) {
                            $title = str_replace("Atm", "ATM", $title);
                        }

                        if (strpos(strtolower($title), "sms") !== false) {
                            $title = str_replace("Sms", "SMS", $title);
                        }

                        if (strpos(strtolower($title), "ibank") !== false) {
                            $title = str_replace("Ibank", "ibank", $title);
                        }

                        if (strpos(strtolower($title), "mbanking") !== false) {
                            $title = str_replace("Mbanking", "mbanking", $title);
                        }

                        if(strtolower($xendit_bank) !== "mandiri" && strtolower($xendit_bank) !== "permata") {
                            if (strpos($title, ucfirst(strtolower($xendit_bank))) !== false) {
                                $title = str_replace(ucfirst(strtolower($xendit_bank)), strtoupper($xendit_bank), $title);
                            }
                        }
                        
                        $vaInstructions[$key]["title"] = $title;

                        $instruction_step = array();

                        foreach($instruction as $k => $inst) {
                            $instruction_step[$k] = str_replace("{{- vaNumber}}", $history->xendit_va, str_replace("{{companyCode}}", $GLOBALS["gld_global"]["xendit"]["company_code"], str_replace("{{companyName}}", $GLOBALS["gld_global"]["xendit"]["merchant_name"], $inst->step)));
                        }

                        $vaInstructions[$key]["steps"] = $instruction_step;

                    }

                    $rspn_history[$kh]["va_instructions"] = $vaInstructions;
                }
            }

            $rspn_data = [
                "total"     => $rspn->data->total,
                "history"   => $rspn_history,
            ];
        } else {
            $rspn_data = $rspn->data;
        }

        ddlog("End get emas history for user " . $profile->data->email . ".", "ladara_emas_mobile");

        return $this->sendSuccess(
            $rspn_data,
            $rspn->message,
            $rspn->code
        );
    }

    /**
     * API detail history emas
     * @Post("/ladara-api/v1/emas/detail_history")
     * @param WP_REST_Request $request
     * @return \WP_REST_Response
     */
    public function emas_detail_history(WP_REST_Request $request) {
        global $wpdb;

        $token = Token::validate_token();
        if($token['status'] != 200){
            return $token;
        }

        $user_id = $token['data']['user_id'];

        ddlog("Start check is user id " . $user_id . " already activate their ladara emas account.", "ladara_emas_mobile");

        if($token['data']['user_emas']['flag'] === false) {
            ddlog("End check is user id " . $user_id . " already activate their ladara emas account. Haven't activated.", "ladara_emas_mobile");

            return $this->sendError(
                [],
                "Kamu belum mengaktivasikan akun Ladara Emas mu!",
                400
            );
        }

        ddlog("End check is user id " .$user_id . " already activate their ladara emas account. Activated.", "ladara_emas_mobile");

        $invoice_order = $request->get_param('invoice_order');
        $type = $request->get_param('invoice_type');

        $profile = gld_profile_emas(array("user_id"=>$user_id));
        $profile = json_decode($profile);

        if($profile->code !== 200) {
            return $this->sendError(
                [],
                $profile->message,
                $profile->code
            );
        }
        
        ddlog("Start get emas history " . $type . " detail invoice " . $invoice_order . " for user " . $profile->data->email . ".", "ladara_emas_mobile");

        $history = $wpdb->get_row(
            "SELECT id, user_id, type, invoice_order, total_gold, total_price, 
                bank_account_name, bank_account, bank_name, 
                buying_rate, selling_rate, booking_fee, tax, partner_fee, payment_method, 
                xendit_fee, xendit_va, xendit_bank, xendit_url, total_payment, status, due_date, created_date 
            FROM ldr_transaksi_emas WHERE invoice_order='".$invoice_order."' AND type='" . $type . "' AND status!='Create Invoice'",
            OBJECT
        );

        if(empty($history)) {
            ddlog("End get emas history " . $type . " detail invoice " . $invoice_order . " for user " . $profile->data->email . ". Invoice " . $invoice_order . " can't be found.", "ladara_emas_mobile");

            return $this->sendError(
                [],
                "Invoice " . $invoice_order . " tidak ditemukan!",
                404
            );
        }

        $user = $wpdb->get_row(
                "SELECT name, id, email, account_name, account_number, bank_code, bank_name, branch 
                    FROM ldr_user_emas WHERE id=" . $history->user_id . "",
                OBJECT
            );

        if(empty($user)) {
            ddlog("End get emas history " . $type . " detail invoice " . $invoice_order . " for user " . $profile->data->email . ". User for invoice " . $invoice_order . " can't be found.", "ladara_emas_mobile");

            return $this->sendError(
                [],
                "User untuk invoice " . $invoice_order . " tidak ditemukan!",
                404
            );
        }
        
        if($history->type === 'buy' && strtolower($history->status) === "success") {
            $history->status_detail = "Pembelian Berhasil";
        }
        if($history->type === 'buy' && strtolower($history->status) === "pending") {
            $history->status_detail = "Pembelian Menunggu Pembayaran";
        }
        if($history->type === 'buy' && strtolower($history->status) === "canceled") {
            $history->status_detail = "Pembelian Telah Dibatalkan";
        }
        if($history->type === 'buy' && strtolower($history->status) === "failed") {
            $history->status_detail = "Pembelian Telah Gagal";
        }
        if($history->type === 'sell' && strtolower($history->status) === "pending") {
            $history->status_detail = "Penjualan Sedang Diproses";
        }
        if($history->type === 'sell' && strtolower($history->status) === "approved") {
            $history->status_detail = "Penjualan Telah Disetujui";
        }
        if($history->type === 'sell' && strtolower($history->status) === "rejected") {
            $history->status_detail = "Penjualan Telah Ditolak";
        }

        $vaInstructions = array();
        if($history->type === 'buy') {
            $va_instructions = gld_va_instructions(array("bank"=>$history->xendit_bank));
            $va_instructions = json_decode($va_instructions);
            $va_instructions = (array)$va_instructions->data;
            foreach($va_instructions as $key => $instruction) {
                $title_string = $key;
                $title = preg_replace('/(?<!\ )[A-Z]/', ' $0', $title_string);
                $title = ucwords($title);

                if(strtolower($title) === "ovo") {
                    $title = "OVO";
                }

                if (strpos(strtolower($title), "atm") !== false) {
                    $title = str_replace("Atm", "ATM", $title);
                }

                if (strpos(strtolower($title), "sms") !== false) {
                    $title = str_replace("Sms", "SMS", $title);
                }

                if (strpos(strtolower($title), "ibank") !== false) {
                    $title = str_replace("Ibank", "ibank", $title);
                }

                if (strpos(strtolower($title), "mbanking") !== false) {
                    $title = str_replace("Mbanking", "mbanking", $title);
                }

                if(strtolower($xendit_bank) !== "mandiri" && strtolower($xendit_bank) !== "permata") {
                    if (strpos($title, ucfirst(strtolower($xendit_bank))) !== false) {
                        $title = str_replace(ucfirst(strtolower($xendit_bank)), strtoupper($xendit_bank), $title);
                    }
                }
                
                $vaInstructions[$key]["title"] = $title;

                $instruction_step = array();

                foreach($instruction as $k => $inst) {
                    $instruction_step[$k] = str_replace("{{- vaNumber}}", $history->xendit_va, str_replace("{{companyCode}}", $GLOBALS["gld_global"]["xendit"]["company_code"], str_replace("{{companyName}}", $GLOBALS["gld_global"]["xendit"]["merchant_name"], $inst->step)));
                }

                $vaInstructions[$key]["steps"] = $instruction_step;

            }
        }

        $history->va_instructions = $vaInstructions;
        
        $rspn = [
            "history"   => $history,
            // "user"      => $user,
        ];

        ddlog("End get emas history " . $type . " detail invoice " . $invoice_order . " for user " . $profile->data->email . ".", "ladara_emas_mobile");

        return $this->sendSuccess(
            $rspn,
            "Sukses.",
            200
        );
    }

    /**
     * API bank list Ladara Emas
     * @Get("/ladara-api/v1/emas/bank")
     * @param WP_REST_Request $request
     * @return \WP_REST_Response
     */
    public function emas_bank(WP_REST_Request $request) {
        $bank_type = $request->get_param('bank_type');
        ddlog("Start get " . $bank_type . " bank list.", "ladara_emas_mobile");
        $rspn = gld_bank(array("type"=>$bank_type));
        $rspn = json_decode($rspn);
        ddlog("End get " . $bank_type . " bank list.", "ladara_emas_mobile");
        return $this->sendSuccess(
            $rspn->data,
            $rspn->message,
            $rspn->code
        );
    }

    /**
     * API get konten kelebihan Ladara Emas
     * @Get("/ladara-api/v1/emas/advantages")
     * @return \WP_REST_Response
     */
    public function emas_landing_page() {
        ddlog("Start ladara emas advantages for index activation page.", "ladara_emas_mobile");
        $rspn = gld_landing_page();
        $rspn = json_decode($rspn);
        ddlog("End ladara emas advantages for index activation page.", "ladara_emas_mobile");
        return $this->sendSuccess(
            $rspn->data,
            $rspn->message,
            200
        );
    }

    /**
     * API get banner Ladara Emas
     * @Get("/ladara-api/v1/emas/banner")
     * @return \WP_REST_Response
     */
    public function emas_banner() {
        ddlog("Start ladara emas banner for index activation page.", "ladara_emas_mobile");
        $rspn = gld_banner();
        $rspn = json_decode($rspn);
        ddlog("End ladara emas banner for index activation page.", "ladara_emas_mobile");
        return $this->sendSuccess(
            $rspn->data,
            $rspn->message,
            200
        );
    }

    /**
     * API get Ladara Emas content
     * @Get("/ladara-api/v1/emas/content")
     * @return \WP_REST_Response
     */
    public function emas_content() {
        $rspn_data = array();

        ddlog("Start ladara emas banner for index activation page.", "ladara_emas_mobile");
        $banner = gld_banner();
        $banner = json_decode($banner);
        ddlog("End ladara emas banner for index activation page.", "ladara_emas_mobile");
        $banner_message = ($banner->code === 200) ? "Sucess get Ladara Emas banner." : "Ladara Emas banner not found, please use default banner.";
        $rspn_data["banner"] = $banner->data;

        ddlog("Start ladara emas advantages for index activation page.", "ladara_emas_mobile");
        $advantages = gld_landing_page();
        $advantages = json_decode($advantages);
        ddlog("End ladara emas advantages for index activation page.", "ladara_emas_mobile");
        $advantage_message = ($advantages->code === 200) ? "Sucess get Ladara Emas advantage content." : "Ladara Emas advantage content not found, please use default content.";
        $rspn_data["advantage"] = $advantages->data;

        $rspn_message = $banner_message . " " . $advantage_message;

        return $this->sendSuccess(
            $rspn_data,
            $rspn_message,
            200
        );
    }

    /**
     * API Connect to treasury
     * @Post("/ladara-api/v1/emas/connect_to_treasury")
     * @param WP_REST_Request $request
     * @return \WP_REST_Response
     */
    public function emas_connect_to_treasury(WP_REST_Request $request) {
        global $wpdb;

        $token = Token::validate_token();
        if($token['status'] != 200){
            return $token;
        }

        $user_id = $token['data']['user_id'];

        $param = array(
            "treasury_email"    => $request->get_param('email'),
            "treasury_password" => $request->get_param('password'),
            "is_update"         => $request->get_param('is_update')
        );

        if(empty($param["treasury_email"])) {
            return $this->sendError(
                [],
                "Email Treasury tidak boleh kosong.",
                400
            );
        }
        
        $check_email_format = gld_check_email_format($param["treasury_email"]);
        if($check_email_format["code"] !== 200) {
            return $this->sendError(
                [],
                $check_email_format["message"],
                $check_email_format["code"]
            );
        }

        if(empty($param["treasury_password"])) {
            return $this->sendError(
                [],
                "Password Treasury tidak boleh kosong.",
                400
            );
        }

        $email = $param["treasury_email"];
        $password = $param["treasury_password"];
        $bank = "";//$param["bank"];
        $norek = "";//$param["norek"];
        $owner = "";//$param["owner"];
        $branch = "";//$param["branch"];

        $user_profile = gld_user_profile(array("user_id"=>$user_id));
        $user_profile = json_decode($user_profile);

        if($user_profile->code !== 200) {
            return $this->sendError(
                [],
                $user_profile->message,
                $user_profile->code
            );
        }

        $body = [
            "client_id"     => getenv('TREASURY_CLIENT_ID'),
            "client_secret" => getenv('TREASURY_CLIENT_SECRET'),
            "grant_type"    => getenv('TREASURY_GRANT_CUSTOMER'),
            "email"         => $email,
            "password"      => $password
        ];

        $curl_header = array();
        $curl_header[] = "Accept: application/json";

        $ch = curl_init();

        curl_setopt_array($ch, array(
        CURLOPT_URL => getenv('TREASURY_BASE_URL') . "/login",
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_ENCODING => "",
        CURLOPT_MAXREDIRS => $GLOBALS["gld_global"]["curl_maxredirs"],
        CURLOPT_TIMEOUT => $GLOBALS["gld_global"]["curl_timeout"],
        CURLOPT_FOLLOWLOCATION => true,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => "POST",
        CURLOPT_POSTFIELDS => $body,
        CURLOPT_HTTPHEADER => array(
            "Accept: application/json",
        ),
        ));
        
        $output_login = curl_exec($ch);
        curl_getinfo($ch, CURLINFO_EFFECTIVE_URL);
        curl_getinfo($ch, CURLINFO_HTTP_CODE);
        curl_close($ch);
        $json_login = json_decode($output_login);

        if(!isset($json_login->meta->code)) {
            return $this->sendError(
                [],
                "Maaf terjadi gangguan untuk cek user Treasury. Silahkan coba lagi beberapa saat lagi.",
                500
            );
        }

        if($json_login->meta->code !== 200) {
            return $this->sendError(
                [],
                "Maaf gagal untuk cek user Treasury, " . $json_login->meta->message . ". Silahkan coba lagi beberapa saat lagi.",
                $json_login->meta->code
            );
        }

        $authorization = "Authorization: Bearer " . $json_login->data->access_token;
        $curl_header = array();
        $curl_header[] = $authorization;

        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, getenv('TREASURY_BASE_URL') . "/profile");
        curl_setopt($curl, CURLOPT_TIMEOUT, $GLOBALS["gld_global"]["curl_timeout"]);
        curl_setopt($curl, CURLOPT_FOLLOWLOCATION, true);
        curl_setopt($curl, CURLOPT_HTTP_VERSION, CURL_HTTP_VERSION_1_1);
        curl_setopt($curl, CURLOPT_CUSTOMREQUEST, "GET");
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_ENCODING, "");
        curl_setopt($curl, CURLOPT_MAXREDIRS, $GLOBALS["gld_global"]["curl_maxredirs"]);
        curl_setopt($curl, CURLOPT_HTTPHEADER, $curl_header);

        $output_trs_profile = curl_exec($curl);
        curl_getinfo($curl, CURLINFO_EFFECTIVE_URL);
        curl_getinfo($curl, CURLINFO_HTTP_CODE);
        curl_close($curl);
        $json_trs_profile = json_decode($output_trs_profile);

        if(!isset($json_trs_profile->meta->code)) {
            return $this->sendError(
                [],
                "Maaf terjadi gangguan untuk mendapatkan profile Treasury. Silahkan coba lagi beberapa saat lagi.",
                500
            );
        }

        if($json_trs_profile->meta->code !== 200) {
            return $this->sendError(
                [],
                "Maaf gagal menghubungkan akun Treasury kamu dengan Ladara Emas. Silahkan coba lagi beberapa saat lagi.",
                $json_trs_profile->meta->code
            );
        }

        $verified_user = 0;
        if (
            strtolower($json_trs_profile->data->RegNoKYCStatus) == "verified" &&
            strtolower($json_trs_profile->data->SelfieKYCStatus) == "verified"
        ) {
            $verified_user = 1;
        }

        if(!isset($param["is_update"]) || (isset($param["is_update"]) && $param["is_update"] === "false")) {
            $wpdb->query("INSERT INTO ldr_user_emas (
                user_id, 
                name, 
                email, 
                ts_password, 
                gender, 
                birthday, 
                phone, 
                security_question, 
                security_question_answer, 
                account_name, 
                account_number, 
                bank_code,  
                bank_name, 
                branch,
                verified_user, 
                gold_balance, 
                gold_balance_in_currency,
                trs_user 
            ) VALUES (
                " . $user_id . ",  
                '" . $user_profile->data->name . "',  
                '" . $email . "',  
                '" . $password . "',  
                '" . $user_profile->data->gender . "', 
                '" . $user_profile->data->birthdate . "',  
                '" . $user_profile->data->phone . "',  
                '',  
                '(connect to treasury)',   
                '" . $owner . "',  
                '" . $norek . "',  
                '" . $bank . "',   
                '" . $bank_name . "',  
                '" . $branch . "',
                " . $verified_user . ",  
                " . $json_trs_profile->data->gold_balance . ",  
                " . $json_trs_profile->data->gold_balance . ",
                1
            )");

            $new_user_emas = $wpdb->get_row(
                "SELECT id, user_id, name, email
                    FROM ldr_user_emas 
                    WHERE user_id=" . $user_id . " 
                    AND email='" . $user_profile->data->email . "' 
                    ORDER BY id DESC LIMIT 1",
                OBJECT
            );

            $wpdb->query("INSERT INTO ldr_user_emas_log (
                user_emas_id, 
                log 
            ) VALUES (
                " . $new_user_emas->id . ",  
                'User " . $new_user_emas->name . " (" . $new_user_emas->email . ") linking their Treasury account to Ladara Emas.' 
            )");

            $data_email = [
                "customer_name"   => $user_profile->data->name,
                "customer_email"  => $user_profile->data->email
            ];

            $customer = new UsersBuilder($user_profile->data->name, $user_profile->data->email);
            $email_customer = (new EmailHelper($customer))->aktivasiAkunEmas($data_email);
            
            $admin = new UsersBuilder($GLOBALS["gld_global"]["email_admin"]["name"], $GLOBALS["gld_global"]["email_admin"]["email"]);
            $email_admin = (new EmailHelper($admin))->adminAktivasiAkunEmas($data_email);

            $notif_data = [
                "userId"        => $user_id,
                "title"         => "Selamat datang di Ladara Emas!",
                "descriptions"  => "Kamu telah melakukan aktivasi akun Ladara Emas. Kamu sudah bisa menabung emas di Ladara Emas.",
                "type"          => "emas",
                "orderId"       => 0,
                "data"          => []
            ];

            Notifications::addNotification($notif_data);
        }
    
        if(isset($param["is_update"]) && $param["is_update"] === "true") {
            $wpdb->query("UPDATE ldr_user_emas 
                SET name='" . $email . "', 
                ts_password='" . $password . "' 
                WHERE id=" . $user_id . ";");

            $wpdb->query("INSERT INTO ldr_user_emas_log (
                    user_emas_id, 
                    log 
                ) VALUES (
                    " . $user_profile->data->id . ",  
                    'User " . $user_profile->data->name . " (" . $user_profile->data->email . ") update their Treasury credentials for linking their Treasury account to Ladara Emas.' 
                )");

            $data_email = [
                "customer_name"   => $user_profile->data->name,
                "customer_email"  => $user_profile->data->email,
                "log_date"        => date("Y-m-d H:i:s")
            ];

            $customer = new UsersBuilder($user_profile->data->name, $user_profile->data->email);
            $email_customer = (new EmailHelper($customer))->updateAkunTreasury($data_email);

            $admin = new UsersBuilder($GLOBALS["gld_global"]["email_admin"]["name"], $GLOBALS["gld_global"]["email_admin"]["email"]);
            $email_admin = (new EmailHelper($admin))->adminUpdateAkunTreasury($data_email);

            $notif_data = [
                "userId"        => $user_id,
                "title"         => "Akun Ladara Emas kamu telah berubah!",
                "descriptions"  => "Kamu telah melakukan perubahan akun Ladara Emas kamu yang terhubung dengan Treasury.",
                "type"          => "emas",
                "orderId"       => 0,
                "data"          => []
            ];

            Notifications::addNotification($notif_data);
        }


        return $this->sendSuccess(
            [],
            "Kamu telah sukses menghubungkan akun Treasury kamu dengan Ladara Emas.",
            200
        );
    }
}