<?php


namespace Ladara\Controllers;

if (! defined('ABSPATH')) {
    exit;
}

use Ladara\Helpers\BaseController;
use Ladara\Helpers\ControllerInterface;
use Ladara\Helpers\Jwt;
use Ladara\Helpers\ProductHelpers;
use Ladara\Models\Notifications;
use Ladara\Models\OrderModel;
use WC_Order_Item_Shipping;
use WP_REST_Request;

/**
 * Class Orders
 * @package Ladara\Controllers
 * @author rikihandoyo
 */
class Orders extends BaseController implements ControllerInterface
{
    /**
     * @inheritDoc
     */
    public function register_routes()
    {
        //get orders
        register_rest_route($this->nameSpace, 'orders', [
            'methods' => 'GET',
            'callback' => [$this,'getOrders'],
        ]);

        // get order by id
        register_rest_route($this->nameSpace, 'order/(?P<id>\d+)', [
            'methods' => 'GET',
            'callback' => [$this,'getOrderById'],
        ]);

        // Create Invoice
        // register_rest_route($this->nameSpace, 'orders/create-order', [
        //     'methods' => 'POST',
        //     'callback' => [$this,'createOrder'],
        // ]);

        // Konfirmasi penerimaan barang
        register_rest_route($this->nameSpace, 'orders/konfirmasi', [
            'methods' => 'POST',
            'callback' => [$this,'konfirmasi'],
        ]);
    }

    /**
     * api get orders user
     * @Get("/ladara-api/v1/orders")
     * @param WP_REST_Request $request
     * @return \WP_REST_Response
     */
    public function getOrders(WP_REST_Request $request)
    {
        $status = $request->get_param('status'); //Options: any, pending, processing, on-hold, completed, cancelled,preparing,ondelivery,processing, refunded, failed and trash. Default is any.
        $limit = $request->get_param('limit');
        $search = $request->get_param('search');
        $page = $request->get_param('page');
        $startDate = $request->get_param('startDate');
        $endDate = $request->get_param('endDate');

        $token = Jwt::validate_token();
        if ($token['status'] != 200) {
            return $token;
        }
        $statusEx = explode(',', $status);
        $args = [
            'status' => $statusEx ?? 'any',
            'limit' => $limit ?? 10,
            'page' => $page,
            'customer_id' => $token['data']['user_id']
//            'customer_id' => 31
        ];
        $orderIdExclude = [];
        if ($search) {
            $orders = [];
            $orderIds = wc_order_search($search);
            $param = [
                'exclude' => $orderIds,
                'return' => 'ids',
                'limit' => -1
            ];
            $orderIdExclude = wc_get_orders($param);
        }
        if ($startDate && $endDate) {
            $startDate = date('Y-m-d', strtotime($startDate));
            $endDate = date('Y-m-d', strtotime($endDate));
            $args['date_created'] = $startDate.'...'.$endDate;
        }
        $args['exclude'] = $orderIdExclude;
        $orders = wc_get_orders($args);

        if (count($orders) > 0) {
            $orderIds = [];
            foreach ($orders as $key => $order) {
                if ($order->parent_id == '') {
                    $orderIds[] = $order->get_id();
                }
            }
            $totalPrice = OrderModel::getTotal($orderIds);
            $detailOrder = OrderModel::getOrder($orderIds);
            foreach ($orders as $key => $order) {
                if ($order->parent_id == '') {
                    //parent_id tidak kosong ketika status refund.
                    $dataOrder = json_decode($order, true);
                    unset($dataOrder['meta_data']);
                    unset($dataOrder['date_modified']);
                    unset($dataOrder['billing']);
                    unset($dataOrder['shipping']);
                    $dataOrder['invoice'] = 'INV/'.date('dmY', strtotime($dataOrder['date_created']['date'])).'/'.$dataOrder['id'];
                    $dataOrder['line_items'] = $this->getProdctFormOrder($order, true);
                    $dataOrder['total'] = isset($totalPrice[$dataOrder['id']]) ? $totalPrice[$dataOrder['id']]->paid_amount : 0;
                    $dataOrder['no_resi'] = $detailOrder[$dataOrder['id']]['courier_resi'] ?? '';
                    $dataOrder['courier_name'] = $detailOrder[$dataOrder['id']]['courier_name'] ?? '';
                    $resp['orders'][] = $dataOrder;
                }
            }

            return $this->sendSuccess($resp, 'Success get orders');
        }

        return $this->sendError([], 'Order Not Found');
    }

    /**
     * api get orders user
     * @Get("/ladara-api/v1/order/{order_id}")
     * @param WP_REST_Request $request
     * @return \WP_REST_Response
     */
    public function getOrderByid(WP_REST_Request $request)
    {
        $token = Jwt::validate_token();
        if ($token['status'] != 200) {
            return $token;
        }
        $id = $request->get_param('id');
        $order = OrderModel::detailOrder($id);

        if (!$order) {
            return $this->sendError([], 'Order Not Found');
        }
        if ($order['user_id'] != $token['data']['user_id']) {
            return $this->sendError([], 'Cannot read this order');
        }
        if ($order) {
            return $this->sendSuccess($order, 'Success get order');
        }

        return $this->sendError([], 'Order Not Found');
    }

    /**
     * APi konfirmasi order
     * @param WP_REST_Request $request
     * @return array|mixed|object|\WP_REST_Response
     */
    public function konfirmasi(WP_REST_Request $request)
    {
        $order_id = $request->get_param('orderId');
        $token = Jwt::validate_token();
        if ($token['status'] != 200) {
            return $token;
        }
        $userId = $token['data']['user_id'];
        $nameUser = $token['data']['display_name'];

        $getOrder = wc_get_order((int)$order_id);
        if ($getOrder) {
            if ($getOrder->get_user_id() != $userId) {
                return $this->sendError([], 'Cannot read this order');
            }

            $order = new \WC_Order($order_id);
            $order->update_status('wc-completed');

            $title_notif = 'Pesanan '.$order_id.' telah selesai.';
            $desc_notif = 'Pesanan telah di konfirmasi sudah sampai dan selesai. Terima Kasih.';
            $dataNotif = [
                'userId' => $userId, //wajib
                'title' => $title_notif, //wajib
                'descriptions' => $desc_notif, //wajib
                'type' => 'pesanan', //pesanan, info, pembayaran, emas // wajib
                'orderId' => $order_id, // optional
                'data' => [] //array optional bisa diisi dengan data lainnya
            ];
            $addNotif = Notifications::addNotification($dataNotif);

            // ==== insert to order log
            date_default_timezone_set("Asia/Jakarta");
            $nowdate = date('Y-m-d H:i:s');
            $order_status = 'completed';
            $log_text = 'Order ID #'.$order_id.', Telah sampai pada tujuan & di konfirmasi oleh '.$nameUser;

            global $wpdb;
            $wpdb->insert('ldr_order_log', [
                'order_id' => $order_id,
                'log' => $log_text,
                'created_date' => $nowdate,
                'order_status' => $order_status,
            ]);

            global $wpdb;
            $wpdb_query = "SELECT * FROM ldr_orders WHERE order_id = '$order_id'";
            $res_query = $wpdb->get_results($wpdb_query, OBJECT);
            $count_res = count($res_query);
            if ($count_res > 0) { // if not exist with same user_id
                global $wpdb;
                $wpdb_query_update = " UPDATE ldr_orders
			              SET order_status = 'wc-completed'
			              WHERE order_id = '$order_id'
			            ";
                $res_query_update = $wpdb->query($wpdb_query_update);
            }

            return $this->sendSuccess([], 'Sucess');
        }

        return $this->sendError([], 'Order Not Found');
    }

    /**
     * fungsi get product from order
     * @param $order
     * @param bool $single true jika hanya mau return 1 image
     * @return array
     */
    private function getProdctFormOrder($order, $single = false)
    {
        $items = $order->get_items();
        $products = [];
        foreach ($items as $key => $item) {
            $product = wc_get_product($item->get_product_id());
            $weight['weight'] = ($product) ? $product->get_weight() * $item->get_quantity() : 0;
            if ($single) {
                $image_id = get_post_thumbnail_id($item->get_product_id());
                $thumb = wp_get_attachment_image_src($image_id, 'medium');
                $image['image'] = get_template_directory_uri().'/library/images/sorry.png';
                if ($thumb) {
                    $image['image'] = $thumb[0];
                }
            } else {
                $image['image'] = (new ProductHelpers())->getProductImage($product);
            }
            $dataItem = $item->get_data();
            $products[] = array_merge($dataItem, $image, $weight);
        }

        return $products;
    }

    /**
     * api create orders user
     * @Post("/ladara-api/v1/create-order")
     * @param WP_REST_Request $request
     * @return \WP_REST_Response
     */
    public function createOrder(WP_REST_Request $request)
    {
        global $wpdb;

        $token = Jwt::validate_token();

        if ($token['status'] != 200) {
            return $token;
        }

        date_default_timezone_set("Asia/Jakarta");
        $currentDate = date('Y-m-d H:i:s');
        $userId = $token['data']['user_id'] ?? 0;
        $userEmail = $token['data']['user_email'] ?? '';
        $addressId = $request->get_param('address_id') ?? 0;
        $shippingNote = $request->get_param('shipping_note') ?? '';
        $shippingCourier = $request->get_param('shipping_courier') ?? 'JNE';
        $shippingService = $request->get_param('shipping_service') ?? 'REG';
        $shippingTotal = $request->get_param('shipping_total') ?? 0;
        $voucherId = $request->get_param('voucher_id') ?? 0;
        $courierName = $shippingCourier.' ( '.$shippingService.' )';
        $carts = $request->get_param('carts');

        $totalAllItem = 0;
        $allCategory = array();

        // ======= Create New WC =======
        $orderStatus = 'wc-pending';
        $orderData = array(
            'status' => $orderStatus,
        );
    
        $order = wc_create_order();
        $orderId = $order->id;

        // ======= Add product item =======
        if ($addressId > 0) {
            $userAddressQuery = "SELECT * FROM ldr_user_address WHERE user_id = '$userId' AND id = '$addressId' ";
            $userAddressResult = $wpdb->get_row($userAddressQuery, OBJECT);

            if ($userAddressResult !== null) {
                $province_code = get_provinceCode($userAddressResult->provinsi); // get from function.php

                $shippingAddress = array(
                    'first_name' => $userAddressResult->nama_penerima,
                    'last_name' => '',
                    'company' => '',
                    'address_1' => $userAddressResult->alamat,
                    'address_2' => $userAddressResult->kecamatan,
                    'city' => $userAddressResult->kota,
                    'state' => $province_code,
                    'postcode' => $userAddressResult->kode_pos,
                    'country' => 'ID',
                    'phone' => $userAddressResult->telepon,
                    'email' => $userEmail
                );

                $orderAddressQuery = "SELECT * FROM ldr_orders_address WHERE  order_id = '$orderId' ";
                $orderAddressResult = $wpdb->get_row($orderAddressQuery, OBJECT);

                if ($orderAddressResult == null) {
                    $wpdb->insert('ldr_orders_address', [
                        'order_id' => $orderId,
                        'nama_penerima' => $userAddressResult->nama_penerima,
                        'telepon_penerima' => $userAddressResult->telepon,
                        'alamat' => $userAddressResult->alamat,
                        'provinsi' => $userAddressResult->provinsi,
                        'kota' => $userAddressResult->kota,
                        'kecamatan' => $userAddressResult->kecamatan,
                        'kode_pos' => $userAddressResult->kode_pos,
                        'latitude' => $userAddressResult->longitude,
                        'nama_toko' => '',
                        'telepon_toko' => '',
                        'created_date' => $currentDate
                    ]);
                }
            } else {
                return $this->sendError('Address not found.', 'Create Order Failed.', 404);
            }
        } else {
            return $this->sendError('Address is required.', 'Create Order Failed.', 422);
        }

        $allProduct = array();
        $saveStock = array();
        $updatedStock = array();
        $totalCount = 0;
        $totalBerat = 0;

        // ============== VALIDASI CART ====================
        $cartIds = array_map(function ($item) {
            return $item['id'];
        }, $carts);

        $cartIdsQuery = implode(', ', $cartIds);
        $checkCartQuery = $wpdb->get_results("SELECT * FROM ldr_cart WHERE user_id = '$userId' AND id in ($cartIdsQuery)", OBJECT);

        if (count($checkCartQuery) !== count($cartIds)) {
            return $this->sendError('Cart not found.', 'Create Order Failed.', 404);
        }
        // ============== VALIDASI CART ====================

        // ======= Add product item =======
        foreach ($carts as $key => $value) {
            $cartId = $value['id'];
            $cartQty = $value['qty'];
            $cartQuery = "SELECT * FROM ldr_cart WHERE user_id = '$userId' AND id = '$cartId'";
            $cartResult = $wpdb->get_row($cartQuery, OBJECT);
            $productId = $cartResult->product_id;
            $product = wc_get_product($productId);

            if ($product) {
                $productSale = $product->get_sale_price();
                $productPrice = $product->get_regular_price();

                if ($productSale !== '') {
                    $productPrice = $productSale;
                }

                $productWeight = $product->get_weight();
                $totalBerat = (int) $totalBerat + (int) $productWeight;
                $priceTotal = (int) $productPrice * (int) $cartQty;
                $totalAllItem = (int) $totalAllItem + (int) $priceTotal;
                $stockId = $cartResult->stock_id;

                $stockQuery = "SELECT * FROM ldr_stock WHERE id = $stockId ";
                $stockResult = $wpdb->get_row($stockQuery, OBJECT);

                if ($stockResult !== null) {
                    $ukuranValue = $stockResult->ukuran_value;
                    $nomorValue = $stockResult->nomor_value;
                    $warnaValue = $stockResult->warna_value;
                    $productQuantity = $stockResult->product_quantity;
                    $saveStock[$stockId] = $warnaValue.' '.$ukuranValue.' '.$nomorValue;

                    // ============== UPDATE STOCK PRODUCT ====================
                    if ($productQuantity > 0) {
                        $newQty = (int) $productQuantity - (int) $cartQty;

                        if ($newQty < 0) {
                            return $this->sendError('Amount exceeded stock.', 'Failed to Create Order.', 500);
                        }

                        $updatedStock[] = [
                            'id' => $stockId,
                            'product_quantity' => $newQty
                        ];
                    }
                    // ============== UPDATE STOCK PRODUCT ====================
                }

                $categories = get_the_terms($productId, 'product_cat');
                foreach ($categories as $category) {
                    $productCatId = $category->term_id;
                    array_push($allCategory, $productCatId);
                }
                    
                $product_qty = $cartQty;
                $finalSale = $productPrice * $product_qty;
                $finalPrice = $productPrice * $product_qty; // no discount
                $priceParams = [
                    'totals' => [
                        'subtotal' => $finalSale,
                        'total' => $finalPrice
                    ]
                ];

                $order->add_product(get_product($productId), $product_qty, $priceParams);

                $komisi_produk = get_field('komisi_produk', $productId);
                if (isset($komisi_produk) and $komisi_produk != '') {
                    $komisi_produk = (int) $komisi_produk;
                } else {
                    $komisi_produk = 0;
                }
                                
                $allProduct[] = [
                    'pro_id' => $productId,
                    'pro_qty' => $cartQty,
                    'stock_id' => $stockId,
                    'pro_price' => $productPrice,
                    'komisi_produk' => $komisi_produk
                ];
            } else {
                return $this->sendError('Product not found.', 'Create Order Failed.', 404);
            }

            $wpdb->delete('ldr_cart', ['id' => $cartId]);
        }
        // ======= Add product item =======

        // ======= Updating Stock =======
        foreach ($updatedStock as $value) {
            $wpdb->update('ldr_stock', ['product_quantity' => $value['product_quantity']], ['id' => $value['id']]);
        }
        // ======= Updating Stock =======

        // ============= USE VOUCHER VALIDATION ============
        $totalOngkir = $shippingTotal;
        $totalDiskon = 0;
        $diskonShipping = 0;
        $diskonTotal = 0;
        $codeCoupon = '';
        $vocErr = 0;

        // ======== set shipping addres =====
        $order->set_address($shippingAddress, 'billing');
        $order->set_address($shippingAddress, 'shipping');
        update_post_meta($order->id, '_customer_user', $userId);
        // ======== set shipping addres =====

        // ======== set payment method =====
        $paymentMethod = 'midtrans';
        $order->set_payment_method($paymentMethod);
        $order->set_payment_method_title($paymentMethod);
        $order->set_transaction_id($orderId);
        $order->set_date_paid($currentDate);

        // Set the array for tax calculations
        $calculateTaxFor = array(
            'country' => 'ID',
            'state' => '', // Can be set (optional)
            'postcode' => '', // Can be set (optional)
            'city' => '', // Can be set (optional)
        );

        // Optionally, set a total shipping amount
        $newShipPrice = $shippingTotal;

        // Get a new instance of the WC_Order_Item_Shipping Object
        $item = new WC_Order_Item_Shipping();

        $shippingName = $courierName;
        $item->set_method_title($shippingName);
        $item->set_method_id("flat_rate:14"); // set an existing Shipping method rate ID
        $item->set_total($newShipPrice); // (optional)
        $item->calculate_taxes($calculateTaxFor);
        $order->add_item($item);
        // ======== set payment method =====

        // ======= save & calculate order =======
        $order->calculate_totals();
        $order->save();
        $order->add_order_note(__('Order #'.$orderId.' was created & status = wc-pending', 'woocommerce'));
        // ======= save & calculate order =======
        // ======== set discount =====
        $totalDiskon = (int) $diskonShipping + (int) $diskonTotal;
        if (isset($totalDiskon) and $totalDiskon > 0 and $totalDiskon != '') {
            $totalDiscount = $totalDiskon;
            // insert discount fix value
            wc_order_add_discount($orderId, __("Voucher : ".$codeCoupon), $totalDiscount);
            // insert discount percentage value
            // wc_order_add_discount( $order_id, __("Discount (5%)"), '5%' );
            $order->add_order_note(__('Order #'.$orderId.' use code voucher : '.$codeCoupon, 'woocommerce'));
        }
        // ======== set discount =====
        // =================== SAVE TO DB MLM ORDERS & ITEMS ====================

        $totalPayment = (int) $totalAllItem + (int) $totalOngkir - (int) $totalDiscount;
        $orderQuery = "SELECT * FROM ldr_orders WHERE order_id = '$orderId' AND user_id = '$userId'";
        $orderResult = $wpdb->get_row($orderQuery, OBJECT);
        $response = [];

        if ($orderResult === null) {
            $wpdb->insert('ldr_orders', [
                'order_id' => $orderId,
                'user_id' => $userId,
                'order_status' => $orderStatus,
                'total_price' => $totalPayment,
                'courier_name' => $courierName,
                'shipping_price' => $newShipPrice,
                'courier_resi' => '',
                'address_id' => $addressId,
                'notes' => $shippingNote,
                'voucher_id' => $voucherId,
                'created_date' => $currentDate
            ]);

            $response = [
                'order_id' => $orderId,
                'amount' => $totalPayment
            ];

            if (isset($allProduct) and !empty($allProduct)) {
                foreach ($allProduct as $key => $value) {
                    $productId = $value['pro_id'];
                    $productQty = $value['pro_qty'];
                    $stockId = $value['stock_id'];
                    $productPrice = $value['pro_price'];
                    $productVariasi = $saveStock[$stockId];
                    $productKomisi = $value['komisi_produk'];

                    $wpdb->insert('ldr_orders_item', [
                        'order_id' => $orderId,
                        'product_id' => $productId,
                        'qty' => $productQty,
                        'price' => $productPrice,
                        'stock_id' => $stockId,
                        'variasi' => $productVariasi,
                        'created_date' => $currentDate,
                        'komisi' => $productKomisi
                    ]);
                }
            }

            // ==== insert to order log
            $orderStatus = 'pending';
            $logText = 'Pembayaran diterima Order ID #'.$orderId.', Status menjadi '.$orderStatus;
            $wpdb->insert('ldr_order_log', [
                'order_id' => $orderId,
                'log' => $logText,
                'order_status' => $orderStatus,
                'created_date' => $currentDate
            ]);
        }

        return $this->sendSuccess($response, 'Create Order Success.', 200);

        die();
    }
}
