<?php
namespace Ladara\Controllers;

if ( ! defined( 'ABSPATH' ) ) {
    exit;
}

use Ladara\Helpers\BaseController;
use Ladara\Helpers\ControllerInterface;
use Ladara\Helpers\Jwt as Token;
use WP_REST_Request;

/**
 * Class Comment
 * @package Ladara\Controllers
 * @author hafiz
 */
class Comment extends BaseController implements ControllerInterface
{

    /**
     * @inheritDoc
     */
    public function register_routes()
    {
        register_rest_route( $this->nameSpace, 'comment', [
            'methods' => 'GET',
            'callback' => [$this,'get_comment'],
        ]);

        register_rest_route( $this->nameSpace, 'comment', [
            'methods' => 'POST',
            'callback' => [$this,'add_comment'],
        ]);
        
    }

    /**
     * api get comment
     * @Get("/ladara-api/v1/comment")
     * @param WP_REST_Request $request
     * @return array|mixed|object|\WP_REST_Response
     */
    public function get_comment(WP_REST_Request $request)
    {
        global $wpdb;

        $product_id = $request->get_param('product_id');
        $page = $request->get_param('page');
        $limit = $request->get_param('limit') ?? 5;
        if ($page > 1){
            $page = ($page * $limit) - $limit;
        }else{
            $page = 0;
        }

        $wpdb_query = "SELECT * FROM eod_comments 
                        WHERE product_id = '$product_id' 
                          AND review_type = '1' 
                          AND reply_id = '0' 
                          order by id desc
                          LIMIT $limit OFFSET $page
                          ";
        $res_query = $wpdb->get_results($wpdb_query, OBJECT);

        $comments = [];

        foreach ($res_query as $key => $value) {
            $dis_id = $value->id;
            $dis_userid = $value->user_id;
            $user_diskusi = get_userdata($dis_userid);
            $display_name = $user_diskusi->display_name;

            // $created_date = date_create($value->created_date);
            // $post_date = date_format($created_date, 'd/m/Y H:i');
            $post_date = $value->created_date;
            $reply_id = $value->reply_id;
            $comment = $value->comment;

            $user_avatar1 = get_field('user_avatar', 'user_'.$dis_userid);

            if(isset($user_avatar1) AND $user_avatar1 != ''){
                $url_prof_photo = wp_get_attachment_image_url($user_avatar1);
            }else{
                $url_prof_photo = get_template_directory_uri().'/library/images/icon_profile.png';
            }

            $comments[] = [
                "id" => $dis_id,
                "name" => $display_name,
                "avatar" => $url_prof_photo,
                "created_date" => $post_date,
                "comment" => $comment,
                "reply" => $this->recursive_reply($product_id, $dis_id)
            ];
        }

        return $this->sendSuccess($comments, 'Success get comments');
    }
    
    /**
     * api add comment
     * @Post("/ladara-api/v1/comment")
     * @param WP_REST_Request $request
     * @return array|mixed|object|\WP_REST_Response
     */
    public function add_comment(WP_REST_Request $request)
    {
        global $wpdb;

        $product_id = $request->get_param('product_id');
        $comment = $request->get_param('comment');
        $reply_id = $request->get_param('reply_id');
        
        
        $token = Token::validate_token();
        if($token['status'] != 200){
            return $token;
        }
        
        $user_id = $token['data']['user_id'];
        
        if(!$product_id || !$comment){
            return $this->sendError([], 'product_id, comment, and reply_id is needed', 401);
        }

        //Insert into custom database
        $insert_result = $wpdb->insert('eod_comments', array(
            'product_id' => $product_id,
            'user_id' => $user_id,
            'created_date' => date('Y-m-d H:i:s'),
            'review_type' => 1,
            'reply_id' => $reply_id ?? 0,
            'comment' => $comment,
            'open' => 0
        ));

        if($insert_result === false){
            return $this->sendError([], 'Error on database insert comment', 403);
        }
        return $this->sendSuccess([], 'Success add comment');
    }


    function recursive_reply($product_id, $dis_id){

        global $wpdb;

        $wpdb_query = "SELECT * FROM eod_comments WHERE product_id = '$product_id' AND reply_id = $dis_id ";
        $res_query = $wpdb->get_results($wpdb_query, OBJECT);

        $comments = array();

        foreach ($res_query as $key => $value) {
            $dis_id = $value->id;
            $dis_userid = $value->user_id;
            $user_diskusi = get_userdata($dis_userid);
            $display_name = $user_diskusi->display_name;

            // $created_date = date_create($value->created_date);
            // $post_date = date_format($created_date, 'd/m/Y H:i');
            $post_date = $value->created_date;
            $reply_id = $value->reply_id;
            $comment = $value->comment;

            $user_avatar1 = get_field('user_avatar', 'user_'.$dis_userid);

            if(isset($user_avatar1) AND $user_avatar1 != ''){
                $url_prof_photo = wp_get_attachment_image_url($user_avatar1);
            }else{
                $url_prof_photo = get_template_directory_uri().'/library/images/icon_profile.png';
            }

            $dis = array(
                "id" => $dis_id,
                "name" => $display_name,
                "avatar" => $url_prof_photo,
                "created_date" => $post_date,
                "comment" => $comment,
                "reply" => $this->recursive_reply($product_id, $dis_id)
            );

            array_push($comments, $dis);
        }
        
        return $comments;
    }


    
}