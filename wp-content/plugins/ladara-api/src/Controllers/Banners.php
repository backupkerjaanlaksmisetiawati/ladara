<?php


namespace Ladara\Controllers;
if ( ! defined( 'ABSPATH' ) ) {
    exit;
}

use Ladara\Helpers\BaseController;
use Ladara\Helpers\ControllerInterface;
use Ladara\Helpers\Jwt as Token;
use WP_Query;
use WP_REST_Request;
use Ladara\Models\Banners as BannerModel;

/**
 * Class Banners
 * @package Ladara\Controllers
 * @author rikihandoyo
 */
class Banners extends BaseController implements ControllerInterface
{

    /**
     * @inheritDoc
     */
    public function register_routes()
    {
        register_rest_route( $this->nameSpace, 'banners', [
            'methods' => 'GET',
            'callback' => [$this,'banners'],
        ]);

        register_rest_route( $this->nameSpace, 'banner/(?P<id>\d+)', [
            'methods' => 'GET',
            'callback' => [$this,'bannerDetail'],
        ]);

        register_rest_route( $this->nameSpace, 'brands', [
            'methods' => 'GET',
            'callback' => [$this,'getBrands'],
        ]);

        register_rest_route( $this->nameSpace, 'seller', [
            'methods' => 'GET',
            'callback' => [$this,'getSeller'],
        ]);

        register_rest_route( $this->nameSpace, 'promo', [
            'methods' => 'GET',
            'callback' => [$this,'promo'],
        ]);

        register_rest_route( $this->nameSpace, 'promo/(?P<id>\d+)', [
            'methods' => 'GET',
            'callback' => [$this,'promoDetail'],
        ]);

        register_rest_route( $this->nameSpace, 'kategori-pilihan', [
            'methods' => 'GET',
            'callback' => [$this,'kategoriPilihan'],
        ]);
    }

    /**
     * Get all banners
     * @Get("/ladara-api/v1/banners")
     * @param WP_REST_Request $request
     * @return \WP_REST_Response
     */
    public function banners(WP_REST_Request $request)
    {
        $a = BannerModel::getAll();
        return $this->sendSuccess($a,'Success get banners');
    }

    /**
     * Get banner detail
     * @Get("/ladara-api/v1/banner/{banner_id}")
     * @param WP_REST_Request $request
     * @return \WP_REST_Response
     */
    public function bannerDetail(WP_REST_Request $request)
    {
        $id = $request->get_param('id');
        $limit = $request->get_param('limit');
        $page = $request->get_param('page');
        $a = BannerModel::getDetail($id, 0, $limit, $page);
        return $this->sendSuccess($a,'Success get banners');
    }

    /**
     * Get all brands
     * @Get("/ladara-api/v1/brands")
     * @param WP_REST_Request $request
     * @return \WP_REST_Response
     */
    public function getBrands(WP_REST_Request $request)
    {
        $showBrand = $request->get_param('show') ?? true;
        $limit = $request->get_param('limit') ?? get_option('ldr_limit_brand_pilihan');
        $page = $request->get_param('page');
        $filter = $request->get_param('filter');
        $withProduct = $request->get_param('with_product') ?? true;

        $a = BannerModel::getBrands($limit, (bool)$showBrand, $page, $filter, $withProduct);
        return $this->sendSuccess($a,'Success get brands');
    }

    /**
     * Get promo
     * @Get("/ladara-api/v1/promo")
     * @param WP_REST_Request $request
     * @return \WP_REST_Response
     */
    public function promo(WP_REST_Request $request)
    {
        $type = $request->get_param('type');
        $limit = $request->get_param('limit');

        $a = BannerModel::getPromo($type,$limit);
        return $this->sendSuccess($a,'Success get promo');
    }

    /**
     * Get promo detail
     * @Get("/ladara-api/v1/promo")
     * @param WP_REST_Request $request
     * @return \WP_REST_Response
     */
    public function promoDetail(WP_REST_Request $request)
    {
        $id = $request->get_param('id');
        $limit = $request->get_param('limit');
        $page = $request->get_param('page');

        $token = Token::validate_token();
        $userId = $token['data']['user_id'] ?? 0;
        $result = BannerModel::getDetail($id, $userId, $limit, $page);
        return $this->sendSuccess($result,'Success get promo');
    }

    public function kategoriPilihan()
    {
        $post = get_post_meta(2,'kategoripilihan',true);
        $resp = [];
        foreach ($post as $key => $row){
            if ($key < get_option('ldr_limit_kategori_pilihan',7)){
                $url = $row['kategori-url'];
                $imageId = $row['kategori-photo'];

                $urlEx = explode('=',$url);
                $slug = $urlEx[1] ?? '';
                $cate = get_term_by('slug', $slug,'product_cat', 'ARRAY_A');

                $linkpoto = wp_get_attachment_image_src($imageId,'full');
                if($linkpoto){
                    $urlphoto = $linkpoto[0];
                }else{
                    $urlphoto = get_template_directory_uri().'/library/images/default_banner.jpg';
                }
                $resp[] = [
                    'name' => $row['kategori-nama'],
                    'cate_id' => $cate['term_id'],
                    'type' => ($cate) ? 'category' : 'emas',
                    'image' =>$urlphoto
                ];
            }

        }
        return $this->sendSuccess($resp,'Success');
    }

    /**
     * get list seller
     * @param WP_REST_Request $request
     * @return \WP_REST_Response
     */
    public function getSeller(WP_REST_Request $request)
    {
        $resp = BannerModel::getSeller();
        return $this->sendSuccess($resp,'Success get brands');
    }
}