<table>
    <tr>
        <td>
            <div class="success" style="text-align: left;">
                <h4>Selamat datang di Ladara, <?php echo $data['user']->getName() ?? ''?>.</h4>
                <p>
                    Anda menerima pesan ini karena baru-baru ini mendaftar untuk akun Ladara.
                </p>
                <p>
                    Konfirmasikan alamat email Anda dengan mengklik tombol di bawah ini. Langkah ini menambah keamanan ekstra untuk bisnis Anda dengan memverifikasi Anda memiliki email ini.
                </p>
                <p style="text-align: center">
                    <a href="<?php echo $data['link'] ?? '' ?>"
                        target="_blank" shape="rect"
                       style="
                    background: #ffffff;
                    text-decoration: none;
                    padding: 10px 20px;
                    border:1.5px solid #0080FF;
                    line-height: 43px;
                    font-size: 16px;
                    color: #000000;
                    font-weight: 600;
                    border-radius: 20px;
                    letter-spacing: .5px;
                    margin-top: 15px;">
                        Konfirmasi Email
                    </a>
                </p>

                <p>
                    Tautan ini akan kedaluwarsa dalam 24 jam. Jika Anda memiliki pertanyaan, kami di sini untuk membantu.
                </p>
            </div>
        </td>
    </tr>
</table>