<table>
    <tr>
        <td colspan="2">
            <div class="success" style="text-align: left;">
                <h3>Pesanan Anda telah kami teruskan ke Penjual.</h3>
                <?php
                    if ($data['payment']->getBank() === 'CREDIT_CARD') {
                        ?>
                        <p>Terima kasih telah menyelesaikan transaksi di Ladara. Pembayaran menggunakan Credit Card berhasil.</p>
                        <?php
                    } else {
                        ?>
                        <p>Terima kasih telah menyelesaikan transaksi di Ladara. Pembayaran menggunakan Virtual Account berhasil.</p>
                        <?php
                    }
                ?>
            </div>
        </td>
    </tr>
    <tr>
        <td style="font-weight: bold;">Total Pembayaran</td>
        <td style="font-weight: bold;">Referensi Pembayaran</td>
    </tr>
    <tr>
        <td><?php echo $data['order']->getTotalPrice() ?></td>
        <td><?php echo $data['order']->getInvoiceNo() ?></td>
    </tr>
    <tr>
        <td style="height: 20px;"></td>
    </tr>
    <tr>
        <td style="font-weight: bold;">Metode Pembayaran</td>
        <td style="font-weight: bold;">Waktu Pembayaran</td>
    </tr>
    <tr>
        <td><?php echo $data['payment']->getBank() ?></td>
        <td><?php echo $data['order']->getPaymentDate() ?></td>
    </tr>
    <tr>
        <td style="height: 20px;"></td>
    </tr>
    <tr>
        <td colspan="2">
            Status Pembayaran <br>
            <strong><?php echo $data['order']->getStatus() ?></strong>
        </td>
    </tr>
    <tr>
        <td colspan="2" style="text-align: center;">
            <a href="<?php echo get_site_url().'/my-order'?>"
                target="_blank" shape="rect"
               style="
                    background: #ffffff;
                    text-decoration: none;
                    padding: 10px 20px;
                    border:1.5px solid #0080FF;
                    line-height: 43px;
                    font-size: 16px;
                    color: #000000;
                    font-weight: 600;
                    border-radius: 20px;
                    letter-spacing: .5px;
                    margin-top: 15px;">
                Cek Status Pesanan
            </a>
        </td>
    </tr>
    <tr>
        <td style="height: 10px;"></td>
    </tr>
    <tr>
        <td  colspan="2" style="height: 20px;border-top: 1px solid #d0cdcd;"></td>
    </tr>
    <tr>
        <td colspan="2">
            <h3>Product Yang Dibeli</h3>
        </td>
    </tr>
    <?php foreach ($data['order']->getItems() as $row) :?>
        <tr>
            <td><?php echo $row['name'] ?? ''?> x <?php echo $row['qty'] ?? ''?></td>
            <td style="font-weight: bold;"><?php echo isset($row['price']) ? 'Rp '.number_format($row['price'],0,'','.') : '' ?></td>
        </tr>
    <?php endforeach ?>
    <tr>
        <td><?php echo $data['shipping']->getCourier(); ?>(<?php echo $data['shipping']->getService(); ?>)</td>
        <td style="font-weight: bold;"><?php echo $data['shipping']->getPrice(); ?></td>
    </tr>
    <tr>
        <td colspan="2" style="height: 20px;border-top: 1px solid #d0cdcd;"></td>
    </tr>
    <tr>
        <td style="font-weight: bold;">Total pembayaran</td>
        <td style="font-weight: bold;"><?php echo $data['order']->getTotalPrice(); ?></td>
    </tr>
    <tr>
        <td style="height: 10px;"></td>
    </tr>

</table>