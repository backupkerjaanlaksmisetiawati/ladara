<table>
    <tr>
        <td>
            <div class="success" style="text-align: left;">
                <h4>Kata sandi Anda baru saja diubah pada <?php echo date('d F Y H:i:s')?></h4>
                <p>
                    Jika hal ini bukan atas sepengetahuan Anda, silahkan hubungi kami di cs@ladara.id
                </p>
                <p>
                    Kalau semuanya sudah lancar – Ayuk <a target="_blank" shape="rect" href="<?php echo get_site_url() ?>">Login</a> dan mulai belanja!  
                </p>

                <p>
                    Kenyamanan Anda menggunakan Ladara adalah tujuan kami. Sampaikan saran dan kritik Anda agar kami dapat senantiasa memperbaharui aplikasi Ladara, demi pengalaman jual beli yang lebih baik lagi.
                </p>
            </div>
        </td>
    </tr>
</table>