<table>
    <tr>
        <td colspan="2">
            <div class="success" style="text-align: left;">
                <h3>Pembayaran melalui <?php echo $data['payment']->getBank() ?> dibatalkan</h3>
            </div>
        </td>
    </tr>
    <tr>
        <td colspan="2">
            <div class="success" style="text-align: left;font-size: 16px;">
                <p>Pembayaran anda telah kami batalkan
                    <br>
                    <span style="color: red;font-weight: bold;">Harap tidak membayar pesanan ini</span>
                </p>
            </div>
        </td>
    </tr>
    <tr>
        <td style="font-weight: bold;">Total Pembayaran</td>
        <td style="font-weight: bold;">Referensi pembayaran</td>
    </tr>
    <tr>
        <td><?php echo $data['order']->getTotalPrice() ?></td>
        <td><?php echo $data['order']->getInvoiceNo() ?></td>
    </tr>
    <tr>
        <td style="height: 20px;"></td>
    </tr>
    <tr>
<<<<<<< HEAD
        <td style="font-weight: bold;">Metode Pembayaran</td>
        <td style="font-weight: bold;">Waktu Pembayaran</td>
=======
        <td style="font-weight: bold;">Metode Pembayran</td>
>>>>>>> 6e166f86af73f20a0dcc68951c4cb40c1a04f320
    </tr>
    <tr>
        <td><?php echo $data['payment']->getBank() ?></td>
    </tr>
    <tr>
        <td style="height: 20px;"></td>
    </tr>
    <tr>
        <td colspan="2">
            Status Pembayaran <br>
            <strong><?php echo $data['order']->getStatus() ?></strong>
        </td>
    </tr>

    <tr>
        <td colspan="2">
            * Apabila Anda menerima email ini namun pembayaran telah Anda lakukan, mohon lampirkan bukti bayar di halaman Kontak Kami
        </td>
    </tr>
    <tr>
        <td style="height: 10px;"></td>
    </tr>
    <tr>
        <td  colspan="2" style="height: 20px;border-top: 1px solid #d0cdcd;"></td>
    </tr>
    <tr>
        <td colspan="2">
            <h3>Product Yang Dibeli</h3>
        </td>
    </tr>
    <?php foreach ($data['order']->getItems() as $row) :?>
        <tr>
            <td><?php echo $row['name'] ?? ''?> x <?php echo $row['qty'] ?? ''?></td>
            <td style="font-weight: bold;"><?php echo isset($row['price']) ? 'Rp '.number_format($row['price'],0,'','.') : '' ?></td>
        </tr>
    <?php endforeach ?>
    <tr>
        <td><?php echo $data['shipping']->getCourier(); ?>(<?php echo $data['shipping']->getService(); ?>)</td>
        <td style="font-weight: bold;"><?php echo $data['shipping']->getPrice(); ?></td>
    </tr>
    <tr>
        <td colspan="2" style="height: 20px;border-top: 1px solid #d0cdcd;"></td>
    </tr>
    <tr>
        <td style="font-weight: bold;">Total pembayaran</td>
        <td style="font-weight: bold;"><?php echo $data['order']->getTotalPrice(); ?></td>
    </tr>
    <tr>
        <td style="height: 10px;"></td>
    </tr>
    <tr>
        <td colspan="2" style="text-align: center;">
            <a href="<?php echo get_site_url()?>"
                target="_blank" shape="rect"
               style="
                    background: #ffffff;
                    text-decoration: none;
                    padding: 10px 20px;
                    border:1.5px solid #0080FF;
                    line-height: 43px;
                    font-size: 16px;
                    color: #000000;
                    font-weight: 600;
                    border-radius: 20px;
                    letter-spacing: .5px;
                    margin-top: 15px;">
                Checkout Ulang
            </a>
        </td>
    </tr>

</table>