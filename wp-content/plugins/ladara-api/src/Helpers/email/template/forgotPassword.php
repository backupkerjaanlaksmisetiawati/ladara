<table>
    <tr>
        <td>
            <div class="success" style="text-align: left;">
                <h4>Hello, <strong><?php echo $data['user']->getName() ?? ''?></strong>.</h4>
                <p>
                    Kami telah menerima permintaan untuk atur ulang kata sandi
                </p>
                <p>
                    Untuk mengatur ulang, silahkan klik dibawah
                </p>
                <p style="text-align: center">
                    <a href="<?php echo $data['link'] ?? '' ?>"
                        target="_blank" shape="rect"
                       style="
                    background: #ffffff;
                    text-decoration: none;
                    padding: 10px 20px;
                    border:1.5px solid #0080FF;
                    line-height: 43px;
                    font-size: 16px;
                    color: #000000;
                    font-weight: 600;
                    border-radius: 20px;
                    letter-spacing: .5px;
                    margin-top: 15px;">
                        Reset Password
                    </a>
                </p>

                <p>
                    <strong>SELANJUTNYA?</strong> Anda akan menerima email konfirmasi mengenai kata sandi Anda yang baru
                </p>
            </div>
        </td>
    </tr>
</table>