<table>
    <tr>
        <td>
            <div class="success" style="text-align: left;">
                <h4>Halo <?= $data['username'] ?>,</h4>
                <p>
                    Jika anda menerima email dari noreply@ladara.id yang berisi subject "Password anda telah diubah." pada hari Jumat, 29 Mei 2020, <b>mohon untuk di abaikan</b>.
                </p>
                <p>
                    Saat ini kami masih dalam tahap pengembangan, sehingga belum dapat digunakan untuk bertransaksi.
                </p>
                <p>
                    Mohon maaf untuk ketidaknyamanan yang terjadi.
                </p>
                <p>
                    Demikian kami informasikan.
                </p>
                <p>
                    Terima kasih.
                </p>
            </div>
        </td>
    </tr>
</table>