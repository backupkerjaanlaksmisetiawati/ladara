<table>
    <tr>
        <td>
            <div class="success" style="text-align: left;">
                <h4>Hello, <strong><?php echo $data['user']->getName() ?? ''?></strong>.</h4>
                <p>
                    Kami telah menerima permintaan untuk atur ulang kata sandi
                </p>
                <p>
                    Untuk mengatur ulang, silahkan klik dibawah
                </p>
                <p>
                    Password Anda : <b><?php echo $data['password'] ?? '' ?></b>
                </p>
                <p>
                    Silahkan merubah password Anda pada halaman ubah kata sandi pada menu pengaturan. <br/>
                    Terima kasih selalu bersama Ladara Indonesia, Ayo mulai menjelajah sekarang!
                </p>
            </div>
        </td>
    </tr>
</table>