<table cellpadding="0" cellspacing="0" align="left" border="0" style="width:100%; float:none;">
  <tr>
    <?php /*<td style="background-color:#efefef; padding:3%;">*/ ?>
    <td>
      <table cellpadding="0" cellspacing="0" align="left" border="0" style="background-color:#ffffff; width:100%; float:none;">
        <?php /*<tr>
          <td align="left" style="font-family:Verdana; padding:20px; text-align:center;">
            <img src="<?php echo get_template_directory_uri(); ?>/library/images/main_logo.png" style="width:130px;" />
          </td>
        </tr>*/ ?>

        <tr>
          <td style="padding:3%;">
            <p style="font-size:12px; margin:0px 0px 10px 0px; font-family:sans-serif;">
              Halo <?php echo $data["customer_name"]; ?>!
            </p>
            
            <h2 style="margin:0px 0px 10px 0px; font-family:sans-serif; font-size:14px;">
              Mohon segera,<br />selesaikan pembayaran kamu
            </h2>

            <p style="font-size:12px; margin:0px 0px 10px 0px; font-family:sans-serif;">
              Kamu telah melakukan permintaan invoice pada waktu
              <strong style="color:#0080FF;"><?php echo date("j", strtotime($data["created_date"])) . " " . month_indonesia(date("n", strtotime($data["created_date"]))) . " " . date("Y", strtotime($data["created_date"])) . " " . date("H:i", strtotime($data["created_date"])); ?> WIB</strong>
              dengan info sebagai berikut:
            </p>

            <table cellpadding="0" cellspacing="0" align="left" border="0" style="width:100%; float:none; margin-bottom:40px;margin-top:30px;">
              <tr>
                <td style="padding:4px 13px 4px 0px; width:40%;">
                  <p style="font-size:12px; margin:0px; font-family:sans-serif;">
                    Nomor Invoice
                  </p>
                </td>
                <td style="padding:4px 0px 4px 0px;">
                  <p style="font-size:12px; margin:0px; font-family:sans-serif; padding:0px 10px;">
                    :
                  </p>
                </td>
                <td style="padding:4px 0px 4px 0px;">
                  <p style="font-size:12px; margin:0px; font-family:sans-serif; word-break:break-all;">
                    <strong style="color:#0080FF;"><?php echo $data["invoice_order"]; ?></strong>
                  </p>
                </td>
              </tr>

              <tr>
                <td style="padding:4px 13px 4px 0px;">
                  <p style="font-size:12px; margin:0px; font-family:sans-serif;">
                    Metode Pembayaran
                  </p>
                </td>
                <td style="padding:4px 0px 4px 0px;">
                  <p style="font-size:12px; margin:0px; font-family:sans-serif; padding:0px 10px;">
                    :
                  </p>
                </td>
                <td style="padding:4px 0px 4px 0px;">
                  <p style="font-size:12px; margin:0px; font-family:sans-serif;">
                    <strong style="color:#0080FF;"><?php echo $data["metode_pembayaran"]; ?></strong>
                  </p>
                </td>
              </tr>

              <tr>
                <td style="padding:4px 13px 4px 0px;">
                  <p style="font-size:12px; margin:0px; font-family:sans-serif;">
                    Batas Waktu Pembayaran
                  </p>
                </td>
                <td style="padding:4px 0px 4px 0px;">
                  <p style="font-size:12px; margin:0px; font-family:sans-serif; padding:0px 10px;">
                    :
                  </p>
                </td>
                <td style="padding:4px 0px 4px 0px;">
                  <p style="font-size:12px; margin:0px; font-family:sans-serif;">
                    <strong style="color:#0080FF;"><?php echo date("j", strtotime($data["due_date"])) . " " . month_indonesia(date("n", strtotime($data["due_date"]))) . " " . date("Y", strtotime($data["due_date"])) . " " . date("H:i", strtotime($data["due_date"])); ?> WIB</strong>
                  </p>
                </td>
              </tr>

              <tr>
                <td style="padding:4px 13px 4px 0px;">
                  <p style="font-size:12px; margin:0px; font-family:sans-serif;">
                    Nilai Beli Emas
                  </p>
                </td>
                <td style="padding:4px 0px 4px 0px;">
                  <p style="font-size:12px; margin:0px; font-family:sans-serif; padding:0px 10px;">
                    :
                  </p>
                </td>
                <td style="padding:4px 0px 4px 0px;">
                  <p style="font-size:12px; margin:0px; font-family:sans-serif;">
                    Rp <?php echo number_format(round($data["total_price"], 0, PHP_ROUND_HALF_UP), 0, ".", "."); ?>
                  </p>
                </td>
              </tr>

              <tr>
                <td style="padding:4px 13px 4px 0px;">
                  <p style="font-size:12px; margin:0px; font-family:sans-serif;">
                    Jumlah Emas
                  </p>
                </td>
                <td style="padding:4px 0px 4px 0px;">
                  <p style="font-size:12px; margin:0px; font-family:sans-serif; padding:0px 10px;">
                    :
                  </p>
                </td>
                <td style="padding:4px 0px 4px 0px;">
                  <p style="font-size:12px; margin:0px; font-family:sans-serif;">
                    <strong style="color:#0080FF;"><?php echo str_replace(".", ",", round($data["total_emas"], 4, PHP_ROUND_HALF_UP)); ?> gram</strong>
                  </p>
                </td>
              </tr>

              <tr>
                <td style="padding:4px 13px 4px 0px;">
                  <p style="font-size:12px; margin:0px; font-family:sans-serif;">
                    Biaya Pemesanan Emas
                  </p>
                </td>
                <td style="padding:4px 0px 4px 0px;">
                  <p style="font-size:12px; margin:0px; font-family:sans-serif; padding:0px 10px;">
                    :
                  </p>
                </td>
                <td style="padding:4px 0px 4px 0px;">
                  <p style="font-size:12px; margin:0px; font-family:sans-serif;">
                    Rp <?php echo number_format(round($data["booking_fee"], 0, PHP_ROUND_HALF_UP), 0, ".", "."); ?>
                  </p>
                </td>
              </tr>

              <tr>
                <td style="padding:4px 13px 4px 0px;">
                  <p style="font-size:12px; margin:0px; font-family:sans-serif;">
                    Pajak
                  </p>
                </td>
                <td style="padding:4px 0px 4px 0px;">
                  <p style="font-size:12px; margin:0px; font-family:sans-serif; padding:0px 10px;">
                    :
                  </p>
                </td>
                <td style="padding:4px 0px 4px 0px;">
                  <p style="font-size:12px; margin:0px; font-family:sans-serif;">
                    Rp <?php echo number_format(round($data["tax"], 0, PHP_ROUND_HALF_UP), 0, ".", "."); ?>
                  </p>
                </td>
              </tr>

              <tr>
                <td style="padding:4px 13px 4px 0px;">
                  <p style="font-size:12px; margin:0px; font-family:sans-serif;">
                    Biaya Admin
                  </p>
                </td>
                <td style="padding:4px 0px 4px 0px;">
                  <p style="font-size:12px; margin:0px; font-family:sans-serif; padding:0px 10px;">
                    :
                  </p>
                </td>
                <td style="padding:4px 0px 4px 0px;">
                  <p style="font-size:12px; margin:0px; font-family:sans-serif;">
                    Rp <?php echo number_format(round($data["partner_fee"], 0, PHP_ROUND_HALF_UP), 0, ".", "."); ?>
                  </p>
                </td>
              </tr>

              <tr>
                <td style="padding:4px 13px 4px 0px;">
                  <p style="font-size:12px; margin:0px; font-family:sans-serif;">
                    Biaya Bank
                  </p>
                </td>
                <td style="padding:4px 0px 4px 0px;">
                  <p style="font-size:12px; margin:0px; font-family:sans-serif; padding:0px 10px;">
                    :
                  </p>
                </td>
                <td style="padding:4px 0px 4px 0px;">
                  <p style="font-size:12px; margin:0px; font-family:sans-serif;">
                    Rp <?php echo number_format(round($data["xendit_fee"], 0, PHP_ROUND_HALF_UP), 0, ".", "."); ?>
                  </p>
                </td>
              </tr>

              <tr>
                <td style="padding:4px 13px 4px 0px;">
                  <p style="font-size:12px; margin:0px; font-family:sans-serif;">
                    Jumlah
                  </p>
                </td>
                <td style="padding:4px 0px 4px 0px;">
                  <p style="font-size:12px; margin:0px; font-family:sans-serif; padding:0px 10px;">
                    :
                  </p>
                </td>
                <td style="padding:4px 0px 4px 0px;">
                  <p style="font-size:12px; margin:0px; font-family:sans-serif;">
                    <strong style="color:#0080FF;">Rp <?php echo number_format(round($data["total_payment"], 0, PHP_ROUND_HALF_UP), 0, ".", "."); ?></strong>
                  </p>
                </td>
              </tr>
            </table>

            <?php if(isset($data["no_va"])) { ?>
              <h2 style="margin:0px 0px 10px 0px; font-size:17px; font-family:sans-serif; text-align:center; line-height:25px; font-weight:normal">
                Kode pembayaran virtual account kamu adalah:
                <br />
                <strong style="color:#0080FF;">
                  <?php echo $data["no_va"]; ?>
                </strong>
              </h2>

              <p style="margin:0px; font-family:sans-serif; font-size:11px;">
                *apabila Anda masih memiliki tagihan <?php echo $data["metode_pembayaran"]; ?> yang belum terbayar, maka transaksi sebelumnya akan kami batalkan secara otomatis.
              </p>

              <?php if(isset($data["va_instructions"])) { ?>
                <h3 style="font-size:14px; font-family:sans-serif; margin:30px 0px 10px 0px;">
                  Langkah-langkah pembayaran dengan <?php echo $data["metode_pembayaran"]; ?>:
                </h3>

                <?php foreach($data["va_instructions"] as $key => $instruction) { ?>
                  <?php 
                    $title_string = $key;
                    $title = preg_replace('/(?<!\ )[A-Z]/', ' $0', $title_string);
                  ?>
                  
                  <h4 style="font-size:13px; font-family:sans-serif; margin:30px 0px 10px 0px;">
                    <?php echo strtoupper($title); ?>
                  </h4>
                  
                  <ul style="font-size:12px; font-family:sans-serif; padding-left: 25px; line-height:22px; margin:0px 0px 10px 0px;">
                    <?php foreach($instruction as $step) { ?>
                      <li>
                        <?php echo str_replace("{{- vaNumber}}", $data["no_va"], str_replace("{{companyCode}}", $data["xen_company_code"], str_replace("{{companyName}}", $data["xen_merchant_name"], $step->step))); ?>
                      </li>
                    <?php } ?>
                  </ul>
                <?php } ?>
              <?php } ?>
            <?php } ?>

            <?php if(isset($data["xendit_url"])) { ?>
              <h2 style="margin:0px 0px 10px 0px; font-size:17px; font-family:sans-serif; text-align:center; font-weight:normal">
                Silahkan klik button di bawah ini untuk melakukan pembayaran:
                <br />
                <br />
                <a href="<?php echo $data["xendit_url"]; ?>" style="background-color:#0080FF; color:#ffffff; text-decoration:none; padding:10px 60px; border-radius:20px; display:inline-block;">
                  Bayar Sekarang
                </a>
              </h2>
            <?php } ?>

            <?php if(isset($data["xendit_payment_code"])) { ?>
              <h2 style="margin:0px 0px 10px 0px; font-size:17px; font-family:sans-serif; text-align:center; line-height:25px; font-weight:normal">
                Kode pembayaran <?php echo ucfirst(strtolower($data["metode_pembayaran"])); ?> kamu adalah:
                <br />
                <strong style="color:#0080FF;">
                  <?php echo $data["xendit_payment_code"]; ?>
                </strong>
              </h2>

              <p style="margin:0px; font-family:sans-serif; font-size:11px;">
                *apabila Anda masih memiliki tagihan <?php echo ucfirst(strtolower($data["metode_pembayaran"])); ?> yang belum terbayar, maka transaksi sebelumnya akan kami batalkan secara otomatis.
              </p>
            <?php } ?>

            <p style="margin:40px 0px 0px 0px; font-size:12px; font-family:sans-serif;">
              Pantau status pembayaran transaksi emasmu pada halaman
              <a href="<?php echo home_url(); ?>/emas/riwayat-transaksi" style="color:#FE5461; text-decoration:none;">
                <strong>Riwayat Transaksi</strong>
              </a>
            </p>
          </td>
        </tr>
      </table>
    </td>
  </tr>
</table>