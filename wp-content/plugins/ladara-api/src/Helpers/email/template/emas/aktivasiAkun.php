<table cellpadding="0" cellspacing="0" align="left" border="0" style="background-color:#ffffff; width:100%; float:none;">
  <tr>
    <td style="padding:3%;">
      <h2 style="margin:0px 0px 10px 0px; font-family:sans-serif; font-size:14px;">
        Selamat datang di Ladara Emas, akun kamu sudah aktif.
      </h2>
      
      <p style="margin:0px 0px 10px 0px; font-family:sans-serif; font-size:12px;">
        Terima kasih atas pendaftaran yang sudah dilakukan.
      </p>

      <p style="margin:0px 0px 10px 0px; font-family:sans-serif; font-size:12px;">
        Informasi akun kamu sebagai berikut:
      </p>

      <p style="margin:0px 0px 10px 0px; font-family:sans-serif; font-size:12px; line-height:16px;">
        Nama: <?php echo $data["customer_name"]; ?>
        <br />
        Email: <?php echo $data["customer_email"]; ?>
        <br />
      </p>

      <p style="margin:0px 0px 10px 0px; font-family:sans-serif; font-size:12px;">
        Kamu dapat mulai menabung emas sekarang.
      </p>

      <p style="margin:0px 0px 10px 0px; font-family:sans-serif; text-align:center; font-size:14px;">
        Klik tombol di bawah ini untuk mulai menabung emas:
        <br />
        <br />
        <a href="<?php echo home_url(); ?>/emas" style="background-color:#0080FF; color:#ffffff; text-decoration:none; padding:10px 60px; border-radius:20px; display:inline-block;">
          Nabung Emas Sekarang
        </a>
      </p>
    </td>
  </tr>
</table>