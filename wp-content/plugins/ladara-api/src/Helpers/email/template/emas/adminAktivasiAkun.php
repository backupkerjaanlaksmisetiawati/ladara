<table cellpadding="0" cellspacing="0" align="left" border="0" style="background-color:#ffffff; width:100%; float:none;">
  <tr>
    <td style="padding:3%;">
      <p style="margin:0px 0px 10px 0px; font-family:sans-serif; font-size:12px;">
        Customer dengan informasi berikut:
      </p>

      <p style="margin:0px 0px 10px 0px; font-family:sans-serif; font-size:12px; line-height:16px;">
        Nama: <?php echo $data["customer_name"]; ?>
        <br />
        Email: <?php echo $data["customer_email"]; ?>
        <br />
      </p>

      <p style="margin:0px 0px 10px 0px; font-family:sans-serif; font-size:12px;">
        Telah mengaktifkan akun Ladara Emas.
      </p>
    </td>
  </tr>
</table>