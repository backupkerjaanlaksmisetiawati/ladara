<table cellpadding="0" cellspacing="0" align="left" border="0" style="width:100%; float:none;">
  <tr>
    <?php /*<td style="background-color:#efefef; padding:3%;">*/ ?>
    <td>
      <table cellpadding="0" cellspacing="0" align="left" border="0" style="background-color:#ffffff; width:100%; float:none;">
        <?php /*<tr>
          <td align="left" style="font-family:Verdana; padding:20px; text-align:center;">
            <img src="<?php echo get_template_directory_uri(); ?>/library/images/main_logo.png" style="width:130px;" />
          </td>
        </tr>*/ ?>

        <tr>
          <td style="padding:3%;">
            <p style="font-size:12px; margin:0px 0px 10px 0px; font-family:sans-serif;">
              <?php echo $data["customer_name"]; ?> (<?php echo $data["customer_email"]; ?>) telah melakukan pembelian emas pada waktu
              <strong style="color:#0080FF;"><?php echo date("j", strtotime($data["created_date"])) . " " . month_indonesia(date("n", strtotime($data["created_date"]))) . " " . date("Y", strtotime($data["created_date"])) . " " . date("H:i", strtotime($data["created_date"])); ?> WIB</strong>
              dengan info sebagai berikut:
            </p>

            <table cellpadding="0" cellspacing="0" align="left" border="0" style="width:100%; float:none; margin-bottom:40px;margin-top:30px;">
              <tr>
                <td style="padding:4px 13px 4px 0px; width:45%;">
                  <p style="font-size:12px; margin:0px; font-family:sans-serif;">
                    Nomor Invoice
                  </p>
                </td>
                <td style="padding:4px 0px 4px 0px;">
                  <p style="font-size:12px; margin:0px; font-family:sans-serif; padding:0px 10px;">
                    :
                  </p>
                </td>
                <td style="padding:4px 0px 4px 0px;">
                  <p style="font-size:12px; margin:0px; font-family:sans-serif; word-break:break-all;">
                    <strong style="color:#0080FF;"><?php echo $data["invoice_order"]; ?></strong>
                  </p>
                </td>
              </tr>

              <tr>
                <td style="padding:4px 13px 4px 0px;">
                  <p style="font-size:12px; margin:0px; font-family:sans-serif;">
                    Metode Pembayaran
                  </p>
                </td>
                <td style="padding:4px 0px 4px 0px;">
                  <p style="font-size:12px; margin:0px; font-family:sans-serif; padding:0px 10px;">
                    :
                  </p>
                </td>
                <td style="padding:4px 0px 4px 0px;">
                  <p style="font-size:12px; margin:0px; font-family:sans-serif;">
                    <strong style="color:#0080FF;"><?php echo $data["metode_pembayaran"]; ?></strong>
                  </p>
                </td>
              </tr>

              <?php if(isset($data["no_va"])) { ?>
                <tr>
                  <td style="padding:4px 13px 4px 0px;">
                    <p style="font-size:12px; margin:0px; font-family:sans-serif;">
                      Virtual Account
                    </p>
                  </td>
                  <td style="padding:4px 0px 4px 0px;">
                    <p style="font-size:12px; margin:0px; font-family:sans-serif; padding:0px 10px;">
                      :
                    </p>
                  </td>
                  <td style="padding:4px 0px 4px 0px;">
                    <p style="font-size:12px; margin:0px; font-family:sans-serif;">
                      <strong style="color:#0080FF;"><?php echo $data["no_va"]; ?></strong>
                    </p>
                  </td>
                </tr>
              <?php } ?>

              <tr>
                <td style="padding:4px 13px 4px 0px;">
                  <p style="font-size:12px; margin:0px; font-family:sans-serif;">
                    Batas Waktu Pembayaran
                  </p>
                </td>
                <td style="padding:4px 0px 4px 0px;">
                  <p style="font-size:12px; margin:0px; font-family:sans-serif; padding:0px 10px;">
                    :
                  </p>
                </td>
                <td style="padding:4px 0px 4px 0px;">
                  <p style="font-size:12px; margin:0px; font-family:sans-serif;">
                    <strong style="color:#0080FF;"><?php echo date("j", strtotime($data["due_date"])) . " " . month_indonesia(date("n", strtotime($data["due_date"]))) . " " . date("Y", strtotime($data["due_date"])) . " " . date("H:i", strtotime($data["due_date"])); ?> WIB</strong>
                  </p>
                </td>
              </tr>

              <tr>
                <td style="padding:4px 13px 4px 0px;">
                  <p style="font-size:12px; margin:0px; font-family:sans-serif;">
                    Nilai Beli Emas
                  </p>
                </td>
                <td style="padding:4px 0px 4px 0px;">
                  <p style="font-size:12px; margin:0px; font-family:sans-serif; padding:0px 10px;">
                    :
                  </p>
                </td>
                <td style="padding:4px 0px 4px 0px;">
                  <p style="font-size:12px; margin:0px; font-family:sans-serif;">
                    Rp <?php echo number_format(round($data["total_price"], 0, PHP_ROUND_HALF_UP), 0, ".", "."); ?>
                  </p>
                </td>
              </tr>

              <tr>
                <td style="padding:4px 13px 4px 0px;">
                  <p style="font-size:12px; margin:0px; font-family:sans-serif;">
                    Jumlah Emas
                  </p>
                </td>
                <td style="padding:4px 0px 4px 0px;">
                  <p style="font-size:12px; margin:0px; font-family:sans-serif; padding:0px 10px;">
                    :
                  </p>
                </td>
                <td style="padding:4px 0px 4px 0px;">
                  <p style="font-size:12px; margin:0px; font-family:sans-serif;">
                    <strong style="color:#0080FF;"><?php echo str_replace(".", ",", round($data["total_emas"], 4, PHP_ROUND_HALF_UP)); ?> gram</strong>
                  </p>
                </td>
              </tr>

              <tr>
                <td style="padding:4px 13px 4px 0px;">
                  <p style="font-size:12px; margin:0px; font-family:sans-serif;">
                    Biaya Pemesanan Emas
                  </p>
                </td>
                <td style="padding:4px 0px 4px 0px;">
                  <p style="font-size:12px; margin:0px; font-family:sans-serif; padding:0px 10px;">
                    :
                  </p>
                </td>
                <td style="padding:4px 0px 4px 0px;">
                  <p style="font-size:12px; margin:0px; font-family:sans-serif;">
                    Rp <?php echo number_format(round($data["booking_fee"], 0, PHP_ROUND_HALF_UP), 0, ".", "."); ?>
                  </p>
                </td>
              </tr>

              <tr>
                <td style="padding:4px 13px 4px 0px;">
                  <p style="font-size:12px; margin:0px; font-family:sans-serif;">
                    Pajak
                  </p>
                </td>
                <td style="padding:4px 0px 4px 0px;">
                  <p style="font-size:12px; margin:0px; font-family:sans-serif; padding:0px 10px;">
                    :
                  </p>
                </td>
                <td style="padding:4px 0px 4px 0px;">
                  <p style="font-size:12px; margin:0px; font-family:sans-serif;">
                    Rp <?php echo number_format(round($data["tax"], 0, PHP_ROUND_HALF_UP), 0, ".", "."); ?>
                  </p>
                </td>
              </tr>

              <tr>
                <td style="padding:4px 13px 4px 0px;">
                  <p style="font-size:12px; margin:0px; font-family:sans-serif;">
                    Biaya Admin
                  </p>
                </td>
                <td style="padding:4px 0px 4px 0px;">
                  <p style="font-size:12px; margin:0px; font-family:sans-serif; padding:0px 10px;">
                    :
                  </p>
                </td>
                <td style="padding:4px 0px 4px 0px;">
                  <p style="font-size:12px; margin:0px; font-family:sans-serif;">
                    Rp <?php echo number_format(round($data["partner_fee"], 0, PHP_ROUND_HALF_UP), 0, ".", "."); ?>
                  </p>
                </td>
              </tr>

              <tr>
                <td style="padding:4px 13px 4px 0px;">
                  <p style="font-size:12px; margin:0px; font-family:sans-serif;">
                    Biaya Bank
                  </p>
                </td>
                <td style="padding:4px 0px 4px 0px;">
                  <p style="font-size:12px; margin:0px; font-family:sans-serif; padding:0px 10px;">
                    :
                  </p>
                </td>
                <td style="padding:4px 0px 4px 0px;">
                  <p style="font-size:12px; margin:0px; font-family:sans-serif;">
                    Rp <?php echo number_format(round($data["xendit_fee"], 0, PHP_ROUND_HALF_UP), 0, ".", "."); ?>
                  </p>
                </td>
              </tr>

              <tr>
                <td style="padding:4px 13px 4px 0px;">
                  <p style="font-size:12px; margin:0px; font-family:sans-serif;">
                    Jumlah
                  </p>
                </td>
                <td style="padding:4px 0px 4px 0px;">
                  <p style="font-size:12px; margin:0px; font-family:sans-serif; padding:0px 10px;">
                    :
                  </p>
                </td>
                <td style="padding:4px 0px 4px 0px;">
                  <p style="font-size:12px; margin:0px; font-family:sans-serif;">
                    <strong style="color:#0080FF;">Rp <?php echo number_format(round($data["total_payment"], 0, PHP_ROUND_HALF_UP), 0, ".", "."); ?></strong>
                  </p>
                </td>
              </tr>
            </table>
          </td>
        </tr>
      </table>
    </td>
  </tr>
</table>