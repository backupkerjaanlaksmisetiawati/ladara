<table cellpadding="0" cellspacing="0" align="left" border="0" style="background-color:#ffffff; width:100%; float:none;">
  <tr>
    <td style="padding:3%;">
      <h2 style="margin:0px 0px 10px 0px; font-family:sans-serif; font-size:14px;">
        Halo <?php echo $data["customer_name"]; ?>,
      </h2>
      
      <h2 style="margin:0px 0px 10px 0px; font-family:sans-serif; font-size:14px;">
        Selamat akun Ladara Emas kamu sudah terverifikasi.
      </h2>

      <p style="margin:0px 0px 10px 0px; font-family:sans-serif; font-size:12px;">
        Kamu sudah bisa melakukan penarikan emasmu.
      </p>

      <p style="margin:0px 0px 10px 0px; font-family:sans-serif; font-size:12px;">
        Mau menabung emas, sekarang sudah bisa dilakukan di Ladara Emas!
      </p>

      <img src="<?php echo get_template_directory_uri(); ?>/library/images/emas/banner-email.png" style="width:100%;" />
    </td>
  </tr>
</table>