<table cellpadding="0" cellspacing="0" align="left" border="0" style="background-color:#ffffff; width:100%; float:none;">
  <tr>
    <td style="padding:3%;">
      <p style="margin:0px 0px 10px 0px; font-family:sans-serif; font-size:12px;">
        Customer dengan informasi berikut:
      </p>

      <p style="margin:0px 0px 10px 0px; font-family:sans-serif; font-size:12px; line-height:16px;">
        Nama: <?php echo $data["customer_name"]; ?>
        <br />
        Email: <?php echo $data["customer_email"]; ?>
        <br />
      </p>

      <p style="margin:0px 0px 10px 0px; font-family:sans-serif; font-size:12px;">
        Telah melakukan perubahan akun Ladara Emas yang terhubung dengan Treasury pada <strong><?php echo date("j", strtotime($data["log_date"])) . " " . month_indonesia(date("n", strtotime($data["log_date"]))) . " " . date("Y", strtotime($data["log_date"])) . " " . date("H:i", strtotime($data["log_date"])); ?> WIB</strong>
      </p>
    </td>
  </tr>
</table>