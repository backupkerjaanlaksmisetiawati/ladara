<table>
    <tr>
        <td>
            <div class="success" style="text-align: left;">
                <h3>Menunggu Pembayaran</h3>
                <p>Checkout berhasil pada tanggal Selasa, <?php echo $data['order']->getDateCreated() ?></p>
            </div>
        </td>
    </tr>

</table>
<table style="width: 100%;">
    <tr>
        <td style="text-align: left;font-weight: bold;">Total Pembayaran</td>
        <td style="text-align: left;font-weight: bold;">Batas Waktu Pembayaran</td>
    </tr>
    <tr>
        <td><?php echo $data['order']->getTotalPrice() ?></td>
        <td><?php echo $data['payment']->getPaymentExpired() ?></td>
    </tr>
    <tr>
        <td style="height: 20px;"></td>
    </tr>
    <tr>
        <td style="text-align: left;font-weight: bold;">Metode Pembayaran</td>
        <td style="text-align: left;font-weight: bold;">Referensi Pembayaran</td>
    </tr>
    <tr>
        <td><?php echo $data['payment']->getBank() ?></td>
        <td><?php echo $data['order']->getInvoiceNo() ?></td>
    </tr>
    <tr>
        <td style="height: 20px;"></td>
    </tr>
    <tr>
        <td style="text-align: left;font-weight: bold;">Kode Pembayaran</td>
        <td style="text-align: left;font-weight: bold;">Status Pembayaran</td>
    </tr>
    <tr>
        <td><?php echo $data['payment']->getVa() ?></td>
        <td><?php echo $data['order']->getStatus() ?></td>
    </tr>
    <tr>
        <td style="height: 20px;"></td>
    </tr>
    <tr>
        <td colspan="2">* Apabila Anda masih memiliki tagihan <?php echo $data['payment']->getBank() ?> yang belum dibayar, maka transaksi sebelumnya akan kami batalkan secara otomatis.</td>
    </tr>
    <tr>
        <td colspan="2">
            <p style="font-weight: bold;">
                Langkah-langkah pembayaran <?php echo $data['payment']->getBank() ?>
            </p>
        </td>
    </tr>
    <tr>
        <td colspan="2">
            <p style="font-weight: bold;">
               <?php foreach ($data['payment']->getPaymentInstruction() as $key => $val):?>
                 <p style="font-weight: bold;"> <?php echo $key ?? ''?></p>
                <ul>
                    <?php foreach ($val as $row) : ?>
                        <li> <?php echo $row['step'] ?? ''?></li>
                    <?php endforeach; ?>
                </ul>
               <?php endforeach;?>
            </p>
        </td>
    </tr>
    <tr>
        <td colspan="2">
            <h3>Rincian Pesanan</h3>
        </td>
    </tr>
    <?php foreach ($data['order']->getItems() as $row) :?>
    <tr>
        <td><?php echo $row['name'] ?? ''?> x <?php echo $row['qty'] ?? ''?></td>
        <td style="font-weight: bold;"><?php echo isset($row['price']) ? 'Rp '.number_format($row['price'],0,'','.') : '' ?></td>
    </tr>
    <?php endforeach ?>
    <tr>
        <td><?php echo $data['shipping']->getCourier(); ?> (<?php echo $data['shipping']->getService(); ?>)</td>
        <td style="font-weight: bold;"><?php echo $data['shipping']->getPrice(); ?></td>
    </tr>
    <tr>
        <td colspan="2" style="height: 20px;border-top: 1px solid #d0cdcd;"></td>
    </tr>
    <tr>
        <td style="font-weight: bold;">Total pembayaran</td>
        <td style="font-weight: bold;"><?php echo $data['order']->getTotalPrice(); ?></td>
    </tr>
    <tr>
        <td style="height: 10px;"></td>
    </tr>
    <tr>
        <td colspan="2">
            <p>
                Pantau status pembayaran anda pada halaman<br>
                <strong>Status Pembayaran.</strong>
            </p>
        </td>
    </tr>
    <tr>
        <td style="height: 20px;"></td>
    </tr>
    <tr>
        <td colspan="2" style="text-align: center;">
            <a href="<?php echo get_site_url().'/my-order'?>"
                target="_blank" shape="rect"
               style="
                    background: #ffffff;
                    text-decoration: none;
                    padding: 10px 20px;
                    border:1.5px solid #0080FF;
                    line-height: 43px;
                    font-size: 16px;
                    color: #000000;
                    font-weight: 600;
                    border-radius: 20px;
                    letter-spacing: .5px;
                    margin-top: 15px;">
                Cek Status Pembayaran
            </a>
        </td>
    </tr>
</table>