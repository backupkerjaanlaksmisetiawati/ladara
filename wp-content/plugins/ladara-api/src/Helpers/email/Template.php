<?php


namespace Ladara\Helpers\email;


class Template
{
    public static function load($theme, $data=[])
    {
        ob_start();
        include 'template/header.php';
        include 'template/'.$theme.'.php';
        include 'template/footer.php';

        return ob_get_clean();

    }
}