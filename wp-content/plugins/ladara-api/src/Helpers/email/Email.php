<?php


namespace Ladara\Helpers\email;

use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\SMTP;
use PHPMailer\PHPMailer\Exception;

class Email
{
    /**
     * config header email
     * @var string[]
     */
    private $headers = ['Content-Type: text/html; charset=UTF-8'];

    /**
     * data email
     * @var array
     */
    private $data;

    /**
     * @var
     */
    private $emailTo;
    /**
     * @var
     */
    private $emailSubject;

    /**
     * template file yang ada di dalam folder template
     * @var
     */
    private $template;

    /**
     * @return bool
     */
    protected function send()
    {
        $options = get_option('wp_mail_smtp');

        $from_email = getenv('SMTP_FROM_ADDRESS');
        $from_name = 'Ladara';
        $to_email = $this->getEmailTo();
        $subject = $this->getEmailSubject();

        $mail = new PHPMailer(true);

        // Send the mail.
        try {
            $mail->isSMTP();
            $mail->Host = getenv('SMTP_HOST');
            $mail->SMTPAuth = true;
            $mail->Username = getenv('SMTP_USERNAME');
            $mail->Password = getenv('SMTP_PASSWORD');
            $mail->SMTPSecure = getenv('SMTP_ENCRYPTION') === 'tls' ? PHPMailer::ENCRYPTION_STARTTLS : PHPMailer::ENCRYPTION_SMTPS;
            $mail->Port = getenv('SMTP_PORT');
        
            $mail->setFrom($from_email, $from_name);
            $mail->addAddress($to_email);

            $mail->isHTML(true);
            $mail->Subject = $subject;
            $mail->Body = Template::load($this->getTemplate(),$this->getData());
        
            $mail->send();

            return 'Message has been sent';
        } catch (Exception $e) {
            return "Message could not be sent. Mailer Error: {$mail->ErrorInfo}";
        }
    }

    /**
     * @return bool
     */
    protected function debug()
    {
        return Template::load($this->getTemplate(),$this->getData());
    }

    /**
     * @return string[]
     */
    public function getHeaders()
    {
        return $this->headers;
    }

    /**
     * @param string[] $headers
     */
    public function setHeaders(array $headers)
    {
        $this->headers = $headers;
    }

    /**
     * @return array
     */
    public function getData()
    {
        return $this->data;
    }

    /**
     * @param array $data
     */
    public function setData($data)
    {
        $this->data = $data;
    }

    /**
     * @return mixed
     */
    public function getEmailTo()
    {
        return $this->emailTo;
    }

    /**
     * @param mixed $emailTo
     */
    public function setEmailTo($emailTo)
    {
        $this->emailTo = $emailTo;
    }

    /**
     * @return mixed
     */
    public function getEmailSubject()
    {
        return $this->emailSubject;
    }

    /**
     * @param mixed $emailSubject
     */
    public function setEmailSubject($emailSubject)
    {
        $this->emailSubject = $emailSubject;
    }

    /**
     * @return mixed
     */
    public function getTemplate()
    {
        return $this->template;
    }

    /**
     * @param mixed $template
     */
    public function setTemplate($template)
    {
        $this->template = $template;
    }
}