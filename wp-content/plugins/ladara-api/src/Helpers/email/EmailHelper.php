<?php

namespace Ladara\Helpers\email;

use Ladara\Helpers\builder\OrderBuilder;
use Ladara\Helpers\builder\PaymentBuilder;
use Ladara\Helpers\builder\ShippingBuilder;
use Ladara\Helpers\builder\UsersBuilder;

if ( ! defined( 'ABSPATH' ) ) {
    exit;
}

class EmailHelper extends Email
{

    /**
     * User object
     * @var UsersBuilder
     */
    private $user;

    public function __construct(UsersBuilder $user)
    {
        $this->setEmailTo($user->getEmail());
        $this->user = $user;
    }

    /**
     * Email register
     * @param $linkAktivasi
     * @return bool
     */
    public function register($linkAktivasi)
    {
        $data = [
            'user' => $this->user,
            'link' => $linkAktivasi
        ];
        $this->setData($data);
        $this->setEmailSubject('Aktivasi Akun Ladara');
        $this->setTemplate('register');
        return $this->send();
    }

    /**
     * email untuk forgot password
     * @param $link
     * @return bool
     */
    public function forgotPassword($link)
    {
        $data = [
            'user' => $this->user,
            'link' => $link
        ];
        $this->setData($data);
        $this->setEmailSubject('Atur ulang password Ladara Akun Anda');
        $this->setTemplate('forgotPassword');
        return $this->send();
    }

    /**
     * email new password
     * @param $password
     * @param string $subject
     * @return bool
     */
    public function newPassword($password, $subject = 'Atur ulang password Ladara Akun Anda')
    {
        $data = [
            'user' => $this->user,
            'password' => $password
        ];
        $this->setData($data);
        $this->setEmailSubject($subject);
        $this->setTemplate('newPassword');
        return $this->send();
    }

    /**
     * email saat berhasil merubah password
     * @return bool
     */
    public function messageForgotPassword()
    {
        $data = [
            'user' => $this->user,
        ];
        $this->setData($data);
        $this->setEmailSubject('Password anda telah diubah');
        $this->setTemplate('messageForgotPassword');
        return $this->send();
    }

    /**
     * email saat berhasil reset password
     * @return bool
     */
    public function messageResetPassword($newPassword)
    {
        $data = [
            'user' => $this->user,
            'new_password' => $newPassword
        ];

        $this->setData($data);
        $this->setEmailSubject('Password anda telah diubah.');
        $this->setTemplate('messageResetPassword');

        return $this->send();
    }

    /**
     * Email saat klarifikasi.
     *
     * @return void
     */
    public function messageMaaf($username)
    {
        $data = [
            'user' => $this->user,
            'username' => $username
        ];

        $this->setData($data);
        $this->setEmailSubject('Mohon abaikan email "Password anda telah diubah"');
        $this->setTemplate('messageMaaf');

        return $this->send();
    }

    /**
     * Email welcome message
     * @return bool|string
     */
    public function welcome()
    {
        $data = [
            'user' => $this->user
        ];

        $this->setData($data);
        $this->setEmailSubject('Selamat Datang di Ladara');
        $this->setTemplate('welcome');
        return $this->send();
    }

    /**
     * email ketika order sudah dibuat dan tinggal meunggu pembayaran
     * @param OrderBuilder $order
     * @param PaymentBuilder $payment
     * @param ShippingBuilder $shipping
     * @return bool|string
     */
    public function menungguPembayaran(OrderBuilder $order, PaymentBuilder $payment, ShippingBuilder $shipping)
    {
        $data = [
            'user' => $this->user,
            'order' => $order,
            'payment' => $payment,
            'shipping' => $shipping
        ];
        $this->setData($data);
        $this->setEmailSubject('Menunggu Pembayaran '.$payment->getBank().' untuk pembayaran '.$order->getInvoiceNo());
        $this->setTemplate('menungguPembayaran');
        return $this->send();
    }

    /**
     * email ketika pembayaran berhasil
     * @param OrderBuilder $order
     * @param PaymentBuilder $payment
     * @param ShippingBuilder $shipping
     * @return bool
     */
    public function pembayaranBerhasil(OrderBuilder $order, PaymentBuilder $payment, ShippingBuilder $shipping)
    {
        $data = [
            'user' => $this->user,
            'order' => $order,
            'payment' => $payment,
            'shipping' => $shipping
        ];
        $this->setData($data);
        $this->setEmailSubject('Order ID '.$order->getInvoiceNo().' Berhasil di bayar');
        $this->setTemplate('pembayaranBerhasil');
        return $this->send();
    }

    /**
     * Email ketika pembayaraan gagal
     * @param OrderBuilder $order
     * @param PaymentBuilder $payment
     * @param ShippingBuilder $shipping
     * @return bool
     */
    public function batalTransaksi(OrderBuilder $order, PaymentBuilder $payment, ShippingBuilder $shipping)
    {
        $data = [
            'user' => $this->user,
            'order' => $order,
            'payment' => $payment,
            'shipping' => $shipping
        ];
        $this->setData($data);
        $this->setEmailSubject('Order ID '.$order->getInvoiceNo().' Dibatalkan');
        $this->setTemplate('batalTransaksi');
        return $this->send();
    }

    /*Ladara emas*/
    //param sementara pakai array tanpa helper untuk semua email ladara emas
    /**
     * Email invoice beli emas
     * @param array $datas
     * @return bool
     */
    public function invoiceBeliEmas($data)
    {
        $this->setData($data);
        $this->setEmailSubject('Pembelian emas dengan invoice ' . $data["invoice_order"] . ' di Ladara Emas');
        $this->setTemplate('emas/invoiceBeli');
        return $this->send();
    }

    /**
     * Email invoice beli emas untuk admin
     * @param array $datas
     * @return bool
     */
    public function adminInvoiceBeliEmas($data)
    {
        $this->setData($data);
        $this->setEmailSubject('Pembelian emas dengan invoice ' . $data["invoice_order"] . ' di Ladara Emas');
        $this->setTemplate('emas/adminInvoiceBeli');
        return $this->send();
    }
    
    /**
     * Email invoice beli emas
     * @param array $datas
     * @return bool
     */
    public function suksesBeliEmas($data)
    {
        $this->setData($data);
        $this->setEmailSubject('Selamat! Pembelian emas dengan invoice ' . $data["invoice_order"] . ' di Ladara Emas telah sukses');
        $this->setTemplate('emas/suksesBeli');
        return $this->send();
    }

    /**
     * Email invoice beli emas untuk admin
     * @param array $datas
     * @return bool
     */
    public function adminSuksesBeliEmas($data)
    {
        $this->setData($data);
        $this->setEmailSubject('Pembelian emas dengan invoice ' . $data["invoice_order"] . ' di Ladara Emas telah sukses');
        $this->setTemplate('emas/adminSuksesBeli');
        return $this->send();
    }
    
    /**
     * Email pembatalan beli emas
     * @param array $datas
     * @return bool
     */
    public function pembatalanBeliEmas($data)
    {
        $this->setData($data);
        $this->setEmailSubject('Pembelian emas dengan invoice ' . $data["invoice_order"] . ' di Ladara Emas telah dibatalkan');
        $this->setTemplate('emas/pembatalanBeli');
        return $this->send();
    }

    /**
     * Email pembatalan beli emas untuk admin
     * @param array $datas
     * @return bool
     */
    public function adminPembatalanBeliEmas($data)
    {
        $this->setData($data);
        $this->setEmailSubject('Pembelian emas dengan invoice ' . $data["invoice_order"] . ' di Ladara Emas telah dibatalkan');
        $this->setTemplate('emas/adminPembatalanBeli');
        return $this->send();
    }
    
    /**
     * Email gagal beli emas
     * @param array $datas
     * @return bool
     */
    public function gagalBeliEmas($data)
    {
        $this->setData($data);
        $this->setEmailSubject('Pembelian emas dengan invoice ' . $data["invoice_order"] . ' di Ladara Emas gagal');
        $this->setTemplate('emas/gagalBeli');
        return $this->send();
    }

    /**
     * Email gagal beli emas untuk admin
     * @param array $datas
     * @return bool
     */
    public function adminGagalBeliEmas($data)
    {
        $this->setData($data);
        $this->setEmailSubject('Pembelian emas dengan invoice ' . $data["invoice_order"] . ' di Ladara Emas gagal');
        $this->setTemplate('emas/adminGagalBeli');
        return $this->send();
    }

    /**
     * Email invoice jual emas
     * @param array $datas
     * @return bool
     */
    public function invoiceJualEmas($data)
    {
        $this->setData($data);
        $this->setEmailSubject('Penjualan emas dengan invoice ' . $data["invoice_order"] . ' di Ladara Emas');
        $this->setTemplate('emas/invoiceJual');
        return $this->send();
    }

    /**
     * Email invoice jual emas untuk admin
     * @param array $datas
     * @return bool
     */
    public function adminInvoiceJualEmas($data)
    {
        $this->setData($data);
        $this->setEmailSubject('Penjualan emas dengan invoice ' . $data["invoice_order"] . ' di Ladara Emas');
        $this->setTemplate('emas/adminInvoiceJual');
        return $this->send();
    }

    /**
     * Email jual emas disetujui
     * @param array $datas
     * @return bool
     */
    public function disetujuiJualEmas($data)
    {
        $this->setData($data);
        $this->setEmailSubject('Selamat! Penjualan emas dengan invoice ' . $data["invoice_order"] . ' di Ladara Emas telah disetujui');
        $this->setTemplate('emas/disetujuiJual');
        return $this->send();
    }

    /**
     * Email invoice beli emas untuk admin
     * @param array $datas
     * @return bool
     */
    public function adminDisetujuiJualEmas($data)
    {
        $this->setData($data);
        $this->setEmailSubject('Penjualan emas dengan invoice ' . $data["invoice_order"] . ' di Ladara Emas telah disetujui');
        $this->setTemplate('emas/adminDisetujuiJual');
        return $this->send();
    }
    
    /**
     * Email pembatalan jual emas
     * @param array $datas
     * @return bool
     */
    public function jualEmasDitolak($data)
    {
        $this->setData($data);
        $this->setEmailSubject('Penjualan emas dengan invoice ' . $data["invoice_order"] . ' di Ladara Emas telah ditolak');
        $this->setTemplate('emas/jualDitolak');
        return $this->send();
    }

    /**
     * Email pembatalan jual emas untuk admin
     * @param array $datas
     * @return bool
     */
    public function adminJualEmasDitolak($data)
    {
        $this->setData($data);
        $this->setEmailSubject('Penjualan emas dengan invoice ' . $data["invoice_order"] . ' di Ladara Emas telah ditolak');
        $this->setTemplate('emas/adminJualDitolak');
        return $this->send();
    }

    /**
     * Email aktivasi akun ladara emas
     * @param array $datas
     * @return bool
     */
    public function aktivasiAkunEmas($data)
    {
        $this->setData($data);
        $this->setEmailSubject('Selamat datang di Ladara Emas');
        $this->setTemplate('emas/aktivasiAkun');
        return $this->send();
    }

    /**
     * Email aktivasi akun ladara emas untuk admin
     * @param array $datas
     * @return bool
     */
    public function adminAktivasiAkunEmas($data)
    {
        $this->setData($data);
        $this->setEmailSubject('Customer ' . $data["customer_name"] . ' (' . $data["customer_email"] . ') telah mendaftar di Ladara Emas');
        $this->setTemplate('emas/adminAktivasiAkun');
        return $this->send();
    }

    /**
     * Email verifikasi akun ladara emas
     * @param array $datas
     * @return bool
     */
    public function verifikasiAkunEmas($data)
    {
        $this->setData($data);
        $this->setEmailSubject('Selamat! Akun Ladara Emas kamu sudah terverifikasi');
        $this->setTemplate('emas/verifikasiAkun');
        return $this->send();
    }

    /**
     * Email verifikasi akun ladara emas untuk admin
     * @param array $datas
     * @return bool
     */
    public function adminVerifikasiAkunEmas($data)
    {
        $this->setData($data);
        $this->setEmailSubject('Customer ' . $data["customer_name"] . ' (' . $data["customer_email"] . ') telah terverifikasi di Ladara Emas');
        $this->setTemplate('emas/adminVerifikasiAkun');
        return $this->send();
    }

    /**
     * Email update akun linked treasury di ladara emas
     * @param array $datas
     * @return bool
     */
    public function updateAkunTreasury($data)
    {
        $this->setData($data);
        $this->setEmailSubject('Akun Ladara Emas kamu telah berubah!');
        $this->setTemplate('emas/updateAkunTreasury');
        return $this->send();
    }

    /**
     * Email update akun linked treasury di ladara emas untuk admin
     * @param array $datas
     * @return bool
     */
    public function adminUpdateAkunTreasury($data)
    {
        $this->setData($data);
        $this->setEmailSubject('Customer ' . $data["customer_name"] . ' (' . $data["customer_email"] . ') telah mengubah akun Ladara Emas');
        $this->setTemplate('emas/adminUpdateAkunTreasury');
        return $this->send();
    }
    /*Ladara emas*/


}