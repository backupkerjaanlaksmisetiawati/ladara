<?php


namespace Ladara\Helpers;


use Ladara\Controllers\Categories;
use Ladara\Models\Products as ProductModel;

class ProductHelpers
{

    /**
     * template untuk response product detail
     * @param $userId
     * @param $product
     * @return array
     */
    public function respProduct($userId, $product)
    {
        $decode =json_decode($product,true);
        unset($decode['meta_data']);
        unset($decode['manage_stock']);
        unset($decode['stock_quantity']);
        unset($decode['stock_status']);

        $resp = $decode;
        $brandData = get_field('product_brand',$product->get_id());
        $sellerData = get_field('seller_name',$product->get_id());
        $bahanData = get_field('product_bahan',$product->get_id());
        $resp['description'] = wp_strip_all_tags($decode['description']);
        $resp['image'] = $this->getProductImage($product);
        $resp['categories'] = $this->getCategories($product->get_id());
        $resp['discount'] = $this->calculateDiscount($product->get_regular_price(), $product->get_sale_price());
        $resp['wishlist'] = ProductModel::checkWishList($userId,$product->get_id());
        $resp['variant'] = ProductModel::getVariant($product->get_id());
        $resp['stock'] = ProductModel::getStock($product->get_id());
        $resp['bahan'] = $bahanData;
        $resp['brand'] = [
            'id' => $brandData->ID,
            'name' => $brandData->post_title
        ];
        $resp['seller'] = [
            'id' => $sellerData->ID,
            'name' => $sellerData->post_title
        ];

        return $resp;
    }

    /**
     * template untuk response product detail
     * @param $userId
     * @param $product
     * @return array
     */
    public function respProductMigration($userId, $products)
    {
        global $wpdb;
        $dataProduct = [];
        $productImage = [];
        foreach ($products as $key => $product){

            $decode =json_decode($product,true);
            unset($decode['meta_data']);
            unset($decode['short_description']);
            unset($decode['manage_stock']);
            unset($decode['stock_quantity']);
            unset($decode['stock_status']);
            $resp[$key] = $decode;
            $brandData = get_field('product_brand',$product->get_id());
            $sellerData = get_field('seller_name',$product->get_id());
            $images = $this->getProductImage($product, true);

            $dataProduct = [
                'id' => $product->get_id(),
                'merchant_id' => $sellerData->ID ?? 0,
                'title' => $decode['name'],
                'permalink' => $decode['slug'],
                'brand_id' => $brandData->ID ?? 0,
                'description' => wp_strip_all_tags($decode['description']),
                'categories_id' => end($decode['category_ids']),
                'video' => '',
                'min_order' => 1,
                'price' => $product->get_price(),
                'stock' => 100,
                'sku' => $decode['sku'],
                'weight' => $decode['weight'],
                'asuransi' => 0,
                'panjang' => $decode['length'] == '' ? 10 : $decode['length'],
                'lebar' => $decode['width'] == '' ? 10 : $decode['width'],
                'tinggi' => $decode['height'] == '' ? 10 : $decode['height'],
                'status' => 1,
                'created_at' => date('Y-m-d H:i:s', strtotime($decode['date_created']['date'])),
                'updated_at' => date('Y-m-d H:i:s', strtotime($decode['date_modified']['date'])),
            ];
            $wpdb->insert('ldr_products', $dataProduct);

            foreach ($images as $index => $image){
                if ($image['medium']){
                    if ($index == 0){
                        $name = 'imageUtama';
                    }elseif ($index == 1){
                        $name = 'imageDepan';
                    }elseif ($index == 2){
                        $name = 'imageSamping';
                    }else{
                        $name = 'imageAtas';
                    }
                    $exPath = explode('wp-content', $image['medium']);
                    if (isset($exPath[1])){
                        $productImage = [
                            'products_id' => $product->get_id(),
                            'name' => $name,
                            'path' => 'wp-content'.$exPath[1],
                        ];

                        $wpdb->insert('ldr_product_images', $productImage);
                    }
                }

            }
        }
        return $resp;
    }

    public function respProductMigrationTags($userId, $products)
    {
        global $wpdb;
        $resp = [] ;
        foreach ($products as $key => $product){
            $id = $product->get_id();
            $tags = get_the_terms($id, 'product_tag');
            foreach ($tags as $tag){
                $resp = [
                    'product_id' => $id,
                    'tag' => $tag->name
                ];
                $wpdb->insert('ldr_product_tags', $resp);
            }
        }

        return $resp;
    }

    /**
     * template Update categories id di product merchant
     * @param $userId
     * @param $product
     * @return array
     */
    public function respProductMigrationUpdateCategories($products)
    {
        global $wpdb;
        foreach ($products as $key => $product){

            $decode =json_decode($product,true);
            unset($decode['meta_data']);
            unset($decode['short_description']);
            unset($decode['manage_stock']);
            unset($decode['stock_quantity']);
            unset($decode['stock_status']);
            $resp[$key] = $decode;
            $idCate = end($decode['category_ids']);
            if (count($decode['category_ids']) > 1){
                $cate = $this->getCategories($product->get_id());
                $idCate = $this->geIdCategoryLastChild($cate);
            }

            $dataProduct = [
                'panjang' => $decode['length'] == '' ? 1 : $decode['length'],
                'lebar' => $decode['width'] == '' ? 1 : $decode['width'],
                'tinggi' => $decode['height'] == '' ? 1 : $decode['height'],
                'categories_id' => $idCate,
            ];
            $wpdb->update('ldr_products', $dataProduct, ['id' => $product->get_id()]);
        }
        return $resp;
    }

    /**
     * template untuk response products / multiple product
     * @param $userId
     * @param $products array products
     * @return array
     */
    public function respProducts($userId, $products=[])
    {
        $resp = [];
        $productIds = [];
        foreach ($products as $product){
            $productIds[]=$product->get_id();
        }
        $wishlist = [];
        if ($userId != 0){
            $wishlist = ProductModel::checkWishListBulk($userId,$productIds);
        }

        foreach ($products as $key => $product){

            $decode =json_decode($product,true);
            unset($decode['meta_data']);
            unset($decode['date_modified']);
            unset($decode['description']);
            unset($decode['short_description']);
            unset($decode['manage_stock']);
            unset($decode['stock_quantity']);
            unset($decode['stock_status']);
            $resp[$key] = $decode;
            $resp[$key]['image'] = $this->getProductImage($product);
            $resp[$key]['categories'] = $this->getCategories($product->get_id());
            $resp[$key]['discount'] = $this->calculateDiscount($product->get_regular_price(), $product->get_sale_price());
            $resp[$key]['wishlist'] = in_array($product->get_id(),$wishlist) ? 1 : 0;
        }
        return $resp;
    }

    public function respCart($userId, $products)
    {
        $resp = [];
        $productIds = [];
        foreach ($products as $product){
            $productIds[]=$product->id;
        }
        $wishlist = ProductModel::checkWishListBulk($userId,$productIds);

        foreach ($products as $key => $product){

            $decode =json_decode($product,true);
            unset($decode['meta_data']);
            $resp[$key] = $decode;
            $resp[$key]['image'] = wp_get_attachment_image_url($products->get_image_id());
            $resp[$key]['categories'] = $this->getCategories($product->id);
            $resp[$key]['discount'] = $this->calculateDiscount($product->regular_price, $product->sale_price);
            $resp[$key]['wishlist'] = in_array($product->id,$wishlist) ? 1 : 0;
        }
        return $resp;
    }

    /**
     * get categories product by productId
     * @param $productId
     * @return array|mixed
     */
    private function getCategories($productId)
    {
        $categories = get_the_terms ( $productId, 'product_cat' );
        $cate = new Categories();
        $tree = $cate->buildTree($categories);
        return $tree[0] ?? [];
    }

    private function geIdCategoryLastChild($categories)
    {
        $id = 0;
        if (count($categories->child) > 0){
            foreach ($categories->child as $child){
                $id = $child->term_id;
                if (count($child->child) > 0){
                    $id = $this->geIdCategoryLastChild($child);
                }
            }
        }

        return $id;
    }

    /**
     * fungsi untuk get image dari product
     * @param $product
     * @return array
     */
    public function getProductImage($product, $pathOnly=false)
    {
        $resp =[];
        $imageId = $product->get_image_id();
        if ($imageId){
            if ($pathOnly){
                $resp[] =[
                    'medium' =>  get_attached_file( $imageId),
                ];
            }else{
                $resp[] =[
                    'full' =>  wp_get_attachment_image_url( $imageId, 'full' ),
                    'medium' =>  wp_get_attachment_image_url( $imageId, 'medium' ),
                    'thumbnail' =>  wp_get_attachment_image_url( $imageId),
                ];
            }

        }
        
        $attachment_ids = $product->get_gallery_image_ids();
        foreach( $attachment_ids as $attachment_id ) {
            if ($pathOnly){
                $resp[] =[
                    'medium' =>  get_attached_file( $attachment_id),
                ];
            }else{
                $resp[] =[
                    'full' =>  wp_get_attachment_image_url( $attachment_id, 'full' ),
                    'medium' =>  wp_get_attachment_image_url( $attachment_id, 'medium' ),
                    'thumbnail' =>  wp_get_attachment_image_url( $attachment_id),
                ];
            }

        }
        return $resp;
    }

    /**
     * calculator persentase diskon
     * @param $regularPrice
     * @param int $salePrice
     * @return array
     */
    private function calculateDiscount($regularPrice, $salePrice=0)
    {
        // Rumus : Persentase (%) = (awal-akhir) / awal x 100%
        if ($salePrice != 0){
            $dis = ($regularPrice - $salePrice) / $regularPrice * 100;
            return [
                'text' => round($dis).'%',
                'int' => round($dis)
            ];
        }
        return [
            'text' => '0%',
            'int' => 0
        ];
    }
}