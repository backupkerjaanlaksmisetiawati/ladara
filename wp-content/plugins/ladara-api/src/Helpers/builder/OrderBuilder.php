<?php


namespace Ladara\Helpers\builder;

use WP_Error;

/**
 * Class OrderBuilder
 * @package Ladara\Helpers\builder
 * @author rikihandoyo
 */
class OrderBuilder
{
    /**
     * @var
     */
    private $dateCreated;

    /**
     * Total harga order sudah termasuk fee dan ongkir
     * @var
     */
    private $totalPrice;

    /**
     * array items
     * @example
     * [
     * 'name' => 'product 2',
     * 'qty' => 1,
     * 'price' => 30000
     * ]
     * @var array
     */
    private $items =[];

    /**
     * @var string
     */
    private $status = 'Menuggu Pembayaran';

    /**
     * @var
     */
    private $invoiceNo;

    /**
     * Waktu order di bayar
     * @var
     */
    private $paymentDate;

    /**
     * @return mixed
     */
    public function getDateCreated()
    {
        return $this->dateCreated;
    }

    /**
     * @param $dateCreated
     * @return OrderBuilder
     */
    public function setDateCreated($dateCreated)
    {
        $dateCreated = date('d F Y H:i',strtotime($dateCreated));
        $this->dateCreated = $dateCreated;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getTotalPrice()
    {
        return $this->totalPrice;
    }

    /**
     * @param mixed $totalPrice
     * @return OrderBuilder
     */
    public function setTotalPrice(int $totalPrice)
    {
        $price = 'Rp '.number_format($totalPrice,0,'','.');
        $this->totalPrice = $price;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getItems()
    {
        return $this->items;
    }

    /**
     * @param mixed $items
     * @return OrderBuilder
     */
    public function setItems(array $items)
    {
        $this->items = $items;
        return $this;
    }

    /**
     * @return string
     */
    public function getStatus(): string
    {
        return $this->status;
    }

    /**
     * @param string $status
     * @return OrderBuilder
     */
    public function setStatus(string $status)
    {
        $this->status = $status;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getInvoiceNo()
    {
        return $this->invoiceNo;
    }

    /**
     * @param mixed $invoiceNo
     * @return OrderBuilder
     */
    public function setInvoiceNo($invoiceNo)
    {
        $this->invoiceNo = $invoiceNo;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getPaymentDate()
    {
        return $this->paymentDate;
    }

    /**
     * @param mixed $paymentDate
     * @return OrderBuilder
     */
    public function setPaymentDate($paymentDate)
    {
        $paymentDate = date('d F Y H:i',strtotime($paymentDate));
        $this->paymentDate = $paymentDate;
        return $this;
    }

    /**
     * counter qty total barang
     * @return int|mixed
     */
    public function countTotalItems()
    {
        $total = 0;
        foreach ($this->items as $row){
            $total += $row['qty']  ?? 0;
        }
        return $total;
    }

    /**
     * count total harga harang saja belum termasuk ongkir dan fee payment gateway
     * @return int|mixed
     */
    public function countHargaBarang()
    {
        $total = 0;
        foreach ($this->items as $row){
            $price = $row['price'] ?? 0 * $row['qty'] ?? 0;
            $total += $price;
        }
        return 'Rp '.number_format($total, 0, '', '.');
    }




}