<?php


namespace Ladara\Helpers\builder;


/**
 * Class ShippingBuilder
 * @package Ladara\Helpers\builder
 * @author rikihandoyo
 */
class ShippingBuilder
{
    /**
     * Shipping Courier ex JNE
     * @var
     */
    private $courier;

    /**
     * Shipping service ex REG
     * @var
     */
    private $service;

    /**
     * Sipping price
     * @var
     */
    private $price;

    /**
     * ShippingBuilder constructor.
     * @param $courier
     * @param $service
     * @param $price
     */
    public function __construct($courier, $service, int $price)
    {
        $this->courier = $courier;
        $this->service = $service;
        $this->price = 'Rp '.number_format($price,0,'','.');
    }

    /**
     * @return mixed
     */
    public function getCourier()
    {
        return $this->courier;
    }

    /**
     * @return mixed
     */
    public function getService()
    {
        return $this->service;
    }

    /**
     * @return mixed
     */
    public function getPrice()
    {
        return $this->price;
    }


}