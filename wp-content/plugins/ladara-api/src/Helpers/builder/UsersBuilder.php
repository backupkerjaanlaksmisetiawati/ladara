<?php


namespace Ladara\Helpers\builder;


/**
 * Class UsersBuilder
 * @package Ladara\Helpers\builder
 * @author rikihandoyo
 */
class UsersBuilder
{
    /**
     * nama user
     * @var
     */
    private $name;
    /**
     * Email user
     * @var
     */
    private $email;
    /**
     * @var
     */
    private $address;
    /**
     * @var
     */
    private $phone;

    /**
     * UsersBuilder constructor.
     * @param $name
     * @param $email
     * @param $address
     * @param $phone
     */
    public function __construct($name, $email, $address='', $phone='')
    {
        $this->name = $name;
        $this->email = $email;
        $this->address = $address;
        $this->phone = $phone;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @return mixed
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @return mixed
     */
    public function getAddress()
    {
        return $this->address;
    }

    /**
     * @return mixed
     */
    public function getPhone()
    {
        return $this->phone;
    }



}