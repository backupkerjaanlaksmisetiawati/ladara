<?php


namespace Ladara\Helpers\builder;


/**
 * Class PaymentBuilder
 * @package Ladara\Helpers\builder
 * @author rikihandoyo
 */
class PaymentBuilder
{
    /**
     * payment expired
     * @var
     */
    private $paymentExpired;

    /**
     * type payemnt VA / CC
     * @var
     */
    private $paymentMethod;

    /**
     * Va number
     * @var
     */
    private $va;

    /**
     * Bank name Ex: BCA virtual account
     * @var
     */
    private $bank;

    /**
     * @var
     */
    private $paymentInstruction;


    /**
     * @return mixed
     */
    public function getPaymentExpired()
    {
        return $this->paymentExpired;
    }

    /**
     * @param mixed $paymentExpired
     * @return PaymentBuilder
     */
    public function setPaymentExpired($paymentExpired)
    {
        $paymentExpired = date('d F Y H:i', strtotime($paymentExpired));
        $this->paymentExpired = $paymentExpired;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getPaymentMethod()
    {
        return $this->paymentMethod;
    }

    /**
     * @param mixed $paymentMethod
     * @return PaymentBuilder
     */
    public function setPaymentMethod(string $paymentMethod)
    {
        $this->paymentMethod = $paymentMethod;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getVa()
    {
        return $this->va;
    }

    /**
     * @param mixed $va
     * @return PaymentBuilder
     */
    public function setVa(int $va)
    {
        $this->va = $va;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getBank()
    {
        return $this->bank;
    }

    /**
     * @param mixed $bank
     * @return PaymentBuilder
     */
    public function setBank(string $bank)
    {
        $this->bank = $bank;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getPaymentInstruction()
    {
        return $this->paymentInstruction;
    }

    /**
     * @param mixed $paymentInstruction
     * @return PaymentBuilder
     */
    public function setPaymentInstruction(array $paymentInstruction)
    {
        $this->paymentInstruction = $paymentInstruction;
        return $this;
    }

}