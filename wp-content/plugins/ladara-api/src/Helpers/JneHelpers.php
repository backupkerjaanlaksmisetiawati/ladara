<?php


namespace Ladara\Helpers;

use GuzzleHttp\Exception\GuzzleException;

class JneHelpers
{
    private $urlTracking = '';

    private $urlCheckPrice = '';

    public function __construct()
    {
        $this->urlTracking = getenv('JNE_TRACKING_ENDPOINT');
        $this->urlCheckPrice = getenv('JNE_PRICE_ENDPOINT');
    }

    public function tracking($NoResi)
    {
        $client = new \GuzzleHttp\Client();

        try {
            $response = $client->request('POST', $this->urlTracking . $NoResi, [
                'form_params' => [
                    'username' => getenv('JNE_USERNAME'),
                    'api_key' => getenv('JNE_API_KEY')
                ]
            ]);

            return json_decode($response->getBody());
        } catch (GuzzleException $e) {
            return  $e->getMessage();
        }
    }
}
