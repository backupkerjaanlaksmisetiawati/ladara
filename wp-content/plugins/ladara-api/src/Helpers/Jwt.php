<?php

namespace Ladara\Helpers;

if ( ! defined( 'ABSPATH' ) ) {
    exit;
}

use Firebase\JWT\ExpiredException;
use Firebase\JWT\JWT as libJwt;
use Firebase\JWT\SignatureInvalidException;
use http\Exception;
use WP_Error;

/**
 * Class Jwt
 * @package Ladara\Helpers
 * @author rikihandoyo
 */
class Jwt
{
    private static $expToken = DAY_IN_SECONDS * 7;
//    private static $expToken = 60;

    /**
     * function untuk generate Token JWT
     * @param $user
     * @return mixed|void|WP_Error
     */
    public static function generateToken($user){
        global $wpdb;

        $secret_key = defined('JWT_AUTH_SECRET_KEY') ? JWT_AUTH_SECRET_KEY : false;

        /** First thing, check the secret key if not exist return a error*/
        if (!$secret_key) {
            return new WP_Error(
                'jwt_auth_bad_config',
                __('JWT is not configurated properly, please contact the admin', 'wp-api-jwt-auth'),
                array(
                    'status' => 403,
                )
            );
        }

        /** Valid credentials, the user exists create the according Token */
        $issuedAt = time();
        $notBefore = apply_filters('jwt_auth_not_before', $issuedAt, $issuedAt);
        $expTime = self::$expToken;
        $expire = apply_filters('jwt_auth_expire', $issuedAt + $expTime, $issuedAt);

        $token = array(
            'iss' => get_bloginfo('url'),
            'iat' => $issuedAt,
            'nbf' => $notBefore,
            'exp' => $expire,
            'data' => array(
                'user' => array(
                    'id' => $user->data->ID,
                    'user_email' => $user->data->user_email,
                    'display_name' => $user->data->display_name
                ),
            ),
            'sub' => array(
                'id' => $user->data->ID,
                'user_email' => $user->data->user_email,
                'display_name' => $user->data->display_name
            ),
        );

        $user_id = $user->data->ID;

        $token_ladara_emas = self::gld_token($user_id);

        $user->data->user_emas = new \stdClass();
        $user->data->user_emas = $token_ladara_emas["data_user_emas"];
        
        $token['data']['user_emas'] = array();
        $token['data']['user_emas'] = $token_ladara_emas["token_user_emas"];

        /** Let the user modify the token data before the sign. */
        $token = libJwt::encode(apply_filters('jwt_auth_token_before_sign', $token, $user), $secret_key);

        $profile_picture = get_field('user_avatar', 'user_'.$user_id);

        $wpdb_query = "SELECT account_type FROM ldr_user_profile WHERE user_id = '$user_id'";
        $res_query = $wpdb->get_results($wpdb_query, OBJECT);
        $account_type = $res_query[0]->account_type;

        /** The token is signed, now create the object with no sensible user data to the client*/
        $data = array(
            'token' => $token,
            'exp_token' => $expTime,
            'user_email' => $user->data->user_email,
            'display_name' => $user->data->display_name,
            'profile_picture' => wp_get_attachment_image_url($profile_picture),
            'account_type' => $account_type,
            'user_id' => $user->data->ID,
        );

        $data["user_emas"]["flag"] = $user->data->user_emas->flag;
        $data["user_emas"]["trs_exist"] = $user->data->user_emas->trs_exist;
        $data["user_emas"]["trs_user"] = $user->data->user_emas->trs_user;
        // $data["user_emas"]["expires_in"] = $user->data->user_emas->expires_in;
        $data["user_emas"]["access_token"] = $user->data->user_emas->access_token;
        // $data["user_emas"]["refresh_token"] = $user->data->user_emas->refresh_token;

        return apply_filters('jwt_auth_token_before_dispatch', $data, $user);
    }

    /**
     * function untuk validate token JWT
     * @param bool $output
     * @return array|mixed|object
     */
    public static function validate_token($output = true)
    {
        /*
         * Looking for the HTTP_AUTHORIZATION header, if not present just
         * return the user.
         */
        $auth = isset($_SERVER['HTTP_AUTHORIZATION']) ? $_SERVER['HTTP_AUTHORIZATION'] : false;

        /* Double check for different auth header string (server dependent) */
        if (!$auth) {
            $auth = isset($_SERVER['REDIRECT_HTTP_AUTHORIZATION']) ? $_SERVER['REDIRECT_HTTP_AUTHORIZATION'] : false;
        }

        if (!$auth) {
            return [
                'status' => 403,
                'message' => 'Authorization header not found bro',
                'data' => [
                    'code' => 'jwt_auth_no_auth_header',
                ],
            ];

        }

        /*
         * The HTTP_AUTHORIZATION is present verify the format
         * if the format is wrong return the user.
         */
        list($token) = sscanf($auth, 'Bearer %s');
        if (!$token) {
            return [
                'status' => 403,
                'message' => 'Authorization header malformed.',
                'data' => [
                    'code' => 'jwt_auth_bad_auth_header',
                ],
            ];
        }

        /** Get the Secret Key */
        $secret_key = defined('JWT_AUTH_SECRET_KEY') ? JWT_AUTH_SECRET_KEY : false;
        if (!$secret_key) {
            return [
                'status' => 403,
                'message' => 'JWT is not configurated properly, please contact the admin',
                'data' => [
                    'code' => 'jwt_auth_bad_config',
                ],
            ];
        }

        /** Try to decode the token */
        try {
            $tokenJwt = $token;
            $token = libJwt::decode($token, $secret_key, array('HS256'));
            /** The Token is decoded now validate the iss */
            if ($token->iss != get_bloginfo('url')) {
                /** The iss do not match, return error */
                return [
                    'status' => 403,
                    'message' => 'The iss do not match with this server',
                    'data' => [
                        'code' => 'jwt_auth_bad_iss',
                    ],
                ];
            }
            /** So far so good, validate the user id in the token */
            if (!isset($token->data->user->id)) {
                /** No user id in the token, abort!! */
                return [
                    'status' => 403,
                    'message' => 'User ID not found in the token',
                    'data' => [
                        'code' => 'jwt_auth_bad_request',
                    ],
                ];
            }
            /** Everything looks good return the decoded token if the $output is false */
            if (!$output) {
                return $token;
            }

            /** If the output is true return an answer to the request to show it */
            return [
                'status' => 200,
                'message' => 'mantul',
                'data' => [
                    'code' => 'jwt_auth_valid_token',
                    'user_id' => (int)$token->data->user->id,
                    'user_email' => $token->data->user->user_email,
                    'display_name' => $token->data->user->display_name,
                    'token' => $tokenJwt,
                    'user_emas' => [
                        // 'data' => $token->data,
                        'flag' => $token->data->user_emas->flag,
                        'trs_exist' => $token->data->user_emas->trs_exist,
                        'trs_user' => $token->data->user_emas->trs_user,
                        // 'expires_in' => $token->data->user_emas->expires_in,
                        'access_token' => $token->data->user_emas->access_token,
                        // 'refresh_token' => $token->data->user_emas->refresh_token
                    ]
                ],
            ];
        } catch (SignatureInvalidException $e) {
            /** Something is wrong trying to decode the token, send back the error */
            return [
                'status' => 403,
                'message' => $e->getMessage(),
                'data' => array(
                    'code' => 'jwt_auth_invalid_token',

                ),
            ];
        } catch (ExpiredException $e){
            return self::refreshToken($token);
        }
    }

    public static function refreshToken($token)
    {
        $secret_key = defined('JWT_AUTH_SECRET_KEY') ? JWT_AUTH_SECRET_KEY : false;
        libJwt::$leeway = 720000;
        $decoded = (array) libJwt::decode($token, $secret_key, ['HS256']);
    
        $user_id = $decoded["data"]->user->id;

        $decoded['iat'] = time();
        $decoded['exp'] = apply_filters('jwt_auth_expire', $decoded['iat'] + self::$expToken, $decoded['iat']);

        $token_ladara_emas = self::gld_token($user_id);
        
        $decoded['data']->user_emas = array();
        $decoded['data']->user_emas = $token_ladara_emas["data_user_emas"];

        $newToken =  libJwt::encode($decoded, $secret_key);
        return [
            'status' => 402,
            'message' => 'Refresh token',
            'data' => [
                'refresh_token' => $newToken,
                'token_type' => 'bearer',
                'expires_in' => $decoded['exp'],
                'user_emas' => [
                    // 'data' => $token->data,
                    'flag' => $token_ladara_emas["data_user_emas"]->flag,
                    'trs_exist' => $token_ladara_emas["data_user_emas"]->trs_exist,
                    'trs_user'  => $token_ladara_emas["data_user_emas"]->trs_user,
                    // 'expires_in' => $token_ladara_emas["data_user_emas"]->expires_in,
                    'access_token' => $token_ladara_emas["data_user_emas"]->access_token,
                    // 'refresh_token' => $token_ladara_emas["data_user_emas"]->refresh_token
                ]
            ]
        ];
    }

    public static function gld_token($user_id) {
        global $wpdb;

        $user_emas = $wpdb->get_row(
            "SELECT id, user_id, email, ts_password, verified_user FROM ldr_user_emas WHERE user_id=$user_id",
            OBJECT
        );

        $data_user_emas = new \stdClass();
        $data_user_emas->flag = false;
        $data_user_emas->trs_exist = false;
        $data_user_emas->trs_user = false;
        $data_user_emas->expires_in = "empty";
        $data_user_emas->access_token = "empty";
        $data_user_emas->refresh_token = "empty";
            
        $token_user_emas = array();
        $token_user_emas['flag'] = false;
        $token_user_emas['trs_exist'] = false;
        $token_user_emas['trs_user'] = false;
        $token_user_emas['expires_in'] = "empty";
        $token_user_emas['access_token'] = "empty";
        $token_user_emas['refresh_token'] = "empty";

        if(!empty($user_emas)) {
            $ch = curl_init();

            //check kalau user adalah user lama ladara emas,
            // hit api update password treasury

            $pass_user_emas = "";
            if(!empty($user_emas->ts_password)) {
                $pass_user_emas = $user_emas->ts_password;
            } else {
                $get_user_web = $wpdb->get_row(
                    "SELECT ID, user_email, user_pass FROM ldr_users
                    WHERE ID=" . $user_id . "",
                    OBJECT
                );

                $body_login_partner = [
                    "client_id"     => getenv('TREASURY_CLIENT_ID'),
                    "client_secret" => getenv('TREASURY_CLIENT_SECRET'),
                    "grant_type"    => getenv('TREASURY_GRANT_CLIENT')
                ];

                curl_setopt_array($ch, array(
                    CURLOPT_URL => getenv('TREASURY_BASE_URL') . "/login",
                    CURLOPT_RETURNTRANSFER => true,
                    CURLOPT_ENCODING => "",
                    CURLOPT_MAXREDIRS => 0,
                    CURLOPT_TIMEOUT => 0,
                    CURLOPT_FOLLOWLOCATION => true,
                    CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                    CURLOPT_CUSTOMREQUEST => "POST",
                    CURLOPT_POSTFIELDS => $body_login_partner
                ));
                
                $output_login_partner = curl_exec($ch);
                curl_getinfo($ch, CURLINFO_EFFECTIVE_URL);
                curl_getinfo($ch, CURLINFO_HTTP_CODE);
                curl_close($ch);
                $json_login_partner = json_decode($output_login_partner);

                if(isset($json_login_partner->meta->code) && $json_login_partner->meta->code === 200) {

                    $body_update_password = [
                        "email"                 => $user_emas->email,
                        "password"              => $get_user_web->user_pass,
                        "password_confirmation" => $get_user_web->user_pass,
                    ];
                    
                    $pass_user_emas = $get_user_web->user_pass;

                    curl_setopt_array($ch, array(
                        CURLOPT_URL => getenv('TREASURY_BASE_URL') . "/update-password",
                        CURLOPT_RETURNTRANSFER => true,
                        CURLOPT_ENCODING => "",
                        CURLOPT_MAXREDIRS => 0,
                        CURLOPT_TIMEOUT => 0,
                        CURLOPT_FOLLOWLOCATION => true,
                        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                        CURLOPT_CUSTOMREQUEST => "POST",
                        CURLOPT_POSTFIELDS => $body_update_password,
                        CURLOPT_HTTPHEADER => array(
                            "Authorization: Bearer " . $json_login_partner->data->access_token
                        ),
                    ));

                    $output_update_password = curl_exec($ch);
                    curl_getinfo($ch, CURLINFO_EFFECTIVE_URL);
                    curl_getinfo($ch, CURLINFO_HTTP_CODE);
                    curl_close($ch);
                    $json_update_password = json_decode($output_update_password);

                    if(!isset($json_update_password->meta->code) || (isset($json_update_password->meta->code) && $json_update_password->meta->code !== 200)) {
                        gld_user_exist_handling(array("user_id"=>$user_id));
                    } else {
                        $data_user_emas->trs_exist = true;
                        $token_user_emas['trs_exist'] = true;

                        $wpdb->query("UPDATE ldr_user_emas SET 
                            web_password='" . $get_user_web->user_pass . "',
                            ts_password='" . $get_user_web->user_pass . "'
                            WHERE user_id=" . $user_id . " AND email='" . $user_emas->email . "'");
                    }

                }

            }

            if(!empty($pass_user_emas)) {
                $body_login_trs = [
                    "client_id"     => getenv('TREASURY_CLIENT_ID'),
                    "client_secret" => getenv('TREASURY_CLIENT_SECRET'),
                    "grant_type"    => getenv('TREASURY_GRANT_CUSTOMER'),
                    "email"         => $user_emas->email,
                    "password"      => $pass_user_emas
                ];

                // $ch = curl_init();
                curl_setopt($ch, CURLOPT_URL, getenv('TREASURY_BASE_URL') . "/login");
                curl_setopt($ch, CURLOPT_TIMEOUT, 0);
                curl_setopt($ch, CURLOPT_HTTPHEADER, array("Accept: application/json"));
                curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
                curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                curl_setopt($ch, CURLOPT_ENCODING, "");
                curl_setopt($ch, CURLOPT_POSTFIELDS, $body_login_trs);

                $output_login_trs = curl_exec($ch);
                curl_getinfo($ch, CURLINFO_EFFECTIVE_URL);
                curl_getinfo($ch, CURLINFO_HTTP_CODE);
                curl_close($ch);
                $login_trs = json_decode($output_login_trs);

                if($login_trs->meta->code === 200) {
                    $data_user_emas->flag = true;
                    $data_user_emas->trs_exist = true;
                    $data_user_emas->expires_in = $login_trs->data->expires_in;
                    $data_user_emas->access_token = $login_trs->data->access_token;
                    $data_user_emas->refresh_token = $login_trs->data->refresh_token;
                    
                    $token_user_emas['flag'] = true;
                    $token_user_emas['trs_exist'] = true;
                    $token_user_emas['expires_in'] = $login_trs->data->expires_in;
                    $token_user_emas['access_token'] = $login_trs->data->access_token;
                    $token_user_emas['refresh_token'] = $login_trs->data->refresh_token;
                }

                if($user_emas->trs_user == 1) {
                    $data_user_emas->trs_user = true;
                    $token_user_emas['trs_user'] = true;
                }
            }
        }

        $return = array(
            "data_user_emas"  => $data_user_emas,
            "token_user_emas" => $token_user_emas
        );

        return $return;

    }
}