<?php


namespace Ladara\Helpers;
use Kreait\Firebase\Exception\FirebaseException;
use Kreait\Firebase\Exception\MessagingException;
use Kreait\Firebase\Factory;
use Kreait\Firebase\Messaging\CloudMessage;
use Kreait\Firebase\Messaging\MulticastSendReport;


class FCM
{
    /**
     * title Notification
     * @var
     */
    private $title;

    /**
     * body notification
     * @var
     */
    private $body;

    /**
     * data notification
     * @var array
     */
    private $data = [];

    /**
     * user token fcm
     * @var
     */
    private $token=[];

    /**
     * function send notifications
     * @return MulticastSendReport
     * @throws FirebaseException
     * @throws MessagingException
     */
    public function send()
   {
       $factory = (new Factory)->withServiceAccount(WP_PLUGIN_DIR.'/ladara-api/service-account.json');
       $messaging = $factory->createMessaging();

       $message = CloudMessage::fromArray([
           'notification'=> [
               'title' => $this->getTitle(),
               'body' => $this->getBody(),
           ],
           'data' => $this->getData()
       ]);

       return $messaging->sendMulticast($message,$this->getToken());
   }

    /**
     * @return mixed
     */
    public function getToken()
    {
        return $this->token;
    }

    /**
     * @param array $tokens
     */
    public function setToken($tokens = [])
    {
        $this->token = $tokens;
    }

    /**
     * @return mixed
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @param mixed $title
     */
    public function setTitle($title)
    {
        $this->title = $title;
    }

    /**
     * @return mixed
     */
    public function getBody()
    {
        return $this->body;
    }

    /**
     * @param mixed $body
     */
    public function setBody($body)
    {
        $this->body = $body;
    }

    /**
     * @return mixed
     */
    public function getData()
    {
        return $this->data;
    }

    /**
     * @param mixed $data
     */
    public function setData($data=[])
    {
        $this->data = $data;
    }



}