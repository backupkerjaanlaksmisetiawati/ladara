<?php


namespace Ladara\Helpers;
use WP_REST_Controller;
use WP_REST_Response;

if ( ! defined( 'ABSPATH' ) ) {
    exit;
}

/**
 * Class BaseController
 * @package Ladara\Helpers
 * @author rikihandoyo
 */
class BaseController extends WP_REST_Controller
{
    /**
     * default name space/route ladara api
     * @var string
     */
    public $nameSpace = 'ladara-api/v1';

    /**
     * global function untuk send response error api
     * @param $data
     * @param string $message
     * @param int $status
     * @return WP_REST_Response
     */
    public function sendError($data, $message='Error', $status=403)
    {
        $resp = [
            'status' => $status,
            'message' => $message,
            'data' => $data
        ];
        return new WP_REST_Response( $resp, 200 );
    }

    /**
     * global function untuk send response success api
     * @param $data
     * @param string $message
     * @param int $status
     * @return WP_REST_Response
     */
    public function sendSuccess($data, $message='Success', $status=200)
    {
        $resp = [
            'status' => $status,
            'message' => $message,
            'data' => $data
        ];
        return new WP_REST_Response( $resp, 200 );
    }

}