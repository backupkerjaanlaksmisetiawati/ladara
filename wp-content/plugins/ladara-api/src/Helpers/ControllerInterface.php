<?php


namespace Ladara\Helpers;

if ( ! defined( 'ABSPATH' ) ) {
    exit;
}
/**
 * Interface ControllerInterface
 * @package Ladara\Helpers
 */
interface ControllerInterface
{
    /**
     * list route di controller untuk di register di wp
     * @return mixed
     */
    public function register_routes();
}