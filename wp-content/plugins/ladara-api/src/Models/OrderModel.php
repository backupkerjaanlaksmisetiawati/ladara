<?php


namespace Ladara\Models;


use Ladara\Helpers\ProductHelpers;

class OrderModel
{
    public static function detailOrder(int $orderId)
    {
        global $wpdb;
        if ($orderId){
            $order = wc_get_order($orderId);
            if (!$order){
                return false;
            }
            $query = "
                SELECT *
                FROM ldr_orders o
                LEFT JOIN ldr_orders_address oa ON oa.order_id = o.order_id
                LEFT JOIN ldr_payment_log pl ON pl.order_id = o.order_id
                WHERE o.order_id = '$orderId' LIMIT 1";
            $detailOrder = $wpdb->get_row($query, OBJECT);

            $order_date = date("d M Y",strtotime($order->get_date_created()));

            $alamat = $detailOrder->alamat.' '.$detailOrder->kecamatan.' '.$detailOrder->kota.' '.$detailOrder->provinsi.' '.$detailOrder->kode_pos;
            $products = [];
            foreach ($order->get_items() as $item){
                $seller_data = get_field('seller_name',$item->get_product_id());
                if (isset($seller_data) AND !empty($seller_data)) {
                    $seller_id = $seller_data->ID;
                    $seller_name = get_the_title($seller_id);
                }else{
                    $seller_id = 0;
                    $seller_name = '';
                }
                $product = wc_get_product( $item->get_product_id() );
                $dataItem = $item->get_data();
                $image_id = get_post_thumbnail_id($item->get_product_id());
                $thumb = wp_get_attachment_image_src( $image_id, 'medium' );
                $image['image'] = get_template_directory_uri().'/library/images/sorry.png';
                if($thumb){
                    $image['image'] = $thumb[0];
                }
//                $image['image'] = (new ProductHelpers())->getProductImage($product);
                $data['weight'] = ($product) ? $product->get_weight() * $item->get_quantity() : 0;
                $dataItem['seller_name'] = $seller_name;
                $products[] = array_merge($dataItem, $image,$data);
            }

            return [
                'user_id' => $detailOrder->user_id,
                'invoiceNo' => self::invoice($order->get_date_created(),$order->get_id()),
                'order_id' => $order->get_id(),
                'orderDate' => $order_date,
                'status' => $order->get_status(),
                'shipping' => [
                    'courier' => $detailOrder->courier_name,
                    'resi' => $detailOrder->courier_resi,
                    'total' => $order->get_shipping_total(),
                    'name' => $order->get_shipping_first_name().' '.$order->get_shipping_last_name(),
                    'telephone' => $order->get_billing_phone(),
                    'address' => $alamat
                ],
                'payment' => [
                    'method' => $detailOrder->payment_method,
                    'bank_code' => $detailOrder->bank_code,
                    'total' => $detailOrder->paid_amount == '' ? (int)$detailOrder->total_price : (int)$detailOrder->paid_amount,
                    'xendit_url' => $detailOrder->xendit_url
                ],
                'items' => $products
            ];
        }
        return false;
    }

    public static function getTotal($orderIds=[])
    {
        $resp = [];
        if (count($orderIds) > 0){
            global $wpdb;
            $idIm = implode(',',$orderIds);
            $q = "SELECT order_id, paid_amount FROM ldr_payment_log where order_id IN ($idIm)";
            $respQ = $wpdb->get_results($q, OBJECT);
            foreach ($respQ as $row){
                $resp[$row->order_id] = $row;
            }
            return $resp;
        }
        return $resp;
    }

    public static function invoice($dateCreated,$orderId)
    {
        return 'INV/'.date('dmY',strtotime($dateCreated)).'/'.$orderId;
    }

    public static function getOrder(array $orderIds)
    {
        global $wpdb;
        $resp=[];
        $idIm = implode(',',$orderIds);
        $query = "
                SELECT *
                FROM ldr_orders o
                WHERE o.order_id IN ($idIm)";
        $detailOrder = $wpdb->get_results($query, ARRAY_A);
        foreach ($detailOrder as $row){
            $resp[$row['order_id']] = $row;
        }
        return $resp;
    }


}