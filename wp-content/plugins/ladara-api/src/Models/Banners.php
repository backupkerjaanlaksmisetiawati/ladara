<?php


namespace Ladara\Models;
if ( ! defined( 'ABSPATH' ) ) {
    exit;
}

use GuzzleHttp\Client;
use Ladara\Helpers\ProductHelpers;
use WP_Query;

/**
 * Class Banners
 * @package Ladara\Models
 * @author rikihandoyo
 */
class Banners
{

    /**
     * query get all banners
     * @return array
     */
    public static function getAll()
    {
        $args=[
            'post_type'  => 'banner',
            'meta_key' => '_thumbnail_id',
        ];
        $loop = new WP_Query($args);
        $a=[];
        if ($loop->have_posts()){
            while ( $loop->have_posts()) {
                $loop->the_post();
                $post_id = get_the_ID();
                $bannerMobileId = get_post_meta($post_id, 'banner-mobile', true);
                $link = get_post_meta($post_id, 'link-belanja-sekarang', true);
                $linkpoto = wp_get_attachment_url($bannerMobileId);
                if ($linkpoto) {
                    $urlphoto = $linkpoto;
                } else {
                    $urlphoto = get_template_directory_uri().'/library/images/default_banner.jpg';
                }
//                https://ladara.id/product?keyword=dapur
//                https://ladara.id/product?category=tas
//                https://ladara.id/brand/anela-hijab
//                https://ladara.id/event/lorem

                $exLink = explode($_SERVER['SERVER_NAME'], $link);

                $type = 'no-type';
                $param = '';
                if (count($exLink)>0){
                    if (isset($exLink[1])){
                        $link2 = explode('?',$exLink[1]);
                        if (isset($link2[1])){
                            //keyword dan category
                            $ex3 = explode('=', $link2[1]);
                            if ($ex3[0] == 'keyword'){
                                $type = 'keyword';
                                $param = $ex3[1];
                            }else{
                                $type = 'category';
                                $term = explode('-', $ex3[1]);
                                $param = end($term) ?? '';
                            }
                        }else{
                            //brand dan event
                            $ex4 = explode('/', $exLink[1]);
                            if ($ex4[1] == 'brand'){
                                $type = 'brand';
                                $post = get_posts([
                                    'post_type'   => 'brand',
                                    'name' => $ex4[2],
                                ]);
                                $idPost= '';
                                if(count($post) > 0){
                                    $idPost = $post[0]->ID;
                                }
                                $param = $idPost;
                            }elseif($ex4[1] == 'event'){
                                $type = 'event';
                                $post = get_posts([
                                    'post_type'   => 'event',
                                    'name' => $ex4[2],
                                ]);
                                $idPost= '';
                                if(count($post) > 0){
                                    $idPost = $post[0]->ID;
                                }
                                $param = $idPost;
                            }else{
                                $type = 'no-type';
                                $param = '';
                            }
                        }
                    }
                }

                $a[] = [
                    'id'=> get_the_ID(),
                    'type' => 'banner',
                    'category' => $type,
                    'param' => $param,
                    'link'=> $link,
                    'title' => html_entity_decode(get_the_title()),
                    'content' => html_entity_decode(get_the_content()),
                    'image' => [
                        'full' => $urlphoto,
                        'medium'=> $urlphoto
                    ],
                ];
            }
        }
        return $a;
    }

    /**
     * query get all brands
     * @param        $limit
     * @param bool   $show_brand
     * @param int    $page
     * @param string $filter
     * @return array
     */
    public static function getBrands($limit, $show_brand=true, $page=0, $filter='', $with_product =true)
    {
        global $wpdb;
        $args=[
            'posts_per_page' => $limit,
            'post_type'  => 'brand',
            'status' => 'publish',
            'orderby' => 'title',
            'order' => 'ASC',
            'paged' => $page,
        ];
        if ($with_product){
            $args['with_product'] = true;
        }
        if ($show_brand){
            $args['orderby'] = 'rand';
//            $args['meta_query'] = [[
//                'key' => 'show_brand',
//                'value' => true,
//                'compare' => 'EXISTS',
//            ]];
        }
        if ($filter){
            $args['starts_with'] = $filter;
        }

        $loop = new WP_Query($args);
        $a=[];
        if ($loop->have_posts()){
            while ( $loop->have_posts()) {
                $loop->the_post();
                $post_id = get_the_ID();
                $linkpoto = wp_get_attachment_image_src(get_post_thumbnail_id($post_id), 'thumbnail');
                if ($linkpoto) {
                    $urlphoto = $linkpoto['0'];
                } else {
                    global $wpdb;
                    $getImage = $wpdb->get_row("SELECT * FROM ldr_posts where post_parent = $post_id");
                    if($getImage){
                        $urlphoto = str_replace(get_site_url(), IMAGE_URL,  $getImage->guid);
                    }else{
                        $urlphoto = get_template_directory_uri().'/library/images/default_sorry.jpg';
                    }
                }

                $a[] = [
                    'id'=> get_the_ID(),
                    'type' => 'brand',
                    'title' => html_entity_decode(get_the_title()),
                    'content' => html_entity_decode(get_the_content()),
                    'image' => [
                        'full' => str_replace(get_site_url(), IMAGE_URL,  $urlphoto),
                        'medium'=> str_replace(get_site_url(), IMAGE_URL,  $urlphoto),
                    ],
                ];
            }
        }
        return $a;
    }

    /**
     * query get all seller
     * @return array
     */
    public static function getSeller()
    {
        $args=[
            'posts_per_page' => -1,
            'post_type'  => 'seller',
            'status' => 'publish',
        ];
        $loop = new WP_Query($args);
        $data=[];
        if ($loop->have_posts()){
            while ( $loop->have_posts()) {
                $loop->the_post();
                $imageFull = get_the_post_thumbnail_url(null,'full');
                $imageMedium = get_the_post_thumbnail_url( null,'medium' );
                if (!$imageFull) {
                    $imageFull = get_site_url().'/wp-content/uploads/woocommerce-placeholder-125x125.png';
                }
                if (!$imageMedium) {
                    $imageMedium = get_site_url().'/wp-content/uploads/woocommerce-placeholder-125x125.png';
                }
                $data[] = [
                    'id'=> get_the_ID(),
                    'type' => 'brand',
                    'title' => html_entity_decode(get_the_title()),
                    'content' => html_entity_decode(get_the_content()),
                    'image' => [
                        'full' => $imageFull,
                        'medium'=> $imageMedium
                    ],
                ];
            }
        }
        return $data;
    }

    /**
     * @param integer $type
     * 1 = all promo pilihan dan brand deaal
     * 2 = promo pilihan
     * 3 = brand deal
     * @param int $limit
     * @return array
     */
    public static function getPromo($type=1,$limit=4)
    {
        $args=[
            'post_type'  => 'promo',
            'posts_per_page' => $limit
        ];
        if ($type == 2){
            $args['meta_key'] ='show_promo';
            $args['meta_value'] = 1;
        }
        if ($type == 3){
            $args['meta_key'] ='brand_deals';
            $args['meta_value'] = 1;
        }

        $loop = new WP_Query($args);
        $data=[];
        if ($loop->have_posts()){
            while ( $loop->have_posts()) {
                $loop->the_post();
                $imageMedium = get_the_post_thumbnail_url( null,'full' );

                if (!$imageMedium) {
                    $imageMedium = get_site_url().'/wp-content/uploads/woocommerce-placeholder-125x125.png';
                }
                $showPromo =get_post_meta(get_the_ID(),'show_promo', true);
                $banner = wp_get_attachment_image_url( (int)get_post_meta(get_the_ID(),'promo_banner',true),'full' );
                if ($showPromo != 1){
                    $banner = wp_get_attachment_image_url( (int)get_post_meta(get_the_ID(),'brand_deals_banner',true),'full' );
                }

                $data[] = [
                    'id'=> get_the_ID(),
                    'type' => $showPromo  == 1 ? 'promo' : 'brand' ,
//                    'all_meta' => get_post_meta(get_the_ID()),
                    'meta' => $showPromo,
                    'title' => html_entity_decode(get_the_title()),
                    'content' => html_entity_decode(get_the_content()),
                    'image' => [
                        'medium'=> $imageMedium,
                        'banner'=> $banner,
                    ],
                ];
            }
        }
        return $data;
    }

    /**
     * @param $id
     * @param int $userId
     * @param int $limit
     * @param int $page
     * @return array|string
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public static function getDetail($id, $userId = 0, $limit= 20, $page=1)
    {
        $post   = get_post( $id );
        if (!$post){
            return 'Post NotFound';
        }
        $imageMedium = get_the_post_thumbnail_url( $id,'full' );
        if (!$imageMedium) {
            $imageMedium = get_site_url().'/wp-content/uploads/woocommerce-placeholder-125x125.png';
        }
        $priductIds = get_post_meta($id,'list_promo_products');
        $priductId = get_post_meta($id,'produk_pilihan');
        $args = [
            'status' => 'publish',
            'include' => $priductIds[0] ?? [],
        ];
        $products = wc_get_products($args);

        $httpClient = new Client();
        try {
            $request = $httpClient->request('GET', MERCHANT_URL.'/api/product/search-products', [
                'json' => [
                    'id' => json_encode($priductId[0]),
                    'limit' => $limit,
                    'page' => $page
                ],
            ]);

            $newProduct = json_decode($request->getBody());
        } catch (\GuzzleHttp\Exception\RequestException $e) {
            $newProduct = json_decode($e->getResponse()->getBody()->getContents());
        }

        return [
            'id'=> $post->ID,
            'title' => $post->post_title,
            'content' => $post->post_content,
            'image' => [
                'medium'=> $imageMedium,
                'promo_banner'=> wp_get_attachment_image_url( (int)get_post_meta($id,'promo_banner',true),'full' ),
                'brand_deals_banner' => wp_get_attachment_image_url( (int)get_post_meta($id,'brand_deals_banner',true),'full' )
            ],
            'productNew' => $newProduct->data->products,
            'product' => (new ProductHelpers())->respProducts($userId, $products)
        ];
    }

}