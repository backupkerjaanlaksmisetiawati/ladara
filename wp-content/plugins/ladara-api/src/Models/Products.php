<?php


namespace Ladara\Models;

if ( ! defined( 'ABSPATH' ) ) {
    exit;
}

/**
 * Class Products
 * @package Ladara\Models
 * @author rikihandoyo
 */
class Products
{
    /**
     * query get all categories
     * @return array|int|\WP_Error
     */
    public static function getCategories()
    {
        $cat_args = array(
//            'orderby'    => '',
//            'order'      => '',
            'hide_empty' => false,
        );
        return get_terms( 'product_cat',$cat_args);
    }

    public static function getChildCategories($cateId)
    {
        $cat_args = array(
            'parent'      => $cateId,
            'hide_empty' => false
        );
        return get_terms( 'product_cat',$cat_args);
    }

    public static function getIdChildCategory($cateId)
    {
        $categories = self::getChildCategories($cateId);
        $data = [];
        foreach ($categories as $cate){
            $data[] = $cate->term_id;
        }
        $data[] = (int)$cateId;
        return $data;
    }


    /**
     * query check whislist
     * @param int $user_id
     * @param int $product_id
     * @return int
     */
    public static function checkWishList($user_id=0, $product_id=0)
    {
        if ($user_id != 0 && $product_id != 0){
            global $wpdb;
            $wpdb_query = "SELECT * FROM ldr_wishlist WHERE user_id = $user_id AND product_id = $product_id";
            $res_query = $wpdb->get_results($wpdb_query, OBJECT);
            if (count($res_query) > 0){
                return 1;
            }
        }
        return 0;
    }

    /**
     * query check product id apakah ada di wishlist user
     * @param $userId
     * @param $productIds
     * @return array
     */
    public static function checkWishListBulk($userId, $productIds)
    {
        $data = [];
        if ($userId != 0 && count($productIds) > 0){
            global $wpdb;
            $ids = implode(',',$productIds);
            $wpdb_query = "SELECT * FROM ldr_wishlist WHERE user_id = $userId AND product_id IN (".$ids.")";
            $res_query = $wpdb->get_results($wpdb_query, OBJECT);
            if (count($res_query) > 0){
                foreach ($res_query as $row){
                    $data[] = $row->product_id;
                }
                return $data;
            }
        }

        return $data;
    }

    /**
     * query get variant colors di product
     * @param $product_id
     * @return array
     */
    public static function getColors($product_id)
    {
        global $wpdb;
        $data= [];
        if ($product_id){
            $wpdb_query = "SELECT DISTINCT warna_value FROM ldr_stock WHERE product_id = $product_id ";
            $res_query = $wpdb->get_results($wpdb_query, OBJECT);
            $count_res = count($res_query);
            if($count_res > 0){
                foreach ($res_query as $key => $value) {
                    $data[] = $value->warna_value;
                }
            }
        }

        return $data;
    }


    /**
     * query get variant size product
     * @param $product_id
     * @return array
     */
    public static function getSize($product_id)
    {
        global $wpdb;
        $data= [];
        $wpdb_query = "SELECT DISTINCT ukuran_value FROM ldr_stock WHERE product_id = $product_id ";
        $res_query = $wpdb->get_results($wpdb_query, OBJECT);
        if(count($res_query) > 0){
            foreach ($res_query as $key => $value) {
                $data[] = $value->ukuran_value;
            }
        }

        return $data;
    }

    /**
     * query get variant nomor product
     * @param $product_id
     * @return array
     */
    public function getNomor($product_id)
    {
        global $wpdb;
        $data= [];
        $wpdb_query = "SELECT DISTINCT nomor_value FROM ldr_stock WHERE product_id = $product_id ";
        $res_query = $wpdb->get_results($wpdb_query, OBJECT);
        if(count($res_query) > 0){
            foreach ($res_query as $key => $value) {
                $data[] = $value->nomor_value;
            }
        }

        return $data;
    }

    public static function getVariant($product_id)
    {
        global $wpdb;
        $data= [
            'color' => [],
            'size' => [],
            'no' => [],
        ];
        $wpdb_query = "SELECT * FROM ldr_stock WHERE product_id = $product_id ";
        $res_query = $wpdb->get_results($wpdb_query, OBJECT);
        if(count($res_query) > 0){
            $color = [];
            $size = [];
            $no = [];
            foreach ($res_query as $key => $value) {
                if ($value->warna_value && !in_array($value->warna_value, $color)){
                    $data['color'][] = $value->warna_value;
                    $color[] = $value->warna_value;
                }
                if ($value->ukuran_value && !in_array($value->ukuran_value, $size)){
                    $data['size'][] = $value->ukuran_value;
                    $size[] = $value->ukuran_value;
                }
                if ($value->nomor_value && !in_array($value->nomor_value, $no)){
                    $data['no'][] = $value->nomor_value;
                    $no[] = $value->nomor_value;
                }
            }
        }

        return $data;
    }

    /**
     * query get variant colors multiple product id
     * @param array $product_id
     * @return array
     */
    public static function getBulkColors($product_id=[])
    {
        global $wpdb;
        $data= [];
        $ids = implode(',',$product_id);
        $wpdb_query = "SELECT DISTINCT warna_value, product_id FROM ldr_stock WHERE product_id IN (".$ids.")";
        $res_query = $wpdb->get_results($wpdb_query, OBJECT);
        if(count($res_query) > 0){
            foreach ($res_query as $value) {
                $data[$value->product_id][] = $value->warna_value;
            }
        }
        return $data;
    }

    /**
     * query Get product id from last seen
     * @param $userId
     * @param int $limit
     * @return array|bool|object|null
     */
    public static function getLastSeen($userId, $limit=10)
    {
        $productIds = [];
        if ($userId){
            global $wpdb;
            $wpdb_query = "select distinct product_id,
                        (
                            select max(date_created) from ldr_last_seen a where a.product_id = b.product_id
                        ) as date
                        from ldr_last_seen b
                        where user_id = $userId
                        order by date desc
                        limit $limit";
            $res_query = $wpdb->get_results($wpdb_query, OBJECT);
            if(count($res_query) > 0){
                foreach ($res_query as $row){
                    $productIds[$row->product_id] = (int)$row->product_id;
                }
            }
        }
        return $productIds;

    }

    /**
     * query add to last seen
     * @param int $userId
     * @param int $productId
     */
    public static function addToLastSeen($userId=0, $productId=0)
    {
        global $wpdb;
        if ($userId && $productId){
            $wpdb_query = "INSERT INTO ldr_last_seen (user_id, product_id) VALUES ($userId, $productId)";
            $res_query = $wpdb->get_results($wpdb_query, OBJECT);
        }
    }

    /**
     * get stock product
     * @param $productId
     * @return array|object|null
     */
    public static function getStock($productId)
    {
        $resp = [];
        global $wpdb;
        if ($productId){
            $wpdb_query = "SELECT * FROM ldr_stock where product_id=$productId";
            $resp = $wpdb->get_results($wpdb_query, ARRAY_A);
        }
        return $resp;
    }


}