<?php


namespace Ladara\Models;

if ( ! defined( 'ABSPATH' ) ) {
    exit;
}

class CartModel
{
    public static function getCart($userId)
    {
        $data = [];
        global $wpdb;
        if ($userId){
            $wpdb_query = /** @lang HSQLDB */
                "SELECT * FROM ldr_cart where user_id = $userId";
            $data = $wpdb->get_results($wpdb_query, OBJECT);
        }
        return $data;
    }
}