<?php


namespace Ladara\Models;


use Kreait\Firebase\Exception\FirebaseException;
use Kreait\Firebase\Exception\MessagingException;
use Ladara\Helpers\FCM;

class Notifications
{
    /**
     * query add token FCM
     * @param $userId
     * @param $token
     * @param string $old
     * @return bool
     */
    public static function addToken($userId, $token, $old='')
    {
        global $wpdb;
        if ($userId){
            if ($old != ''){
                //delete Old token
                $wpdb_query = "DELETE FROM ldr_notification_token WHERE user_id = $userId AND token = '$old'";
                $data = $wpdb->get_results($wpdb_query, OBJECT);
            }
            $wpdb_query = "INSERT INTO ldr_notification_token (user_id, token) VALUES ($userId, '$token')";
            $data = $wpdb->get_results($wpdb_query, OBJECT);
        }
        return true;

    }

    /**
     * Query insert notification dan send push notification
     * @param array $data
     * $data = [
     * 'userId' => 1, //wajib
     * 'title' => 'lorem', //wajib
     * 'descriptions' => 'desc', //wajib
     * 'type' => 'info', //pesanan, info, pembayaran, emas // wajib
     * 'orderId' => 1, // optional
     * 'data' => [] //array optional bisa diisi dengan data lainnya
     * ];
     * @return array
     * @throws FirebaseException
     * @throws MessagingException
     */
    public static function addNotification($data = [])
    {
        date_default_timezone_set("Asia/Jakarta");
        $nowdate = date('Y-m-d H:i:s');

        global $wpdb;
        $userId = $data['userId'] ?? null;
        $merchantId = $data['merchantId'] ?? null;
        $title = $data['title'] ?? false;
        $des = $data['descriptions'] ?? false;
        $orderId = $data['orderId'] ?? 0;
        $orderMerchantId = $data['orderMerchantId'] ?? 0;
        $type = $data['type'] ?? false; //pesanan, info, pembayaran, emas
        $arrData = $data['data'] ?? [];

        if ($type == 'emas'){
            $image = '/library/images/notif_emas.png';
        }elseif ($type == 'pesanan'){
            $image = '/library/images/notif_order.png';
        }elseif ($type == 'pembayaran'){
            $image = '/library/images/notif_transaction.png';
        }else{
            $image = '/library/images/notif_info.png';
        }

        if (($userId === null && $merchantId === null) || !$title || !$des || !$type || !is_array($arrData)){
            return [
                'msg' => 'Data ada yang kurang atau salah',
            ];
        }

        $qInsert = $wpdb->insert('ldr_notifications', [
            'user_id' => $userId,
            'merchant_id' => $merchantId,
            'order_id' => $orderId,
            'order_merchant_id' => $orderMerchantId,
            'type' => $type,
            'url_image' => $image,
            'title' => $title,
            'descriptions' => $des,
            'data' => json_encode($arrData),
            'date_created' => $nowdate,
        ]);

        if ($userId === null) {
            $tempMerchant = $wpdb->get_row("SELECT * FROM ldr_merchants WHERE id = $merchantId");
            $userId = $tempMerchant->user_id;
        }

        if ($qInsert){
            //Push notif
            $Qtokens = self::getTokenByUser($userId);
            if ($Qtokens != '' && count($Qtokens) > 0 ){
                $tokens = [];
                foreach ($Qtokens as $token){
                    $tokens[] = $token['token'];
                }

                $dataPush = [
                    'orderId' => $userId,
                    'type' => $type,
                    'image' => get_template_directory_uri().$image,
                    'title' => $title,
                    'body' => $des,
                ];
                $dataMerge = (count($arrData) > 0 ) ?  array_merge($dataPush, $data['data']) : $dataPush;

                $fcm = new FCM();
                $fcm->setToken($tokens);
                $fcm->setTitle($title);
                $fcm->setBody($des);
                $fcm->setData($dataMerge);
                $notif = $fcm->send();

                return [
                    'fcmBerhasil' => $notif->successes()->count(),
                    'fcmGagal' => $notif->failures()->count(),
                    'msg' => 'success',
                ];
            }
            return [
                'msg' => 'success',
            ];
        }
        return [
            'msg' => 'Gagal insert data',
        ];
    }

    /**
     * get notification by user
     * @param        $userId
     * @param int    $page
     * @param int    $merchantId
     * @return array
     */
    public static function getNotifications($userId,$page = 0,$merchantId = 0)
    {
        $resp = [];
        if ($userId){
            global $wpdb;
            if ($merchantId){
                $where = ' merchant_id = '.$merchantId;
            }else{
                $where = 'user_id= '.$userId;
            }
            if ($page != 0 ){
                $limit = 10;
                $offset = $page == 1 ? 0 : $page * $limit;
                $wpdb_query = "SELECT * FROM ldr_notifications WHERE $where ORDER BY id DESC LIMIT $limit OFFSET $offset";
            }else{
                $wpdb_query = "SELECT * FROM ldr_notifications WHERE $where ORDER BY id DESC";
            }

            $q =  $wpdb->get_results($wpdb_query, ARRAY_A);
            foreach ($q as $row){
                if ($row['data']){
                    $row['data'] = json_decode($row['data'], true);
                }
                if ($row['url_image']){
                    // $row['url_image'] = get_template_directory_uri().$row['url_image'];

                    $url_newimage = home_url().'/wp-content/themes/ladara2020';
                    $url_image = str_replace('https://ladara.id/wp-content/themes/ladara2020', '', $row['url_image']);
                    $url_image_notif = $url_newimage.$url_image;

                    $row['url_image'] = $url_image_notif;
                }
                $resp[] = $row;
            }
            return ['data' => $resp];
        }
        return ['msg' => 'Error'];
    }

    public static function detail($notifId)
    {
        if ($notifId) {
            global $wpdb;
            $wpdb_query = "SELECT * FROM ldr_notifications WHERE id= $notifId";
            $result = $wpdb->get_row($wpdb_query, ARRAY_A);
            if ($result){
                //Update status read
                $wpdb->update('ldr_notifications',[
                    'status' => 1
                ],['id' => $result['id']]);
            }
            if ($result['data']){
                $result['data'] = json_decode($result['data'], true);
            }
            if ($result['url_image']){
                $result['url_image'] = get_template_directory_uri().$result['url_image'];
            }

            return ['data' => $result];

        }
        return ['msg' => 'Error'];
    }

    public static function updateStatus($id, $status=1)
    {
        global $wpdb;
        $wpdb->update('ldr_notifications',[
            'status' => $status
        ],['id' => $id]);

        return $wpdb;
    }

    /**
     * Query get token by user
     * @param $userId
     * @return array|object|string|null
     */
    public static function getTokenByUser($userId)
    {
        if ($userId){
            global $wpdb;
            $wpdb_query = "SELECT token FROM ldr_notification_token WHERE user_id= $userId";
            return $wpdb->get_results($wpdb_query, ARRAY_A);
        }
        return 'User Id Not Found';

    }

}