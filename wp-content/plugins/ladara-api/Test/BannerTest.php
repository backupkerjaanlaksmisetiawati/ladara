<?php

use PHPUnit\Framework\TestCase;
use Ladara\Test\ConfigTest;
if ( ! defined( 'ABSPATH' ) ) {
    exit;
}
class BannerTest extends TestCase
{
    /**
     * @var \GuzzleHttp\Client
     */
    protected $client;

    protected function setUp() : Void
    {
        $this->client = new GuzzleHttp\Client([
            'base_uri' => ConfigTest::$baseUrl
        ]);
    }

    public function testBanners()
    {
        $response = $this->client->request('GET','banners');
        $resp = json_decode($response->getBody()->getContents());
        $this->assertEquals(200, $resp->status);
    }

    public function testBrands()
    {
        $response = $this->client->request('GET','brands');
        $resp = json_decode($response->getBody()->getContents());
        $this->assertEquals(200, $resp->status);
    }
}
