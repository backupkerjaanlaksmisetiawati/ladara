<?php

use Ladara\Test\ConfigTest;
use PHPUnit\Framework\TestCase;
if ( ! defined( 'ABSPATH' ) ) {
    exit;
}
class CategoriesTest extends TestCase
{
    /**
     * @var \GuzzleHttp\Client
     */
    protected $client;

    protected function setUp() : Void
    {
        $this->client = new GuzzleHttp\Client([
            'base_uri' => ConfigTest::$baseUrl
        ]);
    }

    public function testCategories()
    {
        $response = $this->client->request('GET','categories');
        $resp = json_decode($response->getBody()->getContents());
        $this->assertEquals(200, $resp->status);
    }
}
