<?php

use Ladara\Test\ConfigTest;
use PHPUnit\Framework\TestCase;
if ( ! defined( 'ABSPATH' ) ) {
    exit;
}

/**
 * Class AuthTest
 * @author rikihandoyo
 */
class AuthTest extends TestCase
{
    /**
     * @var \GuzzleHttp\Client
     */
    protected $client;
    /**
     * @var int
     */
    protected $userId=0;
    /**
     * @var string
     */
    protected $email = 'test@local.com';
    /**
     * @var string
     */
    protected $password = 'asdf1234';
    /**
     * @var string
     */
    protected $phone = '085764987673';
    /**
     * @var string
     */
    protected $fullname = 'Riki handoyo test';

    /**
     * @return Void
     */
    protected function setUp() : Void
    {
        $this->client = new GuzzleHttp\Client([
            'base_uri' => ConfigTest::$baseUrl
        ]);
    }

    /**
     * @param $id
     * @return mixed
     */
    private function setuserId($id)
    {
        return $this->userId = $id;
    }

    private function setJwt($jwt)
    {
        return $this->jwt = $jwt;
    }


    /**
     * Test Register tanpa email
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function testNoEmail()
    {
        $response = $this->client->request('POST','register',[
            'form_params' => [
                'password' => $this->password,
                'phone' => $this->phone,
                'fullname' => $this->fullname
            ]
        ]);
        $resp = json_decode($response->getBody()->getContents());

        $this->assertEquals(403, $resp->status);
    }

    /**
     * Test register tanpa password
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function testNoPassword()
    {
        $response = $this->client->request('POST','register',[
            'form_params' => [
                'email' => $this->email,
                'phone' => $this->phone,
                'fullname' => $this->fullname
            ]
        ]);
        $resp = json_decode($response->getBody()->getContents());

        $this->assertEquals(403, $resp->status);
    }

    /**
     * Test register tanpa full name
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function testNoFullname()
    {
        $response = $this->client->request('POST','register',[
            'form_params' => [
                'email' => $this->email,
                'password' => $this->password,
                'phone' => $this->phone,
            ]
        ]);
        $resp = json_decode($response->getBody()->getContents());

        $this->assertEquals(403, $resp->status);
    }

    /**
     * Test register tanpa phone
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function testNoPhone()
    {
        $response = $this->client->request('POST','register',[
            'form_params' => [
                'email' => $this->email,
                'password' => $this->password,
                'fullname' => $this->fullname
            ]
        ]);
        $resp = json_decode($response->getBody()->getContents());

        if ($resp->status == 200){
            $this->setuserId($resp->data->id);
            $this->deleteUser();
        }

        $this->assertEquals(200, $resp->status);
    }

    /**
     * Test Register
     */
    public function testRegister()
    {
        $response = $this->register();
        $this->assertEquals(200, $response->status);
    }

    /**
     * Test register dengan data user yang sudah ada
     */
    public function testUserExist()
    {
        $response = $this->register();

        $this->assertEquals(406, $response->status);
    }

    /**
     * Test login tanpa email
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function testLoginNoEmail()
    {
        $response = $this->client->request('POST','login',[
            'form_params' => [
                'password' => $this->password,
            ]
        ]);
        $resp = json_decode($response->getBody()->getContents());
        $this->assertEquals(403, $resp->status);
    }

    /**
     * Test register tanpa input password
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function testLoginNoPassword()
    {
        $response = $this->client->request('POST','login',[
            'form_params' => [
                'email' => $this->email,
            ]
        ]);
        $resp = json_decode($response->getBody()->getContents());
        $this->assertEquals(403, $resp->status);
    }

    /**
     * Test login
     */
    public function testLogin()
    {
        $resp = $this->login();
        $this->assertEquals(200, $resp->status);
    }

    /**
     * Test Get profile
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function testProfile()
    {
        $login = $this->login();
        $response = $this->client->request('GET','profile',[
            'headers' => [
                'Authorization' => 'Bearer '.$login->data->token
            ]
        ]);
        $resp = json_decode($response->getBody()->getContents());
        $this->assertEquals(200, $resp->status);
    }

    /**
     * Test get profile tanpa token jwt
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function testProfileNoToken()
    {
        $response = $this->client->request('GET','profile');
        $resp = json_decode($response->getBody()->getContents());
        $this->assertEquals(403, $resp->status);
    }


    /**
     * Test delete User
     */
    public function testDeleteUser()
    {
        $login = $this->login();
        $this->setuserId($login->data->user_id);
        $response = $this->deleteUser();
        $resp = json_decode($response->getBody()->getContents());
        $this->assertEquals(200, $resp->status);
    }

    /**
     * @return mixed
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    private function login()
    {
        $response = $this->client->request('POST','login',[
            'form_params' => [
                'email' => $this->email,
                'password' => $this->password,
            ]
        ]);
        return json_decode($response->getBody()->getContents());
    }

    /**
     * @return mixed
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    private function register()
    {
         $response = $this->client->request('POST','register',[
            'form_params' => [
                'email' => $this->email,
                'password' => $this->password,
                'phone' => $this->phone,
                'fullname' => $this->fullname
            ]
        ]);
        return json_decode($response->getBody()->getContents());
    }

    /**
     * @return mixed|\Psr\Http\Message\ResponseInterface
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    private function deleteUser()
    {
        return $response = $this->client->request('DELETE','user/'.$this->userId,[
            'headers' => [
                'Authorization' => 'Bearer '.ConfigTest::$jwt
            ]
        ]);
    }

}
