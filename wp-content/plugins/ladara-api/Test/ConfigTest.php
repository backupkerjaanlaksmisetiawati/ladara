<?php
namespace Ladara\Test;
if ( ! defined( 'ABSPATH' ) ) {
    exit;
}
class ConfigTest
{
    /**
     * base url ladara api
     * @var string
     */
    public static $baseUrl= 'http://d186e167.ngrok.io/wp-json/ladara-api/v1/';

    /**
     * @var string
     */
    public static $jwt='eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOlwvXC9sYWRhcmEudGVzdCIsImlhdCI6MTU4NTEyMjI5NCwibmJmIjoxNTg1MTIyMjk0LCJleHAiOjE1ODU3MjcwOTQsImRhdGEiOnsidXNlciI6eyJpZCI6IjE1In19fQ.KqSKrIq7Lgf1zmvqLP7Hfb4zEYS4yO13uUouCLxOCek';

}