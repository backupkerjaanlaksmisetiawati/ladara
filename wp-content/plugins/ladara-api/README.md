# Custom api untuk ladara

### Struktur folder
    src
    ├── Config       #Tempat untuk menyimpan Global config 
    ├── Controllers  #Tempat untuk membuat endpoint baru 
    ├── Helpers      #Tempat global Helpers
    └── Models       #Tempat Function query database
    Test #Tempat Testcase


## Buat endpoint baru
Buat class didalam folder src/Controllers dan extends BaseController implements ControllerInterface
didalam class wajib ada method register_routes

### Contoh controller
````
<?php
namespace Ladara\Controllers;

if ( ! defined( 'ABSPATH' ) ) {
    exit;
}
use Ladara\Helpers\BaseController;
use Ladara\Helpers\ControllerInterface;
use WP_REST_Controller;

class Test extends BaseController implements ControllerInterface
{
    public function register_routes()
    {
        register_rest_route( $this->nameSpace, //Default namespace plugin ini 'ladara-api/v1'
        'test', // URl routing
        [
            'methods' => 'GET', //Http method
            'callback' => [$this,'test'], //callback ke function
        ]);
    }

    public function test()
    {
        return 'Hello World';
    }

}
````
Patikan untuk run ketika membuat class baru
````
cd wp-content/plugins/ladara-api && composer dump-autoload -o
````

## Note: Jangan lupa membuat Unit Test untuk memudahakn perkerjaan.

# Happy Coding

