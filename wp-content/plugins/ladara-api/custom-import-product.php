<?php

//PRODUCT BRAND
function add_product_brand_importer( $options ) {

    // column slug => column name
    $options['product_brand'] = 'Product Brand';

    return $options;
}
add_filter( 'woocommerce_csv_product_import_mapping_options', 'add_product_brand_importer' );

function add_product_brand_to_mapping_screen( $columns ) {

    // potential column name => column slug
    $columns['Product Brand'] = 'product_brand';
    $columns['product brand'] = 'product_brand';

    return $columns;
}
add_filter( 'woocommerce_csv_product_import_mapping_default_columns', 'add_product_brand_to_mapping_screen' );

function process_import_product_brand( $object, $data ) {

    if ( ! empty( $data['product_brand'] ) ) {
        $object->update_meta_data( 'product_brand', $data['product_brand'] );
    }

    return $object;
}
add_filter( 'woocommerce_product_import_pre_insert_product_object', 'process_import_product_brand', 10, 2 );

//product description
function add_product_description_importer( $options ) {

    // column slug => column name
    $options['product_description'] = 'Product Descriptions';

    return $options;
}
add_filter( 'woocommerce_csv_product_import_mapping_options', 'add_product_description_importer' );

function add_product_description_to_mapping_screen( $columns ) {

    // potential column name => column slug
    $columns['Product Descriptions'] = 'product_description';
    $columns['product descriptions'] = 'product_description';

    return $columns;
}
add_filter( 'woocommerce_csv_product_import_mapping_default_columns', 'add_product_description_to_mapping_screen' );

function process_import_product_description( $object, $data ) {

    if ( ! empty( $data['product_description'] ) ) {
        $object->update_meta_data( 'product_description', $data['product_description'] );
    }

    return $object;
}
add_filter( 'woocommerce_product_import_pre_insert_product_object', 'process_import_product_description', 10, 2 );


//komisi_produk
function add_komisi_produk_importer( $options ) {

    // column slug => column name
    $options['komisi_produk'] = 'Product Komisi';

    return $options;
}
add_filter( 'woocommerce_csv_product_import_mapping_options', 'add_komisi_produk_importer' );

function add_komisi_produk_to_mapping_screen( $columns ) {

    // potential column name => column slug
    $columns['Product Komisi'] = 'komisi_produk';
    $columns['product komisi'] = 'komisi_produk';

    return $columns;
}
add_filter( 'woocommerce_csv_product_import_mapping_default_columns', 'add_komisi_produk_to_mapping_screen' );

function process_import_komisi_produk( $object, $data ) {

    if ( ! empty( $data['komisi_produk'] ) ) {
        $object->update_meta_data( 'komisi_produk', $data['komisi_produk'] );
    }

    return $object;
}
add_filter( 'woocommerce_product_import_pre_insert_product_object', 'process_import_komisi_produk', 10, 2 );

//product_bahan
function add_product_bahan_importer( $options ) {

    // column slug => column name
    $options['product_bahan'] = 'Product Bahan';

    return $options;
}
add_filter( 'woocommerce_csv_product_import_mapping_options', 'add_product_bahan_importer' );

function add_product_bahan_to_mapping_screen( $columns ) {

    // potential column name => column slug
    $columns['Product Bahan'] = 'product_bahan';
    $columns['product bahan'] = 'product_bahan';

    return $columns;
}
add_filter( 'woocommerce_csv_product_import_mapping_default_columns', 'add_product_bahan_to_mapping_screen' );

function process_import_product_bahan( $object, $data ) {

    if ( ! empty( $data['product_bahan'] ) ) {
        $object->update_meta_data( 'product_bahan', $data['product_bahan'] );
    }

    return $object;
}
add_filter( 'woocommerce_product_import_pre_insert_product_object', 'process_import_product_bahan', 10, 2 );

//product_ukuran
function add_product_ukuran_importer( $options ) {

    // column slug => column name
    $options['product_ukuran'] = 'Product Ukuran';

    return $options;
}
add_filter( 'woocommerce_csv_product_import_mapping_options', 'add_product_ukuran_importer' );

function add_product_ukuran_to_mapping_screen( $columns ) {

    // potential column name => column slug
    $columns['Product Ukuran'] = 'product_ukuran';
    $columns['product ukuran'] = 'product_ukuran';

    return $columns;
}
add_filter( 'woocommerce_csv_product_import_mapping_default_columns', 'add_product_ukuran_to_mapping_screen' );

function process_import_product_ukuran( $object, $data ) {

    if ( ! empty( $data['product_ukuran'] ) ) {
        $object->update_meta_data( 'product_ukuran', $data['product_ukuran'] );
    }

    return $object;
}
add_filter( 'woocommerce_product_import_pre_insert_product_object', 'process_import_product_ukuran', 10, 2 );

//product_rate
function add_product_rate_importer( $options ) {

    // column slug => column name
    $options['product_rate'] = 'Product Rate';

    return $options;
}
add_filter( 'woocommerce_csv_product_import_mapping_options', 'add_product_rate_importer' );

function add_product_rate_to_mapping_screen( $columns ) {

    // potential column name => column slug
    $columns['Product Rate'] = 'product_rate';
    $columns['product rate'] = 'product_rate';

    return $columns;
}
add_filter( 'woocommerce_csv_product_import_mapping_default_columns', 'add_product_rate_to_mapping_screen' );

function process_import_product_rate( $object, $data ) {

    if ( ! empty( $data['product_rate'] ) ) {
        $object->update_meta_data( 'product_rate', $data['product_rate'] );
    }

    return $object;
}
add_filter( 'woocommerce_product_import_pre_insert_product_object', 'process_import_product_rate', 10, 2 );

//seller_name
function add_seller_name_importer( $options ) {

    // column slug => column name
    $options['seller_name'] = 'Seller Name';

    return $options;
}
add_filter( 'woocommerce_csv_product_import_mapping_options', 'add_seller_name_importer' );

function add_seller_name_to_mapping_screen( $columns ) {

    // potential column name => column slug
    $columns['Seller Name'] = 'seller_name';
    $columns['seller name'] = 'seller_name';

    return $columns;
}
add_filter( 'woocommerce_csv_product_import_mapping_default_columns', 'add_seller_name_to_mapping_screen' );

function process_import_seller_name( $object, $data ) {

    if ( ! empty( $data['seller_name'] ) ) {
        $object->update_meta_data( 'seller_name', $data['seller_name'] );
    }

    return $object;
}
add_filter( 'woocommerce_product_import_pre_insert_product_object', 'process_import_seller_name', 10, 2 );

