<?php
/**
 * @wordpress-plugin
 * Plugin Name:       Custom Api Ladara
 * Plugin URI:        https://ladaraindinesia.com
 * Description:       Ini adalah custom api untuk ladara
 * Version:           0.2
 * Author:            Drife solution
 * Author URI:        https://drife.co.id
 * License:           GPL-2.0+
 * License URI:       http://www.gnu.org/licenses/gpl-2.0.txt
 */
//If this file is called directly, abort.
if (!defined('WPINC')) {
    die;
}

/**
 * Load dependecies managed by composer.
 */
require 'vendor/autoload.php';
require plugin_dir_path(__FILE__) . 'menu.php';
require plugin_dir_path(__FILE__) . 'custom-import-product.php';
require plugin_dir_path(__FILE__) . 'custom-query.php';
//require plugin_dir_path(__FILE__) . 'src/Config/Route.php';


class Ladara
{
    protected $controllers = [];
    protected $migrations = [];

    private $dirController = 'src/Controllers';
    private $dirDatabase = 'src/Database';

    /**
     * Register REST API routes.
     */
    public function register_rest_routes() {
        $controllers = scandir(plugin_dir_path(__FILE__).$this->dirController);
        foreach ( $controllers as $controller_name) {
            if ($controller_name != '.' && $controller_name != '..'){
                $name = explode('.',$controller_name);
                $class = "Ladara\\Controllers\\$name[0]";
                $this->controllers[ $name[0] ] = new $class();
                $this->controllers[ $name[0] ]->register_routes();
            }
        }
    }

    /**
     * migration database di semua class yang ada di folder database
     */
    public function migrationsDatabase() {
        $files = scandir(plugin_dir_path(__FILE__).$this->dirDatabase);
        foreach ( $files as $file) {
            if ($file != '.' && $file != '..' && $file != 'index.php'){
                $name = explode('.',$file);
                $class = "Ladara\\Database\\$name[0]";
                $class = new $class();
                $class->init();
            }
        }
    }
}

/**
 * fungsi untuk meregister semua route yang ada di controllers
 */
function init()
{
    $ladara = new Ladara();
    $ladara->register_rest_routes();
}

/**
 * Fungsi untuk migration database ketika plugin di load.
 * harusnya tidak berat karena didalam migration class terdapat version table jadi sql hanya akan di run ketika ada update version.
 */
function migration()
{
    $ladara = new Ladara();
    $ladara->migrationsDatabase();
}


add_action( 'rest_api_init', 'init');
add_action("plugins_loaded", "migration");
