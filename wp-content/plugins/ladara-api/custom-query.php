<?php

//Custom query
/**
 * Handle a custom 'product brand' query var to get products with the 'product_brand' meta.
 * @param array $query - Args for WP_Query.
 * @param array $query_vars - Query vars from WC_Product_Query.
 * @return array modified $query
 */
function handle_product_brand_query( $query, $query_vars ) {
    if ( ! empty( $query_vars['product_brand'] ) ) {
        $query['meta_query'][] = array(
            'key' => 'product_brand',
            'value' => $query_vars['product_brand'],
            'type' => 'numeric',
            'operator'  => 'IN'
        );
    }

    return $query;
}
add_filter( 'woocommerce_product_data_store_cpt_get_products_query', 'handle_product_brand_query', 10, 2 );


function handle_product_price_range_query( $query, $query_vars ) {
    if ( ! empty( $query_vars['price_range'] ) ) {
        $query['meta_query'][] = [
            'key' => '_price',
            'value' => $query_vars['price_range'],
            'type' => 'numeric',
            'compare' => 'BETWEEN',
        ];
    }

    return $query;
}
add_filter( 'woocommerce_product_data_store_cpt_get_products_query', 'handle_product_price_range_query', 10, 2 );


function __search_by_title_only( $search, $wp_query )
{
    global $wpdb;

    if ( empty( $search ) )
        return $search; // skip processing - no search term in query

    $q = $wp_query->query_vars;
    $n = ! empty( $q['exact'] ) ? '' : '%';

    $search =
    $searchand = '';

    foreach ( (array) $q['search_terms'] as $term ) {
        $term =  $wpdb->esc_like($term);
        $search .= "{$searchand}($wpdb->posts.post_title LIKE '{$n}{$term}{$n}')";
//        $search .= "OR ($wpdb->terms.name LIKE '{$n}{$term}{$n}')";
        $searchand = ' AND ';
    }

    if ( ! empty( $search ) ) {
        $search = " AND ({$search}) ";
        if ( ! is_user_logged_in() )
            $search .= " AND ($wpdb->posts.post_password = '') ";
    }

    return $search;
}
add_filter( 'posts_search', '__search_by_title_only', 500, 2 );

function wpse_298888_posts_where( $where, $query ) {
    global $wpdb;

    $starts_with = esc_sql( $query->get( 'starts_with' ) );

    if ( $starts_with ) {
        $where .= " AND $wpdb->posts.post_title LIKE '$starts_with%'";
    }

    return $where;
}
add_filter( 'posts_where', 'wpse_298888_posts_where', 10, 2 );

function pruduct_brand_exists( $where, $query ) {
    global $wp_query, $wpdb;

    if ($query->get('with_product')) {
        $where .= "
                  AND exists(
                  select pr.id from ldr_products pr
                  left join ldr_merchants m on m.id = pr.merchant_id
                  where pr.brand_id = ldr_posts.ID
                  AND pr.deleted_at is null
                  and m.deleted_at is null
                  and m.status = 1
                )";
    }

    return $where;
}
add_filter( 'posts_where', 'pruduct_brand_exists', 10, 2 );
