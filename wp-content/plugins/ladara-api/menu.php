<?php
function ladara_register_settings() {
    add_option( 'ldr_limit_kategori_pilihan', 7);
    register_setting( 'ladara_options_group', 'ldr_limit_kategori_pilihan', 'myplugin_callback' );

    add_option( 'ldr_limit_brand_pilihan', 7);
    register_setting( 'ladara_options_group', 'ldr_limit_brand_pilihan', 'myplugin_callback' );
}
add_action( 'admin_init', 'ladara_register_settings' );

function ladara_register_options_page() {
    add_options_page('Setting Ladara API', 'Ladara API', 'manage_options', 'ladara-api', 'ladara_options_page');
}
add_action('admin_menu', 'ladara_register_options_page');

 function ladara_options_page(){
?>
  <div>
  <?php screen_icon(); ?>
  <h1>Ladara API settings</h1>
  <form method="post" action="options.php">
      <?php settings_fields( 'ladara_options_group' ); ?>
      <table class="form-table">
          <tr>
              <th scope="row">
                  <label for="ldr_limit_kategori_pilihan">Limit Kategori pilihan</label>
              </th>
              <td>
                  <input type="text" id="ldr_limit_kategori_pilihan" name="ldr_limit_kategori_pilihan" value="<?php echo get_option('ldr_limit_kategori_pilihan'); ?>" />
              </td>
          </tr>
          <tr>
              <th scope="row">
                  <label for="ldr_limit_brand_pilihan">Limit Brands pilihan</label>
              </th>
              <td>
                  <input type="text" id="ldr_limit_brand_pilihan" name="ldr_limit_brand_pilihan" value="<?php echo get_option('ldr_limit_brand_pilihan'); ?>" />
              </td>
          </tr>
      </table>
      <?php  submit_button(); ?>
  </form>
  </div>
<?php } ?>
