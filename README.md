Ladara Ecommerce version 1.

Ladara Ecommerce untuk phase 1 menggunakan wordpress dengan custom database

silahkan update dan koreksi sendiri Built With nya dan keterangan project nya.


## Table of Contents
* [Built With](#built-with)
* [Getting Started](#getting-started)



## Built With ##
* [Wordpress](https://wordpress.com/) - Free and open-source content management system written in PHP and paired with a MySQL or MariaDB database. | version 5.3.2
* [Wordpress Woocommerce](https://wordpress.org/plugins/woocommerce/) - Flexible, open-source eCommerce solution built on WordPress. | version 3.9.3
* [Xendit API](https://www.xendit.co/id) - Payment gateway.
* [Treasury API](https://www.xendit.co/id) - Online gold trading. | version 2

[^](#table-of-contents)



## Getting started

### Aktifkan config wordpress
buat copy file dari `wp-config.php.example` dan rename menjadi `wp-config.php`. Untuk mengubah nama, disarankan lewat editor masing-masing, bukan dari file explorer htdocs local kalian.

silahkan ikuti step-step di bawah (dengan menggunakan visual studio code):
  1. Copy File

![Step 1](https://gitlab.com/iamneals/ladara/-/raw/master/wp-content/themes/ladara2020/library/images/readme_asset/Screenshot_11.jpg)
![Step 2](https://gitlab.com/iamneals/ladara/-/raw/master/wp-content/themes/ladara2020/library/images/readme_asset/Screenshot_12.jpg)

  2. Paste file dengan right click mouse pada index path (1 folder dengan wp-config.php.example dll), setelah itu akan ada file `wp-config.php copy.example` atau sejenisnya

![Step 3](https://gitlab.com/iamneals/ladara/-/raw/master/wp-content/themes/ladara2020/library/images/readme_asset/Screenshot_13.jpg)
![Step 4](https://gitlab.com/iamneals/ladara/-/raw/master/wp-content/themes/ladara2020/library/images/readme_asset/Screenshot_14.jpg)

  3. rename menjadi `wp-config.php`

![Step 5](https://gitlab.com/iamneals/ladara/-/raw/master/wp-content/themes/ladara2020/library/images/readme_asset/Screenshot_15.jpg)

### Setting config wordpress
ubah MySQL settings `DB_NAME`, `DB_USER`, `DB_PASSWORD` dan `DB_HOST`, menyesuaikan dengan local masing-masing
```php
define( 'DB_NAME', 'ladara_db' );
define( 'DB_USER', 'root' );
define( 'DB_PASSWORD', '' );
define( 'DB_HOST', 'localhost' );
```
bisa juga menambahkan config lainnya. kalau menambahkan config lainnya, config tersebut juga harus ditambahkan di file `wp-config.php.example`.

### Aktifkan environment configuration 3rd party
3rd party yang dipakai yaitu Xendit, Treasury, JNE, Facebook dan Google.
buat copy file dari `.env.example` dan rename menjadi `.env`. Untuk mengubah nama, disarankan lewat editor masing-masing, bukan dari file explorer htdocs local kalian.

silahkan ikuti step-step di bawah (dengan menggunakan visual studio code):
  1. Copy File

![Step aktifkan config xendit 1](https://gitlab.com/iamneals/ladara/-/raw/master/wp-content/themes/ladara2020/library/images/readme_asset/Screenshot_17.jpg)
![Step aktifkan config xendit 2](https://gitlab.com/iamneals/ladara/-/raw/master/wp-content/themes/ladara2020/library/images/readme_asset/Screenshot_16.jpg)

  2. Paste file dengan right click mouse pada index path (1 folder dengan wp-config.php.example dll), setelah itu akan ada file `.env copy.example` atau sejenisnya

![Step aktifkan config xendit 3](https://gitlab.com/iamneals/ladara/-/raw/master/wp-content/themes/ladara2020/library/images/readme_asset/Screenshot_18.jpg)
![Step aktifkan config xendit 4](https://gitlab.com/iamneals/ladara/-/raw/master/wp-content/themes/ladara2020/library/images/readme_asset/Screenshot_20.jpg)

  3. rename menjadi `.env`
  
![Step aktifkan config xendit 5](https://gitlab.com/iamneals/ladara/-/raw/master/wp-content/themes/ladara2020/library/images/readme_asset/Screenshot_21.jpg)

### Import database
* Buat database baru di local masing-masing dan import `ladara_db.sql` di database yang sudah kalian buat
![Step Setting database](https://gitlab.com/iamneals/ladara/-/raw/master/wp-content/themes/ladara2020/library/images/readme_asset/Screenshot_22.jpg)


### Informasi tambahan
* meng-ignore file untuk di local saja, tanpa harus di push ke Git. Akses folder `.git/info`, ubah file `exclude`, tambahkan file atau folder yang ingin di ignore di local masing-masing, contoh `notes/`. File ini berfungsi sama dengan `.gitignore` tapi hanya berlaku di local masing-masing dan tidak akan ter-push

[^](#table-of-contents)